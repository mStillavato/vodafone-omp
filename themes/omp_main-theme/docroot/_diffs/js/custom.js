
function closemodal(){
	
	$( ".modal" ).modal('hide');
	

};


$(function() {
if($(".combobox").is(':disabled')){
$(".combobox:disabled").parent().find(".add-on").css("display","none");
}else{
$(".combobox").parent().find(".add-on").css("display","inline-block");
};
});

$(function() {
    if ( document.location.href.indexOf('asset-insert') > -1 ) {
		$('.navbar li').removeClass("active");
        $('.navbar a:contains("Asset Insert")').parent().addClass("active");
    } 
	
	else if ( document.location.href.indexOf('sc-insert') > -1 ) {
		$('.navbar li').removeClass("active");
        $('.navbar a:contains("SC Insert")').parent().addClass("active");
	}
	
	else if ( document.location.href.indexOf('po-insert') > -1 ) {
		$('.navbar li').removeClass("active");
        $('.navbar a:contains("PO Insert")').parent().addClass("active");
	}
    
	else if ( document.location.href.indexOf('po-closed') > -1 ) {
		$('.navbar li').removeClass("active");
        $('.navbar a:contains("Closed")').parent().addClass("active");
	}
	
	else if ( document.location.href.indexOf('gr-closure') > -1 ) {
		$('.navbar li').removeClass("active");
        $('.navbar a:contains("GR Closure")').parent().addClass("active");
	}
    
	else if ( document.location.href.indexOf('gr-request') > -1 ) {
		$('.navbar li').removeClass("active");
        $('.navbar a:contains("GR Request")').parent().addClass("active");
	}
});
	

$(".advancedsearchlink").click(function(){
	$(".advancedsearchcontent").toggle(300);
})


	
	
	
	$(function() { 
	    $( ".datepicker" ).datepicker({
	    showOn: "both",
	      buttonImage: "../../omp_main-theme/images/datepicker.png",
	      buttonImageOnly: true,
	      dateFormat: 'dd/mm/yy',
	    });
	    
	  });
	  
	  
	  
	
 $(document).mouseup(function (e){
    var container = $(".advancedsearchcontent");
	var popoverlink = $(".advancedsearchlink");
	var sbumtsrch = $(".clssrch");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && !popoverlink.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0
		) // ... nor a descendant of the container
    {
        $('.advancedsearchcontent').hide(300);
		$('#advanced').trigger("reset");
		
    } else if (sbumtsrch.is(e.target)) {
	
	setTimeout(function() {$('.advancedsearchcontent').hide(300);}, 100);
	setTimeout(function() {$('#advanced').trigger("reset");}, 500);
	
	
	};
});

 /*$(document).mouseup(function (e){
    var container = $(".idsearch .popover");
	var popoverlink = $(".idsearchbtn");
	var sbumtsrch = $(".idsearch .btn-default");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && !popoverlink.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0
		&& container.length != 0
		) // ... nor a descendant of the container
    {
        $('.idsearchbtn').popover('toggle');

		
    } else if (sbumtsrch.is(e.target)) {
	
	setTimeout(function() {$('.idsearchbtn').popover('toggle')}, 500);
	$("#myTab li").removeClass('active');
	
	
	
	};
});*/


$( ".idsearch .submitbtn" ).click(function() {
	$("#myTab li").removeClass('active');
});


$(".modal").appendTo($("body"));



$(document).ready(function(){
		$('.combobox').combobox();
		if($(".combobox").is(':disabled')){
$(".combobox:disabled").parent().find(".add-on").css("display","none");
}else{
$(".combobox").parent().find(".add-on").css("display","inline-block");
};


$('.modal').on('shown.bs.modal', function (e) {
$("html").css("overflowY","hidden");
 setTimeout(function() {$( ".datepicker" ).datepicker({

	    showOn: "both",
	      buttonImage: "../../omp_main-theme/images/datepicker.png",
	      buttonImageOnly: true,
	      dateFormat: 'dd/mm/yy',
	    });
  
  $(".datepicker:disabled").each(function(){
	$(this).datepicker("destroy");
});
}, 1000);
});

$('.modal').on('hidden.bs.modal', function (e) {
  $("html").css("overflowY","scroll");
});

	
	$(".datepicker").datepicker({ dateFormat: 'dd-mm-yy' });
	
	$('.savebtn , .submitbtn').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 400, 'easeInOutQuint');
			return false;
		});
	
	
setTimeout(function() {$('input, textarea').placeholder()}, 500);

  var characters = 300;
    
    $(".txtareamax").keyup(function(){
	$(this).parent().find(".counter").append("You have <strong>"+  characters+"</strong> characters remaining");
        if($(this).val().length > characters){
        $(this).val($(this).val().substr(0, characters));
        }
    var remaining = characters -  $(this).val().length;
    $(this).parent().find(".counter").html("You have <strong>"+  remaining+"</strong> characters remaining");
    if(remaining <= 20)
    {
        $(this).parent().find(".counter").css("color","red");
    }
    else
    {
       $(this).parent().find(".counter").css("color","black");
    }
    });
	
	$('.txtareamax').bind('paste', function(e) {

$(this).parent().find(".counter").append("You have <strong>"+  characters+"</strong> characters remaining");
        if($(this).val().length > characters){
        $(this).val($(this).val().substr(0, characters));
        }
    var remaining = characters -  $(this).val().length;
    $(this).parent().find(".counter").html("You have <strong>"+  remaining+"</strong> characters remaining");
    if(remaining <= 20)
    {
        $(this).parent().find(".counter").css("color","red");
    }
    else
    {
       $(this).parent().find(".counter").css("color","black");
    }
});

if ($("#banner .nav li:first").hasClass("active")){

$(".idsearch").css("display","block");
$(".advreqnum").css("display","none");
    }
	else{
	       $(".idsearch").remove();
	}

});


$('.signoutbtn').tooltip();
$('.deletered').tooltip();
$('.tltp').tooltip();


$(function() {
	
	var windowheight = $(window).height();
	$('#wrapper').css('min-height', windowheight -100 + 'px');

    var loginmargintop = $(window).height()/2-220;

    if ( windowheight > 500 ) {
        $('.loginfieldset').css('margin-top', loginmargintop + 'px');
 	   } else{
 		   $('.loginfieldset').css('margin-top', '50px');
 		   
 	   		 }
    
			});
			
			$(function() {
			
			setTimeout(function() {
	$(".pproject").change(function() {
if($(this).val()==""){

					$(".combobox-container .subprojectcombo").prop('disabled',true);
					$(".subprojctdiv input").val("");
					$(".subprojctdiv .add-on").css("display","none");
};
});

/*$(".trackingcodeselect").change(function() {
if($(this).val()==""){

			$(".trackingrelated").val("");
			$(".trackingrelated").prop('disabled',false);
				$("#prwbscode").data('combobox').refresh();
				$("#vendor").data('combobox').refresh();
				$("#projectname").data('combobox').refresh();
				$("#budgetCategory").data('combobox').refresh();
				$("#budgetSubCategory").data('combobox').refresh();
				$("#macrodriver").data('combobox').refresh();
				$("#odnpname").data('combobox').refresh();
				$(".trackingrelatedDiv .add-on").css("display","inline-block");
				
				
				$("#subproject").prop('disabled',true);
				$("#subproject").data('combobox').refresh();
				$(".combobox-container .subprojectcombo").prop('disabled',true);
				$(".subprojctdiv input").val("");
				$(".subprojctdiv .add-on").css("display","none");
				
				$("#microdriver").prop('disabled',true);
				$("#microdriver").data('combobox').refresh();
				$(".combobox-container .microcombobox").prop('disabled',true);
				$(".microdiv input").val("");
				$(".microdiv .add-on").css("display","none");
};
});*/

/*$(".potrackingcodeselect").change(function() {
	
	if($(this).val()==""){
		$("#vendor").val(document.getElementById("vendorId").value);
		$("#macrodriver").val(document.getElementById("macroDriverId").value);
		$("#odnpname").val(document.getElementById("odnpNameId").value); 
		$("#projectname").val(document.getElementById("projectId").value);
		$("#wbscode").val(document.getElementById("wbsCodeId").value);
		$(".trackingrelated").prop('disabled',false);
		$("#wbscode").data('combobox').refresh();
		$("#vendor").data('combobox').refresh();
		$("#projectname").data('combobox').refresh();
		$("#macrodriver").data('combobox').refresh();
		$("#odnpname").data('combobox').refresh();
		
						
		$(".trackingrelatedDiv .add-on").css("display","inline-block");
		
		var subProjectId = document.getElementById("subProjectId").value;
		updateSubProjectValue("projectname","subproject",subProjectId);
		$("#subproject").prop('disabled',false);
		$("#subproject").data('combobox').refresh();
		$(".combobox-container .subprojectcombo").prop('disabled',false);
		$(".subprojctdiv .add-on").css("display","inline-block");
		
		
		
		var microDriverId = document.getElementById("microDriverId").value;
		updateMicroDriverSelectOptionsToValue("macrodriver","microdriver",microDriverId);
		$("#microdriver").prop('disabled',false);
		$("#microdriver").data('combobox').refresh();
		$(".combobox-container .microcombobox").prop('disabled',false);
		$(".microdiv .add-on").css("display","inline-block");
	};
	});*/

$(".macromacro").change(function() {
if($(this).val()==""){

					$(".combobox-container .microcombobox").prop('disabled',true);
					$(".microdiv input").val("");
					$(".microdiv .add-on").css("display","none");
};
});

}, 2000);
});

$('.default-value .aui-field-input, .defaultvalue').each(function() {
    var default_value = this.value;
    $(this).focus(function() {
        if(this.value == default_value) {
            this.value = '';
        }
    });
    $(this).blur(function() {
        if(this.value == '') {
            this.value = default_value;
        }
    });
});


	$('#antexprlist a').click(function (e) {
	  e.preventDefault()
	  $(this).tab('show')
	});
	
	
	$(".nav-tabs .dropdown-menu a").click(function(){
var str = $( this ).text();
$(".nav-tabs .dropdown-toggle span").text( str );
});

$(".created").click(function(){
$("#myTabDrop1 span").text("Others");
});


$(window).resize(function() {
	
	var windowheight = $(window).height();
	$('#wrapper').css('min-height', windowheight -100 + 'px');
	   var loginmargintop = $(window).height()/2-220;
	   if ( windowheight > 500 ) {
       $('.loginfieldset').css('margin-top', loginmargintop + 'px');
	   } else{
		   $('.loginfieldset').css('margin-top', '50px');
		   
	   }
	});