package com.hp.omp.model;

import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.GoodReceiptLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class GoodReceiptClp extends BaseModelImpl<GoodReceipt>
    implements GoodReceipt {
    private long _goodReceiptId;
    private String _goodReceiptNumber;
    private Date _requestDate;
    private double _percentage;
    private String _grValue;
    private Date _registerationDate;
    private String _createdUserId;
    private String _positionId;
    private BaseModel<?> _goodReceiptRemoteModel;
    private String _grFile;

    public GoodReceiptClp() {
    }

    public Class<?> getModelClass() {
        return GoodReceipt.class;
    }

    public String getModelClassName() {
        return GoodReceipt.class.getName();
    }

    public long getPrimaryKey() {
        return _goodReceiptId;
    }

    public void setPrimaryKey(long primaryKey) {
        setGoodReceiptId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_goodReceiptId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("goodReceiptId", getGoodReceiptId());
        attributes.put("goodReceiptNumber", getGoodReceiptNumber());
        attributes.put("requestDate", getRequestDate());
        attributes.put("percentage", getPercentage());
        attributes.put("grValue", getGrValue());
        attributes.put("registerationDate", getRegisterationDate());
        attributes.put("createdUserId", getCreatedUserId());
        attributes.put("positionId", getPositionId());
        attributes.put("grFile", getGrFile());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long goodReceiptId = (Long) attributes.get("goodReceiptId");

        if (goodReceiptId != null) {
            setGoodReceiptId(goodReceiptId);
        }

        String goodReceiptNumber = (String) attributes.get("goodReceiptNumber");

        if (goodReceiptNumber != null) {
            setGoodReceiptNumber(goodReceiptNumber);
        }

        Date requestDate = (Date) attributes.get("requestDate");

        if (requestDate != null) {
            setRequestDate(requestDate);
        }

        Double percentage = (Double) attributes.get("percentage");

        if (percentage != null) {
            setPercentage(percentage);
        }

        String grValue = (String) attributes.get("grValue");

        if (grValue != null) {
            setGrValue(grValue);
        }

        Date registerationDate = (Date) attributes.get("registerationDate");

        if (registerationDate != null) {
            setRegisterationDate(registerationDate);
        }

        String createdUserId = (String) attributes.get("createdUserId");

        if (createdUserId != null) {
            setCreatedUserId(createdUserId);
        }

        String positionId = (String) attributes.get("positionId");

        if (positionId != null) {
            setPositionId(positionId);
        }
        
        String grFile = (String) attributes.get("grFile");

        if (grFile != null) {
            setGrFile(grFile);
        }
    }

    public long getGoodReceiptId() {
        return _goodReceiptId;
    }

    public void setGoodReceiptId(long goodReceiptId) {
        _goodReceiptId = goodReceiptId;

        if (_goodReceiptRemoteModel != null) {
            try {
                Class<?> clazz = _goodReceiptRemoteModel.getClass();

                Method method = clazz.getMethod("setGoodReceiptId", long.class);

                method.invoke(_goodReceiptRemoteModel, goodReceiptId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getGoodReceiptNumber() {
        return _goodReceiptNumber;
    }

    public void setGoodReceiptNumber(String goodReceiptNumber) {
        _goodReceiptNumber = goodReceiptNumber;

        if (_goodReceiptRemoteModel != null) {
            try {
                Class<?> clazz = _goodReceiptRemoteModel.getClass();

                Method method = clazz.getMethod("setGoodReceiptNumber",
                        String.class);

                method.invoke(_goodReceiptRemoteModel, goodReceiptNumber);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public Date getRequestDate() {
        return _requestDate;
    }

    public void setRequestDate(Date requestDate) {
        _requestDate = requestDate;

        if (_goodReceiptRemoteModel != null) {
            try {
                Class<?> clazz = _goodReceiptRemoteModel.getClass();

                Method method = clazz.getMethod("setRequestDate", Date.class);

                method.invoke(_goodReceiptRemoteModel, requestDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public double getPercentage() {
        return _percentage;
    }

    public void setPercentage(double percentage) {
        _percentage = percentage;

        if (_goodReceiptRemoteModel != null) {
            try {
                Class<?> clazz = _goodReceiptRemoteModel.getClass();

                Method method = clazz.getMethod("setPercentage", double.class);

                method.invoke(_goodReceiptRemoteModel, percentage);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getGrValue() {
        return _grValue;
    }

    public void setGrValue(String grValue) {
        _grValue = grValue;

        if (_goodReceiptRemoteModel != null) {
            try {
                Class<?> clazz = _goodReceiptRemoteModel.getClass();

                Method method = clazz.getMethod("setGrValue", String.class);

                method.invoke(_goodReceiptRemoteModel, grValue);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public Date getRegisterationDate() {
        return _registerationDate;
    }

    public void setRegisterationDate(Date registerationDate) {
        _registerationDate = registerationDate;

        if (_goodReceiptRemoteModel != null) {
            try {
                Class<?> clazz = _goodReceiptRemoteModel.getClass();

                Method method = clazz.getMethod("setRegisterationDate",
                        Date.class);

                method.invoke(_goodReceiptRemoteModel, registerationDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getCreatedUserId() {
        return _createdUserId;
    }

    public void setCreatedUserId(String createdUserId) {
        _createdUserId = createdUserId;

        if (_goodReceiptRemoteModel != null) {
            try {
                Class<?> clazz = _goodReceiptRemoteModel.getClass();

                Method method = clazz.getMethod("setCreatedUserId", String.class);

                method.invoke(_goodReceiptRemoteModel, createdUserId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getPositionId() {
        return _positionId;
    }

    public void setPositionId(String positionId) {
        _positionId = positionId;

        if (_goodReceiptRemoteModel != null) {
            try {
                Class<?> clazz = _goodReceiptRemoteModel.getClass();

                Method method = clazz.getMethod("setPositionId", String.class);

                method.invoke(_goodReceiptRemoteModel, positionId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public java.lang.String getGrValueReadonly() {
        try {
            String methodName = "getGrValueReadonly";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getPercentageReadonly() {
        try {
            String methodName = "getPercentageReadonly";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getRegisterationDateReadonly() {
        try {
            String methodName = "getRegisterationDateReadonly";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getRegisterationDateText() {
        try {
            String methodName = "getRegisterationDateText";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getGrNumberReadonly() {
        try {
            String methodName = "getGrNumberReadonly";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.Double getMaxPercentage() {
        try {
            String methodName = "getMaxPercentage";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.Double returnObj = (java.lang.Double) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getFormattedGrValue() {
        try {
            String methodName = "getFormattedGrValue";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getGrValueText() {
        try {
            String methodName = "getGrValueText";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String antexFieldsStatus() {
        try {
            String methodName = "antexFieldsStatus";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public com.hp.omp.model.Position getPosition() {
        try {
            String methodName = "getPosition";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            com.hp.omp.model.Position returnObj = (com.hp.omp.model.Position) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getPercentageText() {
        try {
            String methodName = "getPercentageText";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getRequestDateReadonly() {
        try {
            String methodName = "getRequestDateReadonly";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getFormattedPercentageText() {
        try {
            String methodName = "getFormattedPercentageText";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public com.hp.omp.model.custom.GoodReceiptStatus getGoodReceiptStatus() {
        try {
            String methodName = "getGoodReceiptStatus";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            com.hp.omp.model.custom.GoodReceiptStatus returnObj = (com.hp.omp.model.custom.GoodReceiptStatus) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getRequestDateText() {
        try {
            String methodName = "getRequestDateText";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getFormattedGrValueText() {
        try {
            String methodName = "getFormattedGrValueText";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getFormattedPercentage() {
        try {
            String methodName = "getFormattedPercentage";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public BaseModel<?> getGoodReceiptRemoteModel() {
        return _goodReceiptRemoteModel;
    }

    public void setGoodReceiptRemoteModel(BaseModel<?> goodReceiptRemoteModel) {
        _goodReceiptRemoteModel = goodReceiptRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _goodReceiptRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_goodReceiptRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            GoodReceiptLocalServiceUtil.addGoodReceipt(this);
        } else {
            GoodReceiptLocalServiceUtil.updateGoodReceipt(this);
        }
    }

    @Override
    public GoodReceipt toEscapedModel() {
        return (GoodReceipt) ProxyUtil.newProxyInstance(GoodReceipt.class.getClassLoader(),
            new Class[] { GoodReceipt.class }, new AutoEscapeBeanHandler(this));
    }

    public GoodReceipt toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        GoodReceiptClp clone = new GoodReceiptClp();

        clone.setGoodReceiptId(getGoodReceiptId());
        clone.setGoodReceiptNumber(getGoodReceiptNumber());
        clone.setRequestDate(getRequestDate());
        clone.setPercentage(getPercentage());
        clone.setGrValue(getGrValue());
        clone.setRegisterationDate(getRegisterationDate());
        clone.setCreatedUserId(getCreatedUserId());
        clone.setPositionId(getPositionId());
        clone.setGrFile(getGrFile());
        
        return clone;
    }

    public int compareTo(GoodReceipt goodReceipt) {
        long primaryKey = goodReceipt.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof GoodReceiptClp)) {
            return false;
        }

        GoodReceiptClp goodReceipt = (GoodReceiptClp) obj;

        long primaryKey = goodReceipt.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(17);

        sb.append("{goodReceiptId=");
        sb.append(getGoodReceiptId());
        sb.append(", goodReceiptNumber=");
        sb.append(getGoodReceiptNumber());
        sb.append(", requestDate=");
        sb.append(getRequestDate());
        sb.append(", percentage=");
        sb.append(getPercentage());
        sb.append(", grValue=");
        sb.append(getGrValue());
        sb.append(", registerationDate=");
        sb.append(getRegisterationDate());
        sb.append(", createdUserId=");
        sb.append(getCreatedUserId());
        sb.append(", positionId=");
        sb.append(getPositionId());
        sb.append(", grFile=");
        sb.append(getGrFile());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(28);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.GoodReceipt");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>goodReceiptId</column-name><column-value><![CDATA[");
        sb.append(getGoodReceiptId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>goodReceiptNumber</column-name><column-value><![CDATA[");
        sb.append(getGoodReceiptNumber());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>requestDate</column-name><column-value><![CDATA[");
        sb.append(getRequestDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>percentage</column-name><column-value><![CDATA[");
        sb.append(getPercentage());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>grValue</column-name><column-value><![CDATA[");
        sb.append(getGrValue());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>registerationDate</column-name><column-value><![CDATA[");
        sb.append(getRegisterationDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>createdUserId</column-name><column-value><![CDATA[");
        sb.append(getCreatedUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>positionId</column-name><column-value><![CDATA[");
        sb.append(getPositionId());
        sb.append("]]></column-value></column>");
        sb.append(
                "<column><column-name>grFile</column-name><column-value><![CDATA[");
        sb.append(getGrFile());
        sb.append("]]></column-value></column>");
        
        sb.append("</model>");

        return sb.toString();
    }

	@AutoEscape
	public String getGrFile() {
		return _grFile;
	}

	public void setGrFile(String grFile) {
	       _grFile = grFile;

	        if (_goodReceiptRemoteModel != null) {
	            try {
	                Class<?> clazz = _goodReceiptRemoteModel.getClass();

	                Method method = clazz.getMethod("setGrFile",
	                        String.class);

	                method.invoke(_goodReceiptRemoteModel, grFile);
	            } catch (Exception e) {
	                throw new UnsupportedOperationException(e);
	            }
	        }
	}
}
