package com.hp.omp.model.custom;

public class ReportCheckIcDTO {
	private String 	purchaseRequestId;
	private String 	purchaseOrderId;
	private String 	status;
	private String 	fiscalYear;
	private Double	totalValue;
	private int		position;
	private Double	quantity;
	private Double	actualUnitCost;
	private Double	PositionCost;
	private String 	targaTecnica;
	private String 	evoLocation;
	private String  icApprover;
	private String  grRequestor;
	private String  grRequestDate;
	private String  prDescription;
	
	public String getPurchaseRequestId() {
		return purchaseRequestId;
	}
	public void setPurchaseRequestId(String purchaseRequestId) {
		this.purchaseRequestId = purchaseRequestId;
	}
	public String getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(String purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFiscalYear() {
		return fiscalYear;
	}
	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}
	public Double getTotalValue() {
		return totalValue;
	}
	public void setTotalValue(Double totalValue) {
		this.totalValue = totalValue;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getActualUnitCost() {
		return actualUnitCost;
	}
	public void setActualUnitCost(Double actualUnitCost) {
		this.actualUnitCost = actualUnitCost;
	}
	public Double getPositionCost() {
		return PositionCost;
	}
	public void setPositionCost(Double positionCost) {
		PositionCost = positionCost;
	}
	public String getTargaTecnica() {
		return targaTecnica;
	}
	public void setTargaTecnica(String targaTecnica) {
		this.targaTecnica = targaTecnica;
	}
	public String getEvoLocation() {
		return evoLocation;
	}
	public void setEvoLocation(String evoLocation) {
		this.evoLocation = evoLocation;
	}
	public String getIcApprover() {
		return icApprover;
	}
	public void setIcApprover(String icApprover) {
		this.icApprover = icApprover;
	}
	public String getGrRequestor() {
		return grRequestor;
	}
	public void setGrRequestor(String grRequestor) {
		this.grRequestor = grRequestor;
	}
	public String getGrRequestDate() {
		return grRequestDate;
	}
	public void setGrRequestDate(String grRequestDate) {
		this.grRequestDate = grRequestDate;
	}
	public String getPrDescription() {
		return prDescription;
	}
	public void setPrDescription(String prDescription) {
		this.prDescription = prDescription;
	}
}
