package com.hp.omp.model.custom;


public class GoodReceiptListDTO implements ListDTO {
	
	
	private String id;
	private String requestorName;
	private Long requestorID;
	private String vendor;
	private String subCaegory;
	private String totalValue;
	private String fiscalYear;
	private String description;
	private String status;
	private Integer currency;

	public String getIdentifier() {
		return id;
	}

	public void setIdentifier(String id) {
		this.id = id;
	}

	public String getRequestorName() {
		return requestorName;
	}

	public void setRequestorName(String requestorName) {
		this.requestorName = requestorName;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
		
	}

	public String getSubCategory() {
		return subCaegory;
	}

	public void setSubCategory(String subCategory) {
		this.subCaegory = subCategory;
	}

	public String getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(String totalValue) {
		this.totalValue = totalValue;
	}

	public String getFiscalYear() {
		return fiscalYear;
	}

	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
		
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getCurrency() {
		return currency;
	}

	public void setCurrency(Integer currency) {
		this.currency = currency;
		
	}

	public Long getRequestorID() {
		return requestorID;
	}

	public void setRequestorID(Long requestorID) {
		this.requestorID = requestorID;
	}

}
