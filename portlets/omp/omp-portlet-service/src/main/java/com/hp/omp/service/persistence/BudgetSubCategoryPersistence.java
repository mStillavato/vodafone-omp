package com.hp.omp.service.persistence;

import com.hp.omp.model.BudgetSubCategory;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the budget sub category service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see BudgetSubCategoryPersistenceImpl
 * @see BudgetSubCategoryUtil
 * @generated
 */
public interface BudgetSubCategoryPersistence extends BasePersistence<BudgetSubCategory> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link BudgetSubCategoryUtil} to access the budget sub category persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the budget sub category in the entity cache if it is enabled.
    *
    * @param budgetSubCategory the budget sub category
    */
    public void cacheResult(
        com.hp.omp.model.BudgetSubCategory budgetSubCategory);

    /**
    * Caches the budget sub categories in the entity cache if it is enabled.
    *
    * @param budgetSubCategories the budget sub categories
    */
    public void cacheResult(
        java.util.List<com.hp.omp.model.BudgetSubCategory> budgetSubCategories);

    /**
    * Creates a new budget sub category with the primary key. Does not add the budget sub category to the database.
    *
    * @param budgetSubCategoryId the primary key for the new budget sub category
    * @return the new budget sub category
    */
    public com.hp.omp.model.BudgetSubCategory create(long budgetSubCategoryId);

    /**
    * Removes the budget sub category with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param budgetSubCategoryId the primary key of the budget sub category
    * @return the budget sub category that was removed
    * @throws com.hp.omp.NoSuchBudgetSubCategoryException if a budget sub category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.BudgetSubCategory remove(long budgetSubCategoryId)
        throws com.hp.omp.NoSuchBudgetSubCategoryException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.BudgetSubCategory updateImpl(
        com.hp.omp.model.BudgetSubCategory budgetSubCategory, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the budget sub category with the primary key or throws a {@link com.hp.omp.NoSuchBudgetSubCategoryException} if it could not be found.
    *
    * @param budgetSubCategoryId the primary key of the budget sub category
    * @return the budget sub category
    * @throws com.hp.omp.NoSuchBudgetSubCategoryException if a budget sub category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.BudgetSubCategory findByPrimaryKey(
        long budgetSubCategoryId)
        throws com.hp.omp.NoSuchBudgetSubCategoryException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the budget sub category with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param budgetSubCategoryId the primary key of the budget sub category
    * @return the budget sub category, or <code>null</code> if a budget sub category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.BudgetSubCategory fetchByPrimaryKey(
        long budgetSubCategoryId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the budget sub categories.
    *
    * @return the budget sub categories
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.BudgetSubCategory> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the budget sub categories.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of budget sub categories
    * @param end the upper bound of the range of budget sub categories (not inclusive)
    * @return the range of budget sub categories
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.BudgetSubCategory> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the budget sub categories.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of budget sub categories
    * @param end the upper bound of the range of budget sub categories (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of budget sub categories
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.BudgetSubCategory> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the budget sub categories from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of budget sub categories.
    *
    * @return the number of budget sub categories
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
