package com.hp.omp.service.persistence;

import com.hp.omp.model.PRAudit;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the p r audit service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see PRAuditPersistenceImpl
 * @see PRAuditUtil
 * @generated
 */
public interface PRAuditPersistence extends BasePersistence<PRAudit> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link PRAuditUtil} to access the p r audit persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the p r audit in the entity cache if it is enabled.
    *
    * @param prAudit the p r audit
    */
    public void cacheResult(com.hp.omp.model.PRAudit prAudit);

    /**
    * Caches the p r audits in the entity cache if it is enabled.
    *
    * @param prAudits the p r audits
    */
    public void cacheResult(java.util.List<com.hp.omp.model.PRAudit> prAudits);

    /**
    * Creates a new p r audit with the primary key. Does not add the p r audit to the database.
    *
    * @param auditId the primary key for the new p r audit
    * @return the new p r audit
    */
    public com.hp.omp.model.PRAudit create(long auditId);

    /**
    * Removes the p r audit with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param auditId the primary key of the p r audit
    * @return the p r audit that was removed
    * @throws com.hp.omp.NoSuchPRAuditException if a p r audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PRAudit remove(long auditId)
        throws com.hp.omp.NoSuchPRAuditException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.PRAudit updateImpl(
        com.hp.omp.model.PRAudit prAudit, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the p r audit with the primary key or throws a {@link com.hp.omp.NoSuchPRAuditException} if it could not be found.
    *
    * @param auditId the primary key of the p r audit
    * @return the p r audit
    * @throws com.hp.omp.NoSuchPRAuditException if a p r audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PRAudit findByPrimaryKey(long auditId)
        throws com.hp.omp.NoSuchPRAuditException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the p r audit with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param auditId the primary key of the p r audit
    * @return the p r audit, or <code>null</code> if a p r audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PRAudit fetchByPrimaryKey(long auditId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the p r audits.
    *
    * @return the p r audits
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.PRAudit> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the p r audits.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of p r audits
    * @param end the upper bound of the range of p r audits (not inclusive)
    * @return the range of p r audits
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.PRAudit> findAll(int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the p r audits.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of p r audits
    * @param end the upper bound of the range of p r audits (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of p r audits
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.PRAudit> findAll(int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the p r audits from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of p r audits.
    *
    * @return the number of p r audits
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
