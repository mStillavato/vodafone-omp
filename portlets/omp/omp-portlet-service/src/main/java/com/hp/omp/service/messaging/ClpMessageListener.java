package com.hp.omp.service.messaging;

import com.hp.omp.service.BudgetCategoryLocalServiceUtil;
import com.hp.omp.service.BudgetSubCategoryLocalServiceUtil;
import com.hp.omp.service.BuyerLocalServiceUtil;
import com.hp.omp.service.CatalogoLocalServiceUtil;
import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.CostCenterLocalServiceUtil;
import com.hp.omp.service.CostCenterUserLocalServiceUtil;
import com.hp.omp.service.GRAuditLocalServiceUtil;
import com.hp.omp.service.GoodReceiptLocalServiceUtil;
import com.hp.omp.service.LayoutNotNssCatalogoLocalServiceUtil;
import com.hp.omp.service.MacroDriverLocalServiceUtil;
import com.hp.omp.service.MacroMicroDriverLocalServiceUtil;
import com.hp.omp.service.MicroDriverLocalServiceUtil;
import com.hp.omp.service.ODNPCodeLocalServiceUtil;
import com.hp.omp.service.ODNPNameLocalServiceUtil;
import com.hp.omp.service.POAuditLocalServiceUtil;
import com.hp.omp.service.PRAuditLocalServiceUtil;
import com.hp.omp.service.PositionAuditLocalServiceUtil;
import com.hp.omp.service.PositionLocalServiceUtil;
import com.hp.omp.service.ProjectLocalServiceUtil;
import com.hp.omp.service.ProjectSubProjectLocalServiceUtil;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.hp.omp.service.ReportLocalServiceUtil;
import com.hp.omp.service.SubProjectLocalServiceUtil;
import com.hp.omp.service.TrackingCodesLocalServiceUtil;
import com.hp.omp.service.VendorLocalServiceUtil;
import com.hp.omp.service.WbsCodeLocalServiceUtil;
import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;


public class ClpMessageListener extends BaseMessageListener {
    public static String getServletContextName() {
        return ClpSerializer.getServletContextName();
    }

    @Override
    protected void doReceive(Message message) throws Exception {
        String command = message.getString("command");
        String servletContextName = message.getString("servletContextName");

        if (command.equals("undeploy") &&
                servletContextName.equals(getServletContextName())) {
            BudgetCategoryLocalServiceUtil.clearService();

            BudgetSubCategoryLocalServiceUtil.clearService();

            BuyerLocalServiceUtil.clearService();

            CostCenterLocalServiceUtil.clearService();

            CostCenterUserLocalServiceUtil.clearService();

            GoodReceiptLocalServiceUtil.clearService();

            GRAuditLocalServiceUtil.clearService();

            MacroDriverLocalServiceUtil.clearService();

            MacroMicroDriverLocalServiceUtil.clearService();

            MicroDriverLocalServiceUtil.clearService();

            ODNPCodeLocalServiceUtil.clearService();

            ODNPNameLocalServiceUtil.clearService();

            POAuditLocalServiceUtil.clearService();

            PositionLocalServiceUtil.clearService();

            PositionAuditLocalServiceUtil.clearService();

            PRAuditLocalServiceUtil.clearService();

            ProjectLocalServiceUtil.clearService();

            ProjectSubProjectLocalServiceUtil.clearService();

            PurchaseOrderLocalServiceUtil.clearService();

            PurchaseRequestLocalServiceUtil.clearService();

            ReportLocalServiceUtil.clearService();

            SubProjectLocalServiceUtil.clearService();

            TrackingCodesLocalServiceUtil.clearService();

            VendorLocalServiceUtil.clearService();

            WbsCodeLocalServiceUtil.clearService();
            
            CatalogoLocalServiceUtil.clearService();
            
            LayoutNotNssCatalogoLocalServiceUtil.clearService();
        }
    }
}
