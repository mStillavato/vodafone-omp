/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service.persistence;

import java.util.List;

import com.hp.omp.model.LayoutNotNssCatalogo;
import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

/**
 * The persistence utility for the layoutNotNssCatalogo service. This utility wraps {@link LayoutNotNssCatalogoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see LayoutNotNssCatalogoPersistence
 * @see LayoutNotNssCatalogoPersistenceImpl
 * @generated
 */
public class LayoutNotNssCatalogoUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(LayoutNotNssCatalogo layoutNotNssCatalogo) {
		getPersistence().clearCache(layoutNotNssCatalogo);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<LayoutNotNssCatalogo> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<LayoutNotNssCatalogo> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<LayoutNotNssCatalogo> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static LayoutNotNssCatalogo update(LayoutNotNssCatalogo layoutNotNssCatalogo, boolean merge)
		throws SystemException {
		return getPersistence().update(layoutNotNssCatalogo, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static LayoutNotNssCatalogo update(LayoutNotNssCatalogo layoutNotNssCatalogo, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(layoutNotNssCatalogo, merge, serviceContext);
	}

	/**
	* Caches the layoutNotNssCatalogo in the entity cache if it is enabled.
	*
	* @param layoutNotNssCatalogo the layoutNotNssCatalogo
	*/
	public static void cacheResult(com.hp.omp.model.LayoutNotNssCatalogo layoutNotNssCatalogo) {
		getPersistence().cacheResult(layoutNotNssCatalogo);
	}

	/**
	* Caches the catalogos in the entity cache if it is enabled.
	*
	* @param catalogos the catalogos
	*/
	public static void cacheResult(
		java.util.List<com.hp.omp.model.LayoutNotNssCatalogo> layoutNotNssCatalogos) {
		getPersistence().cacheResult(layoutNotNssCatalogos);
	}

	/**
	* Creates a new layoutNotNssCatalogo with the primary key. Does not add the layoutNotNssCatalogo to the database.
	*
	* @param layoutNotNssCatalogoId the primary key for the new layoutNotNssCatalogo
	* @return the new layoutNotNssCatalogo
	*/
	public static com.hp.omp.model.LayoutNotNssCatalogo create(long layoutNotNssCatalogoId) {
		return getPersistence().create(layoutNotNssCatalogoId);
	}

	/**
	* Removes the layoutNotNssCatalogo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param layoutNotNssCatalogoId the primary key of the layoutNotNssCatalogo
	* @return the layoutNotNssCatalogo that was removed
	* @throws com.hp.omp.NoSuchLayoutNotNssCatalogoException if a layoutNotNssCatalogo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.hp.omp.model.LayoutNotNssCatalogo remove(long layoutNotNssCatalogoId)
		throws com.hp.omp.NoSuchLayoutNotNssCatalogoException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().remove(layoutNotNssCatalogoId);
	}

	public static com.hp.omp.model.LayoutNotNssCatalogo updateImpl(
		com.hp.omp.model.LayoutNotNssCatalogo layoutNotNssCatalogo, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(layoutNotNssCatalogo, merge);
	}

	/**
	* Returns the layoutNotNssCatalogo with the primary key or throws a {@link com.hp.omp.NoSuchLayoutNotNssCatalogoException} if it could not be found.
	*
	* @param layoutNotNssCatalogoId the primary key of the layoutNotNssCatalogo
	* @return the layoutNotNssCatalogo
	* @throws com.hp.omp.NoSuchLayoutNotNssCatalogoException if a layoutNotNssCatalogo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.hp.omp.model.LayoutNotNssCatalogo findByPrimaryKey(long layoutNotNssCatalogoId)
		throws com.hp.omp.NoSuchLayoutNotNssCatalogoException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPrimaryKey(layoutNotNssCatalogoId);
	}

	/**
	* Returns the layoutNotNssCatalogo with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param layoutNotNssCatalogoId the primary key of the layoutNotNssCatalogo
	* @return the layoutNotNssCatalogo, or <code>null</code> if a layoutNotNssCatalogo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.hp.omp.model.LayoutNotNssCatalogo fetchByPrimaryKey(long layoutNotNssCatalogoId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(layoutNotNssCatalogoId);
	}

	/**
	* Returns all the catalogos.
	*
	* @return the catalogos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.hp.omp.model.LayoutNotNssCatalogo> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the catalogos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of catalogos
	* @param end the upper bound of the range of catalogos (not inclusive)
	* @return the range of catalogos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.hp.omp.model.LayoutNotNssCatalogo> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the catalogos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of catalogos
	* @param end the upper bound of the range of catalogos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of catalogos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.hp.omp.model.LayoutNotNssCatalogo> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the catalogos from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of catalogos.
	*
	* @return the number of catalogos
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static LayoutNotNssCatalogoPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (LayoutNotNssCatalogoPersistence)PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
					LayoutNotNssCatalogoPersistence.class.getName());

			ReferenceRegistry.registerReference(LayoutNotNssCatalogoUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(LayoutNotNssCatalogoPersistence persistence) {
	}

	private static LayoutNotNssCatalogoPersistence _persistence;
}