package com.hp.omp.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class MacroDriverFinderUtil {
    private static MacroDriverFinder _finder;

    public static java.util.List<com.hp.omp.model.MacroDriver> getMacroDrivers(
        	int start, int end, String searchFilter)
            throws com.liferay.portal.kernel.exception.SystemException {
            return getFinder().getMacroDrivers(start, end, searchFilter);
        }

    public static MacroDriverFinder getFinder() {
        if (_finder == null) {
            _finder = (MacroDriverFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    MacroDriverFinder.class.getName());

            ReferenceRegistry.registerReference(MacroDriverFinderUtil.class,
                "_finder");
        }

        return _finder;
    }

    public void setFinder(MacroDriverFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(MacroDriverFinderUtil.class,
            "_finder");
    }
}
