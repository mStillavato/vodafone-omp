package com.hp.omp.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class PurchaseRequestFinderUtil {
    private static PurchaseRequestFinder _finder;

    public static java.lang.Long getPurchaseRequestCount(
        com.hp.omp.model.custom.SearchDTO searchDTO, int roleCode, long userId) {
        return getFinder().getPurchaseRequestCount(searchDTO, roleCode, userId);
    }

    public static java.util.List<com.hp.omp.model.custom.PrListDTO> getPurchaseRequestList(
        com.hp.omp.model.custom.SearchDTO searchDTO, int roleCode, long userId) {
        return getFinder().getPurchaseRequestList(searchDTO, roleCode, userId);
    }

    public static java.util.List<com.hp.omp.model.custom.PrListDTO> prSearch(
        com.hp.omp.model.custom.SearchDTO searchDTO, int roleCode, long userId) {
        return getFinder().prSearch(searchDTO, roleCode, userId);
    }

    public static java.util.List<com.hp.omp.model.custom.PrListDTO> getAntexPurchaseRequestList(
        com.hp.omp.model.custom.SearchDTO searchDTO) {
        return getFinder().getAntexPurchaseRequestList(searchDTO);
    }

    public static java.lang.Long getAntexPurchaseRequestCount(
        com.hp.omp.model.custom.SearchDTO searchDTO) {
        return getFinder().getAntexPurchaseRequestCount(searchDTO);
    }

    public static PurchaseRequestFinder getFinder() {
        if (_finder == null) {
            _finder = (PurchaseRequestFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    PurchaseRequestFinder.class.getName());

            ReferenceRegistry.registerReference(PurchaseRequestFinderUtil.class,
                "_finder");
        }

        return _finder;
    }

    public void setFinder(PurchaseRequestFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(PurchaseRequestFinderUtil.class,
            "_finder");
    }
}
