package com.hp.omp.service.persistence;

import com.hp.omp.model.PRAudit;
import com.hp.omp.service.PRAuditLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class PRAuditActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public PRAuditActionableDynamicQuery() throws SystemException {
        setBaseLocalService(PRAuditLocalServiceUtil.getService());
        setClass(PRAudit.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("auditId");
    }
}
