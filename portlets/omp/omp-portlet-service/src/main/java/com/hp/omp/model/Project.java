package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Project service. Represents a row in the &quot;PROJECT&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see ProjectModel
 * @see com.hp.omp.model.impl.ProjectImpl
 * @see com.hp.omp.model.impl.ProjectModelImpl
 * @generated
 */
public interface Project extends ProjectModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.ProjectImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
