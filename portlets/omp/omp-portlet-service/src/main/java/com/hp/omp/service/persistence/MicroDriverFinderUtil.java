package com.hp.omp.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class MicroDriverFinderUtil {
    private static MicroDriverFinder _finder;

    public static java.util.List<com.hp.omp.model.MicroDriver> getMicroDriverByMacroDriverId(
        java.lang.Long macroDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getFinder().getMicroDriverByMacroDriverId(macroDriverId);
    }
    
    public static java.util.List<com.hp.omp.model.MicroDriver> getMicroDriversList(
        	int start, int end, String searchFilter)
            throws com.liferay.portal.kernel.exception.SystemException {
            return getFinder().getMicroDriversList(start, end, searchFilter);
    }

    public static MicroDriverFinder getFinder() {
        if (_finder == null) {
            _finder = (MicroDriverFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    MicroDriverFinder.class.getName());

            ReferenceRegistry.registerReference(MicroDriverFinderUtil.class,
                "_finder");
        }

        return _finder;
    }

    public void setFinder(MicroDriverFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(MicroDriverFinderUtil.class,
            "_finder");
    }
}
