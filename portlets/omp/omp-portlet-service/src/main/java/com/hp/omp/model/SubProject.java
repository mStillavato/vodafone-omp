package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the SubProject service. Represents a row in the &quot;SUB_PROJECT&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see SubProjectModel
 * @see com.hp.omp.model.impl.SubProjectImpl
 * @see com.hp.omp.model.impl.SubProjectModelImpl
 * @generated
 */
public interface SubProject extends SubProjectModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.SubProjectImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
