package com.hp.omp.model;

import com.hp.omp.service.BudgetCategoryLocalServiceUtil;
import com.hp.omp.service.ClpSerializer;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class BudgetCategoryClp extends BaseModelImpl<BudgetCategory>
    implements BudgetCategory {
    private long _budgetCategoryId;
    private String _budgetCategoryName;
    private boolean _display;
    private BaseModel<?> _budgetCategoryRemoteModel;

    public BudgetCategoryClp() {
    }

    public Class<?> getModelClass() {
        return BudgetCategory.class;
    }

    public String getModelClassName() {
        return BudgetCategory.class.getName();
    }

    public long getPrimaryKey() {
        return _budgetCategoryId;
    }

    public void setPrimaryKey(long primaryKey) {
        setBudgetCategoryId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_budgetCategoryId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("budgetCategoryId", getBudgetCategoryId());
        attributes.put("budgetCategoryName", getBudgetCategoryName());
        attributes.put("display", getDisplay());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long budgetCategoryId = (Long) attributes.get("budgetCategoryId");

        if (budgetCategoryId != null) {
            setBudgetCategoryId(budgetCategoryId);
        }

        String budgetCategoryName = (String) attributes.get(
                "budgetCategoryName");

        if (budgetCategoryName != null) {
            setBudgetCategoryName(budgetCategoryName);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
    }

    public long getBudgetCategoryId() {
        return _budgetCategoryId;
    }

    public void setBudgetCategoryId(long budgetCategoryId) {
        _budgetCategoryId = budgetCategoryId;

        if (_budgetCategoryRemoteModel != null) {
            try {
                Class<?> clazz = _budgetCategoryRemoteModel.getClass();

                Method method = clazz.getMethod("setBudgetCategoryId",
                        long.class);

                method.invoke(_budgetCategoryRemoteModel, budgetCategoryId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getBudgetCategoryName() {
        return _budgetCategoryName;
    }

    public void setBudgetCategoryName(String budgetCategoryName) {
        _budgetCategoryName = budgetCategoryName;

        if (_budgetCategoryRemoteModel != null) {
            try {
                Class<?> clazz = _budgetCategoryRemoteModel.getClass();

                Method method = clazz.getMethod("setBudgetCategoryName",
                        String.class);

                method.invoke(_budgetCategoryRemoteModel, budgetCategoryName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;

        if (_budgetCategoryRemoteModel != null) {
            try {
                Class<?> clazz = _budgetCategoryRemoteModel.getClass();

                Method method = clazz.getMethod("setDisplay", boolean.class);

                method.invoke(_budgetCategoryRemoteModel, display);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getBudgetCategoryRemoteModel() {
        return _budgetCategoryRemoteModel;
    }

    public void setBudgetCategoryRemoteModel(
        BaseModel<?> budgetCategoryRemoteModel) {
        _budgetCategoryRemoteModel = budgetCategoryRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _budgetCategoryRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_budgetCategoryRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            BudgetCategoryLocalServiceUtil.addBudgetCategory(this);
        } else {
            BudgetCategoryLocalServiceUtil.updateBudgetCategory(this);
        }
    }

    @Override
    public BudgetCategory toEscapedModel() {
        return (BudgetCategory) ProxyUtil.newProxyInstance(BudgetCategory.class.getClassLoader(),
            new Class[] { BudgetCategory.class },
            new AutoEscapeBeanHandler(this));
    }

    public BudgetCategory toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        BudgetCategoryClp clone = new BudgetCategoryClp();

        clone.setBudgetCategoryId(getBudgetCategoryId());
        clone.setBudgetCategoryName(getBudgetCategoryName());
        clone.setDisplay(getDisplay());

        return clone;
    }

    public int compareTo(BudgetCategory budgetCategory) {
        long primaryKey = budgetCategory.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof BudgetCategoryClp)) {
            return false;
        }

        BudgetCategoryClp budgetCategory = (BudgetCategoryClp) obj;

        long primaryKey = budgetCategory.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{budgetCategoryId=");
        sb.append(getBudgetCategoryId());
        sb.append(", budgetCategoryName=");
        sb.append(getBudgetCategoryName());
        sb.append(", display=");
        sb.append(getDisplay());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(13);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.BudgetCategory");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>budgetCategoryId</column-name><column-value><![CDATA[");
        sb.append(getBudgetCategoryId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>budgetCategoryName</column-name><column-value><![CDATA[");
        sb.append(getBudgetCategoryName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>display</column-name><column-value><![CDATA[");
        sb.append(getDisplay());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
