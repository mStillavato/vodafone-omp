/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service.persistence;

import com.hp.omp.model.Catalogo;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the catalogo service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see CatalogoPersistenceImpl
 * @see CatalogoUtil
 * @generated
 */
public interface CatalogoPersistence extends BasePersistence<Catalogo> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link CatalogoUtil} to access the catalogo persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the catalogo in the entity cache if it is enabled.
	*
	* @param catalogo the catalogo
	*/
	public void cacheResult(com.hp.omp.model.Catalogo catalogo);

	/**
	* Caches the catalogos in the entity cache if it is enabled.
	*
	* @param catalogos the catalogos
	*/
	public void cacheResult(java.util.List<com.hp.omp.model.Catalogo> catalogos);

	/**
	* Creates a new catalogo with the primary key. Does not add the catalogo to the database.
	*
	* @param catalogoId the primary key for the new catalogo
	* @return the new catalogo
	*/
	public com.hp.omp.model.Catalogo create(long catalogoId);

	/**
	* Removes the catalogo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param catalogoId the primary key of the catalogo
	* @return the catalogo that was removed
	* @throws com.hp.omp.NoSuchCatalogoException if a catalogo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.hp.omp.model.Catalogo remove(long catalogoId)
		throws com.hp.omp.NoSuchCatalogoException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.hp.omp.model.Catalogo updateImpl(
		com.hp.omp.model.Catalogo catalogo, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the catalogo with the primary key or throws a {@link com.hp.omp.NoSuchCatalogoException} if it could not be found.
	*
	* @param catalogoId the primary key of the catalogo
	* @return the catalogo
	* @throws com.hp.omp.NoSuchCatalogoException if a catalogo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.hp.omp.model.Catalogo findByPrimaryKey(long catalogoId)
		throws com.hp.omp.NoSuchCatalogoException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the catalogo with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param catalogoId the primary key of the catalogo
	* @return the catalogo, or <code>null</code> if a catalogo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.hp.omp.model.Catalogo fetchByPrimaryKey(long catalogoId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the catalogos where fornitoreDesc = &#63;.
	*
	* @param fornitoreDesc the fornitore desc
	* @return the matching catalogos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.hp.omp.model.Catalogo> findByCatalogoVendor(
		java.lang.String fornitoreDesc)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the catalogos where fornitoreDesc = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param fornitoreDesc the fornitore desc
	* @param start the lower bound of the range of catalogos
	* @param end the upper bound of the range of catalogos (not inclusive)
	* @return the range of matching catalogos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.hp.omp.model.Catalogo> findByCatalogoVendor(
		java.lang.String fornitoreDesc, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the catalogos where fornitoreDesc = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param fornitoreDesc the fornitore desc
	* @param start the lower bound of the range of catalogos
	* @param end the upper bound of the range of catalogos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching catalogos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.hp.omp.model.Catalogo> findByCatalogoVendor(
		java.lang.String fornitoreDesc, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first catalogo in the ordered set where fornitoreDesc = &#63;.
	*
	* @param fornitoreDesc the fornitore desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching catalogo
	* @throws com.hp.omp.NoSuchCatalogoException if a matching catalogo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.hp.omp.model.Catalogo findByCatalogoVendor_First(
		java.lang.String fornitoreDesc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.hp.omp.NoSuchCatalogoException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first catalogo in the ordered set where fornitoreDesc = &#63;.
	*
	* @param fornitoreDesc the fornitore desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching catalogo, or <code>null</code> if a matching catalogo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.hp.omp.model.Catalogo fetchByCatalogoVendor_First(
		java.lang.String fornitoreDesc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last catalogo in the ordered set where fornitoreDesc = &#63;.
	*
	* @param fornitoreDesc the fornitore desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching catalogo
	* @throws com.hp.omp.NoSuchCatalogoException if a matching catalogo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.hp.omp.model.Catalogo findByCatalogoVendor_Last(
		java.lang.String fornitoreDesc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.hp.omp.NoSuchCatalogoException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last catalogo in the ordered set where fornitoreDesc = &#63;.
	*
	* @param fornitoreDesc the fornitore desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching catalogo, or <code>null</code> if a matching catalogo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.hp.omp.model.Catalogo fetchByCatalogoVendor_Last(
		java.lang.String fornitoreDesc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the catalogos before and after the current catalogo in the ordered set where fornitoreDesc = &#63;.
	*
	* @param catalogoId the primary key of the current catalogo
	* @param fornitoreDesc the fornitore desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next catalogo
	* @throws com.hp.omp.NoSuchCatalogoException if a catalogo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.hp.omp.model.Catalogo[] findByCatalogoVendor_PrevAndNext(
		long catalogoId, java.lang.String fornitoreDesc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.hp.omp.NoSuchCatalogoException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the catalogos.
	*
	* @return the catalogos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.hp.omp.model.Catalogo> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the catalogos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of catalogos
	* @param end the upper bound of the range of catalogos (not inclusive)
	* @return the range of catalogos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.hp.omp.model.Catalogo> findAll(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the catalogos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of catalogos
	* @param end the upper bound of the range of catalogos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of catalogos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.hp.omp.model.Catalogo> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the catalogos where fornitoreDesc = &#63; from the database.
	*
	* @param fornitoreDesc the fornitore desc
	* @throws SystemException if a system exception occurred
	*/
	public void removeByCatalogoVendor(java.lang.String fornitoreDesc)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the catalogos from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of catalogos where fornitoreDesc = &#63;.
	*
	* @param fornitoreDesc the fornitore desc
	* @return the number of matching catalogos
	* @throws SystemException if a system exception occurred
	*/
	public int countByCatalogoVendor(java.lang.String fornitoreDesc)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of catalogos.
	*
	* @return the number of catalogos
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}