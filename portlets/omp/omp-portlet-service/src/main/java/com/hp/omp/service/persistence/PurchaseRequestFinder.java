package com.hp.omp.service.persistence;

public interface PurchaseRequestFinder {
    public java.lang.Long getPurchaseRequestCount(
        com.hp.omp.model.custom.SearchDTO searchDTO, int roleCode, long userId);

    public java.util.List<com.hp.omp.model.custom.PrListDTO> getPurchaseRequestList(
        com.hp.omp.model.custom.SearchDTO searchDTO, int roleCode, long userId);

    public java.util.List<com.hp.omp.model.custom.PrListDTO> prSearch(
        com.hp.omp.model.custom.SearchDTO searchDTO, int roleCode, long userId);

    public java.util.List<com.hp.omp.model.custom.PrListDTO> getAntexPurchaseRequestList(
        com.hp.omp.model.custom.SearchDTO searchDTO);

    public java.lang.Long getAntexPurchaseRequestCount(
        com.hp.omp.model.custom.SearchDTO searchDTO);
}
