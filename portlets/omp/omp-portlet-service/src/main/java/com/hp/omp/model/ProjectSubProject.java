package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the ProjectSubProject service. Represents a row in the &quot;PROJECT_SUB_PROJECT&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see ProjectSubProjectModel
 * @see com.hp.omp.model.impl.ProjectSubProjectImpl
 * @see com.hp.omp.model.impl.ProjectSubProjectModelImpl
 * @generated
 */
public interface ProjectSubProject extends ProjectSubProjectModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.ProjectSubProjectImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
