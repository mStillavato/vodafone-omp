package com.hp.omp.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link PurchaseOrder}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       PurchaseOrder
 * @generated
 */
public class PurchaseOrderWrapper implements PurchaseOrder,
    ModelWrapper<PurchaseOrder> {
    private PurchaseOrder _purchaseOrder;

    public PurchaseOrderWrapper(PurchaseOrder purchaseOrder) {
        _purchaseOrder = purchaseOrder;
    }

    public Class<?> getModelClass() {
        return PurchaseOrder.class;
    }

    public String getModelClassName() {
        return PurchaseOrder.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("purchaseOrderId", getPurchaseOrderId());
        attributes.put("costCenterId", getCostCenterId());
        attributes.put("vendorId", getVendorId());
        attributes.put("buyer", getBuyer());
        attributes.put("ola", getOla());
        attributes.put("projectId", getProjectId());
        attributes.put("subProjectId", getSubProjectId());
        attributes.put("budgetCategoryId", getBudgetCategoryId());
        attributes.put("budgetSubCategoryId", getBudgetSubCategoryId());
        attributes.put("activityDescription", getActivityDescription());
        attributes.put("macroDriverId", getMacroDriverId());
        attributes.put("microDriverId", getMicroDriverId());
        attributes.put("odnpCodeId", getOdnpCodeId());
        attributes.put("odnpNameId", getOdnpNameId());
        attributes.put("totalValue", getTotalValue());
        attributes.put("currency", getCurrency());
        attributes.put("receiverUserId", getReceiverUserId());
        attributes.put("fiscalYear", getFiscalYear());
        attributes.put("screenNameRequester", getScreenNameRequester());
        attributes.put("screenNameReciever", getScreenNameReciever());
        attributes.put("automatic", getAutomatic());
        attributes.put("status", getStatus());
        attributes.put("createdDate", getCreatedDate());
        attributes.put("createdUserId", getCreatedUserId());
        attributes.put("wbsCodeId", getWbsCodeId());
        attributes.put("trackingCode", getTrackingCode());
        attributes.put("buyerId", getBuyerId());
        attributes.put("targaTecnica", getTargaTecnica());
        attributes.put("evoLocationId", getEvoLocationId());
        attributes.put("tipoPr", getTipoPr());
        attributes.put("plant", getPlant());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long purchaseOrderId = (Long) attributes.get("purchaseOrderId");

        if (purchaseOrderId != null) {
            setPurchaseOrderId(purchaseOrderId);
        }

        String costCenterId = (String) attributes.get("costCenterId");

        if (costCenterId != null) {
            setCostCenterId(costCenterId);
        }

        String vendorId = (String) attributes.get("vendorId");

        if (vendorId != null) {
            setVendorId(vendorId);
        }

        String buyer = (String) attributes.get("buyer");

        if (buyer != null) {
            setBuyer(buyer);
        }

        String ola = (String) attributes.get("ola");

        if (ola != null) {
            setOla(ola);
        }

        String projectId = (String) attributes.get("projectId");

        if (projectId != null) {
            setProjectId(projectId);
        }

        String subProjectId = (String) attributes.get("subProjectId");

        if (subProjectId != null) {
            setSubProjectId(subProjectId);
        }

        String budgetCategoryId = (String) attributes.get("budgetCategoryId");

        if (budgetCategoryId != null) {
            setBudgetCategoryId(budgetCategoryId);
        }

        String budgetSubCategoryId = (String) attributes.get(
                "budgetSubCategoryId");

        if (budgetSubCategoryId != null) {
            setBudgetSubCategoryId(budgetSubCategoryId);
        }

        String activityDescription = (String) attributes.get(
                "activityDescription");

        if (activityDescription != null) {
            setActivityDescription(activityDescription);
        }

        String macroDriverId = (String) attributes.get("macroDriverId");

        if (macroDriverId != null) {
            setMacroDriverId(macroDriverId);
        }

        String microDriverId = (String) attributes.get("microDriverId");

        if (microDriverId != null) {
            setMicroDriverId(microDriverId);
        }

        String odnpCodeId = (String) attributes.get("odnpCodeId");

        if (odnpCodeId != null) {
            setOdnpCodeId(odnpCodeId);
        }

        String odnpNameId = (String) attributes.get("odnpNameId");

        if (odnpNameId != null) {
            setOdnpNameId(odnpNameId);
        }

        String totalValue = (String) attributes.get("totalValue");

        if (totalValue != null) {
            setTotalValue(totalValue);
        }

        String currency = (String) attributes.get("currency");

        if (currency != null) {
            setCurrency(currency);
        }

        String receiverUserId = (String) attributes.get("receiverUserId");

        if (receiverUserId != null) {
            setReceiverUserId(receiverUserId);
        }

        String fiscalYear = (String) attributes.get("fiscalYear");

        if (fiscalYear != null) {
            setFiscalYear(fiscalYear);
        }

        String screenNameRequester = (String) attributes.get(
                "screenNameRequester");

        if (screenNameRequester != null) {
            setScreenNameRequester(screenNameRequester);
        }

        String screenNameReciever = (String) attributes.get(
                "screenNameReciever");

        if (screenNameReciever != null) {
            setScreenNameReciever(screenNameReciever);
        }

        Boolean automatic = (Boolean) attributes.get("automatic");

        if (automatic != null) {
            setAutomatic(automatic);
        }

        Integer status = (Integer) attributes.get("status");

        if (status != null) {
            setStatus(status);
        }

        Date createdDate = (Date) attributes.get("createdDate");

        if (createdDate != null) {
            setCreatedDate(createdDate);
        }

        Long createdUserId = (Long) attributes.get("createdUserId");

        if (createdUserId != null) {
            setCreatedUserId(createdUserId);
        }

        String wbsCodeId = (String) attributes.get("wbsCodeId");

        if (wbsCodeId != null) {
            setWbsCodeId(wbsCodeId);
        }

        String trackingCode = (String) attributes.get("trackingCode");

        if (trackingCode != null) {
            setTrackingCode(trackingCode);
        }

        String buyerId = (String) attributes.get("buyerId");

        if (buyerId != null) {
            setBuyerId(buyerId);
        }
        
        String targaTecnica = (String) attributes.get("targaTecnica");

        if (targaTecnica != null) {
            setTargaTecnica(targaTecnica);
        }
        
        String evoLocationId = (String) attributes.get("evoLocationId");

        if (evoLocationId != null) {
            setEvoLocationId(evoLocationId);
        }
        
        Integer tipoPr = (Integer) attributes.get("tipoPr");

        if (tipoPr != null) {
            setTipoPr(tipoPr);
        }
        
        String plant = (String) attributes.get("plant");

        if (plant != null) {
            setPlant(plant);
        }
    }

    /**
    * Returns the primary key of this purchase order.
    *
    * @return the primary key of this purchase order
    */
    public long getPrimaryKey() {
        return _purchaseOrder.getPrimaryKey();
    }

    /**
    * Sets the primary key of this purchase order.
    *
    * @param primaryKey the primary key of this purchase order
    */
    public void setPrimaryKey(long primaryKey) {
        _purchaseOrder.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the purchase order ID of this purchase order.
    *
    * @return the purchase order ID of this purchase order
    */
    public long getPurchaseOrderId() {
        return _purchaseOrder.getPurchaseOrderId();
    }

    /**
    * Sets the purchase order ID of this purchase order.
    *
    * @param purchaseOrderId the purchase order ID of this purchase order
    */
    public void setPurchaseOrderId(long purchaseOrderId) {
        _purchaseOrder.setPurchaseOrderId(purchaseOrderId);
    }

    /**
    * Returns the cost center ID of this purchase order.
    *
    * @return the cost center ID of this purchase order
    */
    public java.lang.String getCostCenterId() {
        return _purchaseOrder.getCostCenterId();
    }

    /**
    * Sets the cost center ID of this purchase order.
    *
    * @param costCenterId the cost center ID of this purchase order
    */
    public void setCostCenterId(java.lang.String costCenterId) {
        _purchaseOrder.setCostCenterId(costCenterId);
    }

    /**
    * Returns the vendor ID of this purchase order.
    *
    * @return the vendor ID of this purchase order
    */
    public java.lang.String getVendorId() {
        return _purchaseOrder.getVendorId();
    }

    /**
    * Sets the vendor ID of this purchase order.
    *
    * @param vendorId the vendor ID of this purchase order
    */
    public void setVendorId(java.lang.String vendorId) {
        _purchaseOrder.setVendorId(vendorId);
    }

    /**
    * Returns the buyer of this purchase order.
    *
    * @return the buyer of this purchase order
    */
    public java.lang.String getBuyer() {
        return _purchaseOrder.getBuyer();
    }

    /**
    * Sets the buyer of this purchase order.
    *
    * @param buyer the buyer of this purchase order
    */
    public void setBuyer(java.lang.String buyer) {
        _purchaseOrder.setBuyer(buyer);
    }

    /**
    * Returns the ola of this purchase order.
    *
    * @return the ola of this purchase order
    */
    public java.lang.String getOla() {
        return _purchaseOrder.getOla();
    }

    /**
    * Sets the ola of this purchase order.
    *
    * @param ola the ola of this purchase order
    */
    public void setOla(java.lang.String ola) {
        _purchaseOrder.setOla(ola);
    }

    /**
    * Returns the project ID of this purchase order.
    *
    * @return the project ID of this purchase order
    */
    public java.lang.String getProjectId() {
        return _purchaseOrder.getProjectId();
    }

    /**
    * Sets the project ID of this purchase order.
    *
    * @param projectId the project ID of this purchase order
    */
    public void setProjectId(java.lang.String projectId) {
        _purchaseOrder.setProjectId(projectId);
    }

    /**
    * Returns the sub project ID of this purchase order.
    *
    * @return the sub project ID of this purchase order
    */
    public java.lang.String getSubProjectId() {
        return _purchaseOrder.getSubProjectId();
    }

    /**
    * Sets the sub project ID of this purchase order.
    *
    * @param subProjectId the sub project ID of this purchase order
    */
    public void setSubProjectId(java.lang.String subProjectId) {
        _purchaseOrder.setSubProjectId(subProjectId);
    }

    /**
    * Returns the budget category ID of this purchase order.
    *
    * @return the budget category ID of this purchase order
    */
    public java.lang.String getBudgetCategoryId() {
        return _purchaseOrder.getBudgetCategoryId();
    }

    /**
    * Sets the budget category ID of this purchase order.
    *
    * @param budgetCategoryId the budget category ID of this purchase order
    */
    public void setBudgetCategoryId(java.lang.String budgetCategoryId) {
        _purchaseOrder.setBudgetCategoryId(budgetCategoryId);
    }

    /**
    * Returns the budget sub category ID of this purchase order.
    *
    * @return the budget sub category ID of this purchase order
    */
    public java.lang.String getBudgetSubCategoryId() {
        return _purchaseOrder.getBudgetSubCategoryId();
    }

    /**
    * Sets the budget sub category ID of this purchase order.
    *
    * @param budgetSubCategoryId the budget sub category ID of this purchase order
    */
    public void setBudgetSubCategoryId(java.lang.String budgetSubCategoryId) {
        _purchaseOrder.setBudgetSubCategoryId(budgetSubCategoryId);
    }

    /**
    * Returns the activity description of this purchase order.
    *
    * @return the activity description of this purchase order
    */
    public java.lang.String getActivityDescription() {
        return _purchaseOrder.getActivityDescription();
    }

    /**
    * Sets the activity description of this purchase order.
    *
    * @param activityDescription the activity description of this purchase order
    */
    public void setActivityDescription(java.lang.String activityDescription) {
        _purchaseOrder.setActivityDescription(activityDescription);
    }

    /**
    * Returns the macro driver ID of this purchase order.
    *
    * @return the macro driver ID of this purchase order
    */
    public java.lang.String getMacroDriverId() {
        return _purchaseOrder.getMacroDriverId();
    }

    /**
    * Sets the macro driver ID of this purchase order.
    *
    * @param macroDriverId the macro driver ID of this purchase order
    */
    public void setMacroDriverId(java.lang.String macroDriverId) {
        _purchaseOrder.setMacroDriverId(macroDriverId);
    }

    /**
    * Returns the micro driver ID of this purchase order.
    *
    * @return the micro driver ID of this purchase order
    */
    public java.lang.String getMicroDriverId() {
        return _purchaseOrder.getMicroDriverId();
    }

    /**
    * Sets the micro driver ID of this purchase order.
    *
    * @param microDriverId the micro driver ID of this purchase order
    */
    public void setMicroDriverId(java.lang.String microDriverId) {
        _purchaseOrder.setMicroDriverId(microDriverId);
    }

    /**
    * Returns the odnp code ID of this purchase order.
    *
    * @return the odnp code ID of this purchase order
    */
    public java.lang.String getOdnpCodeId() {
        return _purchaseOrder.getOdnpCodeId();
    }

    /**
    * Sets the odnp code ID of this purchase order.
    *
    * @param odnpCodeId the odnp code ID of this purchase order
    */
    public void setOdnpCodeId(java.lang.String odnpCodeId) {
        _purchaseOrder.setOdnpCodeId(odnpCodeId);
    }

    /**
    * Returns the odnp name ID of this purchase order.
    *
    * @return the odnp name ID of this purchase order
    */
    public java.lang.String getOdnpNameId() {
        return _purchaseOrder.getOdnpNameId();
    }

    /**
    * Sets the odnp name ID of this purchase order.
    *
    * @param odnpNameId the odnp name ID of this purchase order
    */
    public void setOdnpNameId(java.lang.String odnpNameId) {
        _purchaseOrder.setOdnpNameId(odnpNameId);
    }

    /**
    * Returns the total value of this purchase order.
    *
    * @return the total value of this purchase order
    */
    public java.lang.String getTotalValue() {
        return _purchaseOrder.getTotalValue();
    }

    /**
    * Sets the total value of this purchase order.
    *
    * @param totalValue the total value of this purchase order
    */
    public void setTotalValue(java.lang.String totalValue) {
        _purchaseOrder.setTotalValue(totalValue);
    }

    /**
    * Returns the currency of this purchase order.
    *
    * @return the currency of this purchase order
    */
    public java.lang.String getCurrency() {
        return _purchaseOrder.getCurrency();
    }

    /**
    * Sets the currency of this purchase order.
    *
    * @param currency the currency of this purchase order
    */
    public void setCurrency(java.lang.String currency) {
        _purchaseOrder.setCurrency(currency);
    }

    /**
    * Returns the receiver user ID of this purchase order.
    *
    * @return the receiver user ID of this purchase order
    */
    public java.lang.String getReceiverUserId() {
        return _purchaseOrder.getReceiverUserId();
    }

    /**
    * Sets the receiver user ID of this purchase order.
    *
    * @param receiverUserId the receiver user ID of this purchase order
    */
    public void setReceiverUserId(java.lang.String receiverUserId) {
        _purchaseOrder.setReceiverUserId(receiverUserId);
    }

    /**
    * Returns the fiscal year of this purchase order.
    *
    * @return the fiscal year of this purchase order
    */
    public java.lang.String getFiscalYear() {
        return _purchaseOrder.getFiscalYear();
    }

    /**
    * Sets the fiscal year of this purchase order.
    *
    * @param fiscalYear the fiscal year of this purchase order
    */
    public void setFiscalYear(java.lang.String fiscalYear) {
        _purchaseOrder.setFiscalYear(fiscalYear);
    }

    /**
    * Returns the screen name requester of this purchase order.
    *
    * @return the screen name requester of this purchase order
    */
    public java.lang.String getScreenNameRequester() {
        return _purchaseOrder.getScreenNameRequester();
    }

    /**
    * Sets the screen name requester of this purchase order.
    *
    * @param screenNameRequester the screen name requester of this purchase order
    */
    public void setScreenNameRequester(java.lang.String screenNameRequester) {
        _purchaseOrder.setScreenNameRequester(screenNameRequester);
    }

    /**
    * Returns the screen name reciever of this purchase order.
    *
    * @return the screen name reciever of this purchase order
    */
    public java.lang.String getScreenNameReciever() {
        return _purchaseOrder.getScreenNameReciever();
    }

    /**
    * Sets the screen name reciever of this purchase order.
    *
    * @param screenNameReciever the screen name reciever of this purchase order
    */
    public void setScreenNameReciever(java.lang.String screenNameReciever) {
        _purchaseOrder.setScreenNameReciever(screenNameReciever);
    }

    /**
    * Returns the automatic of this purchase order.
    *
    * @return the automatic of this purchase order
    */
    public boolean getAutomatic() {
        return _purchaseOrder.getAutomatic();
    }

    /**
    * Returns <code>true</code> if this purchase order is automatic.
    *
    * @return <code>true</code> if this purchase order is automatic; <code>false</code> otherwise
    */
    public boolean isAutomatic() {
        return _purchaseOrder.isAutomatic();
    }

    /**
    * Sets whether this purchase order is automatic.
    *
    * @param automatic the automatic of this purchase order
    */
    public void setAutomatic(boolean automatic) {
        _purchaseOrder.setAutomatic(automatic);
    }

    /**
    * Returns the status of this purchase order.
    *
    * @return the status of this purchase order
    */
    public int getStatus() {
        return _purchaseOrder.getStatus();
    }

    /**
    * Sets the status of this purchase order.
    *
    * @param status the status of this purchase order
    */
    public void setStatus(int status) {
        _purchaseOrder.setStatus(status);
    }

    /**
    * Returns the created date of this purchase order.
    *
    * @return the created date of this purchase order
    */
    public java.util.Date getCreatedDate() {
        return _purchaseOrder.getCreatedDate();
    }

    /**
    * Sets the created date of this purchase order.
    *
    * @param createdDate the created date of this purchase order
    */
    public void setCreatedDate(java.util.Date createdDate) {
        _purchaseOrder.setCreatedDate(createdDate);
    }

    /**
    * Returns the created user ID of this purchase order.
    *
    * @return the created user ID of this purchase order
    */
    public long getCreatedUserId() {
        return _purchaseOrder.getCreatedUserId();
    }

    /**
    * Sets the created user ID of this purchase order.
    *
    * @param createdUserId the created user ID of this purchase order
    */
    public void setCreatedUserId(long createdUserId) {
        _purchaseOrder.setCreatedUserId(createdUserId);
    }

    /**
    * Returns the created user uuid of this purchase order.
    *
    * @return the created user uuid of this purchase order
    * @throws SystemException if a system exception occurred
    */
    public java.lang.String getCreatedUserUuid()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrder.getCreatedUserUuid();
    }

    /**
    * Sets the created user uuid of this purchase order.
    *
    * @param createdUserUuid the created user uuid of this purchase order
    */
    public void setCreatedUserUuid(java.lang.String createdUserUuid) {
        _purchaseOrder.setCreatedUserUuid(createdUserUuid);
    }

    /**
    * Returns the wbs code ID of this purchase order.
    *
    * @return the wbs code ID of this purchase order
    */
    public java.lang.String getWbsCodeId() {
        return _purchaseOrder.getWbsCodeId();
    }

    /**
    * Sets the wbs code ID of this purchase order.
    *
    * @param wbsCodeId the wbs code ID of this purchase order
    */
    public void setWbsCodeId(java.lang.String wbsCodeId) {
        _purchaseOrder.setWbsCodeId(wbsCodeId);
    }

    /**
    * Returns the tracking code of this purchase order.
    *
    * @return the tracking code of this purchase order
    */
    public java.lang.String getTrackingCode() {
        return _purchaseOrder.getTrackingCode();
    }

    /**
    * Sets the tracking code of this purchase order.
    *
    * @param trackingCode the tracking code of this purchase order
    */
    public void setTrackingCode(java.lang.String trackingCode) {
        _purchaseOrder.setTrackingCode(trackingCode);
    }

    /**
    * Returns the buyer ID of this purchase order.
    *
    * @return the buyer ID of this purchase order
    */
    public java.lang.String getBuyerId() {
        return _purchaseOrder.getBuyerId();
    }

    /**
    * Sets the buyer ID of this purchase order.
    *
    * @param buyerId the buyer ID of this purchase order
    */
    public void setBuyerId(java.lang.String buyerId) {
        _purchaseOrder.setBuyerId(buyerId);
    }

    public boolean isNew() {
        return _purchaseOrder.isNew();
    }

    public void setNew(boolean n) {
        _purchaseOrder.setNew(n);
    }

    public boolean isCachedModel() {
        return _purchaseOrder.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _purchaseOrder.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _purchaseOrder.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _purchaseOrder.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _purchaseOrder.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _purchaseOrder.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _purchaseOrder.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new PurchaseOrderWrapper((PurchaseOrder) _purchaseOrder.clone());
    }

    public int compareTo(PurchaseOrder purchaseOrder) {
        return _purchaseOrder.compareTo(purchaseOrder);
    }

    @Override
    public int hashCode() {
        return _purchaseOrder.hashCode();
    }

    public com.liferay.portal.model.CacheModel<PurchaseOrder> toCacheModel() {
        return _purchaseOrder.toCacheModel();
    }

    public PurchaseOrder toEscapedModel() {
        return new PurchaseOrderWrapper(_purchaseOrder.toEscapedModel());
    }

    public PurchaseOrder toUnescapedModel() {
        return new PurchaseOrderWrapper(_purchaseOrder.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _purchaseOrder.toString();
    }

    public java.lang.String toXmlString() {
        return _purchaseOrder.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _purchaseOrder.persist();
    }

    public com.hp.omp.model.custom.PurchaseOrderStatus getPurchaseOrderStatus() {
        return _purchaseOrder.getPurchaseOrderStatus();
    }

    public java.lang.String getTxtBoxStatus()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrder.getTxtBoxStatus();
    }

    public java.lang.String getFieldStatus()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrder.getFieldStatus();
    }

    public void setFieldStatus(java.lang.String fieldStatus)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        _purchaseOrder.setFieldStatus(fieldStatus);
    }

    public boolean getAllowUpdate()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrder.getAllowUpdate();
    }

    public boolean getAllowDelete()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrder.getAllowDelete();
    }

    public boolean getAllowAdd()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrder.getAllowAdd();
    }

    public java.lang.String getTrCodeFieldsStatus()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrder.getTrCodeFieldsStatus();
    }

    public void setTrCodeFieldsStatus(java.lang.String trCodeFieldsStatus) {
        _purchaseOrder.setTrCodeFieldsStatus(trCodeFieldsStatus);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PurchaseOrderWrapper)) {
            return false;
        }

        PurchaseOrderWrapper purchaseOrderWrapper = (PurchaseOrderWrapper) obj;

        if (Validator.equals(_purchaseOrder, purchaseOrderWrapper._purchaseOrder)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public PurchaseOrder getWrappedPurchaseOrder() {
        return _purchaseOrder;
    }

    public PurchaseOrder getWrappedModel() {
        return _purchaseOrder;
    }

    public void resetOriginalValues() {
        _purchaseOrder.resetOriginalValues();
    }
    
	@AutoEscape
	public String getTargaTecnica() {
		return _purchaseOrder.getTargaTecnica();
	}

	public void setTargaTecnica(String targaTecnica) {
		_purchaseOrder.setTargaTecnica(targaTecnica);
	}

	@AutoEscape
	public String getEvoLocationId() {
		return _purchaseOrder.getEvoLocationId();
	}

	public void setEvoLocationId(String evoLocationId) {
		_purchaseOrder.setEvoLocationId(evoLocationId);
	}

	public int getTipoPr() {
		return _purchaseOrder.getTipoPr();
	}

	public void setTipoPr(int tipoPr) {
		_purchaseOrder.setTipoPr(tipoPr);
	}

	@AutoEscape
	public String getPlant() {
		return _purchaseOrder.getPlant();
	}

	public void setPlant(String plant) {
		_purchaseOrder.setPlant(plant);
	}

	public boolean isHasFileGR() {
		return _purchaseOrder.isHasFileGR();
	}
}
