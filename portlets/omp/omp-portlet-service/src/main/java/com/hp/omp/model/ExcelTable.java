package com.hp.omp.model;

import java.util.List;

import com.hp.omp.model.custom.ExcelTableRow;

public interface ExcelTable {
	
	public List<Object> getHeader();
	
	public void setHeader(List<Object> header);
	
	public List<ExcelTableRow> getData();
	
	public void setData(List<ExcelTableRow> data);
	
	public String getTitle();
	
	public void setTitle(String title);
	
	public void setProcessedRows(int rowCount);
	
	public int getProcessedRows();
}
