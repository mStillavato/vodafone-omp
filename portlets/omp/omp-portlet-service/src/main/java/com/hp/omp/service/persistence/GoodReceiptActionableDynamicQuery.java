package com.hp.omp.service.persistence;

import com.hp.omp.model.GoodReceipt;
import com.hp.omp.service.GoodReceiptLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class GoodReceiptActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public GoodReceiptActionableDynamicQuery() throws SystemException {
        setBaseLocalService(GoodReceiptLocalServiceUtil.getService());
        setClass(GoodReceipt.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("goodReceiptId");
    }
}
