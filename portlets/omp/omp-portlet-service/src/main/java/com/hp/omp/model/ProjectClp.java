package com.hp.omp.model;

import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.ProjectLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class ProjectClp extends BaseModelImpl<Project> implements Project {
    private long _projectId;
    private String _projectName;
    private String _description;
    private boolean _display;
    private BaseModel<?> _projectRemoteModel;

    public ProjectClp() {
    }

    public Class<?> getModelClass() {
        return Project.class;
    }

    public String getModelClassName() {
        return Project.class.getName();
    }

    public long getPrimaryKey() {
        return _projectId;
    }

    public void setPrimaryKey(long primaryKey) {
        setProjectId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_projectId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("projectId", getProjectId());
        attributes.put("projectName", getProjectName());
        attributes.put("description", getDescription());
        attributes.put("display", getDisplay());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long projectId = (Long) attributes.get("projectId");

        if (projectId != null) {
            setProjectId(projectId);
        }

        String projectName = (String) attributes.get("projectName");

        if (projectName != null) {
            setProjectName(projectName);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
    }

    public long getProjectId() {
        return _projectId;
    }

    public void setProjectId(long projectId) {
        _projectId = projectId;

        if (_projectRemoteModel != null) {
            try {
                Class<?> clazz = _projectRemoteModel.getClass();

                Method method = clazz.getMethod("setProjectId", long.class);

                method.invoke(_projectRemoteModel, projectId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getProjectName() {
        return _projectName;
    }

    public void setProjectName(String projectName) {
        _projectName = projectName;

        if (_projectRemoteModel != null) {
            try {
                Class<?> clazz = _projectRemoteModel.getClass();

                Method method = clazz.getMethod("setProjectName", String.class);

                method.invoke(_projectRemoteModel, projectName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;

        if (_projectRemoteModel != null) {
            try {
                Class<?> clazz = _projectRemoteModel.getClass();

                Method method = clazz.getMethod("setDescription", String.class);

                method.invoke(_projectRemoteModel, description);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;

        if (_projectRemoteModel != null) {
            try {
                Class<?> clazz = _projectRemoteModel.getClass();

                Method method = clazz.getMethod("setDisplay", boolean.class);

                method.invoke(_projectRemoteModel, display);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getProjectRemoteModel() {
        return _projectRemoteModel;
    }

    public void setProjectRemoteModel(BaseModel<?> projectRemoteModel) {
        _projectRemoteModel = projectRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _projectRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_projectRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            ProjectLocalServiceUtil.addProject(this);
        } else {
            ProjectLocalServiceUtil.updateProject(this);
        }
    }

    @Override
    public Project toEscapedModel() {
        return (Project) ProxyUtil.newProxyInstance(Project.class.getClassLoader(),
            new Class[] { Project.class }, new AutoEscapeBeanHandler(this));
    }

    public Project toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        ProjectClp clone = new ProjectClp();

        clone.setProjectId(getProjectId());
        clone.setProjectName(getProjectName());
        clone.setDescription(getDescription());
        clone.setDisplay(getDisplay());

        return clone;
    }

    public int compareTo(Project project) {
        long primaryKey = project.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ProjectClp)) {
            return false;
        }

        ProjectClp project = (ProjectClp) obj;

        long primaryKey = project.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(9);

        sb.append("{projectId=");
        sb.append(getProjectId());
        sb.append(", projectName=");
        sb.append(getProjectName());
        sb.append(", description=");
        sb.append(getDescription());
        sb.append(", display=");
        sb.append(getDisplay());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(16);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.Project");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>projectId</column-name><column-value><![CDATA[");
        sb.append(getProjectId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>projectName</column-name><column-value><![CDATA[");
        sb.append(getProjectName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>description</column-name><column-value><![CDATA[");
        sb.append(getDescription());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>display</column-name><column-value><![CDATA[");
        sb.append(getDisplay());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
