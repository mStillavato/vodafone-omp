package com.hp.omp.service.persistence;

import com.hp.omp.model.Vendor;
import com.hp.omp.service.VendorLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class VendorActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public VendorActionableDynamicQuery() throws SystemException {
        setBaseLocalService(VendorLocalServiceUtil.getService());
        setClass(Vendor.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("vendorId");
    }
}
