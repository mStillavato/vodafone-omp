package com.hp.omp.service.persistence;

import com.hp.omp.model.MacroMicroDriver;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the macro micro driver service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see MacroMicroDriverPersistenceImpl
 * @see MacroMicroDriverUtil
 * @generated
 */
public interface MacroMicroDriverPersistence extends BasePersistence<MacroMicroDriver> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link MacroMicroDriverUtil} to access the macro micro driver persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the macro micro driver in the entity cache if it is enabled.
    *
    * @param macroMicroDriver the macro micro driver
    */
    public void cacheResult(com.hp.omp.model.MacroMicroDriver macroMicroDriver);

    /**
    * Caches the macro micro drivers in the entity cache if it is enabled.
    *
    * @param macroMicroDrivers the macro micro drivers
    */
    public void cacheResult(
        java.util.List<com.hp.omp.model.MacroMicroDriver> macroMicroDrivers);

    /**
    * Creates a new macro micro driver with the primary key. Does not add the macro micro driver to the database.
    *
    * @param macroMicroDriverId the primary key for the new macro micro driver
    * @return the new macro micro driver
    */
    public com.hp.omp.model.MacroMicroDriver create(long macroMicroDriverId);

    /**
    * Removes the macro micro driver with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param macroMicroDriverId the primary key of the macro micro driver
    * @return the macro micro driver that was removed
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a macro micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver remove(long macroMicroDriverId)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.MacroMicroDriver updateImpl(
        com.hp.omp.model.MacroMicroDriver macroMicroDriver, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the macro micro driver with the primary key or throws a {@link com.hp.omp.NoSuchMacroMicroDriverException} if it could not be found.
    *
    * @param macroMicroDriverId the primary key of the macro micro driver
    * @return the macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a macro micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver findByPrimaryKey(
        long macroMicroDriverId)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the macro micro driver with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param macroMicroDriverId the primary key of the macro micro driver
    * @return the macro micro driver, or <code>null</code> if a macro micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver fetchByPrimaryKey(
        long macroMicroDriverId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the macro micro drivers where macroDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @return the matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MacroMicroDriver> findByMacroDriverId(
        long macroDriverId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the macro micro drivers where macroDriverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param macroDriverId the macro driver ID
    * @param start the lower bound of the range of macro micro drivers
    * @param end the upper bound of the range of macro micro drivers (not inclusive)
    * @return the range of matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MacroMicroDriver> findByMacroDriverId(
        long macroDriverId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the macro micro drivers where macroDriverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param macroDriverId the macro driver ID
    * @param start the lower bound of the range of macro micro drivers
    * @param end the upper bound of the range of macro micro drivers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MacroMicroDriver> findByMacroDriverId(
        long macroDriverId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first macro micro driver in the ordered set where macroDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver findByMacroDriverId_First(
        long macroDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first macro micro driver in the ordered set where macroDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching macro micro driver, or <code>null</code> if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver fetchByMacroDriverId_First(
        long macroDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last macro micro driver in the ordered set where macroDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver findByMacroDriverId_Last(
        long macroDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last macro micro driver in the ordered set where macroDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching macro micro driver, or <code>null</code> if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver fetchByMacroDriverId_Last(
        long macroDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the macro micro drivers before and after the current macro micro driver in the ordered set where macroDriverId = &#63;.
    *
    * @param macroMicroDriverId the primary key of the current macro micro driver
    * @param macroDriverId the macro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a macro micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver[] findByMacroDriverId_PrevAndNext(
        long macroMicroDriverId, long macroDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the macro micro drivers where microDriverId = &#63;.
    *
    * @param microDriverId the micro driver ID
    * @return the matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MacroMicroDriver> findByMicroDriverId(
        long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the macro micro drivers where microDriverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param microDriverId the micro driver ID
    * @param start the lower bound of the range of macro micro drivers
    * @param end the upper bound of the range of macro micro drivers (not inclusive)
    * @return the range of matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MacroMicroDriver> findByMicroDriverId(
        long microDriverId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the macro micro drivers where microDriverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param microDriverId the micro driver ID
    * @param start the lower bound of the range of macro micro drivers
    * @param end the upper bound of the range of macro micro drivers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MacroMicroDriver> findByMicroDriverId(
        long microDriverId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first macro micro driver in the ordered set where microDriverId = &#63;.
    *
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver findByMicroDriverId_First(
        long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first macro micro driver in the ordered set where microDriverId = &#63;.
    *
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching macro micro driver, or <code>null</code> if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver fetchByMicroDriverId_First(
        long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last macro micro driver in the ordered set where microDriverId = &#63;.
    *
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver findByMicroDriverId_Last(
        long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last macro micro driver in the ordered set where microDriverId = &#63;.
    *
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching macro micro driver, or <code>null</code> if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver fetchByMicroDriverId_Last(
        long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the macro micro drivers before and after the current macro micro driver in the ordered set where microDriverId = &#63;.
    *
    * @param macroMicroDriverId the primary key of the current macro micro driver
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a macro micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver[] findByMicroDriverId_PrevAndNext(
        long macroMicroDriverId, long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the macro micro drivers where macroDriverId = &#63; and microDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @return the matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MacroMicroDriver> findByMacroDriverIdAndMicroDriverId(
        long macroDriverId, long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the macro micro drivers where macroDriverId = &#63; and microDriverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @param start the lower bound of the range of macro micro drivers
    * @param end the upper bound of the range of macro micro drivers (not inclusive)
    * @return the range of matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MacroMicroDriver> findByMacroDriverIdAndMicroDriverId(
        long macroDriverId, long microDriverId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the macro micro drivers where macroDriverId = &#63; and microDriverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @param start the lower bound of the range of macro micro drivers
    * @param end the upper bound of the range of macro micro drivers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MacroMicroDriver> findByMacroDriverIdAndMicroDriverId(
        long macroDriverId, long microDriverId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first macro micro driver in the ordered set where macroDriverId = &#63; and microDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver findByMacroDriverIdAndMicroDriverId_First(
        long macroDriverId, long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first macro micro driver in the ordered set where macroDriverId = &#63; and microDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching macro micro driver, or <code>null</code> if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver fetchByMacroDriverIdAndMicroDriverId_First(
        long macroDriverId, long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last macro micro driver in the ordered set where macroDriverId = &#63; and microDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver findByMacroDriverIdAndMicroDriverId_Last(
        long macroDriverId, long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last macro micro driver in the ordered set where macroDriverId = &#63; and microDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching macro micro driver, or <code>null</code> if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver fetchByMacroDriverIdAndMicroDriverId_Last(
        long macroDriverId, long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the macro micro drivers before and after the current macro micro driver in the ordered set where macroDriverId = &#63; and microDriverId = &#63;.
    *
    * @param macroMicroDriverId the primary key of the current macro micro driver
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a macro micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver[] findByMacroDriverIdAndMicroDriverId_PrevAndNext(
        long macroMicroDriverId, long macroDriverId, long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the macro micro drivers.
    *
    * @return the macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MacroMicroDriver> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the macro micro drivers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of macro micro drivers
    * @param end the upper bound of the range of macro micro drivers (not inclusive)
    * @return the range of macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MacroMicroDriver> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the macro micro drivers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of macro micro drivers
    * @param end the upper bound of the range of macro micro drivers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MacroMicroDriver> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the macro micro drivers where macroDriverId = &#63; from the database.
    *
    * @param macroDriverId the macro driver ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByMacroDriverId(long macroDriverId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the macro micro drivers where microDriverId = &#63; from the database.
    *
    * @param microDriverId the micro driver ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByMicroDriverId(long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the macro micro drivers where macroDriverId = &#63; and microDriverId = &#63; from the database.
    *
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByMacroDriverIdAndMicroDriverId(long macroDriverId,
        long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the macro micro drivers from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of macro micro drivers where macroDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @return the number of matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public int countByMacroDriverId(long macroDriverId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of macro micro drivers where microDriverId = &#63;.
    *
    * @param microDriverId the micro driver ID
    * @return the number of matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public int countByMicroDriverId(long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of macro micro drivers where macroDriverId = &#63; and microDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @return the number of matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public int countByMacroDriverIdAndMicroDriverId(long macroDriverId,
        long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of macro micro drivers.
    *
    * @return the number of macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
