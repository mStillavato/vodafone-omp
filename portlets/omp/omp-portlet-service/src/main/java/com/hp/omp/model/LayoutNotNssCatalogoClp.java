/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.model;

import com.hp.omp.service.LayoutNotNssCatalogoLocalServiceUtil;
import com.hp.omp.service.ClpSerializer;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author HP Egypt team
 */
public class LayoutNotNssCatalogoClp extends BaseModelImpl<LayoutNotNssCatalogo> implements LayoutNotNssCatalogo {
	public LayoutNotNssCatalogoClp() {
	}

	public Class<?> getModelClass() {
		return LayoutNotNssCatalogo.class;
	}

	public String getModelClassName() {
		return LayoutNotNssCatalogo.class.getName();
	}

	public long getPrimaryKey() {
		return _layoutNotNssCatalogoId;
	}

	public void setPrimaryKey(long primaryKey) {
		setLayoutNotNssCatalogoId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_layoutNotNssCatalogoId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("layoutNotNssCatalogoId", getLayoutNotNssCatalogoId());
		attributes.put("lineNumber", getLineNumber());
		attributes.put("materialId", getMaterialId());
		attributes.put("materialDesc", getMaterialDesc());
		attributes.put("quantity", getQuantity());
		attributes.put("netPrice", getNetPrice());
		attributes.put("currency", getCurrency());
		attributes.put("deliveryDate", getDeliveryDate());
		attributes.put("deliveryAddress", getDeliveryAddress());
		attributes.put("itemText", getItemText());
		attributes.put("locationEvo", getLocationEvo());
		attributes.put("targaTecnica", getTargaTecnica());
		attributes.put("user_", getUser_());
		attributes.put("vendorCode", getVendorCode());
		attributes.put("catalogoId", getCatalogoId());
		attributes.put("icApproval", getIcApproval());
        attributes.put("categoryCode", getCategoryCode());
        attributes.put("offerNumber", getOfferNumber());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long layoutNotNssCatalogoId = (Long)attributes.get("layoutNotNssCatalogoId");
		if (layoutNotNssCatalogoId != null) {
			setLayoutNotNssCatalogoId(layoutNotNssCatalogoId);
		}
		
		Long lineNumber = (Long)attributes.get("lineNumber");
		if (lineNumber != null) {
			setLineNumber(lineNumber);
		}

		String materialId = (String)attributes.get("materialId");
		if (materialId != null) {
			setMaterialId(materialId);
		}
		
		String materialDesc = (String)attributes.get("materialDesc");
		if (materialDesc != null) {
			setMaterialDesc(materialDesc);
		}
		
		String quantity = (String)attributes.get("quantity");
		if (quantity != null) {
			setQuantity(quantity);
		}
		
		String netPrice = (String)attributes.get("netPrice");
		if (netPrice != null) {
			setNetPrice(netPrice);
		}
		
		String currency = (String)attributes.get("currency");
		if (currency != null) {
			setCurrency(currency);
		}
		
		Date deliveryDate = (Date)attributes.get("deliveryDate");
		if (deliveryDate != null) {
			setDeliveryDate(deliveryDate);
		}

		String deliveryAddress = (String)attributes.get("deliveryAddress");
		if (deliveryAddress != null) {
			setDeliveryAddress(deliveryAddress);
		}

		String itemText = (String)attributes.get("itemText");
		if (itemText != null) {
			setItemText(itemText);
		}

		String locationEvo = (String)attributes.get("locationEvo");
		if (locationEvo != null) {
			setLocationEvo(locationEvo);
		}

		String targaTecnica = (String)attributes.get("targaTecnica");
		if (targaTecnica != null) {
			setTargaTecnica(targaTecnica);
		}

		String user_ = (String)attributes.get("user_");
		if (user_ != null) {
			setUser_(user_);
		}
		
		String vendorCode = (String)attributes.get("vendorCode");
		if (vendorCode != null) {
			setVendorCode(vendorCode);
		}
		
		Long catalogoId = (Long)attributes.get("catalogoId");
		if (catalogoId != null) {
			setCatalogoId(catalogoId);
		}
		
        String icApproval = (String) attributes.get("icApproval");
        if (icApproval != null) {
            setIcApproval(icApproval);
        }
        
        String categoryCode = (String) attributes.get("categoryCode");
        if (categoryCode != null) {
            setCategoryCode(categoryCode);
        }

        String offerNumber = (String) attributes.get("offerNumber");
        if (offerNumber != null) {
            setOfferNumber(offerNumber);
        }
	}

	public BaseModel<?> getLayoutNotNssCatalogoRemoteModel() {
		return _layoutNotNssCatalogoRemoteModel;
	}

	public void setLayoutNotNssCatalogoRemoteModel(BaseModel<?> LayoutNotNssCatalogoRemoteModel) {
		_layoutNotNssCatalogoRemoteModel = LayoutNotNssCatalogoRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _layoutNotNssCatalogoRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_layoutNotNssCatalogoRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			LayoutNotNssCatalogoLocalServiceUtil.addLayoutNotNssCatalogo(this);
		}
		else {
			LayoutNotNssCatalogoLocalServiceUtil.updateLayoutNotNssCatalogo(this);
		}
	}

	@Override
	public LayoutNotNssCatalogo toEscapedModel() {
		return (LayoutNotNssCatalogo)ProxyUtil.newProxyInstance(LayoutNotNssCatalogo.class.getClassLoader(),
			new Class[] { LayoutNotNssCatalogo.class }, new AutoEscapeBeanHandler(this));
	}

	public LayoutNotNssCatalogo toUnescapedModel() {
		return this;
	}

	@Override
	public Object clone() {
		LayoutNotNssCatalogoClp clone = new LayoutNotNssCatalogoClp();

		clone.setLayoutNotNssCatalogoId(getLayoutNotNssCatalogoId());
		clone.setLineNumber(getLineNumber());
		clone.setMaterialId(getMaterialId());
		clone.setMaterialDesc(getMaterialDesc());
		clone.setQuantity(getQuantity());
		clone.setNetPrice(getNetPrice());
		clone.setCurrency(getCurrency());
		clone.setDeliveryDate(getDeliveryDate());
		clone.setDeliveryAddress(getDeliveryAddress());
		clone.setItemText(getItemText());
		clone.setLocationEvo(getLocationEvo());
		clone.setTargaTecnica(getTargaTecnica());
		clone.setUser_(getUser_());
		clone.setVendorCode(getVendorCode());
		clone.setCatalogoId(getCatalogoId());
		clone.setIcApproval(getIcApproval());
        clone.setCategoryCode(getCategoryCode());
        clone.setOfferNumber(getOfferNumber());

		return clone;
	}

	public int compareTo(LayoutNotNssCatalogo layoutNotNssCatalogo) {
		long primaryKey = layoutNotNssCatalogo.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LayoutNotNssCatalogoClp)) {
			return false;
		}

		LayoutNotNssCatalogoClp layoutNotNssCatalogo = (LayoutNotNssCatalogoClp)obj;

		long primaryKey = layoutNotNssCatalogo.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(47);

		sb.append("{layoutNotNssCatalogoId=");
		sb.append(getLayoutNotNssCatalogoId());
		sb.append(", lineNumber=");
		sb.append(getLineNumber());
		sb.append(", materialId=");
		sb.append(getMaterialId());
		sb.append(", materialDesc=");
		sb.append(getMaterialDesc());
		sb.append(", quantity=");
		sb.append(getQuantity());
		sb.append(", netPrice=");
		sb.append(getNetPrice());
		sb.append(", currency=");
		sb.append(getCurrency());
		sb.append(", deliveryDate=");
		sb.append(getDeliveryDate());
		sb.append(", deliveryAddress=");
		sb.append(getDeliveryAddress());
		sb.append(", itemText=");
		sb.append(getItemText());
		sb.append(", locationEvo=");
		sb.append(getLocationEvo());
		sb.append(", targaTecnica=");
		sb.append(getTargaTecnica());
		sb.append(", user_=");
		sb.append(getUser_());
		sb.append(", vendorCode=");
		sb.append(getVendorCode());
		sb.append(", catalogoId=");
		sb.append(getCatalogoId());
        sb.append(", icApproval=");
        sb.append(getIcApproval());
        sb.append(", categoryCode=");
        sb.append(getCategoryCode());
        sb.append(", offerNumber=");
        sb.append(getOfferNumber());

		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(73);

		sb.append("<model><model-name>");
		sb.append("com.hp.omp.model.LayoutNotNssCatalogo");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>layoutNotNssCatalogoId</column-name><column-value><![CDATA[");
		sb.append(getLayoutNotNssCatalogoId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>lineNumber</column-name><column-value><![CDATA[");
		sb.append(getLineNumber());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>materialId</column-name><column-value><![CDATA[");
		sb.append(getMaterialId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>materialDesc</column-name><column-value><![CDATA[");
		sb.append(getMaterialDesc());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>quantity</column-name><column-value><![CDATA[");
		sb.append(getQuantity());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>netPrice</column-name><column-value><![CDATA[");
		sb.append(getNetPrice());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>currency</column-name><column-value><![CDATA[");
		sb.append(getCurrency());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>deliveryDate</column-name><column-value><![CDATA[");
		sb.append(getDeliveryDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>deliveryAddress</column-name><column-value><![CDATA[");
		sb.append(getDeliveryAddress());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>itemText</column-name><column-value><![CDATA[");
		sb.append(getItemText());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>locationEvo</column-name><column-value><![CDATA[");
		sb.append(getLocationEvo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>targaTecnica</column-name><column-value><![CDATA[");
		sb.append(getTargaTecnica());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>user_</column-name><column-value><![CDATA[");
		sb.append(getUser_());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>vendorCode</column-name><column-value><![CDATA[");
		sb.append(getVendorCode());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>catalogoId</column-name><column-value><![CDATA[");
		sb.append(getCatalogoId());
		sb.append("]]></column-value></column>");
        sb.append(
                "<column><column-name>icApproval</column-name><column-value><![CDATA[");
        sb.append(getIcApproval());
        sb.append("]]></column-value></column>");
        sb.append(
                "<column><column-name>categoryCode</column-name><column-value><![CDATA[");
        sb.append(getCategoryCode());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>offerNumber</column-name><column-value><![CDATA[");
        sb.append(getOfferNumber());
        sb.append("]]></column-value></column>");
				
		sb.append("</model>");

		return sb.toString();
	}

	private long _layoutNotNssCatalogoId;
	private long _lineNumber;
	private String _materialId;
	private String _materialDesc;
	private String _quantity;
	private String _netPrice;
	private String _currency;
	private Date _deliveryDate;
	private String _deliveryAddress;
	private String _itemText;
	private String _locationEvo;
	private String _targaTecnica;
	private String _user_;
	private String _vendorCode;
	private long _catalogoId;
	private String _icApproval;
    private String _categoryCode;
    private String _offerNumber;
	private BaseModel<?> _layoutNotNssCatalogoRemoteModel;
	
	public long getLayoutNotNssCatalogoId() {
		return _layoutNotNssCatalogoId;
	}

	public void setLayoutNotNssCatalogoId(long layoutNotNssCatalogoId) {
		_layoutNotNssCatalogoId = layoutNotNssCatalogoId;

		if (_layoutNotNssCatalogoRemoteModel != null) {
			try {
				Class<?> clazz = _layoutNotNssCatalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setLayoutNotNssCatalogoId", long.class);

				method.invoke(_layoutNotNssCatalogoRemoteModel, layoutNotNssCatalogoId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@AutoEscape
	public long getLineNumber() {
		return _lineNumber;
	}

	public void setLineNumber(long lineNumber) {
		_lineNumber = lineNumber;

		if (_layoutNotNssCatalogoRemoteModel != null) {
			try {
				Class<?> clazz = _layoutNotNssCatalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setLineNumber", long.class);

				method.invoke(_layoutNotNssCatalogoRemoteModel, lineNumber);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@AutoEscape
	public String getMaterialId() {
		return _materialId;
	}

	public void setMaterialId(String materialId) {
		_materialId = materialId;

		if (_layoutNotNssCatalogoRemoteModel != null) {
			try {
				Class<?> clazz = _layoutNotNssCatalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setMaterialId",
						String.class);

				method.invoke(_layoutNotNssCatalogoRemoteModel, materialId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@AutoEscape
	public String getMaterialDesc() {
		return _materialDesc;
	}

	public void setMaterialDesc(String materialDesc) {
		_materialDesc = materialDesc;

		if (_layoutNotNssCatalogoRemoteModel != null) {
			try {
				Class<?> clazz = _layoutNotNssCatalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setMaterialDesc",
						String.class);

				method.invoke(_layoutNotNssCatalogoRemoteModel, materialDesc);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@AutoEscape
	public String getQuantity() {
		return _quantity;
	}

	public void setQuantity(String quantity) {
		_quantity = quantity;

		if (_layoutNotNssCatalogoRemoteModel != null) {
			try {
				Class<?> clazz = _layoutNotNssCatalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setQuantity",
						String.class);

				method.invoke(_layoutNotNssCatalogoRemoteModel, quantity);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@AutoEscape
	public String getNetPrice() {
		return _netPrice;
	}

	public void setNetPrice(String netPrice) {
		_netPrice = netPrice;

		if (_layoutNotNssCatalogoRemoteModel != null) {
			try {
				Class<?> clazz = _layoutNotNssCatalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setNetPrice",
						String.class);

				method.invoke(_layoutNotNssCatalogoRemoteModel, netPrice);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@AutoEscape
	public String getCurrency() {
		return _currency;
	}

	public void setCurrency(String currency) {
		_currency = currency;

		if (_layoutNotNssCatalogoRemoteModel != null) {
			try {
				Class<?> clazz = _layoutNotNssCatalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setCurrency",
						String.class);

				method.invoke(_layoutNotNssCatalogoRemoteModel, currency);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@AutoEscape
	public Date getDeliveryDate() {
		return _deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		_deliveryDate = deliveryDate;

		if (_layoutNotNssCatalogoRemoteModel != null) {
			try {
				Class<?> clazz = _layoutNotNssCatalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setDeliveryDate", Date.class);

				method.invoke(_layoutNotNssCatalogoRemoteModel, deliveryDate);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@AutoEscape
	public String getDeliveryAddress() {
		return _deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		_deliveryAddress = deliveryAddress;

		if (_layoutNotNssCatalogoRemoteModel != null) {
			try {
				Class<?> clazz = _layoutNotNssCatalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setDeliveryAddress",
						String.class);

				method.invoke(_layoutNotNssCatalogoRemoteModel, deliveryAddress);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@AutoEscape
	public String getItemText() {
		return _itemText;
	}

	public void setItemText(String itemText) {
		_itemText = itemText;

		if (_layoutNotNssCatalogoRemoteModel != null) {
			try {
				Class<?> clazz = _layoutNotNssCatalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setItemText",
						String.class);

				method.invoke(_layoutNotNssCatalogoRemoteModel, itemText);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@AutoEscape
	public String getLocationEvo() {
		return _locationEvo;
	}

	public void setLocationEvo(String locationEvo) {
		_locationEvo = locationEvo;

		if (_layoutNotNssCatalogoRemoteModel != null) {
			try {
				Class<?> clazz = _layoutNotNssCatalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setLocationEvo",
						String.class);

				method.invoke(_layoutNotNssCatalogoRemoteModel, locationEvo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@AutoEscape
	public String getTargaTecnica() {
		return _targaTecnica;
	}

	public void setTargaTecnica(String targaTecnica) {
		_targaTecnica = targaTecnica;

		if (_layoutNotNssCatalogoRemoteModel != null) {
			try {
				Class<?> clazz = _layoutNotNssCatalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setTargaTecnica",
						String.class);

				method.invoke(_layoutNotNssCatalogoRemoteModel, targaTecnica);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@AutoEscape
	public String getUser_() {
		return _user_;
	}

	public void setUser_(String user_) {
		_user_ = user_;

		if (_layoutNotNssCatalogoRemoteModel != null) {
			try {
				Class<?> clazz = _layoutNotNssCatalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setUser_",
						String.class);

				method.invoke(_layoutNotNssCatalogoRemoteModel, user_);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}
	
	@AutoEscape
	public String getVendorCode() {
		return _vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		_vendorCode = vendorCode;

		if (_layoutNotNssCatalogoRemoteModel != null) {
			try {
				Class<?> clazz = _layoutNotNssCatalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setVendorCode",
						String.class);

				method.invoke(_layoutNotNssCatalogoRemoteModel, vendorCode);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}
	
	@AutoEscape
	public long getCatalogoId() {
		return _catalogoId;
	}

	public void setCatalogoId(long catalogoId) {
		_catalogoId = catalogoId;

		if (_layoutNotNssCatalogoRemoteModel != null) {
			try {
				Class<?> clazz = _layoutNotNssCatalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setCatalogoId", long.class);

				method.invoke(_layoutNotNssCatalogoRemoteModel, catalogoId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@AutoEscape
	public String getIcApproval() {
		return _icApproval;
	}

	public void setIcApproval(String icApproval) {
        _icApproval = icApproval;

        if (_layoutNotNssCatalogoRemoteModel != null) {
            try {
                Class<?> clazz = _layoutNotNssCatalogoRemoteModel.getClass();

                Method method = clazz.getMethod("setIcApproval", String.class);

                method.invoke(_layoutNotNssCatalogoRemoteModel, icApproval);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
	}

	@AutoEscape
	public String getCategoryCode() {
        return _categoryCode;
	}

	public void setCategoryCode(String categoryCode) {        
		_categoryCode = categoryCode;

		if (_layoutNotNssCatalogoRemoteModel != null) {
			try {
				Class<?> clazz = _layoutNotNssCatalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setCategoryCode", String.class);

				method.invoke(_layoutNotNssCatalogoRemoteModel, categoryCode);
			} catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	@AutoEscape
	public String getOfferNumber() {
        return _offerNumber;
	}

	public void setOfferNumber(String offerNumber) {
        _offerNumber = offerNumber;

        if (_layoutNotNssCatalogoRemoteModel != null) {
            try {
                Class<?> clazz = _layoutNotNssCatalogoRemoteModel.getClass();

                Method method = clazz.getMethod("setOfferNumber", String.class);

                method.invoke(_layoutNotNssCatalogoRemoteModel, offerNumber);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
	}
}