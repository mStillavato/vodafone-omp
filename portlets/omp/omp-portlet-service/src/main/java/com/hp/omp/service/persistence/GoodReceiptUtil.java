package com.hp.omp.service.persistence;

import com.hp.omp.model.GoodReceipt;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the good receipt service. This utility wraps {@link GoodReceiptPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see GoodReceiptPersistence
 * @see GoodReceiptPersistenceImpl
 * @generated
 */
public class GoodReceiptUtil {
    private static GoodReceiptPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(GoodReceipt goodReceipt) {
        getPersistence().clearCache(goodReceipt);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<GoodReceipt> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<GoodReceipt> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<GoodReceipt> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static GoodReceipt update(GoodReceipt goodReceipt, boolean merge)
        throws SystemException {
        return getPersistence().update(goodReceipt, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static GoodReceipt update(GoodReceipt goodReceipt, boolean merge,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(goodReceipt, merge, serviceContext);
    }

    /**
    * Caches the good receipt in the entity cache if it is enabled.
    *
    * @param goodReceipt the good receipt
    */
    public static void cacheResult(com.hp.omp.model.GoodReceipt goodReceipt) {
        getPersistence().cacheResult(goodReceipt);
    }

    /**
    * Caches the good receipts in the entity cache if it is enabled.
    *
    * @param goodReceipts the good receipts
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.GoodReceipt> goodReceipts) {
        getPersistence().cacheResult(goodReceipts);
    }

    /**
    * Creates a new good receipt with the primary key. Does not add the good receipt to the database.
    *
    * @param goodReceiptId the primary key for the new good receipt
    * @return the new good receipt
    */
    public static com.hp.omp.model.GoodReceipt create(long goodReceiptId) {
        return getPersistence().create(goodReceiptId);
    }

    /**
    * Removes the good receipt with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param goodReceiptId the primary key of the good receipt
    * @return the good receipt that was removed
    * @throws com.hp.omp.NoSuchGoodReceiptException if a good receipt with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.GoodReceipt remove(long goodReceiptId)
        throws com.hp.omp.NoSuchGoodReceiptException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(goodReceiptId);
    }

    public static com.hp.omp.model.GoodReceipt updateImpl(
        com.hp.omp.model.GoodReceipt goodReceipt, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(goodReceipt, merge);
    }

    /**
    * Returns the good receipt with the primary key or throws a {@link com.hp.omp.NoSuchGoodReceiptException} if it could not be found.
    *
    * @param goodReceiptId the primary key of the good receipt
    * @return the good receipt
    * @throws com.hp.omp.NoSuchGoodReceiptException if a good receipt with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.GoodReceipt findByPrimaryKey(
        long goodReceiptId)
        throws com.hp.omp.NoSuchGoodReceiptException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(goodReceiptId);
    }

    /**
    * Returns the good receipt with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param goodReceiptId the primary key of the good receipt
    * @return the good receipt, or <code>null</code> if a good receipt with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.GoodReceipt fetchByPrimaryKey(
        long goodReceiptId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(goodReceiptId);
    }

    /**
    * Returns all the good receipts where positionId = &#63;.
    *
    * @param positionId the position ID
    * @return the matching good receipts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.GoodReceipt> findByPositionGoodReceipts(
        java.lang.String positionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPositionGoodReceipts(positionId);
    }

    /**
    * Returns a range of all the good receipts where positionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param positionId the position ID
    * @param start the lower bound of the range of good receipts
    * @param end the upper bound of the range of good receipts (not inclusive)
    * @return the range of matching good receipts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.GoodReceipt> findByPositionGoodReceipts(
        java.lang.String positionId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPositionGoodReceipts(positionId, start, end);
    }

    /**
    * Returns an ordered range of all the good receipts where positionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param positionId the position ID
    * @param start the lower bound of the range of good receipts
    * @param end the upper bound of the range of good receipts (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching good receipts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.GoodReceipt> findByPositionGoodReceipts(
        java.lang.String positionId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPositionGoodReceipts(positionId, start, end,
            orderByComparator);
    }

    /**
    * Returns the first good receipt in the ordered set where positionId = &#63;.
    *
    * @param positionId the position ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching good receipt
    * @throws com.hp.omp.NoSuchGoodReceiptException if a matching good receipt could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.GoodReceipt findByPositionGoodReceipts_First(
        java.lang.String positionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchGoodReceiptException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPositionGoodReceipts_First(positionId,
            orderByComparator);
    }

    /**
    * Returns the first good receipt in the ordered set where positionId = &#63;.
    *
    * @param positionId the position ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching good receipt, or <code>null</code> if a matching good receipt could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.GoodReceipt fetchByPositionGoodReceipts_First(
        java.lang.String positionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByPositionGoodReceipts_First(positionId,
            orderByComparator);
    }

    /**
    * Returns the last good receipt in the ordered set where positionId = &#63;.
    *
    * @param positionId the position ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching good receipt
    * @throws com.hp.omp.NoSuchGoodReceiptException if a matching good receipt could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.GoodReceipt findByPositionGoodReceipts_Last(
        java.lang.String positionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchGoodReceiptException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPositionGoodReceipts_Last(positionId,
            orderByComparator);
    }

    /**
    * Returns the last good receipt in the ordered set where positionId = &#63;.
    *
    * @param positionId the position ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching good receipt, or <code>null</code> if a matching good receipt could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.GoodReceipt fetchByPositionGoodReceipts_Last(
        java.lang.String positionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByPositionGoodReceipts_Last(positionId,
            orderByComparator);
    }

    /**
    * Returns the good receipts before and after the current good receipt in the ordered set where positionId = &#63;.
    *
    * @param goodReceiptId the primary key of the current good receipt
    * @param positionId the position ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next good receipt
    * @throws com.hp.omp.NoSuchGoodReceiptException if a good receipt with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.GoodReceipt[] findByPositionGoodReceipts_PrevAndNext(
        long goodReceiptId, java.lang.String positionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchGoodReceiptException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPositionGoodReceipts_PrevAndNext(goodReceiptId,
            positionId, orderByComparator);
    }

    /**
    * Returns all the good receipts.
    *
    * @return the good receipts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.GoodReceipt> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the good receipts.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of good receipts
    * @param end the upper bound of the range of good receipts (not inclusive)
    * @return the range of good receipts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.GoodReceipt> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the good receipts.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of good receipts
    * @param end the upper bound of the range of good receipts (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of good receipts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.GoodReceipt> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the good receipts where positionId = &#63; from the database.
    *
    * @param positionId the position ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByPositionGoodReceipts(java.lang.String positionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByPositionGoodReceipts(positionId);
    }

    /**
    * Removes all the good receipts from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of good receipts where positionId = &#63;.
    *
    * @param positionId the position ID
    * @return the number of matching good receipts
    * @throws SystemException if a system exception occurred
    */
    public static int countByPositionGoodReceipts(java.lang.String positionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByPositionGoodReceipts(positionId);
    }

    /**
    * Returns the number of good receipts.
    *
    * @return the number of good receipts
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static GoodReceiptPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (GoodReceiptPersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    GoodReceiptPersistence.class.getName());

            ReferenceRegistry.registerReference(GoodReceiptUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(GoodReceiptPersistence persistence) {
    }
}
