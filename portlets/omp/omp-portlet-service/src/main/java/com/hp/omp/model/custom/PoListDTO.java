/**
 * 
 */
package com.hp.omp.model.custom;

import java.io.IOException;
import java.io.Serializable;
import java.util.Locale;

/**
 * @author Sami Mohamed
 *
 */
public class PoListDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7684541966359000646L;
	
	private long purchaseOrderId;
	private String screenNameRequester;
	private String screenNameReciever;
	private String vendorName;
	private String budgetSubCategoryName;
	private String totalValue;
	private String fiscalYear;
	private String activityDescription;
	private boolean automatic;
	private int status;
	private String currency;
	
	private String foramttedTotalValue;
	
	private int tipoPr;
	
	public int getTipoPr() {
		return tipoPr;
	}
	public void setTipoPr(int tipoPr) {
		this.tipoPr = tipoPr;
	}
	
	public void setForamttedTotalValue(String foramttedTotalValue) {
		this.foramttedTotalValue = foramttedTotalValue;
	}
	
	public String getForamttedTotalValue() {
		if(this.totalValue !=null && ! "".equals(this.totalValue) && ! "null".equals(this.totalValue)) {
		return OmpFormatterUtil.formatNumberByLocale(Double.valueOf(this.totalValue),Locale.ITALY);
		}else{
			return this.totalValue;
		}
	}
	
	public long getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(long purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	public String getScreenNameRequester() {
		return screenNameRequester;
	}
	public void setScreenNameRequester(String screenNameRequester) {
		this.screenNameRequester = screenNameRequester;
	}
	public String getScreenNameReciever() {
		return screenNameReciever;
	}
	public void setScreenNameReciever(String screenNameReciever) {
		this.screenNameReciever = screenNameReciever;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getBudgetSubCategoryName() {
		return budgetSubCategoryName;
	}
	public void setBudgetSubCategoryName(String budgetSubCategoryName) {
		this.budgetSubCategoryName = budgetSubCategoryName;
	}
	public String getTotalValue() {
		String totalValueCurrency = "";
		
		
		if(this.totalValue != null && !("").equals(this.totalValue)&& !("null").equals(this.totalValue)){
			String currency = "";
			if(this.currency.equals(Currency.USD.getCode()+""))
				currency = "$";
			else if (this.currency.equals(Currency.EUR.getCode()+""))
				currency = "€";
			
			totalValueCurrency = this.totalValue + " " + currency;
		}
		else{
			totalValueCurrency = "";
		}
		return totalValueCurrency;
	}
	public void setTotalValue(String totalValue) {
		this.totalValue = totalValue;
	}
	public String getFiscalYear() {
		return fiscalYear;
	}
	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}
	public String getActivityDescription() {
		return activityDescription;
	}
	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}
	public boolean isAutomatic() {
		return automatic;
	}
	public void setAutomatic(boolean automatic) {
		this.automatic = automatic;
	}
	public String getStatus() {
		if(this.status == PurchaseOrderStatus.GR_RECEIVED.getCode()){
			return "GR Requested";
		}else if(this.status == PurchaseOrderStatus.PO_CLOSED.getCode()){
			return "PO Closed";
		}else if(this.status == PurchaseOrderStatus.PO_DELETED.getCode()){
			return "PO Deleted";
		}else if(this.status == PurchaseOrderStatus.PO_RECEIVED.getCode()){
			return "PO Received";
		}else{
			return "-";
		}
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	
}
