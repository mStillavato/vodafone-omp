package com.hp.omp.model.custom;

public interface ExcelTableRow {
	Object getFieldAtIndex(int index);
	void setFieldAtIndex(Object field, int index);
	int getTotalNumberFields();
}
