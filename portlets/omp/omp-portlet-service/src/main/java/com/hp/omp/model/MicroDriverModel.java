package com.hp.omp.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

/**
 * The base model interface for the MicroDriver service. Represents a row in the &quot;MICRO_DRIVER&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link com.hp.omp.model.impl.MicroDriverModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link com.hp.omp.model.impl.MicroDriverImpl}.
 * </p>
 *
 * @author HP Egypt team
 * @see MicroDriver
 * @see com.hp.omp.model.impl.MicroDriverImpl
 * @see com.hp.omp.model.impl.MicroDriverModelImpl
 * @generated
 */
public interface MicroDriverModel extends BaseModel<MicroDriver> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. All methods that expect a micro driver model instance should use the {@link MicroDriver} interface instead.
     */

    /**
     * Returns the primary key of this micro driver.
     *
     * @return the primary key of this micro driver
     */
    public long getPrimaryKey();

    /**
     * Sets the primary key of this micro driver.
     *
     * @param primaryKey the primary key of this micro driver
     */
    public void setPrimaryKey(long primaryKey);

    /**
     * Returns the micro driver ID of this micro driver.
     *
     * @return the micro driver ID of this micro driver
     */
    public long getMicroDriverId();

    /**
     * Sets the micro driver ID of this micro driver.
     *
     * @param microDriverId the micro driver ID of this micro driver
     */
    public void setMicroDriverId(long microDriverId);

    /**
     * Returns the micro driver name of this micro driver.
     *
     * @return the micro driver name of this micro driver
     */
    @AutoEscape
    public String getMicroDriverName();

    /**
     * Sets the micro driver name of this micro driver.
     *
     * @param microDriverName the micro driver name of this micro driver
     */
    public void setMicroDriverName(String microDriverName);

    /**
     * Returns the display of this micro driver.
     *
     * @return the display of this micro driver
     */
    public boolean getDisplay();

    /**
     * Returns <code>true</code> if this micro driver is display.
     *
     * @return <code>true</code> if this micro driver is display; <code>false</code> otherwise
     */
    public boolean isDisplay();

    /**
     * Sets whether this micro driver is display.
     *
     * @param display the display of this micro driver
     */
    public void setDisplay(boolean display);

    public boolean isNew();

    public void setNew(boolean n);

    public boolean isCachedModel();

    public void setCachedModel(boolean cachedModel);

    public boolean isEscapedModel();

    public Serializable getPrimaryKeyObj();

    public void setPrimaryKeyObj(Serializable primaryKeyObj);

    public ExpandoBridge getExpandoBridge();

    public void setExpandoBridgeAttributes(ServiceContext serviceContext);

    public Object clone();

    public int compareTo(MicroDriver microDriver);

    public int hashCode();

    public CacheModel<MicroDriver> toCacheModel();

    public MicroDriver toEscapedModel();

    public MicroDriver toUnescapedModel();

    public String toString();

    public String toXmlString();
}
