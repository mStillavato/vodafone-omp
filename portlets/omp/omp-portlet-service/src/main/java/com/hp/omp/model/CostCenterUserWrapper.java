package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CostCenterUser}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       CostCenterUser
 * @generated
 */
public class CostCenterUserWrapper implements CostCenterUser,
    ModelWrapper<CostCenterUser> {
    private CostCenterUser _costCenterUser;

    public CostCenterUserWrapper(CostCenterUser costCenterUser) {
        _costCenterUser = costCenterUser;
    }

    public Class<?> getModelClass() {
        return CostCenterUser.class;
    }

    public String getModelClassName() {
        return CostCenterUser.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("costCenterUserId", getCostCenterUserId());
        attributes.put("costCenterId", getCostCenterId());
        attributes.put("userId", getUserId());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long costCenterUserId = (Long) attributes.get("costCenterUserId");

        if (costCenterUserId != null) {
            setCostCenterUserId(costCenterUserId);
        }

        Long costCenterId = (Long) attributes.get("costCenterId");

        if (costCenterId != null) {
            setCostCenterId(costCenterId);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }
    }

    /**
    * Returns the primary key of this cost center user.
    *
    * @return the primary key of this cost center user
    */
    public long getPrimaryKey() {
        return _costCenterUser.getPrimaryKey();
    }

    /**
    * Sets the primary key of this cost center user.
    *
    * @param primaryKey the primary key of this cost center user
    */
    public void setPrimaryKey(long primaryKey) {
        _costCenterUser.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the cost center user ID of this cost center user.
    *
    * @return the cost center user ID of this cost center user
    */
    public long getCostCenterUserId() {
        return _costCenterUser.getCostCenterUserId();
    }

    /**
    * Sets the cost center user ID of this cost center user.
    *
    * @param costCenterUserId the cost center user ID of this cost center user
    */
    public void setCostCenterUserId(long costCenterUserId) {
        _costCenterUser.setCostCenterUserId(costCenterUserId);
    }

    /**
    * Returns the cost center user uuid of this cost center user.
    *
    * @return the cost center user uuid of this cost center user
    * @throws SystemException if a system exception occurred
    */
    public java.lang.String getCostCenterUserUuid()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterUser.getCostCenterUserUuid();
    }

    /**
    * Sets the cost center user uuid of this cost center user.
    *
    * @param costCenterUserUuid the cost center user uuid of this cost center user
    */
    public void setCostCenterUserUuid(java.lang.String costCenterUserUuid) {
        _costCenterUser.setCostCenterUserUuid(costCenterUserUuid);
    }

    /**
    * Returns the cost center ID of this cost center user.
    *
    * @return the cost center ID of this cost center user
    */
    public long getCostCenterId() {
        return _costCenterUser.getCostCenterId();
    }

    /**
    * Sets the cost center ID of this cost center user.
    *
    * @param costCenterId the cost center ID of this cost center user
    */
    public void setCostCenterId(long costCenterId) {
        _costCenterUser.setCostCenterId(costCenterId);
    }

    /**
    * Returns the user ID of this cost center user.
    *
    * @return the user ID of this cost center user
    */
    public long getUserId() {
        return _costCenterUser.getUserId();
    }

    /**
    * Sets the user ID of this cost center user.
    *
    * @param userId the user ID of this cost center user
    */
    public void setUserId(long userId) {
        _costCenterUser.setUserId(userId);
    }

    /**
    * Returns the user uuid of this cost center user.
    *
    * @return the user uuid of this cost center user
    * @throws SystemException if a system exception occurred
    */
    public java.lang.String getUserUuid()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterUser.getUserUuid();
    }

    /**
    * Sets the user uuid of this cost center user.
    *
    * @param userUuid the user uuid of this cost center user
    */
    public void setUserUuid(java.lang.String userUuid) {
        _costCenterUser.setUserUuid(userUuid);
    }

    public boolean isNew() {
        return _costCenterUser.isNew();
    }

    public void setNew(boolean n) {
        _costCenterUser.setNew(n);
    }

    public boolean isCachedModel() {
        return _costCenterUser.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _costCenterUser.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _costCenterUser.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _costCenterUser.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _costCenterUser.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _costCenterUser.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _costCenterUser.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new CostCenterUserWrapper((CostCenterUser) _costCenterUser.clone());
    }

    public int compareTo(CostCenterUser costCenterUser) {
        return _costCenterUser.compareTo(costCenterUser);
    }

    @Override
    public int hashCode() {
        return _costCenterUser.hashCode();
    }

    public com.liferay.portal.model.CacheModel<CostCenterUser> toCacheModel() {
        return _costCenterUser.toCacheModel();
    }

    public CostCenterUser toEscapedModel() {
        return new CostCenterUserWrapper(_costCenterUser.toEscapedModel());
    }

    public CostCenterUser toUnescapedModel() {
        return new CostCenterUserWrapper(_costCenterUser.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _costCenterUser.toString();
    }

    public java.lang.String toXmlString() {
        return _costCenterUser.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _costCenterUser.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CostCenterUserWrapper)) {
            return false;
        }

        CostCenterUserWrapper costCenterUserWrapper = (CostCenterUserWrapper) obj;

        if (Validator.equals(_costCenterUser,
                    costCenterUserWrapper._costCenterUser)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public CostCenterUser getWrappedCostCenterUser() {
        return _costCenterUser;
    }

    public CostCenterUser getWrappedModel() {
        return _costCenterUser;
    }

    public void resetOriginalValues() {
        _costCenterUser.resetOriginalValues();
    }
}
