package com.hp.omp.service.persistence;

public interface GoodReceiptFinder {
    public java.lang.Long getOpenedGoodReceiptByCostCenterCount(
        long costCenterId) throws java.lang.Exception;

    public java.lang.Double getSumCompeleteGRForPO(long purchaseOrderId,
        boolean isClosed);

    public java.lang.Double getSumCompeleteGRForPR(long purchaseRequestId);

    public java.lang.Double getSumValuePositionForPO(long purchaseOrderId);

    public java.lang.Double getSumValuePositionForPR(long purchaseRequestId);

    public java.lang.Long getOpenedGoodReceiptCount()
        throws java.lang.Exception;

    public java.util.List<com.hp.omp.model.custom.ListDTO> getOpenedGoodReceiptByCostCenter(
        long costCenterId, int start, int end) throws java.lang.Exception;

    public java.util.List<com.hp.omp.model.custom.ListDTO> getOpenedGoodReceipt(
        int start, int end) throws java.lang.Exception;
}
