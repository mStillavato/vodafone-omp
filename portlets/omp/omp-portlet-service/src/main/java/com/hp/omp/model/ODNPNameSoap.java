package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class ODNPNameSoap implements Serializable {
    private long _odnpNameId;
    private String _odnpNameName;
    private boolean _display;

    public ODNPNameSoap() {
    }

    public static ODNPNameSoap toSoapModel(ODNPName model) {
        ODNPNameSoap soapModel = new ODNPNameSoap();

        soapModel.setOdnpNameId(model.getOdnpNameId());
        soapModel.setOdnpNameName(model.getOdnpNameName());
        soapModel.setDisplay(model.getDisplay());

        return soapModel;
    }

    public static ODNPNameSoap[] toSoapModels(ODNPName[] models) {
        ODNPNameSoap[] soapModels = new ODNPNameSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static ODNPNameSoap[][] toSoapModels(ODNPName[][] models) {
        ODNPNameSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new ODNPNameSoap[models.length][models[0].length];
        } else {
            soapModels = new ODNPNameSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static ODNPNameSoap[] toSoapModels(List<ODNPName> models) {
        List<ODNPNameSoap> soapModels = new ArrayList<ODNPNameSoap>(models.size());

        for (ODNPName model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new ODNPNameSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _odnpNameId;
    }

    public void setPrimaryKey(long pk) {
        setOdnpNameId(pk);
    }

    public long getOdnpNameId() {
        return _odnpNameId;
    }

    public void setOdnpNameId(long odnpNameId) {
        _odnpNameId = odnpNameId;
    }

    public String getOdnpNameName() {
        return _odnpNameName;
    }

    public void setOdnpNameName(String odnpNameName) {
        _odnpNameName = odnpNameName;
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;
    }
}
