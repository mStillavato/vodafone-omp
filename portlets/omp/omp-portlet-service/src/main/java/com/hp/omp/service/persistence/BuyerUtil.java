package com.hp.omp.service.persistence;

import com.hp.omp.model.Buyer;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the buyer service. This utility wraps {@link BuyerPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see BuyerPersistence
 * @see BuyerPersistenceImpl
 * @generated
 */
public class BuyerUtil {
    private static BuyerPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Buyer buyer) {
        getPersistence().clearCache(buyer);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Buyer> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Buyer> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Buyer> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static Buyer update(Buyer buyer, boolean merge)
        throws SystemException {
        return getPersistence().update(buyer, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static Buyer update(Buyer buyer, boolean merge,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(buyer, merge, serviceContext);
    }

    /**
    * Caches the buyer in the entity cache if it is enabled.
    *
    * @param buyer the buyer
    */
    public static void cacheResult(com.hp.omp.model.Buyer buyer) {
        getPersistence().cacheResult(buyer);
    }

    /**
    * Caches the buyers in the entity cache if it is enabled.
    *
    * @param buyers the buyers
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.Buyer> buyers) {
        getPersistence().cacheResult(buyers);
    }

    /**
    * Creates a new buyer with the primary key. Does not add the buyer to the database.
    *
    * @param buyerId the primary key for the new buyer
    * @return the new buyer
    */
    public static com.hp.omp.model.Buyer create(long buyerId) {
        return getPersistence().create(buyerId);
    }

    /**
    * Removes the buyer with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param buyerId the primary key of the buyer
    * @return the buyer that was removed
    * @throws com.hp.omp.NoSuchBuyerException if a buyer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Buyer remove(long buyerId)
        throws com.hp.omp.NoSuchBuyerException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(buyerId);
    }

    public static com.hp.omp.model.Buyer updateImpl(
        com.hp.omp.model.Buyer buyer, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(buyer, merge);
    }

    /**
    * Returns the buyer with the primary key or throws a {@link com.hp.omp.NoSuchBuyerException} if it could not be found.
    *
    * @param buyerId the primary key of the buyer
    * @return the buyer
    * @throws com.hp.omp.NoSuchBuyerException if a buyer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Buyer findByPrimaryKey(long buyerId)
        throws com.hp.omp.NoSuchBuyerException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(buyerId);
    }

    /**
    * Returns the buyer with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param buyerId the primary key of the buyer
    * @return the buyer, or <code>null</code> if a buyer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Buyer fetchByPrimaryKey(long buyerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(buyerId);
    }

    /**
    * Returns all the buyers.
    *
    * @return the buyers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.Buyer> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the buyers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of buyers
    * @param end the upper bound of the range of buyers (not inclusive)
    * @return the range of buyers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.Buyer> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the buyers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of buyers
    * @param end the upper bound of the range of buyers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of buyers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.Buyer> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the buyers from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of buyers.
    *
    * @return the number of buyers
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static BuyerPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (BuyerPersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    BuyerPersistence.class.getName());

            ReferenceRegistry.registerReference(BuyerUtil.class, "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(BuyerPersistence persistence) {
    }
}
