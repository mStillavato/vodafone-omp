package com.hp.omp.service.persistence;

import com.hp.omp.model.ProjectSubProject;
import com.hp.omp.service.ProjectSubProjectLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class ProjectSubProjectActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public ProjectSubProjectActionableDynamicQuery() throws SystemException {
        setBaseLocalService(ProjectSubProjectLocalServiceUtil.getService());
        setClass(ProjectSubProject.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("projectSubProjectId");
    }
}
