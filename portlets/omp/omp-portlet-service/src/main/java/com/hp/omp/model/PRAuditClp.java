package com.hp.omp.model;

import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.PRAuditLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class PRAuditClp extends BaseModelImpl<PRAudit> implements PRAudit {
    private long _auditId;
    private Long _purchaseRequestId;
    private int _status;
    private Long _userId;
    private Date _changeDate;
    private BaseModel<?> _prAuditRemoteModel;

    public PRAuditClp() {
    }

    public Class<?> getModelClass() {
        return PRAudit.class;
    }

    public String getModelClassName() {
        return PRAudit.class.getName();
    }

    public long getPrimaryKey() {
        return _auditId;
    }

    public void setPrimaryKey(long primaryKey) {
        setAuditId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_auditId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("auditId", getAuditId());
        attributes.put("purchaseRequestId", getPurchaseRequestId());
        attributes.put("status", getStatus());
        attributes.put("userId", getUserId());
        attributes.put("changeDate", getChangeDate());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long auditId = (Long) attributes.get("auditId");

        if (auditId != null) {
            setAuditId(auditId);
        }

        Long purchaseRequestId = (Long) attributes.get("purchaseRequestId");

        if (purchaseRequestId != null) {
            setPurchaseRequestId(purchaseRequestId);
        }

        Integer status = (Integer) attributes.get("status");

        if (status != null) {
            setStatus(status);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        Date changeDate = (Date) attributes.get("changeDate");

        if (changeDate != null) {
            setChangeDate(changeDate);
        }
    }

    public long getAuditId() {
        return _auditId;
    }

    public void setAuditId(long auditId) {
        _auditId = auditId;

        if (_prAuditRemoteModel != null) {
            try {
                Class<?> clazz = _prAuditRemoteModel.getClass();

                Method method = clazz.getMethod("setAuditId", long.class);

                method.invoke(_prAuditRemoteModel, auditId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public Long getPurchaseRequestId() {
        return _purchaseRequestId;
    }

    public void setPurchaseRequestId(Long purchaseRequestId) {
        _purchaseRequestId = purchaseRequestId;

        if (_prAuditRemoteModel != null) {
            try {
                Class<?> clazz = _prAuditRemoteModel.getClass();

                Method method = clazz.getMethod("setPurchaseRequestId",
                        Long.class);

                method.invoke(_prAuditRemoteModel, purchaseRequestId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public int getStatus() {
        return _status;
    }

    public void setStatus(int status) {
        _status = status;

        if (_prAuditRemoteModel != null) {
            try {
                Class<?> clazz = _prAuditRemoteModel.getClass();

                Method method = clazz.getMethod("setStatus", int.class);

                method.invoke(_prAuditRemoteModel, status);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public Long getUserId() {
        return _userId;
    }

    public void setUserId(Long userId) {
        _userId = userId;

        if (_prAuditRemoteModel != null) {
            try {
                Class<?> clazz = _prAuditRemoteModel.getClass();

                Method method = clazz.getMethod("setUserId", Long.class);

                method.invoke(_prAuditRemoteModel, userId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public Date getChangeDate() {
        return _changeDate;
    }

    public void setChangeDate(Date changeDate) {
        _changeDate = changeDate;

        if (_prAuditRemoteModel != null) {
            try {
                Class<?> clazz = _prAuditRemoteModel.getClass();

                Method method = clazz.getMethod("setChangeDate", Date.class);

                method.invoke(_prAuditRemoteModel, changeDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getPRAuditRemoteModel() {
        return _prAuditRemoteModel;
    }

    public void setPRAuditRemoteModel(BaseModel<?> prAuditRemoteModel) {
        _prAuditRemoteModel = prAuditRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _prAuditRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_prAuditRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            PRAuditLocalServiceUtil.addPRAudit(this);
        } else {
            PRAuditLocalServiceUtil.updatePRAudit(this);
        }
    }

    @Override
    public PRAudit toEscapedModel() {
        return (PRAudit) ProxyUtil.newProxyInstance(PRAudit.class.getClassLoader(),
            new Class[] { PRAudit.class }, new AutoEscapeBeanHandler(this));
    }

    public PRAudit toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        PRAuditClp clone = new PRAuditClp();

        clone.setAuditId(getAuditId());
        clone.setPurchaseRequestId(getPurchaseRequestId());
        clone.setStatus(getStatus());
        clone.setUserId(getUserId());
        clone.setChangeDate(getChangeDate());

        return clone;
    }

    public int compareTo(PRAudit prAudit) {
        long primaryKey = prAudit.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PRAuditClp)) {
            return false;
        }

        PRAuditClp prAudit = (PRAuditClp) obj;

        long primaryKey = prAudit.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{auditId=");
        sb.append(getAuditId());
        sb.append(", purchaseRequestId=");
        sb.append(getPurchaseRequestId());
        sb.append(", status=");
        sb.append(getStatus());
        sb.append(", userId=");
        sb.append(getUserId());
        sb.append(", changeDate=");
        sb.append(getChangeDate());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(19);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.PRAudit");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>auditId</column-name><column-value><![CDATA[");
        sb.append(getAuditId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>purchaseRequestId</column-name><column-value><![CDATA[");
        sb.append(getPurchaseRequestId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>status</column-name><column-value><![CDATA[");
        sb.append(getStatus());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>userId</column-name><column-value><![CDATA[");
        sb.append(getUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>changeDate</column-name><column-value><![CDATA[");
        sb.append(getChangeDate());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
