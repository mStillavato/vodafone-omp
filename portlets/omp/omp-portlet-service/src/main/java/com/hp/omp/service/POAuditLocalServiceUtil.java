package com.hp.omp.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the p o audit local service. This utility wraps {@link com.hp.omp.service.impl.POAuditLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see POAuditLocalService
 * @see com.hp.omp.service.base.POAuditLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.POAuditLocalServiceImpl
 * @generated
 */
public class POAuditLocalServiceUtil {
    private static POAuditLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.hp.omp.service.impl.POAuditLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the p o audit to the database. Also notifies the appropriate model listeners.
    *
    * @param poAudit the p o audit
    * @return the p o audit that was added
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.POAudit addPOAudit(
        com.hp.omp.model.POAudit poAudit)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addPOAudit(poAudit);
    }

    /**
    * Creates a new p o audit with the primary key. Does not add the p o audit to the database.
    *
    * @param auditId the primary key for the new p o audit
    * @return the new p o audit
    */
    public static com.hp.omp.model.POAudit createPOAudit(long auditId) {
        return getService().createPOAudit(auditId);
    }

    /**
    * Deletes the p o audit with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param auditId the primary key of the p o audit
    * @return the p o audit that was removed
    * @throws PortalException if a p o audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.POAudit deletePOAudit(long auditId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deletePOAudit(auditId);
    }

    /**
    * Deletes the p o audit from the database. Also notifies the appropriate model listeners.
    *
    * @param poAudit the p o audit
    * @return the p o audit that was removed
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.POAudit deletePOAudit(
        com.hp.omp.model.POAudit poAudit)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deletePOAudit(poAudit);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    public static com.hp.omp.model.POAudit fetchPOAudit(long auditId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchPOAudit(auditId);
    }

    /**
    * Returns the p o audit with the primary key.
    *
    * @param auditId the primary key of the p o audit
    * @return the p o audit
    * @throws PortalException if a p o audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.POAudit getPOAudit(long auditId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPOAudit(auditId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the p o audits.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of p o audits
    * @param end the upper bound of the range of p o audits (not inclusive)
    * @return the range of p o audits
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.POAudit> getPOAudits(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPOAudits(start, end);
    }

    /**
    * Returns the number of p o audits.
    *
    * @return the number of p o audits
    * @throws SystemException if a system exception occurred
    */
    public static int getPOAuditsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPOAuditsCount();
    }

    /**
    * Updates the p o audit in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param poAudit the p o audit
    * @return the p o audit that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.POAudit updatePOAudit(
        com.hp.omp.model.POAudit poAudit)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updatePOAudit(poAudit);
    }

    /**
    * Updates the p o audit in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param poAudit the p o audit
    * @param merge whether to merge the p o audit with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the p o audit that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.POAudit updatePOAudit(
        com.hp.omp.model.POAudit poAudit, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updatePOAudit(poAudit, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static void clearService() {
        _service = null;
    }

    public static POAuditLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    POAuditLocalService.class.getName());

            if (invokableLocalService instanceof POAuditLocalService) {
                _service = (POAuditLocalService) invokableLocalService;
            } else {
                _service = new POAuditLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(POAuditLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated
     */
    public void setService(POAuditLocalService service) {
    }
}
