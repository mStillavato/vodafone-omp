package com.hp.omp.service;

import java.util.List;

import com.hp.omp.model.BudgetCategory;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link BudgetCategoryLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       BudgetCategoryLocalService
 * @generated
 */
public class BudgetCategoryLocalServiceWrapper
    implements BudgetCategoryLocalService,
        ServiceWrapper<BudgetCategoryLocalService> {
    private BudgetCategoryLocalService _budgetCategoryLocalService;

    public BudgetCategoryLocalServiceWrapper(
        BudgetCategoryLocalService budgetCategoryLocalService) {
        _budgetCategoryLocalService = budgetCategoryLocalService;
    }

    /**
    * Adds the budget category to the database. Also notifies the appropriate model listeners.
    *
    * @param budgetCategory the budget category
    * @return the budget category that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.BudgetCategory addBudgetCategory(
        com.hp.omp.model.BudgetCategory budgetCategory)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetCategoryLocalService.addBudgetCategory(budgetCategory);
    }

    /**
    * Creates a new budget category with the primary key. Does not add the budget category to the database.
    *
    * @param budgetCategoryId the primary key for the new budget category
    * @return the new budget category
    */
    public com.hp.omp.model.BudgetCategory createBudgetCategory(
        long budgetCategoryId) {
        return _budgetCategoryLocalService.createBudgetCategory(budgetCategoryId);
    }

    /**
    * Deletes the budget category with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param budgetCategoryId the primary key of the budget category
    * @return the budget category that was removed
    * @throws PortalException if a budget category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.BudgetCategory deleteBudgetCategory(
        long budgetCategoryId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _budgetCategoryLocalService.deleteBudgetCategory(budgetCategoryId);
    }

    /**
    * Deletes the budget category from the database. Also notifies the appropriate model listeners.
    *
    * @param budgetCategory the budget category
    * @return the budget category that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.BudgetCategory deleteBudgetCategory(
        com.hp.omp.model.BudgetCategory budgetCategory)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetCategoryLocalService.deleteBudgetCategory(budgetCategory);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _budgetCategoryLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetCategoryLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetCategoryLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetCategoryLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetCategoryLocalService.dynamicQueryCount(dynamicQuery);
    }

    public com.hp.omp.model.BudgetCategory fetchBudgetCategory(
        long budgetCategoryId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetCategoryLocalService.fetchBudgetCategory(budgetCategoryId);
    }

    /**
    * Returns the budget category with the primary key.
    *
    * @param budgetCategoryId the primary key of the budget category
    * @return the budget category
    * @throws PortalException if a budget category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.BudgetCategory getBudgetCategory(
        long budgetCategoryId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _budgetCategoryLocalService.getBudgetCategory(budgetCategoryId);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _budgetCategoryLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the budget categories.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of budget categories
    * @param end the upper bound of the range of budget categories (not inclusive)
    * @return the range of budget categories
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.BudgetCategory> getBudgetCategories(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetCategoryLocalService.getBudgetCategories(start, end);
    }

    /**
    * Returns the number of budget categories.
    *
    * @return the number of budget categories
    * @throws SystemException if a system exception occurred
    */
    public int getBudgetCategoriesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetCategoryLocalService.getBudgetCategoriesCount();
    }

    /**
    * Updates the budget category in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param budgetCategory the budget category
    * @return the budget category that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.BudgetCategory updateBudgetCategory(
        com.hp.omp.model.BudgetCategory budgetCategory)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetCategoryLocalService.updateBudgetCategory(budgetCategory);
    }

    /**
    * Updates the budget category in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param budgetCategory the budget category
    * @param merge whether to merge the budget category with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the budget category that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.BudgetCategory updateBudgetCategory(
        com.hp.omp.model.BudgetCategory budgetCategory, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetCategoryLocalService.updateBudgetCategory(budgetCategory,
            merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _budgetCategoryLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _budgetCategoryLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _budgetCategoryLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    public com.hp.omp.model.BudgetCategory getBudgetCategoryByName(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetCategoryLocalService.getBudgetCategoryByName(name);
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public BudgetCategoryLocalService getWrappedBudgetCategoryLocalService() {
        return _budgetCategoryLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedBudgetCategoryLocalService(
        BudgetCategoryLocalService budgetCategoryLocalService) {
        _budgetCategoryLocalService = budgetCategoryLocalService;
    }

    public BudgetCategoryLocalService getWrappedService() {
        return _budgetCategoryLocalService;
    }

    public void setWrappedService(
        BudgetCategoryLocalService budgetCategoryLocalService) {
        _budgetCategoryLocalService = budgetCategoryLocalService;
    }

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<BudgetCategory> getBudgetCategoryList(Integer pageNumber, Integer pageSize, String searchFilter)
			throws SystemException {
        return _budgetCategoryLocalService.getBudgetCategoryList(pageNumber, pageSize, searchFilter);
	}
}
