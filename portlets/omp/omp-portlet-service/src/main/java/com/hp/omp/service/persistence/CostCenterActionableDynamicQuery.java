package com.hp.omp.service.persistence;

import com.hp.omp.model.CostCenter;
import com.hp.omp.service.CostCenterLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class CostCenterActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public CostCenterActionableDynamicQuery() throws SystemException {
        setBaseLocalService(CostCenterLocalServiceUtil.getService());
        setClass(CostCenter.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("costCenterId");
    }
}
