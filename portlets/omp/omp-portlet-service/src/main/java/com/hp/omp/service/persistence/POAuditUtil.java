package com.hp.omp.service.persistence;

import com.hp.omp.model.POAudit;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the p o audit service. This utility wraps {@link POAuditPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see POAuditPersistence
 * @see POAuditPersistenceImpl
 * @generated
 */
public class POAuditUtil {
    private static POAuditPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(POAudit poAudit) {
        getPersistence().clearCache(poAudit);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<POAudit> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<POAudit> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<POAudit> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static POAudit update(POAudit poAudit, boolean merge)
        throws SystemException {
        return getPersistence().update(poAudit, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static POAudit update(POAudit poAudit, boolean merge,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(poAudit, merge, serviceContext);
    }

    /**
    * Caches the p o audit in the entity cache if it is enabled.
    *
    * @param poAudit the p o audit
    */
    public static void cacheResult(com.hp.omp.model.POAudit poAudit) {
        getPersistence().cacheResult(poAudit);
    }

    /**
    * Caches the p o audits in the entity cache if it is enabled.
    *
    * @param poAudits the p o audits
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.POAudit> poAudits) {
        getPersistence().cacheResult(poAudits);
    }

    /**
    * Creates a new p o audit with the primary key. Does not add the p o audit to the database.
    *
    * @param auditId the primary key for the new p o audit
    * @return the new p o audit
    */
    public static com.hp.omp.model.POAudit create(long auditId) {
        return getPersistence().create(auditId);
    }

    /**
    * Removes the p o audit with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param auditId the primary key of the p o audit
    * @return the p o audit that was removed
    * @throws com.hp.omp.NoSuchPOAuditException if a p o audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.POAudit remove(long auditId)
        throws com.hp.omp.NoSuchPOAuditException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(auditId);
    }

    public static com.hp.omp.model.POAudit updateImpl(
        com.hp.omp.model.POAudit poAudit, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(poAudit, merge);
    }

    /**
    * Returns the p o audit with the primary key or throws a {@link com.hp.omp.NoSuchPOAuditException} if it could not be found.
    *
    * @param auditId the primary key of the p o audit
    * @return the p o audit
    * @throws com.hp.omp.NoSuchPOAuditException if a p o audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.POAudit findByPrimaryKey(long auditId)
        throws com.hp.omp.NoSuchPOAuditException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(auditId);
    }

    /**
    * Returns the p o audit with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param auditId the primary key of the p o audit
    * @return the p o audit, or <code>null</code> if a p o audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.POAudit fetchByPrimaryKey(long auditId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(auditId);
    }

    /**
    * Returns all the p o audits.
    *
    * @return the p o audits
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.POAudit> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the p o audits.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of p o audits
    * @param end the upper bound of the range of p o audits (not inclusive)
    * @return the range of p o audits
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.POAudit> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the p o audits.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of p o audits
    * @param end the upper bound of the range of p o audits (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of p o audits
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.POAudit> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the p o audits from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of p o audits.
    *
    * @return the number of p o audits
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static POAuditPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (POAuditPersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    POAuditPersistence.class.getName());

            ReferenceRegistry.registerReference(POAuditUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(POAuditPersistence persistence) {
    }
}
