/**
 * 
 */
package com.hp.omp.model.custom;

import java.io.IOException;
import java.io.Serializable;
import java.util.Locale;

/**
 * @author Sami Mohamed
 *
 */
public class CatalogoListDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5537214074130306263L;
	
	private long catalogoId;
	private long vendorId;
	private String fornitoreCode;
	private String fornitoreDesc;
	private String matCod;
	private String matCodFornFinale;
	private String matDesc;
	private String prezzo;
	private String formattedPrezzo;
	private String valuta;
	private String dataFineValidita;
	private String matCatLvl4;
	private String matCatLvl4Desc;
	
	public long getCatalogoId() {
		return catalogoId;
	}
	public void setCatalogoId(long catalogoId) {
		this.catalogoId = catalogoId;
	}
	public long getVendorId() {
		return vendorId;
	}
	public void setVendorId(long vendorId) {
		this.vendorId = vendorId;
	}
	public String getFornitoreCode() {
		return fornitoreCode;
	}
	public void setFornitoreCode(String fornitoreCode) {
		this.fornitoreCode = fornitoreCode;
	}
	public String getFornitoreDesc() {
		return fornitoreDesc;
	}
	public void setFornitoreDesc(String fornitoreDesc) {
		this.fornitoreDesc = fornitoreDesc;
	}
	public String getMatCod() {
		return matCod;
	}
	public void setMatCod(String matCod) {
		this.matCod = matCod;
	}
	public String getMatCodFornFinale() {
		return matCodFornFinale;
	}
	public void setMatCodFornFinale(String matCodFornFinale) {
		this.matCodFornFinale = matCodFornFinale;
	}
	public String getMatDesc() {
		return matDesc;
	}
	public void setMatDesc(String matDesc) {
		this.matDesc = matDesc;
	}
	public String getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(String prezzo) {
		this.prezzo = prezzo;
	}
	public String getFormattedPrezzo() {
		if(this.prezzo != null && ! "".equals(this.prezzo) && ! "null".equals(this.prezzo)) {
			return OmpFormatterUtil.formatNumberByLocale(Double.valueOf(this.prezzo),Locale.ITALY);
		}else{
			return this.prezzo;
		}
	}
	public void setFormattedPrezzo(String formattedPrezzo) {
		this.formattedPrezzo = formattedPrezzo;
	}
	public String getValuta() {
		return valuta;
	}
	public void setValuta(String valuta) {
		this.valuta = valuta;
	}
	public String getDataFineValidita() {
		return dataFineValidita;
	}
	public void setDataFineValidita(String dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}
	public String getMatCatLvl4() {
		return matCatLvl4;
	}
	public void setMatCatLvl4(String matCatLvl4) {
		this.matCatLvl4 = matCatLvl4;
	}
	public String getMatCatLvl4Desc() {
		return matCatLvl4Desc;
	}
	public void setMatCatLvl4Desc(String matCatLvl4Desc) {
		this.matCatLvl4Desc = matCatLvl4Desc;
	}
	
}
