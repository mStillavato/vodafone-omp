package com.hp.omp.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link PRAuditLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       PRAuditLocalService
 * @generated
 */
public class PRAuditLocalServiceWrapper implements PRAuditLocalService,
    ServiceWrapper<PRAuditLocalService> {
    private PRAuditLocalService _prAuditLocalService;

    public PRAuditLocalServiceWrapper(PRAuditLocalService prAuditLocalService) {
        _prAuditLocalService = prAuditLocalService;
    }

    /**
    * Adds the p r audit to the database. Also notifies the appropriate model listeners.
    *
    * @param prAudit the p r audit
    * @return the p r audit that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PRAudit addPRAudit(com.hp.omp.model.PRAudit prAudit)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _prAuditLocalService.addPRAudit(prAudit);
    }

    /**
    * Creates a new p r audit with the primary key. Does not add the p r audit to the database.
    *
    * @param auditId the primary key for the new p r audit
    * @return the new p r audit
    */
    public com.hp.omp.model.PRAudit createPRAudit(long auditId) {
        return _prAuditLocalService.createPRAudit(auditId);
    }

    /**
    * Deletes the p r audit with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param auditId the primary key of the p r audit
    * @return the p r audit that was removed
    * @throws PortalException if a p r audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PRAudit deletePRAudit(long auditId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _prAuditLocalService.deletePRAudit(auditId);
    }

    /**
    * Deletes the p r audit from the database. Also notifies the appropriate model listeners.
    *
    * @param prAudit the p r audit
    * @return the p r audit that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PRAudit deletePRAudit(
        com.hp.omp.model.PRAudit prAudit)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _prAuditLocalService.deletePRAudit(prAudit);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _prAuditLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _prAuditLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _prAuditLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _prAuditLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _prAuditLocalService.dynamicQueryCount(dynamicQuery);
    }

    public com.hp.omp.model.PRAudit fetchPRAudit(long auditId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _prAuditLocalService.fetchPRAudit(auditId);
    }

    /**
    * Returns the p r audit with the primary key.
    *
    * @param auditId the primary key of the p r audit
    * @return the p r audit
    * @throws PortalException if a p r audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PRAudit getPRAudit(long auditId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _prAuditLocalService.getPRAudit(auditId);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _prAuditLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the p r audits.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of p r audits
    * @param end the upper bound of the range of p r audits (not inclusive)
    * @return the range of p r audits
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.PRAudit> getPRAudits(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _prAuditLocalService.getPRAudits(start, end);
    }

    /**
    * Returns the number of p r audits.
    *
    * @return the number of p r audits
    * @throws SystemException if a system exception occurred
    */
    public int getPRAuditsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _prAuditLocalService.getPRAuditsCount();
    }

    /**
    * Updates the p r audit in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param prAudit the p r audit
    * @return the p r audit that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PRAudit updatePRAudit(
        com.hp.omp.model.PRAudit prAudit)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _prAuditLocalService.updatePRAudit(prAudit);
    }

    /**
    * Updates the p r audit in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param prAudit the p r audit
    * @param merge whether to merge the p r audit with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the p r audit that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PRAudit updatePRAudit(
        com.hp.omp.model.PRAudit prAudit, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _prAuditLocalService.updatePRAudit(prAudit, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _prAuditLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _prAuditLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _prAuditLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public PRAuditLocalService getWrappedPRAuditLocalService() {
        return _prAuditLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedPRAuditLocalService(
        PRAuditLocalService prAuditLocalService) {
        _prAuditLocalService = prAuditLocalService;
    }

    public PRAuditLocalService getWrappedService() {
        return _prAuditLocalService;
    }

    public void setWrappedService(PRAuditLocalService prAuditLocalService) {
        _prAuditLocalService = prAuditLocalService;
    }
}
