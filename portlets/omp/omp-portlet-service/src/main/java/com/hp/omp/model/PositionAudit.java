package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the PositionAudit service. Represents a row in the &quot;POSITION_AUDIT&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see PositionAuditModel
 * @see com.hp.omp.model.impl.PositionAuditImpl
 * @see com.hp.omp.model.impl.PositionAuditModelImpl
 * @generated
 */
public interface PositionAudit extends PositionAuditModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.PositionAuditImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
