package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link BudgetCategory}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       BudgetCategory
 * @generated
 */
public class BudgetCategoryWrapper implements BudgetCategory,
    ModelWrapper<BudgetCategory> {
    private BudgetCategory _budgetCategory;

    public BudgetCategoryWrapper(BudgetCategory budgetCategory) {
        _budgetCategory = budgetCategory;
    }

    public Class<?> getModelClass() {
        return BudgetCategory.class;
    }

    public String getModelClassName() {
        return BudgetCategory.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("budgetCategoryId", getBudgetCategoryId());
        attributes.put("budgetCategoryName", getBudgetCategoryName());
        attributes.put("display", getDisplay());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long budgetCategoryId = (Long) attributes.get("budgetCategoryId");

        if (budgetCategoryId != null) {
            setBudgetCategoryId(budgetCategoryId);
        }

        String budgetCategoryName = (String) attributes.get(
                "budgetCategoryName");

        if (budgetCategoryName != null) {
            setBudgetCategoryName(budgetCategoryName);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
    }

    /**
    * Returns the primary key of this budget category.
    *
    * @return the primary key of this budget category
    */
    public long getPrimaryKey() {
        return _budgetCategory.getPrimaryKey();
    }

    /**
    * Sets the primary key of this budget category.
    *
    * @param primaryKey the primary key of this budget category
    */
    public void setPrimaryKey(long primaryKey) {
        _budgetCategory.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the budget category ID of this budget category.
    *
    * @return the budget category ID of this budget category
    */
    public long getBudgetCategoryId() {
        return _budgetCategory.getBudgetCategoryId();
    }

    /**
    * Sets the budget category ID of this budget category.
    *
    * @param budgetCategoryId the budget category ID of this budget category
    */
    public void setBudgetCategoryId(long budgetCategoryId) {
        _budgetCategory.setBudgetCategoryId(budgetCategoryId);
    }

    /**
    * Returns the budget category name of this budget category.
    *
    * @return the budget category name of this budget category
    */
    public java.lang.String getBudgetCategoryName() {
        return _budgetCategory.getBudgetCategoryName();
    }

    /**
    * Sets the budget category name of this budget category.
    *
    * @param budgetCategoryName the budget category name of this budget category
    */
    public void setBudgetCategoryName(java.lang.String budgetCategoryName) {
        _budgetCategory.setBudgetCategoryName(budgetCategoryName);
    }

    /**
    * Returns the display of this budget category.
    *
    * @return the display of this budget category
    */
    public boolean getDisplay() {
        return _budgetCategory.getDisplay();
    }

    /**
    * Returns <code>true</code> if this budget category is display.
    *
    * @return <code>true</code> if this budget category is display; <code>false</code> otherwise
    */
    public boolean isDisplay() {
        return _budgetCategory.isDisplay();
    }

    /**
    * Sets whether this budget category is display.
    *
    * @param display the display of this budget category
    */
    public void setDisplay(boolean display) {
        _budgetCategory.setDisplay(display);
    }

    public boolean isNew() {
        return _budgetCategory.isNew();
    }

    public void setNew(boolean n) {
        _budgetCategory.setNew(n);
    }

    public boolean isCachedModel() {
        return _budgetCategory.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _budgetCategory.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _budgetCategory.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _budgetCategory.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _budgetCategory.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _budgetCategory.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _budgetCategory.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new BudgetCategoryWrapper((BudgetCategory) _budgetCategory.clone());
    }

    public int compareTo(BudgetCategory budgetCategory) {
        return _budgetCategory.compareTo(budgetCategory);
    }

    @Override
    public int hashCode() {
        return _budgetCategory.hashCode();
    }

    public com.liferay.portal.model.CacheModel<BudgetCategory> toCacheModel() {
        return _budgetCategory.toCacheModel();
    }

    public BudgetCategory toEscapedModel() {
        return new BudgetCategoryWrapper(_budgetCategory.toEscapedModel());
    }

    public BudgetCategory toUnescapedModel() {
        return new BudgetCategoryWrapper(_budgetCategory.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _budgetCategory.toString();
    }

    public java.lang.String toXmlString() {
        return _budgetCategory.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _budgetCategory.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof BudgetCategoryWrapper)) {
            return false;
        }

        BudgetCategoryWrapper budgetCategoryWrapper = (BudgetCategoryWrapper) obj;

        if (Validator.equals(_budgetCategory,
                    budgetCategoryWrapper._budgetCategory)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public BudgetCategory getWrappedBudgetCategory() {
        return _budgetCategory;
    }

    public BudgetCategory getWrappedModel() {
        return _budgetCategory;
    }

    public void resetOriginalValues() {
        _budgetCategory.resetOriginalValues();
    }
}
