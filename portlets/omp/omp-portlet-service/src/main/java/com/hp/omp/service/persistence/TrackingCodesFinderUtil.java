package com.hp.omp.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class TrackingCodesFinderUtil {
    private static TrackingCodesFinder _finder;

    public static java.util.List<com.hp.omp.model.TrackingCodes> getTrackingCodesOrderedAsc(
        int start, int end, String searchTCN)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getFinder().getTrackingCodesOrderedAsc(start, end, searchTCN);
    }
    
	public static int getTrackingCodesCountList(String searchFilter)
			throws com.liferay.portal.kernel.exception.SystemException {
		return getFinder().getTrackingCodesCountList(searchFilter);
}
    
    public static TrackingCodesFinder getFinder() {
        if (_finder == null) {
            _finder = (TrackingCodesFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    TrackingCodesFinder.class.getName());

            ReferenceRegistry.registerReference(TrackingCodesFinderUtil.class,
                "_finder");
        }

        return _finder;
    }

    public void setFinder(TrackingCodesFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(TrackingCodesFinderUtil.class,
            "_finder");
    }
}
