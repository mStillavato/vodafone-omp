package com.hp.omp.service.persistence;

import com.hp.omp.model.BudgetCategory;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the budget category service. This utility wraps {@link BudgetCategoryPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see BudgetCategoryPersistence
 * @see BudgetCategoryPersistenceImpl
 * @generated
 */
public class BudgetCategoryUtil {
    private static BudgetCategoryPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(BudgetCategory budgetCategory) {
        getPersistence().clearCache(budgetCategory);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<BudgetCategory> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<BudgetCategory> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<BudgetCategory> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static BudgetCategory update(BudgetCategory budgetCategory,
        boolean merge) throws SystemException {
        return getPersistence().update(budgetCategory, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static BudgetCategory update(BudgetCategory budgetCategory,
        boolean merge, ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(budgetCategory, merge, serviceContext);
    }

    /**
    * Caches the budget category in the entity cache if it is enabled.
    *
    * @param budgetCategory the budget category
    */
    public static void cacheResult(
        com.hp.omp.model.BudgetCategory budgetCategory) {
        getPersistence().cacheResult(budgetCategory);
    }

    /**
    * Caches the budget categories in the entity cache if it is enabled.
    *
    * @param budgetCategories the budget categories
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.BudgetCategory> budgetCategories) {
        getPersistence().cacheResult(budgetCategories);
    }

    /**
    * Creates a new budget category with the primary key. Does not add the budget category to the database.
    *
    * @param budgetCategoryId the primary key for the new budget category
    * @return the new budget category
    */
    public static com.hp.omp.model.BudgetCategory create(long budgetCategoryId) {
        return getPersistence().create(budgetCategoryId);
    }

    /**
    * Removes the budget category with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param budgetCategoryId the primary key of the budget category
    * @return the budget category that was removed
    * @throws com.hp.omp.NoSuchBudgetCategoryException if a budget category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.BudgetCategory remove(long budgetCategoryId)
        throws com.hp.omp.NoSuchBudgetCategoryException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(budgetCategoryId);
    }

    public static com.hp.omp.model.BudgetCategory updateImpl(
        com.hp.omp.model.BudgetCategory budgetCategory, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(budgetCategory, merge);
    }

    /**
    * Returns the budget category with the primary key or throws a {@link com.hp.omp.NoSuchBudgetCategoryException} if it could not be found.
    *
    * @param budgetCategoryId the primary key of the budget category
    * @return the budget category
    * @throws com.hp.omp.NoSuchBudgetCategoryException if a budget category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.BudgetCategory findByPrimaryKey(
        long budgetCategoryId)
        throws com.hp.omp.NoSuchBudgetCategoryException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(budgetCategoryId);
    }

    /**
    * Returns the budget category with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param budgetCategoryId the primary key of the budget category
    * @return the budget category, or <code>null</code> if a budget category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.BudgetCategory fetchByPrimaryKey(
        long budgetCategoryId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(budgetCategoryId);
    }

    /**
    * Returns all the budget categories.
    *
    * @return the budget categories
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.BudgetCategory> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the budget categories.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of budget categories
    * @param end the upper bound of the range of budget categories (not inclusive)
    * @return the range of budget categories
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.BudgetCategory> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the budget categories.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of budget categories
    * @param end the upper bound of the range of budget categories (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of budget categories
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.BudgetCategory> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the budget categories from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of budget categories.
    *
    * @return the number of budget categories
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static BudgetCategoryPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (BudgetCategoryPersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    BudgetCategoryPersistence.class.getName());

            ReferenceRegistry.registerReference(BudgetCategoryUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(BudgetCategoryPersistence persistence) {
    }
}
