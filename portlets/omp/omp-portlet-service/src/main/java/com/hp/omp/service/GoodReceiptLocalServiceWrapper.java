package com.hp.omp.service;

import com.hp.omp.model.GoodReceipt;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link GoodReceiptLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       GoodReceiptLocalService
 * @generated
 */
public class GoodReceiptLocalServiceWrapper implements GoodReceiptLocalService,
    ServiceWrapper<GoodReceiptLocalService> {
    private GoodReceiptLocalService _goodReceiptLocalService;

    public GoodReceiptLocalServiceWrapper(
        GoodReceiptLocalService goodReceiptLocalService) {
        _goodReceiptLocalService = goodReceiptLocalService;
    }

    /**
    * Adds the good receipt to the database. Also notifies the appropriate model listeners.
    *
    * @param goodReceipt the good receipt
    * @return the good receipt that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.GoodReceipt addGoodReceipt(
        com.hp.omp.model.GoodReceipt goodReceipt)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceiptLocalService.addGoodReceipt(goodReceipt);
    }

    /**
    * Creates a new good receipt with the primary key. Does not add the good receipt to the database.
    *
    * @param goodReceiptId the primary key for the new good receipt
    * @return the new good receipt
    */
    public com.hp.omp.model.GoodReceipt createGoodReceipt(long goodReceiptId) {
        return _goodReceiptLocalService.createGoodReceipt(goodReceiptId);
    }

    /**
    * Deletes the good receipt with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param goodReceiptId the primary key of the good receipt
    * @return the good receipt that was removed
    * @throws PortalException if a good receipt with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.GoodReceipt deleteGoodReceipt(long goodReceiptId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _goodReceiptLocalService.deleteGoodReceipt(goodReceiptId);
    }

    /**
    * Deletes the good receipt from the database. Also notifies the appropriate model listeners.
    *
    * @param goodReceipt the good receipt
    * @return the good receipt that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.GoodReceipt deleteGoodReceipt(
        com.hp.omp.model.GoodReceipt goodReceipt)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceiptLocalService.deleteGoodReceipt(goodReceipt);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _goodReceiptLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceiptLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceiptLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceiptLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceiptLocalService.dynamicQueryCount(dynamicQuery);
    }

    public com.hp.omp.model.GoodReceipt fetchGoodReceipt(long goodReceiptId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceiptLocalService.fetchGoodReceipt(goodReceiptId);
    }

    /**
    * Returns the good receipt with the primary key.
    *
    * @param goodReceiptId the primary key of the good receipt
    * @return the good receipt
    * @throws PortalException if a good receipt with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.GoodReceipt getGoodReceipt(long goodReceiptId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _goodReceiptLocalService.getGoodReceipt(goodReceiptId);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _goodReceiptLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the good receipts.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of good receipts
    * @param end the upper bound of the range of good receipts (not inclusive)
    * @return the range of good receipts
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.GoodReceipt> getGoodReceipts(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceiptLocalService.getGoodReceipts(start, end);
    }

    /**
    * Returns the number of good receipts.
    *
    * @return the number of good receipts
    * @throws SystemException if a system exception occurred
    */
    public int getGoodReceiptsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceiptLocalService.getGoodReceiptsCount();
    }

    /**
    * Updates the good receipt in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param goodReceipt the good receipt
    * @return the good receipt that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.GoodReceipt updateGoodReceipt(
        com.hp.omp.model.GoodReceipt goodReceipt)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceiptLocalService.updateGoodReceipt(goodReceipt);
    }

    /**
    * Updates the good receipt in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param goodReceipt the good receipt
    * @param merge whether to merge the good receipt with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the good receipt that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.GoodReceipt updateGoodReceipt(
        com.hp.omp.model.GoodReceipt goodReceipt, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceiptLocalService.updateGoodReceipt(goodReceipt, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _goodReceiptLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _goodReceiptLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _goodReceiptLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    public java.util.List<com.hp.omp.model.GoodReceipt> getPositionGoodReceipts(
        java.lang.String positionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceiptLocalService.getPositionGoodReceipts(positionId);
    }

    public java.util.List<java.util.Date> getPositionMaxRegisterDateGoodReceipts(
        java.lang.String positionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceiptLocalService.getPositionMaxRegisterDateGoodReceipts(positionId);
    }

    public java.lang.Double getSumGRPercentageToPosition(
        java.lang.String positionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceiptLocalService.getSumGRPercentageToPosition(positionId);
    }

    public java.lang.Long getOpenedGoodReceiptByCostCenterCount(
        long costCenterId) {
        return _goodReceiptLocalService.getOpenedGoodReceiptByCostCenterCount(costCenterId);
    }

    public java.lang.Long getOpenedGoodReceiptCount() {
        return _goodReceiptLocalService.getOpenedGoodReceiptCount();
    }

    public java.util.List<com.hp.omp.model.custom.ListDTO> getOpenedGoodReceiptByCostCenter(
        long costCenterId, int start, int end) {
        return _goodReceiptLocalService.getOpenedGoodReceiptByCostCenter(costCenterId,
            start, end);
    }

    public java.util.List<com.hp.omp.model.custom.ListDTO> getOpenedGoodReceipt(
        int start, int end) {
        return _goodReceiptLocalService.getOpenedGoodReceipt(start, end);
    }

    public java.lang.Long getPositionGRCount(java.lang.String positionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceiptLocalService.getPositionGRCount(positionId);
    }

    public java.lang.Long getPositionClosedGRCount(java.lang.String positionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceiptLocalService.getPositionClosedGRCount(positionId);
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public GoodReceiptLocalService getWrappedGoodReceiptLocalService() {
        return _goodReceiptLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedGoodReceiptLocalService(
        GoodReceiptLocalService goodReceiptLocalService) {
        _goodReceiptLocalService = goodReceiptLocalService;
    }

    public GoodReceiptLocalService getWrappedService() {
        return _goodReceiptLocalService;
    }

    public void setWrappedService(
        GoodReceiptLocalService goodReceiptLocalService) {
        _goodReceiptLocalService = goodReceiptLocalService;
    }

	public GoodReceipt updatePoStatus(GoodReceipt goodReceipt)
			throws SystemException {
        return _goodReceiptLocalService.updatePoStatus(goodReceipt);
	}

}
