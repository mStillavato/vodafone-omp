package com.hp.omp.model.custom;


public class PoCatalogNotNssDTO {
	private Integer lineNumber;
	private String materialId;
	private String materialDesc;
	private long quantity;
	private String netPrice;
	private String currency;
	private String deliveryDate;
	private String deliveryAddress;
	private String itemText;
	private String locationEvo;
	private String targaTecnica = null;
	
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getTargaTecnica() {
		return targaTecnica;
	}
	public void setTargaTecnica(String targaTecnica) {
		this.targaTecnica = targaTecnica;
	}
	public Integer getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(Integer lineNumber) {
		this.lineNumber = lineNumber;
	}
	public String getMaterialId() {
		return materialId;
	}
	public void setMaterialId(String materialId) {
		this.materialId = materialId;
	}
	public String getMaterialDesc() {
		return materialDesc;
	}
	public void setMaterialDesc(String materialDesc) {
		this.materialDesc = materialDesc;
	}
	public long getQuantity() {
		return quantity;
	}
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}
	public String getNetPrice() {
		return netPrice;
	}
	public void setNetPrice(String netPrice) {
		this.netPrice = netPrice;
	}
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public String getItemText() {
		return itemText;
	}
	public void setItemText(String itemText) {
		this.itemText = itemText;
	}
	public String getLocationEvo() {
		return locationEvo;
	}
	public void setLocationEvo(String locationEvo) {
		this.locationEvo = locationEvo;
	}
	
}
