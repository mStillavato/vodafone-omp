package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link MacroMicroDriver}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       MacroMicroDriver
 * @generated
 */
public class MacroMicroDriverWrapper implements MacroMicroDriver,
    ModelWrapper<MacroMicroDriver> {
    private MacroMicroDriver _macroMicroDriver;

    public MacroMicroDriverWrapper(MacroMicroDriver macroMicroDriver) {
        _macroMicroDriver = macroMicroDriver;
    }

    public Class<?> getModelClass() {
        return MacroMicroDriver.class;
    }

    public String getModelClassName() {
        return MacroMicroDriver.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("macroMicroDriverId", getMacroMicroDriverId());
        attributes.put("macroDriverId", getMacroDriverId());
        attributes.put("microDriverId", getMicroDriverId());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long macroMicroDriverId = (Long) attributes.get("macroMicroDriverId");

        if (macroMicroDriverId != null) {
            setMacroMicroDriverId(macroMicroDriverId);
        }

        Long macroDriverId = (Long) attributes.get("macroDriverId");

        if (macroDriverId != null) {
            setMacroDriverId(macroDriverId);
        }

        Long microDriverId = (Long) attributes.get("microDriverId");

        if (microDriverId != null) {
            setMicroDriverId(microDriverId);
        }
    }

    /**
    * Returns the primary key of this macro micro driver.
    *
    * @return the primary key of this macro micro driver
    */
    public long getPrimaryKey() {
        return _macroMicroDriver.getPrimaryKey();
    }

    /**
    * Sets the primary key of this macro micro driver.
    *
    * @param primaryKey the primary key of this macro micro driver
    */
    public void setPrimaryKey(long primaryKey) {
        _macroMicroDriver.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the macro micro driver ID of this macro micro driver.
    *
    * @return the macro micro driver ID of this macro micro driver
    */
    public long getMacroMicroDriverId() {
        return _macroMicroDriver.getMacroMicroDriverId();
    }

    /**
    * Sets the macro micro driver ID of this macro micro driver.
    *
    * @param macroMicroDriverId the macro micro driver ID of this macro micro driver
    */
    public void setMacroMicroDriverId(long macroMicroDriverId) {
        _macroMicroDriver.setMacroMicroDriverId(macroMicroDriverId);
    }

    /**
    * Returns the macro driver ID of this macro micro driver.
    *
    * @return the macro driver ID of this macro micro driver
    */
    public long getMacroDriverId() {
        return _macroMicroDriver.getMacroDriverId();
    }

    /**
    * Sets the macro driver ID of this macro micro driver.
    *
    * @param macroDriverId the macro driver ID of this macro micro driver
    */
    public void setMacroDriverId(long macroDriverId) {
        _macroMicroDriver.setMacroDriverId(macroDriverId);
    }

    /**
    * Returns the micro driver ID of this macro micro driver.
    *
    * @return the micro driver ID of this macro micro driver
    */
    public long getMicroDriverId() {
        return _macroMicroDriver.getMicroDriverId();
    }

    /**
    * Sets the micro driver ID of this macro micro driver.
    *
    * @param microDriverId the micro driver ID of this macro micro driver
    */
    public void setMicroDriverId(long microDriverId) {
        _macroMicroDriver.setMicroDriverId(microDriverId);
    }

    public boolean isNew() {
        return _macroMicroDriver.isNew();
    }

    public void setNew(boolean n) {
        _macroMicroDriver.setNew(n);
    }

    public boolean isCachedModel() {
        return _macroMicroDriver.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _macroMicroDriver.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _macroMicroDriver.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _macroMicroDriver.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _macroMicroDriver.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _macroMicroDriver.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _macroMicroDriver.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new MacroMicroDriverWrapper((MacroMicroDriver) _macroMicroDriver.clone());
    }

    public int compareTo(MacroMicroDriver macroMicroDriver) {
        return _macroMicroDriver.compareTo(macroMicroDriver);
    }

    @Override
    public int hashCode() {
        return _macroMicroDriver.hashCode();
    }

    public com.liferay.portal.model.CacheModel<MacroMicroDriver> toCacheModel() {
        return _macroMicroDriver.toCacheModel();
    }

    public MacroMicroDriver toEscapedModel() {
        return new MacroMicroDriverWrapper(_macroMicroDriver.toEscapedModel());
    }

    public MacroMicroDriver toUnescapedModel() {
        return new MacroMicroDriverWrapper(_macroMicroDriver.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _macroMicroDriver.toString();
    }

    public java.lang.String toXmlString() {
        return _macroMicroDriver.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _macroMicroDriver.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof MacroMicroDriverWrapper)) {
            return false;
        }

        MacroMicroDriverWrapper macroMicroDriverWrapper = (MacroMicroDriverWrapper) obj;

        if (Validator.equals(_macroMicroDriver,
                    macroMicroDriverWrapper._macroMicroDriver)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public MacroMicroDriver getWrappedMacroMicroDriver() {
        return _macroMicroDriver;
    }

    public MacroMicroDriver getWrappedModel() {
        return _macroMicroDriver;
    }

    public void resetOriginalValues() {
        _macroMicroDriver.resetOriginalValues();
    }
}
