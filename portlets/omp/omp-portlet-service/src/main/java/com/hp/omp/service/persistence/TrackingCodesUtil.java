package com.hp.omp.service.persistence;

import com.hp.omp.model.TrackingCodes;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the tracking codes service. This utility wraps {@link TrackingCodesPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see TrackingCodesPersistence
 * @see TrackingCodesPersistenceImpl
 * @generated
 */
public class TrackingCodesUtil {
    private static TrackingCodesPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(TrackingCodes trackingCodes) {
        getPersistence().clearCache(trackingCodes);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<TrackingCodes> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<TrackingCodes> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<TrackingCodes> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static TrackingCodes update(TrackingCodes trackingCodes,
        boolean merge) throws SystemException {
        return getPersistence().update(trackingCodes, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static TrackingCodes update(TrackingCodes trackingCodes,
        boolean merge, ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(trackingCodes, merge, serviceContext);
    }

    /**
    * Caches the tracking codes in the entity cache if it is enabled.
    *
    * @param trackingCodes the tracking codes
    */
    public static void cacheResult(com.hp.omp.model.TrackingCodes trackingCodes) {
        getPersistence().cacheResult(trackingCodes);
    }

    /**
    * Caches the tracking codeses in the entity cache if it is enabled.
    *
    * @param trackingCodeses the tracking codeses
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.TrackingCodes> trackingCodeses) {
        getPersistence().cacheResult(trackingCodeses);
    }

    /**
    * Creates a new tracking codes with the primary key. Does not add the tracking codes to the database.
    *
    * @param trackingCodeId the primary key for the new tracking codes
    * @return the new tracking codes
    */
    public static com.hp.omp.model.TrackingCodes create(long trackingCodeId) {
        return getPersistence().create(trackingCodeId);
    }

    /**
    * Removes the tracking codes with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param trackingCodeId the primary key of the tracking codes
    * @return the tracking codes that was removed
    * @throws com.hp.omp.NoSuchTrackingCodesException if a tracking codes with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.TrackingCodes remove(long trackingCodeId)
        throws com.hp.omp.NoSuchTrackingCodesException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(trackingCodeId);
    }

    public static com.hp.omp.model.TrackingCodes updateImpl(
        com.hp.omp.model.TrackingCodes trackingCodes, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(trackingCodes, merge);
    }

    /**
    * Returns the tracking codes with the primary key or throws a {@link com.hp.omp.NoSuchTrackingCodesException} if it could not be found.
    *
    * @param trackingCodeId the primary key of the tracking codes
    * @return the tracking codes
    * @throws com.hp.omp.NoSuchTrackingCodesException if a tracking codes with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.TrackingCodes findByPrimaryKey(
        long trackingCodeId)
        throws com.hp.omp.NoSuchTrackingCodesException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(trackingCodeId);
    }

    /**
    * Returns the tracking codes with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param trackingCodeId the primary key of the tracking codes
    * @return the tracking codes, or <code>null</code> if a tracking codes with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.TrackingCodes fetchByPrimaryKey(
        long trackingCodeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(trackingCodeId);
    }

    /**
    * Returns all the tracking codeses.
    *
    * @return the tracking codeses
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.TrackingCodes> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the tracking codeses.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of tracking codeses
    * @param end the upper bound of the range of tracking codeses (not inclusive)
    * @return the range of tracking codeses
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.TrackingCodes> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the tracking codeses.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of tracking codeses
    * @param end the upper bound of the range of tracking codeses (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of tracking codeses
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.TrackingCodes> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the tracking codeses from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of tracking codeses.
    *
    * @return the number of tracking codeses
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static TrackingCodesPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (TrackingCodesPersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    TrackingCodesPersistence.class.getName());

            ReferenceRegistry.registerReference(TrackingCodesUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(TrackingCodesPersistence persistence) {
    }
}
