package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Vendor service. Represents a row in the &quot;VENDOR&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see VendorModel
 * @see com.hp.omp.model.impl.VendorImpl
 * @see com.hp.omp.model.impl.VendorModelImpl
 * @generated
 */
public interface Vendor extends VendorModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.VendorImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
