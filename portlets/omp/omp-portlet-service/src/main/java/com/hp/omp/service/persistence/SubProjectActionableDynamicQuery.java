package com.hp.omp.service.persistence;

import com.hp.omp.model.SubProject;
import com.hp.omp.service.SubProjectLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class SubProjectActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public SubProjectActionableDynamicQuery() throws SystemException {
        setBaseLocalService(SubProjectLocalServiceUtil.getService());
        setClass(SubProject.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("subProjectId");
    }
}
