package com.hp.omp.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link WbsCode}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       WbsCode
 * @generated
 */
public class WbsCodeWrapper implements WbsCode, ModelWrapper<WbsCode> {
    private WbsCode _wbsCode;

    public WbsCodeWrapper(WbsCode wbsCode) {
        _wbsCode = wbsCode;
    }

    public Class<?> getModelClass() {
        return WbsCode.class;
    }

    public String getModelClassName() {
        return WbsCode.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("wbsCodeId", getWbsCodeId());
        attributes.put("wbsCodeName", getWbsCodeName());
        attributes.put("description", getDescription());
        attributes.put("display", getDisplay());
        attributes.put("wbsOwnerName", getWbsOwnerName());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long wbsCodeId = (Long) attributes.get("wbsCodeId");

        if (wbsCodeId != null) {
            setWbsCodeId(wbsCodeId);
        }

        String wbsCodeName = (String) attributes.get("wbsCodeName");

        if (wbsCodeName != null) {
            setWbsCodeName(wbsCodeName);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
        
        String wbsOwnerName = (String) attributes.get("wbsOwnerName");

        if (wbsOwnerName != null) {
            setWbsOwnerName(wbsOwnerName);
        }
    }

    /**
    * Returns the primary key of this wbs code.
    *
    * @return the primary key of this wbs code
    */
    public long getPrimaryKey() {
        return _wbsCode.getPrimaryKey();
    }

    /**
    * Sets the primary key of this wbs code.
    *
    * @param primaryKey the primary key of this wbs code
    */
    public void setPrimaryKey(long primaryKey) {
        _wbsCode.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the wbs code ID of this wbs code.
    *
    * @return the wbs code ID of this wbs code
    */
    public long getWbsCodeId() {
        return _wbsCode.getWbsCodeId();
    }

    /**
    * Sets the wbs code ID of this wbs code.
    *
    * @param wbsCodeId the wbs code ID of this wbs code
    */
    public void setWbsCodeId(long wbsCodeId) {
        _wbsCode.setWbsCodeId(wbsCodeId);
    }

    /**
    * Returns the wbs code name of this wbs code.
    *
    * @return the wbs code name of this wbs code
    */
    public java.lang.String getWbsCodeName() {
        return _wbsCode.getWbsCodeName();
    }

    /**
    * Sets the wbs code name of this wbs code.
    *
    * @param wbsCodeName the wbs code name of this wbs code
    */
    public void setWbsCodeName(java.lang.String wbsCodeName) {
        _wbsCode.setWbsCodeName(wbsCodeName);
    }

    /**
    * Returns the description of this wbs code.
    *
    * @return the description of this wbs code
    */
    public java.lang.String getDescription() {
        return _wbsCode.getDescription();
    }

    /**
    * Sets the description of this wbs code.
    *
    * @param description the description of this wbs code
    */
    public void setDescription(java.lang.String description) {
        _wbsCode.setDescription(description);
    }

    /**
    * Returns the display of this wbs code.
    *
    * @return the display of this wbs code
    */
    public boolean getDisplay() {
        return _wbsCode.getDisplay();
    }

    /**
    * Returns <code>true</code> if this wbs code is display.
    *
    * @return <code>true</code> if this wbs code is display; <code>false</code> otherwise
    */
    public boolean isDisplay() {
        return _wbsCode.isDisplay();
    }

    /**
    * Sets whether this wbs code is display.
    *
    * @param display the display of this wbs code
    */
    public void setDisplay(boolean display) {
        _wbsCode.setDisplay(display);
    }

    public boolean isNew() {
        return _wbsCode.isNew();
    }

    public void setNew(boolean n) {
        _wbsCode.setNew(n);
    }

    public boolean isCachedModel() {
        return _wbsCode.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _wbsCode.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _wbsCode.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _wbsCode.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _wbsCode.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _wbsCode.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _wbsCode.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new WbsCodeWrapper((WbsCode) _wbsCode.clone());
    }

    public int compareTo(WbsCode wbsCode) {
        return _wbsCode.compareTo(wbsCode);
    }

    @Override
    public int hashCode() {
        return _wbsCode.hashCode();
    }

    public com.liferay.portal.model.CacheModel<WbsCode> toCacheModel() {
        return _wbsCode.toCacheModel();
    }

    public WbsCode toEscapedModel() {
        return new WbsCodeWrapper(_wbsCode.toEscapedModel());
    }

    public WbsCode toUnescapedModel() {
        return new WbsCodeWrapper(_wbsCode.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _wbsCode.toString();
    }

    public java.lang.String toXmlString() {
        return _wbsCode.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _wbsCode.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof WbsCodeWrapper)) {
            return false;
        }

        WbsCodeWrapper wbsCodeWrapper = (WbsCodeWrapper) obj;

        if (Validator.equals(_wbsCode, wbsCodeWrapper._wbsCode)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public WbsCode getWrappedWbsCode() {
        return _wbsCode;
    }

    public WbsCode getWrappedModel() {
        return _wbsCode;
    }

    public void resetOriginalValues() {
        _wbsCode.resetOriginalValues();
    }

	@AutoEscape
	public String getWbsOwnerName() {
		return _wbsCode.getWbsOwnerName();
	}

	public void setWbsOwnerName(String wbsOwnerName) {
		_wbsCode.setWbsOwnerName(wbsOwnerName);
	}
}
