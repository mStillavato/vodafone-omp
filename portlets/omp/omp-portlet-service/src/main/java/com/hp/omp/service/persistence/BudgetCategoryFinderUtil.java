package com.hp.omp.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class BudgetCategoryFinderUtil {
    private static BudgetCategoryFinder _finder;

    public static java.util.List<com.hp.omp.model.BudgetCategory> getBudgetCategoryList(
    	int start, int end, String searchFilter)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getFinder().getBudgetCategoryList(start, end, searchFilter);
    }
    
    public static BudgetCategoryFinder getFinder() {
        if (_finder == null) {
            _finder = (BudgetCategoryFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
            		BudgetCategoryFinder.class.getName());

            ReferenceRegistry.registerReference(BudgetCategoryFinderUtil.class,
                "_finder");
        }

        return _finder;
    }

    public void setFinder(BudgetCategoryFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(BudgetCategoryFinderUtil.class,
            "_finder");
    }
}
