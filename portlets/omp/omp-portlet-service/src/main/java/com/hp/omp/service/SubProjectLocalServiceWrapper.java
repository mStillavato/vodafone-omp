package com.hp.omp.service;

import java.util.List;

import com.hp.omp.model.SubProject;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link SubProjectLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       SubProjectLocalService
 * @generated
 */
public class SubProjectLocalServiceWrapper implements SubProjectLocalService,
    ServiceWrapper<SubProjectLocalService> {
    private SubProjectLocalService _subProjectLocalService;

    public SubProjectLocalServiceWrapper(
        SubProjectLocalService subProjectLocalService) {
        _subProjectLocalService = subProjectLocalService;
    }

    /**
    * Adds the sub project to the database. Also notifies the appropriate model listeners.
    *
    * @param subProject the sub project
    * @return the sub project that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.SubProject addSubProject(
        com.hp.omp.model.SubProject subProject)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _subProjectLocalService.addSubProject(subProject);
    }

    /**
    * Creates a new sub project with the primary key. Does not add the sub project to the database.
    *
    * @param subProjectId the primary key for the new sub project
    * @return the new sub project
    */
    public com.hp.omp.model.SubProject createSubProject(long subProjectId) {
        return _subProjectLocalService.createSubProject(subProjectId);
    }

    /**
    * Deletes the sub project with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param subProjectId the primary key of the sub project
    * @return the sub project that was removed
    * @throws PortalException if a sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.SubProject deleteSubProject(long subProjectId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _subProjectLocalService.deleteSubProject(subProjectId);
    }

    /**
    * Deletes the sub project from the database. Also notifies the appropriate model listeners.
    *
    * @param subProject the sub project
    * @return the sub project that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.SubProject deleteSubProject(
        com.hp.omp.model.SubProject subProject)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _subProjectLocalService.deleteSubProject(subProject);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _subProjectLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _subProjectLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _subProjectLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _subProjectLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _subProjectLocalService.dynamicQueryCount(dynamicQuery);
    }

    public com.hp.omp.model.SubProject fetchSubProject(long subProjectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _subProjectLocalService.fetchSubProject(subProjectId);
    }

    /**
    * Returns the sub project with the primary key.
    *
    * @param subProjectId the primary key of the sub project
    * @return the sub project
    * @throws PortalException if a sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.SubProject getSubProject(long subProjectId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _subProjectLocalService.getSubProject(subProjectId);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _subProjectLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the sub projects.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of sub projects
    * @param end the upper bound of the range of sub projects (not inclusive)
    * @return the range of sub projects
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.SubProject> getSubProjects(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _subProjectLocalService.getSubProjects(start, end);
    }

    /**
    * Returns the number of sub projects.
    *
    * @return the number of sub projects
    * @throws SystemException if a system exception occurred
    */
    public int getSubProjectsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _subProjectLocalService.getSubProjectsCount();
    }

    /**
    * Updates the sub project in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param subProject the sub project
    * @return the sub project that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.SubProject updateSubProject(
        com.hp.omp.model.SubProject subProject)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _subProjectLocalService.updateSubProject(subProject);
    }

    /**
    * Updates the sub project in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param subProject the sub project
    * @param merge whether to merge the sub project with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the sub project that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.SubProject updateSubProject(
        com.hp.omp.model.SubProject subProject, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _subProjectLocalService.updateSubProject(subProject, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _subProjectLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _subProjectLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _subProjectLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    public com.hp.omp.model.SubProject getSubProjectByName(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _subProjectLocalService.getSubProjectByName(name);
    }

    public java.util.List<com.hp.omp.model.SubProject> getSubProjectByProjectId(
        java.lang.Long projectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _subProjectLocalService.getSubProjectByProjectId(projectId);
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public SubProjectLocalService getWrappedSubProjectLocalService() {
        return _subProjectLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedSubProjectLocalService(
        SubProjectLocalService subProjectLocalService) {
        _subProjectLocalService = subProjectLocalService;
    }

    public SubProjectLocalService getWrappedService() {
        return _subProjectLocalService;
    }

    public void setWrappedService(SubProjectLocalService subProjectLocalService) {
        _subProjectLocalService = subProjectLocalService;
    }

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<SubProject> getSubProjectsList(int start, int end,
			String searchFilter) throws SystemException {
		return _subProjectLocalService.getSubProjectsList(start, end, searchFilter);
	}
}
