package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the ODNPCode service. Represents a row in the &quot;ODNP_CODE&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see ODNPCodeModel
 * @see com.hp.omp.model.impl.ODNPCodeImpl
 * @see com.hp.omp.model.impl.ODNPCodeModelImpl
 * @generated
 */
public interface ODNPCode extends ODNPCodeModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.ODNPCodeImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
