package com.hp.omp.service.persistence;

import com.hp.omp.model.ODNPName;
import com.hp.omp.service.ODNPNameLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class ODNPNameActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public ODNPNameActionableDynamicQuery() throws SystemException {
        setBaseLocalService(ODNPNameLocalServiceUtil.getService());
        setClass(ODNPName.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("odnpNameId");
    }
}
