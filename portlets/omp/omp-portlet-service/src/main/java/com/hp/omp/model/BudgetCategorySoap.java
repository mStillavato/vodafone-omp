package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class BudgetCategorySoap implements Serializable {
    private long _budgetCategoryId;
    private String _budgetCategoryName;
    private boolean _display;

    public BudgetCategorySoap() {
    }

    public static BudgetCategorySoap toSoapModel(BudgetCategory model) {
        BudgetCategorySoap soapModel = new BudgetCategorySoap();

        soapModel.setBudgetCategoryId(model.getBudgetCategoryId());
        soapModel.setBudgetCategoryName(model.getBudgetCategoryName());
        soapModel.setDisplay(model.getDisplay());

        return soapModel;
    }

    public static BudgetCategorySoap[] toSoapModels(BudgetCategory[] models) {
        BudgetCategorySoap[] soapModels = new BudgetCategorySoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static BudgetCategorySoap[][] toSoapModels(BudgetCategory[][] models) {
        BudgetCategorySoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new BudgetCategorySoap[models.length][models[0].length];
        } else {
            soapModels = new BudgetCategorySoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static BudgetCategorySoap[] toSoapModels(List<BudgetCategory> models) {
        List<BudgetCategorySoap> soapModels = new ArrayList<BudgetCategorySoap>(models.size());

        for (BudgetCategory model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new BudgetCategorySoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _budgetCategoryId;
    }

    public void setPrimaryKey(long pk) {
        setBudgetCategoryId(pk);
    }

    public long getBudgetCategoryId() {
        return _budgetCategoryId;
    }

    public void setBudgetCategoryId(long budgetCategoryId) {
        _budgetCategoryId = budgetCategoryId;
    }

    public String getBudgetCategoryName() {
        return _budgetCategoryName;
    }

    public void setBudgetCategoryName(String budgetCategoryName) {
        _budgetCategoryName = budgetCategoryName;
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;
    }
}
