package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link POAudit}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       POAudit
 * @generated
 */
public class POAuditWrapper implements POAudit, ModelWrapper<POAudit> {
    private POAudit _poAudit;

    public POAuditWrapper(POAudit poAudit) {
        _poAudit = poAudit;
    }

    public Class<?> getModelClass() {
        return POAudit.class;
    }

    public String getModelClassName() {
        return POAudit.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("auditId", getAuditId());
        attributes.put("purchaseOrderId", getPurchaseOrderId());
        attributes.put("status", getStatus());
        attributes.put("userId", getUserId());
        attributes.put("changeDate", getChangeDate());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long auditId = (Long) attributes.get("auditId");

        if (auditId != null) {
            setAuditId(auditId);
        }

        Long purchaseOrderId = (Long) attributes.get("purchaseOrderId");

        if (purchaseOrderId != null) {
            setPurchaseOrderId(purchaseOrderId);
        }

        Integer status = (Integer) attributes.get("status");

        if (status != null) {
            setStatus(status);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        Date changeDate = (Date) attributes.get("changeDate");

        if (changeDate != null) {
            setChangeDate(changeDate);
        }
    }

    /**
    * Returns the primary key of this p o audit.
    *
    * @return the primary key of this p o audit
    */
    public long getPrimaryKey() {
        return _poAudit.getPrimaryKey();
    }

    /**
    * Sets the primary key of this p o audit.
    *
    * @param primaryKey the primary key of this p o audit
    */
    public void setPrimaryKey(long primaryKey) {
        _poAudit.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the audit ID of this p o audit.
    *
    * @return the audit ID of this p o audit
    */
    public long getAuditId() {
        return _poAudit.getAuditId();
    }

    /**
    * Sets the audit ID of this p o audit.
    *
    * @param auditId the audit ID of this p o audit
    */
    public void setAuditId(long auditId) {
        _poAudit.setAuditId(auditId);
    }

    /**
    * Returns the purchase order ID of this p o audit.
    *
    * @return the purchase order ID of this p o audit
    */
    public java.lang.Long getPurchaseOrderId() {
        return _poAudit.getPurchaseOrderId();
    }

    /**
    * Sets the purchase order ID of this p o audit.
    *
    * @param purchaseOrderId the purchase order ID of this p o audit
    */
    public void setPurchaseOrderId(java.lang.Long purchaseOrderId) {
        _poAudit.setPurchaseOrderId(purchaseOrderId);
    }

    /**
    * Returns the status of this p o audit.
    *
    * @return the status of this p o audit
    */
    public int getStatus() {
        return _poAudit.getStatus();
    }

    /**
    * Sets the status of this p o audit.
    *
    * @param status the status of this p o audit
    */
    public void setStatus(int status) {
        _poAudit.setStatus(status);
    }

    /**
    * Returns the user ID of this p o audit.
    *
    * @return the user ID of this p o audit
    */
    public java.lang.Long getUserId() {
        return _poAudit.getUserId();
    }

    /**
    * Sets the user ID of this p o audit.
    *
    * @param userId the user ID of this p o audit
    */
    public void setUserId(java.lang.Long userId) {
        _poAudit.setUserId(userId);
    }

    /**
    * Returns the change date of this p o audit.
    *
    * @return the change date of this p o audit
    */
    public java.util.Date getChangeDate() {
        return _poAudit.getChangeDate();
    }

    /**
    * Sets the change date of this p o audit.
    *
    * @param changeDate the change date of this p o audit
    */
    public void setChangeDate(java.util.Date changeDate) {
        _poAudit.setChangeDate(changeDate);
    }

    public boolean isNew() {
        return _poAudit.isNew();
    }

    public void setNew(boolean n) {
        _poAudit.setNew(n);
    }

    public boolean isCachedModel() {
        return _poAudit.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _poAudit.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _poAudit.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _poAudit.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _poAudit.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _poAudit.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _poAudit.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new POAuditWrapper((POAudit) _poAudit.clone());
    }

    public int compareTo(POAudit poAudit) {
        return _poAudit.compareTo(poAudit);
    }

    @Override
    public int hashCode() {
        return _poAudit.hashCode();
    }

    public com.liferay.portal.model.CacheModel<POAudit> toCacheModel() {
        return _poAudit.toCacheModel();
    }

    public POAudit toEscapedModel() {
        return new POAuditWrapper(_poAudit.toEscapedModel());
    }

    public POAudit toUnescapedModel() {
        return new POAuditWrapper(_poAudit.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _poAudit.toString();
    }

    public java.lang.String toXmlString() {
        return _poAudit.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _poAudit.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof POAuditWrapper)) {
            return false;
        }

        POAuditWrapper poAuditWrapper = (POAuditWrapper) obj;

        if (Validator.equals(_poAudit, poAuditWrapper._poAudit)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public POAudit getWrappedPOAudit() {
        return _poAudit;
    }

    public POAudit getWrappedModel() {
        return _poAudit;
    }

    public void resetOriginalValues() {
        _poAudit.resetOriginalValues();
    }
}
