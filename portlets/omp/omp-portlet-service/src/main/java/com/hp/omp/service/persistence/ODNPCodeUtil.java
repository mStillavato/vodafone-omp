package com.hp.omp.service.persistence;

import com.hp.omp.model.ODNPCode;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the o d n p code service. This utility wraps {@link ODNPCodePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see ODNPCodePersistence
 * @see ODNPCodePersistenceImpl
 * @generated
 */
public class ODNPCodeUtil {
    private static ODNPCodePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(ODNPCode odnpCode) {
        getPersistence().clearCache(odnpCode);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<ODNPCode> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<ODNPCode> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<ODNPCode> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static ODNPCode update(ODNPCode odnpCode, boolean merge)
        throws SystemException {
        return getPersistence().update(odnpCode, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static ODNPCode update(ODNPCode odnpCode, boolean merge,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(odnpCode, merge, serviceContext);
    }

    /**
    * Caches the o d n p code in the entity cache if it is enabled.
    *
    * @param odnpCode the o d n p code
    */
    public static void cacheResult(com.hp.omp.model.ODNPCode odnpCode) {
        getPersistence().cacheResult(odnpCode);
    }

    /**
    * Caches the o d n p codes in the entity cache if it is enabled.
    *
    * @param odnpCodes the o d n p codes
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.ODNPCode> odnpCodes) {
        getPersistence().cacheResult(odnpCodes);
    }

    /**
    * Creates a new o d n p code with the primary key. Does not add the o d n p code to the database.
    *
    * @param odnpCodeId the primary key for the new o d n p code
    * @return the new o d n p code
    */
    public static com.hp.omp.model.ODNPCode create(long odnpCodeId) {
        return getPersistence().create(odnpCodeId);
    }

    /**
    * Removes the o d n p code with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param odnpCodeId the primary key of the o d n p code
    * @return the o d n p code that was removed
    * @throws com.hp.omp.NoSuchODNPCodeException if a o d n p code with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ODNPCode remove(long odnpCodeId)
        throws com.hp.omp.NoSuchODNPCodeException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(odnpCodeId);
    }

    public static com.hp.omp.model.ODNPCode updateImpl(
        com.hp.omp.model.ODNPCode odnpCode, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(odnpCode, merge);
    }

    /**
    * Returns the o d n p code with the primary key or throws a {@link com.hp.omp.NoSuchODNPCodeException} if it could not be found.
    *
    * @param odnpCodeId the primary key of the o d n p code
    * @return the o d n p code
    * @throws com.hp.omp.NoSuchODNPCodeException if a o d n p code with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ODNPCode findByPrimaryKey(long odnpCodeId)
        throws com.hp.omp.NoSuchODNPCodeException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(odnpCodeId);
    }

    /**
    * Returns the o d n p code with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param odnpCodeId the primary key of the o d n p code
    * @return the o d n p code, or <code>null</code> if a o d n p code with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ODNPCode fetchByPrimaryKey(long odnpCodeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(odnpCodeId);
    }

    /**
    * Returns all the o d n p codes.
    *
    * @return the o d n p codes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ODNPCode> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the o d n p codes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of o d n p codes
    * @param end the upper bound of the range of o d n p codes (not inclusive)
    * @return the range of o d n p codes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ODNPCode> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the o d n p codes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of o d n p codes
    * @param end the upper bound of the range of o d n p codes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of o d n p codes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ODNPCode> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the o d n p codes from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of o d n p codes.
    *
    * @return the number of o d n p codes
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static ODNPCodePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (ODNPCodePersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    ODNPCodePersistence.class.getName());

            ReferenceRegistry.registerReference(ODNPCodeUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(ODNPCodePersistence persistence) {
    }
}
