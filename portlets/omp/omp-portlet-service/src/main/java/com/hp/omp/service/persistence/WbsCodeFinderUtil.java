package com.hp.omp.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class WbsCodeFinderUtil {
	private static WbsCodeFinder _finder;

	public static java.util.List<com.hp.omp.model.WbsCode> getWbsCodesList(
			int start, int end, String searchFilter)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getFinder().getWbsCodesList(start, end, searchFilter);
	}
	
	public static int getWbsCodesCountList(String searchFilter)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getFinder().getWbsCodesCountList(searchFilter);
	}

	public static WbsCodeFinder getFinder() {
		if (_finder == null) {
			_finder = (WbsCodeFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
					WbsCodeFinder.class.getName());

			ReferenceRegistry.registerReference(WbsCodeFinderUtil.class,
					"_finder");
		}

		return _finder;
	}

	public void setFinder(WbsCodeFinder finder) {
		_finder = finder;

		ReferenceRegistry.registerReference(WbsCodeFinderUtil.class,
				"_finder");
	}
}
