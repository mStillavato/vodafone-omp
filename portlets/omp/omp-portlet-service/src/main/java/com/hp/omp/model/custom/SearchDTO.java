/**
 * 
 */
package com.hp.omp.model.custom;

import java.io.Serializable;
import java.util.List;

import com.hp.omp.model.CostCenter;
import com.hp.omp.model.Project;
import com.hp.omp.model.SubProject;
import com.hp.omp.model.WbsCode;

/**
 * @author gewaly
 *
 */
public class SearchDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7684541966359000646L;
	
	private String poNumber  = null;
	private Long prId = null;
	private Integer prStatus = null;
	private Integer poStatus  = null;
	private String fiscalYear = null;
	private Long costCenterId = null;
	private String projectName  = null;
	private String subProjectName  = null;
	private String wbsCode  = null;
	private boolean disableCostCenter;
	private String globalSearchKeyWord = null;
	private Integer automatic = null;
	private String miniSearchValue = null;
	private boolean miniSeach;
	private Integer pageNumber = 1;
	private Integer pageSize =20;
	private String orderby =null;
	private String order = null;
	private Integer positionStatus = null;
	private Integer positionApproved = null;
	private String positionICApproval = null;
	private List<Long> allPONumbers;
	private List<PurchaseOrderStatus> allPOStatus;
	private List<String> allFiscalYears;
	private List<CostCenter> allCostCenters;
	private List<String> allProjectNames;
	private List<String> allSubProjectNames;
	private List<WbsCode> allWbsCodes;
	private List<Project> allProjects;
	private List<SubProject> allSubProjects;
	private Integer start = null;
	private Integer end = null;
	private String userRole = null;
	private String prSearchValue=null;
	private Integer tipoPr = null;
	
	
	/**
	 * @return the poNumber
	 */
	public String getPoNumber() {
		return poNumber;
	}
	/**
	 * @param poNumber the poNumber to set
	 */
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	/**
	 * @return the poStatus
	 */
	public Integer getPoStatus() {
		return poStatus;
	}
	/**
	 * @param poStatus the poStatus to set
	 */
	public void setPoStatus(Integer poStatus) {
		this.poStatus = poStatus;
	}
	/**
	 * @return the fiscalYear
	 */
	public String getFiscalYear() {
		return fiscalYear;
	}
	/**
	 * @param fiscalYear the fiscalYear to set
	 */
	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}
	/**
	 * @return the costCenterId
	 */
	public Long getCostCenterId() {
		return costCenterId;
	}
	/**
	 * @param costCenterId the costCenterId to set
	 */
	public void setCostCenterId(Long costCenterId) {
		this.costCenterId = costCenterId;
	}
	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}
	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	/**
	 * @return the subProjectName
	 */
	public String getSubProjectName() {
		return subProjectName;
	}
	/**
	 * @param subProjectName the subProjectName to set
	 */
	public void setSubProjectName(String subProjectName) {
		this.subProjectName = subProjectName;
	}
	/**
	 * @return the wbsCode
	 */
	public String getWbsCode() {
		return wbsCode;
	}
	/**
	 * @param wbsCode the wbsCode to set
	 */
	public void setWbsCode(String wbsCode) {
		this.wbsCode = wbsCode;
	}
	/**
	 * @return the allPONumbers
	 */
	public List<Long> getAllPONumbers() {
		return allPONumbers;
	}
	/**
	 * @param allPONumbers the allPONumbers to set
	 */
	public void setAllPONumbers(List<Long> allPONumbers) {
		this.allPONumbers = allPONumbers;
	}
	/**
	 * @return the allPOStatus
	 */
	public List<PurchaseOrderStatus> getAllPOStatus() {
		return allPOStatus;
	}
	/**
	 * @param allPositionStatus the allPOStatus to set
	 */
	public void setAllPOStatus(List<PurchaseOrderStatus> allPOStatus) {
		this.allPOStatus = allPOStatus;
	}
	/**
	 * @return the allFiscalYears
	 */
	public List<String> getAllFiscalYears() {
		return allFiscalYears;
	}
	/**
	 * @param allFiscalYears the allFiscalYears to set
	 */
	public void setAllFiscalYears(List<String> allFiscalYears) {
		this.allFiscalYears = allFiscalYears;
	}
	/**
	 * @return the allCostCenters
	 */
	public List<CostCenter> getAllCostCenters() {
		return allCostCenters;
	}
	/**
	 * @param allCostCenters the allCostCenters to set
	 */
	public void setAllCostCenters(List<CostCenter> allCostCenters) {
		this.allCostCenters = allCostCenters;
	}
	/**
	 * @return the allProjectNames
	 */
	public List<String> getAllProjectNames() {
		return allProjectNames;
	}
	/**
	 * @param allProjectNames the allProjectNames to set
	 */
	public void setAllProjectNames(List<String> allProjectNames) {
		this.allProjectNames = allProjectNames;
	}
	/**
	 * @return the allSubProjectNames
	 */
	public List<String> getAllSubProjectNames() {
		return allSubProjectNames;
	}
	/**
	 * @param allSubProjectNames the allSubProjectNames to set
	 */
	public void setAllSubProjectNames(List<String> allSubProjectNames) {
		this.allSubProjectNames = allSubProjectNames;
	}
	/**
	 * @return the allWbsCodes
	 */
	public List<WbsCode> getAllWbsCodes() {
		return allWbsCodes;
	}
	/**
	 * @param allWbsCodes the allWbsCodes to set
	 */
	public void setAllWbsCodes(List<WbsCode> allWbsCodes) {
		this.allWbsCodes = allWbsCodes;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Integer getPrStatus() {
		return prStatus;
	}
	public void setPrStatus(Integer prStatus) {
		this.prStatus = prStatus;
	}
	public Long getPrId() {
		return prId;
	}
	public void setPrId(Long prId) {
		this.prId = prId;
	}
	/**
	 * @return the disableCostCenter
	 */
	public boolean isDisableCostCenter() {
		return disableCostCenter;
	}
	/**
	 * @param disableCostCenter the disableCostCenter to set
	 */
	public void setDisableCostCenter(boolean disableCostCenter) {
		this.disableCostCenter = disableCostCenter;
	}
	/**
	 * @return the miniSearchValue
	 */
	public String getMiniSearchValue() {
		return miniSearchValue;
	}
	/**
	 * @param miniSearchValue the miniSearchValue to set
	 */
	public void setMiniSearchValue(String miniSearchValue) {
		this.miniSearchValue = miniSearchValue;
	}
	/**
	 * @return the miniSeach
	 */
	public boolean isMiniSeach() {
		return miniSeach;
	}
	/**
	 * @param miniSeach the miniSeach to set
	 */
	public void setMiniSeach(boolean miniSeach) {
		this.miniSeach = miniSeach;
	}
	
	public String getGlobalSearchKeyWord() {
		return globalSearchKeyWord;
	}
	public void setGlobalSearchKeyWord(String globalSearchKeyWord) {
		this.globalSearchKeyWord = globalSearchKeyWord;
	}
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public String getOrderby() {
		return orderby;
	}
	public void setOrderby(String orderby) {
		this.orderby = orderby;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	public Integer getAutomatic() {
		return automatic;
	}
	public void setAutomatic(Integer automatic) {
		this.automatic = automatic;
	}
	public Integer getPositionStatus() {
		return positionStatus;
	}
	public void setPositionStatus(Integer positionStatus) {
		this.positionStatus = positionStatus;
	}
	/**
	 * @return the allProjects
	 */
	public List<Project> getAllProjects() {
		return allProjects;
	}
	/**
	 * @param allProjects the allProjects to set
	 */
	public void setAllProjects(List<Project> allProjects) {
		this.allProjects = allProjects;
	}
	/**
	 * @return the allSubProjects
	 */
	public List<SubProject> getAllSubProjects() {
		return allSubProjects;
	}
	/**
	 * @param allSubProjects the allSubProjects to set
	 */
	public void setAllSubProjects(List<SubProject> allSubProjects) {
		this.allSubProjects = allSubProjects;
	}

	
	public Integer getStart() {
		return start;
	}
	public void setStart(Integer start) {
		this.start = start;
	}
	public Integer getEnd() {
		return end;
	}
	public void setEnd(Integer end) {
		this.end = end;
	}
	public Integer getPositionApproved() {
		return positionApproved;
	}
	public void setPositionApproved(Integer positionApproved) {
		this.positionApproved = positionApproved;
	}
	public String getPositionICApproval() {
		return positionICApproval;
	}
	public void setPositionICApproval(String positionICApproval) {
		this.positionICApproval = positionICApproval;
	}
	
	public String getUserRole() {
		return userRole;
	}
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}
	public String getPrSearchValue() {
		return prSearchValue;
	}
	public void setPrSearchValue(String prSearchValue) {
		this.prSearchValue = prSearchValue;
	}
	public Integer getTipoPr() {
		return tipoPr;
	}
	public void setTipoPr(Integer tipoPr) {
		this.tipoPr = tipoPr;
	}
}
