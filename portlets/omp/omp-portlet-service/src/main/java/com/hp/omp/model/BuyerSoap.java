package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class BuyerSoap implements Serializable {
    private long _buyerId;
    private String _name;
    private boolean _display;

    public BuyerSoap() {
    }

    public static BuyerSoap toSoapModel(Buyer model) {
        BuyerSoap soapModel = new BuyerSoap();

        soapModel.setBuyerId(model.getBuyerId());
        soapModel.setName(model.getName());
        soapModel.setDisplay(model.getDisplay());

        return soapModel;
    }

    public static BuyerSoap[] toSoapModels(Buyer[] models) {
        BuyerSoap[] soapModels = new BuyerSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static BuyerSoap[][] toSoapModels(Buyer[][] models) {
        BuyerSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new BuyerSoap[models.length][models[0].length];
        } else {
            soapModels = new BuyerSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static BuyerSoap[] toSoapModels(List<Buyer> models) {
        List<BuyerSoap> soapModels = new ArrayList<BuyerSoap>(models.size());

        for (Buyer model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new BuyerSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _buyerId;
    }

    public void setPrimaryKey(long pk) {
        setBuyerId(pk);
    }

    public long getBuyerId() {
        return _buyerId;
    }

    public void setBuyerId(long buyerId) {
        _buyerId = buyerId;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;
    }
}
