package com.hp.omp.service.persistence;

import com.hp.omp.model.PurchaseRequest;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the purchase request service. This utility wraps {@link PurchaseRequestPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see PurchaseRequestPersistence
 * @see PurchaseRequestPersistenceImpl
 * @generated
 */
public class PurchaseRequestUtil {
    private static PurchaseRequestPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(PurchaseRequest purchaseRequest) {
        getPersistence().clearCache(purchaseRequest);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<PurchaseRequest> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<PurchaseRequest> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<PurchaseRequest> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static PurchaseRequest update(PurchaseRequest purchaseRequest,
        boolean merge) throws SystemException {
        return getPersistence().update(purchaseRequest, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static PurchaseRequest update(PurchaseRequest purchaseRequest,
        boolean merge, ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(purchaseRequest, merge, serviceContext);
    }

    /**
    * Caches the purchase request in the entity cache if it is enabled.
    *
    * @param purchaseRequest the purchase request
    */
    public static void cacheResult(
        com.hp.omp.model.PurchaseRequest purchaseRequest) {
        getPersistence().cacheResult(purchaseRequest);
    }

    /**
    * Caches the purchase requests in the entity cache if it is enabled.
    *
    * @param purchaseRequests the purchase requests
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.PurchaseRequest> purchaseRequests) {
        getPersistence().cacheResult(purchaseRequests);
    }

    /**
    * Creates a new purchase request with the primary key. Does not add the purchase request to the database.
    *
    * @param purchaseRequestId the primary key for the new purchase request
    * @return the new purchase request
    */
    public static com.hp.omp.model.PurchaseRequest create(
        long purchaseRequestId) {
        return getPersistence().create(purchaseRequestId);
    }

    /**
    * Removes the purchase request with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param purchaseRequestId the primary key of the purchase request
    * @return the purchase request that was removed
    * @throws com.hp.omp.NoSuchPurchaseRequestException if a purchase request with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.PurchaseRequest remove(
        long purchaseRequestId)
        throws com.hp.omp.NoSuchPurchaseRequestException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(purchaseRequestId);
    }

    public static com.hp.omp.model.PurchaseRequest updateImpl(
        com.hp.omp.model.PurchaseRequest purchaseRequest, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(purchaseRequest, merge);
    }

    /**
    * Returns the purchase request with the primary key or throws a {@link com.hp.omp.NoSuchPurchaseRequestException} if it could not be found.
    *
    * @param purchaseRequestId the primary key of the purchase request
    * @return the purchase request
    * @throws com.hp.omp.NoSuchPurchaseRequestException if a purchase request with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.PurchaseRequest findByPrimaryKey(
        long purchaseRequestId)
        throws com.hp.omp.NoSuchPurchaseRequestException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(purchaseRequestId);
    }

    /**
    * Returns the purchase request with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param purchaseRequestId the primary key of the purchase request
    * @return the purchase request, or <code>null</code> if a purchase request with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.PurchaseRequest fetchByPrimaryKey(
        long purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(purchaseRequestId);
    }

    /**
    * Returns all the purchase requests.
    *
    * @return the purchase requests
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.PurchaseRequest> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the purchase requests.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of purchase requests
    * @param end the upper bound of the range of purchase requests (not inclusive)
    * @return the range of purchase requests
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.PurchaseRequest> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the purchase requests.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of purchase requests
    * @param end the upper bound of the range of purchase requests (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of purchase requests
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.PurchaseRequest> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the purchase requests from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of purchase requests.
    *
    * @return the number of purchase requests
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static PurchaseRequestPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (PurchaseRequestPersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    PurchaseRequestPersistence.class.getName());

            ReferenceRegistry.registerReference(PurchaseRequestUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(PurchaseRequestPersistence persistence) {
    }
}
