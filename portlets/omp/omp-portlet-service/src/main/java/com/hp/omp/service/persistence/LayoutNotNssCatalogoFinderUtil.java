package com.hp.omp.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class LayoutNotNssCatalogoFinderUtil {
    private static LayoutNotNssCatalogoFinder _finder;

    public static java.util.List<com.hp.omp.model.custom.LayoutNotNssCatalogoListDTO> getLayoutNotNssCatalogoList(String user) {
        return getFinder().getLayoutNotNssCatalogoList(user);
    }
    
    public static java.lang.Long getLayoutNotNssCatalogoListCount(String user) {
        return getFinder().getLayoutNotNssCatalogoListCount(user);
    }
    
    public static void deleteLayoutNotNssCatalogo4User(String user) {
        getFinder().deleteLayoutNotNssCatalogo4User(user);
    }
    
    public static LayoutNotNssCatalogoFinder getFinder() {
        if (_finder == null) {
            _finder = (LayoutNotNssCatalogoFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
            		LayoutNotNssCatalogoFinder.class.getName());

            ReferenceRegistry.registerReference(LayoutNotNssCatalogoFinderUtil.class,
                "_finder");
        }

        return _finder;
    }

    public void setFinder(LayoutNotNssCatalogoFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(LayoutNotNssCatalogoFinderUtil.class,
            "_finder");
    }
}
