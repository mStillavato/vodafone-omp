package com.hp.omp.model;

import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.CostCenterUserLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class CostCenterUserClp extends BaseModelImpl<CostCenterUser>
    implements CostCenterUser {
    private long _costCenterUserId;
    private String _costCenterUserUuid;
    private long _costCenterId;
    private long _userId;
    private String _userUuid;
    private BaseModel<?> _costCenterUserRemoteModel;

    public CostCenterUserClp() {
    }

    public Class<?> getModelClass() {
        return CostCenterUser.class;
    }

    public String getModelClassName() {
        return CostCenterUser.class.getName();
    }

    public long getPrimaryKey() {
        return _costCenterUserId;
    }

    public void setPrimaryKey(long primaryKey) {
        setCostCenterUserId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_costCenterUserId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("costCenterUserId", getCostCenterUserId());
        attributes.put("costCenterId", getCostCenterId());
        attributes.put("userId", getUserId());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long costCenterUserId = (Long) attributes.get("costCenterUserId");

        if (costCenterUserId != null) {
            setCostCenterUserId(costCenterUserId);
        }

        Long costCenterId = (Long) attributes.get("costCenterId");

        if (costCenterId != null) {
            setCostCenterId(costCenterId);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }
    }

    public long getCostCenterUserId() {
        return _costCenterUserId;
    }

    public void setCostCenterUserId(long costCenterUserId) {
        _costCenterUserId = costCenterUserId;

        if (_costCenterUserRemoteModel != null) {
            try {
                Class<?> clazz = _costCenterUserRemoteModel.getClass();

                Method method = clazz.getMethod("setCostCenterUserId",
                        long.class);

                method.invoke(_costCenterUserRemoteModel, costCenterUserId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getCostCenterUserUuid() throws SystemException {
        return PortalUtil.getUserValue(getCostCenterUserId(), "uuid",
            _costCenterUserUuid);
    }

    public void setCostCenterUserUuid(String costCenterUserUuid) {
        _costCenterUserUuid = costCenterUserUuid;
    }

    public long getCostCenterId() {
        return _costCenterId;
    }

    public void setCostCenterId(long costCenterId) {
        _costCenterId = costCenterId;

        if (_costCenterUserRemoteModel != null) {
            try {
                Class<?> clazz = _costCenterUserRemoteModel.getClass();

                Method method = clazz.getMethod("setCostCenterId", long.class);

                method.invoke(_costCenterUserRemoteModel, costCenterId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public long getUserId() {
        return _userId;
    }

    public void setUserId(long userId) {
        _userId = userId;

        if (_costCenterUserRemoteModel != null) {
            try {
                Class<?> clazz = _costCenterUserRemoteModel.getClass();

                Method method = clazz.getMethod("setUserId", long.class);

                method.invoke(_costCenterUserRemoteModel, userId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getUserUuid() throws SystemException {
        return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
    }

    public void setUserUuid(String userUuid) {
        _userUuid = userUuid;
    }

    public BaseModel<?> getCostCenterUserRemoteModel() {
        return _costCenterUserRemoteModel;
    }

    public void setCostCenterUserRemoteModel(
        BaseModel<?> costCenterUserRemoteModel) {
        _costCenterUserRemoteModel = costCenterUserRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _costCenterUserRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_costCenterUserRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            CostCenterUserLocalServiceUtil.addCostCenterUser(this);
        } else {
            CostCenterUserLocalServiceUtil.updateCostCenterUser(this);
        }
    }

    @Override
    public CostCenterUser toEscapedModel() {
        return (CostCenterUser) ProxyUtil.newProxyInstance(CostCenterUser.class.getClassLoader(),
            new Class[] { CostCenterUser.class },
            new AutoEscapeBeanHandler(this));
    }

    public CostCenterUser toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        CostCenterUserClp clone = new CostCenterUserClp();

        clone.setCostCenterUserId(getCostCenterUserId());
        clone.setCostCenterId(getCostCenterId());
        clone.setUserId(getUserId());

        return clone;
    }

    public int compareTo(CostCenterUser costCenterUser) {
        long primaryKey = costCenterUser.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CostCenterUserClp)) {
            return false;
        }

        CostCenterUserClp costCenterUser = (CostCenterUserClp) obj;

        long primaryKey = costCenterUser.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{costCenterUserId=");
        sb.append(getCostCenterUserId());
        sb.append(", costCenterId=");
        sb.append(getCostCenterId());
        sb.append(", userId=");
        sb.append(getUserId());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(13);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.CostCenterUser");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>costCenterUserId</column-name><column-value><![CDATA[");
        sb.append(getCostCenterUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>costCenterId</column-name><column-value><![CDATA[");
        sb.append(getCostCenterId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>userId</column-name><column-value><![CDATA[");
        sb.append(getUserId());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
