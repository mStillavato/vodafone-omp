package com.hp.omp.service.persistence;

import com.hp.omp.model.CostCenterUser;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the cost center user service. This utility wraps {@link CostCenterUserPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see CostCenterUserPersistence
 * @see CostCenterUserPersistenceImpl
 * @generated
 */
public class CostCenterUserUtil {
    private static CostCenterUserPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(CostCenterUser costCenterUser) {
        getPersistence().clearCache(costCenterUser);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<CostCenterUser> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<CostCenterUser> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<CostCenterUser> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static CostCenterUser update(CostCenterUser costCenterUser,
        boolean merge) throws SystemException {
        return getPersistence().update(costCenterUser, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static CostCenterUser update(CostCenterUser costCenterUser,
        boolean merge, ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(costCenterUser, merge, serviceContext);
    }

    /**
    * Caches the cost center user in the entity cache if it is enabled.
    *
    * @param costCenterUser the cost center user
    */
    public static void cacheResult(
        com.hp.omp.model.CostCenterUser costCenterUser) {
        getPersistence().cacheResult(costCenterUser);
    }

    /**
    * Caches the cost center users in the entity cache if it is enabled.
    *
    * @param costCenterUsers the cost center users
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.CostCenterUser> costCenterUsers) {
        getPersistence().cacheResult(costCenterUsers);
    }

    /**
    * Creates a new cost center user with the primary key. Does not add the cost center user to the database.
    *
    * @param costCenterUserId the primary key for the new cost center user
    * @return the new cost center user
    */
    public static com.hp.omp.model.CostCenterUser create(long costCenterUserId) {
        return getPersistence().create(costCenterUserId);
    }

    /**
    * Removes the cost center user with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param costCenterUserId the primary key of the cost center user
    * @return the cost center user that was removed
    * @throws com.hp.omp.NoSuchCostCenterUserException if a cost center user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.CostCenterUser remove(long costCenterUserId)
        throws com.hp.omp.NoSuchCostCenterUserException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(costCenterUserId);
    }

    public static com.hp.omp.model.CostCenterUser updateImpl(
        com.hp.omp.model.CostCenterUser costCenterUser, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(costCenterUser, merge);
    }

    /**
    * Returns the cost center user with the primary key or throws a {@link com.hp.omp.NoSuchCostCenterUserException} if it could not be found.
    *
    * @param costCenterUserId the primary key of the cost center user
    * @return the cost center user
    * @throws com.hp.omp.NoSuchCostCenterUserException if a cost center user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.CostCenterUser findByPrimaryKey(
        long costCenterUserId)
        throws com.hp.omp.NoSuchCostCenterUserException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(costCenterUserId);
    }

    /**
    * Returns the cost center user with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param costCenterUserId the primary key of the cost center user
    * @return the cost center user, or <code>null</code> if a cost center user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.CostCenterUser fetchByPrimaryKey(
        long costCenterUserId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(costCenterUserId);
    }

    /**
    * Returns all the cost center users.
    *
    * @return the cost center users
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.CostCenterUser> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the cost center users.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of cost center users
    * @param end the upper bound of the range of cost center users (not inclusive)
    * @return the range of cost center users
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.CostCenterUser> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the cost center users.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of cost center users
    * @param end the upper bound of the range of cost center users (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of cost center users
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.CostCenterUser> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the cost center users from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of cost center users.
    *
    * @return the number of cost center users
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static CostCenterUserPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (CostCenterUserPersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    CostCenterUserPersistence.class.getName());

            ReferenceRegistry.registerReference(CostCenterUserUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(CostCenterUserPersistence persistence) {
    }
}
