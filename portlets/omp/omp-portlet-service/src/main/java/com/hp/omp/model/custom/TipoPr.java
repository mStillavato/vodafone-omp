package com.hp.omp.model.custom;

import java.util.HashMap;
import java.util.Map;

public enum TipoPr {
	NORMAL(0, "Normal Pr"),
	NSS(1, "NSS Pr"),
	NOT_NSS(2, "Not NSS Pr"),
	VBS(3, "VBS");
	
	private int code;
	private String label;
	
	
	
	private TipoPr(int code, String label){
		this.code = code;
		this.label = label;
	}
	
	
	  private static Map<Integer, TipoPr> codeToStatusMapping;
		 
	    
	 
	    public static TipoPr getStatus(int i) {
	        if (codeToStatusMapping == null) {
	            initMapping();
	        }
	        return codeToStatusMapping.get(i);
	    }
	 
	    private static void initMapping() {
	        codeToStatusMapping = new HashMap<Integer, TipoPr>();
	        for (TipoPr s : values()) {
	            codeToStatusMapping.put(s.code, s);
	        }
	    }
	public int getCode(){
		return code;
	}
	
	public String getLabel(){
		return label;
	}
}
