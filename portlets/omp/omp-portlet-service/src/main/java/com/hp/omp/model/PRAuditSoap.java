package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class PRAuditSoap implements Serializable {
    private long _auditId;
    private Long _purchaseRequestId;
    private int _status;
    private Long _userId;
    private Date _changeDate;

    public PRAuditSoap() {
    }

    public static PRAuditSoap toSoapModel(PRAudit model) {
        PRAuditSoap soapModel = new PRAuditSoap();

        soapModel.setAuditId(model.getAuditId());
        soapModel.setPurchaseRequestId(model.getPurchaseRequestId());
        soapModel.setStatus(model.getStatus());
        soapModel.setUserId(model.getUserId());
        soapModel.setChangeDate(model.getChangeDate());

        return soapModel;
    }

    public static PRAuditSoap[] toSoapModels(PRAudit[] models) {
        PRAuditSoap[] soapModels = new PRAuditSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static PRAuditSoap[][] toSoapModels(PRAudit[][] models) {
        PRAuditSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new PRAuditSoap[models.length][models[0].length];
        } else {
            soapModels = new PRAuditSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static PRAuditSoap[] toSoapModels(List<PRAudit> models) {
        List<PRAuditSoap> soapModels = new ArrayList<PRAuditSoap>(models.size());

        for (PRAudit model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new PRAuditSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _auditId;
    }

    public void setPrimaryKey(long pk) {
        setAuditId(pk);
    }

    public long getAuditId() {
        return _auditId;
    }

    public void setAuditId(long auditId) {
        _auditId = auditId;
    }

    public Long getPurchaseRequestId() {
        return _purchaseRequestId;
    }

    public void setPurchaseRequestId(Long purchaseRequestId) {
        _purchaseRequestId = purchaseRequestId;
    }

    public int getStatus() {
        return _status;
    }

    public void setStatus(int status) {
        _status = status;
    }

    public Long getUserId() {
        return _userId;
    }

    public void setUserId(Long userId) {
        _userId = userId;
    }

    public Date getChangeDate() {
        return _changeDate;
    }

    public void setChangeDate(Date changeDate) {
        _changeDate = changeDate;
    }
}
