package com.hp.omp.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

/**
 * The base model interface for the TrackingCodes service. Represents a row in the &quot;TRACKING_CODES&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link com.hp.omp.model.impl.TrackingCodesModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link com.hp.omp.model.impl.TrackingCodesImpl}.
 * </p>
 *
 * @author HP Egypt team
 * @see TrackingCodes
 * @see com.hp.omp.model.impl.TrackingCodesImpl
 * @see com.hp.omp.model.impl.TrackingCodesModelImpl
 * @generated
 */
public interface TrackingCodesModel extends BaseModel<TrackingCodes> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. All methods that expect a tracking codes model instance should use the {@link TrackingCodes} interface instead.
     */

    /**
     * Returns the primary key of this tracking codes.
     *
     * @return the primary key of this tracking codes
     */
    public long getPrimaryKey();

    /**
     * Sets the primary key of this tracking codes.
     *
     * @param primaryKey the primary key of this tracking codes
     */
    public void setPrimaryKey(long primaryKey);

    /**
     * Returns the tracking code ID of this tracking codes.
     *
     * @return the tracking code ID of this tracking codes
     */
    public long getTrackingCodeId();

    /**
     * Sets the tracking code ID of this tracking codes.
     *
     * @param trackingCodeId the tracking code ID of this tracking codes
     */
    public void setTrackingCodeId(long trackingCodeId);

    /**
     * Returns the tracking code name of this tracking codes.
     *
     * @return the tracking code name of this tracking codes
     */
    @AutoEscape
    public String getTrackingCodeName();

    /**
     * Sets the tracking code name of this tracking codes.
     *
     * @param trackingCodeName the tracking code name of this tracking codes
     */
    public void setTrackingCodeName(String trackingCodeName);

    /**
     * Returns the tracking code description of this tracking codes.
     *
     * @return the tracking code description of this tracking codes
     */
    @AutoEscape
    public String getTrackingCodeDescription();

    /**
     * Sets the tracking code description of this tracking codes.
     *
     * @param trackingCodeDescription the tracking code description of this tracking codes
     */
    public void setTrackingCodeDescription(String trackingCodeDescription);

    /**
     * Returns the project ID of this tracking codes.
     *
     * @return the project ID of this tracking codes
     */
    @AutoEscape
    public String getProjectId();

    /**
     * Sets the project ID of this tracking codes.
     *
     * @param projectId the project ID of this tracking codes
     */
    public void setProjectId(String projectId);

    /**
     * Returns the sub project ID of this tracking codes.
     *
     * @return the sub project ID of this tracking codes
     */
    @AutoEscape
    public String getSubProjectId();

    /**
     * Sets the sub project ID of this tracking codes.
     *
     * @param subProjectId the sub project ID of this tracking codes
     */
    public void setSubProjectId(String subProjectId);

    /**
     * Returns the budget category ID of this tracking codes.
     *
     * @return the budget category ID of this tracking codes
     */
    @AutoEscape
    public String getBudgetCategoryId();

    /**
     * Sets the budget category ID of this tracking codes.
     *
     * @param budgetCategoryId the budget category ID of this tracking codes
     */
    public void setBudgetCategoryId(String budgetCategoryId);

    /**
     * Returns the budget sub category ID of this tracking codes.
     *
     * @return the budget sub category ID of this tracking codes
     */
    @AutoEscape
    public String getBudgetSubCategoryId();

    /**
     * Sets the budget sub category ID of this tracking codes.
     *
     * @param budgetSubCategoryId the budget sub category ID of this tracking codes
     */
    public void setBudgetSubCategoryId(String budgetSubCategoryId);

    /**
     * Returns the macro driver ID of this tracking codes.
     *
     * @return the macro driver ID of this tracking codes
     */
    @AutoEscape
    public String getMacroDriverId();

    /**
     * Sets the macro driver ID of this tracking codes.
     *
     * @param macroDriverId the macro driver ID of this tracking codes
     */
    public void setMacroDriverId(String macroDriverId);

    /**
     * Returns the micro driver ID of this tracking codes.
     *
     * @return the micro driver ID of this tracking codes
     */
    @AutoEscape
    public String getMicroDriverId();

    /**
     * Sets the micro driver ID of this tracking codes.
     *
     * @param microDriverId the micro driver ID of this tracking codes
     */
    public void setMicroDriverId(String microDriverId);

    /**
     * Returns the odnp name ID of this tracking codes.
     *
     * @return the odnp name ID of this tracking codes
     */
    @AutoEscape
    public String getOdnpNameId();

    /**
     * Sets the odnp name ID of this tracking codes.
     *
     * @param odnpNameId the odnp name ID of this tracking codes
     */
    public void setOdnpNameId(String odnpNameId);

    /**
     * Returns the wbs code ID of this tracking codes.
     *
     * @return the wbs code ID of this tracking codes
     */
    @AutoEscape
    public String getWbsCodeId();

    /**
     * Sets the wbs code ID of this tracking codes.
     *
     * @param wbsCodeId the wbs code ID of this tracking codes
     */
    public void setWbsCodeId(String wbsCodeId);

    /**
     * Returns the display of this tracking codes.
     *
     * @return the display of this tracking codes
     */
    public boolean getDisplay();

    /**
     * Returns <code>true</code> if this tracking codes is display.
     *
     * @return <code>true</code> if this tracking codes is display; <code>false</code> otherwise
     */
    public boolean isDisplay();

    /**
     * Sets whether this tracking codes is display.
     *
     * @param display the display of this tracking codes
     */
    public void setDisplay(boolean display);

    public boolean isNew();

    public void setNew(boolean n);

    public boolean isCachedModel();

    public void setCachedModel(boolean cachedModel);

    public boolean isEscapedModel();

    public Serializable getPrimaryKeyObj();

    public void setPrimaryKeyObj(Serializable primaryKeyObj);

    public ExpandoBridge getExpandoBridge();

    public void setExpandoBridgeAttributes(ServiceContext serviceContext);

    public Object clone();

    public int compareTo(TrackingCodes trackingCodes);

    public int hashCode();

    public CacheModel<TrackingCodes> toCacheModel();

    public TrackingCodes toEscapedModel();

    public TrackingCodes toUnescapedModel();

    public String toString();

    public String toXmlString();
}
