/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class LayoutNotNssCatalogoSoap implements Serializable {
	public static LayoutNotNssCatalogoSoap toSoapModel(LayoutNotNssCatalogo model) {
		LayoutNotNssCatalogoSoap soapModel = new LayoutNotNssCatalogoSoap();

		soapModel.setLayoutNotNssCatalogoId(model.getLayoutNotNssCatalogoId());
		soapModel.setLineNumber(model.getLineNumber());
		soapModel.setMaterialId(model.getMaterialId());
		soapModel.setMaterialDesc(model.getMaterialDesc());
		soapModel.setQuantity(model.getQuantity());
		soapModel.setNetPrice(model.getNetPrice());
		soapModel.setCurrency(model.getCurrency());
		soapModel.setDeliveryDate(model.getDeliveryDate());
		soapModel.setDeliveryAddress(model.getDeliveryAddress());
		soapModel.setItemText(model.getItemText());
		soapModel.setLocationEvo(model.getLocationEvo());
		soapModel.setTargaTecnica(model.getTargaTecnica());
		soapModel.setUser_(model.getUser_());
		soapModel.setVendorCode(model.getVendorCode());
		soapModel.setCatalogoId(model.getCatalogoId());

		return soapModel;
	}

	public static LayoutNotNssCatalogoSoap[] toSoapModels(LayoutNotNssCatalogo[] models) {
		LayoutNotNssCatalogoSoap[] soapModels = new LayoutNotNssCatalogoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LayoutNotNssCatalogoSoap[][] toSoapModels(LayoutNotNssCatalogo[][] models) {
		LayoutNotNssCatalogoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LayoutNotNssCatalogoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LayoutNotNssCatalogoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LayoutNotNssCatalogoSoap[] toSoapModels(List<LayoutNotNssCatalogo> models) {
		List<LayoutNotNssCatalogoSoap> soapModels = new ArrayList<LayoutNotNssCatalogoSoap>(models.size());

		for (LayoutNotNssCatalogo model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LayoutNotNssCatalogoSoap[soapModels.size()]);
	}

	public LayoutNotNssCatalogoSoap() {
	}

	public long getPrimaryKey() {
		return _layoutNotNssCatalogoId;
	}

	public void setPrimaryKey(long pk) {
		setLayoutNotNssCatalogoId(pk);
	}

	private long _layoutNotNssCatalogoId;
	private long _lineNumber;
	private String _materialId; 
	private String _materialDesc; 
	private String _quantity; 
	private String _netPrice; 
	private String _currency; 
	private Date _deliveryDate;
	private String _deliveryAddress;
	private String _itemText;
	private String _locationEvo;
	private String _targaTecnica;
	private String _user_;
	private String _vendorCode;
	private long _catalogoId;
	
	public long getLayoutNotNssCatalogoId() {
		return _layoutNotNssCatalogoId;
	}

	public void setLayoutNotNssCatalogoId(long _layoutNotNssCatalogoId) {
		this._layoutNotNssCatalogoId = _layoutNotNssCatalogoId;
	}

	public long getLineNumber() {
		return _lineNumber;
	}

	public void setLineNumber(long _lineNumber) {
		this._lineNumber = _lineNumber;
	}

	public String getMaterialId() {
		return _materialId;
	}

	public void setMaterialId(String _materialId) {
		this._materialId = _materialId;
	}

	public String getMaterialDesc() {
		return _materialDesc;
	}

	public void setMaterialDesc(String _materialDesc) {
		this._materialDesc = _materialDesc;
	}

	public String getQuantity() {
		return _quantity;
	}

	public void setQuantity(String _quantity) {
		this._quantity = _quantity;
	}

	public String getNetPrice() {
		return _netPrice;
	}

	public void setNetPrice(String _netPrice) {
		this._netPrice = _netPrice;
	}

	public String getCurrency() {
		return _currency;
	}

	public void setCurrency(String _currency) {
		this._currency = _currency;
	}

	public Date getDeliveryDate() {
		return _deliveryDate;
	}

	public void setDeliveryDate(Date _deliveryDate) {
		this._deliveryDate = _deliveryDate;
	}

	public String getDeliveryAddress() {
		return _deliveryAddress;
	}

	public void setDeliveryAddress(String _deliveryAddress) {
		this._deliveryAddress = _deliveryAddress;
	}

	public String getItemText() {
		return _itemText;
	}

	public void setItemText(String _itemText) {
		this._itemText = _itemText;
	}

	public String getLocationEvo() {
		return _locationEvo;
	}

	public void setLocationEvo(String _locationEvo) {
		this._locationEvo = _locationEvo;
	}

	public String getTargaTecnica() {
		return _targaTecnica;
	}

	public void setTargaTecnica(String _targaTecnica) {
		this._targaTecnica = _targaTecnica;
	}

	public String getUser_() {
		return _user_;
	}

	public void setUser_(String _user_) {
		this._user_ = _user_;
	}
	
	public String getVendorCode() {
		return _vendorCode;
	}

	public void setVendorCode(String _vendorCode) {
		this._vendorCode = _vendorCode;
	}
	
	public long getCatalogoId() {
		return _catalogoId;
	}

	public void setCatalogoId(long _catalogoId) {
		this._catalogoId = _catalogoId;
	}
	
}