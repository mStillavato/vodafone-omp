package com.hp.omp.model;

import org.apache.poi.ss.usermodel.Workbook;

public interface ExcelReport {
	
	void addExcelTable(ExcelTable excelTable, int order);
	
	ExcelStyler getStyler();
	
	void setStyler(ExcelStyler styler);
	
	Workbook createExelWorkBook();
	
	Workbook getWorkBook();
	
	int getTotalRowsProcessed();

}
