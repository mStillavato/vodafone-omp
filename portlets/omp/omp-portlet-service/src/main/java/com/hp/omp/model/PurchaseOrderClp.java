package com.hp.omp.model;

import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class PurchaseOrderClp extends BaseModelImpl<PurchaseOrder>
    implements PurchaseOrder {
    private long _purchaseOrderId;
    private String _costCenterId;
    private String _vendorId;
    private String _buyer;
    private String _ola;
    private String _projectId;
    private String _subProjectId;
    private String _budgetCategoryId;
    private String _budgetSubCategoryId;
    private String _activityDescription;
    private String _macroDriverId;
    private String _microDriverId;
    private String _odnpCodeId;
    private String _odnpNameId;
    private String _totalValue;
    private String _currency;
    private String _receiverUserId;
    private String _fiscalYear;
    private String _screenNameRequester;
    private String _screenNameReciever;
    private boolean _automatic;
    private int _status;
    private Date _createdDate;
    private long _createdUserId;
    private String _createdUserUuid;
    private String _wbsCodeId;
    private String _trackingCode;
    private String _buyerId;
    private BaseModel<?> _purchaseOrderRemoteModel;
    private String _targaTecnica;
    private String _evoLocationId;
    private int _tipoPr;
    private String _plant;    

    public PurchaseOrderClp() {
    }

    public Class<?> getModelClass() {
        return PurchaseOrder.class;
    }

    public String getModelClassName() {
        return PurchaseOrder.class.getName();
    }

    public long getPrimaryKey() {
        return _purchaseOrderId;
    }

    public void setPrimaryKey(long primaryKey) {
        setPurchaseOrderId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_purchaseOrderId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("purchaseOrderId", getPurchaseOrderId());
        attributes.put("costCenterId", getCostCenterId());
        attributes.put("vendorId", getVendorId());
        attributes.put("buyer", getBuyer());
        attributes.put("ola", getOla());
        attributes.put("projectId", getProjectId());
        attributes.put("subProjectId", getSubProjectId());
        attributes.put("budgetCategoryId", getBudgetCategoryId());
        attributes.put("budgetSubCategoryId", getBudgetSubCategoryId());
        attributes.put("activityDescription", getActivityDescription());
        attributes.put("macroDriverId", getMacroDriverId());
        attributes.put("microDriverId", getMicroDriverId());
        attributes.put("odnpCodeId", getOdnpCodeId());
        attributes.put("odnpNameId", getOdnpNameId());
        attributes.put("totalValue", getTotalValue());
        attributes.put("currency", getCurrency());
        attributes.put("receiverUserId", getReceiverUserId());
        attributes.put("fiscalYear", getFiscalYear());
        attributes.put("screenNameRequester", getScreenNameRequester());
        attributes.put("screenNameReciever", getScreenNameReciever());
        attributes.put("automatic", getAutomatic());
        attributes.put("status", getStatus());
        attributes.put("createdDate", getCreatedDate());
        attributes.put("createdUserId", getCreatedUserId());
        attributes.put("wbsCodeId", getWbsCodeId());
        attributes.put("trackingCode", getTrackingCode());
        attributes.put("buyerId", getBuyerId());
        attributes.put("targaTecnica", getTargaTecnica());
        attributes.put("evoLocationId", getEvoLocationId());
        attributes.put("tipoPr", getTipoPr());
        attributes.put("plant", getPlant());
        
        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long purchaseOrderId = (Long) attributes.get("purchaseOrderId");

        if (purchaseOrderId != null) {
            setPurchaseOrderId(purchaseOrderId);
        }

        String costCenterId = (String) attributes.get("costCenterId");

        if (costCenterId != null) {
            setCostCenterId(costCenterId);
        }

        String vendorId = (String) attributes.get("vendorId");

        if (vendorId != null) {
            setVendorId(vendorId);
        }

        String buyer = (String) attributes.get("buyer");

        if (buyer != null) {
            setBuyer(buyer);
        }

        String ola = (String) attributes.get("ola");

        if (ola != null) {
            setOla(ola);
        }

        String projectId = (String) attributes.get("projectId");

        if (projectId != null) {
            setProjectId(projectId);
        }

        String subProjectId = (String) attributes.get("subProjectId");

        if (subProjectId != null) {
            setSubProjectId(subProjectId);
        }

        String budgetCategoryId = (String) attributes.get("budgetCategoryId");

        if (budgetCategoryId != null) {
            setBudgetCategoryId(budgetCategoryId);
        }

        String budgetSubCategoryId = (String) attributes.get(
                "budgetSubCategoryId");

        if (budgetSubCategoryId != null) {
            setBudgetSubCategoryId(budgetSubCategoryId);
        }

        String activityDescription = (String) attributes.get(
                "activityDescription");

        if (activityDescription != null) {
            setActivityDescription(activityDescription);
        }

        String macroDriverId = (String) attributes.get("macroDriverId");

        if (macroDriverId != null) {
            setMacroDriverId(macroDriverId);
        }

        String microDriverId = (String) attributes.get("microDriverId");

        if (microDriverId != null) {
            setMicroDriverId(microDriverId);
        }

        String odnpCodeId = (String) attributes.get("odnpCodeId");

        if (odnpCodeId != null) {
            setOdnpCodeId(odnpCodeId);
        }

        String odnpNameId = (String) attributes.get("odnpNameId");

        if (odnpNameId != null) {
            setOdnpNameId(odnpNameId);
        }

        String totalValue = (String) attributes.get("totalValue");

        if (totalValue != null) {
            setTotalValue(totalValue);
        }

        String currency = (String) attributes.get("currency");

        if (currency != null) {
            setCurrency(currency);
        }

        String receiverUserId = (String) attributes.get("receiverUserId");

        if (receiverUserId != null) {
            setReceiverUserId(receiverUserId);
        }

        String fiscalYear = (String) attributes.get("fiscalYear");

        if (fiscalYear != null) {
            setFiscalYear(fiscalYear);
        }

        String screenNameRequester = (String) attributes.get(
                "screenNameRequester");

        if (screenNameRequester != null) {
            setScreenNameRequester(screenNameRequester);
        }

        String screenNameReciever = (String) attributes.get(
                "screenNameReciever");

        if (screenNameReciever != null) {
            setScreenNameReciever(screenNameReciever);
        }

        Boolean automatic = (Boolean) attributes.get("automatic");

        if (automatic != null) {
            setAutomatic(automatic);
        }

        Integer status = (Integer) attributes.get("status");

        if (status != null) {
            setStatus(status);
        }

        Date createdDate = (Date) attributes.get("createdDate");

        if (createdDate != null) {
            setCreatedDate(createdDate);
        }

        Long createdUserId = (Long) attributes.get("createdUserId");

        if (createdUserId != null) {
            setCreatedUserId(createdUserId);
        }

        String wbsCodeId = (String) attributes.get("wbsCodeId");

        if (wbsCodeId != null) {
            setWbsCodeId(wbsCodeId);
        }

        String trackingCode = (String) attributes.get("trackingCode");

        if (trackingCode != null) {
            setTrackingCode(trackingCode);
        }

        String buyerId = (String) attributes.get("buyerId");

        if (buyerId != null) {
            setBuyerId(buyerId);
        }
        
        String targaTecnica = (String) attributes.get("targaTecnica");

        if (targaTecnica != null) {
            setTargaTecnica(targaTecnica);
        }
        
        String evoLocationId = (String) attributes.get("evoLocationId");

        if (evoLocationId != null) {
        	setEvoLocationId(evoLocationId);
        }
        
        Integer tipoPr = (Integer) attributes.get("tipoPr");

        if (tipoPr != null) {
            setTipoPr(tipoPr);
        }
        
        String plant = (String) attributes.get("plant");

        if (plant != null) {
        	setPlant(plant);
        }        
    }

    public long getPurchaseOrderId() {
        return _purchaseOrderId;
    }

    public void setPurchaseOrderId(long purchaseOrderId) {
        _purchaseOrderId = purchaseOrderId;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setPurchaseOrderId", long.class);

                method.invoke(_purchaseOrderRemoteModel, purchaseOrderId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getCostCenterId() {
        return _costCenterId;
    }

    public void setCostCenterId(String costCenterId) {
        _costCenterId = costCenterId;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setCostCenterId", String.class);

                method.invoke(_purchaseOrderRemoteModel, costCenterId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getVendorId() {
        return _vendorId;
    }

    public void setVendorId(String vendorId) {
        _vendorId = vendorId;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setVendorId", String.class);

                method.invoke(_purchaseOrderRemoteModel, vendorId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getBuyer() {
        return _buyer;
    }

    public void setBuyer(String buyer) {
        _buyer = buyer;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setBuyer", String.class);

                method.invoke(_purchaseOrderRemoteModel, buyer);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getOla() {
        return _ola;
    }

    public void setOla(String ola) {
        _ola = ola;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setOla", String.class);

                method.invoke(_purchaseOrderRemoteModel, ola);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getProjectId() {
        return _projectId;
    }

    public void setProjectId(String projectId) {
        _projectId = projectId;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setProjectId", String.class);

                method.invoke(_purchaseOrderRemoteModel, projectId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getSubProjectId() {
        return _subProjectId;
    }

    public void setSubProjectId(String subProjectId) {
        _subProjectId = subProjectId;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setSubProjectId", String.class);

                method.invoke(_purchaseOrderRemoteModel, subProjectId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getBudgetCategoryId() {
        return _budgetCategoryId;
    }

    public void setBudgetCategoryId(String budgetCategoryId) {
        _budgetCategoryId = budgetCategoryId;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setBudgetCategoryId",
                        String.class);

                method.invoke(_purchaseOrderRemoteModel, budgetCategoryId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getBudgetSubCategoryId() {
        return _budgetSubCategoryId;
    }

    public void setBudgetSubCategoryId(String budgetSubCategoryId) {
        _budgetSubCategoryId = budgetSubCategoryId;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setBudgetSubCategoryId",
                        String.class);

                method.invoke(_purchaseOrderRemoteModel, budgetSubCategoryId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getActivityDescription() {
        return _activityDescription;
    }

    public void setActivityDescription(String activityDescription) {
        _activityDescription = activityDescription;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setActivityDescription",
                        String.class);

                method.invoke(_purchaseOrderRemoteModel, activityDescription);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getMacroDriverId() {
        return _macroDriverId;
    }

    public void setMacroDriverId(String macroDriverId) {
        _macroDriverId = macroDriverId;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setMacroDriverId", String.class);

                method.invoke(_purchaseOrderRemoteModel, macroDriverId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getMicroDriverId() {
        return _microDriverId;
    }

    public void setMicroDriverId(String microDriverId) {
        _microDriverId = microDriverId;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setMicroDriverId", String.class);

                method.invoke(_purchaseOrderRemoteModel, microDriverId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getOdnpCodeId() {
        return _odnpCodeId;
    }

    public void setOdnpCodeId(String odnpCodeId) {
        _odnpCodeId = odnpCodeId;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setOdnpCodeId", String.class);

                method.invoke(_purchaseOrderRemoteModel, odnpCodeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getOdnpNameId() {
        return _odnpNameId;
    }

    public void setOdnpNameId(String odnpNameId) {
        _odnpNameId = odnpNameId;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setOdnpNameId", String.class);

                method.invoke(_purchaseOrderRemoteModel, odnpNameId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getTotalValue() {
        return _totalValue;
    }

    public void setTotalValue(String totalValue) {
        _totalValue = totalValue;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setTotalValue", String.class);

                method.invoke(_purchaseOrderRemoteModel, totalValue);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getCurrency() {
        return _currency;
    }

    public void setCurrency(String currency) {
        _currency = currency;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setCurrency", String.class);

                method.invoke(_purchaseOrderRemoteModel, currency);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getReceiverUserId() {
        return _receiverUserId;
    }

    public void setReceiverUserId(String receiverUserId) {
        _receiverUserId = receiverUserId;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setReceiverUserId",
                        String.class);

                method.invoke(_purchaseOrderRemoteModel, receiverUserId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getFiscalYear() {
        return _fiscalYear;
    }

    public void setFiscalYear(String fiscalYear) {
        _fiscalYear = fiscalYear;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setFiscalYear", String.class);

                method.invoke(_purchaseOrderRemoteModel, fiscalYear);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getScreenNameRequester() {
        return _screenNameRequester;
    }

    public void setScreenNameRequester(String screenNameRequester) {
        _screenNameRequester = screenNameRequester;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setScreenNameRequester",
                        String.class);

                method.invoke(_purchaseOrderRemoteModel, screenNameRequester);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getScreenNameReciever() {
        return _screenNameReciever;
    }

    public void setScreenNameReciever(String screenNameReciever) {
        _screenNameReciever = screenNameReciever;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setScreenNameReciever",
                        String.class);

                method.invoke(_purchaseOrderRemoteModel, screenNameReciever);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public boolean getAutomatic() {
        return _automatic;
    }

    public boolean isAutomatic() {
        return _automatic;
    }

    public void setAutomatic(boolean automatic) {
        _automatic = automatic;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setAutomatic", boolean.class);

                method.invoke(_purchaseOrderRemoteModel, automatic);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public int getStatus() {
        return _status;
    }

    public void setStatus(int status) {
        _status = status;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setStatus", int.class);

                method.invoke(_purchaseOrderRemoteModel, status);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public Date getCreatedDate() {
        return _createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        _createdDate = createdDate;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setCreatedDate", Date.class);

                method.invoke(_purchaseOrderRemoteModel, createdDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public long getCreatedUserId() {
        return _createdUserId;
    }

    public void setCreatedUserId(long createdUserId) {
        _createdUserId = createdUserId;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setCreatedUserId", long.class);

                method.invoke(_purchaseOrderRemoteModel, createdUserId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getCreatedUserUuid() throws SystemException {
        return PortalUtil.getUserValue(getCreatedUserId(), "uuid",
            _createdUserUuid);
    }

    public void setCreatedUserUuid(String createdUserUuid) {
        _createdUserUuid = createdUserUuid;
    }

    public String getWbsCodeId() {
        return _wbsCodeId;
    }

    public void setWbsCodeId(String wbsCodeId) {
        _wbsCodeId = wbsCodeId;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setWbsCodeId", String.class);

                method.invoke(_purchaseOrderRemoteModel, wbsCodeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getTrackingCode() {
        return _trackingCode;
    }

    public void setTrackingCode(String trackingCode) {
        _trackingCode = trackingCode;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setTrackingCode", String.class);

                method.invoke(_purchaseOrderRemoteModel, trackingCode);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getBuyerId() {
        return _buyerId;
    }

    public void setBuyerId(String buyerId) {
        _buyerId = buyerId;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setBuyerId", String.class);

                method.invoke(_purchaseOrderRemoteModel, buyerId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public com.hp.omp.model.custom.PurchaseOrderStatus getPurchaseOrderStatus() {
        try {
            String methodName = "getPurchaseOrderStatus";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            com.hp.omp.model.custom.PurchaseOrderStatus returnObj = (com.hp.omp.model.custom.PurchaseOrderStatus) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getTxtBoxStatus() {
        try {
            String methodName = "getTxtBoxStatus";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public boolean getAllowDelete() {
        try {
            String methodName = "getAllowDelete";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            Boolean returnObj = (Boolean) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public void setTrCodeFieldsStatus(java.lang.String trCodeFieldsStatus) {
        try {
            String methodName = "setTrCodeFieldsStatus";

            Class<?>[] parameterTypes = new Class<?>[] { java.lang.String.class };

            Object[] parameterValues = new Object[] { trCodeFieldsStatus };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public boolean getAllowAdd() {
        try {
            String methodName = "getAllowAdd";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            Boolean returnObj = (Boolean) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getFieldStatus() {
        try {
            String methodName = "getFieldStatus";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public boolean getAllowUpdate() {
        try {
            String methodName = "getAllowUpdate";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            Boolean returnObj = (Boolean) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public void setFieldStatus(java.lang.String fieldStatus) {
        try {
            String methodName = "setFieldStatus";

            Class<?>[] parameterTypes = new Class<?>[] { java.lang.String.class };

            Object[] parameterValues = new Object[] { fieldStatus };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getTrCodeFieldsStatus() {
        try {
            String methodName = "getTrCodeFieldsStatus";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public BaseModel<?> getPurchaseOrderRemoteModel() {
        return _purchaseOrderRemoteModel;
    }

    public void setPurchaseOrderRemoteModel(
        BaseModel<?> purchaseOrderRemoteModel) {
        _purchaseOrderRemoteModel = purchaseOrderRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _purchaseOrderRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_purchaseOrderRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            PurchaseOrderLocalServiceUtil.addPurchaseOrder(this);
        } else {
            PurchaseOrderLocalServiceUtil.updatePurchaseOrder(this);
        }
    }

    @Override
    public PurchaseOrder toEscapedModel() {
        return (PurchaseOrder) ProxyUtil.newProxyInstance(PurchaseOrder.class.getClassLoader(),
            new Class[] { PurchaseOrder.class }, new AutoEscapeBeanHandler(this));
    }

    public PurchaseOrder toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        PurchaseOrderClp clone = new PurchaseOrderClp();

        clone.setPurchaseOrderId(getPurchaseOrderId());
        clone.setCostCenterId(getCostCenterId());
        clone.setVendorId(getVendorId());
        clone.setBuyer(getBuyer());
        clone.setOla(getOla());
        clone.setProjectId(getProjectId());
        clone.setSubProjectId(getSubProjectId());
        clone.setBudgetCategoryId(getBudgetCategoryId());
        clone.setBudgetSubCategoryId(getBudgetSubCategoryId());
        clone.setActivityDescription(getActivityDescription());
        clone.setMacroDriverId(getMacroDriverId());
        clone.setMicroDriverId(getMicroDriverId());
        clone.setOdnpCodeId(getOdnpCodeId());
        clone.setOdnpNameId(getOdnpNameId());
        clone.setTotalValue(getTotalValue());
        clone.setCurrency(getCurrency());
        clone.setReceiverUserId(getReceiverUserId());
        clone.setFiscalYear(getFiscalYear());
        clone.setScreenNameRequester(getScreenNameRequester());
        clone.setScreenNameReciever(getScreenNameReciever());
        clone.setAutomatic(getAutomatic());
        clone.setStatus(getStatus());
        clone.setCreatedDate(getCreatedDate());
        clone.setCreatedUserId(getCreatedUserId());
        clone.setWbsCodeId(getWbsCodeId());
        clone.setTrackingCode(getTrackingCode());
        clone.setBuyerId(getBuyerId());
        clone.setTargaTecnica(getTargaTecnica());
        clone.setEvoLocationId(getEvoLocationId());
        clone.setTipoPr(getTipoPr());
        clone.setPlant(getPlant());

        return clone;
    }

    public int compareTo(PurchaseOrder purchaseOrder) {
        int value = 0;

        if (getPurchaseOrderId() < purchaseOrder.getPurchaseOrderId()) {
            value = -1;
        } else if (getPurchaseOrderId() > purchaseOrder.getPurchaseOrderId()) {
            value = 1;
        } else {
            value = 0;
        }

        value = value * -1;

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PurchaseOrderClp)) {
            return false;
        }

        PurchaseOrderClp purchaseOrder = (PurchaseOrderClp) obj;

        long primaryKey = purchaseOrder.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(55);

        sb.append("{purchaseOrderId=");
        sb.append(getPurchaseOrderId());
        sb.append(", costCenterId=");
        sb.append(getCostCenterId());
        sb.append(", vendorId=");
        sb.append(getVendorId());
        sb.append(", buyer=");
        sb.append(getBuyer());
        sb.append(", ola=");
        sb.append(getOla());
        sb.append(", projectId=");
        sb.append(getProjectId());
        sb.append(", subProjectId=");
        sb.append(getSubProjectId());
        sb.append(", budgetCategoryId=");
        sb.append(getBudgetCategoryId());
        sb.append(", budgetSubCategoryId=");
        sb.append(getBudgetSubCategoryId());
        sb.append(", activityDescription=");
        sb.append(getActivityDescription());
        sb.append(", macroDriverId=");
        sb.append(getMacroDriverId());
        sb.append(", microDriverId=");
        sb.append(getMicroDriverId());
        sb.append(", odnpCodeId=");
        sb.append(getOdnpCodeId());
        sb.append(", odnpNameId=");
        sb.append(getOdnpNameId());
        sb.append(", totalValue=");
        sb.append(getTotalValue());
        sb.append(", currency=");
        sb.append(getCurrency());
        sb.append(", receiverUserId=");
        sb.append(getReceiverUserId());
        sb.append(", fiscalYear=");
        sb.append(getFiscalYear());
        sb.append(", screenNameRequester=");
        sb.append(getScreenNameRequester());
        sb.append(", screenNameReciever=");
        sb.append(getScreenNameReciever());
        sb.append(", automatic=");
        sb.append(getAutomatic());
        sb.append(", status=");
        sb.append(getStatus());
        sb.append(", createdDate=");
        sb.append(getCreatedDate());
        sb.append(", createdUserId=");
        sb.append(getCreatedUserId());
        sb.append(", wbsCodeId=");
        sb.append(getWbsCodeId());
        sb.append(", trackingCode=");
        sb.append(getTrackingCode());
        sb.append(", buyerId=");
        sb.append(getBuyerId());
        sb.append(", targaTecnica=");
        sb.append(getTargaTecnica());
        sb.append(", evoLocationId=");
        sb.append(getEvoLocationId());
        sb.append(", tipoPr=");
        sb.append(getTipoPr());
        sb.append(", plant=");
        sb.append(getPlant());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(85);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.PurchaseOrder");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>purchaseOrderId</column-name><column-value><![CDATA[");
        sb.append(getPurchaseOrderId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>costCenterId</column-name><column-value><![CDATA[");
        sb.append(getCostCenterId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>vendorId</column-name><column-value><![CDATA[");
        sb.append(getVendorId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>buyer</column-name><column-value><![CDATA[");
        sb.append(getBuyer());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>ola</column-name><column-value><![CDATA[");
        sb.append(getOla());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>projectId</column-name><column-value><![CDATA[");
        sb.append(getProjectId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>subProjectId</column-name><column-value><![CDATA[");
        sb.append(getSubProjectId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>budgetCategoryId</column-name><column-value><![CDATA[");
        sb.append(getBudgetCategoryId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>budgetSubCategoryId</column-name><column-value><![CDATA[");
        sb.append(getBudgetSubCategoryId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>activityDescription</column-name><column-value><![CDATA[");
        sb.append(getActivityDescription());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>macroDriverId</column-name><column-value><![CDATA[");
        sb.append(getMacroDriverId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>microDriverId</column-name><column-value><![CDATA[");
        sb.append(getMicroDriverId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>odnpCodeId</column-name><column-value><![CDATA[");
        sb.append(getOdnpCodeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>odnpNameId</column-name><column-value><![CDATA[");
        sb.append(getOdnpNameId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>totalValue</column-name><column-value><![CDATA[");
        sb.append(getTotalValue());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>currency</column-name><column-value><![CDATA[");
        sb.append(getCurrency());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>receiverUserId</column-name><column-value><![CDATA[");
        sb.append(getReceiverUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>fiscalYear</column-name><column-value><![CDATA[");
        sb.append(getFiscalYear());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>screenNameRequester</column-name><column-value><![CDATA[");
        sb.append(getScreenNameRequester());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>screenNameReciever</column-name><column-value><![CDATA[");
        sb.append(getScreenNameReciever());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>automatic</column-name><column-value><![CDATA[");
        sb.append(getAutomatic());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>status</column-name><column-value><![CDATA[");
        sb.append(getStatus());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>createdDate</column-name><column-value><![CDATA[");
        sb.append(getCreatedDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>createdUserId</column-name><column-value><![CDATA[");
        sb.append(getCreatedUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>wbsCodeId</column-name><column-value><![CDATA[");
        sb.append(getWbsCodeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>trackingCode</column-name><column-value><![CDATA[");
        sb.append(getTrackingCode());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>buyerId</column-name><column-value><![CDATA[");
        sb.append(getBuyerId());
        sb.append("]]></column-value></column>");
        sb.append(
                "<column><column-name>targaTecnica</column-name><column-value><![CDATA[");
        sb.append(getTargaTecnica());
        sb.append("]]></column-value></column>");

        sb.append(
                "<column><column-name>evoLocationId</column-name><column-value><![CDATA[");
        sb.append(getEvoLocationId());
        sb.append("]]></column-value></column>");
 
        sb.append(
                "<column><column-name>tipoPr</column-name><column-value><![CDATA[");
        sb.append(getTipoPr());
        sb.append("]]></column-value></column>");
        
        sb.append(
                "<column><column-name>plant</column-name><column-value><![CDATA[");
        sb.append(getPlant());
        sb.append("]]></column-value></column>"); 
        
        sb.append("</model>");

        return sb.toString();
    }
    
    public String getTargaTecnica() {
        return _targaTecnica;
    }

    public void setTargaTecnica(String targaTecnica) {
    	_targaTecnica = targaTecnica;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setTargaTecnica", String.class);

                method.invoke(_purchaseOrderRemoteModel, targaTecnica);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }
    
    public String getEvoLocationId() {
        return _evoLocationId;
    }

    public void setEvoLocationId(String evoLocationId) {
    	_evoLocationId = evoLocationId;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setEvoLocationId", String.class);

                method.invoke(_purchaseOrderRemoteModel, evoLocationId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }
    
    public String getPlant() {
        return _plant;
    }

    public void setPlant(String plant) {
    	_plant = plant;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setPlant", String.class);

                method.invoke(_purchaseOrderRemoteModel, plant);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }
    
    public int getTipoPr() {
        return _tipoPr;
    }

    public void setTipoPr(int tipoPr) {
        _tipoPr = tipoPr;

        if (_purchaseOrderRemoteModel != null) {
            try {
                Class<?> clazz = _purchaseOrderRemoteModel.getClass();

                Method method = clazz.getMethod("setTipoPr", int.class);

                method.invoke(_purchaseOrderRemoteModel, tipoPr);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

	public boolean isHasFileGR() {
        try {
            String methodName = "isHasFileGR";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            Boolean returnObj = (Boolean) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
	}

}
