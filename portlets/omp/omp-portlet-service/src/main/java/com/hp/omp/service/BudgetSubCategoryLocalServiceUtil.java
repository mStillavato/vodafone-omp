package com.hp.omp.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the budget sub category local service. This utility wraps {@link com.hp.omp.service.impl.BudgetSubCategoryLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see BudgetSubCategoryLocalService
 * @see com.hp.omp.service.base.BudgetSubCategoryLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.BudgetSubCategoryLocalServiceImpl
 * @generated
 */
public class BudgetSubCategoryLocalServiceUtil {
    private static BudgetSubCategoryLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.hp.omp.service.impl.BudgetSubCategoryLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the budget sub category to the database. Also notifies the appropriate model listeners.
    *
    * @param budgetSubCategory the budget sub category
    * @return the budget sub category that was added
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.BudgetSubCategory addBudgetSubCategory(
        com.hp.omp.model.BudgetSubCategory budgetSubCategory)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addBudgetSubCategory(budgetSubCategory);
    }

    /**
    * Creates a new budget sub category with the primary key. Does not add the budget sub category to the database.
    *
    * @param budgetSubCategoryId the primary key for the new budget sub category
    * @return the new budget sub category
    */
    public static com.hp.omp.model.BudgetSubCategory createBudgetSubCategory(
        long budgetSubCategoryId) {
        return getService().createBudgetSubCategory(budgetSubCategoryId);
    }

    /**
    * Deletes the budget sub category with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param budgetSubCategoryId the primary key of the budget sub category
    * @return the budget sub category that was removed
    * @throws PortalException if a budget sub category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.BudgetSubCategory deleteBudgetSubCategory(
        long budgetSubCategoryId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteBudgetSubCategory(budgetSubCategoryId);
    }

    /**
    * Deletes the budget sub category from the database. Also notifies the appropriate model listeners.
    *
    * @param budgetSubCategory the budget sub category
    * @return the budget sub category that was removed
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.BudgetSubCategory deleteBudgetSubCategory(
        com.hp.omp.model.BudgetSubCategory budgetSubCategory)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteBudgetSubCategory(budgetSubCategory);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    public static com.hp.omp.model.BudgetSubCategory fetchBudgetSubCategory(
        long budgetSubCategoryId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchBudgetSubCategory(budgetSubCategoryId);
    }

    /**
    * Returns the budget sub category with the primary key.
    *
    * @param budgetSubCategoryId the primary key of the budget sub category
    * @return the budget sub category
    * @throws PortalException if a budget sub category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.BudgetSubCategory getBudgetSubCategory(
        long budgetSubCategoryId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getBudgetSubCategory(budgetSubCategoryId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the budget sub categories.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of budget sub categories
    * @param end the upper bound of the range of budget sub categories (not inclusive)
    * @return the range of budget sub categories
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.BudgetSubCategory> getBudgetSubCategories(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getBudgetSubCategories(start, end);
    }

    /**
    * Returns the number of budget sub categories.
    *
    * @return the number of budget sub categories
    * @throws SystemException if a system exception occurred
    */
    public static int getBudgetSubCategoriesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getBudgetSubCategoriesCount();
    }

    /**
    * Updates the budget sub category in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param budgetSubCategory the budget sub category
    * @return the budget sub category that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.BudgetSubCategory updateBudgetSubCategory(
        com.hp.omp.model.BudgetSubCategory budgetSubCategory)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateBudgetSubCategory(budgetSubCategory);
    }

    /**
    * Updates the budget sub category in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param budgetSubCategory the budget sub category
    * @param merge whether to merge the budget sub category with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the budget sub category that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.BudgetSubCategory updateBudgetSubCategory(
        com.hp.omp.model.BudgetSubCategory budgetSubCategory, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateBudgetSubCategory(budgetSubCategory, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static com.hp.omp.model.BudgetSubCategory getBudgetSubCategoryByName(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getBudgetSubCategoryByName(name);
    }
    
    public static java.util.List<com.hp.omp.model.BudgetSubCategory> getBudgetSubCategoryList(
            int start, int end, String searchFilter)
            throws com.liferay.portal.kernel.exception.SystemException {
            return getService().getBudgetSubCategoryList(start, end, searchFilter);
        }

    public static void clearService() {
        _service = null;
    }

    public static BudgetSubCategoryLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    BudgetSubCategoryLocalService.class.getName());

            if (invokableLocalService instanceof BudgetSubCategoryLocalService) {
                _service = (BudgetSubCategoryLocalService) invokableLocalService;
            } else {
                _service = new BudgetSubCategoryLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(BudgetSubCategoryLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated
     */
    public void setService(BudgetSubCategoryLocalService service) {
    }
}
