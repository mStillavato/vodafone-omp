package com.hp.omp.service.persistence;

import java.util.List;

import com.hp.omp.model.Catalogo;
import com.hp.omp.model.custom.CatalogoExportListDTO;

public interface CatalogoFinder {
    
    public java.util.List<com.hp.omp.model.custom.CatalogoListDTO> getCatalogoList(
    		com.hp.omp.model.custom.SearchCatalogoDTO searchDTO);
    
    public java.lang.Long getCatalogoListCount(
    		com.hp.omp.model.custom.SearchCatalogoDTO searchDTO);
    
    public java.util.List<com.hp.omp.model.Catalogo> getCatalogoForVendor(
    		int start, int end, String vendorCode)
            throws com.liferay.portal.kernel.exception.SystemException;
    
    public List<CatalogoExportListDTO> getExportCatalogoList(
    		com.hp.omp.model.custom.SearchCatalogoDTO searchDTO);
}
