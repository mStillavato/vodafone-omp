package com.hp.omp.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.BaseLocalService;
import com.liferay.portal.service.InvokableLocalService;
import com.liferay.portal.service.PersistedModelLocalService;

/**
 * The interface for the purchase order local service.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see PurchaseOrderLocalServiceUtil
 * @see com.hp.omp.service.base.PurchaseOrderLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.PurchaseOrderLocalServiceImpl
 * @generated
 */
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
    PortalException.class, SystemException.class}
)
public interface PurchaseOrderLocalService extends BaseLocalService,
    InvokableLocalService, PersistedModelLocalService {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link PurchaseOrderLocalServiceUtil} to access the purchase order local service. Add custom service methods to {@link com.hp.omp.service.impl.PurchaseOrderLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */

    /**
    * Adds the purchase order to the database. Also notifies the appropriate model listeners.
    *
    * @param purchaseOrder the purchase order
    * @return the purchase order that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseOrder addPurchaseOrder(
        com.hp.omp.model.PurchaseOrder purchaseOrder)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Creates a new purchase order with the primary key. Does not add the purchase order to the database.
    *
    * @param purchaseOrderId the primary key for the new purchase order
    * @return the new purchase order
    */
    public com.hp.omp.model.PurchaseOrder createPurchaseOrder(
        long purchaseOrderId);

    /**
    * Deletes the purchase order with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param purchaseOrderId the primary key of the purchase order
    * @return the purchase order that was removed
    * @throws PortalException if a purchase order with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseOrder deletePurchaseOrder(
        long purchaseOrderId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Deletes the purchase order from the database. Also notifies the appropriate model listeners.
    *
    * @param purchaseOrder the purchase order
    * @return the purchase order that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseOrder deletePurchaseOrder(
        com.hp.omp.model.PurchaseOrder purchaseOrder)
        throws com.liferay.portal.kernel.exception.SystemException;

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery();

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public com.hp.omp.model.PurchaseOrder fetchPurchaseOrder(
        long purchaseOrderId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the purchase order with the primary key.
    *
    * @param purchaseOrderId the primary key of the purchase order
    * @return the purchase order
    * @throws PortalException if a purchase order with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public com.hp.omp.model.PurchaseOrder getPurchaseOrder(long purchaseOrderId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the purchase orders.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of purchase orders
    * @param end the upper bound of the range of purchase orders (not inclusive)
    * @return the range of purchase orders
    * @throws SystemException if a system exception occurred
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrders(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of purchase orders.
    *
    * @return the number of purchase orders
    * @throws SystemException if a system exception occurred
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getPurchaseOrdersCount()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Updates the purchase order in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param purchaseOrder the purchase order
    * @return the purchase order that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseOrder updatePurchaseOrder(
        com.hp.omp.model.PurchaseOrder purchaseOrder)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Updates the purchase order in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param purchaseOrder the purchase order
    * @param merge whether to merge the purchase order with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the purchase order that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseOrder updatePurchaseOrder(
        com.hp.omp.model.PurchaseOrder purchaseOrder, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier();

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier);

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrdersListByStatus(
        int status, int start, int end, java.lang.String orderby,
        java.lang.String order) throws java.lang.Exception;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.lang.Long getPurchaseOrdersCountByStatus(int status)
        throws java.lang.Exception;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public long getPurchaseOrdersGRRequestedCount(
        com.hp.omp.model.custom.SearchDTO searchDto);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.hp.omp.model.custom.PoListDTO> getPurchaseOrdersGRRequested(
        com.hp.omp.model.custom.SearchDTO searchDto);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public long getPurchaseOrdersGRClosedCount(
        com.hp.omp.model.custom.SearchDTO searchDto);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.hp.omp.model.custom.PoListDTO> getPurchaseOrdersGRClosed(
        com.hp.omp.model.custom.SearchDTO searchDto);

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrdersListByStatusAndCostCenter(
        int status, long costCenterId, int start, int end,
        java.lang.String orderby, java.lang.String order)
        throws java.lang.Exception;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.lang.Long getPurchaseOrdersCountByStatusAndCostCenter(
        int status, long costCenterId) throws java.lang.Exception;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getTotalNumberOfPurchaseOrdersByStatus(int status)
        throws java.lang.Exception;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrderByProjectId(
        java.lang.String projectId) throws java.lang.Exception;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrderBySubProjectId(
        java.lang.String subProjectId) throws java.lang.Exception;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrderByVendorId(
        java.lang.String vendorId) throws java.lang.Exception;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrderByBudgetCategoryId(
        java.lang.String budgetCategoryId) throws java.lang.Exception;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrderByBudgetSubCategoryId(
        java.lang.String budgetSubCategoryId) throws java.lang.Exception;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrderByOdnpNameId(
        java.lang.String odnpNameId) throws java.lang.Exception;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrderByMacroDriverId(
        java.lang.String macroDriverId) throws java.lang.Exception;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrderByMicroDriverId(
        java.lang.String microDriverId) throws java.lang.Exception;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<java.lang.Long> getPONumbers()
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<java.lang.String> getAllPOProjects()
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<java.lang.String> getSubProjects(
        java.lang.String projectName)
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.hp.omp.model.custom.PoListDTO> getAdvancedSearchResults(
        com.hp.omp.model.custom.SearchDTO searchDTO, int start, int end,
        java.lang.String orderby, java.lang.String order)
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.hp.omp.model.custom.PoListDTO> getAdvancedSearchGrRequestedResults(
        com.hp.omp.model.custom.SearchDTO searchDTO, int start, int end,
        java.lang.String orderby, java.lang.String order)
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.lang.Long getAdvancedSearchResultsCount(
        com.hp.omp.model.custom.SearchDTO searchDTO)
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.lang.Long getAdvancedSearchGrRequestedResultsCount(
        com.hp.omp.model.custom.SearchDTO searchDTO)
        throws com.liferay.portal.kernel.exception.SystemException;
}
