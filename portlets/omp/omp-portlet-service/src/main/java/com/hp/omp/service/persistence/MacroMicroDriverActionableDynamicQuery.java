package com.hp.omp.service.persistence;

import com.hp.omp.model.MacroMicroDriver;
import com.hp.omp.service.MacroMicroDriverLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class MacroMicroDriverActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public MacroMicroDriverActionableDynamicQuery() throws SystemException {
        setBaseLocalService(MacroMicroDriverLocalServiceUtil.getService());
        setClass(MacroMicroDriver.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("macroMicroDriverId");
    }
}
