package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class LocationSoap implements Serializable {
    private long _locationTargaId;
    private String _codiceOffice;
    private String _targaTecnicaSap;
    private String _location;
    private String _pr;
    private Date _dataOnAir;

    public LocationSoap() {
    }

    public static LocationSoap toSoapModel(Location model) {
        LocationSoap soapModel = new LocationSoap();

        soapModel.setLocationTargaId(model.getLocationTargaId());
        soapModel.setCodiceOffice(model.getCodiceOffice());
        soapModel.setTargaTecnicaSap(model.getTargaTecnicaSap());
        soapModel.setLocation(model.getLocation());
        soapModel.setPr(model.getPr());
        soapModel.setDataOnAir(model.getDataOnAir());

        return soapModel;
    }

    public static LocationSoap[] toSoapModels(Location[] models) {
        LocationSoap[] soapModels = new LocationSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static LocationSoap[][] toSoapModels(Location[][] models) {
        LocationSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new LocationSoap[models.length][models[0].length];
        } else {
            soapModels = new LocationSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static LocationSoap[] toSoapModels(List<Location> models) {
        List<LocationSoap> soapModels = new ArrayList<LocationSoap>(models.size());

        for (Location model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new LocationSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _locationTargaId;
    }

    public void setPrimaryKey(long pk) {
        setLocationTargaId(pk);
    }

    public long getLocationTargaId() {
        return _locationTargaId;
    }

    public void setLocationTargaId(long locationTargaId) {
        _locationTargaId = locationTargaId;
    }

    public String getCodiceOffice() {
        return _codiceOffice;
    }

    public void setCodiceOffice(String codiceOffice) {
        _codiceOffice = codiceOffice;
    }

    public String getTargaTecnicaSap() {
        return _targaTecnicaSap;
    }

    public void setTargaTecnicaSap(String targaTecnicaSap) {
        _targaTecnicaSap = targaTecnicaSap;
    }

    public String getLocation() {
        return _location;
    }

    public void setLocation(String location) {
        _location = location;
    }
    
    public String getPr() {
        return _pr;
    }

    public void setPr(String pr) {
        _pr = pr;
    }
    
    public Date getDataOnAir() {
        return _dataOnAir;
    }

    public void setDataOnAir(Date dataOnAir) {
        _dataOnAir = dataOnAir;
    }

}
