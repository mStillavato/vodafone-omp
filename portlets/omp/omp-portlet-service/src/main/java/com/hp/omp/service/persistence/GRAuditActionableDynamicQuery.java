package com.hp.omp.service.persistence;

import com.hp.omp.model.GRAudit;
import com.hp.omp.service.GRAuditLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class GRAuditActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public GRAuditActionableDynamicQuery() throws SystemException {
        setBaseLocalService(GRAuditLocalServiceUtil.getService());
        setClass(GRAudit.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("auditId");
    }
}
