package com.hp.omp.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class BudgetSubCategoryFinderUtil {
    private static BudgetSubCategoryFinder _finder;

    public static java.util.List<com.hp.omp.model.BudgetSubCategory> getBudgetSubCategoryList(
    	int start, int end, String searchFilter)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getFinder().getBudgetSubCategoryList(start, end, searchFilter);
    }
    
    public static BudgetSubCategoryFinder getFinder() {
        if (_finder == null) {
            _finder = (BudgetSubCategoryFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
            		BudgetSubCategoryFinder.class.getName());

            ReferenceRegistry.registerReference(BudgetSubCategoryFinderUtil.class,
                "_finder");
        }

        return _finder;
    }

    public void setFinder(BudgetSubCategoryFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(BudgetSubCategoryFinderUtil.class,
            "_finder");
    }
}
