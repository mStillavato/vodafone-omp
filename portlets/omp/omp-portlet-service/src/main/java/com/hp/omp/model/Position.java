package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Position service. Represents a row in the &quot;POSITION&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see PositionModel
 * @see com.hp.omp.model.impl.PositionImpl
 * @see com.hp.omp.model.impl.PositionModelImpl
 * @generated
 */
public interface Position extends PositionModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.PositionImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public void setCommaFormattedUnitCost(
        java.lang.String commaFormattedUnitCost);

    public java.lang.String getCommaFormattedUnitCost();

    public void setPositionStatusLabel(java.lang.String positionStatusLabel);

    public void setFormattedUnitCost(java.lang.String formatedUnitCost);

    public java.lang.String getFormattedUnitCost();

    public void setApproveBtn(boolean approveBtn);

    public boolean isApproveBtn();

    public java.lang.String getPositionStatusLabel();

    public void setApproverName(java.lang.String approverName);

    public java.lang.String getApproverName();

    public void setApprovalStatus(java.lang.String approvalStatus);

    public java.lang.String getApprovalStatus();

    public boolean getPositionEditBtton();

    public void setPositionEditBtton(boolean positionEditBtton);

    public com.hp.omp.model.custom.PositionStatus getPositionStatus();

    public java.lang.String getDeliveryDateText();

    public java.lang.String getOfferDateText();

    public java.lang.String getFieldStatus()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public java.lang.String getCheckboxStatus()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public java.lang.Double getSumPositionGRPercentage()
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.util.List<com.hp.omp.model.GoodReceipt> getPositionGoodReceipts()
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.lang.String getiCApprovalText();

    public void setiCApprovalText(java.lang.String iCApprovalText);

    public boolean isAddGrBtn()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setAddGrBtn(boolean addGrBtn);

    public boolean isEditGrBtn()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setEditGrBtn(boolean editGrBtn);
}
