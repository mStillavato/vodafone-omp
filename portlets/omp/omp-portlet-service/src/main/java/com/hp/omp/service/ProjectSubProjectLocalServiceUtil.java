package com.hp.omp.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the project sub project local service. This utility wraps {@link com.hp.omp.service.impl.ProjectSubProjectLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see ProjectSubProjectLocalService
 * @see com.hp.omp.service.base.ProjectSubProjectLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.ProjectSubProjectLocalServiceImpl
 * @generated
 */
public class ProjectSubProjectLocalServiceUtil {
    private static ProjectSubProjectLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.hp.omp.service.impl.ProjectSubProjectLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the project sub project to the database. Also notifies the appropriate model listeners.
    *
    * @param projectSubProject the project sub project
    * @return the project sub project that was added
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject addProjectSubProject(
        com.hp.omp.model.ProjectSubProject projectSubProject)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addProjectSubProject(projectSubProject);
    }

    /**
    * Creates a new project sub project with the primary key. Does not add the project sub project to the database.
    *
    * @param projectSubProjectId the primary key for the new project sub project
    * @return the new project sub project
    */
    public static com.hp.omp.model.ProjectSubProject createProjectSubProject(
        long projectSubProjectId) {
        return getService().createProjectSubProject(projectSubProjectId);
    }

    /**
    * Deletes the project sub project with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param projectSubProjectId the primary key of the project sub project
    * @return the project sub project that was removed
    * @throws PortalException if a project sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject deleteProjectSubProject(
        long projectSubProjectId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteProjectSubProject(projectSubProjectId);
    }

    /**
    * Deletes the project sub project from the database. Also notifies the appropriate model listeners.
    *
    * @param projectSubProject the project sub project
    * @return the project sub project that was removed
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject deleteProjectSubProject(
        com.hp.omp.model.ProjectSubProject projectSubProject)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteProjectSubProject(projectSubProject);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    public static com.hp.omp.model.ProjectSubProject fetchProjectSubProject(
        long projectSubProjectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchProjectSubProject(projectSubProjectId);
    }

    /**
    * Returns the project sub project with the primary key.
    *
    * @param projectSubProjectId the primary key of the project sub project
    * @return the project sub project
    * @throws PortalException if a project sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject getProjectSubProject(
        long projectSubProjectId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getProjectSubProject(projectSubProjectId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the project sub projects.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of project sub projects
    * @param end the upper bound of the range of project sub projects (not inclusive)
    * @return the range of project sub projects
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ProjectSubProject> getProjectSubProjects(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getProjectSubProjects(start, end);
    }

    /**
    * Returns the number of project sub projects.
    *
    * @return the number of project sub projects
    * @throws SystemException if a system exception occurred
    */
    public static int getProjectSubProjectsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getProjectSubProjectsCount();
    }

    /**
    * Updates the project sub project in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param projectSubProject the project sub project
    * @return the project sub project that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject updateProjectSubProject(
        com.hp.omp.model.ProjectSubProject projectSubProject)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateProjectSubProject(projectSubProject);
    }

    /**
    * Updates the project sub project in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param projectSubProject the project sub project
    * @param merge whether to merge the project sub project with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the project sub project that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject updateProjectSubProject(
        com.hp.omp.model.ProjectSubProject projectSubProject, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateProjectSubProject(projectSubProject, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static java.util.List<com.hp.omp.model.ProjectSubProject> getProjectSubProjectByProjectId(
        java.lang.Long projectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getProjectSubProjectByProjectId(projectId);
    }

    public static java.util.List<com.hp.omp.model.ProjectSubProject> getProjectSubProjectBySubProjectId(
        java.lang.Long subProjectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getProjectSubProjectBySubProjectId(subProjectId);
    }

    public static java.util.List<com.hp.omp.model.ProjectSubProject> getProjectSubProjectByProjectIdAndSubProjectId(
        java.lang.Long projectId, java.lang.Long subProjectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .getProjectSubProjectByProjectIdAndSubProjectId(projectId,
            subProjectId);
    }

    public static void clearService() {
        _service = null;
    }

    public static ProjectSubProjectLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    ProjectSubProjectLocalService.class.getName());

            if (invokableLocalService instanceof ProjectSubProjectLocalService) {
                _service = (ProjectSubProjectLocalService) invokableLocalService;
            } else {
                _service = new ProjectSubProjectLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(ProjectSubProjectLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated
     */
    public void setService(ProjectSubProjectLocalService service) {
    }
}
