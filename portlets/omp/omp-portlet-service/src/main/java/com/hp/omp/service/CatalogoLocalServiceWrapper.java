/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service;

import java.io.OutputStream;
import java.util.List;

import com.hp.omp.model.Catalogo;
import com.hp.omp.model.custom.CatalogoListDTO;
import com.hp.omp.model.custom.PoCatalogNotNssDTO;
import com.hp.omp.model.custom.SearchCatalogoDTO;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link CatalogoLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       CatalogoLocalService
 * @generated
 */
public class CatalogoLocalServiceWrapper implements CatalogoLocalService,
ServiceWrapper<CatalogoLocalService> {
	public CatalogoLocalServiceWrapper(
			CatalogoLocalService catalogoLocalService) {
		_catalogoLocalService = catalogoLocalService;
	}

	/**
	 * Adds the catalogo to the database. Also notifies the appropriate model listeners.
	 *
	 * @param catalogo the catalogo
	 * @return the catalogo that was added
	 * @throws SystemException if a system exception occurred
	 */
	public com.hp.omp.model.Catalogo addCatalogo(
			com.hp.omp.model.Catalogo catalogo)
					throws com.liferay.portal.kernel.exception.SystemException {
		return _catalogoLocalService.addCatalogo(catalogo);
	}

	/**
	 * Creates a new catalogo with the primary key. Does not add the catalogo to the database.
	 *
	 * @param catalogoId the primary key for the new catalogo
	 * @return the new catalogo
	 */
	public com.hp.omp.model.Catalogo createCatalogo(long catalogoId) {
		return _catalogoLocalService.createCatalogo(catalogoId);
	}

	/**
	 * Deletes the catalogo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param catalogoId the primary key of the catalogo
	 * @return the catalogo that was removed
	 * @throws PortalException if a catalogo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public com.hp.omp.model.Catalogo deleteCatalogo(long catalogoId)
			throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _catalogoLocalService.deleteCatalogo(catalogoId);
	}

	/**
	 * Deletes the catalogo from the database. Also notifies the appropriate model listeners.
	 *
	 * @param catalogo the catalogo
	 * @return the catalogo that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public com.hp.omp.model.Catalogo deleteCatalogo(
			com.hp.omp.model.Catalogo catalogo)
					throws com.liferay.portal.kernel.exception.SystemException {
		return _catalogoLocalService.deleteCatalogo(catalogo);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _catalogoLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
					throws com.liferay.portal.kernel.exception.SystemException {
		return _catalogoLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
			int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _catalogoLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
			int end,
			com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
					throws com.liferay.portal.kernel.exception.SystemException {
		return _catalogoLocalService.dynamicQuery(dynamicQuery, start, end,
				orderByComparator);
	}

	/**
	 * Returns the number of rows that match the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows that match the dynamic query
	 * @throws SystemException if a system exception occurred
	 */
	public long dynamicQueryCount(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
					throws com.liferay.portal.kernel.exception.SystemException {
		return _catalogoLocalService.dynamicQueryCount(dynamicQuery);
	}

	public com.hp.omp.model.Catalogo fetchCatalogo(long catalogoId)
			throws com.liferay.portal.kernel.exception.SystemException {
		return _catalogoLocalService.fetchCatalogo(catalogoId);
	}

	/**
	 * Returns the catalogo with the primary key.
	 *
	 * @param catalogoId the primary key of the catalogo
	 * @return the catalogo
	 * @throws PortalException if a catalogo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public com.hp.omp.model.Catalogo getCatalogo(long catalogoId)
			throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _catalogoLocalService.getCatalogo(catalogoId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
					throws com.liferay.portal.kernel.exception.PortalException,
					com.liferay.portal.kernel.exception.SystemException {
		return _catalogoLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns a range of all the catalogos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of catalogos
	 * @param end the upper bound of the range of catalogos (not inclusive)
	 * @return the range of catalogos
	 * @throws SystemException if a system exception occurred
	 */
	public java.util.List<com.hp.omp.model.Catalogo> getCatalogos(int start,
			int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _catalogoLocalService.getCatalogos(start, end);
	}

	/**
	 * Returns the number of catalogos.
	 *
	 * @return the number of catalogos
	 * @throws SystemException if a system exception occurred
	 */
	public int getCatalogosCount()
			throws com.liferay.portal.kernel.exception.SystemException {
		return _catalogoLocalService.getCatalogosCount();
	}

	/**
	 * Updates the catalogo in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param catalogo the catalogo
	 * @return the catalogo that was updated
	 * @throws SystemException if a system exception occurred
	 */
	public com.hp.omp.model.Catalogo updateCatalogo(
			com.hp.omp.model.Catalogo catalogo)
					throws com.liferay.portal.kernel.exception.SystemException {
		return _catalogoLocalService.updateCatalogo(catalogo);
	}

	/**
	 * Updates the catalogo in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param catalogo the catalogo
	 * @param merge whether to merge the catalogo with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	 * @return the catalogo that was updated
	 * @throws SystemException if a system exception occurred
	 */
	public com.hp.omp.model.Catalogo updateCatalogo(
			com.hp.omp.model.Catalogo catalogo, boolean merge)
					throws com.liferay.portal.kernel.exception.SystemException {
		return _catalogoLocalService.updateCatalogo(catalogo, merge);
	}

	/**
	 * Returns the Spring bean ID for this bean.
	 *
	 * @return the Spring bean ID for this bean
	 */
	public java.lang.String getBeanIdentifier() {
		return _catalogoLocalService.getBeanIdentifier();
	}

	/**
	 * Sets the Spring bean ID for this bean.
	 *
	 * @param beanIdentifier the Spring bean ID for this bean
	 */
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_catalogoLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
			java.lang.String[] parameterTypes, java.lang.Object[] arguments)
					throws java.lang.Throwable {
		return _catalogoLocalService.invokeMethod(name, parameterTypes,
				arguments);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public CatalogoLocalService getWrappedCatalogoLocalService() {
		return _catalogoLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedCatalogoLocalService(
			CatalogoLocalService catalogoLocalService) {
		_catalogoLocalService = catalogoLocalService;
	}

	public CatalogoLocalService getWrappedService() {
		return _catalogoLocalService;
	}

	public void setWrappedService(CatalogoLocalService catalogoLocalService) {
		_catalogoLocalService = catalogoLocalService;
	}

	private CatalogoLocalService _catalogoLocalService;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<com.hp.omp.model.custom.CatalogoListDTO> getCatalogoList(
			com.hp.omp.model.custom.SearchCatalogoDTO searchDTO) {
		return _catalogoLocalService.getCatalogoList(searchDTO);
	}

	public java.lang.Long getCatalogoListCount(
			com.hp.omp.model.custom.SearchCatalogoDTO searchDTO) {
		return _catalogoLocalService.getCatalogoListCount(searchDTO);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Catalogo> getCatalogoForVendor(int start, int end,
			String vendorCode) throws SystemException {
		return _catalogoLocalService.getCatalogoForVendor(start, end, vendorCode);
	}

	public void exportReportAsExcel(List<com.hp.omp.model.custom.LayoutNotNssCatalogoListDTO> listPoCatalogNotNssDTO,
			List<Object> header, OutputStream out) throws SystemException {
		_catalogoLocalService.exportReportAsExcel(listPoCatalogNotNssDTO, header, out);

	}

	public void exportCatalogoAsExcel(List<com.hp.omp.model.custom.CatalogoExportListDTO> listCatalog,
			List<Object> header, OutputStream out) throws SystemException {
		_catalogoLocalService.exportCatalogoAsExcel(listCatalog, header, out);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<com.hp.omp.model.custom.CatalogoExportListDTO> getExportCatalogoList(
			SearchCatalogoDTO searchDTO) {
		return _catalogoLocalService.getExportCatalogoList(searchDTO);
	}

}