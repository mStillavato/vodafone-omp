package com.hp.omp.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link PurchaseRequest}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       PurchaseRequest
 * @generated
 */
public class PurchaseRequestWrapper implements PurchaseRequest,
    ModelWrapper<PurchaseRequest> {
    private PurchaseRequest _purchaseRequest;

    public PurchaseRequestWrapper(PurchaseRequest purchaseRequest) {
        _purchaseRequest = purchaseRequest;
    }

    public Class<?> getModelClass() {
        return PurchaseRequest.class;
    }

    public String getModelClassName() {
        return PurchaseRequest.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("purchaseRequestId", getPurchaseRequestId());
        attributes.put("costCenterId", getCostCenterId());
        attributes.put("vendorId", getVendorId());
        attributes.put("buyer", getBuyer());
        attributes.put("ola", getOla());
        attributes.put("projectId", getProjectId());
        attributes.put("subProjectId", getSubProjectId());
        attributes.put("budgetCategoryId", getBudgetCategoryId());
        attributes.put("budgetSubCategoryId", getBudgetSubCategoryId());
        attributes.put("activityDescription", getActivityDescription());
        attributes.put("macroDriverId", getMacroDriverId());
        attributes.put("microDriverId", getMicroDriverId());
        attributes.put("odnpCodeId", getOdnpCodeId());
        attributes.put("odnpNameId", getOdnpNameId());
        attributes.put("totalValue", getTotalValue());
        attributes.put("currency", getCurrency());
        attributes.put("receiverUserId", getReceiverUserId());
        attributes.put("fiscalYear", getFiscalYear());
        attributes.put("screenNameRequester", getScreenNameRequester());
        attributes.put("screenNameReciever", getScreenNameReciever());
        attributes.put("automatic", getAutomatic());
        attributes.put("status", getStatus());
        attributes.put("createdDate", getCreatedDate());
        attributes.put("createdUserId", getCreatedUserId());
        attributes.put("rejectionCause", getRejectionCause());
        attributes.put("wbsCodeId", getWbsCodeId());
        attributes.put("trackingCode", getTrackingCode());
        attributes.put("buyerId", getBuyerId());
        attributes.put("targaTecnica", getTargaTecnica());
        attributes.put("evoLocationId", getEvoLocationId());
        attributes.put("tipoPr", getTipoPr());
        attributes.put("plant", getPlant());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long purchaseRequestId = (Long) attributes.get("purchaseRequestId");

        if (purchaseRequestId != null) {
            setPurchaseRequestId(purchaseRequestId);
        }

        String costCenterId = (String) attributes.get("costCenterId");

        if (costCenterId != null) {
            setCostCenterId(costCenterId);
        }

        String vendorId = (String) attributes.get("vendorId");

        if (vendorId != null) {
            setVendorId(vendorId);
        }

        String buyer = (String) attributes.get("buyer");

        if (buyer != null) {
            setBuyer(buyer);
        }

        String ola = (String) attributes.get("ola");

        if (ola != null) {
            setOla(ola);
        }

        String projectId = (String) attributes.get("projectId");

        if (projectId != null) {
            setProjectId(projectId);
        }

        String subProjectId = (String) attributes.get("subProjectId");

        if (subProjectId != null) {
            setSubProjectId(subProjectId);
        }

        String budgetCategoryId = (String) attributes.get("budgetCategoryId");

        if (budgetCategoryId != null) {
            setBudgetCategoryId(budgetCategoryId);
        }

        String budgetSubCategoryId = (String) attributes.get(
                "budgetSubCategoryId");

        if (budgetSubCategoryId != null) {
            setBudgetSubCategoryId(budgetSubCategoryId);
        }

        String activityDescription = (String) attributes.get(
                "activityDescription");

        if (activityDescription != null) {
            setActivityDescription(activityDescription);
        }

        String macroDriverId = (String) attributes.get("macroDriverId");

        if (macroDriverId != null) {
            setMacroDriverId(macroDriverId);
        }

        String microDriverId = (String) attributes.get("microDriverId");

        if (microDriverId != null) {
            setMicroDriverId(microDriverId);
        }

        String odnpCodeId = (String) attributes.get("odnpCodeId");

        if (odnpCodeId != null) {
            setOdnpCodeId(odnpCodeId);
        }

        String odnpNameId = (String) attributes.get("odnpNameId");

        if (odnpNameId != null) {
            setOdnpNameId(odnpNameId);
        }

        String totalValue = (String) attributes.get("totalValue");

        if (totalValue != null) {
            setTotalValue(totalValue);
        }

        String currency = (String) attributes.get("currency");

        if (currency != null) {
            setCurrency(currency);
        }

        String receiverUserId = (String) attributes.get("receiverUserId");

        if (receiverUserId != null) {
            setReceiverUserId(receiverUserId);
        }

        String fiscalYear = (String) attributes.get("fiscalYear");

        if (fiscalYear != null) {
            setFiscalYear(fiscalYear);
        }

        String screenNameRequester = (String) attributes.get(
                "screenNameRequester");

        if (screenNameRequester != null) {
            setScreenNameRequester(screenNameRequester);
        }

        String screenNameReciever = (String) attributes.get(
                "screenNameReciever");

        if (screenNameReciever != null) {
            setScreenNameReciever(screenNameReciever);
        }

        Boolean automatic = (Boolean) attributes.get("automatic");

        if (automatic != null) {
            setAutomatic(automatic);
        }

        Integer status = (Integer) attributes.get("status");

        if (status != null) {
            setStatus(status);
        }

        Date createdDate = (Date) attributes.get("createdDate");

        if (createdDate != null) {
            setCreatedDate(createdDate);
        }

        Long createdUserId = (Long) attributes.get("createdUserId");

        if (createdUserId != null) {
            setCreatedUserId(createdUserId);
        }

        String rejectionCause = (String) attributes.get("rejectionCause");

        if (rejectionCause != null) {
            setRejectionCause(rejectionCause);
        }

        String wbsCodeId = (String) attributes.get("wbsCodeId");

        if (wbsCodeId != null) {
            setWbsCodeId(wbsCodeId);
        }

        String trackingCode = (String) attributes.get("trackingCode");

        if (trackingCode != null) {
            setTrackingCode(trackingCode);
        }

        String buyerId = (String) attributes.get("buyerId");

        if (buyerId != null) {
            setBuyerId(buyerId);
        }
        
        String targaTecnica = (String) attributes.get("targaTecnica");

        if (targaTecnica != null) {
            setTargaTecnica(targaTecnica);
        }
        
        String evoLocationId = (String) attributes.get("evoLocationId");

        if (evoLocationId != null) {
            setEvoLocationId(evoLocationId);
        }
        
        Integer tipoPr = (Integer) attributes.get("tipoPr");

        if (tipoPr != null) {
            setTipoPr(tipoPr);
        }
        
        String plant = (String) attributes.get("plant");

        if (plant != null) {
            setPlant(plant);
        }
        
    }

    /**
    * Returns the primary key of this purchase request.
    *
    * @return the primary key of this purchase request
    */
    public long getPrimaryKey() {
        return _purchaseRequest.getPrimaryKey();
    }

    /**
    * Sets the primary key of this purchase request.
    *
    * @param primaryKey the primary key of this purchase request
    */
    public void setPrimaryKey(long primaryKey) {
        _purchaseRequest.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the purchase request ID of this purchase request.
    *
    * @return the purchase request ID of this purchase request
    */
    public long getPurchaseRequestId() {
        return _purchaseRequest.getPurchaseRequestId();
    }

    /**
    * Sets the purchase request ID of this purchase request.
    *
    * @param purchaseRequestId the purchase request ID of this purchase request
    */
    public void setPurchaseRequestId(long purchaseRequestId) {
        _purchaseRequest.setPurchaseRequestId(purchaseRequestId);
    }

    /**
    * Returns the cost center ID of this purchase request.
    *
    * @return the cost center ID of this purchase request
    */
    public java.lang.String getCostCenterId() {
        return _purchaseRequest.getCostCenterId();
    }

    /**
    * Sets the cost center ID of this purchase request.
    *
    * @param costCenterId the cost center ID of this purchase request
    */
    public void setCostCenterId(java.lang.String costCenterId) {
        _purchaseRequest.setCostCenterId(costCenterId);
    }

    /**
    * Returns the vendor ID of this purchase request.
    *
    * @return the vendor ID of this purchase request
    */
    public java.lang.String getVendorId() {
        return _purchaseRequest.getVendorId();
    }

    /**
    * Sets the vendor ID of this purchase request.
    *
    * @param vendorId the vendor ID of this purchase request
    */
    public void setVendorId(java.lang.String vendorId) {
        _purchaseRequest.setVendorId(vendorId);
    }

    /**
    * Returns the buyer of this purchase request.
    *
    * @return the buyer of this purchase request
    */
    public java.lang.String getBuyer() {
        return _purchaseRequest.getBuyer();
    }

    /**
    * Sets the buyer of this purchase request.
    *
    * @param buyer the buyer of this purchase request
    */
    public void setBuyer(java.lang.String buyer) {
        _purchaseRequest.setBuyer(buyer);
    }

    /**
    * Returns the ola of this purchase request.
    *
    * @return the ola of this purchase request
    */
    public java.lang.String getOla() {
        return _purchaseRequest.getOla();
    }

    /**
    * Sets the ola of this purchase request.
    *
    * @param ola the ola of this purchase request
    */
    public void setOla(java.lang.String ola) {
        _purchaseRequest.setOla(ola);
    }

    /**
    * Returns the project ID of this purchase request.
    *
    * @return the project ID of this purchase request
    */
    public java.lang.String getProjectId() {
        return _purchaseRequest.getProjectId();
    }

    /**
    * Sets the project ID of this purchase request.
    *
    * @param projectId the project ID of this purchase request
    */
    public void setProjectId(java.lang.String projectId) {
        _purchaseRequest.setProjectId(projectId);
    }

    /**
    * Returns the sub project ID of this purchase request.
    *
    * @return the sub project ID of this purchase request
    */
    public java.lang.String getSubProjectId() {
        return _purchaseRequest.getSubProjectId();
    }

    /**
    * Sets the sub project ID of this purchase request.
    *
    * @param subProjectId the sub project ID of this purchase request
    */
    public void setSubProjectId(java.lang.String subProjectId) {
        _purchaseRequest.setSubProjectId(subProjectId);
    }

    /**
    * Returns the budget category ID of this purchase request.
    *
    * @return the budget category ID of this purchase request
    */
    public java.lang.String getBudgetCategoryId() {
        return _purchaseRequest.getBudgetCategoryId();
    }

    /**
    * Sets the budget category ID of this purchase request.
    *
    * @param budgetCategoryId the budget category ID of this purchase request
    */
    public void setBudgetCategoryId(java.lang.String budgetCategoryId) {
        _purchaseRequest.setBudgetCategoryId(budgetCategoryId);
    }

    /**
    * Returns the budget sub category ID of this purchase request.
    *
    * @return the budget sub category ID of this purchase request
    */
    public java.lang.String getBudgetSubCategoryId() {
        return _purchaseRequest.getBudgetSubCategoryId();
    }

    /**
    * Sets the budget sub category ID of this purchase request.
    *
    * @param budgetSubCategoryId the budget sub category ID of this purchase request
    */
    public void setBudgetSubCategoryId(java.lang.String budgetSubCategoryId) {
        _purchaseRequest.setBudgetSubCategoryId(budgetSubCategoryId);
    }

    /**
    * Returns the activity description of this purchase request.
    *
    * @return the activity description of this purchase request
    */
    public java.lang.String getActivityDescription() {
        return _purchaseRequest.getActivityDescription();
    }

    /**
    * Sets the activity description of this purchase request.
    *
    * @param activityDescription the activity description of this purchase request
    */
    public void setActivityDescription(java.lang.String activityDescription) {
        _purchaseRequest.setActivityDescription(activityDescription);
    }

    /**
    * Returns the macro driver ID of this purchase request.
    *
    * @return the macro driver ID of this purchase request
    */
    public java.lang.String getMacroDriverId() {
        return _purchaseRequest.getMacroDriverId();
    }

    /**
    * Sets the macro driver ID of this purchase request.
    *
    * @param macroDriverId the macro driver ID of this purchase request
    */
    public void setMacroDriverId(java.lang.String macroDriverId) {
        _purchaseRequest.setMacroDriverId(macroDriverId);
    }

    /**
    * Returns the micro driver ID of this purchase request.
    *
    * @return the micro driver ID of this purchase request
    */
    public java.lang.String getMicroDriverId() {
        return _purchaseRequest.getMicroDriverId();
    }

    /**
    * Sets the micro driver ID of this purchase request.
    *
    * @param microDriverId the micro driver ID of this purchase request
    */
    public void setMicroDriverId(java.lang.String microDriverId) {
        _purchaseRequest.setMicroDriverId(microDriverId);
    }

    /**
    * Returns the odnp code ID of this purchase request.
    *
    * @return the odnp code ID of this purchase request
    */
    public java.lang.String getOdnpCodeId() {
        return _purchaseRequest.getOdnpCodeId();
    }

    /**
    * Sets the odnp code ID of this purchase request.
    *
    * @param odnpCodeId the odnp code ID of this purchase request
    */
    public void setOdnpCodeId(java.lang.String odnpCodeId) {
        _purchaseRequest.setOdnpCodeId(odnpCodeId);
    }

    /**
    * Returns the odnp name ID of this purchase request.
    *
    * @return the odnp name ID of this purchase request
    */
    public java.lang.String getOdnpNameId() {
        return _purchaseRequest.getOdnpNameId();
    }

    /**
    * Sets the odnp name ID of this purchase request.
    *
    * @param odnpNameId the odnp name ID of this purchase request
    */
    public void setOdnpNameId(java.lang.String odnpNameId) {
        _purchaseRequest.setOdnpNameId(odnpNameId);
    }

    /**
    * Returns the total value of this purchase request.
    *
    * @return the total value of this purchase request
    */
    public java.lang.String getTotalValue() {
        return _purchaseRequest.getTotalValue();
    }

    /**
    * Sets the total value of this purchase request.
    *
    * @param totalValue the total value of this purchase request
    */
    public void setTotalValue(java.lang.String totalValue) {
        _purchaseRequest.setTotalValue(totalValue);
    }

    /**
    * Returns the currency of this purchase request.
    *
    * @return the currency of this purchase request
    */
    public java.lang.String getCurrency() {
        return _purchaseRequest.getCurrency();
    }

    /**
    * Sets the currency of this purchase request.
    *
    * @param currency the currency of this purchase request
    */
    public void setCurrency(java.lang.String currency) {
        _purchaseRequest.setCurrency(currency);
    }

    /**
    * Returns the receiver user ID of this purchase request.
    *
    * @return the receiver user ID of this purchase request
    */
    public java.lang.String getReceiverUserId() {
        return _purchaseRequest.getReceiverUserId();
    }

    /**
    * Sets the receiver user ID of this purchase request.
    *
    * @param receiverUserId the receiver user ID of this purchase request
    */
    public void setReceiverUserId(java.lang.String receiverUserId) {
        _purchaseRequest.setReceiverUserId(receiverUserId);
    }

    /**
    * Returns the fiscal year of this purchase request.
    *
    * @return the fiscal year of this purchase request
    */
    public java.lang.String getFiscalYear() {
        return _purchaseRequest.getFiscalYear();
    }

    /**
    * Sets the fiscal year of this purchase request.
    *
    * @param fiscalYear the fiscal year of this purchase request
    */
    public void setFiscalYear(java.lang.String fiscalYear) {
        _purchaseRequest.setFiscalYear(fiscalYear);
    }

    /**
    * Returns the screen name requester of this purchase request.
    *
    * @return the screen name requester of this purchase request
    */
    public java.lang.String getScreenNameRequester() {
        return _purchaseRequest.getScreenNameRequester();
    }

    /**
    * Sets the screen name requester of this purchase request.
    *
    * @param screenNameRequester the screen name requester of this purchase request
    */
    public void setScreenNameRequester(java.lang.String screenNameRequester) {
        _purchaseRequest.setScreenNameRequester(screenNameRequester);
    }

    /**
    * Returns the screen name reciever of this purchase request.
    *
    * @return the screen name reciever of this purchase request
    */
    public java.lang.String getScreenNameReciever() {
        return _purchaseRequest.getScreenNameReciever();
    }

    /**
    * Sets the screen name reciever of this purchase request.
    *
    * @param screenNameReciever the screen name reciever of this purchase request
    */
    public void setScreenNameReciever(java.lang.String screenNameReciever) {
        _purchaseRequest.setScreenNameReciever(screenNameReciever);
    }

    /**
    * Returns the automatic of this purchase request.
    *
    * @return the automatic of this purchase request
    */
    public boolean getAutomatic() {
        return _purchaseRequest.getAutomatic();
    }

    /**
    * Returns <code>true</code> if this purchase request is automatic.
    *
    * @return <code>true</code> if this purchase request is automatic; <code>false</code> otherwise
    */
    public boolean isAutomatic() {
        return _purchaseRequest.isAutomatic();
    }

    /**
    * Sets whether this purchase request is automatic.
    *
    * @param automatic the automatic of this purchase request
    */
    public void setAutomatic(boolean automatic) {
        _purchaseRequest.setAutomatic(automatic);
    }

    /**
    * Returns the status of this purchase request.
    *
    * @return the status of this purchase request
    */
    public int getStatus() {
        return _purchaseRequest.getStatus();
    }

    /**
    * Sets the status of this purchase request.
    *
    * @param status the status of this purchase request
    */
    public void setStatus(int status) {
        _purchaseRequest.setStatus(status);
    }

    /**
    * Returns the created date of this purchase request.
    *
    * @return the created date of this purchase request
    */
    public java.util.Date getCreatedDate() {
        return _purchaseRequest.getCreatedDate();
    }

    /**
    * Sets the created date of this purchase request.
    *
    * @param createdDate the created date of this purchase request
    */
    public void setCreatedDate(java.util.Date createdDate) {
        _purchaseRequest.setCreatedDate(createdDate);
    }

    /**
    * Returns the created user ID of this purchase request.
    *
    * @return the created user ID of this purchase request
    */
    public long getCreatedUserId() {
        return _purchaseRequest.getCreatedUserId();
    }

    /**
    * Sets the created user ID of this purchase request.
    *
    * @param createdUserId the created user ID of this purchase request
    */
    public void setCreatedUserId(long createdUserId) {
        _purchaseRequest.setCreatedUserId(createdUserId);
    }

    /**
    * Returns the created user uuid of this purchase request.
    *
    * @return the created user uuid of this purchase request
    * @throws SystemException if a system exception occurred
    */
    public java.lang.String getCreatedUserUuid()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequest.getCreatedUserUuid();
    }

    /**
    * Sets the created user uuid of this purchase request.
    *
    * @param createdUserUuid the created user uuid of this purchase request
    */
    public void setCreatedUserUuid(java.lang.String createdUserUuid) {
        _purchaseRequest.setCreatedUserUuid(createdUserUuid);
    }

    /**
    * Returns the rejection cause of this purchase request.
    *
    * @return the rejection cause of this purchase request
    */
    public java.lang.String getRejectionCause() {
        return _purchaseRequest.getRejectionCause();
    }

    /**
    * Sets the rejection cause of this purchase request.
    *
    * @param rejectionCause the rejection cause of this purchase request
    */
    public void setRejectionCause(java.lang.String rejectionCause) {
        _purchaseRequest.setRejectionCause(rejectionCause);
    }

    /**
    * Returns the wbs code ID of this purchase request.
    *
    * @return the wbs code ID of this purchase request
    */
    public java.lang.String getWbsCodeId() {
        return _purchaseRequest.getWbsCodeId();
    }

    /**
    * Sets the wbs code ID of this purchase request.
    *
    * @param wbsCodeId the wbs code ID of this purchase request
    */
    public void setWbsCodeId(java.lang.String wbsCodeId) {
        _purchaseRequest.setWbsCodeId(wbsCodeId);
    }

    /**
    * Returns the tracking code of this purchase request.
    *
    * @return the tracking code of this purchase request
    */
    public java.lang.String getTrackingCode() {
        return _purchaseRequest.getTrackingCode();
    }

    /**
    * Sets the tracking code of this purchase request.
    *
    * @param trackingCode the tracking code of this purchase request
    */
    public void setTrackingCode(java.lang.String trackingCode) {
        _purchaseRequest.setTrackingCode(trackingCode);
    }

    /**
    * Returns the buyer ID of this purchase request.
    *
    * @return the buyer ID of this purchase request
    */
    public java.lang.String getBuyerId() {
        return _purchaseRequest.getBuyerId();
    }

    /**
    * Sets the buyer ID of this purchase request.
    *
    * @param buyerId the buyer ID of this purchase request
    */
    public void setBuyerId(java.lang.String buyerId) {
        _purchaseRequest.setBuyerId(buyerId);
    }

    public boolean isNew() {
        return _purchaseRequest.isNew();
    }

    public void setNew(boolean n) {
        _purchaseRequest.setNew(n);
    }

    public boolean isCachedModel() {
        return _purchaseRequest.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _purchaseRequest.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _purchaseRequest.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _purchaseRequest.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _purchaseRequest.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _purchaseRequest.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _purchaseRequest.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new PurchaseRequestWrapper((PurchaseRequest) _purchaseRequest.clone());
    }

    public int compareTo(PurchaseRequest purchaseRequest) {
        return _purchaseRequest.compareTo(purchaseRequest);
    }

    @Override
    public int hashCode() {
        return _purchaseRequest.hashCode();
    }

    public com.liferay.portal.model.CacheModel<PurchaseRequest> toCacheModel() {
        return _purchaseRequest.toCacheModel();
    }

    public PurchaseRequest toEscapedModel() {
        return new PurchaseRequestWrapper(_purchaseRequest.toEscapedModel());
    }

    public PurchaseRequest toUnescapedModel() {
        return new PurchaseRequestWrapper(_purchaseRequest.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _purchaseRequest.toString();
    }

    public java.lang.String toXmlString() {
        return _purchaseRequest.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _purchaseRequest.persist();
    }

    public java.lang.String getCostCenterName() {
        return _purchaseRequest.getCostCenterName();
    }

    public void setCostCenterName(java.lang.String costCenterName) {
        _purchaseRequest.setCostCenterName(costCenterName);
    }

    public com.hp.omp.model.custom.PurchaseRequestStatus getPurchaseRequestStatus() {
        return _purchaseRequest.getPurchaseRequestStatus();
    }

    public boolean isHasFile() {
        return _purchaseRequest.isHasFile();
    }

    public boolean isDisabledProject() {
        return _purchaseRequest.isDisabledProject();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PurchaseRequestWrapper)) {
            return false;
        }

        PurchaseRequestWrapper purchaseRequestWrapper = (PurchaseRequestWrapper) obj;

        if (Validator.equals(_purchaseRequest,
                    purchaseRequestWrapper._purchaseRequest)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public PurchaseRequest getWrappedPurchaseRequest() {
        return _purchaseRequest;
    }

    public PurchaseRequest getWrappedModel() {
        return _purchaseRequest;
    }

    public void resetOriginalValues() {
        _purchaseRequest.resetOriginalValues();
    }

	@AutoEscape
	public String getTargaTecnica() {
		return _purchaseRequest.getTargaTecnica();
	}

	public void setTargaTecnica(String targaTecnica) {
		_purchaseRequest.setTargaTecnica(targaTecnica);
	}

	@AutoEscape
	public String getEvoLocationId() {
		return _purchaseRequest.getEvoLocationId();
	}

	public void setEvoLocationId(String evoLocationId) {
		_purchaseRequest.setEvoLocationId(evoLocationId);
	}

	public int getTipoPr() {
		return _purchaseRequest.getTipoPr();
	}

	public void setTipoPr(int tipoPr) {
		_purchaseRequest.setTipoPr(tipoPr);
	}

	@AutoEscape
	public String getPlant() {
		return _purchaseRequest.getPlant();
	}

	public void setPlant(String plant) {
		_purchaseRequest.setPlant(plant);
	}
}
