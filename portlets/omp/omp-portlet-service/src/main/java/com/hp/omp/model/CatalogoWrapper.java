/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Catalogo}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       Catalogo
 * @generated
 */
public class CatalogoWrapper implements Catalogo, ModelWrapper<Catalogo> {
	public CatalogoWrapper(Catalogo catalogo) {
		_catalogo = catalogo;
	}

	public Class<?> getModelClass() {
		return Catalogo.class;
	}

	public String getModelClassName() {
		return Catalogo.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("catalogoId", getCatalogoId());
		attributes.put("chiaveInforecord", getChiaveInforecord());
		attributes.put("dataCreazione", getDataCreazione());
		attributes.put("dataFineValidita", getDataFineValidita());
		attributes.put("divisione", getDivisione());
		attributes.put("flgCanc", getFlgCanc());
		attributes.put("fornFinCode", getFornFinCode());
		attributes.put("fornitoreCode", getFornitoreCode());
		attributes.put("fornitoreDesc", getFornitoreDesc());
		attributes.put("matCatLvl2", getMatCatLvl2());
		attributes.put("matCatLvl2Desc", getMatCatLvl2Desc());
		attributes.put("matCatLvl4", getMatCatLvl4());
		attributes.put("matCatLvl4Desc", getMatCatLvl4Desc());
		attributes.put("matCod", getMatCod());
		attributes.put("matCodFornFinale", getMatCodFornFinale());
		attributes.put("matDesc", getMatDesc());
		attributes.put("prezzo", getPrezzo());
		attributes.put("purchOrganiz", getPurchOrganiz());
		attributes.put("um", getUm());
		attributes.put("umPrezzo", getUmPrezzo());
		attributes.put("umPrezzoPo", getUmPrezzoPo());
		attributes.put("valuta", getValuta());
		attributes.put("upperMatDesc", getUpperMatDesc());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long catalogoId = (Long)attributes.get("catalogoId");

		if (catalogoId != null) {
			setCatalogoId(catalogoId);
		}

		String chiaveInforecord = (String)attributes.get("chiaveInforecord");

		if (chiaveInforecord != null) {
			setChiaveInforecord(chiaveInforecord);
		}

		Date dataCreazione = (Date)attributes.get("dataCreazione");

		if (dataCreazione != null) {
			setDataCreazione(dataCreazione);
		}

		Date dataFineValidita = (Date)attributes.get("dataFineValidita");

		if (dataFineValidita != null) {
			setDataFineValidita(dataFineValidita);
		}

		String divisione = (String)attributes.get("divisione");

		if (divisione != null) {
			setDivisione(divisione);
		}

		Boolean flgCanc = (Boolean)attributes.get("flgCanc");

		if (flgCanc != null) {
			setFlgCanc(flgCanc);
		}

		String fornFinCode = (String)attributes.get("fornFinCode");

		if (fornFinCode != null) {
			setFornFinCode(fornFinCode);
		}

		String fornitoreCode = (String)attributes.get("fornitoreCode");

		if (fornitoreCode != null) {
			setFornitoreCode(fornitoreCode);
		}

		String fornitoreDesc = (String)attributes.get("fornitoreDesc");

		if (fornitoreDesc != null) {
			setFornitoreDesc(fornitoreDesc);
		}

		String matCatLvl2 = (String)attributes.get("matCatLvl2");

		if (matCatLvl2 != null) {
			setMatCatLvl2(matCatLvl2);
		}

		String matCatLvl2Desc = (String)attributes.get("matCatLvl2Desc");

		if (matCatLvl2Desc != null) {
			setMatCatLvl2Desc(matCatLvl2Desc);
		}

		String matCatLvl4 = (String)attributes.get("matCatLvl4");

		if (matCatLvl4 != null) {
			setMatCatLvl4(matCatLvl4);
		}

		String matCatLvl4Desc = (String)attributes.get("matCatLvl4Desc");

		if (matCatLvl4Desc != null) {
			setMatCatLvl4Desc(matCatLvl4Desc);
		}

		String matCod = (String)attributes.get("matCod");

		if (matCod != null) {
			setMatCod(matCod);
		}

		String matCodFornFinale = (String)attributes.get("matCodFornFinale");

		if (matCodFornFinale != null) {
			setMatCodFornFinale(matCodFornFinale);
		}

		String matDesc = (String)attributes.get("matDesc");

		if (matDesc != null) {
			setMatDesc(matDesc);
		}

		String prezzo = (String)attributes.get("prezzo");

		if (prezzo != null) {
			setPrezzo(prezzo);
		}

		String purchOrganiz = (String)attributes.get("purchOrganiz");

		if (purchOrganiz != null) {
			setPurchOrganiz(purchOrganiz);
		}

		String um = (String)attributes.get("um");

		if (um != null) {
			setUm(um);
		}

		String umPrezzo = (String)attributes.get("umPrezzo");

		if (umPrezzo != null) {
			setUmPrezzo(umPrezzo);
		}

		String umPrezzoPo = (String)attributes.get("umPrezzoPo");

		if (umPrezzoPo != null) {
			setUmPrezzoPo(umPrezzoPo);
		}

		String valuta = (String)attributes.get("valuta");

		if (valuta != null) {
			setValuta(valuta);
		}

		String upperMatDesc = (String)attributes.get("upperMatDesc");

		if (upperMatDesc != null) {
			setUpperMatDesc(upperMatDesc);
		}
	}

	/**
	* Returns the primary key of this catalogo.
	*
	* @return the primary key of this catalogo
	*/
	public long getPrimaryKey() {
		return _catalogo.getPrimaryKey();
	}

	/**
	* Sets the primary key of this catalogo.
	*
	* @param primaryKey the primary key of this catalogo
	*/
	public void setPrimaryKey(long primaryKey) {
		_catalogo.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the catalogo ID of this catalogo.
	*
	* @return the catalogo ID of this catalogo
	*/
	public long getCatalogoId() {
		return _catalogo.getCatalogoId();
	}

	/**
	* Sets the catalogo ID of this catalogo.
	*
	* @param catalogoId the catalogo ID of this catalogo
	*/
	public void setCatalogoId(long catalogoId) {
		_catalogo.setCatalogoId(catalogoId);
	}

	/**
	* Returns the chiave inforecord of this catalogo.
	*
	* @return the chiave inforecord of this catalogo
	*/
	public java.lang.String getChiaveInforecord() {
		return _catalogo.getChiaveInforecord();
	}

	/**
	* Sets the chiave inforecord of this catalogo.
	*
	* @param chiaveInforecord the chiave inforecord of this catalogo
	*/
	public void setChiaveInforecord(java.lang.String chiaveInforecord) {
		_catalogo.setChiaveInforecord(chiaveInforecord);
	}

	/**
	* Returns the data creazione of this catalogo.
	*
	* @return the data creazione of this catalogo
	*/
	public java.util.Date getDataCreazione() {
		return _catalogo.getDataCreazione();
	}

	/**
	* Sets the data creazione of this catalogo.
	*
	* @param dataCreazione the data creazione of this catalogo
	*/
	public void setDataCreazione(java.util.Date dataCreazione) {
		_catalogo.setDataCreazione(dataCreazione);
	}

	/**
	* Returns the data fine validita of this catalogo.
	*
	* @return the data fine validita of this catalogo
	*/
	public java.util.Date getDataFineValidita() {
		return _catalogo.getDataFineValidita();
	}

	/**
	* Sets the data fine validita of this catalogo.
	*
	* @param dataFineValidita the data fine validita of this catalogo
	*/
	public void setDataFineValidita(java.util.Date dataFineValidita) {
		_catalogo.setDataFineValidita(dataFineValidita);
	}

	/**
	* Returns the divisione of this catalogo.
	*
	* @return the divisione of this catalogo
	*/
	public java.lang.String getDivisione() {
		return _catalogo.getDivisione();
	}

	/**
	* Sets the divisione of this catalogo.
	*
	* @param divisione the divisione of this catalogo
	*/
	public void setDivisione(java.lang.String divisione) {
		_catalogo.setDivisione(divisione);
	}

	/**
	* Returns the flg canc of this catalogo.
	*
	* @return the flg canc of this catalogo
	*/
	public boolean getFlgCanc() {
		return _catalogo.getFlgCanc();
	}

	/**
	* Returns <code>true</code> if this catalogo is flg canc.
	*
	* @return <code>true</code> if this catalogo is flg canc; <code>false</code> otherwise
	*/
	public boolean isFlgCanc() {
		return _catalogo.isFlgCanc();
	}

	/**
	* Sets whether this catalogo is flg canc.
	*
	* @param flgCanc the flg canc of this catalogo
	*/
	public void setFlgCanc(boolean flgCanc) {
		_catalogo.setFlgCanc(flgCanc);
	}

	/**
	* Returns the forn fin code of this catalogo.
	*
	* @return the forn fin code of this catalogo
	*/
	public java.lang.String getFornFinCode() {
		return _catalogo.getFornFinCode();
	}

	/**
	* Sets the forn fin code of this catalogo.
	*
	* @param fornFinCode the forn fin code of this catalogo
	*/
	public void setFornFinCode(java.lang.String fornFinCode) {
		_catalogo.setFornFinCode(fornFinCode);
	}

	/**
	* Returns the fornitore code of this catalogo.
	*
	* @return the fornitore code of this catalogo
	*/
	public java.lang.String getFornitoreCode() {
		return _catalogo.getFornitoreCode();
	}

	/**
	* Sets the fornitore code of this catalogo.
	*
	* @param fornitoreCode the fornitore code of this catalogo
	*/
	public void setFornitoreCode(java.lang.String fornitoreCode) {
		_catalogo.setFornitoreCode(fornitoreCode);
	}

	/**
	* Returns the fornitore desc of this catalogo.
	*
	* @return the fornitore desc of this catalogo
	*/
	public java.lang.String getFornitoreDesc() {
		return _catalogo.getFornitoreDesc();
	}

	/**
	* Sets the fornitore desc of this catalogo.
	*
	* @param fornitoreDesc the fornitore desc of this catalogo
	*/
	public void setFornitoreDesc(java.lang.String fornitoreDesc) {
		_catalogo.setFornitoreDesc(fornitoreDesc);
	}

	/**
	* Returns the mat cat lvl2 of this catalogo.
	*
	* @return the mat cat lvl2 of this catalogo
	*/
	public java.lang.String getMatCatLvl2() {
		return _catalogo.getMatCatLvl2();
	}

	/**
	* Sets the mat cat lvl2 of this catalogo.
	*
	* @param matCatLvl2 the mat cat lvl2 of this catalogo
	*/
	public void setMatCatLvl2(java.lang.String matCatLvl2) {
		_catalogo.setMatCatLvl2(matCatLvl2);
	}

	/**
	* Returns the mat cat lvl2 desc of this catalogo.
	*
	* @return the mat cat lvl2 desc of this catalogo
	*/
	public java.lang.String getMatCatLvl2Desc() {
		return _catalogo.getMatCatLvl2Desc();
	}

	/**
	* Sets the mat cat lvl2 desc of this catalogo.
	*
	* @param matCatLvl2Desc the mat cat lvl2 desc of this catalogo
	*/
	public void setMatCatLvl2Desc(java.lang.String matCatLvl2Desc) {
		_catalogo.setMatCatLvl2Desc(matCatLvl2Desc);
	}

	/**
	* Returns the mat cat lvl4 of this catalogo.
	*
	* @return the mat cat lvl4 of this catalogo
	*/
	public java.lang.String getMatCatLvl4() {
		return _catalogo.getMatCatLvl4();
	}

	/**
	* Sets the mat cat lvl4 of this catalogo.
	*
	* @param matCatLvl4 the mat cat lvl4 of this catalogo
	*/
	public void setMatCatLvl4(java.lang.String matCatLvl4) {
		_catalogo.setMatCatLvl4(matCatLvl4);
	}

	/**
	* Returns the mat cat lvl4 desc of this catalogo.
	*
	* @return the mat cat lvl4 desc of this catalogo
	*/
	public java.lang.String getMatCatLvl4Desc() {
		return _catalogo.getMatCatLvl4Desc();
	}

	/**
	* Sets the mat cat lvl4 desc of this catalogo.
	*
	* @param matCatLvl4Desc the mat cat lvl4 desc of this catalogo
	*/
	public void setMatCatLvl4Desc(java.lang.String matCatLvl4Desc) {
		_catalogo.setMatCatLvl4Desc(matCatLvl4Desc);
	}

	/**
	* Returns the mat cod of this catalogo.
	*
	* @return the mat cod of this catalogo
	*/
	public java.lang.String getMatCod() {
		return _catalogo.getMatCod();
	}

	/**
	* Sets the mat cod of this catalogo.
	*
	* @param matCod the mat cod of this catalogo
	*/
	public void setMatCod(java.lang.String matCod) {
		_catalogo.setMatCod(matCod);
	}

	/**
	* Returns the mat cod forn finale of this catalogo.
	*
	* @return the mat cod forn finale of this catalogo
	*/
	public java.lang.String getMatCodFornFinale() {
		return _catalogo.getMatCodFornFinale();
	}

	/**
	* Sets the mat cod forn finale of this catalogo.
	*
	* @param matCodFornFinale the mat cod forn finale of this catalogo
	*/
	public void setMatCodFornFinale(java.lang.String matCodFornFinale) {
		_catalogo.setMatCodFornFinale(matCodFornFinale);
	}

	/**
	* Returns the mat desc of this catalogo.
	*
	* @return the mat desc of this catalogo
	*/
	public java.lang.String getMatDesc() {
		return _catalogo.getMatDesc();
	}

	/**
	* Sets the mat desc of this catalogo.
	*
	* @param matDesc the mat desc of this catalogo
	*/
	public void setMatDesc(java.lang.String matDesc) {
		_catalogo.setMatDesc(matDesc);
	}

	/**
	* Returns the prezzo of this catalogo.
	*
	* @return the prezzo of this catalogo
	*/
	public java.lang.String getPrezzo() {
		return _catalogo.getPrezzo();
	}

	/**
	* Sets the prezzo of this catalogo.
	*
	* @param prezzo the prezzo of this catalogo
	*/
	public void setPrezzo(java.lang.String prezzo) {
		_catalogo.setPrezzo(prezzo);
	}

	/**
	* Returns the purch organiz of this catalogo.
	*
	* @return the purch organiz of this catalogo
	*/
	public java.lang.String getPurchOrganiz() {
		return _catalogo.getPurchOrganiz();
	}

	/**
	* Sets the purch organiz of this catalogo.
	*
	* @param purchOrganiz the purch organiz of this catalogo
	*/
	public void setPurchOrganiz(java.lang.String purchOrganiz) {
		_catalogo.setPurchOrganiz(purchOrganiz);
	}

	/**
	* Returns the um of this catalogo.
	*
	* @return the um of this catalogo
	*/
	public java.lang.String getUm() {
		return _catalogo.getUm();
	}

	/**
	* Sets the um of this catalogo.
	*
	* @param um the um of this catalogo
	*/
	public void setUm(java.lang.String um) {
		_catalogo.setUm(um);
	}

	/**
	* Returns the um prezzo of this catalogo.
	*
	* @return the um prezzo of this catalogo
	*/
	public java.lang.String getUmPrezzo() {
		return _catalogo.getUmPrezzo();
	}

	/**
	* Sets the um prezzo of this catalogo.
	*
	* @param umPrezzo the um prezzo of this catalogo
	*/
	public void setUmPrezzo(java.lang.String umPrezzo) {
		_catalogo.setUmPrezzo(umPrezzo);
	}

	/**
	* Returns the um prezzo po of this catalogo.
	*
	* @return the um prezzo po of this catalogo
	*/
	public java.lang.String getUmPrezzoPo() {
		return _catalogo.getUmPrezzoPo();
	}

	/**
	* Sets the um prezzo po of this catalogo.
	*
	* @param umPrezzoPo the um prezzo po of this catalogo
	*/
	public void setUmPrezzoPo(java.lang.String umPrezzoPo) {
		_catalogo.setUmPrezzoPo(umPrezzoPo);
	}

	/**
	* Returns the valuta of this catalogo.
	*
	* @return the valuta of this catalogo
	*/
	public java.lang.String getValuta() {
		return _catalogo.getValuta();
	}

	/**
	* Sets the valuta of this catalogo.
	*
	* @param valuta the valuta of this catalogo
	*/
	public void setValuta(java.lang.String valuta) {
		_catalogo.setValuta(valuta);
	}

	/**
	* Returns the upper mat desc of this catalogo.
	*
	* @return the upper mat desc of this catalogo
	*/
	public java.lang.String getUpperMatDesc() {
		return _catalogo.getUpperMatDesc();
	}

	/**
	* Sets the upper mat desc of this catalogo.
	*
	* @param upperMatDesc the upper mat desc of this catalogo
	*/
	public void setUpperMatDesc(java.lang.String upperMatDesc) {
		_catalogo.setUpperMatDesc(upperMatDesc);
	}

	public boolean isNew() {
		return _catalogo.isNew();
	}

	public void setNew(boolean n) {
		_catalogo.setNew(n);
	}

	public boolean isCachedModel() {
		return _catalogo.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_catalogo.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _catalogo.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _catalogo.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_catalogo.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _catalogo.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_catalogo.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new CatalogoWrapper((Catalogo)_catalogo.clone());
	}

	public int compareTo(Catalogo catalogo) {
		return _catalogo.compareTo(catalogo);
	}

	@Override
	public int hashCode() {
		return _catalogo.hashCode();
	}

	public com.liferay.portal.model.CacheModel<Catalogo> toCacheModel() {
		return _catalogo.toCacheModel();
	}

	public Catalogo toEscapedModel() {
		return new CatalogoWrapper(_catalogo.toEscapedModel());
	}

	public Catalogo toUnescapedModel() {
		return new CatalogoWrapper(_catalogo.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _catalogo.toString();
	}

	public java.lang.String toXmlString() {
		return _catalogo.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_catalogo.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CatalogoWrapper)) {
			return false;
		}

		CatalogoWrapper catalogoWrapper = (CatalogoWrapper)obj;

		if (Validator.equals(_catalogo, catalogoWrapper._catalogo)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Catalogo getWrappedCatalogo() {
		return _catalogo;
	}

	public Catalogo getWrappedModel() {
		return _catalogo;
	}

	public void resetOriginalValues() {
		_catalogo.resetOriginalValues();
	}

	private Catalogo _catalogo;
}