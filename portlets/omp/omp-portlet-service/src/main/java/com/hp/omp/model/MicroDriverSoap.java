package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class MicroDriverSoap implements Serializable {
    private long _microDriverId;
    private String _microDriverName;
    private boolean _display;

    public MicroDriverSoap() {
    }

    public static MicroDriverSoap toSoapModel(MicroDriver model) {
        MicroDriverSoap soapModel = new MicroDriverSoap();

        soapModel.setMicroDriverId(model.getMicroDriverId());
        soapModel.setMicroDriverName(model.getMicroDriverName());
        soapModel.setDisplay(model.getDisplay());

        return soapModel;
    }

    public static MicroDriverSoap[] toSoapModels(MicroDriver[] models) {
        MicroDriverSoap[] soapModels = new MicroDriverSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static MicroDriverSoap[][] toSoapModels(MicroDriver[][] models) {
        MicroDriverSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new MicroDriverSoap[models.length][models[0].length];
        } else {
            soapModels = new MicroDriverSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static MicroDriverSoap[] toSoapModels(List<MicroDriver> models) {
        List<MicroDriverSoap> soapModels = new ArrayList<MicroDriverSoap>(models.size());

        for (MicroDriver model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new MicroDriverSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _microDriverId;
    }

    public void setPrimaryKey(long pk) {
        setMicroDriverId(pk);
    }

    public long getMicroDriverId() {
        return _microDriverId;
    }

    public void setMicroDriverId(long microDriverId) {
        _microDriverId = microDriverId;
    }

    public String getMicroDriverName() {
        return _microDriverName;
    }

    public void setMicroDriverName(String microDriverName) {
        _microDriverName = microDriverName;
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;
    }
}
