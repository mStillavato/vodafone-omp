package com.hp.omp.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the wbs code local service. This utility wraps {@link com.hp.omp.service.impl.WbsCodeLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see WbsCodeLocalService
 * @see com.hp.omp.service.base.WbsCodeLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.WbsCodeLocalServiceImpl
 * @generated
 */
public class WbsCodeLocalServiceUtil {
	private static WbsCodeLocalService _service;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.hp.omp.service.impl.WbsCodeLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the wbs code to the database. Also notifies the appropriate model listeners.
	 *
	 * @param wbsCode the wbs code
	 * @return the wbs code that was added
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.WbsCode addWbsCode(
			com.hp.omp.model.WbsCode wbsCode)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addWbsCode(wbsCode);
	}

	/**
	 * Creates a new wbs code with the primary key. Does not add the wbs code to the database.
	 *
	 * @param wbsCodeId the primary key for the new wbs code
	 * @return the new wbs code
	 */
	public static com.hp.omp.model.WbsCode createWbsCode(long wbsCodeId) {
		return getService().createWbsCode(wbsCodeId);
	}

	/**
	 * Deletes the wbs code with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param wbsCodeId the primary key of the wbs code
	 * @return the wbs code that was removed
	 * @throws PortalException if a wbs code with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.WbsCode deleteWbsCode(long wbsCodeId)
			throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteWbsCode(wbsCodeId);
	}

	/**
	 * Deletes the wbs code from the database. Also notifies the appropriate model listeners.
	 *
	 * @param wbsCode the wbs code
	 * @return the wbs code that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.WbsCode deleteWbsCode(
			com.hp.omp.model.WbsCode wbsCode)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteWbsCode(wbsCode);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
			int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
			int end,
			com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				.dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows that match the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows that match the dynamic query
	 * @throws SystemException if a system exception occurred
	 */
	public static long dynamicQueryCount(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static com.hp.omp.model.WbsCode fetchWbsCode(long wbsCodeId)
			throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchWbsCode(wbsCodeId);
	}

	/**
	 * Returns the wbs code with the primary key.
	 *
	 * @param wbsCodeId the primary key of the wbs code
	 * @return the wbs code
	 * @throws PortalException if a wbs code with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.WbsCode getWbsCode(long wbsCodeId)
			throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getWbsCode(wbsCodeId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
					throws com.liferay.portal.kernel.exception.PortalException,
					com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns a range of all the wbs codes.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of wbs codes
	 * @param end the upper bound of the range of wbs codes (not inclusive)
	 * @return the range of wbs codes
	 * @throws SystemException if a system exception occurred
	 */
	public static java.util.List<com.hp.omp.model.WbsCode> getWbsCodes(
			int start, int end)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getWbsCodes(start, end);
	}

	/**
	 * Returns the number of wbs codes.
	 *
	 * @return the number of wbs codes
	 * @throws SystemException if a system exception occurred
	 */
	public static int getWbsCodesCount()
			throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getWbsCodesCount();
	}

	public static int getWbsCodesCountList(String searchFilter)
			throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getWbsCodesCountList(searchFilter);
	}

	/**
	 * Updates the wbs code in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param wbsCode the wbs code
	 * @return the wbs code that was updated
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.WbsCode updateWbsCode(
			com.hp.omp.model.WbsCode wbsCode)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateWbsCode(wbsCode);
	}

	/**
	 * Updates the wbs code in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param wbsCode the wbs code
	 * @param merge whether to merge the wbs code with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	 * @return the wbs code that was updated
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.WbsCode updateWbsCode(
			com.hp.omp.model.WbsCode wbsCode, boolean merge)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateWbsCode(wbsCode, merge);
	}

	/**
	 * Returns the Spring bean ID for this bean.
	 *
	 * @return the Spring bean ID for this bean
	 */
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	 * Sets the Spring bean ID for this bean.
	 *
	 * @param beanIdentifier the Spring bean ID for this bean
	 */
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
			java.lang.String[] parameterTypes, java.lang.Object[] arguments)
					throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static com.hp.omp.model.WbsCode getWbsCodeByName(
			java.lang.String name)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getWbsCodeByName(name);
	}

	public static void clearService() {
		_service = null;
	}

	public static WbsCodeLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					WbsCodeLocalService.class.getName());

			if (invokableLocalService instanceof WbsCodeLocalService) {
				_service = (WbsCodeLocalService) invokableLocalService;
			} else {
				_service = new WbsCodeLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(WbsCodeLocalServiceUtil.class,
					"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	 public void setService(WbsCodeLocalService service) {
	}

	public static java.util.List<com.hp.omp.model.WbsCode> getWbsCodesList(
			int start, int end, String searchFilter)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getWbsCodesList(start, end, searchFilter);
	}
	
	public static void exportWbsCodesAsExcel(
			java.util.List<com.hp.omp.model.WbsCode> listWbsCodes,
			java.util.List<java.lang.Object> header, java.io.OutputStream out)
					throws com.liferay.portal.kernel.exception.SystemException {
		getService().exportWbsCodesAsExcel(listWbsCodes, header, out);
	}
}
