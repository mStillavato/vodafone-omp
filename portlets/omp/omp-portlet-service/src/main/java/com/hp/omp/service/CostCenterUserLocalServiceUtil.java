package com.hp.omp.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the cost center user local service. This utility wraps {@link com.hp.omp.service.impl.CostCenterUserLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see CostCenterUserLocalService
 * @see com.hp.omp.service.base.CostCenterUserLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.CostCenterUserLocalServiceImpl
 * @generated
 */
public class CostCenterUserLocalServiceUtil {
    private static CostCenterUserLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.hp.omp.service.impl.CostCenterUserLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the cost center user to the database. Also notifies the appropriate model listeners.
    *
    * @param costCenterUser the cost center user
    * @return the cost center user that was added
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.CostCenterUser addCostCenterUser(
        com.hp.omp.model.CostCenterUser costCenterUser)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addCostCenterUser(costCenterUser);
    }

    /**
    * Creates a new cost center user with the primary key. Does not add the cost center user to the database.
    *
    * @param costCenterUserId the primary key for the new cost center user
    * @return the new cost center user
    */
    public static com.hp.omp.model.CostCenterUser createCostCenterUser(
        long costCenterUserId) {
        return getService().createCostCenterUser(costCenterUserId);
    }

    /**
    * Deletes the cost center user with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param costCenterUserId the primary key of the cost center user
    * @return the cost center user that was removed
    * @throws PortalException if a cost center user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.CostCenterUser deleteCostCenterUser(
        long costCenterUserId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteCostCenterUser(costCenterUserId);
    }

    /**
    * Deletes the cost center user from the database. Also notifies the appropriate model listeners.
    *
    * @param costCenterUser the cost center user
    * @return the cost center user that was removed
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.CostCenterUser deleteCostCenterUser(
        com.hp.omp.model.CostCenterUser costCenterUser)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteCostCenterUser(costCenterUser);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    public static com.hp.omp.model.CostCenterUser fetchCostCenterUser(
        long costCenterUserId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchCostCenterUser(costCenterUserId);
    }

    /**
    * Returns the cost center user with the primary key.
    *
    * @param costCenterUserId the primary key of the cost center user
    * @return the cost center user
    * @throws PortalException if a cost center user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.CostCenterUser getCostCenterUser(
        long costCenterUserId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getCostCenterUser(costCenterUserId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the cost center users.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of cost center users
    * @param end the upper bound of the range of cost center users (not inclusive)
    * @return the range of cost center users
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.CostCenterUser> getCostCenterUsers(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getCostCenterUsers(start, end);
    }

    /**
    * Returns the number of cost center users.
    *
    * @return the number of cost center users
    * @throws SystemException if a system exception occurred
    */
    public static int getCostCenterUsersCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getCostCenterUsersCount();
    }

    /**
    * Updates the cost center user in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param costCenterUser the cost center user
    * @return the cost center user that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.CostCenterUser updateCostCenterUser(
        com.hp.omp.model.CostCenterUser costCenterUser)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateCostCenterUser(costCenterUser);
    }

    /**
    * Updates the cost center user in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param costCenterUser the cost center user
    * @param merge whether to merge the cost center user with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the cost center user that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.CostCenterUser updateCostCenterUser(
        com.hp.omp.model.CostCenterUser costCenterUser, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateCostCenterUser(costCenterUser, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static com.hp.omp.model.CostCenterUser getCostCenterUserByCostCenterAndUser(
        long costCenterId, long userId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .getCostCenterUserByCostCenterAndUser(costCenterId, userId);
    }

    public static com.hp.omp.model.CostCenterUser getCostCenterUserByUserId(
        long userId) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getCostCenterUserByUserId(userId);
    }

    public static com.hp.omp.model.CostCenterUser getCostCenterUserByCostCenterId(
        long costCenterId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getCostCenterUserByCostCenterId(costCenterId);
    }

    public static void clearService() {
        _service = null;
    }

    public static CostCenterUserLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    CostCenterUserLocalService.class.getName());

            if (invokableLocalService instanceof CostCenterUserLocalService) {
                _service = (CostCenterUserLocalService) invokableLocalService;
            } else {
                _service = new CostCenterUserLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(CostCenterUserLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated
     */
    public void setService(CostCenterUserLocalService service) {
    }
}
