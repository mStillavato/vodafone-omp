package com.hp.omp.service.persistence;

public interface BudgetCategoryFinder {
    
    public java.util.List<com.hp.omp.model.BudgetCategory> getBudgetCategoryList(
    		int start, int end, String searchFilter)
            throws com.liferay.portal.kernel.exception.SystemException;
}
