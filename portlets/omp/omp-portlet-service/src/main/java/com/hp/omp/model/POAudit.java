package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the POAudit service. Represents a row in the &quot;PO_AUDIT&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see POAuditModel
 * @see com.hp.omp.model.impl.POAuditImpl
 * @see com.hp.omp.model.impl.POAuditModelImpl
 * @generated
 */
public interface POAudit extends POAuditModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.POAuditImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
