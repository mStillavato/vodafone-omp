package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class ODNPCodeSoap implements Serializable {
    private long _odnpCodeId;
    private String _odnpCodeName;

    public ODNPCodeSoap() {
    }

    public static ODNPCodeSoap toSoapModel(ODNPCode model) {
        ODNPCodeSoap soapModel = new ODNPCodeSoap();

        soapModel.setOdnpCodeId(model.getOdnpCodeId());
        soapModel.setOdnpCodeName(model.getOdnpCodeName());

        return soapModel;
    }

    public static ODNPCodeSoap[] toSoapModels(ODNPCode[] models) {
        ODNPCodeSoap[] soapModels = new ODNPCodeSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static ODNPCodeSoap[][] toSoapModels(ODNPCode[][] models) {
        ODNPCodeSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new ODNPCodeSoap[models.length][models[0].length];
        } else {
            soapModels = new ODNPCodeSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static ODNPCodeSoap[] toSoapModels(List<ODNPCode> models) {
        List<ODNPCodeSoap> soapModels = new ArrayList<ODNPCodeSoap>(models.size());

        for (ODNPCode model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new ODNPCodeSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _odnpCodeId;
    }

    public void setPrimaryKey(long pk) {
        setOdnpCodeId(pk);
    }

    public long getOdnpCodeId() {
        return _odnpCodeId;
    }

    public void setOdnpCodeId(long odnpCodeId) {
        _odnpCodeId = odnpCodeId;
    }

    public String getOdnpCodeName() {
        return _odnpCodeName;
    }

    public void setOdnpCodeName(String odnpCodeName) {
        _odnpCodeName = odnpCodeName;
    }
}
