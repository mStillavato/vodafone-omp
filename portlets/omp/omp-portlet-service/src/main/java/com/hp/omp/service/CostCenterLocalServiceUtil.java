package com.hp.omp.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the cost center local service. This utility wraps {@link com.hp.omp.service.impl.CostCenterLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see CostCenterLocalService
 * @see com.hp.omp.service.base.CostCenterLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.CostCenterLocalServiceImpl
 * @generated
 */
public class CostCenterLocalServiceUtil {
    private static CostCenterLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.hp.omp.service.impl.CostCenterLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the cost center to the database. Also notifies the appropriate model listeners.
    *
    * @param costCenter the cost center
    * @return the cost center that was added
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.CostCenter addCostCenter(
        com.hp.omp.model.CostCenter costCenter)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addCostCenter(costCenter);
    }

    /**
    * Creates a new cost center with the primary key. Does not add the cost center to the database.
    *
    * @param costCenterId the primary key for the new cost center
    * @return the new cost center
    */
    public static com.hp.omp.model.CostCenter createCostCenter(
        long costCenterId) {
        return getService().createCostCenter(costCenterId);
    }

    /**
    * Deletes the cost center with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param costCenterId the primary key of the cost center
    * @return the cost center that was removed
    * @throws PortalException if a cost center with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.CostCenter deleteCostCenter(
        long costCenterId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteCostCenter(costCenterId);
    }

    /**
    * Deletes the cost center from the database. Also notifies the appropriate model listeners.
    *
    * @param costCenter the cost center
    * @return the cost center that was removed
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.CostCenter deleteCostCenter(
        com.hp.omp.model.CostCenter costCenter)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteCostCenter(costCenter);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    public static com.hp.omp.model.CostCenter fetchCostCenter(long costCenterId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchCostCenter(costCenterId);
    }

    /**
    * Returns the cost center with the primary key.
    *
    * @param costCenterId the primary key of the cost center
    * @return the cost center
    * @throws PortalException if a cost center with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.CostCenter getCostCenter(long costCenterId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getCostCenter(costCenterId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the cost centers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of cost centers
    * @param end the upper bound of the range of cost centers (not inclusive)
    * @return the range of cost centers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.CostCenter> getCostCenters(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getCostCenters(start, end);
    }

    /**
    * Returns the number of cost centers.
    *
    * @return the number of cost centers
    * @throws SystemException if a system exception occurred
    */
    public static int getCostCentersCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getCostCentersCount();
    }

    /**
    * Updates the cost center in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param costCenter the cost center
    * @return the cost center that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.CostCenter updateCostCenter(
        com.hp.omp.model.CostCenter costCenter)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateCostCenter(costCenter);
    }

    /**
    * Updates the cost center in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param costCenter the cost center
    * @param merge whether to merge the cost center with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the cost center that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.CostCenter updateCostCenter(
        com.hp.omp.model.CostCenter costCenter, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateCostCenter(costCenter, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static com.hp.omp.model.CostCenter getCostCenterByName(
        java.lang.String name, int gruppoUsers)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getCostCenterByName(name, gruppoUsers);
    }

    public static void clearService() {
        _service = null;
    }

    public static CostCenterLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    CostCenterLocalService.class.getName());

            if (invokableLocalService instanceof CostCenterLocalService) {
                _service = (CostCenterLocalService) invokableLocalService;
            } else {
                _service = new CostCenterLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(CostCenterLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated
     */
    public void setService(CostCenterLocalService service) {
    }
    
    public static java.util.List<com.hp.omp.model.CostCenter> getCostCentersList(
            int start, int end, String searchFilter, int gruppoUsers)
            throws com.liferay.portal.kernel.exception.SystemException {
            return getService().getCostCentersList(start, end, searchFilter, gruppoUsers);
        }
}
