package com.hp.omp.service;

import java.io.OutputStream;
import java.util.List;

import com.hp.omp.model.Vendor;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link VendorLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       VendorLocalService
 * @generated
 */
public class VendorLocalServiceWrapper implements VendorLocalService,
    ServiceWrapper<VendorLocalService> {
    private VendorLocalService _vendorLocalService;

    public VendorLocalServiceWrapper(VendorLocalService vendorLocalService) {
        _vendorLocalService = vendorLocalService;
    }

    /**
    * Adds the vendor to the database. Also notifies the appropriate model listeners.
    *
    * @param vendor the vendor
    * @return the vendor that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Vendor addVendor(com.hp.omp.model.Vendor vendor)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vendorLocalService.addVendor(vendor);
    }

    /**
    * Creates a new vendor with the primary key. Does not add the vendor to the database.
    *
    * @param vendorId the primary key for the new vendor
    * @return the new vendor
    */
    public com.hp.omp.model.Vendor createVendor(long vendorId) {
        return _vendorLocalService.createVendor(vendorId);
    }

    /**
    * Deletes the vendor with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vendorId the primary key of the vendor
    * @return the vendor that was removed
    * @throws PortalException if a vendor with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Vendor deleteVendor(long vendorId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vendorLocalService.deleteVendor(vendorId);
    }

    /**
    * Deletes the vendor from the database. Also notifies the appropriate model listeners.
    *
    * @param vendor the vendor
    * @return the vendor that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Vendor deleteVendor(com.hp.omp.model.Vendor vendor)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vendorLocalService.deleteVendor(vendor);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _vendorLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vendorLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _vendorLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vendorLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vendorLocalService.dynamicQueryCount(dynamicQuery);
    }

    public com.hp.omp.model.Vendor fetchVendor(long vendorId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vendorLocalService.fetchVendor(vendorId);
    }

    /**
    * Returns the vendor with the primary key.
    *
    * @param vendorId the primary key of the vendor
    * @return the vendor
    * @throws PortalException if a vendor with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Vendor getVendor(long vendorId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vendorLocalService.getVendor(vendorId);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vendorLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the vendors.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of vendors
    * @param end the upper bound of the range of vendors (not inclusive)
    * @return the range of vendors
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.Vendor> getVendors(int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vendorLocalService.getVendors(start, end);
    }

    /**
    * Returns the number of vendors.
    *
    * @return the number of vendors
    * @throws SystemException if a system exception occurred
    */
    public int getVendorsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vendorLocalService.getVendorsCount();
    }

    /**
    * Updates the vendor in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param vendor the vendor
    * @return the vendor that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Vendor updateVendor(com.hp.omp.model.Vendor vendor)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vendorLocalService.updateVendor(vendor);
    }

    /**
    * Updates the vendor in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param vendor the vendor
    * @param merge whether to merge the vendor with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the vendor that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Vendor updateVendor(
        com.hp.omp.model.Vendor vendor, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vendorLocalService.updateVendor(vendor, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _vendorLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _vendorLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _vendorLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    public com.hp.omp.model.Vendor getVendorByName(java.lang.String name, int gruppoUsers)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vendorLocalService.getVendorByName(name, gruppoUsers);
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public VendorLocalService getWrappedVendorLocalService() {
        return _vendorLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedVendorLocalService(
        VendorLocalService vendorLocalService) {
        _vendorLocalService = vendorLocalService;
    }

    public VendorLocalService getWrappedService() {
        return _vendorLocalService;
    }

    public void setWrappedService(VendorLocalService vendorLocalService) {
        _vendorLocalService = vendorLocalService;
    }

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Vendor> getVendorsList(int start, int end, String searchFilter, int gruppoUsers)
			throws SystemException {
		return _vendorLocalService.getVendorsList(start, end, searchFilter, gruppoUsers);
	}
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Vendor getVendorBySupplierCode(String supplierCode, int gruppoUsers)
			throws SystemException {
		return _vendorLocalService.getVendorBySupplierCode(supplierCode, gruppoUsers);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getVendorsCountList(String searchFilter, int gruppoUsers) throws SystemException {
		return _vendorLocalService.getVendorsCountList(searchFilter, gruppoUsers);
	}

	public void exportVendorsAsExcel(List<Vendor> listVendors,
			List<Object> header, OutputStream out) throws SystemException {
		_vendorLocalService.exportVendorsAsExcel(listVendors, header, out);
	}
}
