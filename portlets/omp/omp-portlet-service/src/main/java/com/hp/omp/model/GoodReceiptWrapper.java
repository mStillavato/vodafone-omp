package com.hp.omp.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link GoodReceipt}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       GoodReceipt
 * @generated
 */
public class GoodReceiptWrapper implements GoodReceipt,
    ModelWrapper<GoodReceipt> {
    private GoodReceipt _goodReceipt;

    public GoodReceiptWrapper(GoodReceipt goodReceipt) {
        _goodReceipt = goodReceipt;
    }

    public Class<?> getModelClass() {
        return GoodReceipt.class;
    }

    public String getModelClassName() {
        return GoodReceipt.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("goodReceiptId", getGoodReceiptId());
        attributes.put("goodReceiptNumber", getGoodReceiptNumber());
        attributes.put("requestDate", getRequestDate());
        attributes.put("percentage", getPercentage());
        attributes.put("grValue", getGrValue());
        attributes.put("registerationDate", getRegisterationDate());
        attributes.put("createdUserId", getCreatedUserId());
        attributes.put("positionId", getPositionId());
        attributes.put("grFile", getGrFile());
        
        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long goodReceiptId = (Long) attributes.get("goodReceiptId");

        if (goodReceiptId != null) {
            setGoodReceiptId(goodReceiptId);
        }

        String goodReceiptNumber = (String) attributes.get("goodReceiptNumber");

        if (goodReceiptNumber != null) {
            setGoodReceiptNumber(goodReceiptNumber);
        }

        Date requestDate = (Date) attributes.get("requestDate");

        if (requestDate != null) {
            setRequestDate(requestDate);
        }

        Double percentage = (Double) attributes.get("percentage");

        if (percentage != null) {
            setPercentage(percentage);
        }

        String grValue = (String) attributes.get("grValue");

        if (grValue != null) {
            setGrValue(grValue);
        }

        Date registerationDate = (Date) attributes.get("registerationDate");

        if (registerationDate != null) {
            setRegisterationDate(registerationDate);
        }

        String createdUserId = (String) attributes.get("createdUserId");

        if (createdUserId != null) {
            setCreatedUserId(createdUserId);
        }

        String positionId = (String) attributes.get("positionId");

        if (positionId != null) {
            setPositionId(positionId);
        }
        
        String grFile = (String) attributes.get("grFile");

        if (grFile != null) {
            setGrFile(grFile);
        }
    }

    /**
    * Returns the primary key of this good receipt.
    *
    * @return the primary key of this good receipt
    */
    public long getPrimaryKey() {
        return _goodReceipt.getPrimaryKey();
    }

    /**
    * Sets the primary key of this good receipt.
    *
    * @param primaryKey the primary key of this good receipt
    */
    public void setPrimaryKey(long primaryKey) {
        _goodReceipt.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the good receipt ID of this good receipt.
    *
    * @return the good receipt ID of this good receipt
    */
    public long getGoodReceiptId() {
        return _goodReceipt.getGoodReceiptId();
    }

    /**
    * Sets the good receipt ID of this good receipt.
    *
    * @param goodReceiptId the good receipt ID of this good receipt
    */
    public void setGoodReceiptId(long goodReceiptId) {
        _goodReceipt.setGoodReceiptId(goodReceiptId);
    }

    /**
    * Returns the good receipt number of this good receipt.
    *
    * @return the good receipt number of this good receipt
    */
    public java.lang.String getGoodReceiptNumber() {
        return _goodReceipt.getGoodReceiptNumber();
    }

    /**
    * Sets the good receipt number of this good receipt.
    *
    * @param goodReceiptNumber the good receipt number of this good receipt
    */
    public void setGoodReceiptNumber(java.lang.String goodReceiptNumber) {
        _goodReceipt.setGoodReceiptNumber(goodReceiptNumber);
    }

    /**
    * Returns the request date of this good receipt.
    *
    * @return the request date of this good receipt
    */
    public java.util.Date getRequestDate() {
        return _goodReceipt.getRequestDate();
    }

    /**
    * Sets the request date of this good receipt.
    *
    * @param requestDate the request date of this good receipt
    */
    public void setRequestDate(java.util.Date requestDate) {
        _goodReceipt.setRequestDate(requestDate);
    }

    /**
    * Returns the percentage of this good receipt.
    *
    * @return the percentage of this good receipt
    */
    public double getPercentage() {
        return _goodReceipt.getPercentage();
    }

    /**
    * Sets the percentage of this good receipt.
    *
    * @param percentage the percentage of this good receipt
    */
    public void setPercentage(double percentage) {
        _goodReceipt.setPercentage(percentage);
    }

    /**
    * Returns the gr value of this good receipt.
    *
    * @return the gr value of this good receipt
    */
    public java.lang.String getGrValue() {
        return _goodReceipt.getGrValue();
    }

    /**
    * Sets the gr value of this good receipt.
    *
    * @param grValue the gr value of this good receipt
    */
    public void setGrValue(java.lang.String grValue) {
        _goodReceipt.setGrValue(grValue);
    }

    /**
    * Returns the registeration date of this good receipt.
    *
    * @return the registeration date of this good receipt
    */
    public java.util.Date getRegisterationDate() {
        return _goodReceipt.getRegisterationDate();
    }

    /**
    * Sets the registeration date of this good receipt.
    *
    * @param registerationDate the registeration date of this good receipt
    */
    public void setRegisterationDate(java.util.Date registerationDate) {
        _goodReceipt.setRegisterationDate(registerationDate);
    }

    /**
    * Returns the created user ID of this good receipt.
    *
    * @return the created user ID of this good receipt
    */
    public java.lang.String getCreatedUserId() {
        return _goodReceipt.getCreatedUserId();
    }

    /**
    * Sets the created user ID of this good receipt.
    *
    * @param createdUserId the created user ID of this good receipt
    */
    public void setCreatedUserId(java.lang.String createdUserId) {
        _goodReceipt.setCreatedUserId(createdUserId);
    }

    /**
    * Returns the position ID of this good receipt.
    *
    * @return the position ID of this good receipt
    */
    public java.lang.String getPositionId() {
        return _goodReceipt.getPositionId();
    }

    /**
    * Sets the position ID of this good receipt.
    *
    * @param positionId the position ID of this good receipt
    */
    public void setPositionId(java.lang.String positionId) {
        _goodReceipt.setPositionId(positionId);
    }

    public boolean isNew() {
        return _goodReceipt.isNew();
    }

    public void setNew(boolean n) {
        _goodReceipt.setNew(n);
    }

    public boolean isCachedModel() {
        return _goodReceipt.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _goodReceipt.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _goodReceipt.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _goodReceipt.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _goodReceipt.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _goodReceipt.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _goodReceipt.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new GoodReceiptWrapper((GoodReceipt) _goodReceipt.clone());
    }

    public int compareTo(GoodReceipt goodReceipt) {
        return _goodReceipt.compareTo(goodReceipt);
    }

    @Override
    public int hashCode() {
        return _goodReceipt.hashCode();
    }

    public com.liferay.portal.model.CacheModel<GoodReceipt> toCacheModel() {
        return _goodReceipt.toCacheModel();
    }

    public GoodReceipt toEscapedModel() {
        return new GoodReceiptWrapper(_goodReceipt.toEscapedModel());
    }

    public GoodReceipt toUnescapedModel() {
        return new GoodReceiptWrapper(_goodReceipt.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _goodReceipt.toString();
    }

    public java.lang.String toXmlString() {
        return _goodReceipt.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _goodReceipt.persist();
    }

    public com.hp.omp.model.custom.GoodReceiptStatus getGoodReceiptStatus() {
        return _goodReceipt.getGoodReceiptStatus();
    }

    public java.lang.String getRequestDateText() {
        return _goodReceipt.getRequestDateText();
    }

    public java.lang.String getRegisterationDateText() {
        return _goodReceipt.getRegisterationDateText();
    }

    public java.lang.String getPercentageText()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceipt.getPercentageText();
    }

    public java.lang.String getFormattedPercentageText()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceipt.getFormattedPercentageText();
    }

    public java.lang.String getFormattedPercentage()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceipt.getFormattedPercentage();
    }

    public java.lang.Double getMaxPercentage()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _goodReceipt.getMaxPercentage();
    }

    public java.lang.String getGrValueText()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException,
            java.lang.NumberFormatException {
        return _goodReceipt.getGrValueText();
    }

    public java.lang.String getFormattedGrValueText()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException,
            java.lang.NumberFormatException {
        return _goodReceipt.getFormattedGrValueText();
    }

    public java.lang.String getFormattedGrValue()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException,
            java.lang.NumberFormatException {
        return _goodReceipt.getFormattedGrValue();
    }

    public com.hp.omp.model.Position getPosition()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _goodReceipt.getPosition();
    }

    public java.lang.String getRequestDateReadonly()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _goodReceipt.getRequestDateReadonly();
    }

    public java.lang.String getPercentageReadonly()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _goodReceipt.getPercentageReadonly();
    }

    public java.lang.String getGrValueReadonly()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _goodReceipt.getGrValueReadonly();
    }

    public java.lang.String getGrNumberReadonly()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _goodReceipt.getGrNumberReadonly();
    }

    public java.lang.String getRegisterationDateReadonly()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _goodReceipt.getRegisterationDateReadonly();
    }

    public java.lang.String antexFieldsStatus()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _goodReceipt.antexFieldsStatus();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof GoodReceiptWrapper)) {
            return false;
        }

        GoodReceiptWrapper goodReceiptWrapper = (GoodReceiptWrapper) obj;

        if (Validator.equals(_goodReceipt, goodReceiptWrapper._goodReceipt)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public GoodReceipt getWrappedGoodReceipt() {
        return _goodReceipt;
    }

    public GoodReceipt getWrappedModel() {
        return _goodReceipt;
    }

    public void resetOriginalValues() {
        _goodReceipt.resetOriginalValues();
    }

	@AutoEscape
	public String getGrFile() {
		return _goodReceipt.getGrFile();
	}

	public void setGrFile(String grFile) {
		_goodReceipt.setGrFile(grFile);
	}
}
