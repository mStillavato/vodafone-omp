/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.model;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link LayoutNotNssCatalogo}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       LayoutNotNssCatalogo
 * @generated
 */
public class LayoutNotNssCatalogoWrapper implements LayoutNotNssCatalogo, ModelWrapper<LayoutNotNssCatalogo> {
	public LayoutNotNssCatalogoWrapper(LayoutNotNssCatalogo layoutNotNssCatalogo) {
		_layoutNotNssCatalogo = layoutNotNssCatalogo;
	}

	public Class<?> getModelClass() {
		return LayoutNotNssCatalogo.class;
	}

	public String getModelClassName() {
		return LayoutNotNssCatalogo.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("layoutNotNssCatalogoId", getLayoutNotNssCatalogoId());
		attributes.put("lineNumber", getLineNumber());
		attributes.put("materialId", getMaterialId());
		attributes.put("materialDesc", getMaterialDesc());
		attributes.put("quantity", getQuantity());
		attributes.put("netPrice", getNetPrice());
		attributes.put("currency", getCurrency());
		attributes.put("deliveryDate", getDeliveryDate());
		attributes.put("deliveryAddress", getDeliveryAddress());
		attributes.put("itemText", getItemText());
		attributes.put("locationEvo", getLocationEvo());
		attributes.put("targaTecnica", getTargaTecnica());
		attributes.put("user_", getUser_());
		attributes.put("vendorCode", getVendorCode());
		attributes.put("catalogoId", getCatalogoId());
		attributes.put("icApproval", getIcApproval());
        attributes.put("categoryCode", getCategoryCode());
        attributes.put("offerNumber", getOfferNumber());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long layoutNotNssCatalogoId = (Long)attributes.get("layoutNotNssCatalogoId");
		if (layoutNotNssCatalogoId != null) {
			setLayoutNotNssCatalogoId(layoutNotNssCatalogoId);
		}

		Long lineNumber = (Long)attributes.get("lineNumber");
		if (lineNumber != null) {
			setLineNumber(lineNumber);
		}
		
		String materialId = (String)attributes.get("materialId");
		if (materialId != null) {
			setMaterialId(materialId);
		}
		
		String materialDesc = (String)attributes.get("materialDesc");
		if (materialDesc != null) {
			setMaterialDesc(materialDesc);
		}
		
		String quantity = (String)attributes.get("quantity");
		if (quantity != null) {
			setQuantity(quantity);
		}
		
		String netPrice = (String)attributes.get("netPrice");
		if (netPrice != null) {
			setNetPrice(netPrice);
		}
		
		String currency = (String)attributes.get("currency");
		if (currency != null) {
			setCurrency(currency);
		}

		Date deliveryDate = (Date)attributes.get("deliveryDate");
		if (deliveryDate != null) {
			setDeliveryDate(deliveryDate);
		}

		String deliveryAddress = (String)attributes.get("deliveryAddress");
		if (deliveryAddress != null) {
			setDeliveryAddress(deliveryAddress);
		}

		String itemText = (String)attributes.get("itemText");
		if (itemText != null) {
			setItemText(itemText);
		}

		String locationEvo = (String)attributes.get("locationEvo");
		if (locationEvo != null) {
			setLocationEvo(locationEvo);
		}

		String targaTecnica = (String)attributes.get("targaTecnica");
		if (targaTecnica != null) {
			setTargaTecnica(targaTecnica);
		}

		String user_ = (String)attributes.get("user_");
		if (user_ != null) {
			setUser_(user_);
		}
		
		String vendorCode = (String)attributes.get("vendorCode");
		if (vendorCode != null) {
			setVendorCode(vendorCode);
		}
		
		Long catalogoId = (Long)attributes.get("catalogoId");
		if (catalogoId != null) {
			setCatalogoId(catalogoId);
		}
		
        String icApproval = (String) attributes.get("icApproval");
        if (icApproval != null) {
            setIcApproval(icApproval);
        }
		
        String categoryCode = (String) attributes.get("categoryCode");
        if (categoryCode != null) {
            setCategoryCode(categoryCode);
        }

        String offerNumber = (String) attributes.get("offerNumber");
        if (offerNumber != null) {
            setOfferNumber(offerNumber);
        }

	}

	/**
	* Returns the primary key of this layoutNotNssCatalogo.
	*
	* @return the primary key of this layoutNotNssCatalogo
	*/
	public long getPrimaryKey() {
		return _layoutNotNssCatalogo.getPrimaryKey();
	}

	/**
	* Sets the primary key of this layoutNotNssCatalogo.
	*
	* @param primaryKey the primary key of this layoutNotNssCatalogo
	*/
	public void setPrimaryKey(long primaryKey) {
		_layoutNotNssCatalogo.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the layoutNotNssCatalogo ID of this layoutNotNssCatalogo.
	*
	* @return the layoutNotNssCatalogo ID of this layoutNotNssCatalogo
	*/
	public long getLayoutNotNssCatalogoId() {
		return _layoutNotNssCatalogo.getLayoutNotNssCatalogoId();
	}

	/**
	* Sets the layoutNotNssCatalogo ID of this layoutNotNssCatalogo.
	*
	* @param layoutNotNssCatalogoId the layoutNotNssCatalogo ID of this layoutNotNssCatalogo
	*/
	public void setLayoutNotNssCatalogoId(long layoutNotNssCatalogoId) {
		_layoutNotNssCatalogo.setLayoutNotNssCatalogoId(layoutNotNssCatalogoId);
	}

	/**
	* Returns the chiave inforecord of this layoutNotNssCatalogo.
	*
	* @return the chiave inforecord of this layoutNotNssCatalogo
	*/
	public long getLineNumber() {
		return _layoutNotNssCatalogo.getLineNumber();
	}

	public void setLineNumber(long lineNumber) {
		_layoutNotNssCatalogo.setLineNumber(lineNumber);
	}

	public java.lang.String getMaterialId() {
		return _layoutNotNssCatalogo.getMaterialId();
	}
	public void setMaterialId(java.lang.String materialId) {
		_layoutNotNssCatalogo.setMaterialId(materialId);
	}
	
	public java.lang.String getMaterialDesc() {
		return _layoutNotNssCatalogo.getMaterialDesc();
	}
	public void setMaterialDesc(java.lang.String materialDesc) {
		_layoutNotNssCatalogo.setMaterialDesc(materialDesc);
	}
	
	public java.lang.String getQuantity() {
		return _layoutNotNssCatalogo.getQuantity();
	}
	public void setQuantity(java.lang.String quantity) {
		_layoutNotNssCatalogo.setQuantity(quantity);
	}
	
	public java.lang.String getNetPrice() {
		return _layoutNotNssCatalogo.getNetPrice();
	}
	public void setNetPrice(java.lang.String netPrice) {
		_layoutNotNssCatalogo.setNetPrice(netPrice);
	}
	
	public java.lang.String getCurrency() {
		return _layoutNotNssCatalogo.getCurrency();
	}
	public void setCurrency(java.lang.String currency) {
		_layoutNotNssCatalogo.setCurrency(currency);
	}
	
	public java.util.Date getDeliveryDate() {
		return _layoutNotNssCatalogo.getDeliveryDate();
	}
	public void setDeliveryDate(java.util.Date deliveryDate) {
		_layoutNotNssCatalogo.setDeliveryDate(deliveryDate);
	}
	
	public java.lang.String getDeliveryAddress() {
		return _layoutNotNssCatalogo.getDeliveryAddress();
	}
	public void setDeliveryAddress(java.lang.String deliveryAddress) {
		_layoutNotNssCatalogo.setDeliveryAddress(deliveryAddress);
	}
	
	public java.lang.String getItemText() {
		return _layoutNotNssCatalogo.getItemText();
	}
	public void setItemText(java.lang.String itemText) {
		_layoutNotNssCatalogo.setItemText(itemText);
	}	
	
	public java.lang.String getLocationEvo() {
		return _layoutNotNssCatalogo.getLocationEvo();
	}
	public void setLocationEvo(java.lang.String locationEvo) {
		_layoutNotNssCatalogo.setLocationEvo(locationEvo);
	}
	
	public java.lang.String getTargaTecnica() {
		return _layoutNotNssCatalogo.getTargaTecnica();
	}
	public void setTargaTecnica(java.lang.String targaTecnica) {
		_layoutNotNssCatalogo.setTargaTecnica(targaTecnica);
	}
	
	public java.lang.String getUser_() {
		return _layoutNotNssCatalogo.getUser_();
	}
	public void setUser_(java.lang.String user_) {
		_layoutNotNssCatalogo.setUser_(user_);
	}

	public java.lang.String getVendorCode() {
		return _layoutNotNssCatalogo.getVendorCode();
	}
	public void setVendorCode(java.lang.String vendorCode) {
		_layoutNotNssCatalogo.setVendorCode(vendorCode);
	}
	
	public long getCatalogoId() {
		return _layoutNotNssCatalogo.getCatalogoId();
	}
	public void setCatalogoId(long catalogoId) {
		_layoutNotNssCatalogo.setCatalogoId(catalogoId);
	}
	
	public boolean isNew() {
		return _layoutNotNssCatalogo.isNew();
	}

	public void setNew(boolean n) {
		_layoutNotNssCatalogo.setNew(n);
	}

	public boolean isCachedModel() {
		return _layoutNotNssCatalogo.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_layoutNotNssCatalogo.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _layoutNotNssCatalogo.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _layoutNotNssCatalogo.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_layoutNotNssCatalogo.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _layoutNotNssCatalogo.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_layoutNotNssCatalogo.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new LayoutNotNssCatalogoWrapper((LayoutNotNssCatalogo)_layoutNotNssCatalogo.clone());
	}

	public int compareTo(LayoutNotNssCatalogo layoutNotNssCatalogo) {
		return _layoutNotNssCatalogo.compareTo(layoutNotNssCatalogo);
	}

	@Override
	public int hashCode() {
		return _layoutNotNssCatalogo.hashCode();
	}

	public com.liferay.portal.model.CacheModel<LayoutNotNssCatalogo> toCacheModel() {
		return _layoutNotNssCatalogo.toCacheModel();
	}

	public LayoutNotNssCatalogo toEscapedModel() {
		return new LayoutNotNssCatalogoWrapper(_layoutNotNssCatalogo.toEscapedModel());
	}

	public LayoutNotNssCatalogo toUnescapedModel() {
		return new LayoutNotNssCatalogoWrapper(_layoutNotNssCatalogo.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _layoutNotNssCatalogo.toString();
	}

	public java.lang.String toXmlString() {
		return _layoutNotNssCatalogo.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_layoutNotNssCatalogo.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LayoutNotNssCatalogoWrapper)) {
			return false;
		}

		LayoutNotNssCatalogoWrapper layoutNotNssCatalogoWrapper = (LayoutNotNssCatalogoWrapper)obj;

		if (Validator.equals(_layoutNotNssCatalogo, layoutNotNssCatalogoWrapper._layoutNotNssCatalogo)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public LayoutNotNssCatalogo getWrappedLayoutNotNssCatalogoWrapper() {
		return _layoutNotNssCatalogo;
	}

	public LayoutNotNssCatalogo getWrappedModel() {
		return _layoutNotNssCatalogo;
	}

	public void resetOriginalValues() {
		_layoutNotNssCatalogo.resetOriginalValues();
	}

	private LayoutNotNssCatalogo _layoutNotNssCatalogo;

	@AutoEscape
	public String getIcApproval() {
		return _layoutNotNssCatalogo.getIcApproval();
	}

	public void setIcApproval(String icApproval) {
		_layoutNotNssCatalogo.setIcApproval(icApproval);
	}

	@AutoEscape
	public String getCategoryCode() {
		return _layoutNotNssCatalogo.getCategoryCode();
	}

	public void setCategoryCode(String categoryCode) {
		_layoutNotNssCatalogo.setCategoryCode(categoryCode);
	}

	@AutoEscape
	public String getOfferNumber() {
		return _layoutNotNssCatalogo.getOfferNumber();
	}

	public void setOfferNumber(String offerNumber) {
		_layoutNotNssCatalogo.setOfferNumber(offerNumber);
	}
}