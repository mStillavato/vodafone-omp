package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Vendor}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       Vendor
 * @generated
 */
public class VendorWrapper implements Vendor, ModelWrapper<Vendor> {
    private Vendor _vendor;

    public VendorWrapper(Vendor vendor) {
        _vendor = vendor;
    }

    public Class<?> getModelClass() {
        return Vendor.class;
    }

    public String getModelClassName() {
        return Vendor.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("vendorId", getVendorId());
        attributes.put("supplier", getSupplier());
        attributes.put("supplierCode", getSupplierCode());
        attributes.put("vendorName", getVendorName());
        attributes.put("display", getDisplay());
        attributes.put("gruppoUsers", getGruppoUsers());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long vendorId = (Long) attributes.get("vendorId");

        if (vendorId != null) {
            setVendorId(vendorId);
        }

        String supplier = (String) attributes.get("supplier");

        if (supplier != null) {
            setSupplier(supplier);
        }

        String supplierCode = (String) attributes.get("supplierCode");

        if (supplierCode != null) {
            setSupplierCode(supplierCode);
        }

        String vendorName = (String) attributes.get("vendorName");

        if (vendorName != null) {
            setVendorName(vendorName);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
        
        Integer gruppoUsers = (Integer) attributes.get("gruppoUsers");

        if (gruppoUsers != null) {
            setGruppoUsers(gruppoUsers);
        }
    }

    /**
    * Returns the primary key of this vendor.
    *
    * @return the primary key of this vendor
    */
    public long getPrimaryKey() {
        return _vendor.getPrimaryKey();
    }

    /**
    * Sets the primary key of this vendor.
    *
    * @param primaryKey the primary key of this vendor
    */
    public void setPrimaryKey(long primaryKey) {
        _vendor.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the vendor ID of this vendor.
    *
    * @return the vendor ID of this vendor
    */
    public long getVendorId() {
        return _vendor.getVendorId();
    }

    /**
    * Sets the vendor ID of this vendor.
    *
    * @param vendorId the vendor ID of this vendor
    */
    public void setVendorId(long vendorId) {
        _vendor.setVendorId(vendorId);
    }

    /**
    * Returns the supplier of this vendor.
    *
    * @return the supplier of this vendor
    */
    public java.lang.String getSupplier() {
        return _vendor.getSupplier();
    }

    /**
    * Sets the supplier of this vendor.
    *
    * @param supplier the supplier of this vendor
    */
    public void setSupplier(java.lang.String supplier) {
        _vendor.setSupplier(supplier);
    }

    /**
    * Returns the supplier code of this vendor.
    *
    * @return the supplier code of this vendor
    */
    public java.lang.String getSupplierCode() {
        return _vendor.getSupplierCode();
    }

    /**
    * Sets the supplier code of this vendor.
    *
    * @param supplierCode the supplier code of this vendor
    */
    public void setSupplierCode(java.lang.String supplierCode) {
        _vendor.setSupplierCode(supplierCode);
    }

    /**
    * Returns the vendor name of this vendor.
    *
    * @return the vendor name of this vendor
    */
    public java.lang.String getVendorName() {
        return _vendor.getVendorName();
    }

    /**
    * Sets the vendor name of this vendor.
    *
    * @param vendorName the vendor name of this vendor
    */
    public void setVendorName(java.lang.String vendorName) {
        _vendor.setVendorName(vendorName);
    }

    /**
    * Returns the display of this vendor.
    *
    * @return the display of this vendor
    */
    public boolean getDisplay() {
        return _vendor.getDisplay();
    }

    /**
    * Returns <code>true</code> if this vendor is display.
    *
    * @return <code>true</code> if this vendor is display; <code>false</code> otherwise
    */
    public boolean isDisplay() {
        return _vendor.isDisplay();
    }

    /**
    * Sets whether this vendor is display.
    *
    * @param display the display of this vendor
    */
    public void setDisplay(boolean display) {
        _vendor.setDisplay(display);
    }

    public boolean isNew() {
        return _vendor.isNew();
    }

    public void setNew(boolean n) {
        _vendor.setNew(n);
    }

    public boolean isCachedModel() {
        return _vendor.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _vendor.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _vendor.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _vendor.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _vendor.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _vendor.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _vendor.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new VendorWrapper((Vendor) _vendor.clone());
    }

    public int compareTo(Vendor vendor) {
        return _vendor.compareTo(vendor);
    }

    @Override
    public int hashCode() {
        return _vendor.hashCode();
    }

    public com.liferay.portal.model.CacheModel<Vendor> toCacheModel() {
        return _vendor.toCacheModel();
    }

    public Vendor toEscapedModel() {
        return new VendorWrapper(_vendor.toEscapedModel());
    }

    public Vendor toUnescapedModel() {
        return new VendorWrapper(_vendor.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _vendor.toString();
    }

    public java.lang.String toXmlString() {
        return _vendor.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _vendor.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VendorWrapper)) {
            return false;
        }

        VendorWrapper vendorWrapper = (VendorWrapper) obj;

        if (Validator.equals(_vendor, vendorWrapper._vendor)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public Vendor getWrappedVendor() {
        return _vendor;
    }

    public Vendor getWrappedModel() {
        return _vendor;
    }

    public void resetOriginalValues() {
        _vendor.resetOriginalValues();
    }

	public int getGruppoUsers() {
		return _vendor.getGruppoUsers();
	}

	public void setGruppoUsers(int gruppoUsers) {
		_vendor.setGruppoUsers(gruppoUsers);
	}
}
