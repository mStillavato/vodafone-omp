package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the GoodReceipt service. Represents a row in the &quot;GOOD_RECEIPT&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see GoodReceiptModel
 * @see com.hp.omp.model.impl.GoodReceiptImpl
 * @see com.hp.omp.model.impl.GoodReceiptModelImpl
 * @generated
 */
public interface GoodReceipt extends GoodReceiptModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.GoodReceiptImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public com.hp.omp.model.custom.GoodReceiptStatus getGoodReceiptStatus();

    public java.util.Date getRequestDate();

    public java.lang.String getRequestDateText();

    public java.lang.String getRegisterationDateText();

    public java.lang.String getPercentageText()
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.lang.String getFormattedPercentageText()
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.lang.String getFormattedPercentage()
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.lang.Double getMaxPercentage()
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.lang.String getGrValueText()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException,
            java.lang.NumberFormatException;

    public java.lang.String getFormattedGrValueText()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException,
            java.lang.NumberFormatException;

    public java.lang.String getFormattedGrValue()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException,
            java.lang.NumberFormatException;

    public com.hp.omp.model.Position getPosition()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public java.lang.String getRequestDateReadonly()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public java.lang.String getPercentageReadonly()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public java.lang.String getGrValueReadonly()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public java.lang.String getGrNumberReadonly()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public java.lang.String getRegisterationDateReadonly()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public java.lang.String antexFieldsStatus()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;
}
