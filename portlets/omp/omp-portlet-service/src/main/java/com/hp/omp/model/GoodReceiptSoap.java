package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class GoodReceiptSoap implements Serializable {
    private long _goodReceiptId;
    private String _goodReceiptNumber;
    private Date _requestDate;
    private double _percentage;
    private String _grValue;
    private Date _registerationDate;
    private String _createdUserId;
    private String _positionId;

    public GoodReceiptSoap() {
    }

    public static GoodReceiptSoap toSoapModel(GoodReceipt model) {
        GoodReceiptSoap soapModel = new GoodReceiptSoap();

        soapModel.setGoodReceiptId(model.getGoodReceiptId());
        soapModel.setGoodReceiptNumber(model.getGoodReceiptNumber());
        soapModel.setRequestDate(model.getRequestDate());
        soapModel.setPercentage(model.getPercentage());
        soapModel.setGrValue(model.getGrValue());
        soapModel.setRegisterationDate(model.getRegisterationDate());
        soapModel.setCreatedUserId(model.getCreatedUserId());
        soapModel.setPositionId(model.getPositionId());

        return soapModel;
    }

    public static GoodReceiptSoap[] toSoapModels(GoodReceipt[] models) {
        GoodReceiptSoap[] soapModels = new GoodReceiptSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static GoodReceiptSoap[][] toSoapModels(GoodReceipt[][] models) {
        GoodReceiptSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new GoodReceiptSoap[models.length][models[0].length];
        } else {
            soapModels = new GoodReceiptSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static GoodReceiptSoap[] toSoapModels(List<GoodReceipt> models) {
        List<GoodReceiptSoap> soapModels = new ArrayList<GoodReceiptSoap>(models.size());

        for (GoodReceipt model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new GoodReceiptSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _goodReceiptId;
    }

    public void setPrimaryKey(long pk) {
        setGoodReceiptId(pk);
    }

    public long getGoodReceiptId() {
        return _goodReceiptId;
    }

    public void setGoodReceiptId(long goodReceiptId) {
        _goodReceiptId = goodReceiptId;
    }

    public String getGoodReceiptNumber() {
        return _goodReceiptNumber;
    }

    public void setGoodReceiptNumber(String goodReceiptNumber) {
        _goodReceiptNumber = goodReceiptNumber;
    }

    public Date getRequestDate() {
        return _requestDate;
    }

    public void setRequestDate(Date requestDate) {
        _requestDate = requestDate;
    }

    public double getPercentage() {
        return _percentage;
    }

    public void setPercentage(double percentage) {
        _percentage = percentage;
    }

    public String getGrValue() {
        return _grValue;
    }

    public void setGrValue(String grValue) {
        _grValue = grValue;
    }

    public Date getRegisterationDate() {
        return _registerationDate;
    }

    public void setRegisterationDate(Date registerationDate) {
        _registerationDate = registerationDate;
    }

    public String getCreatedUserId() {
        return _createdUserId;
    }

    public void setCreatedUserId(String createdUserId) {
        _createdUserId = createdUserId;
    }

    public String getPositionId() {
        return _positionId;
    }

    public void setPositionId(String positionId) {
        _positionId = positionId;
    }
}
