package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class PositionSoap implements Serializable {
    private long _positionId;
    private String _fiscalYear;
    private String _description;
    private String _categoryCode;
    private String _offerNumber;
    private Date _offerDate;
    private String _quatity;
    private Date _deliveryDate;
    private String _deliveryAddress;
    private String _actualUnitCost;
    private String _numberOfUnit;
    private long _labelNumber;
    private long _poLabelNumber;
    private String _targaTecnica;
    private boolean _approved;
    private String _icApproval;
    private String _approver;
    private String _assetNumber;
    private String _shoppingCart;
    private String _poNumber;
    private String _evoLocation;
    private String _wbsCodeId;
    private String _purchaseRequestId;
    private String _purchaseOrderId;
    private Date _createdDate;
    private long _createdUserId;

    public PositionSoap() {
    }

    public static PositionSoap toSoapModel(Position model) {
        PositionSoap soapModel = new PositionSoap();

        soapModel.setPositionId(model.getPositionId());
        soapModel.setFiscalYear(model.getFiscalYear());
        soapModel.setDescription(model.getDescription());
        soapModel.setCategoryCode(model.getCategoryCode());
        soapModel.setOfferNumber(model.getOfferNumber());
        soapModel.setOfferDate(model.getOfferDate());
        soapModel.setQuatity(model.getQuatity());
        soapModel.setDeliveryDate(model.getDeliveryDate());
        soapModel.setDeliveryAddress(model.getDeliveryAddress());
        soapModel.setActualUnitCost(model.getActualUnitCost());
        soapModel.setNumberOfUnit(model.getNumberOfUnit());
        soapModel.setLabelNumber(model.getLabelNumber());
        soapModel.setPoLabelNumber(model.getPoLabelNumber());
        soapModel.setTargaTecnica(model.getTargaTecnica());
        soapModel.setApproved(model.getApproved());
        soapModel.setIcApproval(model.getIcApproval());
        soapModel.setApprover(model.getApprover());
        soapModel.setAssetNumber(model.getAssetNumber());
        soapModel.setShoppingCart(model.getShoppingCart());
        soapModel.setPoNumber(model.getPoNumber());
        soapModel.setEvoLocation(model.getEvoLocation());
        soapModel.setWbsCodeId(model.getWbsCodeId());
        soapModel.setPurchaseRequestId(model.getPurchaseRequestId());
        soapModel.setPurchaseOrderId(model.getPurchaseOrderId());
        soapModel.setCreatedDate(model.getCreatedDate());
        soapModel.setCreatedUserId(model.getCreatedUserId());

        return soapModel;
    }

    public static PositionSoap[] toSoapModels(Position[] models) {
        PositionSoap[] soapModels = new PositionSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static PositionSoap[][] toSoapModels(Position[][] models) {
        PositionSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new PositionSoap[models.length][models[0].length];
        } else {
            soapModels = new PositionSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static PositionSoap[] toSoapModels(List<Position> models) {
        List<PositionSoap> soapModels = new ArrayList<PositionSoap>(models.size());

        for (Position model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new PositionSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _positionId;
    }

    public void setPrimaryKey(long pk) {
        setPositionId(pk);
    }

    public long getPositionId() {
        return _positionId;
    }

    public void setPositionId(long positionId) {
        _positionId = positionId;
    }

    public String getFiscalYear() {
        return _fiscalYear;
    }

    public void setFiscalYear(String fiscalYear) {
        _fiscalYear = fiscalYear;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public String getCategoryCode() {
        return _categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        _categoryCode = categoryCode;
    }

    public String getOfferNumber() {
        return _offerNumber;
    }

    public void setOfferNumber(String offerNumber) {
        _offerNumber = offerNumber;
    }

    public Date getOfferDate() {
        return _offerDate;
    }

    public void setOfferDate(Date offerDate) {
        _offerDate = offerDate;
    }

    public String getQuatity() {
        return _quatity;
    }

    public void setQuatity(String quatity) {
        _quatity = quatity;
    }

    public Date getDeliveryDate() {
        return _deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        _deliveryDate = deliveryDate;
    }

    public String getDeliveryAddress() {
        return _deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        _deliveryAddress = deliveryAddress;
    }

    public String getActualUnitCost() {
        return _actualUnitCost;
    }

    public void setActualUnitCost(String actualUnitCost) {
        _actualUnitCost = actualUnitCost;
    }

    public String getNumberOfUnit() {
        return _numberOfUnit;
    }

    public void setNumberOfUnit(String numberOfUnit) {
        _numberOfUnit = numberOfUnit;
    }

    public long getLabelNumber() {
        return _labelNumber;
    }

    public void setLabelNumber(long labelNumber) {
        _labelNumber = labelNumber;
    }

    public long getPoLabelNumber() {
        return _poLabelNumber;
    }

    public void setPoLabelNumber(long poLabelNumber) {
        _poLabelNumber = poLabelNumber;
    }

    public String getTargaTecnica() {
        return _targaTecnica;
    }

    public void setTargaTecnica(String targaTecnica) {
        _targaTecnica = targaTecnica;
    }

    public boolean getApproved() {
        return _approved;
    }

    public boolean isApproved() {
        return _approved;
    }

    public void setApproved(boolean approved) {
        _approved = approved;
    }

    public String getIcApproval() {
        return _icApproval;
    }

    public void setIcApproval(String icApproval) {
        _icApproval = icApproval;
    }

    public String getApprover() {
        return _approver;
    }

    public void setApprover(String approver) {
        _approver = approver;
    }

    public String getAssetNumber() {
        return _assetNumber;
    }

    public void setAssetNumber(String assetNumber) {
        _assetNumber = assetNumber;
    }

    public String getShoppingCart() {
        return _shoppingCart;
    }

    public void setShoppingCart(String shoppingCart) {
        _shoppingCart = shoppingCart;
    }

    public String getPoNumber() {
        return _poNumber;
    }

    public void setPoNumber(String poNumber) {
        _poNumber = poNumber;
    }

    public String getEvoLocation() {
        return _evoLocation;
    }

    public void setEvoLocation(String evoLocation) {
        _evoLocation = evoLocation;
    }

    public String getWbsCodeId() {
        return _wbsCodeId;
    }

    public void setWbsCodeId(String wbsCodeId) {
        _wbsCodeId = wbsCodeId;
    }

    public String getPurchaseRequestId() {
        return _purchaseRequestId;
    }

    public void setPurchaseRequestId(String purchaseRequestId) {
        _purchaseRequestId = purchaseRequestId;
    }

    public String getPurchaseOrderId() {
        return _purchaseOrderId;
    }

    public void setPurchaseOrderId(String purchaseOrderId) {
        _purchaseOrderId = purchaseOrderId;
    }

    public Date getCreatedDate() {
        return _createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        _createdDate = createdDate;
    }

    public long getCreatedUserId() {
        return _createdUserId;
    }

    public void setCreatedUserId(long createdUserId) {
        _createdUserId = createdUserId;
    }
}
