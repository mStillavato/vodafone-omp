package com.hp.omp.model.custom;

import java.util.List;

import com.hp.omp.model.CostCenter;
import com.hp.omp.model.Project;
import com.hp.omp.model.SubProject;
import com.hp.omp.model.Vendor;
import com.hp.omp.model.WbsCode;
import com.liferay.portal.kernel.util.HtmlUtil;

public class ReportDTO {
	private String fiscalYear = null;
	private String costCenterName = null;
	private Long costCenterId = null;
	private String projectName = null;
	private Long projectId = null;
	private Long subProjectId = null;
	private String subProjectName = null;
	private String wbsCode = null;
	private String wbsCodeName = null;
	private Integer currency;
	private String userType = null;
	private String targaTecnica = null;
	private Integer prStatus;
	private String vendorName = null;
	private Long vendorId = null;
	private Integer tipoPr = null;
	
	private List<String> allFiscalYears;
	private List<CostCenter> allCostCenters;
	private List<String> allProjectNames;
	private List<String> allSubProjectNames;
	private List<WbsCode> allWbsCodes;
	private List<Project> allProjects;
	private List<SubProject> allSubProjects;
	private List<String> allTargaTecnica;
	private List<Vendor> allVendors;
	private List<String> allVendorNames;
	private List<PurchaseRequestStatus> allPrStatus;
	
	private String closedEUR;
	private String openedEUR;
	private String totalEUR;
	private String closingEUR;
	private String comittedEUR;
	private String prTotalEUR;
	private String aggregateTotalEUR;
	
	private String closedUSD;
	private String openedUSD;
	private String totalUSD;
	private String closingUSD;
	private String comittedUSD;
	private String prTotalUSD;
	private String aggregateTotalUSD;
	
	private boolean hasReport;
	
	private boolean prReport = false;
	
	public String getFiscalYear() {
		return fiscalYear;
	}
	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}
	public String getCostCenterName() {
		return costCenterName;
	}
	public void setCostCenterName(String costCenterName) {
		this.costCenterName = costCenterName;
	}
	public Long getCostCenterId() {
		return costCenterId;
	}
	public void setCostCenterId(Long costCenterId) {
		this.costCenterId = costCenterId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getSubProjectName() {
		return subProjectName;
	}
	public void setSubProjectName(String subProjectName) {
		this.subProjectName = subProjectName;
	}
	public String getWbsCode() {
		return wbsCode;
	}
	public void setWbsCode(String wbsCode) {
		this.wbsCode = wbsCode;
	}
	public Integer getCurrency() {
		return currency;
	}
	public void setCurrency(Integer currency) {
		this.currency = currency;
	}
	public List<String> getAllFiscalYears() {
		return allFiscalYears;
	}
	public void setAllFiscalYears(List<String> allFiscalYears) {
		this.allFiscalYears = allFiscalYears;
	}
	public List<CostCenter> getAllCostCenters() {
		return allCostCenters;
	}
	public void setAllCostCenters(List<CostCenter> allcostCenters) {
		this.allCostCenters = allcostCenters;
	}
	public List<String> getAllProjectNames() {
		return allProjectNames;
	}
	public void setAllProjectNames(List<String> allprojectNames) {
		this.allProjectNames = allprojectNames;
	}
	public List<String> getAllSubProjectNames() {
		return allSubProjectNames;
	}
	public void setAllSubProjectNames(List<String> allsubProjectNames) {
		this.allSubProjectNames = allsubProjectNames;
	}
	public List<WbsCode> getAllWbsCodes() {
		return allWbsCodes;
	}
	public void setAllWbsCodes(List<WbsCode> allwbsCodes) {
		this.allWbsCodes = allwbsCodes;
	}
	public String getClosedEUR() {
		return closedEUR;
	}
	public void setClosedEUR(String closedEUR) {
		this.closedEUR = closedEUR;
		this.hasReport = true;
	}
	public String getOpenedEUR() {
		return openedEUR;
	}
	public void setOpenedEUR(String openedEUR) {
		this.openedEUR = openedEUR;
		this.hasReport = true;
	}
	public String getTotalEUR() {
		return totalEUR;
	}
	public void setTotalEUR(String totalEUR) {
		this.totalEUR = totalEUR;
		this.hasReport = true;
	}
	public String getClosedUSD() {
		return closedUSD;
	}
	public void setClosedUSD(String closedUSD) {
		this.closedUSD = closedUSD;
		this.hasReport = true;
	}
	public String getOpenedUSD() {
		return openedUSD;
	}
	public void setOpenedUSD(String openedUSD) {
		this.openedUSD = openedUSD;
		this.hasReport = true;
	}
	public String getTotalUSD() {
		return totalUSD;
	}
	public void setTotalUSD(String totalUSD) {
		this.totalUSD = totalUSD;
		this.hasReport = true;
	}
	public boolean isHasReport() {
		return hasReport;
	}
	public void setHasReport(boolean hasReport) {
		this.hasReport = hasReport;
	}
	public String getEscapedFiscalYear() {
		return getEscapedValue(fiscalYear);
	}
	public String getEscapedCostCenter() {
		return getEscapedValue(costCenterName);
	}
	public String getEscapedProjectName() {
		return getEscapedValue(projectName);
	}
	public String getEscapedSubProjectName() {
		return getEscapedValue(subProjectName);
	}
	public String getEscapedWbsCodeName() {
		return getEscapedValue(wbsCodeName);
	}
	public String getEscapedAttributeWbsCodeName() {
		return getEscapedAttributeValue(wbsCodeName);
	}
	
	public String getEscapedTargaTecnica() {
		return getEscapedValue(targaTecnica);
	}
	
	private String getEscapedValue(String value){
		if(value == null){
			return "";
		}
		value = HtmlUtil.escape(value);
		return value;
	}
	
	private String getEscapedAttributeValue(String value){
		if(value == null){
			return "";
		}
		value = HtmlUtil.escapeAttribute(value);
		return value;
	}
	/**
	 * @return the allProjects
	 */
	public List<Project> getAllProjects() {
		return allProjects;
	}
	/**
	 * @param allProjects the allProjects to set
	 */
	public void setAllProjects(List<Project> allProjects) {
		this.allProjects = allProjects;
	}
	/**
	 * @return the projectId
	 */
	public Long getProjectId() {
		return projectId;
	}
	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}
	/**
	 * @return the allSubProjects
	 */
	public List<SubProject> getAllSubProjects() {
		return allSubProjects;
	}
	/**
	 * @param allSubProjects the allSubProjects to set
	 */
	public void setAllSubProjects(List<SubProject> allSubProjects) {
		this.allSubProjects = allSubProjects;
	}
	/**
	 * @return the subProjectId
	 */
	public Long getSubProjectId() {
		return subProjectId;
	}
	/**
	 * @param subProjectId the subProjectId to set
	 */
	public void setSubProjectId(Long subProjectId) {
		this.subProjectId = subProjectId;
	}
	/**
	 * @return the wbsCodeName
	 */
	public String getWbsCodeName() {
		return wbsCodeName;
	}
	/**
	 * @param wbsCodeName the wbsCodeName to set
	 */
	public void setWbsCodeName(String wbsCodeName) {
		this.wbsCodeName = wbsCodeName;
	}
	/**
	 * @return the closingEUR
	 */
	public String getClosingEUR() {
		return closingEUR;
	}
	/**
	 * @param closingEUR the closingEUR to set
	 */
	public void setClosingEUR(String closingEUR) {
		this.closingEUR = closingEUR;
	}
	/**
	 * @return the comittedEUR
	 */
	public String getComittedEUR() {
		return comittedEUR;
	}
	/**
	 * @param comittedEUR the comittedEUR to set
	 */
	public void setComittedEUR(String comittedEUR) {
		this.comittedEUR = comittedEUR;
	}
	/**
	 * @return the closingUSD
	 */
	public String getClosingUSD() {
		return closingUSD;
	}
	/**
	 * @param closingUSD the closingUSD to set
	 */
	public void setClosingUSD(String closingUSD) {
		this.closingUSD = closingUSD;
	}
	/**
	 * @return the comittedUSD
	 */
	public String getComittedUSD() {
		return comittedUSD;
	}
	/**
	 * @param comittedUSD the comittedUSD to set
	 */
	public void setComittedUSD(String comittedUSD) {
		this.comittedUSD = comittedUSD;
	}
	
	public String getPrTotalEUR() {
		return prTotalEUR;
	}
	public void setPrTotalEUR(String prTotalEUR) {
		this.prTotalEUR = prTotalEUR;
	}
	public String getAggregateTotalEUR() {
		return aggregateTotalEUR;
	}
	public void setAggregateTotalEUR(String aggregateTotalEUR) {
		this.aggregateTotalEUR = aggregateTotalEUR;
	}
	public String getPrTotalUSD() {
		return prTotalUSD;
	}
	public void setPrTotalUSD(String prTotalUSD) {
		this.prTotalUSD = prTotalUSD;
	}
	public String getAggregateTotalUSD() {
		return aggregateTotalUSD;
	}
	public void setAggregateTotalUSD(String aggregateTotalUSD) {
		this.aggregateTotalUSD = aggregateTotalUSD;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public boolean isPrReport() {
		return prReport;
	}
	public void setPrReport(boolean prReport) {
		this.prReport = prReport;
	}
	public String getTargaTecnica() {
		return targaTecnica;
	}
	public void setTargaTecnica(String targaTecnica) {
		this.targaTecnica = targaTecnica;
	}
	public List<String> getAllTargaTecnica() {
		return allTargaTecnica;
	}
	public void setAllTargaTecnica(List<String> allTargaTecnica) {
		this.allTargaTecnica = allTargaTecnica;
	}
	public Integer getPrStatus() {
		return prStatus;
	}
	public void setPrStatus(Integer prStatus) {
		this.prStatus = prStatus;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public Long getVendorId() {
		return vendorId;
	}
	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}
	public List<Vendor> getAllVendors() {
		return allVendors;
	}
	public void setAllVendors(List<Vendor> allVendors) {
		this.allVendors = allVendors;
	}
	public List<String> getAllVendorNames() {
		return allVendorNames;
	}
	public void setAllVendorNames(List<String> allVendorNames) {
		this.allVendorNames = allVendorNames;
	}
	public List<PurchaseRequestStatus> getAllPrStatus() {
		return allPrStatus;
	}
	public void setAllPrStatus(List<PurchaseRequestStatus> allPrStatus) {
		this.allPrStatus = allPrStatus;
	}
	public Integer getTipoPr() {
		return tipoPr;
	}
	public void setTipoPr(Integer tipoPr) {
		this.tipoPr = tipoPr;
	}
}
