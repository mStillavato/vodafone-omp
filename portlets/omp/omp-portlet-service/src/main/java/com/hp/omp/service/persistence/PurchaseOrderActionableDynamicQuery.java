package com.hp.omp.service.persistence;

import com.hp.omp.model.PurchaseOrder;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class PurchaseOrderActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public PurchaseOrderActionableDynamicQuery() throws SystemException {
        setBaseLocalService(PurchaseOrderLocalServiceUtil.getService());
        setClass(PurchaseOrder.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("purchaseOrderId");
    }
}
