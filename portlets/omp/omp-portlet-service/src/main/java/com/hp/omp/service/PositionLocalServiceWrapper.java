package com.hp.omp.service;

import java.util.List;

import com.hp.omp.model.Position;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link PositionLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       PositionLocalService
 * @generated
 */
public class PositionLocalServiceWrapper implements PositionLocalService,
    ServiceWrapper<PositionLocalService> {
    private PositionLocalService _positionLocalService;

    public PositionLocalServiceWrapper(
        PositionLocalService positionLocalService) {
        _positionLocalService = positionLocalService;
    }

    /**
    * Adds the position to the database. Also notifies the appropriate model listeners.
    *
    * @param position the position
    * @return the position that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Position addPosition(
        com.hp.omp.model.Position position)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.addPosition(position);
    }

    /**
    * Creates a new position with the primary key. Does not add the position to the database.
    *
    * @param positionId the primary key for the new position
    * @return the new position
    */
    public com.hp.omp.model.Position createPosition(long positionId) {
        return _positionLocalService.createPosition(positionId);
    }

    /**
    * Deletes the position with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param positionId the primary key of the position
    * @return the position that was removed
    * @throws PortalException if a position with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Position deletePosition(long positionId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.deletePosition(positionId);
    }

    /**
    * Deletes the position from the database. Also notifies the appropriate model listeners.
    *
    * @param position the position
    * @return the position that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Position deletePosition(
        com.hp.omp.model.Position position)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.deletePosition(position);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _positionLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.dynamicQueryCount(dynamicQuery);
    }

    public com.hp.omp.model.Position fetchPosition(long positionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.fetchPosition(positionId);
    }

    /**
    * Returns the position with the primary key.
    *
    * @param positionId the primary key of the position
    * @return the position
    * @throws PortalException if a position with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Position getPosition(long positionId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.getPosition(positionId);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the positions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of positions
    * @param end the upper bound of the range of positions (not inclusive)
    * @return the range of positions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.Position> getPositions(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.getPositions(start, end);
    }

    /**
    * Returns the number of positions.
    *
    * @return the number of positions
    * @throws SystemException if a system exception occurred
    */
    public int getPositionsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.getPositionsCount();
    }

    /**
    * Updates the position in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param position the position
    * @return the position that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Position updatePosition(
        com.hp.omp.model.Position position)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.updatePosition(position);
    }

    /**
    * Updates the position in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param position the position
    * @param merge whether to merge the position with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the position that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Position updatePosition(
        com.hp.omp.model.Position position, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.updatePosition(position, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _positionLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _positionLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _positionLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    public com.hp.omp.model.Position updatePositionWithoutHocks(
        com.hp.omp.model.Position position, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.updatePositionWithoutHocks(position, merge);
    }

    public java.util.List<java.util.Date> getPositionsMaxRegisterationDate(
        long Id, java.lang.String type)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.getPositionsMaxRegisterationDate(Id, type);
    }

    public java.util.List<com.hp.omp.model.Position> getPurchaseOrderPositions(
        java.lang.String purchaseOrderId, long userId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.getPurchaseOrderPositions(purchaseOrderId,
            userId);
    }

    public java.lang.Long getPurchaseRequestPositionsCount(
        com.hp.omp.model.Position position)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.getPurchaseRequestPositionsCount(position);
    }

    public java.lang.Long getPurchaseRequestPostionsByStatusCount(
        com.hp.omp.model.Position position,
        com.hp.omp.model.custom.PositionStatus status)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.getPurchaseRequestPostionsByStatusCount(position,
            status);
    }

    public java.util.List<com.hp.omp.model.Position> getPurchaseRequestPositions(
        java.lang.String purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.getPurchaseRequestPositions(purchaseRequestId);
    }

    public java.util.List<com.hp.omp.model.Position> getPositionsByPrId(
        long purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.getPositionsByPrId(purchaseRequestId);
    }

    public java.util.List<java.util.Date> getPrPositionsMinimumDeliveryDate(
        long purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.getPrPositionsMinimumDeliveryDate(purchaseRequestId);
    }

    public java.util.List<java.util.Date> getPoRecievedPrPositionsMinimumDeliveryDate(
        long purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.getPoRecievedPrPositionsMinimumDeliveryDate(purchaseRequestId);
    }

    public java.util.List<java.util.Date> getPoPositionsMinimumDeliveryDate(
        long purchaseorderId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.getPoPositionsMinimumDeliveryDate(purchaseorderId);
    }

    public java.util.List<java.lang.Long> getPositionsMaxLabelNumber(
        long purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.getPositionsMaxLabelNumber(purchaseRequestId);
    }

    public java.util.List<com.hp.omp.model.Position> getPositionsByStatus(
        long prId, int positionStatus) throws java.lang.Exception {
        return _positionLocalService.getPositionsByStatus(prId, positionStatus);
    }

    public java.util.List<com.hp.omp.model.Position> getPositionsByStatusOnly(
        int positionStatus) throws java.lang.Exception {
        return _positionLocalService.getPositionsByStatusOnly(positionStatus);
    }

    public java.util.List<com.hp.omp.model.Position> getPositionsByPurchaseOrderId(
        long poId) throws java.lang.Exception {
        return _positionLocalService.getPositionsByPurchaseOrderId(poId);
    }

    public java.util.List<com.hp.omp.model.Position> getPositionsByWbsCodeId(
        java.lang.String wbsCodeId) throws java.lang.Exception {
        return _positionLocalService.getPositionsByWbsCodeId(wbsCodeId);
    }

    public java.util.List<java.lang.String> getAllFiscalYears()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.getAllFiscalYears();
    }

    public java.util.List<java.lang.String> getAllWBSCodes()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.getAllWBSCodes();
    }

    public java.lang.Long getIAndCPositionsCount(
        java.lang.String purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _positionLocalService.getIAndCPositionsCount(purchaseRequestId);
    }

    public java.util.List<com.hp.omp.model.Position> getPositionsByIAndCType(
        long prId, java.lang.String iAndCType) throws java.lang.Exception {
        return _positionLocalService.getPositionsByIAndCType(prId, iAndCType);
    }

    public java.util.List<com.hp.omp.model.Position> getPositionsByIAndCTypeAndApprovalStatus(
        long prId, java.lang.String iAndCType, boolean approved)
        throws java.lang.Exception {
        return _positionLocalService.getPositionsByIAndCTypeAndApprovalStatus(prId,
            iAndCType, approved);
    }

    public java.util.List<com.hp.omp.model.Position> getIAndCPositions(
        long prId) throws java.lang.Exception {
        return _positionLocalService.getIAndCPositions(prId);
    }

    public void copyWBSfromPR(com.hp.omp.model.Position position)
        throws com.liferay.portal.kernel.exception.SystemException {
        _positionLocalService.copyWBSfromPR(position);
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public PositionLocalService getWrappedPositionLocalService() {
        return _positionLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedPositionLocalService(
        PositionLocalService positionLocalService) {
        _positionLocalService = positionLocalService;
    }

    public PositionLocalService getWrappedService() {
        return _positionLocalService;
    }

    public void setWrappedService(PositionLocalService positionLocalService) {
        _positionLocalService = positionLocalService;
    }

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Position> getPositionsNssByStatus(long prId, int positionStatus)
			throws Exception {
		return _positionLocalService.getPositionsNssByStatus(prId, positionStatus);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Position> getPositionsByStatusVbs(long prId, int positionStatus)
			throws Exception {
		return _positionLocalService.getPositionsByStatusVbs(prId, positionStatus);
	}
}
