package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the TrackingCodes service. Represents a row in the &quot;TRACKING_CODES&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see TrackingCodesModel
 * @see com.hp.omp.model.impl.TrackingCodesImpl
 * @see com.hp.omp.model.impl.TrackingCodesModelImpl
 * @generated
 */
public interface TrackingCodes extends TrackingCodesModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.TrackingCodesImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
