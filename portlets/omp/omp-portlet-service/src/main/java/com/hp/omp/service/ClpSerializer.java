package com.hp.omp.service;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.hp.omp.model.BudgetCategoryClp;
import com.hp.omp.model.BudgetSubCategoryClp;
import com.hp.omp.model.BuyerClp;
import com.hp.omp.model.CatalogoClp;
import com.hp.omp.model.LayoutNotNssCatalogoClp;
import com.hp.omp.model.CostCenterClp;
import com.hp.omp.model.CostCenterUserClp;
import com.hp.omp.model.GRAuditClp;
import com.hp.omp.model.GoodReceiptClp;
import com.hp.omp.model.MacroDriverClp;
import com.hp.omp.model.MacroMicroDriverClp;
import com.hp.omp.model.MicroDriverClp;
import com.hp.omp.model.ODNPCodeClp;
import com.hp.omp.model.ODNPNameClp;
import com.hp.omp.model.POAuditClp;
import com.hp.omp.model.PRAuditClp;
import com.hp.omp.model.PositionAuditClp;
import com.hp.omp.model.PositionClp;
import com.hp.omp.model.ProjectClp;
import com.hp.omp.model.ProjectSubProjectClp;
import com.hp.omp.model.PurchaseOrderClp;
import com.hp.omp.model.PurchaseRequestClp;
import com.hp.omp.model.SubProjectClp;
import com.hp.omp.model.TrackingCodesClp;
import com.hp.omp.model.VendorClp;
import com.hp.omp.model.WbsCodeClp;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;


public class ClpSerializer {
    private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
    private static String _servletContextName;
    private static boolean _useReflectionToTranslateThrowable = true;

    public static String getServletContextName() {
        if (Validator.isNotNull(_servletContextName)) {
            return _servletContextName;
        }

        synchronized (ClpSerializer.class) {
            if (Validator.isNotNull(_servletContextName)) {
                return _servletContextName;
            }

            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Class<?> portletPropsClass = classLoader.loadClass(
                        "com.liferay.util.portlet.PortletProps");

                Method getMethod = portletPropsClass.getMethod("get",
                        new Class<?>[] { String.class });

                String portletPropsServletContextName = (String) getMethod.invoke(null,
                        "omp-portlet-deployment-context");

                if (Validator.isNotNull(portletPropsServletContextName)) {
                    _servletContextName = portletPropsServletContextName;
                }
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info(
                        "Unable to locate deployment context from portlet properties");
                }
            }

            if (Validator.isNull(_servletContextName)) {
                try {
                    String propsUtilServletContextName = PropsUtil.get(
                            "omp-portlet-deployment-context");

                    if (Validator.isNotNull(propsUtilServletContextName)) {
                        _servletContextName = propsUtilServletContextName;
                    }
                } catch (Throwable t) {
                    if (_log.isInfoEnabled()) {
                        _log.info(
                            "Unable to locate deployment context from portal properties");
                    }
                }
            }

            if (Validator.isNull(_servletContextName)) {
                _servletContextName = "omp-portlet";
            }

            return _servletContextName;
        }
    }

    public static Object translateInput(BaseModel<?> oldModel) {
        Class<?> oldModelClass = oldModel.getClass();

        String oldModelClassName = oldModelClass.getName();

        if (oldModelClassName.equals(BudgetCategoryClp.class.getName())) {
            return translateInputBudgetCategory(oldModel);
        }

        if (oldModelClassName.equals(BudgetSubCategoryClp.class.getName())) {
            return translateInputBudgetSubCategory(oldModel);
        }

        if (oldModelClassName.equals(BuyerClp.class.getName())) {
            return translateInputBuyer(oldModel);
        }

        if (oldModelClassName.equals(CostCenterClp.class.getName())) {
            return translateInputCostCenter(oldModel);
        }

        if (oldModelClassName.equals(CostCenterUserClp.class.getName())) {
            return translateInputCostCenterUser(oldModel);
        }

        if (oldModelClassName.equals(GoodReceiptClp.class.getName())) {
            return translateInputGoodReceipt(oldModel);
        }

        if (oldModelClassName.equals(GRAuditClp.class.getName())) {
            return translateInputGRAudit(oldModel);
        }

        if (oldModelClassName.equals(MacroDriverClp.class.getName())) {
            return translateInputMacroDriver(oldModel);
        }

        if (oldModelClassName.equals(MacroMicroDriverClp.class.getName())) {
            return translateInputMacroMicroDriver(oldModel);
        }

        if (oldModelClassName.equals(MicroDriverClp.class.getName())) {
            return translateInputMicroDriver(oldModel);
        }

        if (oldModelClassName.equals(ODNPCodeClp.class.getName())) {
            return translateInputODNPCode(oldModel);
        }

        if (oldModelClassName.equals(ODNPNameClp.class.getName())) {
            return translateInputODNPName(oldModel);
        }

        if (oldModelClassName.equals(POAuditClp.class.getName())) {
            return translateInputPOAudit(oldModel);
        }

        if (oldModelClassName.equals(PositionClp.class.getName())) {
            return translateInputPosition(oldModel);
        }

        if (oldModelClassName.equals(PositionAuditClp.class.getName())) {
            return translateInputPositionAudit(oldModel);
        }

        if (oldModelClassName.equals(PRAuditClp.class.getName())) {
            return translateInputPRAudit(oldModel);
        }

        if (oldModelClassName.equals(ProjectClp.class.getName())) {
            return translateInputProject(oldModel);
        }

        if (oldModelClassName.equals(ProjectSubProjectClp.class.getName())) {
            return translateInputProjectSubProject(oldModel);
        }

        if (oldModelClassName.equals(PurchaseOrderClp.class.getName())) {
            return translateInputPurchaseOrder(oldModel);
        }

        if (oldModelClassName.equals(PurchaseRequestClp.class.getName())) {
            return translateInputPurchaseRequest(oldModel);
        }

        if (oldModelClassName.equals(SubProjectClp.class.getName())) {
            return translateInputSubProject(oldModel);
        }

        if (oldModelClassName.equals(TrackingCodesClp.class.getName())) {
            return translateInputTrackingCodes(oldModel);
        }

        if (oldModelClassName.equals(VendorClp.class.getName())) {
            return translateInputVendor(oldModel);
        }

        if (oldModelClassName.equals(WbsCodeClp.class.getName())) {
            return translateInputWbsCode(oldModel);
        }

		if (oldModelClassName.equals(CatalogoClp.class.getName())) {
			return translateInputCatalogo(oldModel);
		}
		
		if (oldModelClassName.equals(LayoutNotNssCatalogoClp.class.getName())) {
			return translateInputLayoutNotNssCatalogo(oldModel);
		}
		
        return oldModel;
    }

    public static Object translateInput(List<Object> oldList) {
        List<Object> newList = new ArrayList<Object>(oldList.size());

        for (int i = 0; i < oldList.size(); i++) {
            Object curObj = oldList.get(i);

            newList.add(translateInput(curObj));
        }

        return newList;
    }

    public static Object translateInputBudgetCategory(BaseModel<?> oldModel) {
        BudgetCategoryClp oldClpModel = (BudgetCategoryClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getBudgetCategoryRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputBudgetSubCategory(BaseModel<?> oldModel) {
        BudgetSubCategoryClp oldClpModel = (BudgetSubCategoryClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getBudgetSubCategoryRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputBuyer(BaseModel<?> oldModel) {
        BuyerClp oldClpModel = (BuyerClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getBuyerRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputCostCenter(BaseModel<?> oldModel) {
        CostCenterClp oldClpModel = (CostCenterClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getCostCenterRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputCostCenterUser(BaseModel<?> oldModel) {
        CostCenterUserClp oldClpModel = (CostCenterUserClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getCostCenterUserRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputGoodReceipt(BaseModel<?> oldModel) {
        GoodReceiptClp oldClpModel = (GoodReceiptClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getGoodReceiptRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputGRAudit(BaseModel<?> oldModel) {
        GRAuditClp oldClpModel = (GRAuditClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getGRAuditRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputMacroDriver(BaseModel<?> oldModel) {
        MacroDriverClp oldClpModel = (MacroDriverClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getMacroDriverRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputMacroMicroDriver(BaseModel<?> oldModel) {
        MacroMicroDriverClp oldClpModel = (MacroMicroDriverClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getMacroMicroDriverRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputMicroDriver(BaseModel<?> oldModel) {
        MicroDriverClp oldClpModel = (MicroDriverClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getMicroDriverRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputODNPCode(BaseModel<?> oldModel) {
        ODNPCodeClp oldClpModel = (ODNPCodeClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getODNPCodeRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputODNPName(BaseModel<?> oldModel) {
        ODNPNameClp oldClpModel = (ODNPNameClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getODNPNameRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputPOAudit(BaseModel<?> oldModel) {
        POAuditClp oldClpModel = (POAuditClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getPOAuditRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputPosition(BaseModel<?> oldModel) {
        PositionClp oldClpModel = (PositionClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getPositionRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputPositionAudit(BaseModel<?> oldModel) {
        PositionAuditClp oldClpModel = (PositionAuditClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getPositionAuditRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputPRAudit(BaseModel<?> oldModel) {
        PRAuditClp oldClpModel = (PRAuditClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getPRAuditRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputProject(BaseModel<?> oldModel) {
        ProjectClp oldClpModel = (ProjectClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getProjectRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputProjectSubProject(BaseModel<?> oldModel) {
        ProjectSubProjectClp oldClpModel = (ProjectSubProjectClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getProjectSubProjectRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputPurchaseOrder(BaseModel<?> oldModel) {
        PurchaseOrderClp oldClpModel = (PurchaseOrderClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getPurchaseOrderRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputPurchaseRequest(BaseModel<?> oldModel) {
        PurchaseRequestClp oldClpModel = (PurchaseRequestClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getPurchaseRequestRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputSubProject(BaseModel<?> oldModel) {
        SubProjectClp oldClpModel = (SubProjectClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getSubProjectRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputTrackingCodes(BaseModel<?> oldModel) {
        TrackingCodesClp oldClpModel = (TrackingCodesClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getTrackingCodesRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputVendor(BaseModel<?> oldModel) {
        VendorClp oldClpModel = (VendorClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getVendorRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputWbsCode(BaseModel<?> oldModel) {
        WbsCodeClp oldClpModel = (WbsCodeClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getWbsCodeRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

	public static Object translateInputCatalogo(BaseModel<?> oldModel) {
		CatalogoClp oldClpModel = (CatalogoClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getCatalogoRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}
	
	public static Object translateInputLayoutNotNssCatalogo(BaseModel<?> oldModel) {
		LayoutNotNssCatalogoClp oldClpModel = (LayoutNotNssCatalogoClp)oldModel;

		BaseModel<?> newModel = oldClpModel.getLayoutNotNssCatalogoRemoteModel();

		newModel.setModelAttributes(oldClpModel.getModelAttributes());

		return newModel;
	}
	
    public static Object translateInput(Object obj) {
        if (obj instanceof BaseModel<?>) {
            return translateInput((BaseModel<?>) obj);
        } else if (obj instanceof List<?>) {
            return translateInput((List<Object>) obj);
        } else {
            return obj;
        }
    }

    public static Object translateOutput(BaseModel<?> oldModel) {
        Class<?> oldModelClass = oldModel.getClass();

        String oldModelClassName = oldModelClass.getName();

        if (oldModelClassName.equals("com.hp.omp.model.impl.BudgetCategoryImpl")) {
            return translateOutputBudgetCategory(oldModel);
        }

        if (oldModelClassName.equals(
                    "com.hp.omp.model.impl.BudgetSubCategoryImpl")) {
            return translateOutputBudgetSubCategory(oldModel);
        }

        if (oldModelClassName.equals("com.hp.omp.model.impl.BuyerImpl")) {
            return translateOutputBuyer(oldModel);
        }

        if (oldModelClassName.equals("com.hp.omp.model.impl.CostCenterImpl")) {
            return translateOutputCostCenter(oldModel);
        }

        if (oldModelClassName.equals("com.hp.omp.model.impl.CostCenterUserImpl")) {
            return translateOutputCostCenterUser(oldModel);
        }

        if (oldModelClassName.equals("com.hp.omp.model.impl.GoodReceiptImpl")) {
            return translateOutputGoodReceipt(oldModel);
        }

        if (oldModelClassName.equals("com.hp.omp.model.impl.GRAuditImpl")) {
            return translateOutputGRAudit(oldModel);
        }

        if (oldModelClassName.equals("com.hp.omp.model.impl.MacroDriverImpl")) {
            return translateOutputMacroDriver(oldModel);
        }

        if (oldModelClassName.equals(
                    "com.hp.omp.model.impl.MacroMicroDriverImpl")) {
            return translateOutputMacroMicroDriver(oldModel);
        }

        if (oldModelClassName.equals("com.hp.omp.model.impl.MicroDriverImpl")) {
            return translateOutputMicroDriver(oldModel);
        }

        if (oldModelClassName.equals("com.hp.omp.model.impl.ODNPCodeImpl")) {
            return translateOutputODNPCode(oldModel);
        }

        if (oldModelClassName.equals("com.hp.omp.model.impl.ODNPNameImpl")) {
            return translateOutputODNPName(oldModel);
        }

        if (oldModelClassName.equals("com.hp.omp.model.impl.POAuditImpl")) {
            return translateOutputPOAudit(oldModel);
        }

        if (oldModelClassName.equals("com.hp.omp.model.impl.PositionImpl")) {
            return translateOutputPosition(oldModel);
        }

        if (oldModelClassName.equals("com.hp.omp.model.impl.PositionAuditImpl")) {
            return translateOutputPositionAudit(oldModel);
        }

        if (oldModelClassName.equals("com.hp.omp.model.impl.PRAuditImpl")) {
            return translateOutputPRAudit(oldModel);
        }

        if (oldModelClassName.equals("com.hp.omp.model.impl.ProjectImpl")) {
            return translateOutputProject(oldModel);
        }

        if (oldModelClassName.equals(
                    "com.hp.omp.model.impl.ProjectSubProjectImpl")) {
            return translateOutputProjectSubProject(oldModel);
        }

        if (oldModelClassName.equals("com.hp.omp.model.impl.PurchaseOrderImpl")) {
            return translateOutputPurchaseOrder(oldModel);
        }

        if (oldModelClassName.equals(
                    "com.hp.omp.model.impl.PurchaseRequestImpl")) {
            return translateOutputPurchaseRequest(oldModel);
        }

        if (oldModelClassName.equals("com.hp.omp.model.impl.SubProjectImpl")) {
            return translateOutputSubProject(oldModel);
        }

        if (oldModelClassName.equals("com.hp.omp.model.impl.TrackingCodesImpl")) {
            return translateOutputTrackingCodes(oldModel);
        }

        if (oldModelClassName.equals("com.hp.omp.model.impl.VendorImpl")) {
            return translateOutputVendor(oldModel);
        }

        if (oldModelClassName.equals("com.hp.omp.model.impl.WbsCodeImpl")) {
            return translateOutputWbsCode(oldModel);
        }

		if (oldModelClassName.equals("com.hp.omp.model.impl.CatalogoImpl")) {
			return translateOutputCatalogo(oldModel);
		}
		
		if (oldModelClassName.equals("com.hp.omp.model.impl.LayoutNotNssCatalogoImpl")) {
			return translateOutputLayoutNotNssCatalogo(oldModel);
		}
		
        return oldModel;
    }

    public static Object translateOutput(List<Object> oldList) {
        List<Object> newList = new ArrayList<Object>(oldList.size());

        for (int i = 0; i < oldList.size(); i++) {
            Object curObj = oldList.get(i);

            newList.add(translateOutput(curObj));
        }

        return newList;
    }

    public static Object translateOutput(Object obj) {
        if (obj instanceof BaseModel<?>) {
            return translateOutput((BaseModel<?>) obj);
        } else if (obj instanceof List<?>) {
            return translateOutput((List<Object>) obj);
        } else {
            return obj;
        }
    }

    public static Throwable translateThrowable(Throwable throwable) {
        if (_useReflectionToTranslateThrowable) {
            try {
                UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

                objectOutputStream.writeObject(throwable);

                objectOutputStream.flush();
                objectOutputStream.close();

                UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
                        0, unsyncByteArrayOutputStream.size());

                Thread currentThread = Thread.currentThread();

                ClassLoader contextClassLoader = currentThread.getContextClassLoader();

                ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
                        contextClassLoader);

                throwable = (Throwable) objectInputStream.readObject();

                objectInputStream.close();

                return throwable;
            } catch (SecurityException se) {
                if (_log.isInfoEnabled()) {
                    _log.info("Do not use reflection to translate throwable");
                }

                _useReflectionToTranslateThrowable = false;
            } catch (Throwable throwable2) {
                _log.error(throwable2, throwable2);

                return throwable2;
            }
        }

        Class<?> clazz = throwable.getClass();

        String className = clazz.getName();

        if (className.equals(PortalException.class.getName())) {
            return new PortalException();
        }

        if (className.equals(SystemException.class.getName())) {
            return new SystemException();
        }

        if (className.equals("com.hp.omp.NoSuchBudgetCategoryException")) {
            return new com.hp.omp.NoSuchBudgetCategoryException();
        }

        if (className.equals("com.hp.omp.NoSuchBudgetSubCategoryException")) {
            return new com.hp.omp.NoSuchBudgetSubCategoryException();
        }

        if (className.equals("com.hp.omp.NoSuchBuyerException")) {
            return new com.hp.omp.NoSuchBuyerException();
        }

        if (className.equals("com.hp.omp.NoSuchCostCenterException")) {
            return new com.hp.omp.NoSuchCostCenterException();
        }

        if (className.equals("com.hp.omp.NoSuchCostCenterUserException")) {
            return new com.hp.omp.NoSuchCostCenterUserException();
        }

        if (className.equals("com.hp.omp.NoSuchGoodReceiptException")) {
            return new com.hp.omp.NoSuchGoodReceiptException();
        }

        if (className.equals("com.hp.omp.NoSuchGRAuditException")) {
            return new com.hp.omp.NoSuchGRAuditException();
        }

        if (className.equals("com.hp.omp.NoSuchMacroDriverException")) {
            return new com.hp.omp.NoSuchMacroDriverException();
        }

        if (className.equals("com.hp.omp.NoSuchMacroMicroDriverException")) {
            return new com.hp.omp.NoSuchMacroMicroDriverException();
        }

        if (className.equals("com.hp.omp.NoSuchMicroDriverException")) {
            return new com.hp.omp.NoSuchMicroDriverException();
        }

        if (className.equals("com.hp.omp.NoSuchODNPCodeException")) {
            return new com.hp.omp.NoSuchODNPCodeException();
        }

        if (className.equals("com.hp.omp.NoSuchODNPNameException")) {
            return new com.hp.omp.NoSuchODNPNameException();
        }

        if (className.equals("com.hp.omp.NoSuchPOAuditException")) {
            return new com.hp.omp.NoSuchPOAuditException();
        }

        if (className.equals("com.hp.omp.NoSuchPositionException")) {
            return new com.hp.omp.NoSuchPositionException();
        }

        if (className.equals("com.hp.omp.NoSuchPositionAuditException")) {
            return new com.hp.omp.NoSuchPositionAuditException();
        }

        if (className.equals("com.hp.omp.NoSuchPRAuditException")) {
            return new com.hp.omp.NoSuchPRAuditException();
        }

        if (className.equals("com.hp.omp.NoSuchProjectException")) {
            return new com.hp.omp.NoSuchProjectException();
        }

        if (className.equals("com.hp.omp.NoSuchProjectSubProjectException")) {
            return new com.hp.omp.NoSuchProjectSubProjectException();
        }

        if (className.equals("com.hp.omp.NoSuchPurchaseOrderException")) {
            return new com.hp.omp.NoSuchPurchaseOrderException();
        }

        if (className.equals("com.hp.omp.NoSuchPurchaseRequestException")) {
            return new com.hp.omp.NoSuchPurchaseRequestException();
        }

        if (className.equals("com.hp.omp.NoSuchSubProjectException")) {
            return new com.hp.omp.NoSuchSubProjectException();
        }

        if (className.equals("com.hp.omp.NoSuchTrackingCodesException")) {
            return new com.hp.omp.NoSuchTrackingCodesException();
        }

        if (className.equals("com.hp.omp.NoSuchVendorException")) {
            return new com.hp.omp.NoSuchVendorException();
        }

        if (className.equals("com.hp.omp.NoSuchWbsCodeException")) {
            return new com.hp.omp.NoSuchWbsCodeException();
        }
        
		if (className.equals("com.hp.omp.NoSuchCatalogoException")) {
			return new com.hp.omp.NoSuchCatalogoException();
		}
		
		if (className.equals("com.hp.omp.NoSuchLayoutNotNssCatalogoException")) {
			return new com.hp.omp.NoSuchLayoutNotNssCatalogoException();
		}

        return throwable;
    }
    
	public static Object translateOutputCatalogo(BaseModel<?> oldModel) {
		CatalogoClp newModel = new CatalogoClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setCatalogoRemoteModel(oldModel);

		return newModel;
	}
	
	public static Object translateOutputLayoutNotNssCatalogo(BaseModel<?> oldModel) {
		LayoutNotNssCatalogoClp newModel = new LayoutNotNssCatalogoClp();

		newModel.setModelAttributes(oldModel.getModelAttributes());

		newModel.setLayoutNotNssCatalogoRemoteModel(oldModel);

		return newModel;
	}

    public static Object translateOutputBudgetCategory(BaseModel<?> oldModel) {
        BudgetCategoryClp newModel = new BudgetCategoryClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setBudgetCategoryRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputBudgetSubCategory(BaseModel<?> oldModel) {
        BudgetSubCategoryClp newModel = new BudgetSubCategoryClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setBudgetSubCategoryRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputBuyer(BaseModel<?> oldModel) {
        BuyerClp newModel = new BuyerClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setBuyerRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputCostCenter(BaseModel<?> oldModel) {
        CostCenterClp newModel = new CostCenterClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setCostCenterRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputCostCenterUser(BaseModel<?> oldModel) {
        CostCenterUserClp newModel = new CostCenterUserClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setCostCenterUserRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputGoodReceipt(BaseModel<?> oldModel) {
        GoodReceiptClp newModel = new GoodReceiptClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setGoodReceiptRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputGRAudit(BaseModel<?> oldModel) {
        GRAuditClp newModel = new GRAuditClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setGRAuditRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputMacroDriver(BaseModel<?> oldModel) {
        MacroDriverClp newModel = new MacroDriverClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setMacroDriverRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputMacroMicroDriver(BaseModel<?> oldModel) {
        MacroMicroDriverClp newModel = new MacroMicroDriverClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setMacroMicroDriverRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputMicroDriver(BaseModel<?> oldModel) {
        MicroDriverClp newModel = new MicroDriverClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setMicroDriverRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputODNPCode(BaseModel<?> oldModel) {
        ODNPCodeClp newModel = new ODNPCodeClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setODNPCodeRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputODNPName(BaseModel<?> oldModel) {
        ODNPNameClp newModel = new ODNPNameClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setODNPNameRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputPOAudit(BaseModel<?> oldModel) {
        POAuditClp newModel = new POAuditClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setPOAuditRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputPosition(BaseModel<?> oldModel) {
        PositionClp newModel = new PositionClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setPositionRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputPositionAudit(BaseModel<?> oldModel) {
        PositionAuditClp newModel = new PositionAuditClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setPositionAuditRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputPRAudit(BaseModel<?> oldModel) {
        PRAuditClp newModel = new PRAuditClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setPRAuditRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputProject(BaseModel<?> oldModel) {
        ProjectClp newModel = new ProjectClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setProjectRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputProjectSubProject(BaseModel<?> oldModel) {
        ProjectSubProjectClp newModel = new ProjectSubProjectClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setProjectSubProjectRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputPurchaseOrder(BaseModel<?> oldModel) {
        PurchaseOrderClp newModel = new PurchaseOrderClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setPurchaseOrderRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputPurchaseRequest(BaseModel<?> oldModel) {
        PurchaseRequestClp newModel = new PurchaseRequestClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setPurchaseRequestRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputSubProject(BaseModel<?> oldModel) {
        SubProjectClp newModel = new SubProjectClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setSubProjectRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputTrackingCodes(BaseModel<?> oldModel) {
        TrackingCodesClp newModel = new TrackingCodesClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setTrackingCodesRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputVendor(BaseModel<?> oldModel) {
        VendorClp newModel = new VendorClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setVendorRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputWbsCode(BaseModel<?> oldModel) {
        WbsCodeClp newModel = new WbsCodeClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setWbsCodeRemoteModel(oldModel);

        return newModel;
    }
}
