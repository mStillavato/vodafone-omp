package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the PurchaseRequest service. Represents a row in the &quot;PURCHASE_REQUEST&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see PurchaseRequestModel
 * @see com.hp.omp.model.impl.PurchaseRequestImpl
 * @see com.hp.omp.model.impl.PurchaseRequestModelImpl
 * @generated
 */
public interface PurchaseRequest extends PurchaseRequestModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.PurchaseRequestImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public java.lang.String getCostCenterName();

    public void setCostCenterName(java.lang.String costCenterName);

    public com.hp.omp.model.custom.PurchaseRequestStatus getPurchaseRequestStatus();

    public boolean isHasFile();

    public boolean isDisabledProject();
}
