package com.hp.omp.service.persistence;

import com.hp.omp.model.TrackingCodes;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the tracking codes service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see TrackingCodesPersistenceImpl
 * @see TrackingCodesUtil
 * @generated
 */
public interface TrackingCodesPersistence extends BasePersistence<TrackingCodes> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link TrackingCodesUtil} to access the tracking codes persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the tracking codes in the entity cache if it is enabled.
    *
    * @param trackingCodes the tracking codes
    */
    public void cacheResult(com.hp.omp.model.TrackingCodes trackingCodes);

    /**
    * Caches the tracking codeses in the entity cache if it is enabled.
    *
    * @param trackingCodeses the tracking codeses
    */
    public void cacheResult(
        java.util.List<com.hp.omp.model.TrackingCodes> trackingCodeses);

    /**
    * Creates a new tracking codes with the primary key. Does not add the tracking codes to the database.
    *
    * @param trackingCodeId the primary key for the new tracking codes
    * @return the new tracking codes
    */
    public com.hp.omp.model.TrackingCodes create(long trackingCodeId);

    /**
    * Removes the tracking codes with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param trackingCodeId the primary key of the tracking codes
    * @return the tracking codes that was removed
    * @throws com.hp.omp.NoSuchTrackingCodesException if a tracking codes with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.TrackingCodes remove(long trackingCodeId)
        throws com.hp.omp.NoSuchTrackingCodesException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.TrackingCodes updateImpl(
        com.hp.omp.model.TrackingCodes trackingCodes, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the tracking codes with the primary key or throws a {@link com.hp.omp.NoSuchTrackingCodesException} if it could not be found.
    *
    * @param trackingCodeId the primary key of the tracking codes
    * @return the tracking codes
    * @throws com.hp.omp.NoSuchTrackingCodesException if a tracking codes with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.TrackingCodes findByPrimaryKey(long trackingCodeId)
        throws com.hp.omp.NoSuchTrackingCodesException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the tracking codes with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param trackingCodeId the primary key of the tracking codes
    * @return the tracking codes, or <code>null</code> if a tracking codes with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.TrackingCodes fetchByPrimaryKey(long trackingCodeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the tracking codeses.
    *
    * @return the tracking codeses
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.TrackingCodes> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the tracking codeses.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of tracking codeses
    * @param end the upper bound of the range of tracking codeses (not inclusive)
    * @return the range of tracking codeses
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.TrackingCodes> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the tracking codeses.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of tracking codeses
    * @param end the upper bound of the range of tracking codeses (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of tracking codeses
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.TrackingCodes> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the tracking codeses from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of tracking codeses.
    *
    * @return the number of tracking codeses
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
