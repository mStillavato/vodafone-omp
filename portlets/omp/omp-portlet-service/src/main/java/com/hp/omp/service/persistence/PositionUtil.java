package com.hp.omp.service.persistence;

import com.hp.omp.model.Position;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the position service. This utility wraps {@link PositionPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see PositionPersistence
 * @see PositionPersistenceImpl
 * @generated
 */
public class PositionUtil {
    private static PositionPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Position position) {
        getPersistence().clearCache(position);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Position> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Position> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Position> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static Position update(Position position, boolean merge)
        throws SystemException {
        return getPersistence().update(position, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static Position update(Position position, boolean merge,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(position, merge, serviceContext);
    }

    /**
    * Caches the position in the entity cache if it is enabled.
    *
    * @param position the position
    */
    public static void cacheResult(com.hp.omp.model.Position position) {
        getPersistence().cacheResult(position);
    }

    /**
    * Caches the positions in the entity cache if it is enabled.
    *
    * @param positions the positions
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.Position> positions) {
        getPersistence().cacheResult(positions);
    }

    /**
    * Creates a new position with the primary key. Does not add the position to the database.
    *
    * @param positionId the primary key for the new position
    * @return the new position
    */
    public static com.hp.omp.model.Position create(long positionId) {
        return getPersistence().create(positionId);
    }

    /**
    * Removes the position with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param positionId the primary key of the position
    * @return the position that was removed
    * @throws com.hp.omp.NoSuchPositionException if a position with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Position remove(long positionId)
        throws com.hp.omp.NoSuchPositionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(positionId);
    }

    public static com.hp.omp.model.Position updateImpl(
        com.hp.omp.model.Position position, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(position, merge);
    }

    /**
    * Returns the position with the primary key or throws a {@link com.hp.omp.NoSuchPositionException} if it could not be found.
    *
    * @param positionId the primary key of the position
    * @return the position
    * @throws com.hp.omp.NoSuchPositionException if a position with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Position findByPrimaryKey(long positionId)
        throws com.hp.omp.NoSuchPositionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(positionId);
    }

    /**
    * Returns the position with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param positionId the primary key of the position
    * @return the position, or <code>null</code> if a position with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Position fetchByPrimaryKey(long positionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(positionId);
    }

    /**
    * Returns all the positions where purchaseOrderId = &#63;.
    *
    * @param purchaseOrderId the purchase order ID
    * @return the matching positions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.Position> findByPurchaseOrderPositions(
        java.lang.String purchaseOrderId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPurchaseOrderPositions(purchaseOrderId);
    }

    /**
    * Returns a range of all the positions where purchaseOrderId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param purchaseOrderId the purchase order ID
    * @param start the lower bound of the range of positions
    * @param end the upper bound of the range of positions (not inclusive)
    * @return the range of matching positions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.Position> findByPurchaseOrderPositions(
        java.lang.String purchaseOrderId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPurchaseOrderPositions(purchaseOrderId, start, end);
    }

    /**
    * Returns an ordered range of all the positions where purchaseOrderId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param purchaseOrderId the purchase order ID
    * @param start the lower bound of the range of positions
    * @param end the upper bound of the range of positions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching positions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.Position> findByPurchaseOrderPositions(
        java.lang.String purchaseOrderId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPurchaseOrderPositions(purchaseOrderId, start, end,
            orderByComparator);
    }

    /**
    * Returns the first position in the ordered set where purchaseOrderId = &#63;.
    *
    * @param purchaseOrderId the purchase order ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching position
    * @throws com.hp.omp.NoSuchPositionException if a matching position could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Position findByPurchaseOrderPositions_First(
        java.lang.String purchaseOrderId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchPositionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPurchaseOrderPositions_First(purchaseOrderId,
            orderByComparator);
    }

    /**
    * Returns the first position in the ordered set where purchaseOrderId = &#63;.
    *
    * @param purchaseOrderId the purchase order ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching position, or <code>null</code> if a matching position could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Position fetchByPurchaseOrderPositions_First(
        java.lang.String purchaseOrderId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByPurchaseOrderPositions_First(purchaseOrderId,
            orderByComparator);
    }

    /**
    * Returns the last position in the ordered set where purchaseOrderId = &#63;.
    *
    * @param purchaseOrderId the purchase order ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching position
    * @throws com.hp.omp.NoSuchPositionException if a matching position could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Position findByPurchaseOrderPositions_Last(
        java.lang.String purchaseOrderId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchPositionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPurchaseOrderPositions_Last(purchaseOrderId,
            orderByComparator);
    }

    /**
    * Returns the last position in the ordered set where purchaseOrderId = &#63;.
    *
    * @param purchaseOrderId the purchase order ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching position, or <code>null</code> if a matching position could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Position fetchByPurchaseOrderPositions_Last(
        java.lang.String purchaseOrderId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByPurchaseOrderPositions_Last(purchaseOrderId,
            orderByComparator);
    }

    /**
    * Returns the positions before and after the current position in the ordered set where purchaseOrderId = &#63;.
    *
    * @param positionId the primary key of the current position
    * @param purchaseOrderId the purchase order ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next position
    * @throws com.hp.omp.NoSuchPositionException if a position with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Position[] findByPurchaseOrderPositions_PrevAndNext(
        long positionId, java.lang.String purchaseOrderId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchPositionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPurchaseOrderPositions_PrevAndNext(positionId,
            purchaseOrderId, orderByComparator);
    }

    /**
    * Returns all the positions where purchaseRequestId = &#63;.
    *
    * @param purchaseRequestId the purchase request ID
    * @return the matching positions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.Position> findByPurchaseRequestPositions(
        java.lang.String purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPurchaseRequestPositions(purchaseRequestId);
    }

    /**
    * Returns a range of all the positions where purchaseRequestId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param purchaseRequestId the purchase request ID
    * @param start the lower bound of the range of positions
    * @param end the upper bound of the range of positions (not inclusive)
    * @return the range of matching positions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.Position> findByPurchaseRequestPositions(
        java.lang.String purchaseRequestId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPurchaseRequestPositions(purchaseRequestId, start, end);
    }

    /**
    * Returns an ordered range of all the positions where purchaseRequestId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param purchaseRequestId the purchase request ID
    * @param start the lower bound of the range of positions
    * @param end the upper bound of the range of positions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching positions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.Position> findByPurchaseRequestPositions(
        java.lang.String purchaseRequestId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPurchaseRequestPositions(purchaseRequestId, start,
            end, orderByComparator);
    }

    /**
    * Returns the first position in the ordered set where purchaseRequestId = &#63;.
    *
    * @param purchaseRequestId the purchase request ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching position
    * @throws com.hp.omp.NoSuchPositionException if a matching position could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Position findByPurchaseRequestPositions_First(
        java.lang.String purchaseRequestId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchPositionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPurchaseRequestPositions_First(purchaseRequestId,
            orderByComparator);
    }

    /**
    * Returns the first position in the ordered set where purchaseRequestId = &#63;.
    *
    * @param purchaseRequestId the purchase request ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching position, or <code>null</code> if a matching position could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Position fetchByPurchaseRequestPositions_First(
        java.lang.String purchaseRequestId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByPurchaseRequestPositions_First(purchaseRequestId,
            orderByComparator);
    }

    /**
    * Returns the last position in the ordered set where purchaseRequestId = &#63;.
    *
    * @param purchaseRequestId the purchase request ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching position
    * @throws com.hp.omp.NoSuchPositionException if a matching position could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Position findByPurchaseRequestPositions_Last(
        java.lang.String purchaseRequestId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchPositionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPurchaseRequestPositions_Last(purchaseRequestId,
            orderByComparator);
    }

    /**
    * Returns the last position in the ordered set where purchaseRequestId = &#63;.
    *
    * @param purchaseRequestId the purchase request ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching position, or <code>null</code> if a matching position could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Position fetchByPurchaseRequestPositions_Last(
        java.lang.String purchaseRequestId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByPurchaseRequestPositions_Last(purchaseRequestId,
            orderByComparator);
    }

    /**
    * Returns the positions before and after the current position in the ordered set where purchaseRequestId = &#63;.
    *
    * @param positionId the primary key of the current position
    * @param purchaseRequestId the purchase request ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next position
    * @throws com.hp.omp.NoSuchPositionException if a position with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Position[] findByPurchaseRequestPositions_PrevAndNext(
        long positionId, java.lang.String purchaseRequestId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchPositionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPurchaseRequestPositions_PrevAndNext(positionId,
            purchaseRequestId, orderByComparator);
    }

    /**
    * Returns all the positions.
    *
    * @return the positions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.Position> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the positions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of positions
    * @param end the upper bound of the range of positions (not inclusive)
    * @return the range of positions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.Position> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the positions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of positions
    * @param end the upper bound of the range of positions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of positions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.Position> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the positions where purchaseOrderId = &#63; from the database.
    *
    * @param purchaseOrderId the purchase order ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByPurchaseOrderPositions(
        java.lang.String purchaseOrderId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByPurchaseOrderPositions(purchaseOrderId);
    }

    /**
    * Removes all the positions where purchaseRequestId = &#63; from the database.
    *
    * @param purchaseRequestId the purchase request ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByPurchaseRequestPositions(
        java.lang.String purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByPurchaseRequestPositions(purchaseRequestId);
    }

    /**
    * Removes all the positions from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of positions where purchaseOrderId = &#63;.
    *
    * @param purchaseOrderId the purchase order ID
    * @return the number of matching positions
    * @throws SystemException if a system exception occurred
    */
    public static int countByPurchaseOrderPositions(
        java.lang.String purchaseOrderId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByPurchaseOrderPositions(purchaseOrderId);
    }

    /**
    * Returns the number of positions where purchaseRequestId = &#63;.
    *
    * @param purchaseRequestId the purchase request ID
    * @return the number of matching positions
    * @throws SystemException if a system exception occurred
    */
    public static int countByPurchaseRequestPositions(
        java.lang.String purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByPurchaseRequestPositions(purchaseRequestId);
    }

    /**
    * Returns the number of positions.
    *
    * @return the number of positions
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static PositionPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (PositionPersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    PositionPersistence.class.getName());

            ReferenceRegistry.registerReference(PositionUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(PositionPersistence persistence) {
    }
}
