package com.hp.omp.service.persistence;

public interface MicroDriverFinder {
    public java.util.List<com.hp.omp.model.MicroDriver> getMicroDriverByMacroDriverId(
        java.lang.Long macroDriverId)
        throws com.liferay.portal.kernel.exception.SystemException;
    
    public java.util.List<com.hp.omp.model.MicroDriver> getMicroDriversList(
    		int start, int end, String searchFilter)
            throws com.liferay.portal.kernel.exception.SystemException;
}
