package com.hp.omp.service.persistence;

import com.hp.omp.model.MicroDriver;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the micro driver service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see MicroDriverPersistenceImpl
 * @see MicroDriverUtil
 * @generated
 */
public interface MicroDriverPersistence extends BasePersistence<MicroDriver> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link MicroDriverUtil} to access the micro driver persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the micro driver in the entity cache if it is enabled.
    *
    * @param microDriver the micro driver
    */
    public void cacheResult(com.hp.omp.model.MicroDriver microDriver);

    /**
    * Caches the micro drivers in the entity cache if it is enabled.
    *
    * @param microDrivers the micro drivers
    */
    public void cacheResult(
        java.util.List<com.hp.omp.model.MicroDriver> microDrivers);

    /**
    * Creates a new micro driver with the primary key. Does not add the micro driver to the database.
    *
    * @param microDriverId the primary key for the new micro driver
    * @return the new micro driver
    */
    public com.hp.omp.model.MicroDriver create(long microDriverId);

    /**
    * Removes the micro driver with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param microDriverId the primary key of the micro driver
    * @return the micro driver that was removed
    * @throws com.hp.omp.NoSuchMicroDriverException if a micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MicroDriver remove(long microDriverId)
        throws com.hp.omp.NoSuchMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.MicroDriver updateImpl(
        com.hp.omp.model.MicroDriver microDriver, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the micro driver with the primary key or throws a {@link com.hp.omp.NoSuchMicroDriverException} if it could not be found.
    *
    * @param microDriverId the primary key of the micro driver
    * @return the micro driver
    * @throws com.hp.omp.NoSuchMicroDriverException if a micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MicroDriver findByPrimaryKey(long microDriverId)
        throws com.hp.omp.NoSuchMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the micro driver with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param microDriverId the primary key of the micro driver
    * @return the micro driver, or <code>null</code> if a micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MicroDriver fetchByPrimaryKey(long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the micro drivers.
    *
    * @return the micro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MicroDriver> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the micro drivers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of micro drivers
    * @param end the upper bound of the range of micro drivers (not inclusive)
    * @return the range of micro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MicroDriver> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the micro drivers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of micro drivers
    * @param end the upper bound of the range of micro drivers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of micro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MicroDriver> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the micro drivers from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of micro drivers.
    *
    * @return the number of micro drivers
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
