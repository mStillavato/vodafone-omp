/**
 * 
 */
package com.hp.omp.model.custom;

import java.io.Serializable;

/**
 * @author Sami Mohamed
 *
 */
public class CatalogoExportListDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 149304978050477779L;
	
	private String chiaveInforecord;
	private String dataCreazione;
	private String dataFineValidita;
	private String divisione;
	private String flgCanc;
	private String fornFinCode;
	private String fornitoreCode;
	private String fornitoreDesc;
	private String matCatLvl2;
	private String matCatLvl2Desc;
	private String matCatLvl4;
	private String matCatLvl4Desc;
	private String matCod;
	private String matCodFornFinale;
	private String matDesc;
	private String prezzo;
	private String purchOrganiz;
	private String um;
	private String umPrezzo;
	private String umPrezzoPo;
	private String valuta;
	private String upperMatDesc;
	
	public String getChiaveInforecord() {
		return chiaveInforecord;
	}
	public void setChiaveInforecord(String chiaveInforecord) {
		this.chiaveInforecord = chiaveInforecord;
	}
	public String getDataCreazione() {
		return dataCreazione;
	}
	public void setDataCreazione(String dataCreazione) {
		this.dataCreazione = dataCreazione;
	}
	public String getDataFineValidita() {
		return dataFineValidita;
	}
	public void setDataFineValidita(String dataFineValidita) {
		this.dataFineValidita = dataFineValidita;
	}
	public String getDivisione() {
		return divisione;
	}
	public void setDivisione(String divisione) {
		this.divisione = divisione;
	}
	public String getFlgCanc() {
		return flgCanc;
	}
	public void setFlgCanc(String flgCanc) {
		this.flgCanc = flgCanc;
	}
	public String getFornFinCode() {
		return fornFinCode;
	}
	public void setFornFinCode(String fornFinCode) {
		this.fornFinCode = fornFinCode;
	}
	public String getFornitoreCode() {
		return fornitoreCode;
	}
	public void setFornitoreCode(String fornitoreCode) {
		this.fornitoreCode = fornitoreCode;
	}
	public String getFornitoreDesc() {
		return fornitoreDesc;
	}
	public void setFornitoreDesc(String fornitoreDesc) {
		this.fornitoreDesc = fornitoreDesc;
	}
	public String getMatCatLvl2() {
		return matCatLvl2;
	}
	public void setMatCatLvl2(String matCatLvl2) {
		this.matCatLvl2 = matCatLvl2;
	}
	public String getMatCatLvl2Desc() {
		return matCatLvl2Desc;
	}
	public void setMatCatLvl2Desc(String matCatLvl2Desc) {
		this.matCatLvl2Desc = matCatLvl2Desc;
	}
	public String getMatCatLvl4() {
		return matCatLvl4;
	}
	public void setMatCatLvl4(String matCatLvl4) {
		this.matCatLvl4 = matCatLvl4;
	}
	public String getMatCatLvl4Desc() {
		return matCatLvl4Desc;
	}
	public void setMatCatLvl4Desc(String matCatLvl4Desc) {
		this.matCatLvl4Desc = matCatLvl4Desc;
	}
	public String getMatCod() {
		return matCod;
	}
	public void setMatCod(String matCod) {
		this.matCod = matCod;
	}
	public String getMatCodFornFinale() {
		return matCodFornFinale;
	}
	public void setMatCodFornFinale(String matCodFornFinale) {
		this.matCodFornFinale = matCodFornFinale;
	}
	public String getMatDesc() {
		return matDesc;
	}
	public void setMatDesc(String matDesc) {
		this.matDesc = matDesc;
	}
	public String getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(String prezzo) {
		this.prezzo = prezzo;
	}
	public String getPurchOrganiz() {
		return purchOrganiz;
	}
	public void setPurchOrganiz(String purchOrganiz) {
		this.purchOrganiz = purchOrganiz;
	}
	public String getUm() {
		return um;
	}
	public void setUm(String um) {
		this.um = um;
	}
	public String getUmPrezzo() {
		return umPrezzo;
	}
	public void setUmPrezzo(String umPrezzo) {
		this.umPrezzo = umPrezzo;
	}
	public String getUmPrezzoPo() {
		return umPrezzoPo;
	}
	public void setUmPrezzoPo(String umPrezzoPo) {
		this.umPrezzoPo = umPrezzoPo;
	}
	public String getValuta() {
		return valuta;
	}
	public void setValuta(String valuta) {
		this.valuta = valuta;
	}
	public String getUpperMatDesc() {
		return upperMatDesc;
	}
	public void setUpperMatDesc(String upperMatDesc) {
		this.upperMatDesc = upperMatDesc;
	}
}
