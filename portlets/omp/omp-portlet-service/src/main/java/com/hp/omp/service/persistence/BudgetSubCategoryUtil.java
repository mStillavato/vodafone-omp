package com.hp.omp.service.persistence;

import com.hp.omp.model.BudgetSubCategory;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the budget sub category service. This utility wraps {@link BudgetSubCategoryPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see BudgetSubCategoryPersistence
 * @see BudgetSubCategoryPersistenceImpl
 * @generated
 */
public class BudgetSubCategoryUtil {
    private static BudgetSubCategoryPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(BudgetSubCategory budgetSubCategory) {
        getPersistence().clearCache(budgetSubCategory);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<BudgetSubCategory> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<BudgetSubCategory> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<BudgetSubCategory> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static BudgetSubCategory update(
        BudgetSubCategory budgetSubCategory, boolean merge)
        throws SystemException {
        return getPersistence().update(budgetSubCategory, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static BudgetSubCategory update(
        BudgetSubCategory budgetSubCategory, boolean merge,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(budgetSubCategory, merge, serviceContext);
    }

    /**
    * Caches the budget sub category in the entity cache if it is enabled.
    *
    * @param budgetSubCategory the budget sub category
    */
    public static void cacheResult(
        com.hp.omp.model.BudgetSubCategory budgetSubCategory) {
        getPersistence().cacheResult(budgetSubCategory);
    }

    /**
    * Caches the budget sub categories in the entity cache if it is enabled.
    *
    * @param budgetSubCategories the budget sub categories
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.BudgetSubCategory> budgetSubCategories) {
        getPersistence().cacheResult(budgetSubCategories);
    }

    /**
    * Creates a new budget sub category with the primary key. Does not add the budget sub category to the database.
    *
    * @param budgetSubCategoryId the primary key for the new budget sub category
    * @return the new budget sub category
    */
    public static com.hp.omp.model.BudgetSubCategory create(
        long budgetSubCategoryId) {
        return getPersistence().create(budgetSubCategoryId);
    }

    /**
    * Removes the budget sub category with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param budgetSubCategoryId the primary key of the budget sub category
    * @return the budget sub category that was removed
    * @throws com.hp.omp.NoSuchBudgetSubCategoryException if a budget sub category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.BudgetSubCategory remove(
        long budgetSubCategoryId)
        throws com.hp.omp.NoSuchBudgetSubCategoryException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(budgetSubCategoryId);
    }

    public static com.hp.omp.model.BudgetSubCategory updateImpl(
        com.hp.omp.model.BudgetSubCategory budgetSubCategory, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(budgetSubCategory, merge);
    }

    /**
    * Returns the budget sub category with the primary key or throws a {@link com.hp.omp.NoSuchBudgetSubCategoryException} if it could not be found.
    *
    * @param budgetSubCategoryId the primary key of the budget sub category
    * @return the budget sub category
    * @throws com.hp.omp.NoSuchBudgetSubCategoryException if a budget sub category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.BudgetSubCategory findByPrimaryKey(
        long budgetSubCategoryId)
        throws com.hp.omp.NoSuchBudgetSubCategoryException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(budgetSubCategoryId);
    }

    /**
    * Returns the budget sub category with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param budgetSubCategoryId the primary key of the budget sub category
    * @return the budget sub category, or <code>null</code> if a budget sub category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.BudgetSubCategory fetchByPrimaryKey(
        long budgetSubCategoryId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(budgetSubCategoryId);
    }

    /**
    * Returns all the budget sub categories.
    *
    * @return the budget sub categories
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.BudgetSubCategory> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the budget sub categories.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of budget sub categories
    * @param end the upper bound of the range of budget sub categories (not inclusive)
    * @return the range of budget sub categories
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.BudgetSubCategory> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the budget sub categories.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of budget sub categories
    * @param end the upper bound of the range of budget sub categories (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of budget sub categories
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.BudgetSubCategory> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the budget sub categories from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of budget sub categories.
    *
    * @return the number of budget sub categories
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static BudgetSubCategoryPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (BudgetSubCategoryPersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    BudgetSubCategoryPersistence.class.getName());

            ReferenceRegistry.registerReference(BudgetSubCategoryUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(BudgetSubCategoryPersistence persistence) {
    }
}
