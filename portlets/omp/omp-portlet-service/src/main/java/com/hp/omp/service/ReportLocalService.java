package com.hp.omp.service;

import java.util.List;

import com.hp.omp.model.custom.ReportCheckIcDTO;
import com.hp.omp.model.custom.SearchReportCheckIcDTO;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.BaseLocalService;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The interface for the report local service.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see ReportLocalServiceUtil
 * @see com.hp.omp.service.base.ReportLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.ReportLocalServiceImpl
 * @generated
 */
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
    PortalException.class, SystemException.class}
)
public interface ReportLocalService extends BaseLocalService,
    InvokableLocalService {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link ReportLocalServiceUtil} to access the report local service. Add custom service methods to {@link com.hp.omp.service.impl.ReportLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier();

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier);

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<java.lang.String> getAllCostCenters()
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<java.lang.String> getAllProjectNames()
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<java.lang.String> getSubProjects(
        java.lang.Long projectId)
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<java.lang.String> getAllFiscalYears()
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.lang.Double generateAggregateReportClosed(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.lang.Double generateAggregateReportClosing(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.lang.Double generateAggregateReportTotalPoistionValues(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.lang.Double generateAggregatePrPositionsWithoutPoNumberValue(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.util.List<com.hp.omp.model.custom.ExcelTableRow> generateDetailReport(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException;

    public void exportDetailsReportAsExcel(
        com.hp.omp.model.custom.ReportDTO reportDTO,
        java.util.List<java.lang.Object> header, java.io.OutputStream out)
        throws com.liferay.portal.kernel.exception.SystemException;

    public void exportReportAsExcel(
        com.hp.omp.model.custom.ReportDTO reportDTO,
        java.util.List<java.lang.Object> header, java.io.OutputStream out)
        throws com.liferay.portal.kernel.exception.SystemException;
    
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public List<ReportCheckIcDTO> getReportCheckIcList(SearchReportCheckIcDTO searchDTO) throws SystemException;
    
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public long getReportCheckIcListCount(SearchReportCheckIcDTO searchDTO) throws SystemException;
    
    public void exportReportCheckIcAsExcel(java.util.List<java.lang.Object> header, 
    		java.io.OutputStream out)
            throws com.liferay.portal.kernel.exception.SystemException;
}
