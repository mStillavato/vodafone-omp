package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link PRAudit}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       PRAudit
 * @generated
 */
public class PRAuditWrapper implements PRAudit, ModelWrapper<PRAudit> {
    private PRAudit _prAudit;

    public PRAuditWrapper(PRAudit prAudit) {
        _prAudit = prAudit;
    }

    public Class<?> getModelClass() {
        return PRAudit.class;
    }

    public String getModelClassName() {
        return PRAudit.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("auditId", getAuditId());
        attributes.put("purchaseRequestId", getPurchaseRequestId());
        attributes.put("status", getStatus());
        attributes.put("userId", getUserId());
        attributes.put("changeDate", getChangeDate());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long auditId = (Long) attributes.get("auditId");

        if (auditId != null) {
            setAuditId(auditId);
        }

        Long purchaseRequestId = (Long) attributes.get("purchaseRequestId");

        if (purchaseRequestId != null) {
            setPurchaseRequestId(purchaseRequestId);
        }

        Integer status = (Integer) attributes.get("status");

        if (status != null) {
            setStatus(status);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        Date changeDate = (Date) attributes.get("changeDate");

        if (changeDate != null) {
            setChangeDate(changeDate);
        }
    }

    /**
    * Returns the primary key of this p r audit.
    *
    * @return the primary key of this p r audit
    */
    public long getPrimaryKey() {
        return _prAudit.getPrimaryKey();
    }

    /**
    * Sets the primary key of this p r audit.
    *
    * @param primaryKey the primary key of this p r audit
    */
    public void setPrimaryKey(long primaryKey) {
        _prAudit.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the audit ID of this p r audit.
    *
    * @return the audit ID of this p r audit
    */
    public long getAuditId() {
        return _prAudit.getAuditId();
    }

    /**
    * Sets the audit ID of this p r audit.
    *
    * @param auditId the audit ID of this p r audit
    */
    public void setAuditId(long auditId) {
        _prAudit.setAuditId(auditId);
    }

    /**
    * Returns the purchase request ID of this p r audit.
    *
    * @return the purchase request ID of this p r audit
    */
    public java.lang.Long getPurchaseRequestId() {
        return _prAudit.getPurchaseRequestId();
    }

    /**
    * Sets the purchase request ID of this p r audit.
    *
    * @param purchaseRequestId the purchase request ID of this p r audit
    */
    public void setPurchaseRequestId(java.lang.Long purchaseRequestId) {
        _prAudit.setPurchaseRequestId(purchaseRequestId);
    }

    /**
    * Returns the status of this p r audit.
    *
    * @return the status of this p r audit
    */
    public int getStatus() {
        return _prAudit.getStatus();
    }

    /**
    * Sets the status of this p r audit.
    *
    * @param status the status of this p r audit
    */
    public void setStatus(int status) {
        _prAudit.setStatus(status);
    }

    /**
    * Returns the user ID of this p r audit.
    *
    * @return the user ID of this p r audit
    */
    public java.lang.Long getUserId() {
        return _prAudit.getUserId();
    }

    /**
    * Sets the user ID of this p r audit.
    *
    * @param userId the user ID of this p r audit
    */
    public void setUserId(java.lang.Long userId) {
        _prAudit.setUserId(userId);
    }

    /**
    * Returns the change date of this p r audit.
    *
    * @return the change date of this p r audit
    */
    public java.util.Date getChangeDate() {
        return _prAudit.getChangeDate();
    }

    /**
    * Sets the change date of this p r audit.
    *
    * @param changeDate the change date of this p r audit
    */
    public void setChangeDate(java.util.Date changeDate) {
        _prAudit.setChangeDate(changeDate);
    }

    public boolean isNew() {
        return _prAudit.isNew();
    }

    public void setNew(boolean n) {
        _prAudit.setNew(n);
    }

    public boolean isCachedModel() {
        return _prAudit.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _prAudit.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _prAudit.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _prAudit.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _prAudit.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _prAudit.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _prAudit.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new PRAuditWrapper((PRAudit) _prAudit.clone());
    }

    public int compareTo(PRAudit prAudit) {
        return _prAudit.compareTo(prAudit);
    }

    @Override
    public int hashCode() {
        return _prAudit.hashCode();
    }

    public com.liferay.portal.model.CacheModel<PRAudit> toCacheModel() {
        return _prAudit.toCacheModel();
    }

    public PRAudit toEscapedModel() {
        return new PRAuditWrapper(_prAudit.toEscapedModel());
    }

    public PRAudit toUnescapedModel() {
        return new PRAuditWrapper(_prAudit.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _prAudit.toString();
    }

    public java.lang.String toXmlString() {
        return _prAudit.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _prAudit.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PRAuditWrapper)) {
            return false;
        }

        PRAuditWrapper prAuditWrapper = (PRAuditWrapper) obj;

        if (Validator.equals(_prAudit, prAuditWrapper._prAudit)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public PRAudit getWrappedPRAudit() {
        return _prAudit;
    }

    public PRAudit getWrappedModel() {
        return _prAudit;
    }

    public void resetOriginalValues() {
        _prAudit.resetOriginalValues();
    }
}
