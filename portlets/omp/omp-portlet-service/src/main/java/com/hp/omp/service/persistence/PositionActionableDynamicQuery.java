package com.hp.omp.service.persistence;

import com.hp.omp.model.Position;
import com.hp.omp.service.PositionLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class PositionActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public PositionActionableDynamicQuery() throws SystemException {
        setBaseLocalService(PositionLocalServiceUtil.getService());
        setClass(Position.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("positionId");
    }
}
