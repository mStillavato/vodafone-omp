package com.hp.omp.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the vendor local service. This utility wraps {@link com.hp.omp.service.impl.VendorLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see VendorLocalService
 * @see com.hp.omp.service.base.VendorLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.VendorLocalServiceImpl
 * @generated
 */
public class VendorLocalServiceUtil {
	private static VendorLocalService _service;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.hp.omp.service.impl.VendorLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the vendor to the database. Also notifies the appropriate model listeners.
	 *
	 * @param vendor the vendor
	 * @return the vendor that was added
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.Vendor addVendor(
			com.hp.omp.model.Vendor vendor)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addVendor(vendor);
	}

	/**
	 * Creates a new vendor with the primary key. Does not add the vendor to the database.
	 *
	 * @param vendorId the primary key for the new vendor
	 * @return the new vendor
	 */
	public static com.hp.omp.model.Vendor createVendor(long vendorId) {
		return getService().createVendor(vendorId);
	}

	/**
	 * Deletes the vendor with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param vendorId the primary key of the vendor
	 * @return the vendor that was removed
	 * @throws PortalException if a vendor with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.Vendor deleteVendor(long vendorId)
			throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteVendor(vendorId);
	}

	/**
	 * Deletes the vendor from the database. Also notifies the appropriate model listeners.
	 *
	 * @param vendor the vendor
	 * @return the vendor that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.Vendor deleteVendor(
			com.hp.omp.model.Vendor vendor)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteVendor(vendor);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
			int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
			int end,
			com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				.dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows that match the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows that match the dynamic query
	 * @throws SystemException if a system exception occurred
	 */
	public static long dynamicQueryCount(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static com.hp.omp.model.Vendor fetchVendor(long vendorId)
			throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchVendor(vendorId);
	}

	/**
	 * Returns the vendor with the primary key.
	 *
	 * @param vendorId the primary key of the vendor
	 * @return the vendor
	 * @throws PortalException if a vendor with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.Vendor getVendor(long vendorId)
			throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getVendor(vendorId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
					throws com.liferay.portal.kernel.exception.PortalException,
					com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns a range of all the vendors.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of vendors
	 * @param end the upper bound of the range of vendors (not inclusive)
	 * @return the range of vendors
	 * @throws SystemException if a system exception occurred
	 */
	public static java.util.List<com.hp.omp.model.Vendor> getVendors(
			int start, int end)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getVendors(start, end);
	}

	/**
	 * Returns the number of vendors.
	 *
	 * @return the number of vendors
	 * @throws SystemException if a system exception occurred
	 */
	public static int getVendorsCount()
			throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getVendorsCount();
	}

	public static int getVendorsCountList(String searchFilter, int gruppoUsers)
			throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getVendorsCountList(searchFilter, gruppoUsers);
	}

	/**
	 * Updates the vendor in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param vendor the vendor
	 * @return the vendor that was updated
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.Vendor updateVendor(
			com.hp.omp.model.Vendor vendor)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateVendor(vendor);
	}

	/**
	 * Updates the vendor in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param vendor the vendor
	 * @param merge whether to merge the vendor with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	 * @return the vendor that was updated
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.Vendor updateVendor(
			com.hp.omp.model.Vendor vendor, boolean merge)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateVendor(vendor, merge);
	}

	/**
	 * Returns the Spring bean ID for this bean.
	 *
	 * @return the Spring bean ID for this bean
	 */
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	 * Sets the Spring bean ID for this bean.
	 *
	 * @param beanIdentifier the Spring bean ID for this bean
	 */
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
			java.lang.String[] parameterTypes, java.lang.Object[] arguments)
					throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static com.hp.omp.model.Vendor getVendorByName(java.lang.String name, int gruppoUsers)
			throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getVendorByName(name, gruppoUsers);
	}

	public static void clearService() {
		_service = null;
	}

	public static VendorLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					VendorLocalService.class.getName());

			if (invokableLocalService instanceof VendorLocalService) {
				_service = (VendorLocalService) invokableLocalService;
			} else {
				_service = new VendorLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(VendorLocalServiceUtil.class,
					"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	 public void setService(VendorLocalService service) {
	}

	public static java.util.List<com.hp.omp.model.Vendor> getVendorsList(
			int start, int end, String searchFilter, int gruppoUsers)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getVendorsList(start, end, searchFilter, gruppoUsers);
	}

	public static com.hp.omp.model.Vendor getVendorBySupplierCode(
			String supplierCode, int gruppoUsers)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getVendorBySupplierCode(supplierCode, gruppoUsers);
	}

	public static void exportVendorsAsExcel(
			java.util.List<com.hp.omp.model.Vendor> listVendors,
			java.util.List<java.lang.Object> header, java.io.OutputStream out)
					throws com.liferay.portal.kernel.exception.SystemException {
		getService().exportVendorsAsExcel(listVendors, header, out);
	}
}
