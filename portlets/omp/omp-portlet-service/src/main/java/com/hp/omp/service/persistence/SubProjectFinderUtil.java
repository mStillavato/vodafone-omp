package com.hp.omp.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class SubProjectFinderUtil {
    private static SubProjectFinder _finder;

    public static java.util.List<com.hp.omp.model.SubProject> getSubProjectByProjectId(
        java.lang.Long projectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getFinder().getSubProjectByProjectId(projectId);
    }
    
    public static java.util.List<com.hp.omp.model.SubProject> getSubProjectsList(
        	int start, int end, String searchFilter)
            throws com.liferay.portal.kernel.exception.SystemException {
            return getFinder().getSubProjectsList(start, end, searchFilter);
        }

    public static SubProjectFinder getFinder() {
        if (_finder == null) {
            _finder = (SubProjectFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    SubProjectFinder.class.getName());

            ReferenceRegistry.registerReference(SubProjectFinderUtil.class,
                "_finder");
        }

        return _finder;
    }

    public void setFinder(SubProjectFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(SubProjectFinderUtil.class,
            "_finder");
    }
}
