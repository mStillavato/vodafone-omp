package com.hp.omp.service;

import java.io.OutputStream;
import java.util.List;

import com.hp.omp.model.custom.ReportCheckIcDTO;
import com.hp.omp.model.custom.SearchReportCheckIcDTO;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link ReportLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       ReportLocalService
 * @generated
 */
public class ReportLocalServiceWrapper implements ReportLocalService,
    ServiceWrapper<ReportLocalService> {
    private ReportLocalService _reportLocalService;

    public ReportLocalServiceWrapper(ReportLocalService reportLocalService) {
        _reportLocalService = reportLocalService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _reportLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _reportLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _reportLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    public java.util.List<java.lang.String> getAllCostCenters()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _reportLocalService.getAllCostCenters();
    }

    public java.util.List<java.lang.String> getAllProjectNames()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _reportLocalService.getAllProjectNames();
    }

    public java.util.List<java.lang.String> getSubProjects(
        java.lang.Long projectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _reportLocalService.getSubProjects(projectId);
    }

    public java.util.List<java.lang.String> getAllFiscalYears()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _reportLocalService.getAllFiscalYears();
    }

    public java.lang.Double generateAggregateReportClosed(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _reportLocalService.generateAggregateReportClosed(reportDTO);
    }

    public java.lang.Double generateAggregateReportClosing(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _reportLocalService.generateAggregateReportClosing(reportDTO);
    }

    public java.lang.Double generateAggregateReportTotalPoistionValues(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _reportLocalService.generateAggregateReportTotalPoistionValues(reportDTO);
    }

    public java.lang.Double generateAggregatePrPositionsWithoutPoNumberValue(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _reportLocalService.generateAggregatePrPositionsWithoutPoNumberValue(reportDTO);
    }

    public java.util.List<com.hp.omp.model.custom.ExcelTableRow> generateDetailReport(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _reportLocalService.generateDetailReport(reportDTO);
    }

    public void exportDetailsReportAsExcel(
        com.hp.omp.model.custom.ReportDTO reportDTO,
        java.util.List<java.lang.Object> header, java.io.OutputStream out)
        throws com.liferay.portal.kernel.exception.SystemException {
        _reportLocalService.exportDetailsReportAsExcel(reportDTO, header, out);
    }

    public void exportReportAsExcel(
        com.hp.omp.model.custom.ReportDTO reportDTO,
        java.util.List<java.lang.Object> header, java.io.OutputStream out)
        throws com.liferay.portal.kernel.exception.SystemException {
        _reportLocalService.exportReportAsExcel(reportDTO, header, out);
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public ReportLocalService getWrappedReportLocalService() {
        return _reportLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedReportLocalService(
        ReportLocalService reportLocalService) {
        _reportLocalService = reportLocalService;
    }

    public ReportLocalService getWrappedService() {
        return _reportLocalService;
    }

    public void setWrappedService(ReportLocalService reportLocalService) {
        _reportLocalService = reportLocalService;
    }

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<ReportCheckIcDTO> getReportCheckIcList(SearchReportCheckIcDTO searchDTO) throws SystemException {
		return _reportLocalService.getReportCheckIcList(searchDTO);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long getReportCheckIcListCount(SearchReportCheckIcDTO searchDTO) throws SystemException {
		return _reportLocalService.getReportCheckIcListCount(searchDTO);
	}

	public void exportReportCheckIcAsExcel(List<Object> header, OutputStream out)
			throws SystemException {
        _reportLocalService.exportReportCheckIcAsExcel(header, out);
    }
}
