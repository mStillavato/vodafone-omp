package com.hp.omp.service;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.BaseLocalService;
import com.liferay.portal.service.InvokableLocalService;
import com.liferay.portal.service.PersistedModelLocalService;

/**
 * The interface for the wbs code local service.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see WbsCodeLocalServiceUtil
 * @see com.hp.omp.service.base.WbsCodeLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.WbsCodeLocalServiceImpl
 * @generated
 */
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
    PortalException.class, SystemException.class}
)
public interface WbsCodeLocalService extends BaseLocalService,
    InvokableLocalService, PersistedModelLocalService {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link WbsCodeLocalServiceUtil} to access the wbs code local service. Add custom service methods to {@link com.hp.omp.service.impl.WbsCodeLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */

    /**
    * Adds the wbs code to the database. Also notifies the appropriate model listeners.
    *
    * @param wbsCode the wbs code
    * @return the wbs code that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.WbsCode addWbsCode(com.hp.omp.model.WbsCode wbsCode)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Creates a new wbs code with the primary key. Does not add the wbs code to the database.
    *
    * @param wbsCodeId the primary key for the new wbs code
    * @return the new wbs code
    */
    public com.hp.omp.model.WbsCode createWbsCode(long wbsCodeId);

    /**
    * Deletes the wbs code with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param wbsCodeId the primary key of the wbs code
    * @return the wbs code that was removed
    * @throws PortalException if a wbs code with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.WbsCode deleteWbsCode(long wbsCodeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Deletes the wbs code from the database. Also notifies the appropriate model listeners.
    *
    * @param wbsCode the wbs code
    * @return the wbs code that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.WbsCode deleteWbsCode(
        com.hp.omp.model.WbsCode wbsCode)
        throws com.liferay.portal.kernel.exception.SystemException;

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery();

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public com.hp.omp.model.WbsCode fetchWbsCode(long wbsCodeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the wbs code with the primary key.
    *
    * @param wbsCodeId the primary key of the wbs code
    * @return the wbs code
    * @throws PortalException if a wbs code with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public com.hp.omp.model.WbsCode getWbsCode(long wbsCodeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the wbs codes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of wbs codes
    * @param end the upper bound of the range of wbs codes (not inclusive)
    * @return the range of wbs codes
    * @throws SystemException if a system exception occurred
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.hp.omp.model.WbsCode> getWbsCodes(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of wbs codes.
    *
    * @return the number of wbs codes
    * @throws SystemException if a system exception occurred
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getWbsCodesCount()
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getWbsCodesCountList(String searchFilter)
        throws com.liferay.portal.kernel.exception.SystemException;
    
    /**
    * Updates the wbs code in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param wbsCode the wbs code
    * @return the wbs code that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.WbsCode updateWbsCode(
        com.hp.omp.model.WbsCode wbsCode)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Updates the wbs code in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param wbsCode the wbs code
    * @param merge whether to merge the wbs code with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the wbs code that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.WbsCode updateWbsCode(
        com.hp.omp.model.WbsCode wbsCode, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier();

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier);

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public com.hp.omp.model.WbsCode getWbsCodeByName(java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException;
    
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.hp.omp.model.WbsCode> getWbsCodesList(int start,
        int end, String searchFilter) throws com.liferay.portal.kernel.exception.SystemException;
    
    public void exportWbsCodesAsExcel(
            List<com.hp.omp.model.WbsCode> listWbsCodes,
            java.util.List<java.lang.Object> header, java.io.OutputStream out)
            throws com.liferay.portal.kernel.exception.SystemException;
}
