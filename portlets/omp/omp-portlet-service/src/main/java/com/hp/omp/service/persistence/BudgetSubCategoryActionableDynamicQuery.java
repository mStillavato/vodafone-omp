package com.hp.omp.service.persistence;

import com.hp.omp.model.BudgetSubCategory;
import com.hp.omp.service.BudgetSubCategoryLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class BudgetSubCategoryActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public BudgetSubCategoryActionableDynamicQuery() throws SystemException {
        setBaseLocalService(BudgetSubCategoryLocalServiceUtil.getService());
        setClass(BudgetSubCategory.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("budgetSubCategoryId");
    }
}
