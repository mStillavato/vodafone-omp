package com.hp.omp.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link PurchaseOrderLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       PurchaseOrderLocalService
 * @generated
 */
public class PurchaseOrderLocalServiceWrapper
    implements PurchaseOrderLocalService,
        ServiceWrapper<PurchaseOrderLocalService> {
    private PurchaseOrderLocalService _purchaseOrderLocalService;

    public PurchaseOrderLocalServiceWrapper(
        PurchaseOrderLocalService purchaseOrderLocalService) {
        _purchaseOrderLocalService = purchaseOrderLocalService;
    }

    /**
    * Adds the purchase order to the database. Also notifies the appropriate model listeners.
    *
    * @param purchaseOrder the purchase order
    * @return the purchase order that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseOrder addPurchaseOrder(
        com.hp.omp.model.PurchaseOrder purchaseOrder)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.addPurchaseOrder(purchaseOrder);
    }

    /**
    * Creates a new purchase order with the primary key. Does not add the purchase order to the database.
    *
    * @param purchaseOrderId the primary key for the new purchase order
    * @return the new purchase order
    */
    public com.hp.omp.model.PurchaseOrder createPurchaseOrder(
        long purchaseOrderId) {
        return _purchaseOrderLocalService.createPurchaseOrder(purchaseOrderId);
    }

    /**
    * Deletes the purchase order with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param purchaseOrderId the primary key of the purchase order
    * @return the purchase order that was removed
    * @throws PortalException if a purchase order with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseOrder deletePurchaseOrder(
        long purchaseOrderId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.deletePurchaseOrder(purchaseOrderId);
    }

    /**
    * Deletes the purchase order from the database. Also notifies the appropriate model listeners.
    *
    * @param purchaseOrder the purchase order
    * @return the purchase order that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseOrder deletePurchaseOrder(
        com.hp.omp.model.PurchaseOrder purchaseOrder)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.deletePurchaseOrder(purchaseOrder);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _purchaseOrderLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.dynamicQueryCount(dynamicQuery);
    }

    public com.hp.omp.model.PurchaseOrder fetchPurchaseOrder(
        long purchaseOrderId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.fetchPurchaseOrder(purchaseOrderId);
    }

    /**
    * Returns the purchase order with the primary key.
    *
    * @param purchaseOrderId the primary key of the purchase order
    * @return the purchase order
    * @throws PortalException if a purchase order with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseOrder getPurchaseOrder(long purchaseOrderId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.getPurchaseOrder(purchaseOrderId);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the purchase orders.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of purchase orders
    * @param end the upper bound of the range of purchase orders (not inclusive)
    * @return the range of purchase orders
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrders(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.getPurchaseOrders(start, end);
    }

    /**
    * Returns the number of purchase orders.
    *
    * @return the number of purchase orders
    * @throws SystemException if a system exception occurred
    */
    public int getPurchaseOrdersCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.getPurchaseOrdersCount();
    }

    /**
    * Updates the purchase order in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param purchaseOrder the purchase order
    * @return the purchase order that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseOrder updatePurchaseOrder(
        com.hp.omp.model.PurchaseOrder purchaseOrder)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.updatePurchaseOrder(purchaseOrder);
    }

    /**
    * Updates the purchase order in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param purchaseOrder the purchase order
    * @param merge whether to merge the purchase order with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the purchase order that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseOrder updatePurchaseOrder(
        com.hp.omp.model.PurchaseOrder purchaseOrder, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.updatePurchaseOrder(purchaseOrder,
            merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _purchaseOrderLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _purchaseOrderLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _purchaseOrderLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrdersListByStatus(
        int status, int start, int end, java.lang.String orderby,
        java.lang.String order) throws java.lang.Exception {
        return _purchaseOrderLocalService.getPurchaseOrdersListByStatus(status,
            start, end, orderby, order);
    }

    public java.lang.Long getPurchaseOrdersCountByStatus(int status)
        throws java.lang.Exception {
        return _purchaseOrderLocalService.getPurchaseOrdersCountByStatus(status);
    }

    public long getPurchaseOrdersGRRequestedCount(
        com.hp.omp.model.custom.SearchDTO searchDto) {
        return _purchaseOrderLocalService.getPurchaseOrdersGRRequestedCount(searchDto);
    }

    public java.util.List<com.hp.omp.model.custom.PoListDTO> getPurchaseOrdersGRRequested(
        com.hp.omp.model.custom.SearchDTO searchDto) {
        return _purchaseOrderLocalService.getPurchaseOrdersGRRequested(searchDto);
    }

    public long getPurchaseOrdersGRClosedCount(
        com.hp.omp.model.custom.SearchDTO searchDto) {
        return _purchaseOrderLocalService.getPurchaseOrdersGRClosedCount(searchDto);
    }

    public java.util.List<com.hp.omp.model.custom.PoListDTO> getPurchaseOrdersGRClosed(
        com.hp.omp.model.custom.SearchDTO searchDto) {
        return _purchaseOrderLocalService.getPurchaseOrdersGRClosed(searchDto);
    }

    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrdersListByStatusAndCostCenter(
        int status, long costCenterId, int start, int end,
        java.lang.String orderby, java.lang.String order)
        throws java.lang.Exception {
        return _purchaseOrderLocalService.getPurchaseOrdersListByStatusAndCostCenter(status,
            costCenterId, start, end, orderby, order);
    }

    public java.lang.Long getPurchaseOrdersCountByStatusAndCostCenter(
        int status, long costCenterId) throws java.lang.Exception {
        return _purchaseOrderLocalService.getPurchaseOrdersCountByStatusAndCostCenter(status,
            costCenterId);
    }

    public int getTotalNumberOfPurchaseOrdersByStatus(int status)
        throws java.lang.Exception {
        return _purchaseOrderLocalService.getTotalNumberOfPurchaseOrdersByStatus(status);
    }

    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrderByProjectId(
        java.lang.String projectId) throws java.lang.Exception {
        return _purchaseOrderLocalService.getPurchaseOrderByProjectId(projectId);
    }

    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrderBySubProjectId(
        java.lang.String subProjectId) throws java.lang.Exception {
        return _purchaseOrderLocalService.getPurchaseOrderBySubProjectId(subProjectId);
    }

    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrderByVendorId(
        java.lang.String vendorId) throws java.lang.Exception {
        return _purchaseOrderLocalService.getPurchaseOrderByVendorId(vendorId);
    }

    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrderByBudgetCategoryId(
        java.lang.String budgetCategoryId) throws java.lang.Exception {
        return _purchaseOrderLocalService.getPurchaseOrderByBudgetCategoryId(budgetCategoryId);
    }

    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrderByBudgetSubCategoryId(
        java.lang.String budgetSubCategoryId) throws java.lang.Exception {
        return _purchaseOrderLocalService.getPurchaseOrderByBudgetSubCategoryId(budgetSubCategoryId);
    }

    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrderByOdnpNameId(
        java.lang.String odnpNameId) throws java.lang.Exception {
        return _purchaseOrderLocalService.getPurchaseOrderByOdnpNameId(odnpNameId);
    }

    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrderByMacroDriverId(
        java.lang.String macroDriverId) throws java.lang.Exception {
        return _purchaseOrderLocalService.getPurchaseOrderByMacroDriverId(macroDriverId);
    }

    public java.util.List<com.hp.omp.model.PurchaseOrder> getPurchaseOrderByMicroDriverId(
        java.lang.String microDriverId) throws java.lang.Exception {
        return _purchaseOrderLocalService.getPurchaseOrderByMicroDriverId(microDriverId);
    }

    public java.util.List<java.lang.Long> getPONumbers()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.getPONumbers();
    }

    public java.util.List<java.lang.String> getAllPOProjects()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.getAllPOProjects();
    }

    public java.util.List<java.lang.String> getSubProjects(
        java.lang.String projectName)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.getSubProjects(projectName);
    }

    public java.util.List<com.hp.omp.model.custom.PoListDTO> getAdvancedSearchResults(
        com.hp.omp.model.custom.SearchDTO searchDTO, int start, int end,
        java.lang.String orderby, java.lang.String order)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.getAdvancedSearchResults(searchDTO,
            start, end, orderby, order);
    }

    public java.util.List<com.hp.omp.model.custom.PoListDTO> getAdvancedSearchGrRequestedResults(
        com.hp.omp.model.custom.SearchDTO searchDTO, int start, int end,
        java.lang.String orderby, java.lang.String order)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.getAdvancedSearchGrRequestedResults(searchDTO,
            start, end, orderby, order);
    }

    public java.lang.Long getAdvancedSearchResultsCount(
        com.hp.omp.model.custom.SearchDTO searchDTO)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.getAdvancedSearchResultsCount(searchDTO);
    }

    public java.lang.Long getAdvancedSearchGrRequestedResultsCount(
        com.hp.omp.model.custom.SearchDTO searchDTO)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseOrderLocalService.getAdvancedSearchGrRequestedResultsCount(searchDTO);
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public PurchaseOrderLocalService getWrappedPurchaseOrderLocalService() {
        return _purchaseOrderLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedPurchaseOrderLocalService(
        PurchaseOrderLocalService purchaseOrderLocalService) {
        _purchaseOrderLocalService = purchaseOrderLocalService;
    }

    public PurchaseOrderLocalService getWrappedService() {
        return _purchaseOrderLocalService;
    }

    public void setWrappedService(
        PurchaseOrderLocalService purchaseOrderLocalService) {
        _purchaseOrderLocalService = purchaseOrderLocalService;
    }
}
