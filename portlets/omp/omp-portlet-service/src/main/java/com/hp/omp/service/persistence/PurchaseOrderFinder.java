package com.hp.omp.service.persistence;

public interface PurchaseOrderFinder {
    public long getPurchaseOrdersGRRequestedCount(
        com.hp.omp.model.custom.SearchDTO searchDto);

    public long getPurchaseOrdersGRClosedCount(
        com.hp.omp.model.custom.SearchDTO searchDto);

    public java.util.List<com.hp.omp.model.custom.PoListDTO> getPurchaseOrdersGRRequested(
        com.hp.omp.model.custom.SearchDTO searchDto);

    public java.util.List<com.hp.omp.model.custom.PoListDTO> getPurchaseOrdersGRClosed(
        com.hp.omp.model.custom.SearchDTO searchDto);

    public java.util.List<java.lang.Long> getPONumbers()
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.util.List<java.lang.String> getAllPOProjects()
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.util.List<java.lang.String> getSubProjects(
        java.lang.String projectName)
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.util.List<com.hp.omp.model.custom.PoListDTO> getAdvancedSearchResults(
        com.hp.omp.model.custom.SearchDTO searchDTO, int start, int end,
        java.lang.String orderby, java.lang.String order)
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.lang.Long getAdvancedSearchResultsCount(
        com.hp.omp.model.custom.SearchDTO searchDTO)
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.util.List<com.hp.omp.model.custom.PoListDTO> getAdvancedSearchGrRequestedResults(
        com.hp.omp.model.custom.SearchDTO searchDTO, int start, int end,
        java.lang.String orderby, java.lang.String order)
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.lang.Long getAdvancedSearchGrRequestedResultsCount(
        com.hp.omp.model.custom.SearchDTO searchDTO)
        throws com.liferay.portal.kernel.exception.SystemException;
}
