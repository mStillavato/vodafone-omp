package com.hp.omp.service.persistence;

import com.hp.omp.model.GRAudit;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the g r audit service. This utility wraps {@link GRAuditPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see GRAuditPersistence
 * @see GRAuditPersistenceImpl
 * @generated
 */
public class GRAuditUtil {
    private static GRAuditPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(GRAudit grAudit) {
        getPersistence().clearCache(grAudit);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<GRAudit> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<GRAudit> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<GRAudit> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static GRAudit update(GRAudit grAudit, boolean merge)
        throws SystemException {
        return getPersistence().update(grAudit, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static GRAudit update(GRAudit grAudit, boolean merge,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(grAudit, merge, serviceContext);
    }

    /**
    * Caches the g r audit in the entity cache if it is enabled.
    *
    * @param grAudit the g r audit
    */
    public static void cacheResult(com.hp.omp.model.GRAudit grAudit) {
        getPersistence().cacheResult(grAudit);
    }

    /**
    * Caches the g r audits in the entity cache if it is enabled.
    *
    * @param grAudits the g r audits
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.GRAudit> grAudits) {
        getPersistence().cacheResult(grAudits);
    }

    /**
    * Creates a new g r audit with the primary key. Does not add the g r audit to the database.
    *
    * @param auditId the primary key for the new g r audit
    * @return the new g r audit
    */
    public static com.hp.omp.model.GRAudit create(long auditId) {
        return getPersistence().create(auditId);
    }

    /**
    * Removes the g r audit with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param auditId the primary key of the g r audit
    * @return the g r audit that was removed
    * @throws com.hp.omp.NoSuchGRAuditException if a g r audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.GRAudit remove(long auditId)
        throws com.hp.omp.NoSuchGRAuditException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(auditId);
    }

    public static com.hp.omp.model.GRAudit updateImpl(
        com.hp.omp.model.GRAudit grAudit, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(grAudit, merge);
    }

    /**
    * Returns the g r audit with the primary key or throws a {@link com.hp.omp.NoSuchGRAuditException} if it could not be found.
    *
    * @param auditId the primary key of the g r audit
    * @return the g r audit
    * @throws com.hp.omp.NoSuchGRAuditException if a g r audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.GRAudit findByPrimaryKey(long auditId)
        throws com.hp.omp.NoSuchGRAuditException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(auditId);
    }

    /**
    * Returns the g r audit with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param auditId the primary key of the g r audit
    * @return the g r audit, or <code>null</code> if a g r audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.GRAudit fetchByPrimaryKey(long auditId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(auditId);
    }

    /**
    * Returns all the g r audits.
    *
    * @return the g r audits
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.GRAudit> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the g r audits.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of g r audits
    * @param end the upper bound of the range of g r audits (not inclusive)
    * @return the range of g r audits
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.GRAudit> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the g r audits.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of g r audits
    * @param end the upper bound of the range of g r audits (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of g r audits
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.GRAudit> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the g r audits from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of g r audits.
    *
    * @return the number of g r audits
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static GRAuditPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (GRAuditPersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    GRAuditPersistence.class.getName());

            ReferenceRegistry.registerReference(GRAuditUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(GRAuditPersistence persistence) {
    }
}
