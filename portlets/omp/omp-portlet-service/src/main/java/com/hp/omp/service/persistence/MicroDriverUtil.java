package com.hp.omp.service.persistence;

import com.hp.omp.model.MicroDriver;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the micro driver service. This utility wraps {@link MicroDriverPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see MicroDriverPersistence
 * @see MicroDriverPersistenceImpl
 * @generated
 */
public class MicroDriverUtil {
    private static MicroDriverPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(MicroDriver microDriver) {
        getPersistence().clearCache(microDriver);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<MicroDriver> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<MicroDriver> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<MicroDriver> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static MicroDriver update(MicroDriver microDriver, boolean merge)
        throws SystemException {
        return getPersistence().update(microDriver, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static MicroDriver update(MicroDriver microDriver, boolean merge,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(microDriver, merge, serviceContext);
    }

    /**
    * Caches the micro driver in the entity cache if it is enabled.
    *
    * @param microDriver the micro driver
    */
    public static void cacheResult(com.hp.omp.model.MicroDriver microDriver) {
        getPersistence().cacheResult(microDriver);
    }

    /**
    * Caches the micro drivers in the entity cache if it is enabled.
    *
    * @param microDrivers the micro drivers
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.MicroDriver> microDrivers) {
        getPersistence().cacheResult(microDrivers);
    }

    /**
    * Creates a new micro driver with the primary key. Does not add the micro driver to the database.
    *
    * @param microDriverId the primary key for the new micro driver
    * @return the new micro driver
    */
    public static com.hp.omp.model.MicroDriver create(long microDriverId) {
        return getPersistence().create(microDriverId);
    }

    /**
    * Removes the micro driver with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param microDriverId the primary key of the micro driver
    * @return the micro driver that was removed
    * @throws com.hp.omp.NoSuchMicroDriverException if a micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MicroDriver remove(long microDriverId)
        throws com.hp.omp.NoSuchMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(microDriverId);
    }

    public static com.hp.omp.model.MicroDriver updateImpl(
        com.hp.omp.model.MicroDriver microDriver, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(microDriver, merge);
    }

    /**
    * Returns the micro driver with the primary key or throws a {@link com.hp.omp.NoSuchMicroDriverException} if it could not be found.
    *
    * @param microDriverId the primary key of the micro driver
    * @return the micro driver
    * @throws com.hp.omp.NoSuchMicroDriverException if a micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MicroDriver findByPrimaryKey(
        long microDriverId)
        throws com.hp.omp.NoSuchMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(microDriverId);
    }

    /**
    * Returns the micro driver with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param microDriverId the primary key of the micro driver
    * @return the micro driver, or <code>null</code> if a micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MicroDriver fetchByPrimaryKey(
        long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(microDriverId);
    }

    /**
    * Returns all the micro drivers.
    *
    * @return the micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MicroDriver> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the micro drivers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of micro drivers
    * @param end the upper bound of the range of micro drivers (not inclusive)
    * @return the range of micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MicroDriver> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the micro drivers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of micro drivers
    * @param end the upper bound of the range of micro drivers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MicroDriver> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the micro drivers from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of micro drivers.
    *
    * @return the number of micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static MicroDriverPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (MicroDriverPersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    MicroDriverPersistence.class.getName());

            ReferenceRegistry.registerReference(MicroDriverUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(MicroDriverPersistence persistence) {
    }
}
