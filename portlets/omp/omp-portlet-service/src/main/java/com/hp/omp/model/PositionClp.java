package com.hp.omp.model;

import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.PositionLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class PositionClp extends BaseModelImpl<Position> implements Position {
    private long _positionId;
    private String _fiscalYear;
    private String _description;
    private String _categoryCode;
    private String _offerNumber;
    private Date _offerDate;
    private String _quatity;
    private Date _deliveryDate;
    private String _deliveryAddress;
    private String _actualUnitCost;
    private String _numberOfUnit;
    private long _labelNumber;
    private long _poLabelNumber;
    private String _targaTecnica;
    private boolean _approved;
    private String _icApproval;
    private String _approver;
    private String _assetNumber;
    private String _shoppingCart;
    private String _poNumber;
    private String _evoLocation;
    private String _wbsCodeId;
    private String _purchaseRequestId;
    private String _purchaseOrderId;
    private Date _createdDate;
    private long _createdUserId;
    private String _createdUserUuid;
    private String _evoCodMateriale;
    private String _evoDesMateriale;
    private BaseModel<?> _positionRemoteModel;

    public PositionClp() {
    }

    public Class<?> getModelClass() {
        return Position.class;
    }

    public String getModelClassName() {
        return Position.class.getName();
    }

    public long getPrimaryKey() {
        return _positionId;
    }

    public void setPrimaryKey(long primaryKey) {
        setPositionId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_positionId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("positionId", getPositionId());
        attributes.put("fiscalYear", getFiscalYear());
        attributes.put("description", getDescription());
        attributes.put("categoryCode", getCategoryCode());
        attributes.put("offerNumber", getOfferNumber());
        attributes.put("offerDate", getOfferDate());
        attributes.put("quatity", getQuatity());
        attributes.put("deliveryDate", getDeliveryDate());
        attributes.put("deliveryAddress", getDeliveryAddress());
        attributes.put("actualUnitCost", getActualUnitCost());
        attributes.put("numberOfUnit", getNumberOfUnit());
        attributes.put("labelNumber", getLabelNumber());
        attributes.put("poLabelNumber", getPoLabelNumber());
        attributes.put("targaTecnica", getTargaTecnica());
        attributes.put("approved", getApproved());
        attributes.put("icApproval", getIcApproval());
        attributes.put("approver", getApprover());
        attributes.put("assetNumber", getAssetNumber());
        attributes.put("shoppingCart", getShoppingCart());
        attributes.put("poNumber", getPoNumber());
        attributes.put("evoLocation", getEvoLocation());
        attributes.put("wbsCodeId", getWbsCodeId());
        attributes.put("purchaseRequestId", getPurchaseRequestId());
        attributes.put("purchaseOrderId", getPurchaseOrderId());
        attributes.put("createdDate", getCreatedDate());
        attributes.put("createdUserId", getCreatedUserId());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long positionId = (Long) attributes.get("positionId");

        if (positionId != null) {
            setPositionId(positionId);
        }

        String fiscalYear = (String) attributes.get("fiscalYear");

        if (fiscalYear != null) {
            setFiscalYear(fiscalYear);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        String categoryCode = (String) attributes.get("categoryCode");

        if (categoryCode != null) {
            setCategoryCode(categoryCode);
        }

        String offerNumber = (String) attributes.get("offerNumber");

        if (offerNumber != null) {
            setOfferNumber(offerNumber);
        }

        Date offerDate = (Date) attributes.get("offerDate");

        if (offerDate != null) {
            setOfferDate(offerDate);
        }

        String quatity = (String) attributes.get("quatity");

        if (quatity != null) {
            setQuatity(quatity);
        }

        Date deliveryDate = (Date) attributes.get("deliveryDate");

        if (deliveryDate != null) {
            setDeliveryDate(deliveryDate);
        }

        String deliveryAddress = (String) attributes.get("deliveryAddress");

        if (deliveryAddress != null) {
            setDeliveryAddress(deliveryAddress);
        }

        String actualUnitCost = (String) attributes.get("actualUnitCost");

        if (actualUnitCost != null) {
            setActualUnitCost(actualUnitCost);
        }

        String numberOfUnit = (String) attributes.get("numberOfUnit");

        if (numberOfUnit != null) {
            setNumberOfUnit(numberOfUnit);
        }

        Long labelNumber = (Long) attributes.get("labelNumber");

        if (labelNumber != null) {
            setLabelNumber(labelNumber);
        }

        Long poLabelNumber = (Long) attributes.get("poLabelNumber");

        if (poLabelNumber != null) {
            setPoLabelNumber(poLabelNumber);
        }

        String targaTecnica = (String) attributes.get("targaTecnica");

        if (targaTecnica != null) {
            setTargaTecnica(targaTecnica);
        }

        Boolean approved = (Boolean) attributes.get("approved");

        if (approved != null) {
            setApproved(approved);
        }

        String icApproval = (String) attributes.get("icApproval");

        if (icApproval != null) {
            setIcApproval(icApproval);
        }

        String approver = (String) attributes.get("approver");

        if (approver != null) {
            setApprover(approver);
        }

        String assetNumber = (String) attributes.get("assetNumber");

        if (assetNumber != null) {
            setAssetNumber(assetNumber);
        }

        String shoppingCart = (String) attributes.get("shoppingCart");

        if (shoppingCart != null) {
            setShoppingCart(shoppingCart);
        }

        String poNumber = (String) attributes.get("poNumber");

        if (poNumber != null) {
            setPoNumber(poNumber);
        }

        String evoLocation = (String) attributes.get("evoLocation");

        if (evoLocation != null) {
            setEvoLocation(evoLocation);
        }

        String wbsCodeId = (String) attributes.get("wbsCodeId");

        if (wbsCodeId != null) {
            setWbsCodeId(wbsCodeId);
        }

        String purchaseRequestId = (String) attributes.get("purchaseRequestId");

        if (purchaseRequestId != null) {
            setPurchaseRequestId(purchaseRequestId);
        }

        String purchaseOrderId = (String) attributes.get("purchaseOrderId");

        if (purchaseOrderId != null) {
            setPurchaseOrderId(purchaseOrderId);
        }

        Date createdDate = (Date) attributes.get("createdDate");

        if (createdDate != null) {
            setCreatedDate(createdDate);
        }

        Long createdUserId = (Long) attributes.get("createdUserId");

        if (createdUserId != null) {
            setCreatedUserId(createdUserId);
        }
    }

    public long getPositionId() {
        return _positionId;
    }

    public void setPositionId(long positionId) {
        _positionId = positionId;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setPositionId", long.class);

                method.invoke(_positionRemoteModel, positionId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getFiscalYear() {
        return _fiscalYear;
    }

    public void setFiscalYear(String fiscalYear) {
        _fiscalYear = fiscalYear;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setFiscalYear", String.class);

                method.invoke(_positionRemoteModel, fiscalYear);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setDescription", String.class);

                method.invoke(_positionRemoteModel, description);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getCategoryCode() {
        return _categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        _categoryCode = categoryCode;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setCategoryCode", String.class);

                method.invoke(_positionRemoteModel, categoryCode);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getOfferNumber() {
        return _offerNumber;
    }

    public void setOfferNumber(String offerNumber) {
        _offerNumber = offerNumber;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setOfferNumber", String.class);

                method.invoke(_positionRemoteModel, offerNumber);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public Date getOfferDate() {
        return _offerDate;
    }

    public void setOfferDate(Date offerDate) {
        _offerDate = offerDate;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setOfferDate", Date.class);

                method.invoke(_positionRemoteModel, offerDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getQuatity() {
        return _quatity;
    }

    public void setQuatity(String quatity) {
        _quatity = quatity;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setQuatity", String.class);

                method.invoke(_positionRemoteModel, quatity);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public Date getDeliveryDate() {
        return _deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        _deliveryDate = deliveryDate;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setDeliveryDate", Date.class);

                method.invoke(_positionRemoteModel, deliveryDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getDeliveryAddress() {
        return _deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        _deliveryAddress = deliveryAddress;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setDeliveryAddress",
                        String.class);

                method.invoke(_positionRemoteModel, deliveryAddress);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getActualUnitCost() {
        return _actualUnitCost;
    }

    public void setActualUnitCost(String actualUnitCost) {
        _actualUnitCost = actualUnitCost;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setActualUnitCost",
                        String.class);

                method.invoke(_positionRemoteModel, actualUnitCost);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getNumberOfUnit() {
        return _numberOfUnit;
    }

    public void setNumberOfUnit(String numberOfUnit) {
        _numberOfUnit = numberOfUnit;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setNumberOfUnit", String.class);

                method.invoke(_positionRemoteModel, numberOfUnit);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public long getLabelNumber() {
        return _labelNumber;
    }

    public void setLabelNumber(long labelNumber) {
        _labelNumber = labelNumber;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setLabelNumber", long.class);

                method.invoke(_positionRemoteModel, labelNumber);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public long getPoLabelNumber() {
        return _poLabelNumber;
    }

    public void setPoLabelNumber(long poLabelNumber) {
        _poLabelNumber = poLabelNumber;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setPoLabelNumber", long.class);

                method.invoke(_positionRemoteModel, poLabelNumber);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getTargaTecnica() {
        return _targaTecnica;
    }

    public void setTargaTecnica(String targaTecnica) {
        _targaTecnica = targaTecnica;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setTargaTecnica", String.class);

                method.invoke(_positionRemoteModel, targaTecnica);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public boolean getApproved() {
        return _approved;
    }

    public boolean isApproved() {
        return _approved;
    }

    public void setApproved(boolean approved) {
        _approved = approved;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setApproved", boolean.class);

                method.invoke(_positionRemoteModel, approved);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getIcApproval() {
        return _icApproval;
    }

    public void setIcApproval(String icApproval) {
        _icApproval = icApproval;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setIcApproval", String.class);

                method.invoke(_positionRemoteModel, icApproval);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getApprover() {
        return _approver;
    }

    public void setApprover(String approver) {
        _approver = approver;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setApprover", String.class);

                method.invoke(_positionRemoteModel, approver);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getAssetNumber() {
        return _assetNumber;
    }

    public void setAssetNumber(String assetNumber) {
        _assetNumber = assetNumber;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setAssetNumber", String.class);

                method.invoke(_positionRemoteModel, assetNumber);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getShoppingCart() {
        return _shoppingCart;
    }

    public void setShoppingCart(String shoppingCart) {
        _shoppingCart = shoppingCart;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setShoppingCart", String.class);

                method.invoke(_positionRemoteModel, shoppingCart);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getPoNumber() {
        return _poNumber;
    }

    public void setPoNumber(String poNumber) {
        _poNumber = poNumber;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setPoNumber", String.class);

                method.invoke(_positionRemoteModel, poNumber);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getEvoLocation() {
        return _evoLocation;
    }

    public void setEvoLocation(String evoLocation) {
        _evoLocation = evoLocation;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setEvoLocation", String.class);

                method.invoke(_positionRemoteModel, evoLocation);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getWbsCodeId() {
        return _wbsCodeId;
    }

    public void setWbsCodeId(String wbsCodeId) {
        _wbsCodeId = wbsCodeId;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setWbsCodeId", String.class);

                method.invoke(_positionRemoteModel, wbsCodeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getPurchaseRequestId() {
        return _purchaseRequestId;
    }

    public void setPurchaseRequestId(String purchaseRequestId) {
        _purchaseRequestId = purchaseRequestId;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setPurchaseRequestId",
                        String.class);

                method.invoke(_positionRemoteModel, purchaseRequestId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getPurchaseOrderId() {
        return _purchaseOrderId;
    }

    public void setPurchaseOrderId(String purchaseOrderId) {
        _purchaseOrderId = purchaseOrderId;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setPurchaseOrderId",
                        String.class);

                method.invoke(_positionRemoteModel, purchaseOrderId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public Date getCreatedDate() {
        return _createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        _createdDate = createdDate;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setCreatedDate", Date.class);

                method.invoke(_positionRemoteModel, createdDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public long getCreatedUserId() {
        return _createdUserId;
    }

    public void setCreatedUserId(long createdUserId) {
        _createdUserId = createdUserId;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setCreatedUserId", long.class);

                method.invoke(_positionRemoteModel, createdUserId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getCreatedUserUuid() throws SystemException {
        return PortalUtil.getUserValue(getCreatedUserId(), "uuid",
            _createdUserUuid);
    }

    public void setCreatedUserUuid(String createdUserUuid) {
        _createdUserUuid = createdUserUuid;
    }

    public java.lang.String getApproverName() {
        try {
            String methodName = "getApproverName";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public void setApproveBtn(boolean approveBtn) {
        try {
            String methodName = "setApproveBtn";

            Class<?>[] parameterTypes = new Class<?>[] { boolean.class };

            Object[] parameterValues = new Object[] { approveBtn };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public void setApprovalStatus(java.lang.String approvalStatus) {
        try {
            String methodName = "setApprovalStatus";

            Class<?>[] parameterTypes = new Class<?>[] { java.lang.String.class };

            Object[] parameterValues = new Object[] { approvalStatus };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getCheckboxStatus() {
        try {
            String methodName = "getCheckboxStatus";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public void setEditGrBtn(boolean editGrBtn) {
        try {
            String methodName = "setEditGrBtn";

            Class<?>[] parameterTypes = new Class<?>[] { boolean.class };

            Object[] parameterValues = new Object[] { editGrBtn };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.Double getSumPositionGRPercentage() {
        try {
            String methodName = "getSumPositionGRPercentage";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.Double returnObj = (java.lang.Double) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getOfferDateText() {
        try {
            String methodName = "getOfferDateText";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public com.hp.omp.model.custom.PositionStatus getPositionStatus() {
        try {
            String methodName = "getPositionStatus";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            com.hp.omp.model.custom.PositionStatus returnObj = (com.hp.omp.model.custom.PositionStatus) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getApprovalStatus() {
        try {
            String methodName = "getApprovalStatus";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getCommaFormattedUnitCost() {
        try {
            String methodName = "getCommaFormattedUnitCost";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public boolean isAddGrBtn() {
        try {
            String methodName = "isAddGrBtn";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            Boolean returnObj = (Boolean) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getFormattedUnitCost() {
        try {
            String methodName = "getFormattedUnitCost";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public void setFormattedUnitCost(java.lang.String formatedUnitCost) {
        try {
            String methodName = "setFormattedUnitCost";

            Class<?>[] parameterTypes = new Class<?>[] { java.lang.String.class };

            Object[] parameterValues = new Object[] { formatedUnitCost };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getiCApprovalText() {
        try {
            String methodName = "getiCApprovalText";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public void setiCApprovalText(java.lang.String iCApprovalText) {
        try {
            String methodName = "setiCApprovalText";

            Class<?>[] parameterTypes = new Class<?>[] { java.lang.String.class };

            Object[] parameterValues = new Object[] { iCApprovalText };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public void setAddGrBtn(boolean addGrBtn) {
        try {
            String methodName = "setAddGrBtn";

            Class<?>[] parameterTypes = new Class<?>[] { boolean.class };

            Object[] parameterValues = new Object[] { addGrBtn };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public boolean isApproveBtn() {
        try {
            String methodName = "isApproveBtn";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            Boolean returnObj = (Boolean) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getFieldStatus() {
        try {
            String methodName = "getFieldStatus";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public void setPositionEditBtton(boolean positionEditBtton) {
        try {
            String methodName = "setPositionEditBtton";

            Class<?>[] parameterTypes = new Class<?>[] { boolean.class };

            Object[] parameterValues = new Object[] { positionEditBtton };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public void setCommaFormattedUnitCost(
        java.lang.String commaFormattedUnitCost) {
        try {
            String methodName = "setCommaFormattedUnitCost";

            Class<?>[] parameterTypes = new Class<?>[] { java.lang.String.class };

            Object[] parameterValues = new Object[] { commaFormattedUnitCost };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getDeliveryDateText() {
        try {
            String methodName = "getDeliveryDateText";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public void setPositionStatusLabel(java.lang.String positionStatusLabel) {
        try {
            String methodName = "setPositionStatusLabel";

            Class<?>[] parameterTypes = new Class<?>[] { java.lang.String.class };

            Object[] parameterValues = new Object[] { positionStatusLabel };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.lang.String getPositionStatusLabel() {
        try {
            String methodName = "getPositionStatusLabel";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public java.util.List<com.hp.omp.model.GoodReceipt> getPositionGoodReceipts() {
        try {
            String methodName = "getPositionGoodReceipts";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.util.List<com.hp.omp.model.GoodReceipt> returnObj = (java.util.List<com.hp.omp.model.GoodReceipt>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public void setApproverName(java.lang.String approverName) {
        try {
            String methodName = "setApproverName";

            Class<?>[] parameterTypes = new Class<?>[] { java.lang.String.class };

            Object[] parameterValues = new Object[] { approverName };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public boolean isEditGrBtn() {
        try {
            String methodName = "isEditGrBtn";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            Boolean returnObj = (Boolean) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public boolean getPositionEditBtton() {
        try {
            String methodName = "getPositionEditBtton";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            Boolean returnObj = (Boolean) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }
    
    public String getEvoDesMateriale() {
        return _evoDesMateriale;
    }

    public void setEvoDesMateriale(String evoDesMateriale) {
        _evoDesMateriale = evoDesMateriale;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setEvoDesMateriale",
                        String.class);

                method.invoke(_positionRemoteModel, evoDesMateriale);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }
    
    public String getEvoCodMateriale() {
        return _evoCodMateriale;
    }

    public void setEvoCodMateriale(String evoCodMateriale) {
        _evoCodMateriale = evoCodMateriale;

        if (_positionRemoteModel != null) {
            try {
                Class<?> clazz = _positionRemoteModel.getClass();

                Method method = clazz.getMethod("setEvoCodMateriale",
                        String.class);

                method.invoke(_positionRemoteModel, evoCodMateriale);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getPositionRemoteModel() {
        return _positionRemoteModel;
    }

    public void setPositionRemoteModel(BaseModel<?> positionRemoteModel) {
        _positionRemoteModel = positionRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _positionRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_positionRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            PositionLocalServiceUtil.addPosition(this);
        } else {
            PositionLocalServiceUtil.updatePosition(this);
        }
    }

    @Override
    public Position toEscapedModel() {
        return (Position) ProxyUtil.newProxyInstance(Position.class.getClassLoader(),
            new Class[] { Position.class }, new AutoEscapeBeanHandler(this));
    }

    public Position toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        PositionClp clone = new PositionClp();

        clone.setPositionId(getPositionId());
        clone.setFiscalYear(getFiscalYear());
        clone.setDescription(getDescription());
        clone.setCategoryCode(getCategoryCode());
        clone.setOfferNumber(getOfferNumber());
        clone.setOfferDate(getOfferDate());
        clone.setQuatity(getQuatity());
        clone.setDeliveryDate(getDeliveryDate());
        clone.setDeliveryAddress(getDeliveryAddress());
        clone.setActualUnitCost(getActualUnitCost());
        clone.setNumberOfUnit(getNumberOfUnit());
        clone.setLabelNumber(getLabelNumber());
        clone.setPoLabelNumber(getPoLabelNumber());
        clone.setTargaTecnica(getTargaTecnica());
        clone.setApproved(getApproved());
        clone.setIcApproval(getIcApproval());
        clone.setApprover(getApprover());
        clone.setAssetNumber(getAssetNumber());
        clone.setShoppingCart(getShoppingCart());
        clone.setPoNumber(getPoNumber());
        clone.setEvoLocation(getEvoLocation());
        clone.setWbsCodeId(getWbsCodeId());
        clone.setPurchaseRequestId(getPurchaseRequestId());
        clone.setPurchaseOrderId(getPurchaseOrderId());
        clone.setCreatedDate(getCreatedDate());
        clone.setCreatedUserId(getCreatedUserId());

        return clone;
    }

    public int compareTo(Position position) {
        long primaryKey = position.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PositionClp)) {
            return false;
        }

        PositionClp position = (PositionClp) obj;

        long primaryKey = position.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(53);

        sb.append("{positionId=");
        sb.append(getPositionId());
        sb.append(", fiscalYear=");
        sb.append(getFiscalYear());
        sb.append(", description=");
        sb.append(getDescription());
        sb.append(", categoryCode=");
        sb.append(getCategoryCode());
        sb.append(", offerNumber=");
        sb.append(getOfferNumber());
        sb.append(", offerDate=");
        sb.append(getOfferDate());
        sb.append(", quatity=");
        sb.append(getQuatity());
        sb.append(", deliveryDate=");
        sb.append(getDeliveryDate());
        sb.append(", deliveryAddress=");
        sb.append(getDeliveryAddress());
        sb.append(", actualUnitCost=");
        sb.append(getActualUnitCost());
        sb.append(", numberOfUnit=");
        sb.append(getNumberOfUnit());
        sb.append(", labelNumber=");
        sb.append(getLabelNumber());
        sb.append(", poLabelNumber=");
        sb.append(getPoLabelNumber());
        sb.append(", targaTecnica=");
        sb.append(getTargaTecnica());
        sb.append(", approved=");
        sb.append(getApproved());
        sb.append(", icApproval=");
        sb.append(getIcApproval());
        sb.append(", approver=");
        sb.append(getApprover());
        sb.append(", assetNumber=");
        sb.append(getAssetNumber());
        sb.append(", shoppingCart=");
        sb.append(getShoppingCart());
        sb.append(", poNumber=");
        sb.append(getPoNumber());
        sb.append(", evoLocation=");
        sb.append(getEvoLocation());
        sb.append(", wbsCodeId=");
        sb.append(getWbsCodeId());
        sb.append(", purchaseRequestId=");
        sb.append(getPurchaseRequestId());
        sb.append(", purchaseOrderId=");
        sb.append(getPurchaseOrderId());
        sb.append(", createdDate=");
        sb.append(getCreatedDate());
        sb.append(", createdUserId=");
        sb.append(getCreatedUserId());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(82);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.Position");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>positionId</column-name><column-value><![CDATA[");
        sb.append(getPositionId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>fiscalYear</column-name><column-value><![CDATA[");
        sb.append(getFiscalYear());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>description</column-name><column-value><![CDATA[");
        sb.append(getDescription());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>categoryCode</column-name><column-value><![CDATA[");
        sb.append(getCategoryCode());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>offerNumber</column-name><column-value><![CDATA[");
        sb.append(getOfferNumber());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>offerDate</column-name><column-value><![CDATA[");
        sb.append(getOfferDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>quatity</column-name><column-value><![CDATA[");
        sb.append(getQuatity());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>deliveryDate</column-name><column-value><![CDATA[");
        sb.append(getDeliveryDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>deliveryAddress</column-name><column-value><![CDATA[");
        sb.append(getDeliveryAddress());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>actualUnitCost</column-name><column-value><![CDATA[");
        sb.append(getActualUnitCost());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>numberOfUnit</column-name><column-value><![CDATA[");
        sb.append(getNumberOfUnit());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>labelNumber</column-name><column-value><![CDATA[");
        sb.append(getLabelNumber());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>poLabelNumber</column-name><column-value><![CDATA[");
        sb.append(getPoLabelNumber());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>targaTecnica</column-name><column-value><![CDATA[");
        sb.append(getTargaTecnica());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>approved</column-name><column-value><![CDATA[");
        sb.append(getApproved());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>icApproval</column-name><column-value><![CDATA[");
        sb.append(getIcApproval());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>approver</column-name><column-value><![CDATA[");
        sb.append(getApprover());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>assetNumber</column-name><column-value><![CDATA[");
        sb.append(getAssetNumber());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>shoppingCart</column-name><column-value><![CDATA[");
        sb.append(getShoppingCart());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>poNumber</column-name><column-value><![CDATA[");
        sb.append(getPoNumber());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>evoLocation</column-name><column-value><![CDATA[");
        sb.append(getEvoLocation());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>wbsCodeId</column-name><column-value><![CDATA[");
        sb.append(getWbsCodeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>purchaseRequestId</column-name><column-value><![CDATA[");
        sb.append(getPurchaseRequestId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>purchaseOrderId</column-name><column-value><![CDATA[");
        sb.append(getPurchaseOrderId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>createdDate</column-name><column-value><![CDATA[");
        sb.append(getCreatedDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>createdUserId</column-name><column-value><![CDATA[");
        sb.append(getCreatedUserId());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
