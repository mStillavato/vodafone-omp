package com.hp.omp.model.custom;

import java.util.HashMap;
import java.util.Map;

public enum PurchaseRequestStatus {
	CREATED(1,"Created"),
	UPDATED(2,"Updated"),
	SUBMITTED(3,"Submitted"),
	CLOSED(5,"GR Closed"),
	DELETED(6,"Deleted"),
	ASSET_RECEIVED(7,"Asset Received"),
	SC_RECEIVED(8,"PR/SC Received"),
	PO_RECEIVED(9,"PO Received"),
	PR_ASSIGNED(10, "PR Assigned"),
	PENDING_IC_APPROVAL(11, "Pending IC approval"),
	REJECTED(12, "Rejected");
	
	private int code;
	private String label;
	
	
	
	private PurchaseRequestStatus(int code, String label){
		this.code = code;
		this.label = label;
	}
	
	
	  private static Map<Integer, PurchaseRequestStatus> codeToStatusMapping;
		 
	 
	    public static PurchaseRequestStatus getStatus(int i) {
	        if (codeToStatusMapping == null) {
	            initMapping();
	        }
	        return codeToStatusMapping.get(i);
	    }
	 
	    private static void initMapping() {
	        codeToStatusMapping = new HashMap<Integer, PurchaseRequestStatus>();
	        for (PurchaseRequestStatus s : values()) {
	            codeToStatusMapping.put(s.code, s);
	        }
	    }
	public int getCode(){
		return code;
	}
	
	public String getLabel(){
		return label;
	}
}
