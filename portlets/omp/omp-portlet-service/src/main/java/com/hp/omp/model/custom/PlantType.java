package com.hp.omp.model.custom;

import java.util.HashMap;
import java.util.Map;

public enum PlantType {
	PLANT_IT3V("IT3V", "IT3V – GNED Groppello"),
	PLANT_IT4V("IT4V", "IT4V – GNED Maddaloni"),
	PLANT_IT5V("IT5V", "IT5V – FAE Groppello"),
	PLANT_IT6V("IT6V", "IT6V – FAE Maddaloni"),
	PLANT_DI2V("DI2V", "DI2V – Dropshipment"),
	PLANT_DISV("DISV", "DISV - Dropshipment Spare");
	
	private String code;
	private String label;
	
	private PlantType(String code, String label){
		this.code = code;
		this.label = label;
	}
	
	
	  private static Map<String, PlantType> codeToStatusMapping;
		 
	    
	 
	    public static PlantType getStatus(int i) {
	        if (codeToStatusMapping == null) {
	            initMapping();
	        }
	        return codeToStatusMapping.get(i);
	    }
	 
	    private static void initMapping() {
	        codeToStatusMapping = new HashMap<String, PlantType>();
	        for (PlantType s : values()) {
	            codeToStatusMapping.put(s.code, s);
	        }
	    }
	public String getCode(){
		return code;
	}
	
	public String getLabel(){
		return label;
	}
}
