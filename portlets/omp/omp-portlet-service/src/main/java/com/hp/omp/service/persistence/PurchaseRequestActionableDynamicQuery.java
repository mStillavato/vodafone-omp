package com.hp.omp.service.persistence;

import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class PurchaseRequestActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public PurchaseRequestActionableDynamicQuery() throws SystemException {
        setBaseLocalService(PurchaseRequestLocalServiceUtil.getService());
        setClass(PurchaseRequest.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("purchaseRequestId");
    }
}
