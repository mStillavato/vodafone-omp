package com.hp.omp.service.persistence;

public interface ODNPNameFinder {
    
    public java.util.List<com.hp.omp.model.ODNPName> getODNPNamesList(
    		int start, int end, String searchFilter)
            throws com.liferay.portal.kernel.exception.SystemException;
}
