package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link SubProject}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       SubProject
 * @generated
 */
public class SubProjectWrapper implements SubProject, ModelWrapper<SubProject> {
    private SubProject _subProject;

    public SubProjectWrapper(SubProject subProject) {
        _subProject = subProject;
    }

    public Class<?> getModelClass() {
        return SubProject.class;
    }

    public String getModelClassName() {
        return SubProject.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("subProjectId", getSubProjectId());
        attributes.put("subProjectName", getSubProjectName());
        attributes.put("description", getDescription());
        attributes.put("display", getDisplay());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long subProjectId = (Long) attributes.get("subProjectId");

        if (subProjectId != null) {
            setSubProjectId(subProjectId);
        }

        String subProjectName = (String) attributes.get("subProjectName");

        if (subProjectName != null) {
            setSubProjectName(subProjectName);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
    }

    /**
    * Returns the primary key of this sub project.
    *
    * @return the primary key of this sub project
    */
    public long getPrimaryKey() {
        return _subProject.getPrimaryKey();
    }

    /**
    * Sets the primary key of this sub project.
    *
    * @param primaryKey the primary key of this sub project
    */
    public void setPrimaryKey(long primaryKey) {
        _subProject.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the sub project ID of this sub project.
    *
    * @return the sub project ID of this sub project
    */
    public long getSubProjectId() {
        return _subProject.getSubProjectId();
    }

    /**
    * Sets the sub project ID of this sub project.
    *
    * @param subProjectId the sub project ID of this sub project
    */
    public void setSubProjectId(long subProjectId) {
        _subProject.setSubProjectId(subProjectId);
    }

    /**
    * Returns the sub project name of this sub project.
    *
    * @return the sub project name of this sub project
    */
    public java.lang.String getSubProjectName() {
        return _subProject.getSubProjectName();
    }

    /**
    * Sets the sub project name of this sub project.
    *
    * @param subProjectName the sub project name of this sub project
    */
    public void setSubProjectName(java.lang.String subProjectName) {
        _subProject.setSubProjectName(subProjectName);
    }

    /**
    * Returns the description of this sub project.
    *
    * @return the description of this sub project
    */
    public java.lang.String getDescription() {
        return _subProject.getDescription();
    }

    /**
    * Sets the description of this sub project.
    *
    * @param description the description of this sub project
    */
    public void setDescription(java.lang.String description) {
        _subProject.setDescription(description);
    }

    /**
    * Returns the display of this sub project.
    *
    * @return the display of this sub project
    */
    public boolean getDisplay() {
        return _subProject.getDisplay();
    }

    /**
    * Returns <code>true</code> if this sub project is display.
    *
    * @return <code>true</code> if this sub project is display; <code>false</code> otherwise
    */
    public boolean isDisplay() {
        return _subProject.isDisplay();
    }

    /**
    * Sets whether this sub project is display.
    *
    * @param display the display of this sub project
    */
    public void setDisplay(boolean display) {
        _subProject.setDisplay(display);
    }

    public boolean isNew() {
        return _subProject.isNew();
    }

    public void setNew(boolean n) {
        _subProject.setNew(n);
    }

    public boolean isCachedModel() {
        return _subProject.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _subProject.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _subProject.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _subProject.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _subProject.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _subProject.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _subProject.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new SubProjectWrapper((SubProject) _subProject.clone());
    }

    public int compareTo(SubProject subProject) {
        return _subProject.compareTo(subProject);
    }

    @Override
    public int hashCode() {
        return _subProject.hashCode();
    }

    public com.liferay.portal.model.CacheModel<SubProject> toCacheModel() {
        return _subProject.toCacheModel();
    }

    public SubProject toEscapedModel() {
        return new SubProjectWrapper(_subProject.toEscapedModel());
    }

    public SubProject toUnescapedModel() {
        return new SubProjectWrapper(_subProject.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _subProject.toString();
    }

    public java.lang.String toXmlString() {
        return _subProject.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _subProject.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof SubProjectWrapper)) {
            return false;
        }

        SubProjectWrapper subProjectWrapper = (SubProjectWrapper) obj;

        if (Validator.equals(_subProject, subProjectWrapper._subProject)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public SubProject getWrappedSubProject() {
        return _subProject;
    }

    public SubProject getWrappedModel() {
        return _subProject;
    }

    public void resetOriginalValues() {
        _subProject.resetOriginalValues();
    }
}
