package com.hp.omp.service;

import java.io.OutputStream;
import java.util.List;

import com.hp.omp.model.Buyer;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link BuyerLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       BuyerLocalService
 * @generated
 */
public class BuyerLocalServiceWrapper implements BuyerLocalService,
    ServiceWrapper<BuyerLocalService> {
    private BuyerLocalService _buyerLocalService;

    public BuyerLocalServiceWrapper(BuyerLocalService buyerLocalService) {
        _buyerLocalService = buyerLocalService;
    }

    /**
    * Adds the buyer to the database. Also notifies the appropriate model listeners.
    *
    * @param buyer the buyer
    * @return the buyer that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Buyer addBuyer(com.hp.omp.model.Buyer buyer)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _buyerLocalService.addBuyer(buyer);
    }

    /**
    * Creates a new buyer with the primary key. Does not add the buyer to the database.
    *
    * @param buyerId the primary key for the new buyer
    * @return the new buyer
    */
    public com.hp.omp.model.Buyer createBuyer(long buyerId) {
        return _buyerLocalService.createBuyer(buyerId);
    }

    /**
    * Deletes the buyer with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param buyerId the primary key of the buyer
    * @return the buyer that was removed
    * @throws PortalException if a buyer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Buyer deleteBuyer(long buyerId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _buyerLocalService.deleteBuyer(buyerId);
    }

    /**
    * Deletes the buyer from the database. Also notifies the appropriate model listeners.
    *
    * @param buyer the buyer
    * @return the buyer that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Buyer deleteBuyer(com.hp.omp.model.Buyer buyer)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _buyerLocalService.deleteBuyer(buyer);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _buyerLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _buyerLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _buyerLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _buyerLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _buyerLocalService.dynamicQueryCount(dynamicQuery);
    }

    public com.hp.omp.model.Buyer fetchBuyer(long buyerId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _buyerLocalService.fetchBuyer(buyerId);
    }

    /**
    * Returns the buyer with the primary key.
    *
    * @param buyerId the primary key of the buyer
    * @return the buyer
    * @throws PortalException if a buyer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Buyer getBuyer(long buyerId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _buyerLocalService.getBuyer(buyerId);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _buyerLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the buyers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of buyers
    * @param end the upper bound of the range of buyers (not inclusive)
    * @return the range of buyers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.Buyer> getBuyers(int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _buyerLocalService.getBuyers(start, end);
    }

    /**
    * Returns the number of buyers.
    *
    * @return the number of buyers
    * @throws SystemException if a system exception occurred
    */
    public int getBuyersCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _buyerLocalService.getBuyersCount();
    }

    /**
    * Updates the buyer in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param buyer the buyer
    * @return the buyer that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Buyer updateBuyer(com.hp.omp.model.Buyer buyer)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _buyerLocalService.updateBuyer(buyer);
    }

    /**
    * Updates the buyer in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param buyer the buyer
    * @param merge whether to merge the buyer with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the buyer that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Buyer updateBuyer(com.hp.omp.model.Buyer buyer,
        boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _buyerLocalService.updateBuyer(buyer, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _buyerLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _buyerLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _buyerLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    public java.util.List<com.hp.omp.model.Buyer> getBuyersList(
        java.lang.Integer pageNumber, java.lang.Integer pageSize)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _buyerLocalService.getBuyersList(pageNumber, pageSize);
    }

    public com.hp.omp.model.Buyer getBuyerByName(java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _buyerLocalService.getBuyerByName(name);
    }

    public java.util.List<com.hp.omp.model.Buyer> getDisplayedBuyers()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _buyerLocalService.getDisplayedBuyers();
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public BuyerLocalService getWrappedBuyerLocalService() {
        return _buyerLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedBuyerLocalService(BuyerLocalService buyerLocalService) {
        _buyerLocalService = buyerLocalService;
    }

    public BuyerLocalService getWrappedService() {
        return _buyerLocalService;
    }

    public void setWrappedService(BuyerLocalService buyerLocalService) {
        _buyerLocalService = buyerLocalService;
    }

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Buyer> getBuyersList(Integer pageNumber, Integer pageSize,
			String searchFilter, int gruppoUsers) throws SystemException {
		return _buyerLocalService.getBuyersList(pageNumber, pageSize, searchFilter, gruppoUsers);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getBuyersCountList(String searchFilter, int gruppoUsers) throws SystemException {
		return _buyerLocalService.getBuyersCountList(searchFilter, gruppoUsers);
	}

	public void exportBuyersAsExcel(List<Buyer> listBuyers,
			List<Object> header, OutputStream out) throws SystemException {
		_buyerLocalService.exportBuyersAsExcel(listBuyers, header, out);
	}
}
