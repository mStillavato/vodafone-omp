package com.hp.omp.service.persistence;

import com.hp.omp.model.PositionAudit;
import com.hp.omp.service.PositionAuditLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class PositionAuditActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public PositionAuditActionableDynamicQuery() throws SystemException {
        setBaseLocalService(PositionAuditLocalServiceUtil.getService());
        setClass(PositionAudit.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("auditId");
    }
}
