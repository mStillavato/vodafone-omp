package com.hp.omp.model;

import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.MicroDriverLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class MicroDriverClp extends BaseModelImpl<MicroDriver>
    implements MicroDriver {
    private long _microDriverId;
    private String _microDriverName;
    private boolean _display;
    private BaseModel<?> _microDriverRemoteModel;

    public MicroDriverClp() {
    }

    public Class<?> getModelClass() {
        return MicroDriver.class;
    }

    public String getModelClassName() {
        return MicroDriver.class.getName();
    }

    public long getPrimaryKey() {
        return _microDriverId;
    }

    public void setPrimaryKey(long primaryKey) {
        setMicroDriverId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_microDriverId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("microDriverId", getMicroDriverId());
        attributes.put("microDriverName", getMicroDriverName());
        attributes.put("display", getDisplay());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long microDriverId = (Long) attributes.get("microDriverId");

        if (microDriverId != null) {
            setMicroDriverId(microDriverId);
        }

        String microDriverName = (String) attributes.get("microDriverName");

        if (microDriverName != null) {
            setMicroDriverName(microDriverName);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
    }

    public long getMicroDriverId() {
        return _microDriverId;
    }

    public void setMicroDriverId(long microDriverId) {
        _microDriverId = microDriverId;

        if (_microDriverRemoteModel != null) {
            try {
                Class<?> clazz = _microDriverRemoteModel.getClass();

                Method method = clazz.getMethod("setMicroDriverId", long.class);

                method.invoke(_microDriverRemoteModel, microDriverId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getMicroDriverName() {
        return _microDriverName;
    }

    public void setMicroDriverName(String microDriverName) {
        _microDriverName = microDriverName;

        if (_microDriverRemoteModel != null) {
            try {
                Class<?> clazz = _microDriverRemoteModel.getClass();

                Method method = clazz.getMethod("setMicroDriverName",
                        String.class);

                method.invoke(_microDriverRemoteModel, microDriverName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;

        if (_microDriverRemoteModel != null) {
            try {
                Class<?> clazz = _microDriverRemoteModel.getClass();

                Method method = clazz.getMethod("setDisplay", boolean.class);

                method.invoke(_microDriverRemoteModel, display);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getMicroDriverRemoteModel() {
        return _microDriverRemoteModel;
    }

    public void setMicroDriverRemoteModel(BaseModel<?> microDriverRemoteModel) {
        _microDriverRemoteModel = microDriverRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _microDriverRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_microDriverRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            MicroDriverLocalServiceUtil.addMicroDriver(this);
        } else {
            MicroDriverLocalServiceUtil.updateMicroDriver(this);
        }
    }

    @Override
    public MicroDriver toEscapedModel() {
        return (MicroDriver) ProxyUtil.newProxyInstance(MicroDriver.class.getClassLoader(),
            new Class[] { MicroDriver.class }, new AutoEscapeBeanHandler(this));
    }

    public MicroDriver toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        MicroDriverClp clone = new MicroDriverClp();

        clone.setMicroDriverId(getMicroDriverId());
        clone.setMicroDriverName(getMicroDriverName());
        clone.setDisplay(getDisplay());

        return clone;
    }

    public int compareTo(MicroDriver microDriver) {
        long primaryKey = microDriver.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof MicroDriverClp)) {
            return false;
        }

        MicroDriverClp microDriver = (MicroDriverClp) obj;

        long primaryKey = microDriver.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{microDriverId=");
        sb.append(getMicroDriverId());
        sb.append(", microDriverName=");
        sb.append(getMicroDriverName());
        sb.append(", display=");
        sb.append(getDisplay());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(13);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.MicroDriver");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>microDriverId</column-name><column-value><![CDATA[");
        sb.append(getMicroDriverId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>microDriverName</column-name><column-value><![CDATA[");
        sb.append(getMicroDriverName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>display</column-name><column-value><![CDATA[");
        sb.append(getDisplay());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
