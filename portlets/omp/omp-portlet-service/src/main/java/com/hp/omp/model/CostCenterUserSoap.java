package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class CostCenterUserSoap implements Serializable {
    private long _costCenterUserId;
    private long _costCenterId;
    private long _userId;

    public CostCenterUserSoap() {
    }

    public static CostCenterUserSoap toSoapModel(CostCenterUser model) {
        CostCenterUserSoap soapModel = new CostCenterUserSoap();

        soapModel.setCostCenterUserId(model.getCostCenterUserId());
        soapModel.setCostCenterId(model.getCostCenterId());
        soapModel.setUserId(model.getUserId());

        return soapModel;
    }

    public static CostCenterUserSoap[] toSoapModels(CostCenterUser[] models) {
        CostCenterUserSoap[] soapModels = new CostCenterUserSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static CostCenterUserSoap[][] toSoapModels(CostCenterUser[][] models) {
        CostCenterUserSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new CostCenterUserSoap[models.length][models[0].length];
        } else {
            soapModels = new CostCenterUserSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static CostCenterUserSoap[] toSoapModels(List<CostCenterUser> models) {
        List<CostCenterUserSoap> soapModels = new ArrayList<CostCenterUserSoap>(models.size());

        for (CostCenterUser model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new CostCenterUserSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _costCenterUserId;
    }

    public void setPrimaryKey(long pk) {
        setCostCenterUserId(pk);
    }

    public long getCostCenterUserId() {
        return _costCenterUserId;
    }

    public void setCostCenterUserId(long costCenterUserId) {
        _costCenterUserId = costCenterUserId;
    }

    public long getCostCenterId() {
        return _costCenterId;
    }

    public void setCostCenterId(long costCenterId) {
        _costCenterId = costCenterId;
    }

    public long getUserId() {
        return _userId;
    }

    public void setUserId(long userId) {
        _userId = userId;
    }
}
