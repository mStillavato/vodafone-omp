package com.hp.omp.model;

import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.WbsCodeLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class WbsCodeClp extends BaseModelImpl<WbsCode> implements WbsCode {
    private long _wbsCodeId;
    private String _wbsCodeName;
    private String _description;
    private boolean _display;
    private String _wbsOwnerName;
    private BaseModel<?> _wbsCodeRemoteModel;

    public WbsCodeClp() {
    }

    public Class<?> getModelClass() {
        return WbsCode.class;
    }

    public String getModelClassName() {
        return WbsCode.class.getName();
    }

    public long getPrimaryKey() {
        return _wbsCodeId;
    }

    public void setPrimaryKey(long primaryKey) {
        setWbsCodeId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_wbsCodeId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("wbsCodeId", getWbsCodeId());
        attributes.put("wbsCodeName", getWbsCodeName());
        attributes.put("description", getDescription());
        attributes.put("display", getDisplay());
        attributes.put("wbsOwnerName", getWbsOwnerName());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long wbsCodeId = (Long) attributes.get("wbsCodeId");

        if (wbsCodeId != null) {
            setWbsCodeId(wbsCodeId);
        }

        String wbsCodeName = (String) attributes.get("wbsCodeName");

        if (wbsCodeName != null) {
            setWbsCodeName(wbsCodeName);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
        
        String wbsOwnerName = (String) attributes.get("wbsOwnerName");

        if (wbsOwnerName != null) {
            setWbsOwnerName(wbsOwnerName);
        }
    }

    public long getWbsCodeId() {
        return _wbsCodeId;
    }

    public void setWbsCodeId(long wbsCodeId) {
        _wbsCodeId = wbsCodeId;

        if (_wbsCodeRemoteModel != null) {
            try {
                Class<?> clazz = _wbsCodeRemoteModel.getClass();

                Method method = clazz.getMethod("setWbsCodeId", long.class);

                method.invoke(_wbsCodeRemoteModel, wbsCodeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getWbsCodeName() {
        return _wbsCodeName;
    }

    public void setWbsCodeName(String wbsCodeName) {
        _wbsCodeName = wbsCodeName;

        if (_wbsCodeRemoteModel != null) {
            try {
                Class<?> clazz = _wbsCodeRemoteModel.getClass();

                Method method = clazz.getMethod("setWbsCodeName", String.class);

                method.invoke(_wbsCodeRemoteModel, wbsCodeName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;

        if (_wbsCodeRemoteModel != null) {
            try {
                Class<?> clazz = _wbsCodeRemoteModel.getClass();

                Method method = clazz.getMethod("setDescription", String.class);

                method.invoke(_wbsCodeRemoteModel, description);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;

        if (_wbsCodeRemoteModel != null) {
            try {
                Class<?> clazz = _wbsCodeRemoteModel.getClass();

                Method method = clazz.getMethod("setDisplay", boolean.class);

                method.invoke(_wbsCodeRemoteModel, display);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }
    
    public String getWbsOwnerName() {
        return _wbsOwnerName;
    }

    public void setWbsOwnerName(String wbsOwnerName) {
        _wbsOwnerName = wbsOwnerName;

        if (_wbsCodeRemoteModel != null) {
            try {
                Class<?> clazz = _wbsCodeRemoteModel.getClass();

                Method method = clazz.getMethod("setWbsOwnerName", String.class);

                method.invoke(_wbsCodeRemoteModel, wbsOwnerName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getWbsCodeRemoteModel() {
        return _wbsCodeRemoteModel;
    }

    public void setWbsCodeRemoteModel(BaseModel<?> wbsCodeRemoteModel) {
        _wbsCodeRemoteModel = wbsCodeRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _wbsCodeRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_wbsCodeRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            WbsCodeLocalServiceUtil.addWbsCode(this);
        } else {
            WbsCodeLocalServiceUtil.updateWbsCode(this);
        }
    }

    @Override
    public WbsCode toEscapedModel() {
        return (WbsCode) ProxyUtil.newProxyInstance(WbsCode.class.getClassLoader(),
            new Class[] { WbsCode.class }, new AutoEscapeBeanHandler(this));
    }

    public WbsCode toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        WbsCodeClp clone = new WbsCodeClp();

        clone.setWbsCodeId(getWbsCodeId());
        clone.setWbsCodeName(getWbsCodeName());
        clone.setDescription(getDescription());
        clone.setDisplay(getDisplay());
        clone.setWbsOwnerName(getWbsOwnerName());

        return clone;
    }

    public int compareTo(WbsCode wbsCode) {
        long primaryKey = wbsCode.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof WbsCodeClp)) {
            return false;
        }

        WbsCodeClp wbsCode = (WbsCodeClp) obj;

        long primaryKey = wbsCode.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(9);

        sb.append("{wbsCodeId=");
        sb.append(getWbsCodeId());
        sb.append(", wbsCodeName=");
        sb.append(getWbsCodeName());
        sb.append(", description=");
        sb.append(getDescription());
        sb.append(", display=");
        sb.append(getDisplay());
        sb.append(", wbsOwnerName=");
        sb.append(getWbsOwnerName());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(16);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.WbsCode");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>wbsCodeId</column-name><column-value><![CDATA[");
        sb.append(getWbsCodeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>wbsCodeName</column-name><column-value><![CDATA[");
        sb.append(getWbsCodeName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>description</column-name><column-value><![CDATA[");
        sb.append(getDescription());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>display</column-name><column-value><![CDATA[");
        sb.append(getDisplay());
        sb.append("]]></column-value></column>");
        sb.append(
                "<column><column-name>wbsOwnerName</column-name><column-value><![CDATA[");
            sb.append(getWbsOwnerName());
            sb.append("]]></column-value></column>");
            
        sb.append("</model>");

        return sb.toString();
    }
}
