package com.hp.omp.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the tracking codes local service. This utility wraps {@link com.hp.omp.service.impl.TrackingCodesLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see TrackingCodesLocalService
 * @see com.hp.omp.service.base.TrackingCodesLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.TrackingCodesLocalServiceImpl
 * @generated
 */
public class TrackingCodesLocalServiceUtil {
	private static TrackingCodesLocalService _service;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.hp.omp.service.impl.TrackingCodesLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the tracking codes to the database. Also notifies the appropriate model listeners.
	 *
	 * @param trackingCodes the tracking codes
	 * @return the tracking codes that was added
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.TrackingCodes addTrackingCodes(
			com.hp.omp.model.TrackingCodes trackingCodes)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addTrackingCodes(trackingCodes);
	}

	/**
	 * Creates a new tracking codes with the primary key. Does not add the tracking codes to the database.
	 *
	 * @param trackingCodeId the primary key for the new tracking codes
	 * @return the new tracking codes
	 */
	public static com.hp.omp.model.TrackingCodes createTrackingCodes(
			long trackingCodeId) {
		return getService().createTrackingCodes(trackingCodeId);
	}

	/**
	 * Deletes the tracking codes with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param trackingCodeId the primary key of the tracking codes
	 * @return the tracking codes that was removed
	 * @throws PortalException if a tracking codes with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.TrackingCodes deleteTrackingCodes(
			long trackingCodeId)
					throws com.liferay.portal.kernel.exception.PortalException,
					com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteTrackingCodes(trackingCodeId);
	}

	/**
	 * Deletes the tracking codes from the database. Also notifies the appropriate model listeners.
	 *
	 * @param trackingCodes the tracking codes
	 * @return the tracking codes that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.TrackingCodes deleteTrackingCodes(
			com.hp.omp.model.TrackingCodes trackingCodes)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteTrackingCodes(trackingCodes);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
			int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
			int end,
			com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				.dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows that match the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows that match the dynamic query
	 * @throws SystemException if a system exception occurred
	 */
	public static long dynamicQueryCount(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static com.hp.omp.model.TrackingCodes fetchTrackingCodes(
			long trackingCodeId)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchTrackingCodes(trackingCodeId);
	}

	/**
	 * Returns the tracking codes with the primary key.
	 *
	 * @param trackingCodeId the primary key of the tracking codes
	 * @return the tracking codes
	 * @throws PortalException if a tracking codes with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.TrackingCodes getTrackingCodes(
			long trackingCodeId)
					throws com.liferay.portal.kernel.exception.PortalException,
					com.liferay.portal.kernel.exception.SystemException {
		return getService().getTrackingCodes(trackingCodeId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
					throws com.liferay.portal.kernel.exception.PortalException,
					com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns a range of all the tracking codeses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of tracking codeses
	 * @param end the upper bound of the range of tracking codeses (not inclusive)
	 * @return the range of tracking codeses
	 * @throws SystemException if a system exception occurred
	 */
	public static java.util.List<com.hp.omp.model.TrackingCodes> getTrackingCodeses(
			int start, int end)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getTrackingCodeses(start, end);
	}

	/**
	 * Returns the number of tracking codeses.
	 *
	 * @return the number of tracking codeses
	 * @throws SystemException if a system exception occurred
	 */
	public static int getTrackingCodesesCount()
			throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getTrackingCodesesCount();
	}

	/**
	 * Updates the tracking codes in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param trackingCodes the tracking codes
	 * @return the tracking codes that was updated
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.TrackingCodes updateTrackingCodes(
			com.hp.omp.model.TrackingCodes trackingCodes)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateTrackingCodes(trackingCodes);
	}

	/**
	 * Updates the tracking codes in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param trackingCodes the tracking codes
	 * @param merge whether to merge the tracking codes with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	 * @return the tracking codes that was updated
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.TrackingCodes updateTrackingCodes(
			com.hp.omp.model.TrackingCodes trackingCodes, boolean merge)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateTrackingCodes(trackingCodes, merge);
	}

	/**
	 * Returns the Spring bean ID for this bean.
	 *
	 * @return the Spring bean ID for this bean
	 */
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	 * Sets the Spring bean ID for this bean.
	 *
	 * @param beanIdentifier the Spring bean ID for this bean
	 */
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
			java.lang.String[] parameterTypes, java.lang.Object[] arguments)
					throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static com.hp.omp.model.TrackingCodes getTrackingCodesByFields(
			com.hp.omp.model.TrackingCodes trackingCode)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getTrackingCodesByFields(trackingCode);
	}

	public static com.hp.omp.model.TrackingCodes getTrackingCodesByPRFields(
			com.hp.omp.model.PurchaseRequest pr)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getTrackingCodesByPRFields(pr);
	}

	public static com.hp.omp.model.TrackingCodes getTrackingCodeByName(
			java.lang.String name)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getTrackingCodeByName(name);
	}

	public static java.util.List<com.hp.omp.model.TrackingCodes> getTrackingCodesList(
			java.lang.Integer pageNumber, java.lang.Integer pageSize, String searchTCN)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getTrackingCodesList(pageNumber, pageSize, searchTCN);
	}

	public static int getTrackingCodesCountList(String searchTCN)
			throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getTrackingCodesCountList(searchTCN);
	}
	
	public static void exportTrackingCodesAsExcel(
			java.util.List<com.hp.omp.model.TrackingCodes> listTrackingCodes,
			java.util.List<java.lang.Object> header, java.io.OutputStream out)
					throws com.liferay.portal.kernel.exception.SystemException {
		getService().exportTrackingCodesAsExcel(listTrackingCodes, header, out);
	}

	public static void clearService() {
		_service = null;
	}

	public static TrackingCodesLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					TrackingCodesLocalService.class.getName());

			if (invokableLocalService instanceof TrackingCodesLocalService) {
				_service = (TrackingCodesLocalService) invokableLocalService;
			} else {
				_service = new TrackingCodesLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(TrackingCodesLocalServiceUtil.class,
					"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	 public void setService(TrackingCodesLocalService service) {
	}
}
