package com.hp.omp.model;

import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.ODNPNameLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class ODNPNameClp extends BaseModelImpl<ODNPName> implements ODNPName {
    private long _odnpNameId;
    private String _odnpNameName;
    private boolean _display;
    private BaseModel<?> _odnpNameRemoteModel;

    public ODNPNameClp() {
    }

    public Class<?> getModelClass() {
        return ODNPName.class;
    }

    public String getModelClassName() {
        return ODNPName.class.getName();
    }

    public long getPrimaryKey() {
        return _odnpNameId;
    }

    public void setPrimaryKey(long primaryKey) {
        setOdnpNameId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_odnpNameId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("odnpNameId", getOdnpNameId());
        attributes.put("odnpNameName", getOdnpNameName());
        attributes.put("display", getDisplay());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long odnpNameId = (Long) attributes.get("odnpNameId");

        if (odnpNameId != null) {
            setOdnpNameId(odnpNameId);
        }

        String odnpNameName = (String) attributes.get("odnpNameName");

        if (odnpNameName != null) {
            setOdnpNameName(odnpNameName);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
    }

    public long getOdnpNameId() {
        return _odnpNameId;
    }

    public void setOdnpNameId(long odnpNameId) {
        _odnpNameId = odnpNameId;

        if (_odnpNameRemoteModel != null) {
            try {
                Class<?> clazz = _odnpNameRemoteModel.getClass();

                Method method = clazz.getMethod("setOdnpNameId", long.class);

                method.invoke(_odnpNameRemoteModel, odnpNameId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getOdnpNameName() {
        return _odnpNameName;
    }

    public void setOdnpNameName(String odnpNameName) {
        _odnpNameName = odnpNameName;

        if (_odnpNameRemoteModel != null) {
            try {
                Class<?> clazz = _odnpNameRemoteModel.getClass();

                Method method = clazz.getMethod("setOdnpNameName", String.class);

                method.invoke(_odnpNameRemoteModel, odnpNameName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;

        if (_odnpNameRemoteModel != null) {
            try {
                Class<?> clazz = _odnpNameRemoteModel.getClass();

                Method method = clazz.getMethod("setDisplay", boolean.class);

                method.invoke(_odnpNameRemoteModel, display);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getODNPNameRemoteModel() {
        return _odnpNameRemoteModel;
    }

    public void setODNPNameRemoteModel(BaseModel<?> odnpNameRemoteModel) {
        _odnpNameRemoteModel = odnpNameRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _odnpNameRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_odnpNameRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            ODNPNameLocalServiceUtil.addODNPName(this);
        } else {
            ODNPNameLocalServiceUtil.updateODNPName(this);
        }
    }

    @Override
    public ODNPName toEscapedModel() {
        return (ODNPName) ProxyUtil.newProxyInstance(ODNPName.class.getClassLoader(),
            new Class[] { ODNPName.class }, new AutoEscapeBeanHandler(this));
    }

    public ODNPName toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        ODNPNameClp clone = new ODNPNameClp();

        clone.setOdnpNameId(getOdnpNameId());
        clone.setOdnpNameName(getOdnpNameName());
        clone.setDisplay(getDisplay());

        return clone;
    }

    public int compareTo(ODNPName odnpName) {
        long primaryKey = odnpName.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ODNPNameClp)) {
            return false;
        }

        ODNPNameClp odnpName = (ODNPNameClp) obj;

        long primaryKey = odnpName.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{odnpNameId=");
        sb.append(getOdnpNameId());
        sb.append(", odnpNameName=");
        sb.append(getOdnpNameName());
        sb.append(", display=");
        sb.append(getDisplay());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(13);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.ODNPName");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>odnpNameId</column-name><column-value><![CDATA[");
        sb.append(getOdnpNameId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>odnpNameName</column-name><column-value><![CDATA[");
        sb.append(getOdnpNameName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>display</column-name><column-value><![CDATA[");
        sb.append(getDisplay());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
