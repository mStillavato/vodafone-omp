package com.hp.omp.model.custom;

import java.util.HashMap;
import java.util.Map;

public enum PurchaseOrderStatus {
	
	PO_RECEIVED(5,"PO_RECEIVED"),//this is automatic the PR take it once it is created
	GR_RECEIVED(6,"GR_RECEIVED"),//After all the GR for all the PR's positions completed by Eng which means don't have registration date and the sum of the percentage is 100%. 
	PO_CLOSED(7,"PO_CLOSED"),//After all the GR for all the PR's positions completed by ANTEX which means has registration date and the sum of the percentage is 100%.
	PO_DELETED(8,"PO_DELETED");
	
	private int code;
	private String label;
	
	
	
	private PurchaseOrderStatus(int code, String label){
		this.code = code;
		this.label = label;
	}
	
	
	  private static Map<Integer, PurchaseOrderStatus> codeToStatusMapping;
		 
	    
	 
	    public static PurchaseOrderStatus getStatus(int i) {
	        if (codeToStatusMapping == null) {
	            initMapping();
	        }
	        return codeToStatusMapping.get(i);
	    }
	 
	    private static void initMapping() {
	        codeToStatusMapping = new HashMap<Integer, PurchaseOrderStatus>();
	        for (PurchaseOrderStatus s : values()) {
	            codeToStatusMapping.put(s.code, s);
	        }
	    }
	public int getCode(){
		return code;
	}
	
	public String getLabel(){
		return label;
	}
}
