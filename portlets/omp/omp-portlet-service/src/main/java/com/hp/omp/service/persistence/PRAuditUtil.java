package com.hp.omp.service.persistence;

import com.hp.omp.model.PRAudit;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the p r audit service. This utility wraps {@link PRAuditPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see PRAuditPersistence
 * @see PRAuditPersistenceImpl
 * @generated
 */
public class PRAuditUtil {
    private static PRAuditPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(PRAudit prAudit) {
        getPersistence().clearCache(prAudit);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<PRAudit> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<PRAudit> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<PRAudit> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static PRAudit update(PRAudit prAudit, boolean merge)
        throws SystemException {
        return getPersistence().update(prAudit, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static PRAudit update(PRAudit prAudit, boolean merge,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(prAudit, merge, serviceContext);
    }

    /**
    * Caches the p r audit in the entity cache if it is enabled.
    *
    * @param prAudit the p r audit
    */
    public static void cacheResult(com.hp.omp.model.PRAudit prAudit) {
        getPersistence().cacheResult(prAudit);
    }

    /**
    * Caches the p r audits in the entity cache if it is enabled.
    *
    * @param prAudits the p r audits
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.PRAudit> prAudits) {
        getPersistence().cacheResult(prAudits);
    }

    /**
    * Creates a new p r audit with the primary key. Does not add the p r audit to the database.
    *
    * @param auditId the primary key for the new p r audit
    * @return the new p r audit
    */
    public static com.hp.omp.model.PRAudit create(long auditId) {
        return getPersistence().create(auditId);
    }

    /**
    * Removes the p r audit with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param auditId the primary key of the p r audit
    * @return the p r audit that was removed
    * @throws com.hp.omp.NoSuchPRAuditException if a p r audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.PRAudit remove(long auditId)
        throws com.hp.omp.NoSuchPRAuditException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(auditId);
    }

    public static com.hp.omp.model.PRAudit updateImpl(
        com.hp.omp.model.PRAudit prAudit, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(prAudit, merge);
    }

    /**
    * Returns the p r audit with the primary key or throws a {@link com.hp.omp.NoSuchPRAuditException} if it could not be found.
    *
    * @param auditId the primary key of the p r audit
    * @return the p r audit
    * @throws com.hp.omp.NoSuchPRAuditException if a p r audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.PRAudit findByPrimaryKey(long auditId)
        throws com.hp.omp.NoSuchPRAuditException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(auditId);
    }

    /**
    * Returns the p r audit with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param auditId the primary key of the p r audit
    * @return the p r audit, or <code>null</code> if a p r audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.PRAudit fetchByPrimaryKey(long auditId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(auditId);
    }

    /**
    * Returns all the p r audits.
    *
    * @return the p r audits
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.PRAudit> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the p r audits.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of p r audits
    * @param end the upper bound of the range of p r audits (not inclusive)
    * @return the range of p r audits
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.PRAudit> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the p r audits.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of p r audits
    * @param end the upper bound of the range of p r audits (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of p r audits
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.PRAudit> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the p r audits from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of p r audits.
    *
    * @return the number of p r audits
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static PRAuditPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (PRAuditPersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    PRAuditPersistence.class.getName());

            ReferenceRegistry.registerReference(PRAuditUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(PRAuditPersistence persistence) {
    }
}
