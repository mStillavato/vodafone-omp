package com.hp.omp.service;

import com.liferay.portal.service.InvokableLocalService;


public class PurchaseRequestLocalServiceClp
    implements PurchaseRequestLocalService {
    private InvokableLocalService _invokableLocalService;
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName2;
    private String[] _methodParameterTypes2;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;
    private String _methodName9;
    private String[] _methodParameterTypes9;
    private String _methodName10;
    private String[] _methodParameterTypes10;
    private String _methodName11;
    private String[] _methodParameterTypes11;
    private String _methodName12;
    private String[] _methodParameterTypes12;
    private String _methodName13;
    private String[] _methodParameterTypes13;
    private String _methodName14;
    private String[] _methodParameterTypes14;
    private String _methodName15;
    private String[] _methodParameterTypes15;
    private String _methodName16;
    private String[] _methodParameterTypes16;
    private String _methodName17;
    private String[] _methodParameterTypes17;
    private String _methodName19;
    private String[] _methodParameterTypes19;
    private String _methodName20;
    private String[] _methodParameterTypes20;
    private String _methodName21;
    private String[] _methodParameterTypes21;
    private String _methodName22;
    private String[] _methodParameterTypes22;
    private String _methodName23;
    private String[] _methodParameterTypes23;
    private String _methodName24;
    private String[] _methodParameterTypes24;
    private String _methodName25;
    private String[] _methodParameterTypes25;
    private String _methodName26;
    private String[] _methodParameterTypes26;
    private String _methodName27;
    private String[] _methodParameterTypes27;
    private String _methodName28;
    private String[] _methodParameterTypes28;
    private String _methodName29;
    private String[] _methodParameterTypes29;
    private String _methodName30;
    private String[] _methodParameterTypes30;
    private String _methodName31;
    private String[] _methodParameterTypes31;
    private String _methodName32;
    private String[] _methodParameterTypes32;
    private String _methodName33;
    private String[] _methodParameterTypes33;
    private String _methodName34;
    private String[] _methodParameterTypes34;
    private String _methodName35;
    private String[] _methodParameterTypes35;
    private String _methodName36;
    private String[] _methodParameterTypes36;
    private String _methodName37;
    private String[] _methodParameterTypes37;
    private String _methodName38;
    private String[] _methodParameterTypes38;
    private String _methodName39;
    private String[] _methodParameterTypes39;
    private String _methodName40;
    private String[] _methodParameterTypes40;
    private String _methodName41;
    private String[] _methodParameterTypes41;
    private String _methodName42;
    private String[] _methodParameterTypes42;

    public PurchaseRequestLocalServiceClp(
        InvokableLocalService invokableLocalService) {
        _invokableLocalService = invokableLocalService;

        _methodName0 = "addPurchaseRequest";

        _methodParameterTypes0 = new String[] { "com.hp.omp.model.PurchaseRequest" };

        _methodName1 = "createPurchaseRequest";

        _methodParameterTypes1 = new String[] { "long" };

        _methodName2 = "deletePurchaseRequest";

        _methodParameterTypes2 = new String[] { "long" };

        _methodName3 = "deletePurchaseRequest";

        _methodParameterTypes3 = new String[] { "com.hp.omp.model.PurchaseRequest" };

        _methodName4 = "dynamicQuery";

        _methodParameterTypes4 = new String[] {  };

        _methodName5 = "dynamicQuery";

        _methodParameterTypes5 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName6 = "dynamicQuery";

        _methodParameterTypes6 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
            };

        _methodName7 = "dynamicQuery";

        _methodParameterTypes7 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName8 = "dynamicQueryCount";

        _methodParameterTypes8 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName9 = "fetchPurchaseRequest";

        _methodParameterTypes9 = new String[] { "long" };

        _methodName10 = "getPurchaseRequest";

        _methodParameterTypes10 = new String[] { "long" };

        _methodName11 = "getPersistedModel";

        _methodParameterTypes11 = new String[] { "java.io.Serializable" };

        _methodName12 = "getPurchaseRequests";

        _methodParameterTypes12 = new String[] { "int", "int" };

        _methodName13 = "getPurchaseRequestsCount";

        _methodParameterTypes13 = new String[] {  };

        _methodName14 = "updatePurchaseRequest";

        _methodParameterTypes14 = new String[] {
                "com.hp.omp.model.PurchaseRequest"
            };

        _methodName15 = "updatePurchaseRequest";

        _methodParameterTypes15 = new String[] {
                "com.hp.omp.model.PurchaseRequest", "boolean"
            };

        _methodName16 = "getBeanIdentifier";

        _methodParameterTypes16 = new String[] {  };

        _methodName17 = "setBeanIdentifier";

        _methodParameterTypes17 = new String[] { "java.lang.String" };

        _methodName19 = "getPurchaseRequestByStatus";

        _methodParameterTypes19 = new String[] { "long", "int" };

        _methodName20 = "getPurchaseRequests";

        _methodParameterTypes20 = new String[] { "java.util.List" };

        _methodName21 = "getPurchaseRequesListByStatus";

        _methodParameterTypes21 = new String[] { "int" };

        _methodName22 = "getPurchaseRequesListByStatus";

        _methodParameterTypes22 = new String[] {
                "int", "int", "int", "java.lang.String", "java.lang.String"
            };

        _methodName23 = "getPurchaseRequesListByStatusCount";

        _methodParameterTypes23 = new String[] { "int", "boolean" };

        _methodName24 = "getPurchaseRequesListByStatusAndType";

        _methodParameterTypes24 = new String[] {
                "int", "boolean", "int", "int", "java.lang.String",
                "java.lang.String"
            };

        _methodName25 = "getPurchaseRequesListByPositionStatusAndType";

        _methodParameterTypes25 = new String[] {
                "java.util.Set", "boolean", "int", "int", "java.lang.String",
                "java.lang.String"
            };

        _methodName26 = "getPurchaseRequestListByPositionStatusAndType";

        _methodParameterTypes26 = new String[] {
                "int", "boolean", "int", "int", "java.lang.String",
                "java.lang.String"
            };

        _methodName27 = "getPurchaseRequestListByPositionStatusAndTypeCount";

        _methodParameterTypes27 = new String[] { "int", "boolean" };

        _methodName28 = "getPRListByStatusAndCostCenter";

        _methodParameterTypes28 = new String[] {
                "int", "long", "int", "int", "int", "java.lang.String",
                "java.lang.String"
            };

        _methodName29 = "getPRListByStatusAndCostCenterCount";

        _methodParameterTypes29 = new String[] { "int", "long", "int" };

        _methodName30 = "getPurchaseRequestList";

        _methodParameterTypes30 = new String[] {
                "com.hp.omp.model.custom.SearchDTO", "int", "long"
            };

        _methodName31 = "prSearch";

        _methodParameterTypes31 = new String[] {
                "com.hp.omp.model.custom.SearchDTO", "int", "long"
            };

        _methodName32 = "getPurchaseRequestCount";

        _methodParameterTypes32 = new String[] {
                "com.hp.omp.model.custom.SearchDTO", "int", "long"
            };

        _methodName33 = "getAntexPurchaseRequestList";

        _methodParameterTypes33 = new String[] {
                "com.hp.omp.model.custom.SearchDTO"
            };

        _methodName34 = "getAntexPurchaseRequestCount";

        _methodParameterTypes34 = new String[] {
                "com.hp.omp.model.custom.SearchDTO"
            };

        _methodName35 = "getPurchaseRequestByProjectId";

        _methodParameterTypes35 = new String[] { "java.lang.String" };

        _methodName36 = "getPurchaseRequestBySubProjectId";

        _methodParameterTypes36 = new String[] { "java.lang.String" };

        _methodName37 = "getPurchaseRequestByVendorId";

        _methodParameterTypes37 = new String[] { "java.lang.String" };

        _methodName38 = "getPurchaseRequestByBudgetCategoryId";

        _methodParameterTypes38 = new String[] { "java.lang.String" };

        _methodName39 = "getPurchaseRequestByBudgetSubCategoryId";

        _methodParameterTypes39 = new String[] { "java.lang.String" };

        _methodName40 = "getPurchaseRequestByOdnpNameId";

        _methodParameterTypes40 = new String[] { "java.lang.String" };

        _methodName41 = "getPurchaseRequestByMacroDriverId";

        _methodParameterTypes41 = new String[] { "java.lang.String" };

        _methodName42 = "getPurchaseRequestByMicroDriverId";

        _methodParameterTypes42 = new String[] { "java.lang.String" };
    }

    public com.hp.omp.model.PurchaseRequest addPurchaseRequest(
        com.hp.omp.model.PurchaseRequest purchaseRequest)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName0,
                    _methodParameterTypes0,
                    new Object[] { ClpSerializer.translateInput(purchaseRequest) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.hp.omp.model.PurchaseRequest) ClpSerializer.translateOutput(returnObj);
    }

    public com.hp.omp.model.PurchaseRequest createPurchaseRequest(
        long purchaseRequestId) {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName1,
                    _methodParameterTypes1, new Object[] { purchaseRequestId });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.hp.omp.model.PurchaseRequest) ClpSerializer.translateOutput(returnObj);
    }

    public com.hp.omp.model.PurchaseRequest deletePurchaseRequest(
        long purchaseRequestId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName2,
                    _methodParameterTypes2, new Object[] { purchaseRequestId });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
                throw (com.liferay.portal.kernel.exception.PortalException) t;
            }

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.hp.omp.model.PurchaseRequest) ClpSerializer.translateOutput(returnObj);
    }

    public com.hp.omp.model.PurchaseRequest deletePurchaseRequest(
        com.hp.omp.model.PurchaseRequest purchaseRequest)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName3,
                    _methodParameterTypes3,
                    new Object[] { ClpSerializer.translateInput(purchaseRequest) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.hp.omp.model.PurchaseRequest) ClpSerializer.translateOutput(returnObj);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName4,
                    _methodParameterTypes4, new Object[] {  });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.liferay.portal.kernel.dao.orm.DynamicQuery) ClpSerializer.translateOutput(returnObj);
    }

    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName5,
                    _methodParameterTypes5,
                    new Object[] { ClpSerializer.translateInput(dynamicQuery) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List) ClpSerializer.translateOutput(returnObj);
    }

    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName6,
                    _methodParameterTypes6,
                    new Object[] {
                        ClpSerializer.translateInput(dynamicQuery),
                        
                    start,
                        
                    end
                    });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List) ClpSerializer.translateOutput(returnObj);
    }

    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName7,
                    _methodParameterTypes7,
                    new Object[] {
                        ClpSerializer.translateInput(dynamicQuery),
                        
                    start,
                        
                    end,
                        
                    ClpSerializer.translateInput(orderByComparator)
                    });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List) ClpSerializer.translateOutput(returnObj);
    }

    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName8,
                    _methodParameterTypes8,
                    new Object[] { ClpSerializer.translateInput(dynamicQuery) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return ((Long) returnObj).longValue();
    }

    public com.hp.omp.model.PurchaseRequest fetchPurchaseRequest(
        long purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName9,
                    _methodParameterTypes9, new Object[] { purchaseRequestId });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.hp.omp.model.PurchaseRequest) ClpSerializer.translateOutput(returnObj);
    }

    public com.hp.omp.model.PurchaseRequest getPurchaseRequest(
        long purchaseRequestId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName10,
                    _methodParameterTypes10, new Object[] { purchaseRequestId });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
                throw (com.liferay.portal.kernel.exception.PortalException) t;
            }

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.hp.omp.model.PurchaseRequest) ClpSerializer.translateOutput(returnObj);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName11,
                    _methodParameterTypes11,
                    new Object[] { ClpSerializer.translateInput(primaryKeyObj) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
                throw (com.liferay.portal.kernel.exception.PortalException) t;
            }

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.liferay.portal.model.PersistedModel) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequests(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName12,
                    _methodParameterTypes12, new Object[] { start, end });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.PurchaseRequest>) ClpSerializer.translateOutput(returnObj);
    }

    public int getPurchaseRequestsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName13,
                    _methodParameterTypes13, new Object[] {  });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return ((Integer) returnObj).intValue();
    }

    public com.hp.omp.model.PurchaseRequest updatePurchaseRequest(
        com.hp.omp.model.PurchaseRequest purchaseRequest)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName14,
                    _methodParameterTypes14,
                    new Object[] { ClpSerializer.translateInput(purchaseRequest) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.hp.omp.model.PurchaseRequest) ClpSerializer.translateOutput(returnObj);
    }

    public com.hp.omp.model.PurchaseRequest updatePurchaseRequest(
        com.hp.omp.model.PurchaseRequest purchaseRequest, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName15,
                    _methodParameterTypes15,
                    new Object[] {
                        ClpSerializer.translateInput(purchaseRequest),
                        
                    merge
                    });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.hp.omp.model.PurchaseRequest) ClpSerializer.translateOutput(returnObj);
    }

    public java.lang.String getBeanIdentifier() {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName16,
                    _methodParameterTypes16, new Object[] {  });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.lang.String) ClpSerializer.translateOutput(returnObj);
    }

    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        try {
            _invokableLocalService.invokeMethod(_methodName17,
                _methodParameterTypes17,
                new Object[] { ClpSerializer.translateInput(beanIdentifier) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        throw new UnsupportedOperationException();
    }

    public com.hp.omp.model.PurchaseRequest getPurchaseRequestByStatus(
        long prId, int status)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName19,
                    _methodParameterTypes19, new Object[] { prId, status });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (com.hp.omp.model.PurchaseRequest) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequests(
        java.util.List<java.lang.Long> purchaseRequestsIDList)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName20,
                    _methodParameterTypes20,
                    new Object[] {
                        ClpSerializer.translateInput(purchaseRequestsIDList)
                    });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.PurchaseRequest>) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequesListByStatus(
        int status) throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName21,
                    _methodParameterTypes21, new Object[] { status });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.PurchaseRequest>) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequesListByStatus(
        int status, int start, int end, java.lang.String orderby,
        java.lang.String order)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName22,
                    _methodParameterTypes22,
                    new Object[] {
                        status,
                        
                    start,
                        
                    end,
                        
                    ClpSerializer.translateInput(orderby),
                        
                    ClpSerializer.translateInput(order)
                    });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.PurchaseRequest>) ClpSerializer.translateOutput(returnObj);
    }

    public java.lang.Long getPurchaseRequesListByStatusCount(int status,
        boolean automatic)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName23,
                    _methodParameterTypes23, new Object[] { status, automatic });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.lang.Long) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequesListByStatusAndType(
        int status, boolean automatic, int start, int end,
        java.lang.String orderby, java.lang.String order)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName24,
                    _methodParameterTypes24,
                    new Object[] {
                        status,
                        
                    automatic,
                        
                    start,
                        
                    end,
                        
                    ClpSerializer.translateInput(orderby),
                        
                    ClpSerializer.translateInput(order)
                    });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.PurchaseRequest>) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequesListByPositionStatusAndType(
        java.util.Set<java.lang.Long> purchaseRequestIdList, boolean automatic,
        int start, int end, java.lang.String orderby, java.lang.String order)
        throws java.lang.Exception {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName25,
                    _methodParameterTypes25,
                    new Object[] {
                        ClpSerializer.translateInput(purchaseRequestIdList),
                        
                    automatic,
                        
                    start,
                        
                    end,
                        
                    ClpSerializer.translateInput(orderby),
                        
                    ClpSerializer.translateInput(order)
                    });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof java.lang.Exception) {
                throw (java.lang.Exception) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.PurchaseRequest>) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequestListByPositionStatusAndType(
        int positionStatus, boolean automatic, int start, int end,
        java.lang.String orderby, java.lang.String order)
        throws java.lang.Exception {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName26,
                    _methodParameterTypes26,
                    new Object[] {
                        positionStatus,
                        
                    automatic,
                        
                    start,
                        
                    end,
                        
                    ClpSerializer.translateInput(orderby),
                        
                    ClpSerializer.translateInput(order)
                    });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof java.lang.Exception) {
                throw (java.lang.Exception) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.PurchaseRequest>) ClpSerializer.translateOutput(returnObj);
    }

    public java.lang.Long getPurchaseRequestListByPositionStatusAndTypeCount(
        int positionStatus, boolean automatic) throws java.lang.Exception {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName27,
                    _methodParameterTypes27,
                    new Object[] { positionStatus, automatic });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof java.lang.Exception) {
                throw (java.lang.Exception) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.lang.Long) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPRListByStatusAndCostCenter(
        int status, long costCenterId, int roleCode, int start, int end,
        java.lang.String orderby, java.lang.String order)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName28,
                    _methodParameterTypes28,
                    new Object[] {
                        status,
                        
                    costCenterId,
                        
                    roleCode,
                        
                    start,
                        
                    end,
                        
                    ClpSerializer.translateInput(orderby),
                        
                    ClpSerializer.translateInput(order)
                    });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.PurchaseRequest>) ClpSerializer.translateOutput(returnObj);
    }

    public java.lang.Long getPRListByStatusAndCostCenterCount(int status,
        long costCenterId, int roleCode)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName29,
                    _methodParameterTypes29,
                    new Object[] { status, costCenterId, roleCode });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.lang.Long) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.custom.PrListDTO> getPurchaseRequestList(
        com.hp.omp.model.custom.SearchDTO searchDTO, int roleCode, long userId) {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName30,
                    _methodParameterTypes30,
                    new Object[] {
                        ClpSerializer.translateInput(searchDTO),
                        
                    roleCode,
                        
                    userId
                    });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.custom.PrListDTO>) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.custom.PrListDTO> prSearch(
        com.hp.omp.model.custom.SearchDTO searchDTO, int roleCode, long userId) {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName31,
                    _methodParameterTypes31,
                    new Object[] {
                        ClpSerializer.translateInput(searchDTO),
                        
                    roleCode,
                        
                    userId
                    });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.custom.PrListDTO>) ClpSerializer.translateOutput(returnObj);
    }

    public java.lang.Long getPurchaseRequestCount(
        com.hp.omp.model.custom.SearchDTO searchDTO, int roleCode, long userId) {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName32,
                    _methodParameterTypes32,
                    new Object[] {
                        ClpSerializer.translateInput(searchDTO),
                        
                    roleCode,
                        
                    userId
                    });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.lang.Long) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.custom.PrListDTO> getAntexPurchaseRequestList(
        com.hp.omp.model.custom.SearchDTO searchDTO) {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName33,
                    _methodParameterTypes33,
                    new Object[] { ClpSerializer.translateInput(searchDTO) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.custom.PrListDTO>) ClpSerializer.translateOutput(returnObj);
    }

    public java.lang.Long getAntexPurchaseRequestCount(
        com.hp.omp.model.custom.SearchDTO searchDTO) {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName34,
                    _methodParameterTypes34,
                    new Object[] { ClpSerializer.translateInput(searchDTO) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.lang.Long) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequestByProjectId(
        java.lang.String projectId) throws java.lang.Exception {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName35,
                    _methodParameterTypes35,
                    new Object[] { ClpSerializer.translateInput(projectId) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof java.lang.Exception) {
                throw (java.lang.Exception) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.PurchaseRequest>) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequestBySubProjectId(
        java.lang.String subProjectId) throws java.lang.Exception {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName36,
                    _methodParameterTypes36,
                    new Object[] { ClpSerializer.translateInput(subProjectId) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof java.lang.Exception) {
                throw (java.lang.Exception) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.PurchaseRequest>) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequestByVendorId(
        java.lang.String vendorId) throws java.lang.Exception {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName37,
                    _methodParameterTypes37,
                    new Object[] { ClpSerializer.translateInput(vendorId) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof java.lang.Exception) {
                throw (java.lang.Exception) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.PurchaseRequest>) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequestByBudgetCategoryId(
        java.lang.String budgetCategoryId) throws java.lang.Exception {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName38,
                    _methodParameterTypes38,
                    new Object[] { ClpSerializer.translateInput(
                            budgetCategoryId) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof java.lang.Exception) {
                throw (java.lang.Exception) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.PurchaseRequest>) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequestByBudgetSubCategoryId(
        java.lang.String budgetSubCategoryId) throws java.lang.Exception {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName39,
                    _methodParameterTypes39,
                    new Object[] {
                        ClpSerializer.translateInput(budgetSubCategoryId)
                    });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof java.lang.Exception) {
                throw (java.lang.Exception) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.PurchaseRequest>) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequestByOdnpNameId(
        java.lang.String odnpNameId) throws java.lang.Exception {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName40,
                    _methodParameterTypes40,
                    new Object[] { ClpSerializer.translateInput(odnpNameId) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof java.lang.Exception) {
                throw (java.lang.Exception) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.PurchaseRequest>) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequestByMacroDriverId(
        java.lang.String macroDriver) throws java.lang.Exception {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName41,
                    _methodParameterTypes41,
                    new Object[] { ClpSerializer.translateInput(macroDriver) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof java.lang.Exception) {
                throw (java.lang.Exception) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.PurchaseRequest>) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequestByMicroDriverId(
        java.lang.String microDriver) throws java.lang.Exception {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName42,
                    _methodParameterTypes42,
                    new Object[] { ClpSerializer.translateInput(microDriver) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof java.lang.Exception) {
                throw (java.lang.Exception) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.PurchaseRequest>) ClpSerializer.translateOutput(returnObj);
    }
}
