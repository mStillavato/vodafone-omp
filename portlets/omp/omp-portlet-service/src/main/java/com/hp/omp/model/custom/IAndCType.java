package com.hp.omp.model.custom;

import java.util.HashMap;
import java.util.Map;

public enum IAndCType {
	ICFIELD(1, "I&C Field"),
	ICTEST(2,"I&C Test"),
	ICSERVIZI(3, "Servizi");
	
	private int code;
	private String label;

	private IAndCType(int code, String label){
		this.code = code;
		this.label = label;
	}

	private static Map<Integer, IAndCType> codeToStatusMapping;

	public static IAndCType getStatus(int i) {
		if (codeToStatusMapping == null) {
			initMapping();
		}
		return codeToStatusMapping.get(i);
	}

	private static void initMapping() {
		codeToStatusMapping = new HashMap<Integer, IAndCType>();
		for (IAndCType s : values()) {
			codeToStatusMapping.put(s.code, s);
		}
	}
	
	public int getCode(){
		return code;
	}

	public String getLabel(){
		return label;
	}
}
