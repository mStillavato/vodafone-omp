package com.hp.omp.service;

import java.io.OutputStream;
import java.util.List;

import com.hp.omp.model.custom.ReportCheckIcDTO;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.InvokableLocalService;


public class ReportLocalServiceClp implements ReportLocalService {
    private InvokableLocalService _invokableLocalService;
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;
    private String _methodName9;
    private String[] _methodParameterTypes9;
    private String _methodName10;
    private String[] _methodParameterTypes10;
    private String _methodName11;
    private String[] _methodParameterTypes11;
    private String _methodName12;
    private String[] _methodParameterTypes12;
    private String _methodName13;
    private String[] _methodParameterTypes13;
    
    private String _methodName14;
    private String[] _methodParameterTypes14;
    private String _methodName15;
    private String[] _methodParameterTypes15;
    private String _methodName16;
    private String[] _methodParameterTypes16;

    public ReportLocalServiceClp(InvokableLocalService invokableLocalService) {
        _invokableLocalService = invokableLocalService;

        _methodName0 = "getBeanIdentifier";
        _methodParameterTypes0 = new String[] {  };

        _methodName1 = "setBeanIdentifier";
        _methodParameterTypes1 = new String[] { "java.lang.String" };

        _methodName3 = "getAllCostCenters";
        _methodParameterTypes3 = new String[] {  };

        _methodName4 = "getAllProjectNames";
        _methodParameterTypes4 = new String[] {  };

        _methodName5 = "getSubProjects";
        _methodParameterTypes5 = new String[] { "java.lang.Long" };

        _methodName6 = "getAllFiscalYears";
        _methodParameterTypes6 = new String[] {  };

        _methodName7 = "generateAggregateReportClosed";
        _methodParameterTypes7 = new String[] {
                "com.hp.omp.model.custom.ReportDTO"
            };

        _methodName8 = "generateAggregateReportClosing";
        _methodParameterTypes8 = new String[] {
                "com.hp.omp.model.custom.ReportDTO"
            };

        _methodName9 = "generateAggregateReportTotalPoistionValues";
        _methodParameterTypes9 = new String[] {
                "com.hp.omp.model.custom.ReportDTO"
            };

        _methodName10 = "generateAggregatePrPositionsWithoutPoNumberValue";
        _methodParameterTypes10 = new String[] {
                "com.hp.omp.model.custom.ReportDTO"
            };

        _methodName11 = "generateDetailReport";
        _methodParameterTypes11 = new String[] {
                "com.hp.omp.model.custom.ReportDTO"
            };

        _methodName12 = "exportDetailsReportAsExcel";
        _methodParameterTypes12 = new String[] {
                "com.hp.omp.model.custom.ReportDTO", "java.util.List",
                "java.io.OutputStream"
            };

        _methodName13 = "exportReportAsExcel";
        _methodParameterTypes13 = new String[] {
                "com.hp.omp.model.custom.ReportDTO", "java.util.List",
                "java.io.OutputStream"
            };
        
        _methodName14 = "getReportCheckIcList";
        _methodParameterTypes14 = new String[] {
                "com.hp.omp.model.custom.SearchReportCheckIcDTO"
            };
        
        _methodName15 = "getReportCheckIcListCount";
        _methodParameterTypes15 = new String[] {
                "com.hp.omp.model.custom.SearchReportCheckIcDTO"
            };
        
        _methodName16 = "exportReportCheckIcAsExcel";
        _methodParameterTypes16 = new String[] {"java.util.List", "java.io.OutputStream"};
    }

    public java.lang.String getBeanIdentifier() {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName0,
                    _methodParameterTypes0, new Object[] {  });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.lang.String) ClpSerializer.translateOutput(returnObj);
    }

    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        try {
            _invokableLocalService.invokeMethod(_methodName1,
                _methodParameterTypes1,
                new Object[] { ClpSerializer.translateInput(beanIdentifier) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        throw new UnsupportedOperationException();
    }

    public java.util.List<java.lang.String> getAllCostCenters()
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName3,
                    _methodParameterTypes3, new Object[] {  });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<java.lang.String>) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<java.lang.String> getAllProjectNames()
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName4,
                    _methodParameterTypes4, new Object[] {  });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<java.lang.String>) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<java.lang.String> getSubProjects(
        java.lang.Long projectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName5,
                    _methodParameterTypes5,
                    new Object[] { ClpSerializer.translateInput(projectId) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<java.lang.String>) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<java.lang.String> getAllFiscalYears()
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName6,
                    _methodParameterTypes6, new Object[] {  });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<java.lang.String>) ClpSerializer.translateOutput(returnObj);
    }

    public java.lang.Double generateAggregateReportClosed(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName7,
                    _methodParameterTypes7,
                    new Object[] { ClpSerializer.translateInput(reportDTO) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.lang.Double) ClpSerializer.translateOutput(returnObj);
    }

    public java.lang.Double generateAggregateReportClosing(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName8,
                    _methodParameterTypes8,
                    new Object[] { ClpSerializer.translateInput(reportDTO) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.lang.Double) ClpSerializer.translateOutput(returnObj);
    }

    public java.lang.Double generateAggregateReportTotalPoistionValues(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName9,
                    _methodParameterTypes9,
                    new Object[] { ClpSerializer.translateInput(reportDTO) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.lang.Double) ClpSerializer.translateOutput(returnObj);
    }

    public java.lang.Double generateAggregatePrPositionsWithoutPoNumberValue(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName10,
                    _methodParameterTypes10,
                    new Object[] { ClpSerializer.translateInput(reportDTO) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.lang.Double) ClpSerializer.translateOutput(returnObj);
    }

    public java.util.List<com.hp.omp.model.custom.ExcelTableRow> generateDetailReport(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName11,
                    _methodParameterTypes11,
                    new Object[] { ClpSerializer.translateInput(reportDTO) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<com.hp.omp.model.custom.ExcelTableRow>) ClpSerializer.translateOutput(returnObj);
    }

    public void exportDetailsReportAsExcel(
        com.hp.omp.model.custom.ReportDTO reportDTO,
        java.util.List<java.lang.Object> header, java.io.OutputStream out)
        throws com.liferay.portal.kernel.exception.SystemException {
        try {
            _invokableLocalService.invokeMethod(_methodName12,
                _methodParameterTypes12,
                new Object[] {
                    ClpSerializer.translateInput(reportDTO),
                    
                ClpSerializer.translateInput(header),
                    
                ClpSerializer.translateInput(out)
                });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }
    }

    public void exportReportAsExcel(
        com.hp.omp.model.custom.ReportDTO reportDTO,
        java.util.List<java.lang.Object> header, java.io.OutputStream out)
        throws com.liferay.portal.kernel.exception.SystemException {
        try {
            _invokableLocalService.invokeMethod(_methodName13,
                _methodParameterTypes13,
                new Object[] {
                    ClpSerializer.translateInput(reportDTO),
                    
                ClpSerializer.translateInput(header),
                    
                ClpSerializer.translateInput(out)
                });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }
    }

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<ReportCheckIcDTO> getReportCheckIcList(
			com.hp.omp.model.custom.SearchReportCheckIcDTO searchDTO) throws SystemException {
		
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName14,
					_methodParameterTypes14,
                    new Object[] { ClpSerializer.translateInput(searchDTO) });
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException) t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return (List<com.hp.omp.model.custom.ReportCheckIcDTO>) ClpSerializer.translateOutput(returnObj);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long getReportCheckIcListCount(
			com.hp.omp.model.custom.SearchReportCheckIcDTO searchDTO) throws SystemException {
		
        Object returnObj = null;

        try {
            returnObj = _invokableLocalService.invokeMethod(_methodName15,
                    _methodParameterTypes15, new Object[] { ClpSerializer.translateInput(searchDTO) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return ((Integer) returnObj).intValue();
	}

	public void exportReportCheckIcAsExcel(List<Object> header, OutputStream out)
			throws SystemException {
        try {
            _invokableLocalService.invokeMethod(_methodName16,
                _methodParameterTypes16,
                new Object[] {ClpSerializer.translateInput(header),
            					ClpSerializer.translateInput(out)
                });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }
    }
}
