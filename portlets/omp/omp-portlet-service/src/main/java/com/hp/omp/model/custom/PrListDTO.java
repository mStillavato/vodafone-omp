/**
 * 
 */
package com.hp.omp.model.custom;

import java.io.IOException;
import java.io.Serializable;
import java.util.Locale;

/**
 * @author Sami Mohamed
 *
 */
public class PrListDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7684541966359000646L;
	
	private long purchaseRequestId;
	private String screenNameRequester;
	private String screenNameReciever;
	private String vendorName;
	private String budgetSubCategoryName;
	private String totalValue;
	private String costCenterName;
	private String fiscalYear;
	private String activityDescription;
	private boolean automatic;
	private int status;
	private String currency;
	private String costCenterId;
	boolean hasFile;
	
	private String foramttedTotalValue;
	
	private int tipoPr;
	
	
	public int getTipoPr() {
		return tipoPr;
	}
	public void setTipoPr(int tipoPr) {
		this.tipoPr = tipoPr;
	}
	
	public void setForamttedTotalValue(String foramttedTotalValue) {
		this.foramttedTotalValue = foramttedTotalValue;
	}
	public String getForamttedTotalValue() {
		if(this.totalValue !=null && ! "".equals(this.totalValue) && ! "null".equals(this.totalValue)) {
		return OmpFormatterUtil.formatNumberByLocale(Double.valueOf(this.totalValue),Locale.ITALY);
		}else{
			return this.totalValue;
		}
	}
	
	public String getCostCenterId() {
		return costCenterId;
	}
	public void setCostCenterId(String costCenterId) {
		this.costCenterId = costCenterId;
	}
	public boolean isHasFile() {
		return hasFile;
	}
	public void setHasFile(boolean hasFile) {
		this.hasFile = hasFile;
	}
	public long getPurchaseRequestId() {
		return purchaseRequestId;
	}
	public void setPurchaseRequestId(long purchaseRequestId) {
		this.purchaseRequestId = purchaseRequestId;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getScreenNameRequester() {
		return screenNameRequester;
	}
	public void setScreenNameRequester(String screenNameRequester) {
		this.screenNameRequester = screenNameRequester;
	}
	public String getScreenNameReciever() {
		return screenNameReciever;
	}
	public void setScreenNameReciever(String screenNameReciever) {
		this.screenNameReciever = screenNameReciever;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getBudgetSubCategoryName() {
		return budgetSubCategoryName;
	}
	public void setBudgetSubCategoryName(String budgetSubCategoryName) {
		this.budgetSubCategoryName = budgetSubCategoryName;
	}
	public String getTotalValue() {
		
		String totalValueCurrency = "";
		
		
		if(this.totalValue != null && !("").equals(this.totalValue)&& !("null").equals(this.totalValue)){
			String currency = "";
			if(this.currency.equals(Currency.USD.getCode()+""))
				currency = "$";
			else if (this.currency.equals(Currency.EUR.getCode()+""))
				currency = "€";
			
			totalValueCurrency = this.totalValue + " " + currency;
		}
		else{
			totalValueCurrency = "";
		}
		return totalValueCurrency;
	}
	public void setTotalValue(String totalValue) {
		this.totalValue = totalValue;
	}
	public String getCostCenterName() {
		return costCenterName;
	}
	public void setCostCenterName(String costCenterName) {
		this.costCenterName = costCenterName;
	}
	public String getFiscalYear() {
		return fiscalYear;
	}
	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}
	public String getActivityDescription() {
		return activityDescription;
	}
	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}
	public boolean isAutomatic() {
		return automatic;
	}
	public void setAutomatic(boolean automatic) {
		this.automatic = automatic;
	}
	
	public String getStatus() {
		
		String statusName = "";

		statusName = PurchaseRequestStatus.getStatus(this.status).getLabel();
        return statusName;
	}
	public void setStatus(int status) {
		this.status = status;
	}

}
