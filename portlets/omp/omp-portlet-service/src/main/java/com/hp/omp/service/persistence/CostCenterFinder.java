package com.hp.omp.service.persistence;

public interface CostCenterFinder {
    
    public java.util.List<com.hp.omp.model.CostCenter> getCostCentersList(
    		int start, int end, String searchFilter, int gruppoUsers)
            throws com.liferay.portal.kernel.exception.SystemException;
}
