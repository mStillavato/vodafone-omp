package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link BudgetSubCategory}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       BudgetSubCategory
 * @generated
 */
public class BudgetSubCategoryWrapper implements BudgetSubCategory,
    ModelWrapper<BudgetSubCategory> {
    private BudgetSubCategory _budgetSubCategory;

    public BudgetSubCategoryWrapper(BudgetSubCategory budgetSubCategory) {
        _budgetSubCategory = budgetSubCategory;
    }

    public Class<?> getModelClass() {
        return BudgetSubCategory.class;
    }

    public String getModelClassName() {
        return BudgetSubCategory.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("budgetSubCategoryId", getBudgetSubCategoryId());
        attributes.put("budgetSubCategoryName", getBudgetSubCategoryName());
        attributes.put("display", getDisplay());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long budgetSubCategoryId = (Long) attributes.get("budgetSubCategoryId");

        if (budgetSubCategoryId != null) {
            setBudgetSubCategoryId(budgetSubCategoryId);
        }

        String budgetSubCategoryName = (String) attributes.get(
                "budgetSubCategoryName");

        if (budgetSubCategoryName != null) {
            setBudgetSubCategoryName(budgetSubCategoryName);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
    }

    /**
    * Returns the primary key of this budget sub category.
    *
    * @return the primary key of this budget sub category
    */
    public long getPrimaryKey() {
        return _budgetSubCategory.getPrimaryKey();
    }

    /**
    * Sets the primary key of this budget sub category.
    *
    * @param primaryKey the primary key of this budget sub category
    */
    public void setPrimaryKey(long primaryKey) {
        _budgetSubCategory.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the budget sub category ID of this budget sub category.
    *
    * @return the budget sub category ID of this budget sub category
    */
    public long getBudgetSubCategoryId() {
        return _budgetSubCategory.getBudgetSubCategoryId();
    }

    /**
    * Sets the budget sub category ID of this budget sub category.
    *
    * @param budgetSubCategoryId the budget sub category ID of this budget sub category
    */
    public void setBudgetSubCategoryId(long budgetSubCategoryId) {
        _budgetSubCategory.setBudgetSubCategoryId(budgetSubCategoryId);
    }

    /**
    * Returns the budget sub category name of this budget sub category.
    *
    * @return the budget sub category name of this budget sub category
    */
    public java.lang.String getBudgetSubCategoryName() {
        return _budgetSubCategory.getBudgetSubCategoryName();
    }

    /**
    * Sets the budget sub category name of this budget sub category.
    *
    * @param budgetSubCategoryName the budget sub category name of this budget sub category
    */
    public void setBudgetSubCategoryName(java.lang.String budgetSubCategoryName) {
        _budgetSubCategory.setBudgetSubCategoryName(budgetSubCategoryName);
    }

    /**
    * Returns the display of this budget sub category.
    *
    * @return the display of this budget sub category
    */
    public boolean getDisplay() {
        return _budgetSubCategory.getDisplay();
    }

    /**
    * Returns <code>true</code> if this budget sub category is display.
    *
    * @return <code>true</code> if this budget sub category is display; <code>false</code> otherwise
    */
    public boolean isDisplay() {
        return _budgetSubCategory.isDisplay();
    }

    /**
    * Sets whether this budget sub category is display.
    *
    * @param display the display of this budget sub category
    */
    public void setDisplay(boolean display) {
        _budgetSubCategory.setDisplay(display);
    }

    public boolean isNew() {
        return _budgetSubCategory.isNew();
    }

    public void setNew(boolean n) {
        _budgetSubCategory.setNew(n);
    }

    public boolean isCachedModel() {
        return _budgetSubCategory.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _budgetSubCategory.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _budgetSubCategory.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _budgetSubCategory.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _budgetSubCategory.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _budgetSubCategory.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _budgetSubCategory.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new BudgetSubCategoryWrapper((BudgetSubCategory) _budgetSubCategory.clone());
    }

    public int compareTo(BudgetSubCategory budgetSubCategory) {
        return _budgetSubCategory.compareTo(budgetSubCategory);
    }

    @Override
    public int hashCode() {
        return _budgetSubCategory.hashCode();
    }

    public com.liferay.portal.model.CacheModel<BudgetSubCategory> toCacheModel() {
        return _budgetSubCategory.toCacheModel();
    }

    public BudgetSubCategory toEscapedModel() {
        return new BudgetSubCategoryWrapper(_budgetSubCategory.toEscapedModel());
    }

    public BudgetSubCategory toUnescapedModel() {
        return new BudgetSubCategoryWrapper(_budgetSubCategory.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _budgetSubCategory.toString();
    }

    public java.lang.String toXmlString() {
        return _budgetSubCategory.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _budgetSubCategory.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof BudgetSubCategoryWrapper)) {
            return false;
        }

        BudgetSubCategoryWrapper budgetSubCategoryWrapper = (BudgetSubCategoryWrapper) obj;

        if (Validator.equals(_budgetSubCategory,
                    budgetSubCategoryWrapper._budgetSubCategory)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public BudgetSubCategory getWrappedBudgetSubCategory() {
        return _budgetSubCategory;
    }

    public BudgetSubCategory getWrappedModel() {
        return _budgetSubCategory;
    }

    public void resetOriginalValues() {
        _budgetSubCategory.resetOriginalValues();
    }
}
