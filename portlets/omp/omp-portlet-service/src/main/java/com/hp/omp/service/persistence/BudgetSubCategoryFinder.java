package com.hp.omp.service.persistence;

public interface BudgetSubCategoryFinder {
    
    public java.util.List<com.hp.omp.model.BudgetSubCategory> getBudgetSubCategoryList(
    		int start, int end, String searchFilter)
            throws com.liferay.portal.kernel.exception.SystemException;
}
