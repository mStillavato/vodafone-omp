package com.hp.omp.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the position local service. This utility wraps {@link com.hp.omp.service.impl.PositionLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see PositionLocalService
 * @see com.hp.omp.service.base.PositionLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.PositionLocalServiceImpl
 * @generated
 */
public class PositionLocalServiceUtil {
    private static PositionLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.hp.omp.service.impl.PositionLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the position to the database. Also notifies the appropriate model listeners.
    *
    * @param position the position
    * @return the position that was added
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Position addPosition(
        com.hp.omp.model.Position position)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addPosition(position);
    }

    /**
    * Creates a new position with the primary key. Does not add the position to the database.
    *
    * @param positionId the primary key for the new position
    * @return the new position
    */
    public static com.hp.omp.model.Position createPosition(long positionId) {
        return getService().createPosition(positionId);
    }

    /**
    * Deletes the position with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param positionId the primary key of the position
    * @return the position that was removed
    * @throws PortalException if a position with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Position deletePosition(long positionId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deletePosition(positionId);
    }

    /**
    * Deletes the position from the database. Also notifies the appropriate model listeners.
    *
    * @param position the position
    * @return the position that was removed
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Position deletePosition(
        com.hp.omp.model.Position position)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deletePosition(position);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    public static com.hp.omp.model.Position fetchPosition(long positionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchPosition(positionId);
    }

    /**
    * Returns the position with the primary key.
    *
    * @param positionId the primary key of the position
    * @return the position
    * @throws PortalException if a position with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Position getPosition(long positionId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPosition(positionId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the positions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of positions
    * @param end the upper bound of the range of positions (not inclusive)
    * @return the range of positions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.Position> getPositions(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPositions(start, end);
    }

    /**
    * Returns the number of positions.
    *
    * @return the number of positions
    * @throws SystemException if a system exception occurred
    */
    public static int getPositionsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPositionsCount();
    }

    /**
    * Updates the position in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param position the position
    * @return the position that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Position updatePosition(
        com.hp.omp.model.Position position)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updatePosition(position);
    }

    /**
    * Updates the position in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param position the position
    * @param merge whether to merge the position with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the position that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Position updatePosition(
        com.hp.omp.model.Position position, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updatePosition(position, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static com.hp.omp.model.Position updatePositionWithoutHocks(
        com.hp.omp.model.Position position, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updatePositionWithoutHocks(position, merge);
    }

    public static java.util.List<java.util.Date> getPositionsMaxRegisterationDate(
        long Id, java.lang.String type)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPositionsMaxRegisterationDate(Id, type);
    }

    public static java.util.List<com.hp.omp.model.Position> getPurchaseOrderPositions(
        java.lang.String purchaseOrderId, long userId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPurchaseOrderPositions(purchaseOrderId, userId);
    }

    public static java.lang.Long getPurchaseRequestPositionsCount(
        com.hp.omp.model.Position position)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPurchaseRequestPositionsCount(position);
    }

    public static java.lang.Long getPurchaseRequestPostionsByStatusCount(
        com.hp.omp.model.Position position,
        com.hp.omp.model.custom.PositionStatus status)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .getPurchaseRequestPostionsByStatusCount(position, status);
    }

    public static java.util.List<com.hp.omp.model.Position> getPurchaseRequestPositions(
        java.lang.String purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPurchaseRequestPositions(purchaseRequestId);
    }

    public static java.util.List<com.hp.omp.model.Position> getPositionsByPrId(
        long purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPositionsByPrId(purchaseRequestId);
    }

    public static java.util.List<java.util.Date> getPrPositionsMinimumDeliveryDate(
        long purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPrPositionsMinimumDeliveryDate(purchaseRequestId);
    }

    public static java.util.List<java.util.Date> getPoRecievedPrPositionsMinimumDeliveryDate(
        long purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .getPoRecievedPrPositionsMinimumDeliveryDate(purchaseRequestId);
    }

    public static java.util.List<java.util.Date> getPoPositionsMinimumDeliveryDate(
        long purchaseorderId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPoPositionsMinimumDeliveryDate(purchaseorderId);
    }

    public static java.util.List<java.lang.Long> getPositionsMaxLabelNumber(
        long purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPositionsMaxLabelNumber(purchaseRequestId);
    }

    public static java.util.List<com.hp.omp.model.Position> getPositionsByStatus(
        long prId, int positionStatus) throws java.lang.Exception {
        return getService().getPositionsByStatus(prId, positionStatus);
    }
    
    public static java.util.List<com.hp.omp.model.Position> getPositionsByStatusVbs(
            long prId, int positionStatus) throws java.lang.Exception {
            return getService().getPositionsByStatusVbs(prId, positionStatus);
        }
    
    public static java.util.List<com.hp.omp.model.Position> getPositionsNssByStatus(
            long prId, int positionStatus) throws java.lang.Exception {
            return getService().getPositionsNssByStatus(prId, positionStatus);
        }   

    public static java.util.List<com.hp.omp.model.Position> getPositionsByStatusOnly(
        int positionStatus) throws java.lang.Exception {
        return getService().getPositionsByStatusOnly(positionStatus);
    }

    public static java.util.List<com.hp.omp.model.Position> getPositionsByPurchaseOrderId(
        long poId) throws java.lang.Exception {
        return getService().getPositionsByPurchaseOrderId(poId);
    }

    public static java.util.List<com.hp.omp.model.Position> getPositionsByWbsCodeId(
        java.lang.String wbsCodeId) throws java.lang.Exception {
        return getService().getPositionsByWbsCodeId(wbsCodeId);
    }

    public static java.util.List<java.lang.String> getAllFiscalYears()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getAllFiscalYears();
    }

    public static java.util.List<java.lang.String> getAllWBSCodes()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getAllWBSCodes();
    }

    public static java.lang.Long getIAndCPositionsCount(
        java.lang.String purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getIAndCPositionsCount(purchaseRequestId);
    }

    public static java.util.List<com.hp.omp.model.Position> getPositionsByIAndCType(
        long prId, java.lang.String iAndCType) throws java.lang.Exception {
        return getService().getPositionsByIAndCType(prId, iAndCType);
    }

    public static java.util.List<com.hp.omp.model.Position> getPositionsByIAndCTypeAndApprovalStatus(
        long prId, java.lang.String iAndCType, boolean approved)
        throws java.lang.Exception {
        return getService()
                   .getPositionsByIAndCTypeAndApprovalStatus(prId, iAndCType,
            approved);
    }

    public static java.util.List<com.hp.omp.model.Position> getIAndCPositions(
        long prId) throws java.lang.Exception {
        return getService().getIAndCPositions(prId);
    }

    public static void copyWBSfromPR(com.hp.omp.model.Position position)
        throws com.liferay.portal.kernel.exception.SystemException {
        getService().copyWBSfromPR(position);
    }

    public static void clearService() {
        _service = null;
    }

    public static PositionLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    PositionLocalService.class.getName());

            if (invokableLocalService instanceof PositionLocalService) {
                _service = (PositionLocalService) invokableLocalService;
            } else {
                _service = new PositionLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(PositionLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated
     */
    public void setService(PositionLocalService service) {
    }
}
