package com.hp.omp.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class PositionFinderUtil {
    private static PositionFinder _finder;

    public static java.util.List<java.lang.String> getAllFiscalYears()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getFinder().getAllFiscalYears();
    }

    public static java.util.List<java.lang.String> getAllWBSCodes()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getFinder().getAllWBSCodes();
    }

    public static java.lang.Long getIAndCPositionsCount(
        java.lang.String purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getFinder().getIAndCPositionsCount(purchaseRequestId);
    }

    public static PositionFinder getFinder() {
        if (_finder == null) {
            _finder = (PositionFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    PositionFinder.class.getName());

            ReferenceRegistry.registerReference(PositionFinderUtil.class,
                "_finder");
        }

        return _finder;
    }

    public void setFinder(PositionFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(PositionFinderUtil.class, "_finder");
    }
}
