package com.hp.omp.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class ProjectFinderUtil {
	private static ProjectFinder _finder;

	public static java.util.List<com.hp.omp.model.Project> getProjectsList(
			int start, int end, String searchFilter)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getFinder().getProjectsList(start, end, searchFilter);
	}
	
	public static int getProjectsCountList(String searchFilter)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getFinder().getProjectsCountList(searchFilter);
	}

	public static ProjectFinder getFinder() {
		if (_finder == null) {
			_finder = (ProjectFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
					ProjectFinder.class.getName());

			ReferenceRegistry.registerReference(ProjectFinderUtil.class,
					"_finder");
		}

		return _finder;
	}

	public void setFinder(ProjectFinder finder) {
		_finder = finder;

		ReferenceRegistry.registerReference(ProjectFinderUtil.class,
				"_finder");
	}
}
