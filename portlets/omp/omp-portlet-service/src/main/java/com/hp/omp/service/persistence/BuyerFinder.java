package com.hp.omp.service.persistence;

public interface BuyerFinder {
    public java.util.List<com.hp.omp.model.Buyer> getBuyersList(
            int start, int end, String searchFilter, int gruppoUsers)
            throws com.liferay.portal.kernel.exception.SystemException;
    
    public int getBuyersCountList(String searchFilter, int gruppoUsers)
            throws com.liferay.portal.kernel.exception.SystemException;
}
