package com.hp.omp.service;

import java.io.OutputStream;
import java.util.List;

import com.hp.omp.model.TrackingCodes;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link TrackingCodesLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       TrackingCodesLocalService
 * @generated
 */
public class TrackingCodesLocalServiceWrapper
    implements TrackingCodesLocalService,
        ServiceWrapper<TrackingCodesLocalService> {
    private TrackingCodesLocalService _trackingCodesLocalService;

    public TrackingCodesLocalServiceWrapper(
        TrackingCodesLocalService trackingCodesLocalService) {
        _trackingCodesLocalService = trackingCodesLocalService;
    }

    /**
    * Adds the tracking codes to the database. Also notifies the appropriate model listeners.
    *
    * @param trackingCodes the tracking codes
    * @return the tracking codes that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.TrackingCodes addTrackingCodes(
        com.hp.omp.model.TrackingCodes trackingCodes)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trackingCodesLocalService.addTrackingCodes(trackingCodes);
    }

    /**
    * Creates a new tracking codes with the primary key. Does not add the tracking codes to the database.
    *
    * @param trackingCodeId the primary key for the new tracking codes
    * @return the new tracking codes
    */
    public com.hp.omp.model.TrackingCodes createTrackingCodes(
        long trackingCodeId) {
        return _trackingCodesLocalService.createTrackingCodes(trackingCodeId);
    }

    /**
    * Deletes the tracking codes with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param trackingCodeId the primary key of the tracking codes
    * @return the tracking codes that was removed
    * @throws PortalException if a tracking codes with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.TrackingCodes deleteTrackingCodes(
        long trackingCodeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _trackingCodesLocalService.deleteTrackingCodes(trackingCodeId);
    }

    /**
    * Deletes the tracking codes from the database. Also notifies the appropriate model listeners.
    *
    * @param trackingCodes the tracking codes
    * @return the tracking codes that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.TrackingCodes deleteTrackingCodes(
        com.hp.omp.model.TrackingCodes trackingCodes)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trackingCodesLocalService.deleteTrackingCodes(trackingCodes);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _trackingCodesLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trackingCodesLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _trackingCodesLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trackingCodesLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trackingCodesLocalService.dynamicQueryCount(dynamicQuery);
    }

    public com.hp.omp.model.TrackingCodes fetchTrackingCodes(
        long trackingCodeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trackingCodesLocalService.fetchTrackingCodes(trackingCodeId);
    }

    /**
    * Returns the tracking codes with the primary key.
    *
    * @param trackingCodeId the primary key of the tracking codes
    * @return the tracking codes
    * @throws PortalException if a tracking codes with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.TrackingCodes getTrackingCodes(long trackingCodeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _trackingCodesLocalService.getTrackingCodes(trackingCodeId);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _trackingCodesLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the tracking codeses.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of tracking codeses
    * @param end the upper bound of the range of tracking codeses (not inclusive)
    * @return the range of tracking codeses
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.TrackingCodes> getTrackingCodeses(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trackingCodesLocalService.getTrackingCodeses(start, end);
    }

    /**
    * Returns the number of tracking codeses.
    *
    * @return the number of tracking codeses
    * @throws SystemException if a system exception occurred
    */
    public int getTrackingCodesesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trackingCodesLocalService.getTrackingCodesesCount();
    }

    /**
    * Updates the tracking codes in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param trackingCodes the tracking codes
    * @return the tracking codes that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.TrackingCodes updateTrackingCodes(
        com.hp.omp.model.TrackingCodes trackingCodes)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trackingCodesLocalService.updateTrackingCodes(trackingCodes);
    }

    /**
    * Updates the tracking codes in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param trackingCodes the tracking codes
    * @param merge whether to merge the tracking codes with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the tracking codes that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.TrackingCodes updateTrackingCodes(
        com.hp.omp.model.TrackingCodes trackingCodes, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trackingCodesLocalService.updateTrackingCodes(trackingCodes,
            merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _trackingCodesLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _trackingCodesLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _trackingCodesLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    public com.hp.omp.model.TrackingCodes getTrackingCodesByFields(
        com.hp.omp.model.TrackingCodes trackingCode)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trackingCodesLocalService.getTrackingCodesByFields(trackingCode);
    }

    public com.hp.omp.model.TrackingCodes getTrackingCodesByPRFields(
        com.hp.omp.model.PurchaseRequest pr)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trackingCodesLocalService.getTrackingCodesByPRFields(pr);
    }

    public com.hp.omp.model.TrackingCodes getTrackingCodeByName(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _trackingCodesLocalService.getTrackingCodeByName(name);
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public TrackingCodesLocalService getWrappedTrackingCodesLocalService() {
        return _trackingCodesLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedTrackingCodesLocalService(
        TrackingCodesLocalService trackingCodesLocalService) {
        _trackingCodesLocalService = trackingCodesLocalService;
    }

    public TrackingCodesLocalService getWrappedService() {
        return _trackingCodesLocalService;
    }

    public void setWrappedService(
        TrackingCodesLocalService trackingCodesLocalService) {
        _trackingCodesLocalService = trackingCodesLocalService;
    }

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<TrackingCodes> getTrackingCodesList(Integer pageNumber,
			Integer pageSize, String searchTCN) throws SystemException {
        return _trackingCodesLocalService.getTrackingCodesList(pageNumber,
                pageSize, searchTCN);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getTrackingCodesCountList(String searchTCN) throws SystemException {
		return _trackingCodesLocalService.getTrackingCodesCountList(searchTCN);
	}

	public void exportTrackingCodesAsExcel(
			List<TrackingCodes> listTrackingCodes, List<Object> header,
			OutputStream out) throws SystemException {
		_trackingCodesLocalService.exportTrackingCodesAsExcel(listTrackingCodes, header, out);
	}
}
