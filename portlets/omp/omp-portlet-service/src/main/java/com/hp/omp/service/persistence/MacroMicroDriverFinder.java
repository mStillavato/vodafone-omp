package com.hp.omp.service.persistence;

public interface MacroMicroDriverFinder {
    public java.util.List<com.hp.omp.model.MacroDriver> getMacroDriversByMicroDriverId(
        java.lang.Long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException;
}
