package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link MacroDriver}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       MacroDriver
 * @generated
 */
public class MacroDriverWrapper implements MacroDriver,
    ModelWrapper<MacroDriver> {
    private MacroDriver _macroDriver;

    public MacroDriverWrapper(MacroDriver macroDriver) {
        _macroDriver = macroDriver;
    }

    public Class<?> getModelClass() {
        return MacroDriver.class;
    }

    public String getModelClassName() {
        return MacroDriver.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("macroDriverId", getMacroDriverId());
        attributes.put("macroDriverName", getMacroDriverName());
        attributes.put("display", getDisplay());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long macroDriverId = (Long) attributes.get("macroDriverId");

        if (macroDriverId != null) {
            setMacroDriverId(macroDriverId);
        }

        String macroDriverName = (String) attributes.get("macroDriverName");

        if (macroDriverName != null) {
            setMacroDriverName(macroDriverName);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
    }

    /**
    * Returns the primary key of this macro driver.
    *
    * @return the primary key of this macro driver
    */
    public long getPrimaryKey() {
        return _macroDriver.getPrimaryKey();
    }

    /**
    * Sets the primary key of this macro driver.
    *
    * @param primaryKey the primary key of this macro driver
    */
    public void setPrimaryKey(long primaryKey) {
        _macroDriver.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the macro driver ID of this macro driver.
    *
    * @return the macro driver ID of this macro driver
    */
    public long getMacroDriverId() {
        return _macroDriver.getMacroDriverId();
    }

    /**
    * Sets the macro driver ID of this macro driver.
    *
    * @param macroDriverId the macro driver ID of this macro driver
    */
    public void setMacroDriverId(long macroDriverId) {
        _macroDriver.setMacroDriverId(macroDriverId);
    }

    /**
    * Returns the macro driver name of this macro driver.
    *
    * @return the macro driver name of this macro driver
    */
    public java.lang.String getMacroDriverName() {
        return _macroDriver.getMacroDriverName();
    }

    /**
    * Sets the macro driver name of this macro driver.
    *
    * @param macroDriverName the macro driver name of this macro driver
    */
    public void setMacroDriverName(java.lang.String macroDriverName) {
        _macroDriver.setMacroDriverName(macroDriverName);
    }

    /**
    * Returns the display of this macro driver.
    *
    * @return the display of this macro driver
    */
    public boolean getDisplay() {
        return _macroDriver.getDisplay();
    }

    /**
    * Returns <code>true</code> if this macro driver is display.
    *
    * @return <code>true</code> if this macro driver is display; <code>false</code> otherwise
    */
    public boolean isDisplay() {
        return _macroDriver.isDisplay();
    }

    /**
    * Sets whether this macro driver is display.
    *
    * @param display the display of this macro driver
    */
    public void setDisplay(boolean display) {
        _macroDriver.setDisplay(display);
    }

    public boolean isNew() {
        return _macroDriver.isNew();
    }

    public void setNew(boolean n) {
        _macroDriver.setNew(n);
    }

    public boolean isCachedModel() {
        return _macroDriver.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _macroDriver.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _macroDriver.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _macroDriver.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _macroDriver.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _macroDriver.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _macroDriver.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new MacroDriverWrapper((MacroDriver) _macroDriver.clone());
    }

    public int compareTo(MacroDriver macroDriver) {
        return _macroDriver.compareTo(macroDriver);
    }

    @Override
    public int hashCode() {
        return _macroDriver.hashCode();
    }

    public com.liferay.portal.model.CacheModel<MacroDriver> toCacheModel() {
        return _macroDriver.toCacheModel();
    }

    public MacroDriver toEscapedModel() {
        return new MacroDriverWrapper(_macroDriver.toEscapedModel());
    }

    public MacroDriver toUnescapedModel() {
        return new MacroDriverWrapper(_macroDriver.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _macroDriver.toString();
    }

    public java.lang.String toXmlString() {
        return _macroDriver.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _macroDriver.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof MacroDriverWrapper)) {
            return false;
        }

        MacroDriverWrapper macroDriverWrapper = (MacroDriverWrapper) obj;

        if (Validator.equals(_macroDriver, macroDriverWrapper._macroDriver)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public MacroDriver getWrappedMacroDriver() {
        return _macroDriver;
    }

    public MacroDriver getWrappedModel() {
        return _macroDriver;
    }

    public void resetOriginalValues() {
        _macroDriver.resetOriginalValues();
    }
}
