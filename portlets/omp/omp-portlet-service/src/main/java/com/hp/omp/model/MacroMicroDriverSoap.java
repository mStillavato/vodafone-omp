package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class MacroMicroDriverSoap implements Serializable {
    private long _macroMicroDriverId;
    private long _macroDriverId;
    private long _microDriverId;

    public MacroMicroDriverSoap() {
    }

    public static MacroMicroDriverSoap toSoapModel(MacroMicroDriver model) {
        MacroMicroDriverSoap soapModel = new MacroMicroDriverSoap();

        soapModel.setMacroMicroDriverId(model.getMacroMicroDriverId());
        soapModel.setMacroDriverId(model.getMacroDriverId());
        soapModel.setMicroDriverId(model.getMicroDriverId());

        return soapModel;
    }

    public static MacroMicroDriverSoap[] toSoapModels(MacroMicroDriver[] models) {
        MacroMicroDriverSoap[] soapModels = new MacroMicroDriverSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static MacroMicroDriverSoap[][] toSoapModels(
        MacroMicroDriver[][] models) {
        MacroMicroDriverSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new MacroMicroDriverSoap[models.length][models[0].length];
        } else {
            soapModels = new MacroMicroDriverSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static MacroMicroDriverSoap[] toSoapModels(
        List<MacroMicroDriver> models) {
        List<MacroMicroDriverSoap> soapModels = new ArrayList<MacroMicroDriverSoap>(models.size());

        for (MacroMicroDriver model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new MacroMicroDriverSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _macroMicroDriverId;
    }

    public void setPrimaryKey(long pk) {
        setMacroMicroDriverId(pk);
    }

    public long getMacroMicroDriverId() {
        return _macroMicroDriverId;
    }

    public void setMacroMicroDriverId(long macroMicroDriverId) {
        _macroMicroDriverId = macroMicroDriverId;
    }

    public long getMacroDriverId() {
        return _macroDriverId;
    }

    public void setMacroDriverId(long macroDriverId) {
        _macroDriverId = macroDriverId;
    }

    public long getMicroDriverId() {
        return _microDriverId;
    }

    public void setMicroDriverId(long microDriverId) {
        _microDriverId = microDriverId;
    }
}
