package com.hp.omp.service.persistence;

import com.hp.omp.model.SubProject;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the sub project service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see SubProjectPersistenceImpl
 * @see SubProjectUtil
 * @generated
 */
public interface SubProjectPersistence extends BasePersistence<SubProject> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link SubProjectUtil} to access the sub project persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the sub project in the entity cache if it is enabled.
    *
    * @param subProject the sub project
    */
    public void cacheResult(com.hp.omp.model.SubProject subProject);

    /**
    * Caches the sub projects in the entity cache if it is enabled.
    *
    * @param subProjects the sub projects
    */
    public void cacheResult(
        java.util.List<com.hp.omp.model.SubProject> subProjects);

    /**
    * Creates a new sub project with the primary key. Does not add the sub project to the database.
    *
    * @param subProjectId the primary key for the new sub project
    * @return the new sub project
    */
    public com.hp.omp.model.SubProject create(long subProjectId);

    /**
    * Removes the sub project with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param subProjectId the primary key of the sub project
    * @return the sub project that was removed
    * @throws com.hp.omp.NoSuchSubProjectException if a sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.SubProject remove(long subProjectId)
        throws com.hp.omp.NoSuchSubProjectException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.SubProject updateImpl(
        com.hp.omp.model.SubProject subProject, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the sub project with the primary key or throws a {@link com.hp.omp.NoSuchSubProjectException} if it could not be found.
    *
    * @param subProjectId the primary key of the sub project
    * @return the sub project
    * @throws com.hp.omp.NoSuchSubProjectException if a sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.SubProject findByPrimaryKey(long subProjectId)
        throws com.hp.omp.NoSuchSubProjectException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the sub project with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param subProjectId the primary key of the sub project
    * @return the sub project, or <code>null</code> if a sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.SubProject fetchByPrimaryKey(long subProjectId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the sub projects.
    *
    * @return the sub projects
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.SubProject> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the sub projects.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of sub projects
    * @param end the upper bound of the range of sub projects (not inclusive)
    * @return the range of sub projects
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.SubProject> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the sub projects.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of sub projects
    * @param end the upper bound of the range of sub projects (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of sub projects
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.SubProject> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the sub projects from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of sub projects.
    *
    * @return the number of sub projects
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
