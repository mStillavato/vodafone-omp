package com.hp.omp.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the macro driver local service. This utility wraps {@link com.hp.omp.service.impl.MacroDriverLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see MacroDriverLocalService
 * @see com.hp.omp.service.base.MacroDriverLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.MacroDriverLocalServiceImpl
 * @generated
 */
public class MacroDriverLocalServiceUtil {
    private static MacroDriverLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.hp.omp.service.impl.MacroDriverLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the macro driver to the database. Also notifies the appropriate model listeners.
    *
    * @param macroDriver the macro driver
    * @return the macro driver that was added
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroDriver addMacroDriver(
        com.hp.omp.model.MacroDriver macroDriver)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addMacroDriver(macroDriver);
    }

    /**
    * Creates a new macro driver with the primary key. Does not add the macro driver to the database.
    *
    * @param macroDriverId the primary key for the new macro driver
    * @return the new macro driver
    */
    public static com.hp.omp.model.MacroDriver createMacroDriver(
        long macroDriverId) {
        return getService().createMacroDriver(macroDriverId);
    }

    /**
    * Deletes the macro driver with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param macroDriverId the primary key of the macro driver
    * @return the macro driver that was removed
    * @throws PortalException if a macro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroDriver deleteMacroDriver(
        long macroDriverId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteMacroDriver(macroDriverId);
    }

    /**
    * Deletes the macro driver from the database. Also notifies the appropriate model listeners.
    *
    * @param macroDriver the macro driver
    * @return the macro driver that was removed
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroDriver deleteMacroDriver(
        com.hp.omp.model.MacroDriver macroDriver)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteMacroDriver(macroDriver);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    public static com.hp.omp.model.MacroDriver fetchMacroDriver(
        long macroDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchMacroDriver(macroDriverId);
    }

    /**
    * Returns the macro driver with the primary key.
    *
    * @param macroDriverId the primary key of the macro driver
    * @return the macro driver
    * @throws PortalException if a macro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroDriver getMacroDriver(
        long macroDriverId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getMacroDriver(macroDriverId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the macro drivers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of macro drivers
    * @param end the upper bound of the range of macro drivers (not inclusive)
    * @return the range of macro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MacroDriver> getMacroDrivers(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getMacroDrivers(start, end);
    }

    /**
    * Returns the number of macro drivers.
    *
    * @return the number of macro drivers
    * @throws SystemException if a system exception occurred
    */
    public static int getMacroDriversCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getMacroDriversCount();
    }

    /**
    * Updates the macro driver in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param macroDriver the macro driver
    * @return the macro driver that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroDriver updateMacroDriver(
        com.hp.omp.model.MacroDriver macroDriver)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateMacroDriver(macroDriver);
    }

    /**
    * Updates the macro driver in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param macroDriver the macro driver
    * @param merge whether to merge the macro driver with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the macro driver that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroDriver updateMacroDriver(
        com.hp.omp.model.MacroDriver macroDriver, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateMacroDriver(macroDriver, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static com.hp.omp.model.MacroDriver getMacroDriverByName(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getMacroDriverByName(name);
    }

    public static void clearService() {
        _service = null;
    }

    public static MacroDriverLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    MacroDriverLocalService.class.getName());

            if (invokableLocalService instanceof MacroDriverLocalService) {
                _service = (MacroDriverLocalService) invokableLocalService;
            } else {
                _service = new MacroDriverLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(MacroDriverLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated
     */
    public void setService(MacroDriverLocalService service) {
    }
    
    public static java.util.List<com.hp.omp.model.MacroDriver> getMacroDrivers(
            int start, int end, String searchFilter)
            throws com.liferay.portal.kernel.exception.SystemException {
            return getService().getMacroDrivers(start, end, searchFilter);
        }
}
