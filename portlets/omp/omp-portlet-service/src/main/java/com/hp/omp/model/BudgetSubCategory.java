package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the BudgetSubCategory service. Represents a row in the &quot;BUDGET_SUB_CATEGORY&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see BudgetSubCategoryModel
 * @see com.hp.omp.model.impl.BudgetSubCategoryImpl
 * @see com.hp.omp.model.impl.BudgetSubCategoryModelImpl
 * @generated
 */
public interface BudgetSubCategory extends BudgetSubCategoryModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.BudgetSubCategoryImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
