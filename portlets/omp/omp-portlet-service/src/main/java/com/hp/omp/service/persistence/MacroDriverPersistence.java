package com.hp.omp.service.persistence;

import com.hp.omp.model.MacroDriver;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the macro driver service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see MacroDriverPersistenceImpl
 * @see MacroDriverUtil
 * @generated
 */
public interface MacroDriverPersistence extends BasePersistence<MacroDriver> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link MacroDriverUtil} to access the macro driver persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the macro driver in the entity cache if it is enabled.
    *
    * @param macroDriver the macro driver
    */
    public void cacheResult(com.hp.omp.model.MacroDriver macroDriver);

    /**
    * Caches the macro drivers in the entity cache if it is enabled.
    *
    * @param macroDrivers the macro drivers
    */
    public void cacheResult(
        java.util.List<com.hp.omp.model.MacroDriver> macroDrivers);

    /**
    * Creates a new macro driver with the primary key. Does not add the macro driver to the database.
    *
    * @param macroDriverId the primary key for the new macro driver
    * @return the new macro driver
    */
    public com.hp.omp.model.MacroDriver create(long macroDriverId);

    /**
    * Removes the macro driver with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param macroDriverId the primary key of the macro driver
    * @return the macro driver that was removed
    * @throws com.hp.omp.NoSuchMacroDriverException if a macro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroDriver remove(long macroDriverId)
        throws com.hp.omp.NoSuchMacroDriverException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.MacroDriver updateImpl(
        com.hp.omp.model.MacroDriver macroDriver, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the macro driver with the primary key or throws a {@link com.hp.omp.NoSuchMacroDriverException} if it could not be found.
    *
    * @param macroDriverId the primary key of the macro driver
    * @return the macro driver
    * @throws com.hp.omp.NoSuchMacroDriverException if a macro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroDriver findByPrimaryKey(long macroDriverId)
        throws com.hp.omp.NoSuchMacroDriverException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the macro driver with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param macroDriverId the primary key of the macro driver
    * @return the macro driver, or <code>null</code> if a macro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroDriver fetchByPrimaryKey(long macroDriverId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the macro drivers.
    *
    * @return the macro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MacroDriver> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the macro drivers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of macro drivers
    * @param end the upper bound of the range of macro drivers (not inclusive)
    * @return the range of macro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MacroDriver> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the macro drivers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of macro drivers
    * @param end the upper bound of the range of macro drivers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of macro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MacroDriver> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the macro drivers from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of macro drivers.
    *
    * @return the number of macro drivers
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
