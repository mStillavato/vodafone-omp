package com.hp.omp.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.BaseLocalService;
import com.liferay.portal.service.InvokableLocalService;
import com.liferay.portal.service.PersistedModelLocalService;

/**
 * The interface for the o d n p name local service.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see ODNPNameLocalServiceUtil
 * @see com.hp.omp.service.base.ODNPNameLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.ODNPNameLocalServiceImpl
 * @generated
 */
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
    PortalException.class, SystemException.class}
)
public interface ODNPNameLocalService extends BaseLocalService,
    InvokableLocalService, PersistedModelLocalService {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link ODNPNameLocalServiceUtil} to access the o d n p name local service. Add custom service methods to {@link com.hp.omp.service.impl.ODNPNameLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */

    /**
    * Adds the o d n p name to the database. Also notifies the appropriate model listeners.
    *
    * @param odnpName the o d n p name
    * @return the o d n p name that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ODNPName addODNPName(
        com.hp.omp.model.ODNPName odnpName)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Creates a new o d n p name with the primary key. Does not add the o d n p name to the database.
    *
    * @param odnpNameId the primary key for the new o d n p name
    * @return the new o d n p name
    */
    public com.hp.omp.model.ODNPName createODNPName(long odnpNameId);

    /**
    * Deletes the o d n p name with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param odnpNameId the primary key of the o d n p name
    * @return the o d n p name that was removed
    * @throws PortalException if a o d n p name with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ODNPName deleteODNPName(long odnpNameId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Deletes the o d n p name from the database. Also notifies the appropriate model listeners.
    *
    * @param odnpName the o d n p name
    * @return the o d n p name that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ODNPName deleteODNPName(
        com.hp.omp.model.ODNPName odnpName)
        throws com.liferay.portal.kernel.exception.SystemException;

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery();

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public com.hp.omp.model.ODNPName fetchODNPName(long odnpNameId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the o d n p name with the primary key.
    *
    * @param odnpNameId the primary key of the o d n p name
    * @return the o d n p name
    * @throws PortalException if a o d n p name with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public com.hp.omp.model.ODNPName getODNPName(long odnpNameId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the o d n p names.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of o d n p names
    * @param end the upper bound of the range of o d n p names (not inclusive)
    * @return the range of o d n p names
    * @throws SystemException if a system exception occurred
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.hp.omp.model.ODNPName> getODNPNames(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of o d n p names.
    *
    * @return the number of o d n p names
    * @throws SystemException if a system exception occurred
    */
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public int getODNPNamesCount()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Updates the o d n p name in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param odnpName the o d n p name
    * @return the o d n p name that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ODNPName updateODNPName(
        com.hp.omp.model.ODNPName odnpName)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Updates the o d n p name in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param odnpName the o d n p name
    * @param merge whether to merge the o d n p name with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the o d n p name that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ODNPName updateODNPName(
        com.hp.omp.model.ODNPName odnpName, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier();

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier);

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public com.hp.omp.model.ODNPName getOdnpNameByName(java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException;
    
    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<com.hp.omp.model.ODNPName> getODNPNamesList(int start,
        int end, String searchFilter) throws com.liferay.portal.kernel.exception.SystemException;
}
