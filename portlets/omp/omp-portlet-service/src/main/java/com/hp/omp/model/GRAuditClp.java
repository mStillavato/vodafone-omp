package com.hp.omp.model;

import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.GRAuditLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class GRAuditClp extends BaseModelImpl<GRAudit> implements GRAudit {
    private long _auditId;
    private Long _goodReceiptId;
    private int _status;
    private Long _userId;
    private Date _changeDate;
    private BaseModel<?> _grAuditRemoteModel;

    public GRAuditClp() {
    }

    public Class<?> getModelClass() {
        return GRAudit.class;
    }

    public String getModelClassName() {
        return GRAudit.class.getName();
    }

    public long getPrimaryKey() {
        return _auditId;
    }

    public void setPrimaryKey(long primaryKey) {
        setAuditId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_auditId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("auditId", getAuditId());
        attributes.put("goodReceiptId", getGoodReceiptId());
        attributes.put("status", getStatus());
        attributes.put("userId", getUserId());
        attributes.put("changeDate", getChangeDate());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long auditId = (Long) attributes.get("auditId");

        if (auditId != null) {
            setAuditId(auditId);
        }

        Long goodReceiptId = (Long) attributes.get("goodReceiptId");

        if (goodReceiptId != null) {
            setGoodReceiptId(goodReceiptId);
        }

        Integer status = (Integer) attributes.get("status");

        if (status != null) {
            setStatus(status);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        Date changeDate = (Date) attributes.get("changeDate");

        if (changeDate != null) {
            setChangeDate(changeDate);
        }
    }

    public long getAuditId() {
        return _auditId;
    }

    public void setAuditId(long auditId) {
        _auditId = auditId;

        if (_grAuditRemoteModel != null) {
            try {
                Class<?> clazz = _grAuditRemoteModel.getClass();

                Method method = clazz.getMethod("setAuditId", long.class);

                method.invoke(_grAuditRemoteModel, auditId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public Long getGoodReceiptId() {
        return _goodReceiptId;
    }

    public void setGoodReceiptId(Long goodReceiptId) {
        _goodReceiptId = goodReceiptId;

        if (_grAuditRemoteModel != null) {
            try {
                Class<?> clazz = _grAuditRemoteModel.getClass();

                Method method = clazz.getMethod("setGoodReceiptId", Long.class);

                method.invoke(_grAuditRemoteModel, goodReceiptId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public int getStatus() {
        return _status;
    }

    public void setStatus(int status) {
        _status = status;

        if (_grAuditRemoteModel != null) {
            try {
                Class<?> clazz = _grAuditRemoteModel.getClass();

                Method method = clazz.getMethod("setStatus", int.class);

                method.invoke(_grAuditRemoteModel, status);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public Long getUserId() {
        return _userId;
    }

    public void setUserId(Long userId) {
        _userId = userId;

        if (_grAuditRemoteModel != null) {
            try {
                Class<?> clazz = _grAuditRemoteModel.getClass();

                Method method = clazz.getMethod("setUserId", Long.class);

                method.invoke(_grAuditRemoteModel, userId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public Date getChangeDate() {
        return _changeDate;
    }

    public void setChangeDate(Date changeDate) {
        _changeDate = changeDate;

        if (_grAuditRemoteModel != null) {
            try {
                Class<?> clazz = _grAuditRemoteModel.getClass();

                Method method = clazz.getMethod("setChangeDate", Date.class);

                method.invoke(_grAuditRemoteModel, changeDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getGRAuditRemoteModel() {
        return _grAuditRemoteModel;
    }

    public void setGRAuditRemoteModel(BaseModel<?> grAuditRemoteModel) {
        _grAuditRemoteModel = grAuditRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _grAuditRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_grAuditRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            GRAuditLocalServiceUtil.addGRAudit(this);
        } else {
            GRAuditLocalServiceUtil.updateGRAudit(this);
        }
    }

    @Override
    public GRAudit toEscapedModel() {
        return (GRAudit) ProxyUtil.newProxyInstance(GRAudit.class.getClassLoader(),
            new Class[] { GRAudit.class }, new AutoEscapeBeanHandler(this));
    }

    public GRAudit toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        GRAuditClp clone = new GRAuditClp();

        clone.setAuditId(getAuditId());
        clone.setGoodReceiptId(getGoodReceiptId());
        clone.setStatus(getStatus());
        clone.setUserId(getUserId());
        clone.setChangeDate(getChangeDate());

        return clone;
    }

    public int compareTo(GRAudit grAudit) {
        long primaryKey = grAudit.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof GRAuditClp)) {
            return false;
        }

        GRAuditClp grAudit = (GRAuditClp) obj;

        long primaryKey = grAudit.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{auditId=");
        sb.append(getAuditId());
        sb.append(", goodReceiptId=");
        sb.append(getGoodReceiptId());
        sb.append(", status=");
        sb.append(getStatus());
        sb.append(", userId=");
        sb.append(getUserId());
        sb.append(", changeDate=");
        sb.append(getChangeDate());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(19);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.GRAudit");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>auditId</column-name><column-value><![CDATA[");
        sb.append(getAuditId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>goodReceiptId</column-name><column-value><![CDATA[");
        sb.append(getGoodReceiptId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>status</column-name><column-value><![CDATA[");
        sb.append(getStatus());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>userId</column-name><column-value><![CDATA[");
        sb.append(getUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>changeDate</column-name><column-value><![CDATA[");
        sb.append(getChangeDate());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
