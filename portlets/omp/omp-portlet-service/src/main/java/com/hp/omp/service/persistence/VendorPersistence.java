package com.hp.omp.service.persistence;

import com.hp.omp.model.Vendor;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the vendor service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see VendorPersistenceImpl
 * @see VendorUtil
 * @generated
 */
public interface VendorPersistence extends BasePersistence<Vendor> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link VendorUtil} to access the vendor persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the vendor in the entity cache if it is enabled.
    *
    * @param vendor the vendor
    */
    public void cacheResult(com.hp.omp.model.Vendor vendor);

    /**
    * Caches the vendors in the entity cache if it is enabled.
    *
    * @param vendors the vendors
    */
    public void cacheResult(java.util.List<com.hp.omp.model.Vendor> vendors);

    /**
    * Creates a new vendor with the primary key. Does not add the vendor to the database.
    *
    * @param vendorId the primary key for the new vendor
    * @return the new vendor
    */
    public com.hp.omp.model.Vendor create(long vendorId);

    /**
    * Removes the vendor with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vendorId the primary key of the vendor
    * @return the vendor that was removed
    * @throws com.hp.omp.NoSuchVendorException if a vendor with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Vendor remove(long vendorId)
        throws com.hp.omp.NoSuchVendorException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.Vendor updateImpl(com.hp.omp.model.Vendor vendor,
        boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vendor with the primary key or throws a {@link com.hp.omp.NoSuchVendorException} if it could not be found.
    *
    * @param vendorId the primary key of the vendor
    * @return the vendor
    * @throws com.hp.omp.NoSuchVendorException if a vendor with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Vendor findByPrimaryKey(long vendorId)
        throws com.hp.omp.NoSuchVendorException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vendor with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param vendorId the primary key of the vendor
    * @return the vendor, or <code>null</code> if a vendor with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Vendor fetchByPrimaryKey(long vendorId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vendors.
    *
    * @return the vendors
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.Vendor> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vendors.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of vendors
    * @param end the upper bound of the range of vendors (not inclusive)
    * @return the range of vendors
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.Vendor> findAll(int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vendors.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of vendors
    * @param end the upper bound of the range of vendors (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of vendors
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.Vendor> findAll(int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the vendors from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vendors.
    *
    * @return the number of vendors
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
