package com.hp.omp.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the o d n p code local service. This utility wraps {@link com.hp.omp.service.impl.ODNPCodeLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see ODNPCodeLocalService
 * @see com.hp.omp.service.base.ODNPCodeLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.ODNPCodeLocalServiceImpl
 * @generated
 */
public class ODNPCodeLocalServiceUtil {
    private static ODNPCodeLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.hp.omp.service.impl.ODNPCodeLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the o d n p code to the database. Also notifies the appropriate model listeners.
    *
    * @param odnpCode the o d n p code
    * @return the o d n p code that was added
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ODNPCode addODNPCode(
        com.hp.omp.model.ODNPCode odnpCode)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addODNPCode(odnpCode);
    }

    /**
    * Creates a new o d n p code with the primary key. Does not add the o d n p code to the database.
    *
    * @param odnpCodeId the primary key for the new o d n p code
    * @return the new o d n p code
    */
    public static com.hp.omp.model.ODNPCode createODNPCode(long odnpCodeId) {
        return getService().createODNPCode(odnpCodeId);
    }

    /**
    * Deletes the o d n p code with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param odnpCodeId the primary key of the o d n p code
    * @return the o d n p code that was removed
    * @throws PortalException if a o d n p code with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ODNPCode deleteODNPCode(long odnpCodeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteODNPCode(odnpCodeId);
    }

    /**
    * Deletes the o d n p code from the database. Also notifies the appropriate model listeners.
    *
    * @param odnpCode the o d n p code
    * @return the o d n p code that was removed
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ODNPCode deleteODNPCode(
        com.hp.omp.model.ODNPCode odnpCode)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteODNPCode(odnpCode);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    public static com.hp.omp.model.ODNPCode fetchODNPCode(long odnpCodeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchODNPCode(odnpCodeId);
    }

    /**
    * Returns the o d n p code with the primary key.
    *
    * @param odnpCodeId the primary key of the o d n p code
    * @return the o d n p code
    * @throws PortalException if a o d n p code with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ODNPCode getODNPCode(long odnpCodeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getODNPCode(odnpCodeId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the o d n p codes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of o d n p codes
    * @param end the upper bound of the range of o d n p codes (not inclusive)
    * @return the range of o d n p codes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ODNPCode> getODNPCodes(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getODNPCodes(start, end);
    }

    /**
    * Returns the number of o d n p codes.
    *
    * @return the number of o d n p codes
    * @throws SystemException if a system exception occurred
    */
    public static int getODNPCodesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getODNPCodesCount();
    }

    /**
    * Updates the o d n p code in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param odnpCode the o d n p code
    * @return the o d n p code that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ODNPCode updateODNPCode(
        com.hp.omp.model.ODNPCode odnpCode)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateODNPCode(odnpCode);
    }

    /**
    * Updates the o d n p code in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param odnpCode the o d n p code
    * @param merge whether to merge the o d n p code with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the o d n p code that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ODNPCode updateODNPCode(
        com.hp.omp.model.ODNPCode odnpCode, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateODNPCode(odnpCode, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static void clearService() {
        _service = null;
    }

    public static ODNPCodeLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    ODNPCodeLocalService.class.getName());

            if (invokableLocalService instanceof ODNPCodeLocalService) {
                _service = (ODNPCodeLocalService) invokableLocalService;
            } else {
                _service = new ODNPCodeLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(ODNPCodeLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated
     */
    public void setService(ODNPCodeLocalService service) {
    }
}
