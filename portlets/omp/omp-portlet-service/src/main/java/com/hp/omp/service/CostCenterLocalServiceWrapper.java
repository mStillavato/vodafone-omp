package com.hp.omp.service;

import java.util.List;

import com.hp.omp.model.CostCenter;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link CostCenterLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       CostCenterLocalService
 * @generated
 */
public class CostCenterLocalServiceWrapper implements CostCenterLocalService,
    ServiceWrapper<CostCenterLocalService> {
    private CostCenterLocalService _costCenterLocalService;

    public CostCenterLocalServiceWrapper(
        CostCenterLocalService costCenterLocalService) {
        _costCenterLocalService = costCenterLocalService;
    }

    /**
    * Adds the cost center to the database. Also notifies the appropriate model listeners.
    *
    * @param costCenter the cost center
    * @return the cost center that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.CostCenter addCostCenter(
        com.hp.omp.model.CostCenter costCenter)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterLocalService.addCostCenter(costCenter);
    }

    /**
    * Creates a new cost center with the primary key. Does not add the cost center to the database.
    *
    * @param costCenterId the primary key for the new cost center
    * @return the new cost center
    */
    public com.hp.omp.model.CostCenter createCostCenter(long costCenterId) {
        return _costCenterLocalService.createCostCenter(costCenterId);
    }

    /**
    * Deletes the cost center with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param costCenterId the primary key of the cost center
    * @return the cost center that was removed
    * @throws PortalException if a cost center with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.CostCenter deleteCostCenter(long costCenterId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _costCenterLocalService.deleteCostCenter(costCenterId);
    }

    /**
    * Deletes the cost center from the database. Also notifies the appropriate model listeners.
    *
    * @param costCenter the cost center
    * @return the cost center that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.CostCenter deleteCostCenter(
        com.hp.omp.model.CostCenter costCenter)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterLocalService.deleteCostCenter(costCenter);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _costCenterLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterLocalService.dynamicQueryCount(dynamicQuery);
    }

    public com.hp.omp.model.CostCenter fetchCostCenter(long costCenterId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterLocalService.fetchCostCenter(costCenterId);
    }

    /**
    * Returns the cost center with the primary key.
    *
    * @param costCenterId the primary key of the cost center
    * @return the cost center
    * @throws PortalException if a cost center with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.CostCenter getCostCenter(long costCenterId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _costCenterLocalService.getCostCenter(costCenterId);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _costCenterLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the cost centers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of cost centers
    * @param end the upper bound of the range of cost centers (not inclusive)
    * @return the range of cost centers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.CostCenter> getCostCenters(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterLocalService.getCostCenters(start, end);
    }

    /**
    * Returns the number of cost centers.
    *
    * @return the number of cost centers
    * @throws SystemException if a system exception occurred
    */
    public int getCostCentersCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterLocalService.getCostCentersCount();
    }

    /**
    * Updates the cost center in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param costCenter the cost center
    * @return the cost center that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.CostCenter updateCostCenter(
        com.hp.omp.model.CostCenter costCenter)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterLocalService.updateCostCenter(costCenter);
    }

    /**
    * Updates the cost center in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param costCenter the cost center
    * @param merge whether to merge the cost center with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the cost center that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.CostCenter updateCostCenter(
        com.hp.omp.model.CostCenter costCenter, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterLocalService.updateCostCenter(costCenter, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _costCenterLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _costCenterLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _costCenterLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    public com.hp.omp.model.CostCenter getCostCenterByName(
        java.lang.String name, int gruppoUsers)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterLocalService.getCostCenterByName(name, gruppoUsers);
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public CostCenterLocalService getWrappedCostCenterLocalService() {
        return _costCenterLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedCostCenterLocalService(
        CostCenterLocalService costCenterLocalService) {
        _costCenterLocalService = costCenterLocalService;
    }

    public CostCenterLocalService getWrappedService() {
        return _costCenterLocalService;
    }

    public void setWrappedService(CostCenterLocalService costCenterLocalService) {
        _costCenterLocalService = costCenterLocalService;
    }

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<CostCenter> getCostCentersList(int start, int end,
			String searchFilter, int gruppoUsers) throws SystemException {
		return _costCenterLocalService.getCostCentersList(start, end, searchFilter, gruppoUsers);
	}
}
