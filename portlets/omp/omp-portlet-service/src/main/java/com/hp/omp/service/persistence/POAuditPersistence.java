package com.hp.omp.service.persistence;

import com.hp.omp.model.POAudit;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the p o audit service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see POAuditPersistenceImpl
 * @see POAuditUtil
 * @generated
 */
public interface POAuditPersistence extends BasePersistence<POAudit> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link POAuditUtil} to access the p o audit persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the p o audit in the entity cache if it is enabled.
    *
    * @param poAudit the p o audit
    */
    public void cacheResult(com.hp.omp.model.POAudit poAudit);

    /**
    * Caches the p o audits in the entity cache if it is enabled.
    *
    * @param poAudits the p o audits
    */
    public void cacheResult(java.util.List<com.hp.omp.model.POAudit> poAudits);

    /**
    * Creates a new p o audit with the primary key. Does not add the p o audit to the database.
    *
    * @param auditId the primary key for the new p o audit
    * @return the new p o audit
    */
    public com.hp.omp.model.POAudit create(long auditId);

    /**
    * Removes the p o audit with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param auditId the primary key of the p o audit
    * @return the p o audit that was removed
    * @throws com.hp.omp.NoSuchPOAuditException if a p o audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.POAudit remove(long auditId)
        throws com.hp.omp.NoSuchPOAuditException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.POAudit updateImpl(
        com.hp.omp.model.POAudit poAudit, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the p o audit with the primary key or throws a {@link com.hp.omp.NoSuchPOAuditException} if it could not be found.
    *
    * @param auditId the primary key of the p o audit
    * @return the p o audit
    * @throws com.hp.omp.NoSuchPOAuditException if a p o audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.POAudit findByPrimaryKey(long auditId)
        throws com.hp.omp.NoSuchPOAuditException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the p o audit with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param auditId the primary key of the p o audit
    * @return the p o audit, or <code>null</code> if a p o audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.POAudit fetchByPrimaryKey(long auditId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the p o audits.
    *
    * @return the p o audits
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.POAudit> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the p o audits.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of p o audits
    * @param end the upper bound of the range of p o audits (not inclusive)
    * @return the range of p o audits
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.POAudit> findAll(int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the p o audits.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of p o audits
    * @param end the upper bound of the range of p o audits (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of p o audits
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.POAudit> findAll(int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the p o audits from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of p o audits.
    *
    * @return the number of p o audits
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
