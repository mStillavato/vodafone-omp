package com.hp.omp.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the good receipt local service. This utility wraps {@link com.hp.omp.service.impl.GoodReceiptLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see GoodReceiptLocalService
 * @see com.hp.omp.service.base.GoodReceiptLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.GoodReceiptLocalServiceImpl
 * @generated
 */
public class GoodReceiptLocalServiceUtil {
    private static GoodReceiptLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.hp.omp.service.impl.GoodReceiptLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the good receipt to the database. Also notifies the appropriate model listeners.
    *
    * @param goodReceipt the good receipt
    * @return the good receipt that was added
    * @throws SystemException if a system exception occurred
     * @throws PortalException 
    */
    public static com.hp.omp.model.GoodReceipt addGoodReceipt(
        com.hp.omp.model.GoodReceipt goodReceipt)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addGoodReceipt(goodReceipt);
    }

    /**
    * Creates a new good receipt with the primary key. Does not add the good receipt to the database.
    *
    * @param goodReceiptId the primary key for the new good receipt
    * @return the new good receipt
    */
    public static com.hp.omp.model.GoodReceipt createGoodReceipt(
        long goodReceiptId) {
        return getService().createGoodReceipt(goodReceiptId);
    }

    /**
    * Deletes the good receipt with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param goodReceiptId the primary key of the good receipt
    * @return the good receipt that was removed
    * @throws PortalException if a good receipt with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.GoodReceipt deleteGoodReceipt(
        long goodReceiptId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteGoodReceipt(goodReceiptId);
    }

    /**
    * Deletes the good receipt from the database. Also notifies the appropriate model listeners.
    *
    * @param goodReceipt the good receipt
    * @return the good receipt that was removed
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.GoodReceipt deleteGoodReceipt(
        com.hp.omp.model.GoodReceipt goodReceipt)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteGoodReceipt(goodReceipt);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    public static com.hp.omp.model.GoodReceipt fetchGoodReceipt(
        long goodReceiptId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchGoodReceipt(goodReceiptId);
    }

    /**
    * Returns the good receipt with the primary key.
    *
    * @param goodReceiptId the primary key of the good receipt
    * @return the good receipt
    * @throws PortalException if a good receipt with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.GoodReceipt getGoodReceipt(
        long goodReceiptId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getGoodReceipt(goodReceiptId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the good receipts.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of good receipts
    * @param end the upper bound of the range of good receipts (not inclusive)
    * @return the range of good receipts
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.GoodReceipt> getGoodReceipts(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getGoodReceipts(start, end);
    }

    /**
    * Returns the number of good receipts.
    *
    * @return the number of good receipts
    * @throws SystemException if a system exception occurred
    */
    public static int getGoodReceiptsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getGoodReceiptsCount();
    }

    /**
    * Updates the good receipt in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param goodReceipt the good receipt
    * @return the good receipt that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.GoodReceipt updateGoodReceipt(
        com.hp.omp.model.GoodReceipt goodReceipt)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateGoodReceipt(goodReceipt);
    }
    
    /**
    * Updates the good receipt in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param goodReceipt the good receipt
    * @param merge whether to merge the good receipt with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the good receipt that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.GoodReceipt updateGoodReceipt(
        com.hp.omp.model.GoodReceipt goodReceipt, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateGoodReceipt(goodReceipt, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static java.util.List<com.hp.omp.model.GoodReceipt> getPositionGoodReceipts(
        java.lang.String positionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPositionGoodReceipts(positionId);
    }

    public static java.util.List<java.util.Date> getPositionMaxRegisterDateGoodReceipts(
        java.lang.String positionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPositionMaxRegisterDateGoodReceipts(positionId);
    }

    public static java.lang.Double getSumGRPercentageToPosition(
        java.lang.String positionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getSumGRPercentageToPosition(positionId);
    }

    public static java.lang.Long getOpenedGoodReceiptByCostCenterCount(
        long costCenterId) {
        return getService().getOpenedGoodReceiptByCostCenterCount(costCenterId);
    }

    public static java.lang.Long getOpenedGoodReceiptCount() {
        return getService().getOpenedGoodReceiptCount();
    }

    public static java.util.List<com.hp.omp.model.custom.ListDTO> getOpenedGoodReceiptByCostCenter(
        long costCenterId, int start, int end) {
        return getService()
                   .getOpenedGoodReceiptByCostCenter(costCenterId, start, end);
    }

    public static java.util.List<com.hp.omp.model.custom.ListDTO> getOpenedGoodReceipt(
        int start, int end) {
        return getService().getOpenedGoodReceipt(start, end);
    }

    public static java.lang.Long getPositionGRCount(java.lang.String positionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPositionGRCount(positionId);
    }

    public static java.lang.Long getPositionClosedGRCount(
        java.lang.String positionId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPositionClosedGRCount(positionId);
    }
    
    public static com.hp.omp.model.GoodReceipt updatePoStatus(
            com.hp.omp.model.GoodReceipt goodReceipt)
            throws com.liferay.portal.kernel.exception.SystemException {
            return getService().updatePoStatus(goodReceipt);
    }

    public static void clearService() {
        _service = null;
    }

    public static GoodReceiptLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    GoodReceiptLocalService.class.getName());

            if (invokableLocalService instanceof GoodReceiptLocalService) {
                _service = (GoodReceiptLocalService) invokableLocalService;
            } else {
                _service = new GoodReceiptLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(GoodReceiptLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated
     */
    public void setService(GoodReceiptLocalService service) {
    }
}
