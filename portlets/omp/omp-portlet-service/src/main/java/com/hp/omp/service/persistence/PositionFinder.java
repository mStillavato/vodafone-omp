package com.hp.omp.service.persistence;

public interface PositionFinder {
    public java.util.List<java.lang.String> getAllFiscalYears()
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.util.List<java.lang.String> getAllWBSCodes()
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.lang.Long getIAndCPositionsCount(
        java.lang.String purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException;
}
