package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Position}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       Position
 * @generated
 */
public class PositionWrapper implements Position, ModelWrapper<Position> {
    private Position _position;

    public PositionWrapper(Position position) {
        _position = position;
    }

    public Class<?> getModelClass() {
        return Position.class;
    }

    public String getModelClassName() {
        return Position.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("positionId", getPositionId());
        attributes.put("fiscalYear", getFiscalYear());
        attributes.put("description", getDescription());
        attributes.put("categoryCode", getCategoryCode());
        attributes.put("offerNumber", getOfferNumber());
        attributes.put("offerDate", getOfferDate());
        attributes.put("quatity", getQuatity());
        attributes.put("deliveryDate", getDeliveryDate());
        attributes.put("deliveryAddress", getDeliveryAddress());
        attributes.put("actualUnitCost", getActualUnitCost());
        attributes.put("numberOfUnit", getNumberOfUnit());
        attributes.put("labelNumber", getLabelNumber());
        attributes.put("poLabelNumber", getPoLabelNumber());
        attributes.put("targaTecnica", getTargaTecnica());
        attributes.put("approved", getApproved());
        attributes.put("icApproval", getIcApproval());
        attributes.put("approver", getApprover());
        attributes.put("assetNumber", getAssetNumber());
        attributes.put("shoppingCart", getShoppingCart());
        attributes.put("poNumber", getPoNumber());
        attributes.put("evoLocation", getEvoLocation());
        attributes.put("wbsCodeId", getWbsCodeId());
        attributes.put("purchaseRequestId", getPurchaseRequestId());
        attributes.put("purchaseOrderId", getPurchaseOrderId());
        attributes.put("createdDate", getCreatedDate());
        attributes.put("createdUserId", getCreatedUserId());
        attributes.put("evoCodMateriale", getEvoCodMateriale());
        attributes.put("evoDesMateriale", getEvoDesMateriale());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long positionId = (Long) attributes.get("positionId");

        if (positionId != null) {
            setPositionId(positionId);
        }

        String fiscalYear = (String) attributes.get("fiscalYear");

        if (fiscalYear != null) {
            setFiscalYear(fiscalYear);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        String categoryCode = (String) attributes.get("categoryCode");

        if (categoryCode != null) {
            setCategoryCode(categoryCode);
        }

        String offerNumber = (String) attributes.get("offerNumber");

        if (offerNumber != null) {
            setOfferNumber(offerNumber);
        }

        Date offerDate = (Date) attributes.get("offerDate");

        if (offerDate != null) {
            setOfferDate(offerDate);
        }

        String quatity = (String) attributes.get("quatity");

        if (quatity != null) {
            setQuatity(quatity);
        }

        Date deliveryDate = (Date) attributes.get("deliveryDate");

        if (deliveryDate != null) {
            setDeliveryDate(deliveryDate);
        }

        String deliveryAddress = (String) attributes.get("deliveryAddress");

        if (deliveryAddress != null) {
            setDeliveryAddress(deliveryAddress);
        }

        String actualUnitCost = (String) attributes.get("actualUnitCost");

        if (actualUnitCost != null) {
            setActualUnitCost(actualUnitCost);
        }

        String numberOfUnit = (String) attributes.get("numberOfUnit");

        if (numberOfUnit != null) {
            setNumberOfUnit(numberOfUnit);
        }

        Long labelNumber = (Long) attributes.get("labelNumber");

        if (labelNumber != null) {
            setLabelNumber(labelNumber);
        }

        Long poLabelNumber = (Long) attributes.get("poLabelNumber");

        if (poLabelNumber != null) {
            setPoLabelNumber(poLabelNumber);
        }

        String targaTecnica = (String) attributes.get("targaTecnica");

        if (targaTecnica != null) {
            setTargaTecnica(targaTecnica);
        }

        Boolean approved = (Boolean) attributes.get("approved");

        if (approved != null) {
            setApproved(approved);
        }

        String icApproval = (String) attributes.get("icApproval");

        if (icApproval != null) {
            setIcApproval(icApproval);
        }

        String approver = (String) attributes.get("approver");

        if (approver != null) {
            setApprover(approver);
        }

        String assetNumber = (String) attributes.get("assetNumber");

        if (assetNumber != null) {
            setAssetNumber(assetNumber);
        }

        String shoppingCart = (String) attributes.get("shoppingCart");

        if (shoppingCart != null) {
            setShoppingCart(shoppingCart);
        }

        String poNumber = (String) attributes.get("poNumber");

        if (poNumber != null) {
            setPoNumber(poNumber);
        }

        String evoLocation = (String) attributes.get("evoLocation");

        if (evoLocation != null) {
            setEvoLocation(evoLocation);
        }

        String evoCodMateriale = (String) attributes.get("evoCodMateriale");

        if (evoCodMateriale != null) {
            setEvoCodMateriale(evoCodMateriale);
        }
        
        String evoDesMateriale = (String) attributes.get("evoDesMateriale");

        if (evoDesMateriale != null) {
            setEvoDesMateriale(evoDesMateriale);
        }

        String wbsCodeId = (String) attributes.get("wbsCodeId");

        if (wbsCodeId != null) {
            setWbsCodeId(wbsCodeId);
        }

        String purchaseRequestId = (String) attributes.get("purchaseRequestId");

        if (purchaseRequestId != null) {
            setPurchaseRequestId(purchaseRequestId);
        }

        String purchaseOrderId = (String) attributes.get("purchaseOrderId");

        if (purchaseOrderId != null) {
            setPurchaseOrderId(purchaseOrderId);
        }

        Date createdDate = (Date) attributes.get("createdDate");

        if (createdDate != null) {
            setCreatedDate(createdDate);
        }

        Long createdUserId = (Long) attributes.get("createdUserId");

        if (createdUserId != null) {
            setCreatedUserId(createdUserId);
        }
    }

    /**
    * Returns the primary key of this position.
    *
    * @return the primary key of this position
    */
    public long getPrimaryKey() {
        return _position.getPrimaryKey();
    }

    /**
    * Sets the primary key of this position.
    *
    * @param primaryKey the primary key of this position
    */
    public void setPrimaryKey(long primaryKey) {
        _position.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the position ID of this position.
    *
    * @return the position ID of this position
    */
    public long getPositionId() {
        return _position.getPositionId();
    }

    /**
    * Sets the position ID of this position.
    *
    * @param positionId the position ID of this position
    */
    public void setPositionId(long positionId) {
        _position.setPositionId(positionId);
    }

    /**
    * Returns the fiscal year of this position.
    *
    * @return the fiscal year of this position
    */
    public java.lang.String getFiscalYear() {
        return _position.getFiscalYear();
    }

    /**
    * Sets the fiscal year of this position.
    *
    * @param fiscalYear the fiscal year of this position
    */
    public void setFiscalYear(java.lang.String fiscalYear) {
        _position.setFiscalYear(fiscalYear);
    }

    /**
    * Returns the description of this position.
    *
    * @return the description of this position
    */
    public java.lang.String getDescription() {
        return _position.getDescription();
    }

    /**
    * Sets the description of this position.
    *
    * @param description the description of this position
    */
    public void setDescription(java.lang.String description) {
        _position.setDescription(description);
    }

    /**
    * Returns the category code of this position.
    *
    * @return the category code of this position
    */
    public java.lang.String getCategoryCode() {
        return _position.getCategoryCode();
    }

    /**
    * Sets the category code of this position.
    *
    * @param categoryCode the category code of this position
    */
    public void setCategoryCode(java.lang.String categoryCode) {
        _position.setCategoryCode(categoryCode);
    }

    /**
    * Returns the offer number of this position.
    *
    * @return the offer number of this position
    */
    public java.lang.String getOfferNumber() {
        return _position.getOfferNumber();
    }

    /**
    * Sets the offer number of this position.
    *
    * @param offerNumber the offer number of this position
    */
    public void setOfferNumber(java.lang.String offerNumber) {
        _position.setOfferNumber(offerNumber);
    }

    /**
    * Returns the offer date of this position.
    *
    * @return the offer date of this position
    */
    public java.util.Date getOfferDate() {
        return _position.getOfferDate();
    }

    /**
    * Sets the offer date of this position.
    *
    * @param offerDate the offer date of this position
    */
    public void setOfferDate(java.util.Date offerDate) {
        _position.setOfferDate(offerDate);
    }

    /**
    * Returns the quatity of this position.
    *
    * @return the quatity of this position
    */
    public java.lang.String getQuatity() {
        return _position.getQuatity();
    }

    /**
    * Sets the quatity of this position.
    *
    * @param quatity the quatity of this position
    */
    public void setQuatity(java.lang.String quatity) {
        _position.setQuatity(quatity);
    }

    /**
    * Returns the delivery date of this position.
    *
    * @return the delivery date of this position
    */
    public java.util.Date getDeliveryDate() {
        return _position.getDeliveryDate();
    }

    /**
    * Sets the delivery date of this position.
    *
    * @param deliveryDate the delivery date of this position
    */
    public void setDeliveryDate(java.util.Date deliveryDate) {
        _position.setDeliveryDate(deliveryDate);
    }

    /**
    * Returns the delivery address of this position.
    *
    * @return the delivery address of this position
    */
    public java.lang.String getDeliveryAddress() {
        return _position.getDeliveryAddress();
    }

    /**
    * Sets the delivery address of this position.
    *
    * @param deliveryAddress the delivery address of this position
    */
    public void setDeliveryAddress(java.lang.String deliveryAddress) {
        _position.setDeliveryAddress(deliveryAddress);
    }

    /**
    * Returns the actual unit cost of this position.
    *
    * @return the actual unit cost of this position
    */
    public java.lang.String getActualUnitCost() {
        return _position.getActualUnitCost();
    }

    /**
    * Sets the actual unit cost of this position.
    *
    * @param actualUnitCost the actual unit cost of this position
    */
    public void setActualUnitCost(java.lang.String actualUnitCost) {
        _position.setActualUnitCost(actualUnitCost);
    }

    /**
    * Returns the number of unit of this position.
    *
    * @return the number of unit of this position
    */
    public java.lang.String getNumberOfUnit() {
        return _position.getNumberOfUnit();
    }

    /**
    * Sets the number of unit of this position.
    *
    * @param numberOfUnit the number of unit of this position
    */
    public void setNumberOfUnit(java.lang.String numberOfUnit) {
        _position.setNumberOfUnit(numberOfUnit);
    }

    /**
    * Returns the label number of this position.
    *
    * @return the label number of this position
    */
    public long getLabelNumber() {
        return _position.getLabelNumber();
    }

    /**
    * Sets the label number of this position.
    *
    * @param labelNumber the label number of this position
    */
    public void setLabelNumber(long labelNumber) {
        _position.setLabelNumber(labelNumber);
    }

    /**
    * Returns the po label number of this position.
    *
    * @return the po label number of this position
    */
    public long getPoLabelNumber() {
        return _position.getPoLabelNumber();
    }

    /**
    * Sets the po label number of this position.
    *
    * @param poLabelNumber the po label number of this position
    */
    public void setPoLabelNumber(long poLabelNumber) {
        _position.setPoLabelNumber(poLabelNumber);
    }

    /**
    * Returns the targa tecnica of this position.
    *
    * @return the targa tecnica of this position
    */
    public java.lang.String getTargaTecnica() {
        return _position.getTargaTecnica();
    }

    /**
    * Sets the targa tecnica of this position.
    *
    * @param targaTecnica the targa tecnica of this position
    */
    public void setTargaTecnica(java.lang.String targaTecnica) {
        _position.setTargaTecnica(targaTecnica);
    }

    /**
    * Returns the approved of this position.
    *
    * @return the approved of this position
    */
    public boolean getApproved() {
        return _position.getApproved();
    }

    /**
    * Returns <code>true</code> if this position is approved.
    *
    * @return <code>true</code> if this position is approved; <code>false</code> otherwise
    */
    public boolean isApproved() {
        return _position.isApproved();
    }

    /**
    * Sets whether this position is approved.
    *
    * @param approved the approved of this position
    */
    public void setApproved(boolean approved) {
        _position.setApproved(approved);
    }

    /**
    * Returns the ic approval of this position.
    *
    * @return the ic approval of this position
    */
    public java.lang.String getIcApproval() {
        return _position.getIcApproval();
    }

    /**
    * Sets the ic approval of this position.
    *
    * @param icApproval the ic approval of this position
    */
    public void setIcApproval(java.lang.String icApproval) {
        _position.setIcApproval(icApproval);
    }

    /**
    * Returns the approver of this position.
    *
    * @return the approver of this position
    */
    public java.lang.String getApprover() {
        return _position.getApprover();
    }

    /**
    * Sets the approver of this position.
    *
    * @param approver the approver of this position
    */
    public void setApprover(java.lang.String approver) {
        _position.setApprover(approver);
    }

    /**
    * Returns the asset number of this position.
    *
    * @return the asset number of this position
    */
    public java.lang.String getAssetNumber() {
        return _position.getAssetNumber();
    }

    /**
    * Sets the asset number of this position.
    *
    * @param assetNumber the asset number of this position
    */
    public void setAssetNumber(java.lang.String assetNumber) {
        _position.setAssetNumber(assetNumber);
    }

    /**
    * Returns the shopping cart of this position.
    *
    * @return the shopping cart of this position
    */
    public java.lang.String getShoppingCart() {
        return _position.getShoppingCart();
    }

    /**
    * Sets the shopping cart of this position.
    *
    * @param shoppingCart the shopping cart of this position
    */
    public void setShoppingCart(java.lang.String shoppingCart) {
        _position.setShoppingCart(shoppingCart);
    }

    /**
    * Returns the po number of this position.
    *
    * @return the po number of this position
    */
    public java.lang.String getPoNumber() {
        return _position.getPoNumber();
    }

    /**
    * Sets the po number of this position.
    *
    * @param poNumber the po number of this position
    */
    public void setPoNumber(java.lang.String poNumber) {
        _position.setPoNumber(poNumber);
    }

    /**
    * Returns the evo location of this position.
    *
    * @return the evo location of this position
    */
    public java.lang.String getEvoLocation() {
        return _position.getEvoLocation();
    }

    /**
    * Sets the evo location of this position.
    *
    * @param evoLocation the evo location of this position
    */
    public void setEvoLocation(java.lang.String evoLocation) {
        _position.setEvoLocation(evoLocation);
    }

    /**
    * Returns the evo material id of this position.
    *
    * @return the evo material id of this position
    */
    public java.lang.String getEvoCodMateriale() {
        return _position.getEvoCodMateriale();
    }

    /**
    * Sets the evo material id of this position.
    *
    * @param evoCodMateriale the evo material id of this position
    */
    public void setEvoCodMateriale(java.lang.String evoCodMateriale) {
        _position.setEvoCodMateriale(evoCodMateriale);
    }

    /**
    * Returns the evo material id of this position.
    *
    * @return the evo material id of this position
    */
    public java.lang.String getEvoDesMateriale() {
        return _position.getEvoDesMateriale();
    }

    /**
    * Sets the evo material id of this position.
    *
    * @param evoDesMateriale the evo material id of this position
    */
    public void setEvoDesMateriale(java.lang.String evoDesMateriale) {
        _position.setEvoDesMateriale(evoDesMateriale);
    }
    
    /**
    * Returns the wbs code ID of this position.
    *
    * @return the wbs code ID of this position
    */
    public java.lang.String getWbsCodeId() {
        return _position.getWbsCodeId();
    }

    /**
    * Sets the wbs code ID of this position.
    *
    * @param wbsCodeId the wbs code ID of this position
    */
    public void setWbsCodeId(java.lang.String wbsCodeId) {
        _position.setWbsCodeId(wbsCodeId);
    }

    /**
    * Returns the purchase request ID of this position.
    *
    * @return the purchase request ID of this position
    */
    public java.lang.String getPurchaseRequestId() {
        return _position.getPurchaseRequestId();
    }

    /**
    * Sets the purchase request ID of this position.
    *
    * @param purchaseRequestId the purchase request ID of this position
    */
    public void setPurchaseRequestId(java.lang.String purchaseRequestId) {
        _position.setPurchaseRequestId(purchaseRequestId);
    }

    /**
    * Returns the purchase order ID of this position.
    *
    * @return the purchase order ID of this position
    */
    public java.lang.String getPurchaseOrderId() {
        return _position.getPurchaseOrderId();
    }

    /**
    * Sets the purchase order ID of this position.
    *
    * @param purchaseOrderId the purchase order ID of this position
    */
    public void setPurchaseOrderId(java.lang.String purchaseOrderId) {
        _position.setPurchaseOrderId(purchaseOrderId);
    }

    /**
    * Returns the created date of this position.
    *
    * @return the created date of this position
    */
    public java.util.Date getCreatedDate() {
        return _position.getCreatedDate();
    }

    /**
    * Sets the created date of this position.
    *
    * @param createdDate the created date of this position
    */
    public void setCreatedDate(java.util.Date createdDate) {
        _position.setCreatedDate(createdDate);
    }

    /**
    * Returns the created user ID of this position.
    *
    * @return the created user ID of this position
    */
    public long getCreatedUserId() {
        return _position.getCreatedUserId();
    }

    /**
    * Sets the created user ID of this position.
    *
    * @param createdUserId the created user ID of this position
    */
    public void setCreatedUserId(long createdUserId) {
        _position.setCreatedUserId(createdUserId);
    }

    /**
    * Returns the created user uuid of this position.
    *
    * @return the created user uuid of this position
    * @throws SystemException if a system exception occurred
    */
    public java.lang.String getCreatedUserUuid()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _position.getCreatedUserUuid();
    }

    /**
    * Sets the created user uuid of this position.
    *
    * @param createdUserUuid the created user uuid of this position
    */
    public void setCreatedUserUuid(java.lang.String createdUserUuid) {
        _position.setCreatedUserUuid(createdUserUuid);
    }

    public boolean isNew() {
        return _position.isNew();
    }

    public void setNew(boolean n) {
        _position.setNew(n);
    }

    public boolean isCachedModel() {
        return _position.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _position.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _position.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _position.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _position.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _position.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _position.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new PositionWrapper((Position) _position.clone());
    }

    public int compareTo(Position position) {
        return _position.compareTo(position);
    }

    @Override
    public int hashCode() {
        return _position.hashCode();
    }

    public com.liferay.portal.model.CacheModel<Position> toCacheModel() {
        return _position.toCacheModel();
    }

    public Position toEscapedModel() {
        return new PositionWrapper(_position.toEscapedModel());
    }

    public Position toUnescapedModel() {
        return new PositionWrapper(_position.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _position.toString();
    }

    public java.lang.String toXmlString() {
        return _position.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _position.persist();
    }

    public void setCommaFormattedUnitCost(
        java.lang.String commaFormattedUnitCost) {
        _position.setCommaFormattedUnitCost(commaFormattedUnitCost);
    }

    public java.lang.String getCommaFormattedUnitCost() {
        return _position.getCommaFormattedUnitCost();
    }

    public void setPositionStatusLabel(java.lang.String positionStatusLabel) {
        _position.setPositionStatusLabel(positionStatusLabel);
    }

    public void setFormattedUnitCost(java.lang.String formatedUnitCost) {
        _position.setFormattedUnitCost(formatedUnitCost);
    }

    public java.lang.String getFormattedUnitCost() {
        return _position.getFormattedUnitCost();
    }

    public void setApproveBtn(boolean approveBtn) {
        _position.setApproveBtn(approveBtn);
    }

    public boolean isApproveBtn() {
        return _position.isApproveBtn();
    }

    public java.lang.String getPositionStatusLabel() {
        return _position.getPositionStatusLabel();
    }

    public void setApproverName(java.lang.String approverName) {
        _position.setApproverName(approverName);
    }

    public java.lang.String getApproverName() {
        return _position.getApproverName();
    }

    public void setApprovalStatus(java.lang.String approvalStatus) {
        _position.setApprovalStatus(approvalStatus);
    }

    public java.lang.String getApprovalStatus() {
        return _position.getApprovalStatus();
    }

    public boolean getPositionEditBtton() {
        return _position.getPositionEditBtton();
    }

    public void setPositionEditBtton(boolean positionEditBtton) {
        _position.setPositionEditBtton(positionEditBtton);
    }

    public com.hp.omp.model.custom.PositionStatus getPositionStatus() {
        return _position.getPositionStatus();
    }

    public java.lang.String getDeliveryDateText() {
        return _position.getDeliveryDateText();
    }

    public java.lang.String getOfferDateText() {
        return _position.getOfferDateText();
    }

    public java.lang.String getFieldStatus()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _position.getFieldStatus();
    }

    public java.lang.String getCheckboxStatus()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _position.getCheckboxStatus();
    }

    public java.lang.Double getSumPositionGRPercentage()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _position.getSumPositionGRPercentage();
    }

    public java.util.List<com.hp.omp.model.GoodReceipt> getPositionGoodReceipts()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _position.getPositionGoodReceipts();
    }

    public java.lang.String getiCApprovalText() {
        return _position.getiCApprovalText();
    }

    public void setiCApprovalText(java.lang.String iCApprovalText) {
        _position.setiCApprovalText(iCApprovalText);
    }

    public boolean isAddGrBtn()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _position.isAddGrBtn();
    }

    public void setAddGrBtn(boolean addGrBtn) {
        _position.setAddGrBtn(addGrBtn);
    }

    public boolean isEditGrBtn()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _position.isEditGrBtn();
    }

    public void setEditGrBtn(boolean editGrBtn) {
        _position.setEditGrBtn(editGrBtn);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PositionWrapper)) {
            return false;
        }

        PositionWrapper positionWrapper = (PositionWrapper) obj;

        if (Validator.equals(_position, positionWrapper._position)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public Position getWrappedPosition() {
        return _position;
    }

    public Position getWrappedModel() {
        return _position;
    }

    public void resetOriginalValues() {
        _position.resetOriginalValues();
    }
}
