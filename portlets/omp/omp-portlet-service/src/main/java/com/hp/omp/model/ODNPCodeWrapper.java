package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ODNPCode}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       ODNPCode
 * @generated
 */
public class ODNPCodeWrapper implements ODNPCode, ModelWrapper<ODNPCode> {
    private ODNPCode _odnpCode;

    public ODNPCodeWrapper(ODNPCode odnpCode) {
        _odnpCode = odnpCode;
    }

    public Class<?> getModelClass() {
        return ODNPCode.class;
    }

    public String getModelClassName() {
        return ODNPCode.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("odnpCodeId", getOdnpCodeId());
        attributes.put("odnpCodeName", getOdnpCodeName());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long odnpCodeId = (Long) attributes.get("odnpCodeId");

        if (odnpCodeId != null) {
            setOdnpCodeId(odnpCodeId);
        }

        String odnpCodeName = (String) attributes.get("odnpCodeName");

        if (odnpCodeName != null) {
            setOdnpCodeName(odnpCodeName);
        }
    }

    /**
    * Returns the primary key of this o d n p code.
    *
    * @return the primary key of this o d n p code
    */
    public long getPrimaryKey() {
        return _odnpCode.getPrimaryKey();
    }

    /**
    * Sets the primary key of this o d n p code.
    *
    * @param primaryKey the primary key of this o d n p code
    */
    public void setPrimaryKey(long primaryKey) {
        _odnpCode.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the odnp code ID of this o d n p code.
    *
    * @return the odnp code ID of this o d n p code
    */
    public long getOdnpCodeId() {
        return _odnpCode.getOdnpCodeId();
    }

    /**
    * Sets the odnp code ID of this o d n p code.
    *
    * @param odnpCodeId the odnp code ID of this o d n p code
    */
    public void setOdnpCodeId(long odnpCodeId) {
        _odnpCode.setOdnpCodeId(odnpCodeId);
    }

    /**
    * Returns the odnp code name of this o d n p code.
    *
    * @return the odnp code name of this o d n p code
    */
    public java.lang.String getOdnpCodeName() {
        return _odnpCode.getOdnpCodeName();
    }

    /**
    * Sets the odnp code name of this o d n p code.
    *
    * @param odnpCodeName the odnp code name of this o d n p code
    */
    public void setOdnpCodeName(java.lang.String odnpCodeName) {
        _odnpCode.setOdnpCodeName(odnpCodeName);
    }

    public boolean isNew() {
        return _odnpCode.isNew();
    }

    public void setNew(boolean n) {
        _odnpCode.setNew(n);
    }

    public boolean isCachedModel() {
        return _odnpCode.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _odnpCode.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _odnpCode.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _odnpCode.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _odnpCode.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _odnpCode.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _odnpCode.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new ODNPCodeWrapper((ODNPCode) _odnpCode.clone());
    }

    public int compareTo(ODNPCode odnpCode) {
        return _odnpCode.compareTo(odnpCode);
    }

    @Override
    public int hashCode() {
        return _odnpCode.hashCode();
    }

    public com.liferay.portal.model.CacheModel<ODNPCode> toCacheModel() {
        return _odnpCode.toCacheModel();
    }

    public ODNPCode toEscapedModel() {
        return new ODNPCodeWrapper(_odnpCode.toEscapedModel());
    }

    public ODNPCode toUnescapedModel() {
        return new ODNPCodeWrapper(_odnpCode.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _odnpCode.toString();
    }

    public java.lang.String toXmlString() {
        return _odnpCode.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _odnpCode.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ODNPCodeWrapper)) {
            return false;
        }

        ODNPCodeWrapper odnpCodeWrapper = (ODNPCodeWrapper) obj;

        if (Validator.equals(_odnpCode, odnpCodeWrapper._odnpCode)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public ODNPCode getWrappedODNPCode() {
        return _odnpCode;
    }

    public ODNPCode getWrappedModel() {
        return _odnpCode;
    }

    public void resetOriginalValues() {
        _odnpCode.resetOriginalValues();
    }
}
