package com.hp.omp.model;

import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.ODNPCodeLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class ODNPCodeClp extends BaseModelImpl<ODNPCode> implements ODNPCode {
    private long _odnpCodeId;
    private String _odnpCodeName;
    private BaseModel<?> _odnpCodeRemoteModel;

    public ODNPCodeClp() {
    }

    public Class<?> getModelClass() {
        return ODNPCode.class;
    }

    public String getModelClassName() {
        return ODNPCode.class.getName();
    }

    public long getPrimaryKey() {
        return _odnpCodeId;
    }

    public void setPrimaryKey(long primaryKey) {
        setOdnpCodeId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_odnpCodeId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("odnpCodeId", getOdnpCodeId());
        attributes.put("odnpCodeName", getOdnpCodeName());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long odnpCodeId = (Long) attributes.get("odnpCodeId");

        if (odnpCodeId != null) {
            setOdnpCodeId(odnpCodeId);
        }

        String odnpCodeName = (String) attributes.get("odnpCodeName");

        if (odnpCodeName != null) {
            setOdnpCodeName(odnpCodeName);
        }
    }

    public long getOdnpCodeId() {
        return _odnpCodeId;
    }

    public void setOdnpCodeId(long odnpCodeId) {
        _odnpCodeId = odnpCodeId;

        if (_odnpCodeRemoteModel != null) {
            try {
                Class<?> clazz = _odnpCodeRemoteModel.getClass();

                Method method = clazz.getMethod("setOdnpCodeId", long.class);

                method.invoke(_odnpCodeRemoteModel, odnpCodeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getOdnpCodeName() {
        return _odnpCodeName;
    }

    public void setOdnpCodeName(String odnpCodeName) {
        _odnpCodeName = odnpCodeName;

        if (_odnpCodeRemoteModel != null) {
            try {
                Class<?> clazz = _odnpCodeRemoteModel.getClass();

                Method method = clazz.getMethod("setOdnpCodeName", String.class);

                method.invoke(_odnpCodeRemoteModel, odnpCodeName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getODNPCodeRemoteModel() {
        return _odnpCodeRemoteModel;
    }

    public void setODNPCodeRemoteModel(BaseModel<?> odnpCodeRemoteModel) {
        _odnpCodeRemoteModel = odnpCodeRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _odnpCodeRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_odnpCodeRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            ODNPCodeLocalServiceUtil.addODNPCode(this);
        } else {
            ODNPCodeLocalServiceUtil.updateODNPCode(this);
        }
    }

    @Override
    public ODNPCode toEscapedModel() {
        return (ODNPCode) ProxyUtil.newProxyInstance(ODNPCode.class.getClassLoader(),
            new Class[] { ODNPCode.class }, new AutoEscapeBeanHandler(this));
    }

    public ODNPCode toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        ODNPCodeClp clone = new ODNPCodeClp();

        clone.setOdnpCodeId(getOdnpCodeId());
        clone.setOdnpCodeName(getOdnpCodeName());

        return clone;
    }

    public int compareTo(ODNPCode odnpCode) {
        long primaryKey = odnpCode.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ODNPCodeClp)) {
            return false;
        }

        ODNPCodeClp odnpCode = (ODNPCodeClp) obj;

        long primaryKey = odnpCode.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(5);

        sb.append("{odnpCodeId=");
        sb.append(getOdnpCodeId());
        sb.append(", odnpCodeName=");
        sb.append(getOdnpCodeName());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(10);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.ODNPCode");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>odnpCodeId</column-name><column-value><![CDATA[");
        sb.append(getOdnpCodeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>odnpCodeName</column-name><column-value><![CDATA[");
        sb.append(getOdnpCodeName());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
