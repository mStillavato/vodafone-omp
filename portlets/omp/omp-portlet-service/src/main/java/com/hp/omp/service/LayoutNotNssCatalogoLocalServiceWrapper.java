/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service;

import java.io.OutputStream;
import java.util.List;

import com.hp.omp.model.LayoutNotNssCatalogo;
import com.hp.omp.model.custom.PoCatalogNotNssDTO;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link LayoutNotNssCatalogoLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       LayoutNotNssCatalogoLocalService
 * @generated
 */
public class LayoutNotNssCatalogoLocalServiceWrapper implements LayoutNotNssCatalogoLocalService,
ServiceWrapper<LayoutNotNssCatalogoLocalService> {
	public LayoutNotNssCatalogoLocalServiceWrapper(
			LayoutNotNssCatalogoLocalService layoutNotNssCatalogoLocalService) {
		_layoutNotNssCatalogoLocalService = layoutNotNssCatalogoLocalService;
	}

	/**
	 * Adds the layoutNotNssCatalogo to the database. Also notifies the appropriate model listeners.
	 *
	 * @param layoutNotNssCatalogo the layoutNotNssCatalogo
	 * @return the layoutNotNssCatalogo that was added
	 * @throws SystemException if a system exception occurred
	 */
	public com.hp.omp.model.LayoutNotNssCatalogo addLayoutNotNssCatalogo(
			com.hp.omp.model.LayoutNotNssCatalogo layoutNotNssCatalogo)
					throws com.liferay.portal.kernel.exception.SystemException {
		return _layoutNotNssCatalogoLocalService.addLayoutNotNssCatalogo(layoutNotNssCatalogo);
	}

	/**
	 * Creates a new layoutNotNssCatalogo with the primary key. Does not add the layoutNotNssCatalogo to the database.
	 *
	 * @param layoutNotNssCatalogoId the primary key for the new layoutNotNssCatalogo
	 * @return the new layoutNotNssCatalogo
	 */
	public com.hp.omp.model.LayoutNotNssCatalogo createLayoutNotNssCatalogo(long layoutNotNssCatalogoId) {
		return _layoutNotNssCatalogoLocalService.createLayoutNotNssCatalogo(layoutNotNssCatalogoId);
	}

	/**
	 * Deletes the layoutNotNssCatalogo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param layoutNotNssCatalogoId the primary key of the layoutNotNssCatalogo
	 * @return the layoutNotNssCatalogo that was removed
	 * @throws PortalException if a layoutNotNssCatalogo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public com.hp.omp.model.LayoutNotNssCatalogo deleteLayoutNotNssCatalogo(long layoutNotNssCatalogoId)
			throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _layoutNotNssCatalogoLocalService.deleteLayoutNotNssCatalogo(layoutNotNssCatalogoId);
	}

	/**
	 * Deletes the layoutNotNssCatalogo from the database. Also notifies the appropriate model listeners.
	 *
	 * @param layoutNotNssCatalogo the layoutNotNssCatalogo
	 * @return the layoutNotNssCatalogo that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public com.hp.omp.model.LayoutNotNssCatalogo deleteLayoutNotNssCatalogo(
			com.hp.omp.model.LayoutNotNssCatalogo layoutNotNssCatalogo)
					throws com.liferay.portal.kernel.exception.SystemException {
		return _layoutNotNssCatalogoLocalService.deleteLayoutNotNssCatalogo(layoutNotNssCatalogo);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _layoutNotNssCatalogoLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
					throws com.liferay.portal.kernel.exception.SystemException {
		return _layoutNotNssCatalogoLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
			int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _layoutNotNssCatalogoLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
			int end,
			com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
					throws com.liferay.portal.kernel.exception.SystemException {
		return _layoutNotNssCatalogoLocalService.dynamicQuery(dynamicQuery, start, end,
				orderByComparator);
	}

	/**
	 * Returns the number of rows that match the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows that match the dynamic query
	 * @throws SystemException if a system exception occurred
	 */
	public long dynamicQueryCount(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
					throws com.liferay.portal.kernel.exception.SystemException {
		return _layoutNotNssCatalogoLocalService.dynamicQueryCount(dynamicQuery);
	}

	public com.hp.omp.model.LayoutNotNssCatalogo fetchLayoutNotNssCatalogo(long layoutNotNssCatalogoId)
			throws com.liferay.portal.kernel.exception.SystemException {
		return _layoutNotNssCatalogoLocalService.fetchLayoutNotNssCatalogo(layoutNotNssCatalogoId);
	}

	/**
	 * Returns the layoutNotNssCatalogo with the primary key.
	 *
	 * @param layoutNotNssCatalogoId the primary key of the layoutNotNssCatalogo
	 * @return the layoutNotNssCatalogo
	 * @throws PortalException if a layoutNotNssCatalogo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public com.hp.omp.model.LayoutNotNssCatalogo getLayoutNotNssCatalogo(long layoutNotNssCatalogoId)
			throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _layoutNotNssCatalogoLocalService.getLayoutNotNssCatalogo(layoutNotNssCatalogoId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
					throws com.liferay.portal.kernel.exception.PortalException,
					com.liferay.portal.kernel.exception.SystemException {
		return _layoutNotNssCatalogoLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns a range of all the catalogos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of catalogos
	 * @param end the upper bound of the range of catalogos (not inclusive)
	 * @return the range of catalogos
	 * @throws SystemException if a system exception occurred
	 */
	public java.util.List<com.hp.omp.model.LayoutNotNssCatalogo> getLayoutNotNssCatalogos(int start,
			int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _layoutNotNssCatalogoLocalService.getLayoutNotNssCatalogos(start, end);
	}

	/**
	 * Returns the number of catalogos.
	 *
	 * @return the number of catalogos
	 * @throws SystemException if a system exception occurred
	 */
	public int getLayoutNotNssCatalogosCount()
			throws com.liferay.portal.kernel.exception.SystemException {
		return _layoutNotNssCatalogoLocalService.getLayoutNotNssCatalogosCount();
	}

	/**
	 * Updates the layoutNotNssCatalogo in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param layoutNotNssCatalogo the layoutNotNssCatalogo
	 * @return the layoutNotNssCatalogo that was updated
	 * @throws SystemException if a system exception occurred
	 */
	public com.hp.omp.model.LayoutNotNssCatalogo updateLayoutNotNssCatalogo(
			com.hp.omp.model.LayoutNotNssCatalogo layoutNotNssCatalogo)
					throws com.liferay.portal.kernel.exception.SystemException {
		return _layoutNotNssCatalogoLocalService.updateLayoutNotNssCatalogo(layoutNotNssCatalogo);
	}

	/**
	 * Updates the layoutNotNssCatalogo in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param layoutNotNssCatalogo the layoutNotNssCatalogo
	 * @param merge whether to merge the layoutNotNssCatalogo with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	 * @return the layoutNotNssCatalogo that was updated
	 * @throws SystemException if a system exception occurred
	 */
	public com.hp.omp.model.LayoutNotNssCatalogo updateLayoutNotNssCatalogo(
			com.hp.omp.model.LayoutNotNssCatalogo layoutNotNssCatalogo, boolean merge)
					throws com.liferay.portal.kernel.exception.SystemException {
		return _layoutNotNssCatalogoLocalService.updateLayoutNotNssCatalogo(layoutNotNssCatalogo, merge);
	}

	/**
	 * Returns the Spring bean ID for this bean.
	 *
	 * @return the Spring bean ID for this bean
	 */
	public java.lang.String getBeanIdentifier() {
		return _layoutNotNssCatalogoLocalService.getBeanIdentifier();
	}

	/**
	 * Sets the Spring bean ID for this bean.
	 *
	 * @param beanIdentifier the Spring bean ID for this bean
	 */
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_layoutNotNssCatalogoLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
			java.lang.String[] parameterTypes, java.lang.Object[] arguments)
					throws java.lang.Throwable {
		return _layoutNotNssCatalogoLocalService.invokeMethod(name, parameterTypes,
				arguments);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public LayoutNotNssCatalogoLocalService getWrappedLayoutNotNssCatalogoLocalService() {
		return _layoutNotNssCatalogoLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedLayoutNotNssCatalogoLocalService(
			LayoutNotNssCatalogoLocalService layoutNotNssCatalogoLocalService) {
		_layoutNotNssCatalogoLocalService = layoutNotNssCatalogoLocalService;
	}

	public LayoutNotNssCatalogoLocalService getWrappedService() {
		return _layoutNotNssCatalogoLocalService;
	}

	public void setWrappedService(LayoutNotNssCatalogoLocalService layoutNotNssCatalogoLocalService) {
		_layoutNotNssCatalogoLocalService = layoutNotNssCatalogoLocalService;
	}

	private LayoutNotNssCatalogoLocalService _layoutNotNssCatalogoLocalService;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<com.hp.omp.model.custom.LayoutNotNssCatalogoListDTO> getLayoutNotNssCatalogoList(String user) {
		return _layoutNotNssCatalogoLocalService.getLayoutNotNssCatalogoList(user);
	}

	public java.lang.Long getLayoutNotNssCatalogoListCount(String user) {
		return _layoutNotNssCatalogoLocalService.getLayoutNotNssCatalogoListCount(user);
	}

	public void deleteLayoutNotNssCatalogo4User(String user)
			throws SystemException {
		_layoutNotNssCatalogoLocalService.deleteLayoutNotNssCatalogo4User(user);
	}

}