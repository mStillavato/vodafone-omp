package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the PRAudit service. Represents a row in the &quot;PR_AUDIT&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see PRAuditModel
 * @see com.hp.omp.model.impl.PRAuditImpl
 * @see com.hp.omp.model.impl.PRAuditModelImpl
 * @generated
 */
public interface PRAudit extends PRAuditModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.PRAuditImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
