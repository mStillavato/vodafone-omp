package com.hp.omp.service.persistence;

public interface TrackingCodesFinder {
    public java.util.List<com.hp.omp.model.TrackingCodes> getTrackingCodesOrderedAsc(
            int start, int end, String searchTCN)
            throws com.liferay.portal.kernel.exception.SystemException;
    
    public int getTrackingCodesCountList(String searchFilter)
            throws com.liferay.portal.kernel.exception.SystemException;
}
