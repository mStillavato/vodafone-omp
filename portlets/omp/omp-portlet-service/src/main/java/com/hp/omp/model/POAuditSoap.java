package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class POAuditSoap implements Serializable {
    private long _auditId;
    private Long _purchaseOrderId;
    private int _status;
    private Long _userId;
    private Date _changeDate;

    public POAuditSoap() {
    }

    public static POAuditSoap toSoapModel(POAudit model) {
        POAuditSoap soapModel = new POAuditSoap();

        soapModel.setAuditId(model.getAuditId());
        soapModel.setPurchaseOrderId(model.getPurchaseOrderId());
        soapModel.setStatus(model.getStatus());
        soapModel.setUserId(model.getUserId());
        soapModel.setChangeDate(model.getChangeDate());

        return soapModel;
    }

    public static POAuditSoap[] toSoapModels(POAudit[] models) {
        POAuditSoap[] soapModels = new POAuditSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static POAuditSoap[][] toSoapModels(POAudit[][] models) {
        POAuditSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new POAuditSoap[models.length][models[0].length];
        } else {
            soapModels = new POAuditSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static POAuditSoap[] toSoapModels(List<POAudit> models) {
        List<POAuditSoap> soapModels = new ArrayList<POAuditSoap>(models.size());

        for (POAudit model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new POAuditSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _auditId;
    }

    public void setPrimaryKey(long pk) {
        setAuditId(pk);
    }

    public long getAuditId() {
        return _auditId;
    }

    public void setAuditId(long auditId) {
        _auditId = auditId;
    }

    public Long getPurchaseOrderId() {
        return _purchaseOrderId;
    }

    public void setPurchaseOrderId(Long purchaseOrderId) {
        _purchaseOrderId = purchaseOrderId;
    }

    public int getStatus() {
        return _status;
    }

    public void setStatus(int status) {
        _status = status;
    }

    public Long getUserId() {
        return _userId;
    }

    public void setUserId(Long userId) {
        _userId = userId;
    }

    public Date getChangeDate() {
        return _changeDate;
    }

    public void setChangeDate(Date changeDate) {
        _changeDate = changeDate;
    }
}
