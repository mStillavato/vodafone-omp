package com.hp.omp.service.persistence;

import com.hp.omp.model.BudgetCategory;
import com.hp.omp.service.BudgetCategoryLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class BudgetCategoryActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public BudgetCategoryActionableDynamicQuery() throws SystemException {
        setBaseLocalService(BudgetCategoryLocalServiceUtil.getService());
        setClass(BudgetCategory.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("budgetCategoryId");
    }
}
