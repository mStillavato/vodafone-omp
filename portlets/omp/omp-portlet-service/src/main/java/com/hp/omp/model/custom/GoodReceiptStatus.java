package com.hp.omp.model.custom;

import java.util.HashMap;
import java.util.Map;

public enum GoodReceiptStatus {

	GR_REQUESTED(7,"GR_REQUESTED"),
	GR_CLOSED(8,"GR_CLOSED"),
	GR_DELETED(9,"GR_DELETED");
	
	private int code;
	private String label;
	
	
	
	private GoodReceiptStatus(int code, String label){
		this.code = code;
		this.label = label;
	}
	
	
	  private static Map<Integer, GoodReceiptStatus> codeToStatusMapping;
		 
	    
	 
	    public static GoodReceiptStatus getStatus(int i) {
	        if (codeToStatusMapping == null) {
	            initMapping();
	        }
	        return codeToStatusMapping.get(i);
	    }
	 
	    private static void initMapping() {
	        codeToStatusMapping = new HashMap<Integer, GoodReceiptStatus>();
	        for (GoodReceiptStatus s : values()) {
	            codeToStatusMapping.put(s.code, s);
	        }
	    }
	public int getCode(){
		return code;
	}
	
	public String getLabel(){
		return label;
	}
}
