package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class WbsCodeSoap implements Serializable {
    private long _wbsCodeId;
    private String _wbsCodeName;
    private String _description;
    private boolean _display;

    public WbsCodeSoap() {
    }

    public static WbsCodeSoap toSoapModel(WbsCode model) {
        WbsCodeSoap soapModel = new WbsCodeSoap();

        soapModel.setWbsCodeId(model.getWbsCodeId());
        soapModel.setWbsCodeName(model.getWbsCodeName());
        soapModel.setDescription(model.getDescription());
        soapModel.setDisplay(model.getDisplay());

        return soapModel;
    }

    public static WbsCodeSoap[] toSoapModels(WbsCode[] models) {
        WbsCodeSoap[] soapModels = new WbsCodeSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static WbsCodeSoap[][] toSoapModels(WbsCode[][] models) {
        WbsCodeSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new WbsCodeSoap[models.length][models[0].length];
        } else {
            soapModels = new WbsCodeSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static WbsCodeSoap[] toSoapModels(List<WbsCode> models) {
        List<WbsCodeSoap> soapModels = new ArrayList<WbsCodeSoap>(models.size());

        for (WbsCode model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new WbsCodeSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _wbsCodeId;
    }

    public void setPrimaryKey(long pk) {
        setWbsCodeId(pk);
    }

    public long getWbsCodeId() {
        return _wbsCodeId;
    }

    public void setWbsCodeId(long wbsCodeId) {
        _wbsCodeId = wbsCodeId;
    }

    public String getWbsCodeName() {
        return _wbsCodeName;
    }

    public void setWbsCodeName(String wbsCodeName) {
        _wbsCodeName = wbsCodeName;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;
    }
}
