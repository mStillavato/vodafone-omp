package com.hp.omp.service.persistence;

import com.hp.omp.model.ODNPCode;
import com.hp.omp.service.ODNPCodeLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class ODNPCodeActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public ODNPCodeActionableDynamicQuery() throws SystemException {
        setBaseLocalService(ODNPCodeLocalServiceUtil.getService());
        setClass(ODNPCode.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("odnpCodeId");
    }
}
