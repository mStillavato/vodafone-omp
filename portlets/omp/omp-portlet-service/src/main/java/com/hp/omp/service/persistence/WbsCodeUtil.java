package com.hp.omp.service.persistence;

import com.hp.omp.model.WbsCode;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the wbs code service. This utility wraps {@link WbsCodePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see WbsCodePersistence
 * @see WbsCodePersistenceImpl
 * @generated
 */
public class WbsCodeUtil {
    private static WbsCodePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(WbsCode wbsCode) {
        getPersistence().clearCache(wbsCode);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<WbsCode> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<WbsCode> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<WbsCode> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static WbsCode update(WbsCode wbsCode, boolean merge)
        throws SystemException {
        return getPersistence().update(wbsCode, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static WbsCode update(WbsCode wbsCode, boolean merge,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(wbsCode, merge, serviceContext);
    }

    /**
    * Caches the wbs code in the entity cache if it is enabled.
    *
    * @param wbsCode the wbs code
    */
    public static void cacheResult(com.hp.omp.model.WbsCode wbsCode) {
        getPersistence().cacheResult(wbsCode);
    }

    /**
    * Caches the wbs codes in the entity cache if it is enabled.
    *
    * @param wbsCodes the wbs codes
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.WbsCode> wbsCodes) {
        getPersistence().cacheResult(wbsCodes);
    }

    /**
    * Creates a new wbs code with the primary key. Does not add the wbs code to the database.
    *
    * @param wbsCodeId the primary key for the new wbs code
    * @return the new wbs code
    */
    public static com.hp.omp.model.WbsCode create(long wbsCodeId) {
        return getPersistence().create(wbsCodeId);
    }

    /**
    * Removes the wbs code with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param wbsCodeId the primary key of the wbs code
    * @return the wbs code that was removed
    * @throws com.hp.omp.NoSuchWbsCodeException if a wbs code with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.WbsCode remove(long wbsCodeId)
        throws com.hp.omp.NoSuchWbsCodeException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(wbsCodeId);
    }

    public static com.hp.omp.model.WbsCode updateImpl(
        com.hp.omp.model.WbsCode wbsCode, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(wbsCode, merge);
    }

    /**
    * Returns the wbs code with the primary key or throws a {@link com.hp.omp.NoSuchWbsCodeException} if it could not be found.
    *
    * @param wbsCodeId the primary key of the wbs code
    * @return the wbs code
    * @throws com.hp.omp.NoSuchWbsCodeException if a wbs code with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.WbsCode findByPrimaryKey(long wbsCodeId)
        throws com.hp.omp.NoSuchWbsCodeException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(wbsCodeId);
    }

    /**
    * Returns the wbs code with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param wbsCodeId the primary key of the wbs code
    * @return the wbs code, or <code>null</code> if a wbs code with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.WbsCode fetchByPrimaryKey(long wbsCodeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(wbsCodeId);
    }

    /**
    * Returns all the wbs codes.
    *
    * @return the wbs codes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.WbsCode> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the wbs codes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of wbs codes
    * @param end the upper bound of the range of wbs codes (not inclusive)
    * @return the range of wbs codes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.WbsCode> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the wbs codes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of wbs codes
    * @param end the upper bound of the range of wbs codes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of wbs codes
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.WbsCode> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the wbs codes from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of wbs codes.
    *
    * @return the number of wbs codes
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static WbsCodePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (WbsCodePersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    WbsCodePersistence.class.getName());

            ReferenceRegistry.registerReference(WbsCodeUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(WbsCodePersistence persistence) {
    }
}
