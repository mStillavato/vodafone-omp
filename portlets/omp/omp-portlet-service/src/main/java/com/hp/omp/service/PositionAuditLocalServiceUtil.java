package com.hp.omp.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the position audit local service. This utility wraps {@link com.hp.omp.service.impl.PositionAuditLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see PositionAuditLocalService
 * @see com.hp.omp.service.base.PositionAuditLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.PositionAuditLocalServiceImpl
 * @generated
 */
public class PositionAuditLocalServiceUtil {
    private static PositionAuditLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.hp.omp.service.impl.PositionAuditLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the position audit to the database. Also notifies the appropriate model listeners.
    *
    * @param positionAudit the position audit
    * @return the position audit that was added
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.PositionAudit addPositionAudit(
        com.hp.omp.model.PositionAudit positionAudit)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addPositionAudit(positionAudit);
    }

    /**
    * Creates a new position audit with the primary key. Does not add the position audit to the database.
    *
    * @param auditId the primary key for the new position audit
    * @return the new position audit
    */
    public static com.hp.omp.model.PositionAudit createPositionAudit(
        long auditId) {
        return getService().createPositionAudit(auditId);
    }

    /**
    * Deletes the position audit with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param auditId the primary key of the position audit
    * @return the position audit that was removed
    * @throws PortalException if a position audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.PositionAudit deletePositionAudit(
        long auditId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deletePositionAudit(auditId);
    }

    /**
    * Deletes the position audit from the database. Also notifies the appropriate model listeners.
    *
    * @param positionAudit the position audit
    * @return the position audit that was removed
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.PositionAudit deletePositionAudit(
        com.hp.omp.model.PositionAudit positionAudit)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deletePositionAudit(positionAudit);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    public static com.hp.omp.model.PositionAudit fetchPositionAudit(
        long auditId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchPositionAudit(auditId);
    }

    /**
    * Returns the position audit with the primary key.
    *
    * @param auditId the primary key of the position audit
    * @return the position audit
    * @throws PortalException if a position audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.PositionAudit getPositionAudit(long auditId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPositionAudit(auditId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the position audits.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of position audits
    * @param end the upper bound of the range of position audits (not inclusive)
    * @return the range of position audits
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.PositionAudit> getPositionAudits(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPositionAudits(start, end);
    }

    /**
    * Returns the number of position audits.
    *
    * @return the number of position audits
    * @throws SystemException if a system exception occurred
    */
    public static int getPositionAuditsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPositionAuditsCount();
    }

    /**
    * Updates the position audit in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param positionAudit the position audit
    * @return the position audit that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.PositionAudit updatePositionAudit(
        com.hp.omp.model.PositionAudit positionAudit)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updatePositionAudit(positionAudit);
    }

    /**
    * Updates the position audit in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param positionAudit the position audit
    * @param merge whether to merge the position audit with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the position audit that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.PositionAudit updatePositionAudit(
        com.hp.omp.model.PositionAudit positionAudit, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updatePositionAudit(positionAudit, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static void clearService() {
        _service = null;
    }

    public static PositionAuditLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    PositionAuditLocalService.class.getName());

            if (invokableLocalService instanceof PositionAuditLocalService) {
                _service = (PositionAuditLocalService) invokableLocalService;
            } else {
                _service = new PositionAuditLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(PositionAuditLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated
     */
    public void setService(PositionAuditLocalService service) {
    }
}
