package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class PurchaseRequestSoap implements Serializable {
    private long _purchaseRequestId;
    private String _costCenterId;
    private String _vendorId;
    private String _buyer;
    private String _ola;
    private String _projectId;
    private String _subProjectId;
    private String _budgetCategoryId;
    private String _budgetSubCategoryId;
    private String _activityDescription;
    private String _macroDriverId;
    private String _microDriverId;
    private String _odnpCodeId;
    private String _odnpNameId;
    private String _totalValue;
    private String _currency;
    private String _receiverUserId;
    private String _fiscalYear;
    private String _screenNameRequester;
    private String _screenNameReciever;
    private boolean _automatic;
    private int _status;
    private Date _createdDate;
    private long _createdUserId;
    private String _rejectionCause;
    private String _wbsCodeId;
    private String _trackingCode;
    private String _buyerId;
    private String _targaTecnica;
    private String _evoLocationId;
    private int _tipoPr;
    private String _plant;

	public PurchaseRequestSoap() {
    }

    public static PurchaseRequestSoap toSoapModel(PurchaseRequest model) {
        PurchaseRequestSoap soapModel = new PurchaseRequestSoap();

        soapModel.setPurchaseRequestId(model.getPurchaseRequestId());
        soapModel.setCostCenterId(model.getCostCenterId());
        soapModel.setVendorId(model.getVendorId());
        soapModel.setBuyer(model.getBuyer());
        soapModel.setOla(model.getOla());
        soapModel.setProjectId(model.getProjectId());
        soapModel.setSubProjectId(model.getSubProjectId());
        soapModel.setBudgetCategoryId(model.getBudgetCategoryId());
        soapModel.setBudgetSubCategoryId(model.getBudgetSubCategoryId());
        soapModel.setActivityDescription(model.getActivityDescription());
        soapModel.setMacroDriverId(model.getMacroDriverId());
        soapModel.setMicroDriverId(model.getMicroDriverId());
        soapModel.setOdnpCodeId(model.getOdnpCodeId());
        soapModel.setOdnpNameId(model.getOdnpNameId());
        soapModel.setTotalValue(model.getTotalValue());
        soapModel.setCurrency(model.getCurrency());
        soapModel.setReceiverUserId(model.getReceiverUserId());
        soapModel.setFiscalYear(model.getFiscalYear());
        soapModel.setScreenNameRequester(model.getScreenNameRequester());
        soapModel.setScreenNameReciever(model.getScreenNameReciever());
        soapModel.setAutomatic(model.getAutomatic());
        soapModel.setStatus(model.getStatus());
        soapModel.setCreatedDate(model.getCreatedDate());
        soapModel.setCreatedUserId(model.getCreatedUserId());
        soapModel.setRejectionCause(model.getRejectionCause());
        soapModel.setWbsCodeId(model.getWbsCodeId());
        soapModel.setTrackingCode(model.getTrackingCode());
        soapModel.setBuyerId(model.getBuyerId());
        soapModel.setTargaTecnica(model.getTargaTecnica());
        soapModel.setEvoLocationId(model.getEvoLocationId());
        soapModel.setTipoPr(model.getTipoPr());
        soapModel.setPlant(model.getPlant());

        return soapModel;
    }

    public static PurchaseRequestSoap[] toSoapModels(PurchaseRequest[] models) {
        PurchaseRequestSoap[] soapModels = new PurchaseRequestSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static PurchaseRequestSoap[][] toSoapModels(
        PurchaseRequest[][] models) {
        PurchaseRequestSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new PurchaseRequestSoap[models.length][models[0].length];
        } else {
            soapModels = new PurchaseRequestSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static PurchaseRequestSoap[] toSoapModels(
        List<PurchaseRequest> models) {
        List<PurchaseRequestSoap> soapModels = new ArrayList<PurchaseRequestSoap>(models.size());

        for (PurchaseRequest model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new PurchaseRequestSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _purchaseRequestId;
    }

    public void setPrimaryKey(long pk) {
        setPurchaseRequestId(pk);
    }

    public long getPurchaseRequestId() {
        return _purchaseRequestId;
    }

    public void setPurchaseRequestId(long purchaseRequestId) {
        _purchaseRequestId = purchaseRequestId;
    }

    public String getCostCenterId() {
        return _costCenterId;
    }

    public void setCostCenterId(String costCenterId) {
        _costCenterId = costCenterId;
    }

    public String getVendorId() {
        return _vendorId;
    }

    public void setVendorId(String vendorId) {
        _vendorId = vendorId;
    }

    public String getBuyer() {
        return _buyer;
    }

    public void setBuyer(String buyer) {
        _buyer = buyer;
    }

    public String getOla() {
        return _ola;
    }

    public void setOla(String ola) {
        _ola = ola;
    }

    public String getProjectId() {
        return _projectId;
    }

    public void setProjectId(String projectId) {
        _projectId = projectId;
    }

    public String getSubProjectId() {
        return _subProjectId;
    }

    public void setSubProjectId(String subProjectId) {
        _subProjectId = subProjectId;
    }

    public String getBudgetCategoryId() {
        return _budgetCategoryId;
    }

    public void setBudgetCategoryId(String budgetCategoryId) {
        _budgetCategoryId = budgetCategoryId;
    }

    public String getBudgetSubCategoryId() {
        return _budgetSubCategoryId;
    }

    public void setBudgetSubCategoryId(String budgetSubCategoryId) {
        _budgetSubCategoryId = budgetSubCategoryId;
    }

    public String getActivityDescription() {
        return _activityDescription;
    }

    public void setActivityDescription(String activityDescription) {
        _activityDescription = activityDescription;
    }

    public String getMacroDriverId() {
        return _macroDriverId;
    }

    public void setMacroDriverId(String macroDriverId) {
        _macroDriverId = macroDriverId;
    }

    public String getMicroDriverId() {
        return _microDriverId;
    }

    public void setMicroDriverId(String microDriverId) {
        _microDriverId = microDriverId;
    }

    public String getOdnpCodeId() {
        return _odnpCodeId;
    }

    public void setOdnpCodeId(String odnpCodeId) {
        _odnpCodeId = odnpCodeId;
    }

    public String getOdnpNameId() {
        return _odnpNameId;
    }

    public void setOdnpNameId(String odnpNameId) {
        _odnpNameId = odnpNameId;
    }

    public String getTotalValue() {
        return _totalValue;
    }

    public void setTotalValue(String totalValue) {
        _totalValue = totalValue;
    }

    public String getCurrency() {
        return _currency;
    }

    public void setCurrency(String currency) {
        _currency = currency;
    }

    public String getReceiverUserId() {
        return _receiverUserId;
    }

    public void setReceiverUserId(String receiverUserId) {
        _receiverUserId = receiverUserId;
    }

    public String getFiscalYear() {
        return _fiscalYear;
    }

    public void setFiscalYear(String fiscalYear) {
        _fiscalYear = fiscalYear;
    }

    public String getScreenNameRequester() {
        return _screenNameRequester;
    }

    public void setScreenNameRequester(String screenNameRequester) {
        _screenNameRequester = screenNameRequester;
    }

    public String getScreenNameReciever() {
        return _screenNameReciever;
    }

    public void setScreenNameReciever(String screenNameReciever) {
        _screenNameReciever = screenNameReciever;
    }

    public boolean getAutomatic() {
        return _automatic;
    }

    public boolean isAutomatic() {
        return _automatic;
    }

    public void setAutomatic(boolean automatic) {
        _automatic = automatic;
    }

    public int getStatus() {
        return _status;
    }

    public void setStatus(int status) {
        _status = status;
    }

    public Date getCreatedDate() {
        return _createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        _createdDate = createdDate;
    }

    public long getCreatedUserId() {
        return _createdUserId;
    }

    public void setCreatedUserId(long createdUserId) {
        _createdUserId = createdUserId;
    }

    public String getRejectionCause() {
        return _rejectionCause;
    }

    public void setRejectionCause(String rejectionCause) {
        _rejectionCause = rejectionCause;
    }

    public String getWbsCodeId() {
        return _wbsCodeId;
    }

    public void setWbsCodeId(String wbsCodeId) {
        _wbsCodeId = wbsCodeId;
    }

    public String getTrackingCode() {
        return _trackingCode;
    }

    public void setTrackingCode(String trackingCode) {
        _trackingCode = trackingCode;
    }

    public String getBuyerId() {
        return _buyerId;
    }

    public void setBuyerId(String buyerId) {
        _buyerId = buyerId;
    }
    
    public String getTargaTecnica() {
		return _targaTecnica;
	}

	public void setTargaTecnica(String _targaTecnica) {
		this._targaTecnica = _targaTecnica;
	}

	public String getEvoLocationId() {
		return _evoLocationId;
	}

	public void setEvoLocationId(String _evoLocationId) {
		this._evoLocationId = _evoLocationId;
	}

	public int getTipoPr() {
		return _tipoPr;
	}

	public void setTipoPr(int _tipoPr) {
		this._tipoPr = _tipoPr;
	}

	public String getPlant() {
		return _plant;
	}

	public void setPlant(String _plant) {
		this._plant = _plant;
	}
}
