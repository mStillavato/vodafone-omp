package com.hp.omp.model;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Workbook;

public interface ExcelStyler {
	Workbook getWorkBook();
	void setWorkBook(Workbook workbook);
	CellStyle getHeaderCellStyle();
	CellStyle getEvenCellStyle();
	CellStyle getOddCellStyle();
	CellStyle getEvenNumericCellStyle();
	CellStyle getOddNumericCellStyle();
}
