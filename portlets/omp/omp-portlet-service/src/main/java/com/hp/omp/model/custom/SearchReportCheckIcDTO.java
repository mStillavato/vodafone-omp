package com.hp.omp.model.custom;

public class SearchReportCheckIcDTO {
	private String 	purchaseRequestId;
	private String 	purchaseOrderId;
	private String 	status;
	private String 	fiscalYear;
	private Double	totalValue;
	private int		position;
	private Double	quantity;
	private Double	actualUnitCost;
	private String 	targaTecnica;
	private String 	evoLocation;
	private Integer pageNumber = 1;
	private Integer pageSize = 20;
	
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public String getPurchaseRequestId() {
		return purchaseRequestId;
	}
	public void setPurchaseRequestId(String purchaseRequestId) {
		this.purchaseRequestId = purchaseRequestId;
	}
	public String getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(String purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFiscalYear() {
		return fiscalYear;
	}
	public void setFiscalYear(String fiscalYear) {
		this.fiscalYear = fiscalYear;
	}
	public Double getTotalValue() {
		return totalValue;
	}
	public void setTotalValue(Double totalValue) {
		this.totalValue = totalValue;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getActualUnitCost() {
		return actualUnitCost;
	}
	public void setActualUnitCost(Double actualUnitCost) {
		this.actualUnitCost = actualUnitCost;
	}
	public String getTargaTecnica() {
		return targaTecnica;
	}
	public void setTargaTecnica(String targaTecnica) {
		this.targaTecnica = targaTecnica;
	}
	public String getEvoLocation() {
		return evoLocation;
	}
	public void setEvoLocation(String evoLocation) {
		this.evoLocation = evoLocation;
	}
}
