package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the CostCenterUser service. Represents a row in the &quot;COST_CENTER_USER&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see CostCenterUserModel
 * @see com.hp.omp.model.impl.CostCenterUserImpl
 * @see com.hp.omp.model.impl.CostCenterUserModelImpl
 * @generated
 */
public interface CostCenterUser extends CostCenterUserModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.CostCenterUserImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
