package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class PositionAuditSoap implements Serializable {
    private long _auditId;
    private Long _positionId;
    private int _status;
    private Long _userId;
    private Date _changeDate;

    public PositionAuditSoap() {
    }

    public static PositionAuditSoap toSoapModel(PositionAudit model) {
        PositionAuditSoap soapModel = new PositionAuditSoap();

        soapModel.setAuditId(model.getAuditId());
        soapModel.setPositionId(model.getPositionId());
        soapModel.setStatus(model.getStatus());
        soapModel.setUserId(model.getUserId());
        soapModel.setChangeDate(model.getChangeDate());

        return soapModel;
    }

    public static PositionAuditSoap[] toSoapModels(PositionAudit[] models) {
        PositionAuditSoap[] soapModels = new PositionAuditSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static PositionAuditSoap[][] toSoapModels(PositionAudit[][] models) {
        PositionAuditSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new PositionAuditSoap[models.length][models[0].length];
        } else {
            soapModels = new PositionAuditSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static PositionAuditSoap[] toSoapModels(List<PositionAudit> models) {
        List<PositionAuditSoap> soapModels = new ArrayList<PositionAuditSoap>(models.size());

        for (PositionAudit model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new PositionAuditSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _auditId;
    }

    public void setPrimaryKey(long pk) {
        setAuditId(pk);
    }

    public long getAuditId() {
        return _auditId;
    }

    public void setAuditId(long auditId) {
        _auditId = auditId;
    }

    public Long getPositionId() {
        return _positionId;
    }

    public void setPositionId(Long positionId) {
        _positionId = positionId;
    }

    public int getStatus() {
        return _status;
    }

    public void setStatus(int status) {
        _status = status;
    }

    public Long getUserId() {
        return _userId;
    }

    public void setUserId(Long userId) {
        _userId = userId;
    }

    public Date getChangeDate() {
        return _changeDate;
    }

    public void setChangeDate(Date changeDate) {
        _changeDate = changeDate;
    }
}
