package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Location service. Represents a row in the &quot;LOCATION&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see LocationModel
 * @see com.hp.omp.model.impl.LocationImpl
 * @see com.hp.omp.model.impl.LocationModelImpl
 * @generated
 */
public interface Location extends LocationModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.LocationImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
