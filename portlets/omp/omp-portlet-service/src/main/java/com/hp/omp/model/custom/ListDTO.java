package com.hp.omp.model.custom;

public interface ListDTO {
	
	String getIdentifier();
	void setIdentifier(String id);
	
	String getRequestorName();
	void setRequestorName(String requestorName);
	
	Long getRequestorID();
	void setRequestorID(Long requestorID);
	
	String getVendor();
	void setVendor(String vendor);
	
	String getSubCategory();
	void setSubCategory(String subCategory);
	
	String getTotalValue();
	void setTotalValue(String totalValue);
	
	String getFiscalYear();
	void setFiscalYear(String fiscalYear);
	
	String getDescription();
	void setDescription(String description);
	
	String getStatus();
	void setStatus(String status);
	
	Integer getCurrency();
	void setCurrency(Integer currency);
	
}
