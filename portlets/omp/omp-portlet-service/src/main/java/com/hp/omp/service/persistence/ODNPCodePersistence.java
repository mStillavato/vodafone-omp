package com.hp.omp.service.persistence;

import com.hp.omp.model.ODNPCode;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the o d n p code service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see ODNPCodePersistenceImpl
 * @see ODNPCodeUtil
 * @generated
 */
public interface ODNPCodePersistence extends BasePersistence<ODNPCode> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link ODNPCodeUtil} to access the o d n p code persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the o d n p code in the entity cache if it is enabled.
    *
    * @param odnpCode the o d n p code
    */
    public void cacheResult(com.hp.omp.model.ODNPCode odnpCode);

    /**
    * Caches the o d n p codes in the entity cache if it is enabled.
    *
    * @param odnpCodes the o d n p codes
    */
    public void cacheResult(java.util.List<com.hp.omp.model.ODNPCode> odnpCodes);

    /**
    * Creates a new o d n p code with the primary key. Does not add the o d n p code to the database.
    *
    * @param odnpCodeId the primary key for the new o d n p code
    * @return the new o d n p code
    */
    public com.hp.omp.model.ODNPCode create(long odnpCodeId);

    /**
    * Removes the o d n p code with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param odnpCodeId the primary key of the o d n p code
    * @return the o d n p code that was removed
    * @throws com.hp.omp.NoSuchODNPCodeException if a o d n p code with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ODNPCode remove(long odnpCodeId)
        throws com.hp.omp.NoSuchODNPCodeException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.ODNPCode updateImpl(
        com.hp.omp.model.ODNPCode odnpCode, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the o d n p code with the primary key or throws a {@link com.hp.omp.NoSuchODNPCodeException} if it could not be found.
    *
    * @param odnpCodeId the primary key of the o d n p code
    * @return the o d n p code
    * @throws com.hp.omp.NoSuchODNPCodeException if a o d n p code with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ODNPCode findByPrimaryKey(long odnpCodeId)
        throws com.hp.omp.NoSuchODNPCodeException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the o d n p code with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param odnpCodeId the primary key of the o d n p code
    * @return the o d n p code, or <code>null</code> if a o d n p code with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ODNPCode fetchByPrimaryKey(long odnpCodeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the o d n p codes.
    *
    * @return the o d n p codes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.ODNPCode> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the o d n p codes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of o d n p codes
    * @param end the upper bound of the range of o d n p codes (not inclusive)
    * @return the range of o d n p codes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.ODNPCode> findAll(int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the o d n p codes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of o d n p codes
    * @param end the upper bound of the range of o d n p codes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of o d n p codes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.ODNPCode> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the o d n p codes from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of o d n p codes.
    *
    * @return the number of o d n p codes
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
