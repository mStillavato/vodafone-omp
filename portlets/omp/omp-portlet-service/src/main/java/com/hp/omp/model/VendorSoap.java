package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class VendorSoap implements Serializable {
    private long _vendorId;
    private String _supplier;
    private String _supplierCode;
    private String _vendorName;
    private boolean _display;

    public VendorSoap() {
    }

    public static VendorSoap toSoapModel(Vendor model) {
        VendorSoap soapModel = new VendorSoap();

        soapModel.setVendorId(model.getVendorId());
        soapModel.setSupplier(model.getSupplier());
        soapModel.setSupplierCode(model.getSupplierCode());
        soapModel.setVendorName(model.getVendorName());
        soapModel.setDisplay(model.getDisplay());

        return soapModel;
    }

    public static VendorSoap[] toSoapModels(Vendor[] models) {
        VendorSoap[] soapModels = new VendorSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static VendorSoap[][] toSoapModels(Vendor[][] models) {
        VendorSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new VendorSoap[models.length][models[0].length];
        } else {
            soapModels = new VendorSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static VendorSoap[] toSoapModels(List<Vendor> models) {
        List<VendorSoap> soapModels = new ArrayList<VendorSoap>(models.size());

        for (Vendor model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new VendorSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _vendorId;
    }

    public void setPrimaryKey(long pk) {
        setVendorId(pk);
    }

    public long getVendorId() {
        return _vendorId;
    }

    public void setVendorId(long vendorId) {
        _vendorId = vendorId;
    }

    public String getSupplier() {
        return _supplier;
    }

    public void setSupplier(String supplier) {
        _supplier = supplier;
    }

    public String getSupplierCode() {
        return _supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
        _supplierCode = supplierCode;
    }

    public String getVendorName() {
        return _vendorName;
    }

    public void setVendorName(String vendorName) {
        _vendorName = vendorName;
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;
    }
}
