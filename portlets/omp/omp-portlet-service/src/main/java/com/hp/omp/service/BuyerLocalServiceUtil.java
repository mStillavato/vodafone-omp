package com.hp.omp.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the buyer local service. This utility wraps {@link com.hp.omp.service.impl.BuyerLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see BuyerLocalService
 * @see com.hp.omp.service.base.BuyerLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.BuyerLocalServiceImpl
 * @generated
 */
public class BuyerLocalServiceUtil {
	private static BuyerLocalService _service;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link com.hp.omp.service.impl.BuyerLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * Adds the buyer to the database. Also notifies the appropriate model listeners.
	 *
	 * @param buyer the buyer
	 * @return the buyer that was added
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.Buyer addBuyer(com.hp.omp.model.Buyer buyer)
			throws com.liferay.portal.kernel.exception.SystemException {
		return getService().addBuyer(buyer);
	}

	/**
	 * Creates a new buyer with the primary key. Does not add the buyer to the database.
	 *
	 * @param buyerId the primary key for the new buyer
	 * @return the new buyer
	 */
	public static com.hp.omp.model.Buyer createBuyer(long buyerId) {
		return getService().createBuyer(buyerId);
	}

	/**
	 * Deletes the buyer with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param buyerId the primary key of the buyer
	 * @return the buyer that was removed
	 * @throws PortalException if a buyer with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.Buyer deleteBuyer(long buyerId)
			throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteBuyer(buyerId);
	}

	/**
	 * Deletes the buyer from the database. Also notifies the appropriate model listeners.
	 *
	 * @param buyer the buyer
	 * @return the buyer that was removed
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.Buyer deleteBuyer(
			com.hp.omp.model.Buyer buyer)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().deleteBuyer(buyer);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
			int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 * @throws SystemException if a system exception occurred
	 */
	@SuppressWarnings("rawtypes")
	public static java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
			int end,
			com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				.dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows that match the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows that match the dynamic query
	 * @throws SystemException if a system exception occurred
	 */
	public static long dynamicQueryCount(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	public static com.hp.omp.model.Buyer fetchBuyer(long buyerId)
			throws com.liferay.portal.kernel.exception.SystemException {
		return getService().fetchBuyer(buyerId);
	}

	/**
	 * Returns the buyer with the primary key.
	 *
	 * @param buyerId the primary key of the buyer
	 * @return the buyer
	 * @throws PortalException if a buyer with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.Buyer getBuyer(long buyerId)
			throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return getService().getBuyer(buyerId);
	}

	public static com.liferay.portal.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
					throws com.liferay.portal.kernel.exception.PortalException,
					com.liferay.portal.kernel.exception.SystemException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	 * Returns a range of all the buyers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of buyers
	 * @param end the upper bound of the range of buyers (not inclusive)
	 * @return the range of buyers
	 * @throws SystemException if a system exception occurred
	 */
	public static java.util.List<com.hp.omp.model.Buyer> getBuyers(int start,
			int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getBuyers(start, end);
	}

	/**
	 * Returns the number of buyers.
	 *
	 * @return the number of buyers
	 * @throws SystemException if a system exception occurred
	 */
	public static int getBuyersCount()
			throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getBuyersCount();
	}

	/**
	 * Updates the buyer in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param buyer the buyer
	 * @return the buyer that was updated
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.Buyer updateBuyer(
			com.hp.omp.model.Buyer buyer)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateBuyer(buyer);
	}

	/**
	 * Updates the buyer in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * @param buyer the buyer
	 * @param merge whether to merge the buyer with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	 * @return the buyer that was updated
	 * @throws SystemException if a system exception occurred
	 */
	public static com.hp.omp.model.Buyer updateBuyer(
			com.hp.omp.model.Buyer buyer, boolean merge)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().updateBuyer(buyer, merge);
	}

	/**
	 * Returns the Spring bean ID for this bean.
	 *
	 * @return the Spring bean ID for this bean
	 */
	public static java.lang.String getBeanIdentifier() {
		return getService().getBeanIdentifier();
	}

	/**
	 * Sets the Spring bean ID for this bean.
	 *
	 * @param beanIdentifier the Spring bean ID for this bean
	 */
	public static void setBeanIdentifier(java.lang.String beanIdentifier) {
		getService().setBeanIdentifier(beanIdentifier);
	}

	public static java.lang.Object invokeMethod(java.lang.String name,
			java.lang.String[] parameterTypes, java.lang.Object[] arguments)
					throws java.lang.Throwable {
		return getService().invokeMethod(name, parameterTypes, arguments);
	}

	public static java.util.List<com.hp.omp.model.Buyer> getBuyersList(
			java.lang.Integer pageNumber, java.lang.Integer pageSize)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getBuyersList(pageNumber, pageSize);
	}

	public static java.util.List<com.hp.omp.model.Buyer> getBuyersList(
			java.lang.Integer pageNumber, java.lang.Integer pageSize, String searchFilter, int gruppoUsers)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getBuyersList(pageNumber, pageSize, searchFilter, gruppoUsers);
	}
	
	public static int getBuyersCountList(String searchFilter, int gruppoUsers)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getBuyersCountList(searchFilter, gruppoUsers);
	}

	public static com.hp.omp.model.Buyer getBuyerByName(java.lang.String name)
			throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getBuyerByName(name);
	}

	public static java.util.List<com.hp.omp.model.Buyer> getDisplayedBuyers()
			throws com.liferay.portal.kernel.exception.SystemException {
		return getService().getDisplayedBuyers();
	}
	
	public static void exportBuyersAsExcel(
			java.util.List<com.hp.omp.model.Buyer> listBuyers,
			java.util.List<java.lang.Object> header, java.io.OutputStream out)
					throws com.liferay.portal.kernel.exception.SystemException {
		getService().exportBuyersAsExcel(listBuyers, header, out);
	}

	public static void clearService() {
		_service = null;
	}

	public static BuyerLocalService getService() {
		if (_service == null) {
			InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
					BuyerLocalService.class.getName());

			if (invokableLocalService instanceof BuyerLocalService) {
				_service = (BuyerLocalService) invokableLocalService;
			} else {
				_service = new BuyerLocalServiceClp(invokableLocalService);
			}

			ReferenceRegistry.registerReference(BuyerLocalServiceUtil.class,
					"_service");
		}

		return _service;
	}

	/**
	 * @deprecated
	 */
	 public void setService(BuyerLocalService service) {
	}
}
