package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link MicroDriver}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       MicroDriver
 * @generated
 */
public class MicroDriverWrapper implements MicroDriver,
    ModelWrapper<MicroDriver> {
    private MicroDriver _microDriver;

    public MicroDriverWrapper(MicroDriver microDriver) {
        _microDriver = microDriver;
    }

    public Class<?> getModelClass() {
        return MicroDriver.class;
    }

    public String getModelClassName() {
        return MicroDriver.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("microDriverId", getMicroDriverId());
        attributes.put("microDriverName", getMicroDriverName());
        attributes.put("display", getDisplay());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long microDriverId = (Long) attributes.get("microDriverId");

        if (microDriverId != null) {
            setMicroDriverId(microDriverId);
        }

        String microDriverName = (String) attributes.get("microDriverName");

        if (microDriverName != null) {
            setMicroDriverName(microDriverName);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
    }

    /**
    * Returns the primary key of this micro driver.
    *
    * @return the primary key of this micro driver
    */
    public long getPrimaryKey() {
        return _microDriver.getPrimaryKey();
    }

    /**
    * Sets the primary key of this micro driver.
    *
    * @param primaryKey the primary key of this micro driver
    */
    public void setPrimaryKey(long primaryKey) {
        _microDriver.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the micro driver ID of this micro driver.
    *
    * @return the micro driver ID of this micro driver
    */
    public long getMicroDriverId() {
        return _microDriver.getMicroDriverId();
    }

    /**
    * Sets the micro driver ID of this micro driver.
    *
    * @param microDriverId the micro driver ID of this micro driver
    */
    public void setMicroDriverId(long microDriverId) {
        _microDriver.setMicroDriverId(microDriverId);
    }

    /**
    * Returns the micro driver name of this micro driver.
    *
    * @return the micro driver name of this micro driver
    */
    public java.lang.String getMicroDriverName() {
        return _microDriver.getMicroDriverName();
    }

    /**
    * Sets the micro driver name of this micro driver.
    *
    * @param microDriverName the micro driver name of this micro driver
    */
    public void setMicroDriverName(java.lang.String microDriverName) {
        _microDriver.setMicroDriverName(microDriverName);
    }

    /**
    * Returns the display of this micro driver.
    *
    * @return the display of this micro driver
    */
    public boolean getDisplay() {
        return _microDriver.getDisplay();
    }

    /**
    * Returns <code>true</code> if this micro driver is display.
    *
    * @return <code>true</code> if this micro driver is display; <code>false</code> otherwise
    */
    public boolean isDisplay() {
        return _microDriver.isDisplay();
    }

    /**
    * Sets whether this micro driver is display.
    *
    * @param display the display of this micro driver
    */
    public void setDisplay(boolean display) {
        _microDriver.setDisplay(display);
    }

    public boolean isNew() {
        return _microDriver.isNew();
    }

    public void setNew(boolean n) {
        _microDriver.setNew(n);
    }

    public boolean isCachedModel() {
        return _microDriver.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _microDriver.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _microDriver.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _microDriver.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _microDriver.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _microDriver.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _microDriver.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new MicroDriverWrapper((MicroDriver) _microDriver.clone());
    }

    public int compareTo(MicroDriver microDriver) {
        return _microDriver.compareTo(microDriver);
    }

    @Override
    public int hashCode() {
        return _microDriver.hashCode();
    }

    public com.liferay.portal.model.CacheModel<MicroDriver> toCacheModel() {
        return _microDriver.toCacheModel();
    }

    public MicroDriver toEscapedModel() {
        return new MicroDriverWrapper(_microDriver.toEscapedModel());
    }

    public MicroDriver toUnescapedModel() {
        return new MicroDriverWrapper(_microDriver.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _microDriver.toString();
    }

    public java.lang.String toXmlString() {
        return _microDriver.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _microDriver.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof MicroDriverWrapper)) {
            return false;
        }

        MicroDriverWrapper microDriverWrapper = (MicroDriverWrapper) obj;

        if (Validator.equals(_microDriver, microDriverWrapper._microDriver)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public MicroDriver getWrappedMicroDriver() {
        return _microDriver;
    }

    public MicroDriver getWrappedModel() {
        return _microDriver;
    }

    public void resetOriginalValues() {
        _microDriver.resetOriginalValues();
    }
}
