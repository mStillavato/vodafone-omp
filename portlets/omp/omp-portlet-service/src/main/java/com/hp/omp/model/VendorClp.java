package com.hp.omp.model;

import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.VendorLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class VendorClp extends BaseModelImpl<Vendor> implements Vendor {
    private long _vendorId;
    private String _supplier;
    private String _supplierCode;
    private String _vendorName;
    private boolean _display;
    private int _gruppoUsers;
    private BaseModel<?> _vendorRemoteModel;

    public VendorClp() {
    }

    public Class<?> getModelClass() {
        return Vendor.class;
    }

    public String getModelClassName() {
        return Vendor.class.getName();
    }

    public long getPrimaryKey() {
        return _vendorId;
    }

    public void setPrimaryKey(long primaryKey) {
        setVendorId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_vendorId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("vendorId", getVendorId());
        attributes.put("supplier", getSupplier());
        attributes.put("supplierCode", getSupplierCode());
        attributes.put("vendorName", getVendorName());
        attributes.put("display", getDisplay());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long vendorId = (Long) attributes.get("vendorId");

        if (vendorId != null) {
            setVendorId(vendorId);
        }

        String supplier = (String) attributes.get("supplier");

        if (supplier != null) {
            setSupplier(supplier);
        }

        String supplierCode = (String) attributes.get("supplierCode");

        if (supplierCode != null) {
            setSupplierCode(supplierCode);
        }

        String vendorName = (String) attributes.get("vendorName");

        if (vendorName != null) {
            setVendorName(vendorName);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
    }

    public long getVendorId() {
        return _vendorId;
    }

    public void setVendorId(long vendorId) {
        _vendorId = vendorId;

        if (_vendorRemoteModel != null) {
            try {
                Class<?> clazz = _vendorRemoteModel.getClass();

                Method method = clazz.getMethod("setVendorId", long.class);

                method.invoke(_vendorRemoteModel, vendorId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getSupplier() {
        return _supplier;
    }

    public void setSupplier(String supplier) {
        _supplier = supplier;

        if (_vendorRemoteModel != null) {
            try {
                Class<?> clazz = _vendorRemoteModel.getClass();

                Method method = clazz.getMethod("setSupplier", String.class);

                method.invoke(_vendorRemoteModel, supplier);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getSupplierCode() {
        return _supplierCode;
    }

    public void setSupplierCode(String supplierCode) {
        _supplierCode = supplierCode;

        if (_vendorRemoteModel != null) {
            try {
                Class<?> clazz = _vendorRemoteModel.getClass();

                Method method = clazz.getMethod("setSupplierCode", String.class);

                method.invoke(_vendorRemoteModel, supplierCode);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getVendorName() {
        return _vendorName;
    }

    public void setVendorName(String vendorName) {
        _vendorName = vendorName;

        if (_vendorRemoteModel != null) {
            try {
                Class<?> clazz = _vendorRemoteModel.getClass();

                Method method = clazz.getMethod("setVendorName", String.class);

                method.invoke(_vendorRemoteModel, vendorName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;

        if (_vendorRemoteModel != null) {
            try {
                Class<?> clazz = _vendorRemoteModel.getClass();

                Method method = clazz.getMethod("setDisplay", boolean.class);

                method.invoke(_vendorRemoteModel, display);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getVendorRemoteModel() {
        return _vendorRemoteModel;
    }

    public void setVendorRemoteModel(BaseModel<?> vendorRemoteModel) {
        _vendorRemoteModel = vendorRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _vendorRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_vendorRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            VendorLocalServiceUtil.addVendor(this);
        } else {
            VendorLocalServiceUtil.updateVendor(this);
        }
    }

    @Override
    public Vendor toEscapedModel() {
        return (Vendor) ProxyUtil.newProxyInstance(Vendor.class.getClassLoader(),
            new Class[] { Vendor.class }, new AutoEscapeBeanHandler(this));
    }

    public Vendor toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        VendorClp clone = new VendorClp();

        clone.setVendorId(getVendorId());
        clone.setSupplier(getSupplier());
        clone.setSupplierCode(getSupplierCode());
        clone.setVendorName(getVendorName());
        clone.setDisplay(getDisplay());

        return clone;
    }

    public int compareTo(Vendor vendor) {
        long primaryKey = vendor.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VendorClp)) {
            return false;
        }

        VendorClp vendor = (VendorClp) obj;

        long primaryKey = vendor.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{vendorId=");
        sb.append(getVendorId());
        sb.append(", supplier=");
        sb.append(getSupplier());
        sb.append(", supplierCode=");
        sb.append(getSupplierCode());
        sb.append(", vendorName=");
        sb.append(getVendorName());
        sb.append(", display=");
        sb.append(getDisplay());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(19);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.Vendor");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>vendorId</column-name><column-value><![CDATA[");
        sb.append(getVendorId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>supplier</column-name><column-value><![CDATA[");
        sb.append(getSupplier());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>supplierCode</column-name><column-value><![CDATA[");
        sb.append(getSupplierCode());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>vendorName</column-name><column-value><![CDATA[");
        sb.append(getVendorName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>display</column-name><column-value><![CDATA[");
        sb.append(getDisplay());
        sb.append("]]></column-value></column>");
        sb.append(
                "<column><column-name>gruppoUsers</column-name><column-value><![CDATA[");
        sb.append(getGruppoUsers());
        sb.append("]]></column-value></column>");
        sb.append("</model>");

        return sb.toString();
    }

	public int getGruppoUsers() {
		return _gruppoUsers;
	}

	public void setGruppoUsers(int gruppoUsers) {
        _gruppoUsers = gruppoUsers;

        if (_vendorRemoteModel != null) {
            try {
                Class<?> clazz = _vendorRemoteModel.getClass();

                Method method = clazz.getMethod("setGruppoUsers", int.class);

                method.invoke(_vendorRemoteModel, gruppoUsers);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
	}
}
