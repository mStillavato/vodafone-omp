package com.hp.omp.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link PurchaseRequestLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       PurchaseRequestLocalService
 * @generated
 */
public class PurchaseRequestLocalServiceWrapper
    implements PurchaseRequestLocalService,
        ServiceWrapper<PurchaseRequestLocalService> {
    private PurchaseRequestLocalService _purchaseRequestLocalService;

    public PurchaseRequestLocalServiceWrapper(
        PurchaseRequestLocalService purchaseRequestLocalService) {
        _purchaseRequestLocalService = purchaseRequestLocalService;
    }

    /**
    * Adds the purchase request to the database. Also notifies the appropriate model listeners.
    *
    * @param purchaseRequest the purchase request
    * @return the purchase request that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseRequest addPurchaseRequest(
        com.hp.omp.model.PurchaseRequest purchaseRequest)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.addPurchaseRequest(purchaseRequest);
    }

    /**
    * Creates a new purchase request with the primary key. Does not add the purchase request to the database.
    *
    * @param purchaseRequestId the primary key for the new purchase request
    * @return the new purchase request
    */
    public com.hp.omp.model.PurchaseRequest createPurchaseRequest(
        long purchaseRequestId) {
        return _purchaseRequestLocalService.createPurchaseRequest(purchaseRequestId);
    }

    /**
    * Deletes the purchase request with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param purchaseRequestId the primary key of the purchase request
    * @return the purchase request that was removed
    * @throws PortalException if a purchase request with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseRequest deletePurchaseRequest(
        long purchaseRequestId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.deletePurchaseRequest(purchaseRequestId);
    }

    /**
    * Deletes the purchase request from the database. Also notifies the appropriate model listeners.
    *
    * @param purchaseRequest the purchase request
    * @return the purchase request that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseRequest deletePurchaseRequest(
        com.hp.omp.model.PurchaseRequest purchaseRequest)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.deletePurchaseRequest(purchaseRequest);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _purchaseRequestLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.dynamicQuery(dynamicQuery, start,
            end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.dynamicQueryCount(dynamicQuery);
    }

    public com.hp.omp.model.PurchaseRequest fetchPurchaseRequest(
        long purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.fetchPurchaseRequest(purchaseRequestId);
    }

    /**
    * Returns the purchase request with the primary key.
    *
    * @param purchaseRequestId the primary key of the purchase request
    * @return the purchase request
    * @throws PortalException if a purchase request with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseRequest getPurchaseRequest(
        long purchaseRequestId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.getPurchaseRequest(purchaseRequestId);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the purchase requests.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of purchase requests
    * @param end the upper bound of the range of purchase requests (not inclusive)
    * @return the range of purchase requests
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequests(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.getPurchaseRequests(start, end);
    }

    /**
    * Returns the number of purchase requests.
    *
    * @return the number of purchase requests
    * @throws SystemException if a system exception occurred
    */
    public int getPurchaseRequestsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.getPurchaseRequestsCount();
    }

    /**
    * Updates the purchase request in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param purchaseRequest the purchase request
    * @return the purchase request that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseRequest updatePurchaseRequest(
        com.hp.omp.model.PurchaseRequest purchaseRequest)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.updatePurchaseRequest(purchaseRequest);
    }

    /**
    * Updates the purchase request in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param purchaseRequest the purchase request
    * @param merge whether to merge the purchase request with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the purchase request that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseRequest updatePurchaseRequest(
        com.hp.omp.model.PurchaseRequest purchaseRequest, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.updatePurchaseRequest(purchaseRequest,
            merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _purchaseRequestLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _purchaseRequestLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _purchaseRequestLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    public com.hp.omp.model.PurchaseRequest getPurchaseRequestByStatus(
        long prId, int status)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.getPurchaseRequestByStatus(prId,
            status);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequests(
        java.util.List<java.lang.Long> purchaseRequestsIDList)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.getPurchaseRequests(purchaseRequestsIDList);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequesListByStatus(
        int status) throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.getPurchaseRequesListByStatus(status);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequesListByStatus(
        int status, int start, int end, java.lang.String orderby,
        java.lang.String order)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.getPurchaseRequesListByStatus(status,
            start, end, orderby, order);
    }

    public java.lang.Long getPurchaseRequesListByStatusCount(int status,
        boolean automatic)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.getPurchaseRequesListByStatusCount(status,
            automatic);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequesListByStatusAndType(
        int status, boolean automatic, int start, int end,
        java.lang.String orderby, java.lang.String order)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.getPurchaseRequesListByStatusAndType(status,
            automatic, start, end, orderby, order);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequesListByPositionStatusAndType(
        java.util.Set<java.lang.Long> purchaseRequestIdList, boolean automatic,
        int start, int end, java.lang.String orderby, java.lang.String order)
        throws java.lang.Exception {
        return _purchaseRequestLocalService.getPurchaseRequesListByPositionStatusAndType(purchaseRequestIdList,
            automatic, start, end, orderby, order);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequestListByPositionStatusAndType(
        int positionStatus, boolean automatic, int start, int end,
        java.lang.String orderby, java.lang.String order)
        throws java.lang.Exception {
        return _purchaseRequestLocalService.getPurchaseRequestListByPositionStatusAndType(positionStatus,
            automatic, start, end, orderby, order);
    }

    public java.lang.Long getPurchaseRequestListByPositionStatusAndTypeCount(
        int positionStatus, boolean automatic) throws java.lang.Exception {
        return _purchaseRequestLocalService.getPurchaseRequestListByPositionStatusAndTypeCount(positionStatus,
            automatic);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPRListByStatusAndCostCenter(
        int status, long costCenterId, int roleCode, int start, int end,
        java.lang.String orderby, java.lang.String order)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.getPRListByStatusAndCostCenter(status,
            costCenterId, roleCode, start, end, orderby, order);
    }

    public java.lang.Long getPRListByStatusAndCostCenterCount(int status,
        long costCenterId, int roleCode)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _purchaseRequestLocalService.getPRListByStatusAndCostCenterCount(status,
            costCenterId, roleCode);
    }

    public java.util.List<com.hp.omp.model.custom.PrListDTO> getPurchaseRequestList(
        com.hp.omp.model.custom.SearchDTO searchDTO, int roleCode, long userId) {
        return _purchaseRequestLocalService.getPurchaseRequestList(searchDTO,
            roleCode, userId);
    }

    public java.util.List<com.hp.omp.model.custom.PrListDTO> prSearch(
        com.hp.omp.model.custom.SearchDTO searchDTO, int roleCode, long userId) {
        return _purchaseRequestLocalService.prSearch(searchDTO, roleCode, userId);
    }

    public java.lang.Long getPurchaseRequestCount(
        com.hp.omp.model.custom.SearchDTO searchDTO, int roleCode, long userId) {
        return _purchaseRequestLocalService.getPurchaseRequestCount(searchDTO,
            roleCode, userId);
    }

    public java.util.List<com.hp.omp.model.custom.PrListDTO> getAntexPurchaseRequestList(
        com.hp.omp.model.custom.SearchDTO searchDTO) {
        return _purchaseRequestLocalService.getAntexPurchaseRequestList(searchDTO);
    }

    public java.lang.Long getAntexPurchaseRequestCount(
        com.hp.omp.model.custom.SearchDTO searchDTO) {
        return _purchaseRequestLocalService.getAntexPurchaseRequestCount(searchDTO);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequestByProjectId(
        java.lang.String projectId) throws java.lang.Exception {
        return _purchaseRequestLocalService.getPurchaseRequestByProjectId(projectId);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequestBySubProjectId(
        java.lang.String subProjectId) throws java.lang.Exception {
        return _purchaseRequestLocalService.getPurchaseRequestBySubProjectId(subProjectId);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequestByVendorId(
        java.lang.String vendorId) throws java.lang.Exception {
        return _purchaseRequestLocalService.getPurchaseRequestByVendorId(vendorId);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequestByBudgetCategoryId(
        java.lang.String budgetCategoryId) throws java.lang.Exception {
        return _purchaseRequestLocalService.getPurchaseRequestByBudgetCategoryId(budgetCategoryId);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequestByBudgetSubCategoryId(
        java.lang.String budgetSubCategoryId) throws java.lang.Exception {
        return _purchaseRequestLocalService.getPurchaseRequestByBudgetSubCategoryId(budgetSubCategoryId);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequestByOdnpNameId(
        java.lang.String odnpNameId) throws java.lang.Exception {
        return _purchaseRequestLocalService.getPurchaseRequestByOdnpNameId(odnpNameId);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequestByMacroDriverId(
        java.lang.String macroDriver) throws java.lang.Exception {
        return _purchaseRequestLocalService.getPurchaseRequestByMacroDriverId(macroDriver);
    }

    public java.util.List<com.hp.omp.model.PurchaseRequest> getPurchaseRequestByMicroDriverId(
        java.lang.String microDriver) throws java.lang.Exception {
        return _purchaseRequestLocalService.getPurchaseRequestByMicroDriverId(microDriver);
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public PurchaseRequestLocalService getWrappedPurchaseRequestLocalService() {
        return _purchaseRequestLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedPurchaseRequestLocalService(
        PurchaseRequestLocalService purchaseRequestLocalService) {
        _purchaseRequestLocalService = purchaseRequestLocalService;
    }

    public PurchaseRequestLocalService getWrappedService() {
        return _purchaseRequestLocalService;
    }

    public void setWrappedService(
        PurchaseRequestLocalService purchaseRequestLocalService) {
        _purchaseRequestLocalService = purchaseRequestLocalService;
    }
}
