package com.hp.omp.service.persistence;

import com.hp.omp.model.Project;
import com.hp.omp.service.ProjectLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class ProjectActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public ProjectActionableDynamicQuery() throws SystemException {
        setBaseLocalService(ProjectLocalServiceUtil.getService());
        setClass(Project.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("projectId");
    }
}
