package com.hp.omp.service.persistence;

import com.hp.omp.model.Vendor;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the vendor service. This utility wraps {@link VendorPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see VendorPersistence
 * @see VendorPersistenceImpl
 * @generated
 */
public class VendorUtil {
    private static VendorPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Vendor vendor) {
        getPersistence().clearCache(vendor);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Vendor> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Vendor> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Vendor> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static Vendor update(Vendor vendor, boolean merge)
        throws SystemException {
        return getPersistence().update(vendor, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static Vendor update(Vendor vendor, boolean merge,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(vendor, merge, serviceContext);
    }

    /**
    * Caches the vendor in the entity cache if it is enabled.
    *
    * @param vendor the vendor
    */
    public static void cacheResult(com.hp.omp.model.Vendor vendor) {
        getPersistence().cacheResult(vendor);
    }

    /**
    * Caches the vendors in the entity cache if it is enabled.
    *
    * @param vendors the vendors
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.Vendor> vendors) {
        getPersistence().cacheResult(vendors);
    }

    /**
    * Creates a new vendor with the primary key. Does not add the vendor to the database.
    *
    * @param vendorId the primary key for the new vendor
    * @return the new vendor
    */
    public static com.hp.omp.model.Vendor create(long vendorId) {
        return getPersistence().create(vendorId);
    }

    /**
    * Removes the vendor with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param vendorId the primary key of the vendor
    * @return the vendor that was removed
    * @throws com.hp.omp.NoSuchVendorException if a vendor with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Vendor remove(long vendorId)
        throws com.hp.omp.NoSuchVendorException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(vendorId);
    }

    public static com.hp.omp.model.Vendor updateImpl(
        com.hp.omp.model.Vendor vendor, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(vendor, merge);
    }

    /**
    * Returns the vendor with the primary key or throws a {@link com.hp.omp.NoSuchVendorException} if it could not be found.
    *
    * @param vendorId the primary key of the vendor
    * @return the vendor
    * @throws com.hp.omp.NoSuchVendorException if a vendor with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Vendor findByPrimaryKey(long vendorId)
        throws com.hp.omp.NoSuchVendorException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(vendorId);
    }

    /**
    * Returns the vendor with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param vendorId the primary key of the vendor
    * @return the vendor, or <code>null</code> if a vendor with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.Vendor fetchByPrimaryKey(long vendorId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(vendorId);
    }

    /**
    * Returns all the vendors.
    *
    * @return the vendors
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.Vendor> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the vendors.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of vendors
    * @param end the upper bound of the range of vendors (not inclusive)
    * @return the range of vendors
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.Vendor> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the vendors.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of vendors
    * @param end the upper bound of the range of vendors (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of vendors
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.Vendor> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the vendors from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of vendors.
    *
    * @return the number of vendors
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static VendorPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (VendorPersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    VendorPersistence.class.getName());

            ReferenceRegistry.registerReference(VendorUtil.class, "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(VendorPersistence persistence) {
    }
}
