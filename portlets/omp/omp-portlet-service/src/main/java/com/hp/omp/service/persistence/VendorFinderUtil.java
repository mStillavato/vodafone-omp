package com.hp.omp.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class VendorFinderUtil {
	private static VendorFinder _finder;

	public static java.util.List<com.hp.omp.model.Vendor> getVendorsList(
			int start, int end, String searchFilter, int gruppoUsers)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getFinder().getVendorsList(start, end, searchFilter, gruppoUsers);
	}
	
	public static java.util.List<com.hp.omp.model.Vendor> getVendorBySupplierCode(
			String supplierCode, int gruppoUsers)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getFinder().getVendorBySupplierCode(supplierCode, gruppoUsers);
	}	

	public static int getVendorsCountList(String searchFilter, int gruppoUsers)
			throws com.liferay.portal.kernel.exception.SystemException {
		return getFinder().getVendorsCountList(searchFilter, gruppoUsers);
	}
	
	public static VendorFinder getFinder() {
		if (_finder == null) {
			_finder = (VendorFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
					VendorFinder.class.getName());

			ReferenceRegistry.registerReference(VendorFinderUtil.class,
					"_finder");
		}

		return _finder;
	}

	public void setFinder(VendorFinder finder) {
		_finder = finder;

		ReferenceRegistry.registerReference(VendorFinderUtil.class,
				"_finder");
	}
}
