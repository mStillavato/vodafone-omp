package com.hp.omp.service.persistence;

public interface LayoutNotNssCatalogoFinder {
    
    public java.util.List<com.hp.omp.model.custom.LayoutNotNssCatalogoListDTO> getLayoutNotNssCatalogoList(String user);
    
    public java.lang.Long getLayoutNotNssCatalogoListCount(String user);
    
    public void deleteLayoutNotNssCatalogo4User(String user);
}
