package com.hp.omp.service.persistence;

import com.hp.omp.model.CostCenter;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the cost center service. This utility wraps {@link CostCenterPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see CostCenterPersistence
 * @see CostCenterPersistenceImpl
 * @generated
 */
public class CostCenterUtil {
    private static CostCenterPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(CostCenter costCenter) {
        getPersistence().clearCache(costCenter);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<CostCenter> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<CostCenter> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<CostCenter> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static CostCenter update(CostCenter costCenter, boolean merge)
        throws SystemException {
        return getPersistence().update(costCenter, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static CostCenter update(CostCenter costCenter, boolean merge,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(costCenter, merge, serviceContext);
    }

    /**
    * Caches the cost center in the entity cache if it is enabled.
    *
    * @param costCenter the cost center
    */
    public static void cacheResult(com.hp.omp.model.CostCenter costCenter) {
        getPersistence().cacheResult(costCenter);
    }

    /**
    * Caches the cost centers in the entity cache if it is enabled.
    *
    * @param costCenters the cost centers
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.CostCenter> costCenters) {
        getPersistence().cacheResult(costCenters);
    }

    /**
    * Creates a new cost center with the primary key. Does not add the cost center to the database.
    *
    * @param costCenterId the primary key for the new cost center
    * @return the new cost center
    */
    public static com.hp.omp.model.CostCenter create(long costCenterId) {
        return getPersistence().create(costCenterId);
    }

    /**
    * Removes the cost center with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param costCenterId the primary key of the cost center
    * @return the cost center that was removed
    * @throws com.hp.omp.NoSuchCostCenterException if a cost center with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.CostCenter remove(long costCenterId)
        throws com.hp.omp.NoSuchCostCenterException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(costCenterId);
    }

    public static com.hp.omp.model.CostCenter updateImpl(
        com.hp.omp.model.CostCenter costCenter, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(costCenter, merge);
    }

    /**
    * Returns the cost center with the primary key or throws a {@link com.hp.omp.NoSuchCostCenterException} if it could not be found.
    *
    * @param costCenterId the primary key of the cost center
    * @return the cost center
    * @throws com.hp.omp.NoSuchCostCenterException if a cost center with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.CostCenter findByPrimaryKey(
        long costCenterId)
        throws com.hp.omp.NoSuchCostCenterException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(costCenterId);
    }

    /**
    * Returns the cost center with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param costCenterId the primary key of the cost center
    * @return the cost center, or <code>null</code> if a cost center with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.CostCenter fetchByPrimaryKey(
        long costCenterId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(costCenterId);
    }

    /**
    * Returns all the cost centers.
    *
    * @return the cost centers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.CostCenter> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the cost centers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of cost centers
    * @param end the upper bound of the range of cost centers (not inclusive)
    * @return the range of cost centers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.CostCenter> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the cost centers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of cost centers
    * @param end the upper bound of the range of cost centers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of cost centers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.CostCenter> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the cost centers from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of cost centers.
    *
    * @return the number of cost centers
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static CostCenterPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (CostCenterPersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    CostCenterPersistence.class.getName());

            ReferenceRegistry.registerReference(CostCenterUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(CostCenterPersistence persistence) {
    }
}
