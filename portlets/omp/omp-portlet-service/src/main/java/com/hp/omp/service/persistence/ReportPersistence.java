package com.hp.omp.service.persistence;

import java.util.List;

import com.hp.omp.model.custom.ExcelTableRow;
import com.hp.omp.model.custom.ReportCheckIcDTO;
import com.hp.omp.model.custom.ReportDTO;
import com.hp.omp.model.custom.SearchReportCheckIcDTO;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.exception.SystemException;

public interface ReportPersistence {
	void setSessionFactory(SessionFactory sessionFactory);
	List<String> getAllCostCenters() throws SystemException;
	List<String> getAllFiscalYears() throws SystemException;
	List<String> getAllProjectNames() throws SystemException;
	List<String> getSubProjects(Long projectId) throws SystemException;
	Double generateAggregateReport(ReportDTO reportDTO, boolean closed) throws SystemException;
	List<ExcelTableRow> generateDetailReport(ReportDTO reportDTO) throws SystemException; 
	Double generateAggregateReportTotalPoistionValues(ReportDTO reportDTO) throws SystemException;
	Double generateAggregatePrPositionsWithoutPoNumberValue(ReportDTO reportDTO) throws SystemException;
	List<ExcelTableRow> generateControllerDetailReport(ReportDTO reportDTO) throws SystemException;
	List<ReportCheckIcDTO> getReportCheckIcList(SearchReportCheckIcDTO searchDTO) throws SystemException;
	Long getReportCheckIcListCount(SearchReportCheckIcDTO searchDTO) throws SystemException;
	List<ExcelTableRow> generateCheckIcReport() throws SystemException; 
}
