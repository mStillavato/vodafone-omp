/**
 * 
 */
package com.hp.omp.model.custom;

import java.io.Serializable;

/**
 * @author Sami Mohamed
 *
 */
public class LocationDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5537214074130306263L;
	
	private long locationTargaId;
	private String codiceOffice;
	private String targaTecnicaSap;
	private String location;
	private String pr;
	private String dataOnAir;
	
	public long getLocationTargaId() {
		return locationTargaId;
	}
	public void setLocationTargaId(long locationTargaId) {
		this.locationTargaId = locationTargaId;
	}
	public String getCodiceOffice() {
		return codiceOffice;
	}
	public void setCodiceOffice(String codiceOffice) {
		this.codiceOffice = codiceOffice;
	}
	public String getTargaTecnicaSap() {
		return targaTecnicaSap;
	}
	public void setTargaTecnicaSap(String targaTecnicaSap) {
		this.targaTecnicaSap = targaTecnicaSap;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getPr() {
		return pr;
	}
	public void setPr(String pr) {
		this.pr = pr;
	}
	public String getDataOnAir() {
		return dataOnAir;
	}
	public void setDataOnAir(String dataOnAir) {
		this.dataOnAir = dataOnAir;
	}
}
