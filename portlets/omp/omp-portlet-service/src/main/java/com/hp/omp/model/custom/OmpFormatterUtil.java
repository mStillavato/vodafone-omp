package com.hp.omp.model.custom;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * @author Sami Mohamed
 * 
 */
public class OmpFormatterUtil {
	
	
	public static String formatNumberByLocale(double number,Locale locale) {
		NumberFormat nf = NumberFormat.getInstance(locale);
		nf.setMaximumFractionDigits(2);
		if(number > 999) {
		nf.setMinimumFractionDigits(2);
		}
		return nf.format(number);
	}
	
	public static NumberFormat getNumberFormatByLocale(Locale locale) {
		NumberFormat nf = NumberFormat.getInstance(locale);
		nf.setMaximumFractionDigits(2);
//		if(number > 999) {
//		nf.setMinimumFractionDigits(2);
//		}
		return nf;
	}
}
