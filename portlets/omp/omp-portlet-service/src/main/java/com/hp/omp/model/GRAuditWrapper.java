package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link GRAudit}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       GRAudit
 * @generated
 */
public class GRAuditWrapper implements GRAudit, ModelWrapper<GRAudit> {
    private GRAudit _grAudit;

    public GRAuditWrapper(GRAudit grAudit) {
        _grAudit = grAudit;
    }

    public Class<?> getModelClass() {
        return GRAudit.class;
    }

    public String getModelClassName() {
        return GRAudit.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("auditId", getAuditId());
        attributes.put("goodReceiptId", getGoodReceiptId());
        attributes.put("status", getStatus());
        attributes.put("userId", getUserId());
        attributes.put("changeDate", getChangeDate());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long auditId = (Long) attributes.get("auditId");

        if (auditId != null) {
            setAuditId(auditId);
        }

        Long goodReceiptId = (Long) attributes.get("goodReceiptId");

        if (goodReceiptId != null) {
            setGoodReceiptId(goodReceiptId);
        }

        Integer status = (Integer) attributes.get("status");

        if (status != null) {
            setStatus(status);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        Date changeDate = (Date) attributes.get("changeDate");

        if (changeDate != null) {
            setChangeDate(changeDate);
        }
    }

    /**
    * Returns the primary key of this g r audit.
    *
    * @return the primary key of this g r audit
    */
    public long getPrimaryKey() {
        return _grAudit.getPrimaryKey();
    }

    /**
    * Sets the primary key of this g r audit.
    *
    * @param primaryKey the primary key of this g r audit
    */
    public void setPrimaryKey(long primaryKey) {
        _grAudit.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the audit ID of this g r audit.
    *
    * @return the audit ID of this g r audit
    */
    public long getAuditId() {
        return _grAudit.getAuditId();
    }

    /**
    * Sets the audit ID of this g r audit.
    *
    * @param auditId the audit ID of this g r audit
    */
    public void setAuditId(long auditId) {
        _grAudit.setAuditId(auditId);
    }

    /**
    * Returns the good receipt ID of this g r audit.
    *
    * @return the good receipt ID of this g r audit
    */
    public java.lang.Long getGoodReceiptId() {
        return _grAudit.getGoodReceiptId();
    }

    /**
    * Sets the good receipt ID of this g r audit.
    *
    * @param goodReceiptId the good receipt ID of this g r audit
    */
    public void setGoodReceiptId(java.lang.Long goodReceiptId) {
        _grAudit.setGoodReceiptId(goodReceiptId);
    }

    /**
    * Returns the status of this g r audit.
    *
    * @return the status of this g r audit
    */
    public int getStatus() {
        return _grAudit.getStatus();
    }

    /**
    * Sets the status of this g r audit.
    *
    * @param status the status of this g r audit
    */
    public void setStatus(int status) {
        _grAudit.setStatus(status);
    }

    /**
    * Returns the user ID of this g r audit.
    *
    * @return the user ID of this g r audit
    */
    public java.lang.Long getUserId() {
        return _grAudit.getUserId();
    }

    /**
    * Sets the user ID of this g r audit.
    *
    * @param userId the user ID of this g r audit
    */
    public void setUserId(java.lang.Long userId) {
        _grAudit.setUserId(userId);
    }

    /**
    * Returns the change date of this g r audit.
    *
    * @return the change date of this g r audit
    */
    public java.util.Date getChangeDate() {
        return _grAudit.getChangeDate();
    }

    /**
    * Sets the change date of this g r audit.
    *
    * @param changeDate the change date of this g r audit
    */
    public void setChangeDate(java.util.Date changeDate) {
        _grAudit.setChangeDate(changeDate);
    }

    public boolean isNew() {
        return _grAudit.isNew();
    }

    public void setNew(boolean n) {
        _grAudit.setNew(n);
    }

    public boolean isCachedModel() {
        return _grAudit.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _grAudit.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _grAudit.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _grAudit.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _grAudit.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _grAudit.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _grAudit.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new GRAuditWrapper((GRAudit) _grAudit.clone());
    }

    public int compareTo(GRAudit grAudit) {
        return _grAudit.compareTo(grAudit);
    }

    @Override
    public int hashCode() {
        return _grAudit.hashCode();
    }

    public com.liferay.portal.model.CacheModel<GRAudit> toCacheModel() {
        return _grAudit.toCacheModel();
    }

    public GRAudit toEscapedModel() {
        return new GRAuditWrapper(_grAudit.toEscapedModel());
    }

    public GRAudit toUnescapedModel() {
        return new GRAuditWrapper(_grAudit.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _grAudit.toString();
    }

    public java.lang.String toXmlString() {
        return _grAudit.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _grAudit.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof GRAuditWrapper)) {
            return false;
        }

        GRAuditWrapper grAuditWrapper = (GRAuditWrapper) obj;

        if (Validator.equals(_grAudit, grAuditWrapper._grAudit)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public GRAudit getWrappedGRAudit() {
        return _grAudit;
    }

    public GRAudit getWrappedModel() {
        return _grAudit;
    }

    public void resetOriginalValues() {
        _grAudit.resetOriginalValues();
    }
}
