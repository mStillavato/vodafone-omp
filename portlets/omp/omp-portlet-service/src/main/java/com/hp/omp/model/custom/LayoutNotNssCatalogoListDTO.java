/**
 * 
 */
package com.hp.omp.model.custom;

import java.io.IOException;
import java.io.Serializable;
import java.util.Locale;

/**
 * @author Sami Mohamed
 *
 */
public class LayoutNotNssCatalogoListDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5537214074130306263L;
	
	private long layoutNotNssCatalogoId;
	private long lineNumber;
	private String materialId;
	private String materialDesc;
	private String quantity;
	private String netPrice;
	private String currency;
	private String deliveryDate;
	private String deliveryAddress;
	private String itemText;
	private String locationEvo;
	private String targaTecnica;
	private String user_;
	private String vendorCode;
	private long catalogoId;
	private String icApproval;
	private String categoryCode;
	private String offerNumber;
	
	public long getLayoutNotNssCatalogoId() {
		return layoutNotNssCatalogoId;
	}
	public void setLayoutNotNssCatalogoId(long layoutNotNssCatalogoId) {
		this.layoutNotNssCatalogoId = layoutNotNssCatalogoId;
	}
	public long getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(long lineNumber) {
		this.lineNumber = lineNumber;
	}
	public String getMaterialId() {
		return materialId;
	}
	public void setMaterialId(String materialId) {
		this.materialId = materialId;
	}
	public String getMaterialDesc() {
		return materialDesc;
	}
	public void setMaterialDesc(String materialDesc) {
		this.materialDesc = materialDesc;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getNetPrice() {
		return netPrice;
	}
	public void setNetPrice(String netPrice) {
		this.netPrice = netPrice;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public String getItemText() {
		return itemText;
	}
	public void setItemText(String itemText) {
		this.itemText = itemText;
	}
	public String getLocationEvo() {
		return locationEvo;
	}
	public void setLocationEvo(String locationEvo) {
		this.locationEvo = locationEvo;
	}
	public String getTargaTecnica() {
		return targaTecnica;
	}
	public void setTargaTecnica(String targaTecnica) {
		this.targaTecnica = targaTecnica;
	}
	public String getUser_() {
		return user_;
	}
	public void setUser_(String user_) {
		this.user_ = user_;
	}
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	public long getCatalogoId() {
		return catalogoId;
	}
	public void setCatalogoId(long catalogoId) {
		this.catalogoId = catalogoId;
	}
	public String getIcApproval() {
		return icApproval;
	}
	public void setIcApproval(String icApproval) {
		this.icApproval = icApproval;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getOfferNumber() {
		return offerNumber;
	}
	public void setOfferNumber(String offerNumber) {
		this.offerNumber = offerNumber;
	}

}
