package com.hp.omp.model;

import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.MacroDriverLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class MacroDriverClp extends BaseModelImpl<MacroDriver>
    implements MacroDriver {
    private long _macroDriverId;
    private String _macroDriverName;
    private boolean _display;
    private BaseModel<?> _macroDriverRemoteModel;

    public MacroDriverClp() {
    }

    public Class<?> getModelClass() {
        return MacroDriver.class;
    }

    public String getModelClassName() {
        return MacroDriver.class.getName();
    }

    public long getPrimaryKey() {
        return _macroDriverId;
    }

    public void setPrimaryKey(long primaryKey) {
        setMacroDriverId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_macroDriverId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("macroDriverId", getMacroDriverId());
        attributes.put("macroDriverName", getMacroDriverName());
        attributes.put("display", getDisplay());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long macroDriverId = (Long) attributes.get("macroDriverId");

        if (macroDriverId != null) {
            setMacroDriverId(macroDriverId);
        }

        String macroDriverName = (String) attributes.get("macroDriverName");

        if (macroDriverName != null) {
            setMacroDriverName(macroDriverName);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
    }

    public long getMacroDriverId() {
        return _macroDriverId;
    }

    public void setMacroDriverId(long macroDriverId) {
        _macroDriverId = macroDriverId;

        if (_macroDriverRemoteModel != null) {
            try {
                Class<?> clazz = _macroDriverRemoteModel.getClass();

                Method method = clazz.getMethod("setMacroDriverId", long.class);

                method.invoke(_macroDriverRemoteModel, macroDriverId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getMacroDriverName() {
        return _macroDriverName;
    }

    public void setMacroDriverName(String macroDriverName) {
        _macroDriverName = macroDriverName;

        if (_macroDriverRemoteModel != null) {
            try {
                Class<?> clazz = _macroDriverRemoteModel.getClass();

                Method method = clazz.getMethod("setMacroDriverName",
                        String.class);

                method.invoke(_macroDriverRemoteModel, macroDriverName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;

        if (_macroDriverRemoteModel != null) {
            try {
                Class<?> clazz = _macroDriverRemoteModel.getClass();

                Method method = clazz.getMethod("setDisplay", boolean.class);

                method.invoke(_macroDriverRemoteModel, display);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getMacroDriverRemoteModel() {
        return _macroDriverRemoteModel;
    }

    public void setMacroDriverRemoteModel(BaseModel<?> macroDriverRemoteModel) {
        _macroDriverRemoteModel = macroDriverRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _macroDriverRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_macroDriverRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            MacroDriverLocalServiceUtil.addMacroDriver(this);
        } else {
            MacroDriverLocalServiceUtil.updateMacroDriver(this);
        }
    }

    @Override
    public MacroDriver toEscapedModel() {
        return (MacroDriver) ProxyUtil.newProxyInstance(MacroDriver.class.getClassLoader(),
            new Class[] { MacroDriver.class }, new AutoEscapeBeanHandler(this));
    }

    public MacroDriver toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        MacroDriverClp clone = new MacroDriverClp();

        clone.setMacroDriverId(getMacroDriverId());
        clone.setMacroDriverName(getMacroDriverName());
        clone.setDisplay(getDisplay());

        return clone;
    }

    public int compareTo(MacroDriver macroDriver) {
        long primaryKey = macroDriver.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof MacroDriverClp)) {
            return false;
        }

        MacroDriverClp macroDriver = (MacroDriverClp) obj;

        long primaryKey = macroDriver.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{macroDriverId=");
        sb.append(getMacroDriverId());
        sb.append(", macroDriverName=");
        sb.append(getMacroDriverName());
        sb.append(", display=");
        sb.append(getDisplay());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(13);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.MacroDriver");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>macroDriverId</column-name><column-value><![CDATA[");
        sb.append(getMacroDriverId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>macroDriverName</column-name><column-value><![CDATA[");
        sb.append(getMacroDriverName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>display</column-name><column-value><![CDATA[");
        sb.append(getDisplay());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
