package com.hp.omp.service.persistence;

import com.hp.omp.model.Catalogo;
import com.hp.omp.model.custom.SearchCatalogoDTO;
import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class CatalogoFinderUtil {
    private static CatalogoFinder _finder;

    public static java.util.List<com.hp.omp.model.custom.CatalogoListDTO> getCatalogoList(SearchCatalogoDTO searchDTO) {
        return getFinder().getCatalogoList(searchDTO);
    }
    
    public static java.util.List<com.hp.omp.model.custom.CatalogoExportListDTO> getExportCatalogoList(SearchCatalogoDTO searchDTO) {
        return getFinder().getExportCatalogoList(searchDTO);
    }
    
    public static java.lang.Long getCatalogoListCount(SearchCatalogoDTO searchDTO) {
        return getFinder().getCatalogoListCount(searchDTO);
    }
    
    public static java.util.List<com.hp.omp.model.Catalogo> getCatalogoForVendor(
    		int start, int end, String vendorCode)
            throws com.liferay.portal.kernel.exception.SystemException {
            return getFinder().getCatalogoForVendor(start, end, vendorCode);
    }
    
    public static CatalogoFinder getFinder() {
        if (_finder == null) {
            _finder = (CatalogoFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
            		CatalogoFinder.class.getName());

            ReferenceRegistry.registerReference(CatalogoFinderUtil.class,
                "_finder");
        }

        return _finder;
    }

    public void setFinder(CatalogoFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(CatalogoFinderUtil.class,
            "_finder");
    }
}
