package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the GRAudit service. Represents a row in the &quot;GR_AUDIT&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see GRAuditModel
 * @see com.hp.omp.model.impl.GRAuditImpl
 * @see com.hp.omp.model.impl.GRAuditModelImpl
 * @generated
 */
public interface GRAudit extends GRAuditModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.GRAuditImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
