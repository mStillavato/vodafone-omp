package com.hp.omp.service.persistence;

import com.hp.omp.model.ODNPName;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the o d n p name service. This utility wraps {@link ODNPNamePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see ODNPNamePersistence
 * @see ODNPNamePersistenceImpl
 * @generated
 */
public class ODNPNameUtil {
    private static ODNPNamePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(ODNPName odnpName) {
        getPersistence().clearCache(odnpName);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<ODNPName> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<ODNPName> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<ODNPName> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static ODNPName update(ODNPName odnpName, boolean merge)
        throws SystemException {
        return getPersistence().update(odnpName, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static ODNPName update(ODNPName odnpName, boolean merge,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(odnpName, merge, serviceContext);
    }

    /**
    * Caches the o d n p name in the entity cache if it is enabled.
    *
    * @param odnpName the o d n p name
    */
    public static void cacheResult(com.hp.omp.model.ODNPName odnpName) {
        getPersistence().cacheResult(odnpName);
    }

    /**
    * Caches the o d n p names in the entity cache if it is enabled.
    *
    * @param odnpNames the o d n p names
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.ODNPName> odnpNames) {
        getPersistence().cacheResult(odnpNames);
    }

    /**
    * Creates a new o d n p name with the primary key. Does not add the o d n p name to the database.
    *
    * @param odnpNameId the primary key for the new o d n p name
    * @return the new o d n p name
    */
    public static com.hp.omp.model.ODNPName create(long odnpNameId) {
        return getPersistence().create(odnpNameId);
    }

    /**
    * Removes the o d n p name with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param odnpNameId the primary key of the o d n p name
    * @return the o d n p name that was removed
    * @throws com.hp.omp.NoSuchODNPNameException if a o d n p name with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ODNPName remove(long odnpNameId)
        throws com.hp.omp.NoSuchODNPNameException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(odnpNameId);
    }

    public static com.hp.omp.model.ODNPName updateImpl(
        com.hp.omp.model.ODNPName odnpName, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(odnpName, merge);
    }

    /**
    * Returns the o d n p name with the primary key or throws a {@link com.hp.omp.NoSuchODNPNameException} if it could not be found.
    *
    * @param odnpNameId the primary key of the o d n p name
    * @return the o d n p name
    * @throws com.hp.omp.NoSuchODNPNameException if a o d n p name with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ODNPName findByPrimaryKey(long odnpNameId)
        throws com.hp.omp.NoSuchODNPNameException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(odnpNameId);
    }

    /**
    * Returns the o d n p name with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param odnpNameId the primary key of the o d n p name
    * @return the o d n p name, or <code>null</code> if a o d n p name with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ODNPName fetchByPrimaryKey(long odnpNameId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(odnpNameId);
    }

    /**
    * Returns all the o d n p names.
    *
    * @return the o d n p names
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ODNPName> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the o d n p names.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of o d n p names
    * @param end the upper bound of the range of o d n p names (not inclusive)
    * @return the range of o d n p names
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ODNPName> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the o d n p names.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of o d n p names
    * @param end the upper bound of the range of o d n p names (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of o d n p names
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ODNPName> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the o d n p names from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of o d n p names.
    *
    * @return the number of o d n p names
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static ODNPNamePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (ODNPNamePersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    ODNPNamePersistence.class.getName());

            ReferenceRegistry.registerReference(ODNPNameUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(ODNPNamePersistence persistence) {
    }
}
