package com.hp.omp.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class CostCenterFinderUtil {
	private static CostCenterFinder _finder;

	public static java.util.List<com.hp.omp.model.CostCenter> getCostCentersList(
			int start, int end, String searchFilter, int gruppoUsers)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getFinder().getCostCentersList(start, end, searchFilter, gruppoUsers);
	}

	public static CostCenterFinder getFinder() {
		if (_finder == null) {
			_finder = (CostCenterFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
					CostCenterFinder.class.getName());

			ReferenceRegistry.registerReference(CostCenterFinderUtil.class,
					"_finder");
		}

		return _finder;
	}

	public void setFinder(CostCenterFinder finder) {
		_finder = finder;

		ReferenceRegistry.registerReference(CostCenterFinderUtil.class,
				"_finder");
	}
}
