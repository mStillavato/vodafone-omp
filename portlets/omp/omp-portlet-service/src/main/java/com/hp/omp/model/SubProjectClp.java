package com.hp.omp.model;

import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.SubProjectLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class SubProjectClp extends BaseModelImpl<SubProject>
    implements SubProject {
    private long _subProjectId;
    private String _subProjectName;
    private String _description;
    private boolean _display;
    private BaseModel<?> _subProjectRemoteModel;

    public SubProjectClp() {
    }

    public Class<?> getModelClass() {
        return SubProject.class;
    }

    public String getModelClassName() {
        return SubProject.class.getName();
    }

    public long getPrimaryKey() {
        return _subProjectId;
    }

    public void setPrimaryKey(long primaryKey) {
        setSubProjectId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_subProjectId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("subProjectId", getSubProjectId());
        attributes.put("subProjectName", getSubProjectName());
        attributes.put("description", getDescription());
        attributes.put("display", getDisplay());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long subProjectId = (Long) attributes.get("subProjectId");

        if (subProjectId != null) {
            setSubProjectId(subProjectId);
        }

        String subProjectName = (String) attributes.get("subProjectName");

        if (subProjectName != null) {
            setSubProjectName(subProjectName);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
    }

    public long getSubProjectId() {
        return _subProjectId;
    }

    public void setSubProjectId(long subProjectId) {
        _subProjectId = subProjectId;

        if (_subProjectRemoteModel != null) {
            try {
                Class<?> clazz = _subProjectRemoteModel.getClass();

                Method method = clazz.getMethod("setSubProjectId", long.class);

                method.invoke(_subProjectRemoteModel, subProjectId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getSubProjectName() {
        return _subProjectName;
    }

    public void setSubProjectName(String subProjectName) {
        _subProjectName = subProjectName;

        if (_subProjectRemoteModel != null) {
            try {
                Class<?> clazz = _subProjectRemoteModel.getClass();

                Method method = clazz.getMethod("setSubProjectName",
                        String.class);

                method.invoke(_subProjectRemoteModel, subProjectName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;

        if (_subProjectRemoteModel != null) {
            try {
                Class<?> clazz = _subProjectRemoteModel.getClass();

                Method method = clazz.getMethod("setDescription", String.class);

                method.invoke(_subProjectRemoteModel, description);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;

        if (_subProjectRemoteModel != null) {
            try {
                Class<?> clazz = _subProjectRemoteModel.getClass();

                Method method = clazz.getMethod("setDisplay", boolean.class);

                method.invoke(_subProjectRemoteModel, display);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getSubProjectRemoteModel() {
        return _subProjectRemoteModel;
    }

    public void setSubProjectRemoteModel(BaseModel<?> subProjectRemoteModel) {
        _subProjectRemoteModel = subProjectRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _subProjectRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_subProjectRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            SubProjectLocalServiceUtil.addSubProject(this);
        } else {
            SubProjectLocalServiceUtil.updateSubProject(this);
        }
    }

    @Override
    public SubProject toEscapedModel() {
        return (SubProject) ProxyUtil.newProxyInstance(SubProject.class.getClassLoader(),
            new Class[] { SubProject.class }, new AutoEscapeBeanHandler(this));
    }

    public SubProject toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        SubProjectClp clone = new SubProjectClp();

        clone.setSubProjectId(getSubProjectId());
        clone.setSubProjectName(getSubProjectName());
        clone.setDescription(getDescription());
        clone.setDisplay(getDisplay());

        return clone;
    }

    public int compareTo(SubProject subProject) {
        long primaryKey = subProject.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof SubProjectClp)) {
            return false;
        }

        SubProjectClp subProject = (SubProjectClp) obj;

        long primaryKey = subProject.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(9);

        sb.append("{subProjectId=");
        sb.append(getSubProjectId());
        sb.append(", subProjectName=");
        sb.append(getSubProjectName());
        sb.append(", description=");
        sb.append(getDescription());
        sb.append(", display=");
        sb.append(getDisplay());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(16);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.SubProject");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>subProjectId</column-name><column-value><![CDATA[");
        sb.append(getSubProjectId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>subProjectName</column-name><column-value><![CDATA[");
        sb.append(getSubProjectName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>description</column-name><column-value><![CDATA[");
        sb.append(getDescription());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>display</column-name><column-value><![CDATA[");
        sb.append(getDisplay());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
