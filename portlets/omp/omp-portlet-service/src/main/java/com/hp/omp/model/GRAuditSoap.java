package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class GRAuditSoap implements Serializable {
    private long _auditId;
    private Long _goodReceiptId;
    private int _status;
    private Long _userId;
    private Date _changeDate;

    public GRAuditSoap() {
    }

    public static GRAuditSoap toSoapModel(GRAudit model) {
        GRAuditSoap soapModel = new GRAuditSoap();

        soapModel.setAuditId(model.getAuditId());
        soapModel.setGoodReceiptId(model.getGoodReceiptId());
        soapModel.setStatus(model.getStatus());
        soapModel.setUserId(model.getUserId());
        soapModel.setChangeDate(model.getChangeDate());

        return soapModel;
    }

    public static GRAuditSoap[] toSoapModels(GRAudit[] models) {
        GRAuditSoap[] soapModels = new GRAuditSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static GRAuditSoap[][] toSoapModels(GRAudit[][] models) {
        GRAuditSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new GRAuditSoap[models.length][models[0].length];
        } else {
            soapModels = new GRAuditSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static GRAuditSoap[] toSoapModels(List<GRAudit> models) {
        List<GRAuditSoap> soapModels = new ArrayList<GRAuditSoap>(models.size());

        for (GRAudit model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new GRAuditSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _auditId;
    }

    public void setPrimaryKey(long pk) {
        setAuditId(pk);
    }

    public long getAuditId() {
        return _auditId;
    }

    public void setAuditId(long auditId) {
        _auditId = auditId;
    }

    public Long getGoodReceiptId() {
        return _goodReceiptId;
    }

    public void setGoodReceiptId(Long goodReceiptId) {
        _goodReceiptId = goodReceiptId;
    }

    public int getStatus() {
        return _status;
    }

    public void setStatus(int status) {
        _status = status;
    }

    public Long getUserId() {
        return _userId;
    }

    public void setUserId(Long userId) {
        _userId = userId;
    }

    public Date getChangeDate() {
        return _changeDate;
    }

    public void setChangeDate(Date changeDate) {
        _changeDate = changeDate;
    }
}
