package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the WbsCode service. Represents a row in the &quot;WBS_CODE&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see WbsCodeModel
 * @see com.hp.omp.model.impl.WbsCodeImpl
 * @see com.hp.omp.model.impl.WbsCodeModelImpl
 * @generated
 */
public interface WbsCode extends WbsCodeModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.WbsCodeImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
