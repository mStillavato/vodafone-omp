package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the PurchaseOrder service. Represents a row in the &quot;PURCHASE_ORDER&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see PurchaseOrderModel
 * @see com.hp.omp.model.impl.PurchaseOrderImpl
 * @see com.hp.omp.model.impl.PurchaseOrderModelImpl
 * @generated
 */
public interface PurchaseOrder extends PurchaseOrderModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.PurchaseOrderImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public com.hp.omp.model.custom.PurchaseOrderStatus getPurchaseOrderStatus();

    public java.lang.String getTxtBoxStatus()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public java.lang.String getFieldStatus()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setFieldStatus(java.lang.String fieldStatus)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public boolean getAllowUpdate()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public boolean getAllowDelete()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public boolean getAllowAdd()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public java.lang.String getTrCodeFieldsStatus()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setTrCodeFieldsStatus(java.lang.String trCodeFieldsStatus);
    
    public boolean isHasFileGR();
}
