package com.hp.omp.model.custom;

import java.util.HashMap;
import java.util.Map;

public enum GruppoUsers {
	GNED(0, "GNED"),
	VBS(1, "VBS");
	
	private int code;
	private String label;
	
	private GruppoUsers(int code, String label){
		this.code = code;
		this.label = label;
	}
	
	
	  private static Map<Integer, GruppoUsers> codeToStatusMapping;
		 
	    
	 
	    public static GruppoUsers getStatus(int i) {
	        if (codeToStatusMapping == null) {
	            initMapping();
	        }
	        return codeToStatusMapping.get(i);
	    }
	 
	    private static void initMapping() {
	        codeToStatusMapping = new HashMap<Integer, GruppoUsers>();
	        for (GruppoUsers s : values()) {
	            codeToStatusMapping.put(s.code, s);
	        }
	    }
	public int getCode(){
		return code;
	}
	
	public String getLabel(){
		return label;
	}
}
