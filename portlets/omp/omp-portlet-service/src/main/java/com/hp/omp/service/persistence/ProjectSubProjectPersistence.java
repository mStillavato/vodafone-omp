package com.hp.omp.service.persistence;

import com.hp.omp.model.ProjectSubProject;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the project sub project service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see ProjectSubProjectPersistenceImpl
 * @see ProjectSubProjectUtil
 * @generated
 */
public interface ProjectSubProjectPersistence extends BasePersistence<ProjectSubProject> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link ProjectSubProjectUtil} to access the project sub project persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the project sub project in the entity cache if it is enabled.
    *
    * @param projectSubProject the project sub project
    */
    public void cacheResult(
        com.hp.omp.model.ProjectSubProject projectSubProject);

    /**
    * Caches the project sub projects in the entity cache if it is enabled.
    *
    * @param projectSubProjects the project sub projects
    */
    public void cacheResult(
        java.util.List<com.hp.omp.model.ProjectSubProject> projectSubProjects);

    /**
    * Creates a new project sub project with the primary key. Does not add the project sub project to the database.
    *
    * @param projectSubProjectId the primary key for the new project sub project
    * @return the new project sub project
    */
    public com.hp.omp.model.ProjectSubProject create(long projectSubProjectId);

    /**
    * Removes the project sub project with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param projectSubProjectId the primary key of the project sub project
    * @return the project sub project that was removed
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a project sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ProjectSubProject remove(long projectSubProjectId)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.ProjectSubProject updateImpl(
        com.hp.omp.model.ProjectSubProject projectSubProject, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the project sub project with the primary key or throws a {@link com.hp.omp.NoSuchProjectSubProjectException} if it could not be found.
    *
    * @param projectSubProjectId the primary key of the project sub project
    * @return the project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a project sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ProjectSubProject findByPrimaryKey(
        long projectSubProjectId)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the project sub project with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param projectSubProjectId the primary key of the project sub project
    * @return the project sub project, or <code>null</code> if a project sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ProjectSubProject fetchByPrimaryKey(
        long projectSubProjectId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the project sub projects where projectId = &#63;.
    *
    * @param projectId the project ID
    * @return the matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.ProjectSubProject> findByProjectId(
        long projectId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the project sub projects where projectId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param projectId the project ID
    * @param start the lower bound of the range of project sub projects
    * @param end the upper bound of the range of project sub projects (not inclusive)
    * @return the range of matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.ProjectSubProject> findByProjectId(
        long projectId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the project sub projects where projectId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param projectId the project ID
    * @param start the lower bound of the range of project sub projects
    * @param end the upper bound of the range of project sub projects (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.ProjectSubProject> findByProjectId(
        long projectId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first project sub project in the ordered set where projectId = &#63;.
    *
    * @param projectId the project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ProjectSubProject findByProjectId_First(
        long projectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first project sub project in the ordered set where projectId = &#63;.
    *
    * @param projectId the project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching project sub project, or <code>null</code> if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ProjectSubProject fetchByProjectId_First(
        long projectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last project sub project in the ordered set where projectId = &#63;.
    *
    * @param projectId the project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ProjectSubProject findByProjectId_Last(
        long projectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last project sub project in the ordered set where projectId = &#63;.
    *
    * @param projectId the project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching project sub project, or <code>null</code> if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ProjectSubProject fetchByProjectId_Last(
        long projectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the project sub projects before and after the current project sub project in the ordered set where projectId = &#63;.
    *
    * @param projectSubProjectId the primary key of the current project sub project
    * @param projectId the project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a project sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ProjectSubProject[] findByProjectId_PrevAndNext(
        long projectSubProjectId, long projectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the project sub projects where subProjectId = &#63;.
    *
    * @param subProjectId the sub project ID
    * @return the matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.ProjectSubProject> findBySubProjectId(
        long subProjectId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the project sub projects where subProjectId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param subProjectId the sub project ID
    * @param start the lower bound of the range of project sub projects
    * @param end the upper bound of the range of project sub projects (not inclusive)
    * @return the range of matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.ProjectSubProject> findBySubProjectId(
        long subProjectId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the project sub projects where subProjectId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param subProjectId the sub project ID
    * @param start the lower bound of the range of project sub projects
    * @param end the upper bound of the range of project sub projects (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.ProjectSubProject> findBySubProjectId(
        long subProjectId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first project sub project in the ordered set where subProjectId = &#63;.
    *
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ProjectSubProject findBySubProjectId_First(
        long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first project sub project in the ordered set where subProjectId = &#63;.
    *
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching project sub project, or <code>null</code> if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ProjectSubProject fetchBySubProjectId_First(
        long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last project sub project in the ordered set where subProjectId = &#63;.
    *
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ProjectSubProject findBySubProjectId_Last(
        long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last project sub project in the ordered set where subProjectId = &#63;.
    *
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching project sub project, or <code>null</code> if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ProjectSubProject fetchBySubProjectId_Last(
        long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the project sub projects before and after the current project sub project in the ordered set where subProjectId = &#63;.
    *
    * @param projectSubProjectId the primary key of the current project sub project
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a project sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ProjectSubProject[] findBySubProjectId_PrevAndNext(
        long projectSubProjectId, long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the project sub projects where projectId = &#63; and subProjectId = &#63;.
    *
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @return the matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.ProjectSubProject> findByProjectIdAndSubProjectId(
        long projectId, long subProjectId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the project sub projects where projectId = &#63; and subProjectId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @param start the lower bound of the range of project sub projects
    * @param end the upper bound of the range of project sub projects (not inclusive)
    * @return the range of matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.ProjectSubProject> findByProjectIdAndSubProjectId(
        long projectId, long subProjectId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the project sub projects where projectId = &#63; and subProjectId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @param start the lower bound of the range of project sub projects
    * @param end the upper bound of the range of project sub projects (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.ProjectSubProject> findByProjectIdAndSubProjectId(
        long projectId, long subProjectId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first project sub project in the ordered set where projectId = &#63; and subProjectId = &#63;.
    *
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ProjectSubProject findByProjectIdAndSubProjectId_First(
        long projectId, long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first project sub project in the ordered set where projectId = &#63; and subProjectId = &#63;.
    *
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching project sub project, or <code>null</code> if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ProjectSubProject fetchByProjectIdAndSubProjectId_First(
        long projectId, long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last project sub project in the ordered set where projectId = &#63; and subProjectId = &#63;.
    *
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ProjectSubProject findByProjectIdAndSubProjectId_Last(
        long projectId, long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last project sub project in the ordered set where projectId = &#63; and subProjectId = &#63;.
    *
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching project sub project, or <code>null</code> if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ProjectSubProject fetchByProjectIdAndSubProjectId_Last(
        long projectId, long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the project sub projects before and after the current project sub project in the ordered set where projectId = &#63; and subProjectId = &#63;.
    *
    * @param projectSubProjectId the primary key of the current project sub project
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a project sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ProjectSubProject[] findByProjectIdAndSubProjectId_PrevAndNext(
        long projectSubProjectId, long projectId, long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the project sub projects.
    *
    * @return the project sub projects
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.ProjectSubProject> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the project sub projects.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of project sub projects
    * @param end the upper bound of the range of project sub projects (not inclusive)
    * @return the range of project sub projects
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.ProjectSubProject> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the project sub projects.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of project sub projects
    * @param end the upper bound of the range of project sub projects (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of project sub projects
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.ProjectSubProject> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the project sub projects where projectId = &#63; from the database.
    *
    * @param projectId the project ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByProjectId(long projectId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the project sub projects where subProjectId = &#63; from the database.
    *
    * @param subProjectId the sub project ID
    * @throws SystemException if a system exception occurred
    */
    public void removeBySubProjectId(long subProjectId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the project sub projects where projectId = &#63; and subProjectId = &#63; from the database.
    *
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByProjectIdAndSubProjectId(long projectId,
        long subProjectId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the project sub projects from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of project sub projects where projectId = &#63;.
    *
    * @param projectId the project ID
    * @return the number of matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public int countByProjectId(long projectId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of project sub projects where subProjectId = &#63;.
    *
    * @param subProjectId the sub project ID
    * @return the number of matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public int countBySubProjectId(long subProjectId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of project sub projects where projectId = &#63; and subProjectId = &#63;.
    *
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @return the number of matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public int countByProjectIdAndSubProjectId(long projectId, long subProjectId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of project sub projects.
    *
    * @return the number of project sub projects
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
