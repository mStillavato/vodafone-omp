/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service.persistence;

import com.hp.omp.model.Catalogo;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the catalogo service. This utility wraps {@link CatalogoPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see CatalogoPersistence
 * @see CatalogoPersistenceImpl
 * @generated
 */
public class CatalogoUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Catalogo catalogo) {
		getPersistence().clearCache(catalogo);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Catalogo> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Catalogo> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Catalogo> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Catalogo update(Catalogo catalogo, boolean merge)
		throws SystemException {
		return getPersistence().update(catalogo, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Catalogo update(Catalogo catalogo, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(catalogo, merge, serviceContext);
	}

	/**
	* Caches the catalogo in the entity cache if it is enabled.
	*
	* @param catalogo the catalogo
	*/
	public static void cacheResult(com.hp.omp.model.Catalogo catalogo) {
		getPersistence().cacheResult(catalogo);
	}

	/**
	* Caches the catalogos in the entity cache if it is enabled.
	*
	* @param catalogos the catalogos
	*/
	public static void cacheResult(
		java.util.List<com.hp.omp.model.Catalogo> catalogos) {
		getPersistence().cacheResult(catalogos);
	}

	/**
	* Creates a new catalogo with the primary key. Does not add the catalogo to the database.
	*
	* @param catalogoId the primary key for the new catalogo
	* @return the new catalogo
	*/
	public static com.hp.omp.model.Catalogo create(long catalogoId) {
		return getPersistence().create(catalogoId);
	}

	/**
	* Removes the catalogo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param catalogoId the primary key of the catalogo
	* @return the catalogo that was removed
	* @throws com.hp.omp.NoSuchCatalogoException if a catalogo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.hp.omp.model.Catalogo remove(long catalogoId)
		throws com.hp.omp.NoSuchCatalogoException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().remove(catalogoId);
	}

	public static com.hp.omp.model.Catalogo updateImpl(
		com.hp.omp.model.Catalogo catalogo, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(catalogo, merge);
	}

	/**
	* Returns the catalogo with the primary key or throws a {@link com.hp.omp.NoSuchCatalogoException} if it could not be found.
	*
	* @param catalogoId the primary key of the catalogo
	* @return the catalogo
	* @throws com.hp.omp.NoSuchCatalogoException if a catalogo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.hp.omp.model.Catalogo findByPrimaryKey(long catalogoId)
		throws com.hp.omp.NoSuchCatalogoException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByPrimaryKey(catalogoId);
	}

	/**
	* Returns the catalogo with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param catalogoId the primary key of the catalogo
	* @return the catalogo, or <code>null</code> if a catalogo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.hp.omp.model.Catalogo fetchByPrimaryKey(long catalogoId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(catalogoId);
	}

	/**
	* Returns all the catalogos where fornitoreDesc = &#63;.
	*
	* @param fornitoreDesc the fornitore desc
	* @return the matching catalogos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.hp.omp.model.Catalogo> findByCatalogoVendor(
		java.lang.String fornitoreDesc)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCatalogoVendor(fornitoreDesc);
	}

	/**
	* Returns a range of all the catalogos where fornitoreDesc = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param fornitoreDesc the fornitore desc
	* @param start the lower bound of the range of catalogos
	* @param end the upper bound of the range of catalogos (not inclusive)
	* @return the range of matching catalogos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.hp.omp.model.Catalogo> findByCatalogoVendor(
		java.lang.String fornitoreDesc, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByCatalogoVendor(fornitoreDesc, start, end);
	}

	/**
	* Returns an ordered range of all the catalogos where fornitoreDesc = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param fornitoreDesc the fornitore desc
	* @param start the lower bound of the range of catalogos
	* @param end the upper bound of the range of catalogos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching catalogos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.hp.omp.model.Catalogo> findByCatalogoVendor(
		java.lang.String fornitoreDesc, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCatalogoVendor(fornitoreDesc, start, end,
			orderByComparator);
	}

	/**
	* Returns the first catalogo in the ordered set where fornitoreDesc = &#63;.
	*
	* @param fornitoreDesc the fornitore desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching catalogo
	* @throws com.hp.omp.NoSuchCatalogoException if a matching catalogo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.hp.omp.model.Catalogo findByCatalogoVendor_First(
		java.lang.String fornitoreDesc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.hp.omp.NoSuchCatalogoException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCatalogoVendor_First(fornitoreDesc, orderByComparator);
	}

	/**
	* Returns the first catalogo in the ordered set where fornitoreDesc = &#63;.
	*
	* @param fornitoreDesc the fornitore desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching catalogo, or <code>null</code> if a matching catalogo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.hp.omp.model.Catalogo fetchByCatalogoVendor_First(
		java.lang.String fornitoreDesc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCatalogoVendor_First(fornitoreDesc, orderByComparator);
	}

	/**
	* Returns the last catalogo in the ordered set where fornitoreDesc = &#63;.
	*
	* @param fornitoreDesc the fornitore desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching catalogo
	* @throws com.hp.omp.NoSuchCatalogoException if a matching catalogo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.hp.omp.model.Catalogo findByCatalogoVendor_Last(
		java.lang.String fornitoreDesc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.hp.omp.NoSuchCatalogoException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCatalogoVendor_Last(fornitoreDesc, orderByComparator);
	}

	/**
	* Returns the last catalogo in the ordered set where fornitoreDesc = &#63;.
	*
	* @param fornitoreDesc the fornitore desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching catalogo, or <code>null</code> if a matching catalogo could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.hp.omp.model.Catalogo fetchByCatalogoVendor_Last(
		java.lang.String fornitoreDesc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .fetchByCatalogoVendor_Last(fornitoreDesc, orderByComparator);
	}

	/**
	* Returns the catalogos before and after the current catalogo in the ordered set where fornitoreDesc = &#63;.
	*
	* @param catalogoId the primary key of the current catalogo
	* @param fornitoreDesc the fornitore desc
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next catalogo
	* @throws com.hp.omp.NoSuchCatalogoException if a catalogo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.hp.omp.model.Catalogo[] findByCatalogoVendor_PrevAndNext(
		long catalogoId, java.lang.String fornitoreDesc,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.hp.omp.NoSuchCatalogoException,
			com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByCatalogoVendor_PrevAndNext(catalogoId, fornitoreDesc,
			orderByComparator);
	}

	/**
	* Returns all the catalogos.
	*
	* @return the catalogos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.hp.omp.model.Catalogo> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the catalogos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of catalogos
	* @param end the upper bound of the range of catalogos (not inclusive)
	* @return the range of catalogos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.hp.omp.model.Catalogo> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the catalogos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of catalogos
	* @param end the upper bound of the range of catalogos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of catalogos
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.hp.omp.model.Catalogo> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the catalogos where fornitoreDesc = &#63; from the database.
	*
	* @param fornitoreDesc the fornitore desc
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByCatalogoVendor(java.lang.String fornitoreDesc)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByCatalogoVendor(fornitoreDesc);
	}

	/**
	* Removes all the catalogos from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of catalogos where fornitoreDesc = &#63;.
	*
	* @param fornitoreDesc the fornitore desc
	* @return the number of matching catalogos
	* @throws SystemException if a system exception occurred
	*/
	public static int countByCatalogoVendor(java.lang.String fornitoreDesc)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByCatalogoVendor(fornitoreDesc);
	}

	/**
	* Returns the number of catalogos.
	*
	* @return the number of catalogos
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static CatalogoPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (CatalogoPersistence)PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
					CatalogoPersistence.class.getName());

			ReferenceRegistry.registerReference(CatalogoUtil.class,
				"_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(CatalogoPersistence persistence) {
	}

	private static CatalogoPersistence _persistence;
}