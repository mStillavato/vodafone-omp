package com.hp.omp.service.persistence;

import com.hp.omp.model.Position;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the position service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see PositionPersistenceImpl
 * @see PositionUtil
 * @generated
 */
public interface PositionPersistence extends BasePersistence<Position> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link PositionUtil} to access the position persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the position in the entity cache if it is enabled.
    *
    * @param position the position
    */
    public void cacheResult(com.hp.omp.model.Position position);

    /**
    * Caches the positions in the entity cache if it is enabled.
    *
    * @param positions the positions
    */
    public void cacheResult(java.util.List<com.hp.omp.model.Position> positions);

    /**
    * Creates a new position with the primary key. Does not add the position to the database.
    *
    * @param positionId the primary key for the new position
    * @return the new position
    */
    public com.hp.omp.model.Position create(long positionId);

    /**
    * Removes the position with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param positionId the primary key of the position
    * @return the position that was removed
    * @throws com.hp.omp.NoSuchPositionException if a position with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Position remove(long positionId)
        throws com.hp.omp.NoSuchPositionException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.Position updateImpl(
        com.hp.omp.model.Position position, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the position with the primary key or throws a {@link com.hp.omp.NoSuchPositionException} if it could not be found.
    *
    * @param positionId the primary key of the position
    * @return the position
    * @throws com.hp.omp.NoSuchPositionException if a position with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Position findByPrimaryKey(long positionId)
        throws com.hp.omp.NoSuchPositionException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the position with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param positionId the primary key of the position
    * @return the position, or <code>null</code> if a position with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Position fetchByPrimaryKey(long positionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the positions where purchaseOrderId = &#63;.
    *
    * @param purchaseOrderId the purchase order ID
    * @return the matching positions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.Position> findByPurchaseOrderPositions(
        java.lang.String purchaseOrderId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the positions where purchaseOrderId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param purchaseOrderId the purchase order ID
    * @param start the lower bound of the range of positions
    * @param end the upper bound of the range of positions (not inclusive)
    * @return the range of matching positions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.Position> findByPurchaseOrderPositions(
        java.lang.String purchaseOrderId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the positions where purchaseOrderId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param purchaseOrderId the purchase order ID
    * @param start the lower bound of the range of positions
    * @param end the upper bound of the range of positions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching positions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.Position> findByPurchaseOrderPositions(
        java.lang.String purchaseOrderId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first position in the ordered set where purchaseOrderId = &#63;.
    *
    * @param purchaseOrderId the purchase order ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching position
    * @throws com.hp.omp.NoSuchPositionException if a matching position could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Position findByPurchaseOrderPositions_First(
        java.lang.String purchaseOrderId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchPositionException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first position in the ordered set where purchaseOrderId = &#63;.
    *
    * @param purchaseOrderId the purchase order ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching position, or <code>null</code> if a matching position could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Position fetchByPurchaseOrderPositions_First(
        java.lang.String purchaseOrderId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last position in the ordered set where purchaseOrderId = &#63;.
    *
    * @param purchaseOrderId the purchase order ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching position
    * @throws com.hp.omp.NoSuchPositionException if a matching position could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Position findByPurchaseOrderPositions_Last(
        java.lang.String purchaseOrderId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchPositionException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last position in the ordered set where purchaseOrderId = &#63;.
    *
    * @param purchaseOrderId the purchase order ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching position, or <code>null</code> if a matching position could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Position fetchByPurchaseOrderPositions_Last(
        java.lang.String purchaseOrderId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the positions before and after the current position in the ordered set where purchaseOrderId = &#63;.
    *
    * @param positionId the primary key of the current position
    * @param purchaseOrderId the purchase order ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next position
    * @throws com.hp.omp.NoSuchPositionException if a position with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Position[] findByPurchaseOrderPositions_PrevAndNext(
        long positionId, java.lang.String purchaseOrderId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchPositionException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the positions where purchaseRequestId = &#63;.
    *
    * @param purchaseRequestId the purchase request ID
    * @return the matching positions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.Position> findByPurchaseRequestPositions(
        java.lang.String purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the positions where purchaseRequestId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param purchaseRequestId the purchase request ID
    * @param start the lower bound of the range of positions
    * @param end the upper bound of the range of positions (not inclusive)
    * @return the range of matching positions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.Position> findByPurchaseRequestPositions(
        java.lang.String purchaseRequestId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the positions where purchaseRequestId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param purchaseRequestId the purchase request ID
    * @param start the lower bound of the range of positions
    * @param end the upper bound of the range of positions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching positions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.Position> findByPurchaseRequestPositions(
        java.lang.String purchaseRequestId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first position in the ordered set where purchaseRequestId = &#63;.
    *
    * @param purchaseRequestId the purchase request ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching position
    * @throws com.hp.omp.NoSuchPositionException if a matching position could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Position findByPurchaseRequestPositions_First(
        java.lang.String purchaseRequestId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchPositionException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first position in the ordered set where purchaseRequestId = &#63;.
    *
    * @param purchaseRequestId the purchase request ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching position, or <code>null</code> if a matching position could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Position fetchByPurchaseRequestPositions_First(
        java.lang.String purchaseRequestId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last position in the ordered set where purchaseRequestId = &#63;.
    *
    * @param purchaseRequestId the purchase request ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching position
    * @throws com.hp.omp.NoSuchPositionException if a matching position could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Position findByPurchaseRequestPositions_Last(
        java.lang.String purchaseRequestId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchPositionException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last position in the ordered set where purchaseRequestId = &#63;.
    *
    * @param purchaseRequestId the purchase request ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching position, or <code>null</code> if a matching position could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Position fetchByPurchaseRequestPositions_Last(
        java.lang.String purchaseRequestId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the positions before and after the current position in the ordered set where purchaseRequestId = &#63;.
    *
    * @param positionId the primary key of the current position
    * @param purchaseRequestId the purchase request ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next position
    * @throws com.hp.omp.NoSuchPositionException if a position with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Position[] findByPurchaseRequestPositions_PrevAndNext(
        long positionId, java.lang.String purchaseRequestId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchPositionException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the positions.
    *
    * @return the positions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.Position> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the positions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of positions
    * @param end the upper bound of the range of positions (not inclusive)
    * @return the range of positions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.Position> findAll(int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the positions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of positions
    * @param end the upper bound of the range of positions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of positions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.Position> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the positions where purchaseOrderId = &#63; from the database.
    *
    * @param purchaseOrderId the purchase order ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByPurchaseOrderPositions(java.lang.String purchaseOrderId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the positions where purchaseRequestId = &#63; from the database.
    *
    * @param purchaseRequestId the purchase request ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByPurchaseRequestPositions(
        java.lang.String purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the positions from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of positions where purchaseOrderId = &#63;.
    *
    * @param purchaseOrderId the purchase order ID
    * @return the number of matching positions
    * @throws SystemException if a system exception occurred
    */
    public int countByPurchaseOrderPositions(java.lang.String purchaseOrderId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of positions where purchaseRequestId = &#63;.
    *
    * @param purchaseRequestId the purchase request ID
    * @return the number of matching positions
    * @throws SystemException if a system exception occurred
    */
    public int countByPurchaseRequestPositions(
        java.lang.String purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of positions.
    *
    * @return the number of positions
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
