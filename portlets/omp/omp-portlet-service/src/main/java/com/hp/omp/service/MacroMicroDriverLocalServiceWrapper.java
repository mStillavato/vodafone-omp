package com.hp.omp.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link MacroMicroDriverLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       MacroMicroDriverLocalService
 * @generated
 */
public class MacroMicroDriverLocalServiceWrapper
    implements MacroMicroDriverLocalService,
        ServiceWrapper<MacroMicroDriverLocalService> {
    private MacroMicroDriverLocalService _macroMicroDriverLocalService;

    public MacroMicroDriverLocalServiceWrapper(
        MacroMicroDriverLocalService macroMicroDriverLocalService) {
        _macroMicroDriverLocalService = macroMicroDriverLocalService;
    }

    /**
    * Adds the macro micro driver to the database. Also notifies the appropriate model listeners.
    *
    * @param macroMicroDriver the macro micro driver
    * @return the macro micro driver that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver addMacroMicroDriver(
        com.hp.omp.model.MacroMicroDriver macroMicroDriver)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroMicroDriverLocalService.addMacroMicroDriver(macroMicroDriver);
    }

    /**
    * Creates a new macro micro driver with the primary key. Does not add the macro micro driver to the database.
    *
    * @param macroMicroDriverId the primary key for the new macro micro driver
    * @return the new macro micro driver
    */
    public com.hp.omp.model.MacroMicroDriver createMacroMicroDriver(
        long macroMicroDriverId) {
        return _macroMicroDriverLocalService.createMacroMicroDriver(macroMicroDriverId);
    }

    /**
    * Deletes the macro micro driver with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param macroMicroDriverId the primary key of the macro micro driver
    * @return the macro micro driver that was removed
    * @throws PortalException if a macro micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver deleteMacroMicroDriver(
        long macroMicroDriverId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _macroMicroDriverLocalService.deleteMacroMicroDriver(macroMicroDriverId);
    }

    /**
    * Deletes the macro micro driver from the database. Also notifies the appropriate model listeners.
    *
    * @param macroMicroDriver the macro micro driver
    * @return the macro micro driver that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver deleteMacroMicroDriver(
        com.hp.omp.model.MacroMicroDriver macroMicroDriver)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroMicroDriverLocalService.deleteMacroMicroDriver(macroMicroDriver);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _macroMicroDriverLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroMicroDriverLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _macroMicroDriverLocalService.dynamicQuery(dynamicQuery, start,
            end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroMicroDriverLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroMicroDriverLocalService.dynamicQueryCount(dynamicQuery);
    }

    public com.hp.omp.model.MacroMicroDriver fetchMacroMicroDriver(
        long macroMicroDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroMicroDriverLocalService.fetchMacroMicroDriver(macroMicroDriverId);
    }

    /**
    * Returns the macro micro driver with the primary key.
    *
    * @param macroMicroDriverId the primary key of the macro micro driver
    * @return the macro micro driver
    * @throws PortalException if a macro micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver getMacroMicroDriver(
        long macroMicroDriverId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _macroMicroDriverLocalService.getMacroMicroDriver(macroMicroDriverId);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _macroMicroDriverLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the macro micro drivers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of macro micro drivers
    * @param end the upper bound of the range of macro micro drivers (not inclusive)
    * @return the range of macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MacroMicroDriver> getMacroMicroDrivers(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroMicroDriverLocalService.getMacroMicroDrivers(start, end);
    }

    /**
    * Returns the number of macro micro drivers.
    *
    * @return the number of macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public int getMacroMicroDriversCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroMicroDriverLocalService.getMacroMicroDriversCount();
    }

    /**
    * Updates the macro micro driver in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param macroMicroDriver the macro micro driver
    * @return the macro micro driver that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver updateMacroMicroDriver(
        com.hp.omp.model.MacroMicroDriver macroMicroDriver)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroMicroDriverLocalService.updateMacroMicroDriver(macroMicroDriver);
    }

    /**
    * Updates the macro micro driver in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param macroMicroDriver the macro micro driver
    * @param merge whether to merge the macro micro driver with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the macro micro driver that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroMicroDriver updateMacroMicroDriver(
        com.hp.omp.model.MacroMicroDriver macroMicroDriver, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroMicroDriverLocalService.updateMacroMicroDriver(macroMicroDriver,
            merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _macroMicroDriverLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _macroMicroDriverLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _macroMicroDriverLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    public java.util.List<com.hp.omp.model.MacroMicroDriver> getMacroMicroDriverByMacroDriverId(
        java.lang.Long macroDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroMicroDriverLocalService.getMacroMicroDriverByMacroDriverId(macroDriverId);
    }

    public java.util.List<com.hp.omp.model.MacroMicroDriver> getMacroMicroDriverByMicroDriverId(
        java.lang.Long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroMicroDriverLocalService.getMacroMicroDriverByMicroDriverId(microDriverId);
    }

    public java.util.List<com.hp.omp.model.MacroMicroDriver> getMacroMicroDriverByMacroDriverIdAndMicroDriverId(
        java.lang.Long macroDriverId, java.lang.Long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroMicroDriverLocalService.getMacroMicroDriverByMacroDriverIdAndMicroDriverId(macroDriverId,
            microDriverId);
    }

    public java.util.List<com.hp.omp.model.MacroDriver> getMacroDriversByMicroDriverId(
        java.lang.Long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroMicroDriverLocalService.getMacroDriversByMicroDriverId(microDriverId);
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public MacroMicroDriverLocalService getWrappedMacroMicroDriverLocalService() {
        return _macroMicroDriverLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedMacroMicroDriverLocalService(
        MacroMicroDriverLocalService macroMicroDriverLocalService) {
        _macroMicroDriverLocalService = macroMicroDriverLocalService;
    }

    public MacroMicroDriverLocalService getWrappedService() {
        return _macroMicroDriverLocalService;
    }

    public void setWrappedService(
        MacroMicroDriverLocalService macroMicroDriverLocalService) {
        _macroMicroDriverLocalService = macroMicroDriverLocalService;
    }
}
