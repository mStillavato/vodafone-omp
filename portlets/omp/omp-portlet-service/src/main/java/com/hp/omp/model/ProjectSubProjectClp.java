package com.hp.omp.model;

import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.ProjectSubProjectLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class ProjectSubProjectClp extends BaseModelImpl<ProjectSubProject>
    implements ProjectSubProject {
    private long _projectSubProjectId;
    private long _projectId;
    private long _subProjectId;
    private BaseModel<?> _projectSubProjectRemoteModel;

    public ProjectSubProjectClp() {
    }

    public Class<?> getModelClass() {
        return ProjectSubProject.class;
    }

    public String getModelClassName() {
        return ProjectSubProject.class.getName();
    }

    public long getPrimaryKey() {
        return _projectSubProjectId;
    }

    public void setPrimaryKey(long primaryKey) {
        setProjectSubProjectId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_projectSubProjectId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("projectSubProjectId", getProjectSubProjectId());
        attributes.put("projectId", getProjectId());
        attributes.put("subProjectId", getSubProjectId());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long projectSubProjectId = (Long) attributes.get("projectSubProjectId");

        if (projectSubProjectId != null) {
            setProjectSubProjectId(projectSubProjectId);
        }

        Long projectId = (Long) attributes.get("projectId");

        if (projectId != null) {
            setProjectId(projectId);
        }

        Long subProjectId = (Long) attributes.get("subProjectId");

        if (subProjectId != null) {
            setSubProjectId(subProjectId);
        }
    }

    public long getProjectSubProjectId() {
        return _projectSubProjectId;
    }

    public void setProjectSubProjectId(long projectSubProjectId) {
        _projectSubProjectId = projectSubProjectId;

        if (_projectSubProjectRemoteModel != null) {
            try {
                Class<?> clazz = _projectSubProjectRemoteModel.getClass();

                Method method = clazz.getMethod("setProjectSubProjectId",
                        long.class);

                method.invoke(_projectSubProjectRemoteModel, projectSubProjectId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public long getProjectId() {
        return _projectId;
    }

    public void setProjectId(long projectId) {
        _projectId = projectId;

        if (_projectSubProjectRemoteModel != null) {
            try {
                Class<?> clazz = _projectSubProjectRemoteModel.getClass();

                Method method = clazz.getMethod("setProjectId", long.class);

                method.invoke(_projectSubProjectRemoteModel, projectId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public long getSubProjectId() {
        return _subProjectId;
    }

    public void setSubProjectId(long subProjectId) {
        _subProjectId = subProjectId;

        if (_projectSubProjectRemoteModel != null) {
            try {
                Class<?> clazz = _projectSubProjectRemoteModel.getClass();

                Method method = clazz.getMethod("setSubProjectId", long.class);

                method.invoke(_projectSubProjectRemoteModel, subProjectId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getProjectSubProjectRemoteModel() {
        return _projectSubProjectRemoteModel;
    }

    public void setProjectSubProjectRemoteModel(
        BaseModel<?> projectSubProjectRemoteModel) {
        _projectSubProjectRemoteModel = projectSubProjectRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _projectSubProjectRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_projectSubProjectRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            ProjectSubProjectLocalServiceUtil.addProjectSubProject(this);
        } else {
            ProjectSubProjectLocalServiceUtil.updateProjectSubProject(this);
        }
    }

    @Override
    public ProjectSubProject toEscapedModel() {
        return (ProjectSubProject) ProxyUtil.newProxyInstance(ProjectSubProject.class.getClassLoader(),
            new Class[] { ProjectSubProject.class },
            new AutoEscapeBeanHandler(this));
    }

    public ProjectSubProject toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        ProjectSubProjectClp clone = new ProjectSubProjectClp();

        clone.setProjectSubProjectId(getProjectSubProjectId());
        clone.setProjectId(getProjectId());
        clone.setSubProjectId(getSubProjectId());

        return clone;
    }

    public int compareTo(ProjectSubProject projectSubProject) {
        long primaryKey = projectSubProject.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ProjectSubProjectClp)) {
            return false;
        }

        ProjectSubProjectClp projectSubProject = (ProjectSubProjectClp) obj;

        long primaryKey = projectSubProject.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{projectSubProjectId=");
        sb.append(getProjectSubProjectId());
        sb.append(", projectId=");
        sb.append(getProjectId());
        sb.append(", subProjectId=");
        sb.append(getSubProjectId());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(13);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.ProjectSubProject");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>projectSubProjectId</column-name><column-value><![CDATA[");
        sb.append(getProjectSubProjectId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>projectId</column-name><column-value><![CDATA[");
        sb.append(getProjectId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>subProjectId</column-name><column-value><![CDATA[");
        sb.append(getSubProjectId());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
