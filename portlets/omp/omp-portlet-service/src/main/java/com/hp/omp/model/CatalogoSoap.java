/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class CatalogoSoap implements Serializable {
	public static CatalogoSoap toSoapModel(Catalogo model) {
		CatalogoSoap soapModel = new CatalogoSoap();

		soapModel.setCatalogoId(model.getCatalogoId());
		soapModel.setChiaveInforecord(model.getChiaveInforecord());
		soapModel.setDataCreazione(model.getDataCreazione());
		soapModel.setDataFineValidita(model.getDataFineValidita());
		soapModel.setDivisione(model.getDivisione());
		soapModel.setFlgCanc(model.getFlgCanc());
		soapModel.setFornFinCode(model.getFornFinCode());
		soapModel.setFornitoreCode(model.getFornitoreCode());
		soapModel.setFornitoreDesc(model.getFornitoreDesc());
		soapModel.setMatCatLvl2(model.getMatCatLvl2());
		soapModel.setMatCatLvl2Desc(model.getMatCatLvl2Desc());
		soapModel.setMatCatLvl4(model.getMatCatLvl4());
		soapModel.setMatCatLvl4Desc(model.getMatCatLvl4Desc());
		soapModel.setMatCod(model.getMatCod());
		soapModel.setMatCodFornFinale(model.getMatCodFornFinale());
		soapModel.setMatDesc(model.getMatDesc());
		soapModel.setPrezzo(model.getPrezzo());
		soapModel.setPurchOrganiz(model.getPurchOrganiz());
		soapModel.setUm(model.getUm());
		soapModel.setUmPrezzo(model.getUmPrezzo());
		soapModel.setUmPrezzoPo(model.getUmPrezzoPo());
		soapModel.setValuta(model.getValuta());
		soapModel.setUpperMatDesc(model.getUpperMatDesc());

		return soapModel;
	}

	public static CatalogoSoap[] toSoapModels(Catalogo[] models) {
		CatalogoSoap[] soapModels = new CatalogoSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static CatalogoSoap[][] toSoapModels(Catalogo[][] models) {
		CatalogoSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new CatalogoSoap[models.length][models[0].length];
		}
		else {
			soapModels = new CatalogoSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static CatalogoSoap[] toSoapModels(List<Catalogo> models) {
		List<CatalogoSoap> soapModels = new ArrayList<CatalogoSoap>(models.size());

		for (Catalogo model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new CatalogoSoap[soapModels.size()]);
	}

	public CatalogoSoap() {
	}

	public long getPrimaryKey() {
		return _catalogoId;
	}

	public void setPrimaryKey(long pk) {
		setCatalogoId(pk);
	}

	public long getCatalogoId() {
		return _catalogoId;
	}

	public void setCatalogoId(long catalogoId) {
		_catalogoId = catalogoId;
	}

	public String getChiaveInforecord() {
		return _chiaveInforecord;
	}

	public void setChiaveInforecord(String chiaveInforecord) {
		_chiaveInforecord = chiaveInforecord;
	}

	public Date getDataCreazione() {
		return _dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		_dataCreazione = dataCreazione;
	}

	public Date getDataFineValidita() {
		return _dataFineValidita;
	}

	public void setDataFineValidita(Date dataFineValidita) {
		_dataFineValidita = dataFineValidita;
	}

	public String getDivisione() {
		return _divisione;
	}

	public void setDivisione(String divisione) {
		_divisione = divisione;
	}

	public boolean getFlgCanc() {
		return _flgCanc;
	}

	public boolean isFlgCanc() {
		return _flgCanc;
	}

	public void setFlgCanc(boolean flgCanc) {
		_flgCanc = flgCanc;
	}

	public String getFornFinCode() {
		return _fornFinCode;
	}

	public void setFornFinCode(String fornFinCode) {
		_fornFinCode = fornFinCode;
	}

	public String getFornitoreCode() {
		return _fornitoreCode;
	}

	public void setFornitoreCode(String fornitoreCode) {
		_fornitoreCode = fornitoreCode;
	}

	public String getFornitoreDesc() {
		return _fornitoreDesc;
	}

	public void setFornitoreDesc(String fornitoreDesc) {
		_fornitoreDesc = fornitoreDesc;
	}

	public String getMatCatLvl2() {
		return _matCatLvl2;
	}

	public void setMatCatLvl2(String matCatLvl2) {
		_matCatLvl2 = matCatLvl2;
	}

	public String getMatCatLvl2Desc() {
		return _matCatLvl2Desc;
	}

	public void setMatCatLvl2Desc(String matCatLvl2Desc) {
		_matCatLvl2Desc = matCatLvl2Desc;
	}

	public String getMatCatLvl4() {
		return _matCatLvl4;
	}

	public void setMatCatLvl4(String matCatLvl4) {
		_matCatLvl4 = matCatLvl4;
	}

	public String getMatCatLvl4Desc() {
		return _matCatLvl4Desc;
	}

	public void setMatCatLvl4Desc(String matCatLvl4Desc) {
		_matCatLvl4Desc = matCatLvl4Desc;
	}

	public String getMatCod() {
		return _matCod;
	}

	public void setMatCod(String matCod) {
		_matCod = matCod;
	}

	public String getMatCodFornFinale() {
		return _matCodFornFinale;
	}

	public void setMatCodFornFinale(String matCodFornFinale) {
		_matCodFornFinale = matCodFornFinale;
	}

	public String getMatDesc() {
		return _matDesc;
	}

	public void setMatDesc(String matDesc) {
		_matDesc = matDesc;
	}

	public String getPrezzo() {
		return _prezzo;
	}

	public void setPrezzo(String prezzo) {
		_prezzo = prezzo;
	}

	public String getPurchOrganiz() {
		return _purchOrganiz;
	}

	public void setPurchOrganiz(String purchOrganiz) {
		_purchOrganiz = purchOrganiz;
	}

	public String getUm() {
		return _um;
	}

	public void setUm(String um) {
		_um = um;
	}

	public String getUmPrezzo() {
		return _umPrezzo;
	}

	public void setUmPrezzo(String umPrezzo) {
		_umPrezzo = umPrezzo;
	}

	public String getUmPrezzoPo() {
		return _umPrezzoPo;
	}

	public void setUmPrezzoPo(String umPrezzoPo) {
		_umPrezzoPo = umPrezzoPo;
	}

	public String getValuta() {
		return _valuta;
	}

	public void setValuta(String valuta) {
		_valuta = valuta;
	}

	public String getUpperMatDesc() {
		return _upperMatDesc;
	}

	public void setUpperMatDesc(String upperMatDesc) {
		_upperMatDesc = upperMatDesc;
	}

	private long _catalogoId;
	private String _chiaveInforecord;
	private Date _dataCreazione;
	private Date _dataFineValidita;
	private String _divisione;
	private boolean _flgCanc;
	private String _fornFinCode;
	private String _fornitoreCode;
	private String _fornitoreDesc;
	private String _matCatLvl2;
	private String _matCatLvl2Desc;
	private String _matCatLvl4;
	private String _matCatLvl4Desc;
	private String _matCod;
	private String _matCodFornFinale;
	private String _matDesc;
	private String _prezzo;
	private String _purchOrganiz;
	private String _um;
	private String _umPrezzo;
	private String _umPrezzoPo;
	private String _valuta;
	private String _upperMatDesc;
}