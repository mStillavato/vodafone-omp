package com.hp.omp.service.persistence;

public interface WbsCodeFinder {
    
    public java.util.List<com.hp.omp.model.WbsCode> getWbsCodesList(
    		int start, int end, String searchFilter)
            throws com.liferay.portal.kernel.exception.SystemException;
 
    public int getWbsCodesCountList(String searchFilter)
            throws com.liferay.portal.kernel.exception.SystemException;
    
}
