package com.hp.omp.service.persistence;

public interface VendorFinder {
    
    public java.util.List<com.hp.omp.model.Vendor> getVendorsList(
    		int start, int end, String searchFilter, int gruppoUsers)
            throws com.liferay.portal.kernel.exception.SystemException;
    
    public java.util.List<com.hp.omp.model.Vendor> getVendorBySupplierCode(
    		String supplierCode, int gruppoUsers)
            throws com.liferay.portal.kernel.exception.SystemException;
    
    public int getVendorsCountList(String searchFilter, int gruppoUsers)
            throws com.liferay.portal.kernel.exception.SystemException;
}
