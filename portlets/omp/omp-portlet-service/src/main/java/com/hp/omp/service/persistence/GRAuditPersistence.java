package com.hp.omp.service.persistence;

import com.hp.omp.model.GRAudit;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the g r audit service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see GRAuditPersistenceImpl
 * @see GRAuditUtil
 * @generated
 */
public interface GRAuditPersistence extends BasePersistence<GRAudit> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link GRAuditUtil} to access the g r audit persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the g r audit in the entity cache if it is enabled.
    *
    * @param grAudit the g r audit
    */
    public void cacheResult(com.hp.omp.model.GRAudit grAudit);

    /**
    * Caches the g r audits in the entity cache if it is enabled.
    *
    * @param grAudits the g r audits
    */
    public void cacheResult(java.util.List<com.hp.omp.model.GRAudit> grAudits);

    /**
    * Creates a new g r audit with the primary key. Does not add the g r audit to the database.
    *
    * @param auditId the primary key for the new g r audit
    * @return the new g r audit
    */
    public com.hp.omp.model.GRAudit create(long auditId);

    /**
    * Removes the g r audit with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param auditId the primary key of the g r audit
    * @return the g r audit that was removed
    * @throws com.hp.omp.NoSuchGRAuditException if a g r audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.GRAudit remove(long auditId)
        throws com.hp.omp.NoSuchGRAuditException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.GRAudit updateImpl(
        com.hp.omp.model.GRAudit grAudit, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the g r audit with the primary key or throws a {@link com.hp.omp.NoSuchGRAuditException} if it could not be found.
    *
    * @param auditId the primary key of the g r audit
    * @return the g r audit
    * @throws com.hp.omp.NoSuchGRAuditException if a g r audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.GRAudit findByPrimaryKey(long auditId)
        throws com.hp.omp.NoSuchGRAuditException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the g r audit with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param auditId the primary key of the g r audit
    * @return the g r audit, or <code>null</code> if a g r audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.GRAudit fetchByPrimaryKey(long auditId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the g r audits.
    *
    * @return the g r audits
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.GRAudit> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the g r audits.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of g r audits
    * @param end the upper bound of the range of g r audits (not inclusive)
    * @return the range of g r audits
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.GRAudit> findAll(int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the g r audits.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of g r audits
    * @param end the upper bound of the range of g r audits (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of g r audits
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.GRAudit> findAll(int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the g r audits from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of g r audits.
    *
    * @return the number of g r audits
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
