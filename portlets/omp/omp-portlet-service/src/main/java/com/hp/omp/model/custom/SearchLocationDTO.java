/**
 * 
 */
package com.hp.omp.model.custom;

import java.io.Serializable;
import java.util.List;

import com.hp.omp.model.CostCenter;
import com.hp.omp.model.Project;
import com.hp.omp.model.SubProject;
import com.hp.omp.model.WbsCode;

/**
 * @author gewaly
 *
 */
public class SearchLocationDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7684541966359000646L;
	
	private String codiceOffice  = null;
	private String targaTecnicaSap = null;
	private String location = null;
	private String pr = null;
	private String globalSearchKeyWord = null;
	private String miniSearchValue = null;
	private boolean miniSeach;
	private Integer pageNumber = 1;
	private Integer pageSize = 20;
	
	public String getCodiceOffice() {
		return codiceOffice;
	}
	public void setCodiceOffice(String codiceOffice) {
		this.codiceOffice = codiceOffice;
	}
	public String getTargaTecnicaSap() {
		return targaTecnicaSap;
	}
	public void setTargaTecnicaSap(String targaTecnicaSap) {
		this.targaTecnicaSap = targaTecnicaSap;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getPr() {
		return pr;
	}
	public void setPr(String pr) {
		this.pr = pr;
	}
	public String getGlobalSearchKeyWord() {
		return globalSearchKeyWord;
	}
	public void setGlobalSearchKeyWord(String globalSearchKeyWord) {
		this.globalSearchKeyWord = globalSearchKeyWord;
	}
	public String getMiniSearchValue() {
		return miniSearchValue;
	}
	public void setMiniSearchValue(String miniSearchValue) {
		this.miniSearchValue = miniSearchValue;
	}
	public boolean isMiniSeach() {
		return miniSeach;
	}
	public void setMiniSeach(boolean miniSeach) {
		this.miniSeach = miniSeach;
	}
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
}