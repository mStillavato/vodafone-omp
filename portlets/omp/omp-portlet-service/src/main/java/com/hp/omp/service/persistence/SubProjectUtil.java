package com.hp.omp.service.persistence;

import com.hp.omp.model.SubProject;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the sub project service. This utility wraps {@link SubProjectPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see SubProjectPersistence
 * @see SubProjectPersistenceImpl
 * @generated
 */
public class SubProjectUtil {
    private static SubProjectPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(SubProject subProject) {
        getPersistence().clearCache(subProject);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<SubProject> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<SubProject> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<SubProject> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static SubProject update(SubProject subProject, boolean merge)
        throws SystemException {
        return getPersistence().update(subProject, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static SubProject update(SubProject subProject, boolean merge,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(subProject, merge, serviceContext);
    }

    /**
    * Caches the sub project in the entity cache if it is enabled.
    *
    * @param subProject the sub project
    */
    public static void cacheResult(com.hp.omp.model.SubProject subProject) {
        getPersistence().cacheResult(subProject);
    }

    /**
    * Caches the sub projects in the entity cache if it is enabled.
    *
    * @param subProjects the sub projects
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.SubProject> subProjects) {
        getPersistence().cacheResult(subProjects);
    }

    /**
    * Creates a new sub project with the primary key. Does not add the sub project to the database.
    *
    * @param subProjectId the primary key for the new sub project
    * @return the new sub project
    */
    public static com.hp.omp.model.SubProject create(long subProjectId) {
        return getPersistence().create(subProjectId);
    }

    /**
    * Removes the sub project with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param subProjectId the primary key of the sub project
    * @return the sub project that was removed
    * @throws com.hp.omp.NoSuchSubProjectException if a sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.SubProject remove(long subProjectId)
        throws com.hp.omp.NoSuchSubProjectException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(subProjectId);
    }

    public static com.hp.omp.model.SubProject updateImpl(
        com.hp.omp.model.SubProject subProject, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(subProject, merge);
    }

    /**
    * Returns the sub project with the primary key or throws a {@link com.hp.omp.NoSuchSubProjectException} if it could not be found.
    *
    * @param subProjectId the primary key of the sub project
    * @return the sub project
    * @throws com.hp.omp.NoSuchSubProjectException if a sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.SubProject findByPrimaryKey(
        long subProjectId)
        throws com.hp.omp.NoSuchSubProjectException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(subProjectId);
    }

    /**
    * Returns the sub project with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param subProjectId the primary key of the sub project
    * @return the sub project, or <code>null</code> if a sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.SubProject fetchByPrimaryKey(
        long subProjectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(subProjectId);
    }

    /**
    * Returns all the sub projects.
    *
    * @return the sub projects
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.SubProject> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the sub projects.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of sub projects
    * @param end the upper bound of the range of sub projects (not inclusive)
    * @return the range of sub projects
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.SubProject> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the sub projects.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of sub projects
    * @param end the upper bound of the range of sub projects (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of sub projects
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.SubProject> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the sub projects from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of sub projects.
    *
    * @return the number of sub projects
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static SubProjectPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (SubProjectPersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    SubProjectPersistence.class.getName());

            ReferenceRegistry.registerReference(SubProjectUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(SubProjectPersistence persistence) {
    }
}
