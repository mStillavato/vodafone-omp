package com.hp.omp.service.persistence;

import com.hp.omp.model.MacroDriver;
import com.hp.omp.service.MacroDriverLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class MacroDriverActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public MacroDriverActionableDynamicQuery() throws SystemException {
        setBaseLocalService(MacroDriverLocalServiceUtil.getService());
        setClass(MacroDriver.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("macroDriverId");
    }
}
