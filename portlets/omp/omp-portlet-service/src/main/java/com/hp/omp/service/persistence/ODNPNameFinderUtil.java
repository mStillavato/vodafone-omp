package com.hp.omp.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class ODNPNameFinderUtil {
    private static ODNPNameFinder _finder;

    public static java.util.List<com.hp.omp.model.ODNPName> getODNPNamesList(
    	int start, int end, String searchFilter)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getFinder().getODNPNamesList(start, end, searchFilter);
    }
    
    public static ODNPNameFinder getFinder() {
        if (_finder == null) {
            _finder = (ODNPNameFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
            		ODNPNameFinder.class.getName());

            ReferenceRegistry.registerReference(ODNPNameFinderUtil.class,
                "_finder");
        }

        return _finder;
    }

    public void setFinder(ODNPNameFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(ODNPNameFinderUtil.class,
            "_finder");
    }
}
