package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Project}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       Project
 * @generated
 */
public class ProjectWrapper implements Project, ModelWrapper<Project> {
    private Project _project;

    public ProjectWrapper(Project project) {
        _project = project;
    }

    public Class<?> getModelClass() {
        return Project.class;
    }

    public String getModelClassName() {
        return Project.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("projectId", getProjectId());
        attributes.put("projectName", getProjectName());
        attributes.put("description", getDescription());
        attributes.put("display", getDisplay());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long projectId = (Long) attributes.get("projectId");

        if (projectId != null) {
            setProjectId(projectId);
        }

        String projectName = (String) attributes.get("projectName");

        if (projectName != null) {
            setProjectName(projectName);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
    }

    /**
    * Returns the primary key of this project.
    *
    * @return the primary key of this project
    */
    public long getPrimaryKey() {
        return _project.getPrimaryKey();
    }

    /**
    * Sets the primary key of this project.
    *
    * @param primaryKey the primary key of this project
    */
    public void setPrimaryKey(long primaryKey) {
        _project.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the project ID of this project.
    *
    * @return the project ID of this project
    */
    public long getProjectId() {
        return _project.getProjectId();
    }

    /**
    * Sets the project ID of this project.
    *
    * @param projectId the project ID of this project
    */
    public void setProjectId(long projectId) {
        _project.setProjectId(projectId);
    }

    /**
    * Returns the project name of this project.
    *
    * @return the project name of this project
    */
    public java.lang.String getProjectName() {
        return _project.getProjectName();
    }

    /**
    * Sets the project name of this project.
    *
    * @param projectName the project name of this project
    */
    public void setProjectName(java.lang.String projectName) {
        _project.setProjectName(projectName);
    }

    /**
    * Returns the description of this project.
    *
    * @return the description of this project
    */
    public java.lang.String getDescription() {
        return _project.getDescription();
    }

    /**
    * Sets the description of this project.
    *
    * @param description the description of this project
    */
    public void setDescription(java.lang.String description) {
        _project.setDescription(description);
    }

    /**
    * Returns the display of this project.
    *
    * @return the display of this project
    */
    public boolean getDisplay() {
        return _project.getDisplay();
    }

    /**
    * Returns <code>true</code> if this project is display.
    *
    * @return <code>true</code> if this project is display; <code>false</code> otherwise
    */
    public boolean isDisplay() {
        return _project.isDisplay();
    }

    /**
    * Sets whether this project is display.
    *
    * @param display the display of this project
    */
    public void setDisplay(boolean display) {
        _project.setDisplay(display);
    }

    public boolean isNew() {
        return _project.isNew();
    }

    public void setNew(boolean n) {
        _project.setNew(n);
    }

    public boolean isCachedModel() {
        return _project.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _project.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _project.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _project.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _project.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _project.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _project.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new ProjectWrapper((Project) _project.clone());
    }

    public int compareTo(Project project) {
        return _project.compareTo(project);
    }

    @Override
    public int hashCode() {
        return _project.hashCode();
    }

    public com.liferay.portal.model.CacheModel<Project> toCacheModel() {
        return _project.toCacheModel();
    }

    public Project toEscapedModel() {
        return new ProjectWrapper(_project.toEscapedModel());
    }

    public Project toUnescapedModel() {
        return new ProjectWrapper(_project.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _project.toString();
    }

    public java.lang.String toXmlString() {
        return _project.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _project.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ProjectWrapper)) {
            return false;
        }

        ProjectWrapper projectWrapper = (ProjectWrapper) obj;

        if (Validator.equals(_project, projectWrapper._project)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public Project getWrappedProject() {
        return _project;
    }

    public Project getWrappedModel() {
        return _project;
    }

    public void resetOriginalValues() {
        _project.resetOriginalValues();
    }
}
