package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Buyer}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       Buyer
 * @generated
 */
public class BuyerWrapper implements Buyer, ModelWrapper<Buyer> {
    private Buyer _buyer;

    public BuyerWrapper(Buyer buyer) {
        _buyer = buyer;
    }

    public Class<?> getModelClass() {
        return Buyer.class;
    }

    public String getModelClassName() {
        return Buyer.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("buyerId", getBuyerId());
        attributes.put("name", getName());
        attributes.put("display", getDisplay());
        attributes.put("gruppoUsers", getGruppoUsers());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long buyerId = (Long) attributes.get("buyerId");

        if (buyerId != null) {
            setBuyerId(buyerId);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
        
        Integer gruppoUsers = (Integer) attributes.get("gruppoUsers");

        if (gruppoUsers != null) {
            setGruppoUsers(gruppoUsers);
        }
    }

    /**
    * Returns the primary key of this buyer.
    *
    * @return the primary key of this buyer
    */
    public long getPrimaryKey() {
        return _buyer.getPrimaryKey();
    }

    /**
    * Sets the primary key of this buyer.
    *
    * @param primaryKey the primary key of this buyer
    */
    public void setPrimaryKey(long primaryKey) {
        _buyer.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the buyer ID of this buyer.
    *
    * @return the buyer ID of this buyer
    */
    public long getBuyerId() {
        return _buyer.getBuyerId();
    }

    /**
    * Sets the buyer ID of this buyer.
    *
    * @param buyerId the buyer ID of this buyer
    */
    public void setBuyerId(long buyerId) {
        _buyer.setBuyerId(buyerId);
    }

    /**
    * Returns the name of this buyer.
    *
    * @return the name of this buyer
    */
    public java.lang.String getName() {
        return _buyer.getName();
    }

    /**
    * Sets the name of this buyer.
    *
    * @param name the name of this buyer
    */
    public void setName(java.lang.String name) {
        _buyer.setName(name);
    }

    /**
    * Returns the display of this buyer.
    *
    * @return the display of this buyer
    */
    public boolean getDisplay() {
        return _buyer.getDisplay();
    }

    /**
    * Returns <code>true</code> if this buyer is display.
    *
    * @return <code>true</code> if this buyer is display; <code>false</code> otherwise
    */
    public boolean isDisplay() {
        return _buyer.isDisplay();
    }

    /**
    * Sets whether this buyer is display.
    *
    * @param display the display of this buyer
    */
    public void setDisplay(boolean display) {
        _buyer.setDisplay(display);
    }

    public boolean isNew() {
        return _buyer.isNew();
    }

    public void setNew(boolean n) {
        _buyer.setNew(n);
    }

    public boolean isCachedModel() {
        return _buyer.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _buyer.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _buyer.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _buyer.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _buyer.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _buyer.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _buyer.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new BuyerWrapper((Buyer) _buyer.clone());
    }

    public int compareTo(Buyer buyer) {
        return _buyer.compareTo(buyer);
    }

    @Override
    public int hashCode() {
        return _buyer.hashCode();
    }

    public com.liferay.portal.model.CacheModel<Buyer> toCacheModel() {
        return _buyer.toCacheModel();
    }

    public Buyer toEscapedModel() {
        return new BuyerWrapper(_buyer.toEscapedModel());
    }

    public Buyer toUnescapedModel() {
        return new BuyerWrapper(_buyer.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _buyer.toString();
    }

    public java.lang.String toXmlString() {
        return _buyer.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _buyer.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof BuyerWrapper)) {
            return false;
        }

        BuyerWrapper buyerWrapper = (BuyerWrapper) obj;

        if (Validator.equals(_buyer, buyerWrapper._buyer)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public Buyer getWrappedBuyer() {
        return _buyer;
    }

    public Buyer getWrappedModel() {
        return _buyer;
    }

    public void resetOriginalValues() {
        _buyer.resetOriginalValues();
    }
    
	public int getGruppoUsers() {
		return _buyer.getGruppoUsers();
	}

	public void setGruppoUsers(int gruppoUsers) {
		_buyer.setGruppoUsers(gruppoUsers);
	}
}
