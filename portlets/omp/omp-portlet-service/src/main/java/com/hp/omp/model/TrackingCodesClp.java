package com.hp.omp.model;

import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.TrackingCodesLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class TrackingCodesClp extends BaseModelImpl<TrackingCodes>
    implements TrackingCodes {
    private long _trackingCodeId;
    private String _trackingCodeName;
    private String _trackingCodeDescription;
    private String _projectId;
    private String _subProjectId;
    private String _budgetCategoryId;
    private String _budgetSubCategoryId;
    private String _macroDriverId;
    private String _microDriverId;
    private String _odnpNameId;
    private String _wbsCodeId;
    private boolean _display;
    private BaseModel<?> _trackingCodesRemoteModel;

    public TrackingCodesClp() {
    }

    public Class<?> getModelClass() {
        return TrackingCodes.class;
    }

    public String getModelClassName() {
        return TrackingCodes.class.getName();
    }

    public long getPrimaryKey() {
        return _trackingCodeId;
    }

    public void setPrimaryKey(long primaryKey) {
        setTrackingCodeId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_trackingCodeId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("trackingCodeId", getTrackingCodeId());
        attributes.put("trackingCodeName", getTrackingCodeName());
        attributes.put("trackingCodeDescription", getTrackingCodeDescription());
        attributes.put("projectId", getProjectId());
        attributes.put("subProjectId", getSubProjectId());
        attributes.put("budgetCategoryId", getBudgetCategoryId());
        attributes.put("budgetSubCategoryId", getBudgetSubCategoryId());
        attributes.put("macroDriverId", getMacroDriverId());
        attributes.put("microDriverId", getMicroDriverId());
        attributes.put("odnpNameId", getOdnpNameId());
        attributes.put("wbsCodeId", getWbsCodeId());
        attributes.put("display", getDisplay());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long trackingCodeId = (Long) attributes.get("trackingCodeId");

        if (trackingCodeId != null) {
            setTrackingCodeId(trackingCodeId);
        }

        String trackingCodeName = (String) attributes.get("trackingCodeName");

        if (trackingCodeName != null) {
            setTrackingCodeName(trackingCodeName);
        }

        String trackingCodeDescription = (String) attributes.get(
                "trackingCodeDescription");

        if (trackingCodeDescription != null) {
            setTrackingCodeDescription(trackingCodeDescription);
        }

        String projectId = (String) attributes.get("projectId");

        if (projectId != null) {
            setProjectId(projectId);
        }

        String subProjectId = (String) attributes.get("subProjectId");

        if (subProjectId != null) {
            setSubProjectId(subProjectId);
        }

        String budgetCategoryId = (String) attributes.get("budgetCategoryId");

        if (budgetCategoryId != null) {
            setBudgetCategoryId(budgetCategoryId);
        }

        String budgetSubCategoryId = (String) attributes.get(
                "budgetSubCategoryId");

        if (budgetSubCategoryId != null) {
            setBudgetSubCategoryId(budgetSubCategoryId);
        }

        String macroDriverId = (String) attributes.get("macroDriverId");

        if (macroDriverId != null) {
            setMacroDriverId(macroDriverId);
        }

        String microDriverId = (String) attributes.get("microDriverId");

        if (microDriverId != null) {
            setMicroDriverId(microDriverId);
        }

        String odnpNameId = (String) attributes.get("odnpNameId");

        if (odnpNameId != null) {
            setOdnpNameId(odnpNameId);
        }

        String wbsCodeId = (String) attributes.get("wbsCodeId");

        if (wbsCodeId != null) {
            setWbsCodeId(wbsCodeId);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
    }

    public long getTrackingCodeId() {
        return _trackingCodeId;
    }

    public void setTrackingCodeId(long trackingCodeId) {
        _trackingCodeId = trackingCodeId;

        if (_trackingCodesRemoteModel != null) {
            try {
                Class<?> clazz = _trackingCodesRemoteModel.getClass();

                Method method = clazz.getMethod("setTrackingCodeId", long.class);

                method.invoke(_trackingCodesRemoteModel, trackingCodeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getTrackingCodeName() {
        return _trackingCodeName;
    }

    public void setTrackingCodeName(String trackingCodeName) {
        _trackingCodeName = trackingCodeName;

        if (_trackingCodesRemoteModel != null) {
            try {
                Class<?> clazz = _trackingCodesRemoteModel.getClass();

                Method method = clazz.getMethod("setTrackingCodeName",
                        String.class);

                method.invoke(_trackingCodesRemoteModel, trackingCodeName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getTrackingCodeDescription() {
        return _trackingCodeDescription;
    }

    public void setTrackingCodeDescription(String trackingCodeDescription) {
        _trackingCodeDescription = trackingCodeDescription;

        if (_trackingCodesRemoteModel != null) {
            try {
                Class<?> clazz = _trackingCodesRemoteModel.getClass();

                Method method = clazz.getMethod("setTrackingCodeDescription",
                        String.class);

                method.invoke(_trackingCodesRemoteModel, trackingCodeDescription);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getProjectId() {
        return _projectId;
    }

    public void setProjectId(String projectId) {
        _projectId = projectId;

        if (_trackingCodesRemoteModel != null) {
            try {
                Class<?> clazz = _trackingCodesRemoteModel.getClass();

                Method method = clazz.getMethod("setProjectId", String.class);

                method.invoke(_trackingCodesRemoteModel, projectId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getSubProjectId() {
        return _subProjectId;
    }

    public void setSubProjectId(String subProjectId) {
        _subProjectId = subProjectId;

        if (_trackingCodesRemoteModel != null) {
            try {
                Class<?> clazz = _trackingCodesRemoteModel.getClass();

                Method method = clazz.getMethod("setSubProjectId", String.class);

                method.invoke(_trackingCodesRemoteModel, subProjectId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getBudgetCategoryId() {
        return _budgetCategoryId;
    }

    public void setBudgetCategoryId(String budgetCategoryId) {
        _budgetCategoryId = budgetCategoryId;

        if (_trackingCodesRemoteModel != null) {
            try {
                Class<?> clazz = _trackingCodesRemoteModel.getClass();

                Method method = clazz.getMethod("setBudgetCategoryId",
                        String.class);

                method.invoke(_trackingCodesRemoteModel, budgetCategoryId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getBudgetSubCategoryId() {
        return _budgetSubCategoryId;
    }

    public void setBudgetSubCategoryId(String budgetSubCategoryId) {
        _budgetSubCategoryId = budgetSubCategoryId;

        if (_trackingCodesRemoteModel != null) {
            try {
                Class<?> clazz = _trackingCodesRemoteModel.getClass();

                Method method = clazz.getMethod("setBudgetSubCategoryId",
                        String.class);

                method.invoke(_trackingCodesRemoteModel, budgetSubCategoryId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getMacroDriverId() {
        return _macroDriverId;
    }

    public void setMacroDriverId(String macroDriverId) {
        _macroDriverId = macroDriverId;

        if (_trackingCodesRemoteModel != null) {
            try {
                Class<?> clazz = _trackingCodesRemoteModel.getClass();

                Method method = clazz.getMethod("setMacroDriverId", String.class);

                method.invoke(_trackingCodesRemoteModel, macroDriverId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getMicroDriverId() {
        return _microDriverId;
    }

    public void setMicroDriverId(String microDriverId) {
        _microDriverId = microDriverId;

        if (_trackingCodesRemoteModel != null) {
            try {
                Class<?> clazz = _trackingCodesRemoteModel.getClass();

                Method method = clazz.getMethod("setMicroDriverId", String.class);

                method.invoke(_trackingCodesRemoteModel, microDriverId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getOdnpNameId() {
        return _odnpNameId;
    }

    public void setOdnpNameId(String odnpNameId) {
        _odnpNameId = odnpNameId;

        if (_trackingCodesRemoteModel != null) {
            try {
                Class<?> clazz = _trackingCodesRemoteModel.getClass();

                Method method = clazz.getMethod("setOdnpNameId", String.class);

                method.invoke(_trackingCodesRemoteModel, odnpNameId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getWbsCodeId() {
        return _wbsCodeId;
    }

    public void setWbsCodeId(String wbsCodeId) {
        _wbsCodeId = wbsCodeId;

        if (_trackingCodesRemoteModel != null) {
            try {
                Class<?> clazz = _trackingCodesRemoteModel.getClass();

                Method method = clazz.getMethod("setWbsCodeId", String.class);

                method.invoke(_trackingCodesRemoteModel, wbsCodeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;

        if (_trackingCodesRemoteModel != null) {
            try {
                Class<?> clazz = _trackingCodesRemoteModel.getClass();

                Method method = clazz.getMethod("setDisplay", boolean.class);

                method.invoke(_trackingCodesRemoteModel, display);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getTrackingCodesRemoteModel() {
        return _trackingCodesRemoteModel;
    }

    public void setTrackingCodesRemoteModel(
        BaseModel<?> trackingCodesRemoteModel) {
        _trackingCodesRemoteModel = trackingCodesRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _trackingCodesRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_trackingCodesRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            TrackingCodesLocalServiceUtil.addTrackingCodes(this);
        } else {
            TrackingCodesLocalServiceUtil.updateTrackingCodes(this);
        }
    }

    @Override
    public TrackingCodes toEscapedModel() {
        return (TrackingCodes) ProxyUtil.newProxyInstance(TrackingCodes.class.getClassLoader(),
            new Class[] { TrackingCodes.class }, new AutoEscapeBeanHandler(this));
    }

    public TrackingCodes toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        TrackingCodesClp clone = new TrackingCodesClp();

        clone.setTrackingCodeId(getTrackingCodeId());
        clone.setTrackingCodeName(getTrackingCodeName());
        clone.setTrackingCodeDescription(getTrackingCodeDescription());
        clone.setProjectId(getProjectId());
        clone.setSubProjectId(getSubProjectId());
        clone.setBudgetCategoryId(getBudgetCategoryId());
        clone.setBudgetSubCategoryId(getBudgetSubCategoryId());
        clone.setMacroDriverId(getMacroDriverId());
        clone.setMicroDriverId(getMicroDriverId());
        clone.setOdnpNameId(getOdnpNameId());
        clone.setWbsCodeId(getWbsCodeId());
        clone.setDisplay(getDisplay());

        return clone;
    }

    public int compareTo(TrackingCodes trackingCodes) {
        long primaryKey = trackingCodes.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof TrackingCodesClp)) {
            return false;
        }

        TrackingCodesClp trackingCodes = (TrackingCodesClp) obj;

        long primaryKey = trackingCodes.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(25);

        sb.append("{trackingCodeId=");
        sb.append(getTrackingCodeId());
        sb.append(", trackingCodeName=");
        sb.append(getTrackingCodeName());
        sb.append(", trackingCodeDescription=");
        sb.append(getTrackingCodeDescription());
        sb.append(", projectId=");
        sb.append(getProjectId());
        sb.append(", subProjectId=");
        sb.append(getSubProjectId());
        sb.append(", budgetCategoryId=");
        sb.append(getBudgetCategoryId());
        sb.append(", budgetSubCategoryId=");
        sb.append(getBudgetSubCategoryId());
        sb.append(", macroDriverId=");
        sb.append(getMacroDriverId());
        sb.append(", microDriverId=");
        sb.append(getMicroDriverId());
        sb.append(", odnpNameId=");
        sb.append(getOdnpNameId());
        sb.append(", wbsCodeId=");
        sb.append(getWbsCodeId());
        sb.append(", display=");
        sb.append(getDisplay());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(40);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.TrackingCodes");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>trackingCodeId</column-name><column-value><![CDATA[");
        sb.append(getTrackingCodeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>trackingCodeName</column-name><column-value><![CDATA[");
        sb.append(getTrackingCodeName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>trackingCodeDescription</column-name><column-value><![CDATA[");
        sb.append(getTrackingCodeDescription());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>projectId</column-name><column-value><![CDATA[");
        sb.append(getProjectId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>subProjectId</column-name><column-value><![CDATA[");
        sb.append(getSubProjectId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>budgetCategoryId</column-name><column-value><![CDATA[");
        sb.append(getBudgetCategoryId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>budgetSubCategoryId</column-name><column-value><![CDATA[");
        sb.append(getBudgetSubCategoryId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>macroDriverId</column-name><column-value><![CDATA[");
        sb.append(getMacroDriverId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>microDriverId</column-name><column-value><![CDATA[");
        sb.append(getMicroDriverId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>odnpNameId</column-name><column-value><![CDATA[");
        sb.append(getOdnpNameId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>wbsCodeId</column-name><column-value><![CDATA[");
        sb.append(getWbsCodeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>display</column-name><column-value><![CDATA[");
        sb.append(getDisplay());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
