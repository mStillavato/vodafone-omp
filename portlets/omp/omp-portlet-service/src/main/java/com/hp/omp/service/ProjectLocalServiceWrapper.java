package com.hp.omp.service;

import java.io.OutputStream;
import java.util.List;

import com.hp.omp.model.Project;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link ProjectLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       ProjectLocalService
 * @generated
 */
public class ProjectLocalServiceWrapper implements ProjectLocalService,
    ServiceWrapper<ProjectLocalService> {
    private ProjectLocalService _projectLocalService;

    public ProjectLocalServiceWrapper(ProjectLocalService projectLocalService) {
        _projectLocalService = projectLocalService;
    }

    /**
    * Adds the project to the database. Also notifies the appropriate model listeners.
    *
    * @param project the project
    * @return the project that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Project addProject(com.hp.omp.model.Project project)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _projectLocalService.addProject(project);
    }

    /**
    * Creates a new project with the primary key. Does not add the project to the database.
    *
    * @param projectId the primary key for the new project
    * @return the new project
    */
    public com.hp.omp.model.Project createProject(long projectId) {
        return _projectLocalService.createProject(projectId);
    }

    /**
    * Deletes the project with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param projectId the primary key of the project
    * @return the project that was removed
    * @throws PortalException if a project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Project deleteProject(long projectId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _projectLocalService.deleteProject(projectId);
    }

    /**
    * Deletes the project from the database. Also notifies the appropriate model listeners.
    *
    * @param project the project
    * @return the project that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Project deleteProject(
        com.hp.omp.model.Project project)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _projectLocalService.deleteProject(project);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _projectLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _projectLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _projectLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _projectLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _projectLocalService.dynamicQueryCount(dynamicQuery);
    }

    public com.hp.omp.model.Project fetchProject(long projectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _projectLocalService.fetchProject(projectId);
    }

    /**
    * Returns the project with the primary key.
    *
    * @param projectId the primary key of the project
    * @return the project
    * @throws PortalException if a project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Project getProject(long projectId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _projectLocalService.getProject(projectId);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _projectLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the projects.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of projects
    * @param end the upper bound of the range of projects (not inclusive)
    * @return the range of projects
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.Project> getProjects(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _projectLocalService.getProjects(start, end);
    }

    /**
    * Returns the number of projects.
    *
    * @return the number of projects
    * @throws SystemException if a system exception occurred
    */
    public int getProjectsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _projectLocalService.getProjectsCount();
    }

    /**
    * Updates the project in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param project the project
    * @return the project that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Project updateProject(
        com.hp.omp.model.Project project)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _projectLocalService.updateProject(project);
    }

    /**
    * Updates the project in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param project the project
    * @param merge whether to merge the project with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the project that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Project updateProject(
        com.hp.omp.model.Project project, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _projectLocalService.updateProject(project, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _projectLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _projectLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _projectLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    public com.hp.omp.model.Project getProjectByName(java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _projectLocalService.getProjectByName(name);
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public ProjectLocalService getWrappedProjectLocalService() {
        return _projectLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedProjectLocalService(
        ProjectLocalService projectLocalService) {
        _projectLocalService = projectLocalService;
    }

    public ProjectLocalService getWrappedService() {
        return _projectLocalService;
    }

    public void setWrappedService(ProjectLocalService projectLocalService) {
        _projectLocalService = projectLocalService;
    }

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Project> getProjectsList(int start, int end, String searchFilter)
			throws SystemException {
		return _projectLocalService.getProjectsList(start, end, searchFilter);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getProjectsCountList(String searchFilter) throws SystemException {
		return _projectLocalService.getProjectsCountList(searchFilter);
	}

	public void exportProjectsAsExcel(List<Project> listProjects,
			List<Object> header, OutputStream out) throws SystemException {
		_projectLocalService.exportProjectsAsExcel(listProjects, header, out);
	}
}
