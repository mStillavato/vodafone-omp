package com.hp.omp.service;

import java.io.OutputStream;
import java.util.List;

import com.hp.omp.model.Vendor;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.InvokableLocalService;


public class VendorLocalServiceClp implements VendorLocalService {
	private InvokableLocalService _invokableLocalService;
	private String _methodName0;
	private String[] _methodParameterTypes0;
	private String _methodName1;
	private String[] _methodParameterTypes1;
	private String _methodName2;
	private String[] _methodParameterTypes2;
	private String _methodName3;
	private String[] _methodParameterTypes3;
	private String _methodName4;
	private String[] _methodParameterTypes4;
	private String _methodName5;
	private String[] _methodParameterTypes5;
	private String _methodName6;
	private String[] _methodParameterTypes6;
	private String _methodName7;
	private String[] _methodParameterTypes7;
	private String _methodName8;
	private String[] _methodParameterTypes8;
	private String _methodName9;
	private String[] _methodParameterTypes9;
	private String _methodName10;
	private String[] _methodParameterTypes10;
	private String _methodName11;
	private String[] _methodParameterTypes11;
	private String _methodName12;
	private String[] _methodParameterTypes12;
	private String _methodName13;
	private String[] _methodParameterTypes13;
	private String _methodName14;
	private String[] _methodParameterTypes14;
	private String _methodName15;
	private String[] _methodParameterTypes15;
	private String _methodName16;
	private String[] _methodParameterTypes16;
	private String _methodName17;
	private String[] _methodParameterTypes17;
	private String _methodName19;
	private String[] _methodParameterTypes19;
	private String _methodName20;
	private String[] _methodParameterTypes20;
	private String _methodName21;
	private String[] _methodParameterTypes21;    
	private String _methodName22;
	private String[] _methodParameterTypes22; 
	private String _methodName23;
	private String[] _methodParameterTypes23;

	public VendorLocalServiceClp(InvokableLocalService invokableLocalService) {
		_invokableLocalService = invokableLocalService;

		_methodName0 = "addVendor";

		_methodParameterTypes0 = new String[] { "com.hp.omp.model.Vendor" };

		_methodName1 = "createVendor";

		_methodParameterTypes1 = new String[] { "long" };

		_methodName2 = "deleteVendor";

		_methodParameterTypes2 = new String[] { "long" };

		_methodName3 = "deleteVendor";

		_methodParameterTypes3 = new String[] { "com.hp.omp.model.Vendor" };

		_methodName4 = "dynamicQuery";

		_methodParameterTypes4 = new String[] {  };

		_methodName5 = "dynamicQuery";

		_methodParameterTypes5 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
		};

		_methodName6 = "dynamicQuery";

		_methodParameterTypes6 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
		};

		_methodName7 = "dynamicQuery";

		_methodParameterTypes7 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
				"com.liferay.portal.kernel.util.OrderByComparator"
		};

		_methodName8 = "dynamicQueryCount";

		_methodParameterTypes8 = new String[] {
				"com.liferay.portal.kernel.dao.orm.DynamicQuery"
		};

		_methodName9 = "fetchVendor";

		_methodParameterTypes9 = new String[] { "long" };

		_methodName10 = "getVendor";

		_methodParameterTypes10 = new String[] { "long" };

		_methodName11 = "getPersistedModel";

		_methodParameterTypes11 = new String[] { "java.io.Serializable" };

		_methodName12 = "getVendors";

		_methodParameterTypes12 = new String[] { "int", "int" };

		_methodName13 = "getVendorsCount";

		_methodParameterTypes13 = new String[] {  };

		_methodName14 = "updateVendor";

		_methodParameterTypes14 = new String[] { "com.hp.omp.model.Vendor" };

		_methodName15 = "updateVendor";

		_methodParameterTypes15 = new String[] {
				"com.hp.omp.model.Vendor", "boolean"
		};

		_methodName16 = "getBeanIdentifier";

		_methodParameterTypes16 = new String[] {  };

		_methodName17 = "setBeanIdentifier";

		_methodParameterTypes17 = new String[] { "java.lang.String" };

		_methodName19 = "getVendorByName";

		_methodParameterTypes19 = new String[] { "java.lang.String", "int" };

		_methodName20 = "getVendorsList";

		_methodParameterTypes20 = new String[] { "int", "int", "java.lang.String", "int" };

		_methodName21 = "getVendorBySupplierCode";

		_methodParameterTypes21 = new String[] { "java.lang.String", "int" };

		_methodName22 = "getVendorsCountList";

		_methodParameterTypes22 = new String[] { "java.lang.String", "int" };
		
        _methodName23 = "exportVendorsAsExcel";
        _methodParameterTypes23 = new String[] { "java.util.List", "java.util.List", "java.io.OutputStream" };
	}

	public com.hp.omp.model.Vendor addVendor(com.hp.omp.model.Vendor vendor)
			throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName0,
					_methodParameterTypes0,
					new Object[] { ClpSerializer.translateInput(vendor) });
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException) t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return (com.hp.omp.model.Vendor) ClpSerializer.translateOutput(returnObj);
	}

	public com.hp.omp.model.Vendor createVendor(long vendorId) {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName1,
					_methodParameterTypes1, new Object[] { vendorId });
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return (com.hp.omp.model.Vendor) ClpSerializer.translateOutput(returnObj);
	}

	public com.hp.omp.model.Vendor deleteVendor(long vendorId)
			throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName2,
					_methodParameterTypes2, new Object[] { vendorId });
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
				throw (com.liferay.portal.kernel.exception.PortalException) t;
			}

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException) t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return (com.hp.omp.model.Vendor) ClpSerializer.translateOutput(returnObj);
	}

	public com.hp.omp.model.Vendor deleteVendor(com.hp.omp.model.Vendor vendor)
			throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName3,
					_methodParameterTypes3,
					new Object[] { ClpSerializer.translateInput(vendor) });
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException) t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return (com.hp.omp.model.Vendor) ClpSerializer.translateOutput(returnObj);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName4,
					_methodParameterTypes4, new Object[] {  });
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return (com.liferay.portal.kernel.dao.orm.DynamicQuery) ClpSerializer.translateOutput(returnObj);
	}

	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
					throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName5,
					_methodParameterTypes5,
					new Object[] { ClpSerializer.translateInput(dynamicQuery) });
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException) t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return (java.util.List) ClpSerializer.translateOutput(returnObj);
	}

	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
			int end) throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName6,
					_methodParameterTypes6,
					new Object[] {
					ClpSerializer.translateInput(dynamicQuery),

					start,

					end
			});
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException) t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return (java.util.List) ClpSerializer.translateOutput(returnObj);
	}

	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
			int end,
			com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
					throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName7,
					_methodParameterTypes7,
					new Object[] {
					ClpSerializer.translateInput(dynamicQuery),

					start,

					end,

					ClpSerializer.translateInput(orderByComparator)
			});
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException) t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return (java.util.List) ClpSerializer.translateOutput(returnObj);
	}

	public long dynamicQueryCount(
			com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
					throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName8,
					_methodParameterTypes8,
					new Object[] { ClpSerializer.translateInput(dynamicQuery) });
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException) t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return ((Long) returnObj).longValue();
	}

	public com.hp.omp.model.Vendor fetchVendor(long vendorId)
			throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName9,
					_methodParameterTypes9, new Object[] { vendorId });
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException) t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return (com.hp.omp.model.Vendor) ClpSerializer.translateOutput(returnObj);
	}

	public com.hp.omp.model.Vendor getVendor(long vendorId)
			throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName10,
					_methodParameterTypes10, new Object[] { vendorId });
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
				throw (com.liferay.portal.kernel.exception.PortalException) t;
			}

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException) t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return (com.hp.omp.model.Vendor) ClpSerializer.translateOutput(returnObj);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
					throws com.liferay.portal.kernel.exception.PortalException,
					com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName11,
					_methodParameterTypes11,
					new Object[] { ClpSerializer.translateInput(primaryKeyObj) });
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
				throw (com.liferay.portal.kernel.exception.PortalException) t;
			}

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException) t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return (com.liferay.portal.model.PersistedModel) ClpSerializer.translateOutput(returnObj);
	}

	public java.util.List<com.hp.omp.model.Vendor> getVendors(int start, int end)
			throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName12,
					_methodParameterTypes12, new Object[] { start, end });
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException) t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return (java.util.List<com.hp.omp.model.Vendor>) ClpSerializer.translateOutput(returnObj);
	}

	public int getVendorsCount()
			throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName13,
					_methodParameterTypes13, new Object[] {  });
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException) t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return ((Integer) returnObj).intValue();
	}

	public com.hp.omp.model.Vendor updateVendor(com.hp.omp.model.Vendor vendor)
			throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName14,
					_methodParameterTypes14,
					new Object[] { ClpSerializer.translateInput(vendor) });
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException) t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return (com.hp.omp.model.Vendor) ClpSerializer.translateOutput(returnObj);
	}

	public com.hp.omp.model.Vendor updateVendor(
			com.hp.omp.model.Vendor vendor, boolean merge)
					throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName15,
					_methodParameterTypes15,
					new Object[] { ClpSerializer.translateInput(vendor), merge });
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException) t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return (com.hp.omp.model.Vendor) ClpSerializer.translateOutput(returnObj);
	}

	public java.lang.String getBeanIdentifier() {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName16,
					_methodParameterTypes16, new Object[] {  });
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return (java.lang.String) ClpSerializer.translateOutput(returnObj);
	}

	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		try {
			_invokableLocalService.invokeMethod(_methodName17,
					_methodParameterTypes17,
					new Object[] { ClpSerializer.translateInput(beanIdentifier) });
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}
	}

	public java.lang.Object invokeMethod(java.lang.String name,
			java.lang.String[] parameterTypes, java.lang.Object[] arguments)
					throws java.lang.Throwable {
		throw new UnsupportedOperationException();
	}

	public com.hp.omp.model.Vendor getVendorByName(java.lang.String name, int gruppoUsers)
			throws com.liferay.portal.kernel.exception.SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName19,
					_methodParameterTypes19,
					new Object[] { ClpSerializer.translateInput(name),
					ClpSerializer.translateInput(gruppoUsers)});
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException) t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return (com.hp.omp.model.Vendor) ClpSerializer.translateOutput(returnObj);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Vendor> getVendorsList(int start, int end, String searchFilter, int gruppoUsers)
			throws SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName20,
					_methodParameterTypes20,
					new Object[] { ClpSerializer.translateInput(start),
					ClpSerializer.translateInput(end), 
					ClpSerializer.translateInput(searchFilter),
					ClpSerializer.translateInput(gruppoUsers)
			});
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException) t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return (List<com.hp.omp.model.Vendor>) ClpSerializer.translateOutput(returnObj);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Vendor getVendorBySupplierCode(String supplierCode, int gruppoUsers)
			throws SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName21,
					_methodParameterTypes21,
					new Object[] { ClpSerializer.translateInput(supplierCode),
					ClpSerializer.translateInput(gruppoUsers) });
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException) t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return (com.hp.omp.model.Vendor) ClpSerializer.translateOutput(returnObj);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getVendorsCountList(String searchFilter, int gruppoUsers) throws SystemException {
		Object returnObj = null;

		try {
			returnObj = _invokableLocalService.invokeMethod(_methodName22,
					_methodParameterTypes22, 
					new Object[] { ClpSerializer.translateInput(searchFilter), ClpSerializer.translateInput(gruppoUsers)  });
		} catch (Throwable t) {
			t = ClpSerializer.translateThrowable(t);

			if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
				throw (com.liferay.portal.kernel.exception.SystemException) t;
			}

			if (t instanceof RuntimeException) {
				throw (RuntimeException) t;
			} else {
				throw new RuntimeException(t.getClass().getName() +
						" is not a valid exception");
			}
		}

		return ((Integer) returnObj).intValue();
	}

	public void exportVendorsAsExcel(List<Vendor> listVendors,
			List<Object> header, OutputStream out) throws SystemException {
        try {
            _invokableLocalService.invokeMethod(_methodName23,
                _methodParameterTypes23,
                new Object[] { ClpSerializer.translateInput(listVendors), 
            		ClpSerializer.translateInput(header), 
            		ClpSerializer.translateInput(out)
                });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }
		
	}
}
