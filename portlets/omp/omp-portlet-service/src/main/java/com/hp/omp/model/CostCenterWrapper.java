package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link CostCenter}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       CostCenter
 * @generated
 */
public class CostCenterWrapper implements CostCenter, ModelWrapper<CostCenter> {
    private CostCenter _costCenter;

    public CostCenterWrapper(CostCenter costCenter) {
        _costCenter = costCenter;
    }

    public Class<?> getModelClass() {
        return CostCenter.class;
    }

    public String getModelClassName() {
        return CostCenter.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("costCenterId", getCostCenterId());
        attributes.put("costCenterName", getCostCenterName());
        attributes.put("description", getDescription());
        attributes.put("display", getDisplay());
        attributes.put("gruppoUsers", getGruppoUsers());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long costCenterId = (Long) attributes.get("costCenterId");

        if (costCenterId != null) {
            setCostCenterId(costCenterId);
        }

        String costCenterName = (String) attributes.get("costCenterName");

        if (costCenterName != null) {
            setCostCenterName(costCenterName);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
        
        Integer gruppoUsers = (Integer) attributes.get("gruppoUsers");

        if (gruppoUsers != null) {
            setGruppoUsers(gruppoUsers);
        }
    }

    /**
    * Returns the primary key of this cost center.
    *
    * @return the primary key of this cost center
    */
    public long getPrimaryKey() {
        return _costCenter.getPrimaryKey();
    }

    /**
    * Sets the primary key of this cost center.
    *
    * @param primaryKey the primary key of this cost center
    */
    public void setPrimaryKey(long primaryKey) {
        _costCenter.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the cost center ID of this cost center.
    *
    * @return the cost center ID of this cost center
    */
    public long getCostCenterId() {
        return _costCenter.getCostCenterId();
    }

    /**
    * Sets the cost center ID of this cost center.
    *
    * @param costCenterId the cost center ID of this cost center
    */
    public void setCostCenterId(long costCenterId) {
        _costCenter.setCostCenterId(costCenterId);
    }

    /**
    * Returns the cost center name of this cost center.
    *
    * @return the cost center name of this cost center
    */
    public java.lang.String getCostCenterName() {
        return _costCenter.getCostCenterName();
    }

    /**
    * Sets the cost center name of this cost center.
    *
    * @param costCenterName the cost center name of this cost center
    */
    public void setCostCenterName(java.lang.String costCenterName) {
        _costCenter.setCostCenterName(costCenterName);
    }

    /**
    * Returns the description of this cost center.
    *
    * @return the description of this cost center
    */
    public java.lang.String getDescription() {
        return _costCenter.getDescription();
    }

    /**
    * Sets the description of this cost center.
    *
    * @param description the description of this cost center
    */
    public void setDescription(java.lang.String description) {
        _costCenter.setDescription(description);
    }

    /**
    * Returns the display of this cost center.
    *
    * @return the display of this cost center
    */
    public boolean getDisplay() {
        return _costCenter.getDisplay();
    }

    /**
    * Returns <code>true</code> if this cost center is display.
    *
    * @return <code>true</code> if this cost center is display; <code>false</code> otherwise
    */
    public boolean isDisplay() {
        return _costCenter.isDisplay();
    }

    /**
    * Sets whether this cost center is display.
    *
    * @param display the display of this cost center
    */
    public void setDisplay(boolean display) {
        _costCenter.setDisplay(display);
    }

    public boolean isNew() {
        return _costCenter.isNew();
    }

    public void setNew(boolean n) {
        _costCenter.setNew(n);
    }

    public boolean isCachedModel() {
        return _costCenter.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _costCenter.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _costCenter.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _costCenter.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _costCenter.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _costCenter.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _costCenter.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new CostCenterWrapper((CostCenter) _costCenter.clone());
    }

    public int compareTo(CostCenter costCenter) {
        return _costCenter.compareTo(costCenter);
    }

    @Override
    public int hashCode() {
        return _costCenter.hashCode();
    }

    public com.liferay.portal.model.CacheModel<CostCenter> toCacheModel() {
        return _costCenter.toCacheModel();
    }

    public CostCenter toEscapedModel() {
        return new CostCenterWrapper(_costCenter.toEscapedModel());
    }

    public CostCenter toUnescapedModel() {
        return new CostCenterWrapper(_costCenter.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _costCenter.toString();
    }

    public java.lang.String toXmlString() {
        return _costCenter.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _costCenter.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CostCenterWrapper)) {
            return false;
        }

        CostCenterWrapper costCenterWrapper = (CostCenterWrapper) obj;

        if (Validator.equals(_costCenter, costCenterWrapper._costCenter)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public CostCenter getWrappedCostCenter() {
        return _costCenter;
    }

    public CostCenter getWrappedModel() {
        return _costCenter;
    }

    public void resetOriginalValues() {
        _costCenter.resetOriginalValues();
    }
    
	public int getGruppoUsers() {
		return _costCenter.getGruppoUsers();
	}

	public void setGruppoUsers(int gruppoUsers) {
		_costCenter.setGruppoUsers(gruppoUsers);
	}
}
