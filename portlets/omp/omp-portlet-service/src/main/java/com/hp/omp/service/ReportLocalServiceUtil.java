package com.hp.omp.service;

import com.hp.omp.model.custom.SearchReportCheckIcDTO;
import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the report local service. This utility wraps {@link com.hp.omp.service.impl.ReportLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see ReportLocalService
 * @see com.hp.omp.service.base.ReportLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.ReportLocalServiceImpl
 * @generated
 */
public class ReportLocalServiceUtil {
    private static ReportLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.hp.omp.service.impl.ReportLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static java.util.List<java.lang.String> getAllCostCenters()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getAllCostCenters();
    }

    public static java.util.List<java.lang.String> getAllProjectNames()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getAllProjectNames();
    }

    public static java.util.List<java.lang.String> getSubProjects(
        java.lang.Long projectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getSubProjects(projectId);
    }

    public static java.util.List<java.lang.String> getAllFiscalYears()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getAllFiscalYears();
    }

    public static java.lang.Double generateAggregateReportClosed(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().generateAggregateReportClosed(reportDTO);
    }

    public static java.lang.Double generateAggregateReportClosing(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().generateAggregateReportClosing(reportDTO);
    }

    public static java.lang.Double generateAggregateReportTotalPoistionValues(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().generateAggregateReportTotalPoistionValues(reportDTO);
    }

    public static java.lang.Double generateAggregatePrPositionsWithoutPoNumberValue(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().generateAggregatePrPositionsWithoutPoNumberValue(reportDTO);
    }

    public static java.util.List<com.hp.omp.model.custom.ExcelTableRow> generateDetailReport(
        com.hp.omp.model.custom.ReportDTO reportDTO)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().generateDetailReport(reportDTO);
    }

    public static void exportDetailsReportAsExcel(
        com.hp.omp.model.custom.ReportDTO reportDTO,
        java.util.List<java.lang.Object> header, java.io.OutputStream out)
        throws com.liferay.portal.kernel.exception.SystemException {
        getService().exportDetailsReportAsExcel(reportDTO, header, out);
    }

    public static void exportReportAsExcel(
        com.hp.omp.model.custom.ReportDTO reportDTO,
        java.util.List<java.lang.Object> header, java.io.OutputStream out)
        throws com.liferay.portal.kernel.exception.SystemException {
        getService().exportReportAsExcel(reportDTO, header, out);
    }

    public static void exportReportCheckIcAsExcel(
    		java.util.List<java.lang.Object> header, java.io.OutputStream out)
    				throws com.liferay.portal.kernel.exception.SystemException {
    	getService().exportReportCheckIcAsExcel(header, out);
    }

    public static java.util.List<com.hp.omp.model.custom.ReportCheckIcDTO> geReportCheckIcList(SearchReportCheckIcDTO searchDTO) 
    		throws SystemException {
    	return getService().getReportCheckIcList(searchDTO);
    }

    public static java.lang.Long getReportCheckIcListCount(SearchReportCheckIcDTO searchDTO) 
    		throws SystemException {
    	return getService().getReportCheckIcListCount(searchDTO);
    }
    	
    public static void clearService() {
        _service = null;
    }

    public static ReportLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    ReportLocalService.class.getName());

            if (invokableLocalService instanceof ReportLocalService) {
                _service = (ReportLocalService) invokableLocalService;
            } else {
                _service = new ReportLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(ReportLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated
     */
    public void setService(ReportLocalService service) {
    }
}
