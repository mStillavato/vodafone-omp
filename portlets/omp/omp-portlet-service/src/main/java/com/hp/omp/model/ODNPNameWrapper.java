package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ODNPName}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       ODNPName
 * @generated
 */
public class ODNPNameWrapper implements ODNPName, ModelWrapper<ODNPName> {
    private ODNPName _odnpName;

    public ODNPNameWrapper(ODNPName odnpName) {
        _odnpName = odnpName;
    }

    public Class<?> getModelClass() {
        return ODNPName.class;
    }

    public String getModelClassName() {
        return ODNPName.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("odnpNameId", getOdnpNameId());
        attributes.put("odnpNameName", getOdnpNameName());
        attributes.put("display", getDisplay());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long odnpNameId = (Long) attributes.get("odnpNameId");

        if (odnpNameId != null) {
            setOdnpNameId(odnpNameId);
        }

        String odnpNameName = (String) attributes.get("odnpNameName");

        if (odnpNameName != null) {
            setOdnpNameName(odnpNameName);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
    }

    /**
    * Returns the primary key of this o d n p name.
    *
    * @return the primary key of this o d n p name
    */
    public long getPrimaryKey() {
        return _odnpName.getPrimaryKey();
    }

    /**
    * Sets the primary key of this o d n p name.
    *
    * @param primaryKey the primary key of this o d n p name
    */
    public void setPrimaryKey(long primaryKey) {
        _odnpName.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the odnp name ID of this o d n p name.
    *
    * @return the odnp name ID of this o d n p name
    */
    public long getOdnpNameId() {
        return _odnpName.getOdnpNameId();
    }

    /**
    * Sets the odnp name ID of this o d n p name.
    *
    * @param odnpNameId the odnp name ID of this o d n p name
    */
    public void setOdnpNameId(long odnpNameId) {
        _odnpName.setOdnpNameId(odnpNameId);
    }

    /**
    * Returns the odnp name name of this o d n p name.
    *
    * @return the odnp name name of this o d n p name
    */
    public java.lang.String getOdnpNameName() {
        return _odnpName.getOdnpNameName();
    }

    /**
    * Sets the odnp name name of this o d n p name.
    *
    * @param odnpNameName the odnp name name of this o d n p name
    */
    public void setOdnpNameName(java.lang.String odnpNameName) {
        _odnpName.setOdnpNameName(odnpNameName);
    }

    /**
    * Returns the display of this o d n p name.
    *
    * @return the display of this o d n p name
    */
    public boolean getDisplay() {
        return _odnpName.getDisplay();
    }

    /**
    * Returns <code>true</code> if this o d n p name is display.
    *
    * @return <code>true</code> if this o d n p name is display; <code>false</code> otherwise
    */
    public boolean isDisplay() {
        return _odnpName.isDisplay();
    }

    /**
    * Sets whether this o d n p name is display.
    *
    * @param display the display of this o d n p name
    */
    public void setDisplay(boolean display) {
        _odnpName.setDisplay(display);
    }

    public boolean isNew() {
        return _odnpName.isNew();
    }

    public void setNew(boolean n) {
        _odnpName.setNew(n);
    }

    public boolean isCachedModel() {
        return _odnpName.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _odnpName.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _odnpName.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _odnpName.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _odnpName.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _odnpName.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _odnpName.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new ODNPNameWrapper((ODNPName) _odnpName.clone());
    }

    public int compareTo(ODNPName odnpName) {
        return _odnpName.compareTo(odnpName);
    }

    @Override
    public int hashCode() {
        return _odnpName.hashCode();
    }

    public com.liferay.portal.model.CacheModel<ODNPName> toCacheModel() {
        return _odnpName.toCacheModel();
    }

    public ODNPName toEscapedModel() {
        return new ODNPNameWrapper(_odnpName.toEscapedModel());
    }

    public ODNPName toUnescapedModel() {
        return new ODNPNameWrapper(_odnpName.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _odnpName.toString();
    }

    public java.lang.String toXmlString() {
        return _odnpName.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _odnpName.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ODNPNameWrapper)) {
            return false;
        }

        ODNPNameWrapper odnpNameWrapper = (ODNPNameWrapper) obj;

        if (Validator.equals(_odnpName, odnpNameWrapper._odnpName)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public ODNPName getWrappedODNPName() {
        return _odnpName;
    }

    public ODNPName getWrappedModel() {
        return _odnpName;
    }

    public void resetOriginalValues() {
        _odnpName.resetOriginalValues();
    }
}
