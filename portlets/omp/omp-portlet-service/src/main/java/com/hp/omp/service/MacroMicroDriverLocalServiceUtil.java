package com.hp.omp.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the macro micro driver local service. This utility wraps {@link com.hp.omp.service.impl.MacroMicroDriverLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see MacroMicroDriverLocalService
 * @see com.hp.omp.service.base.MacroMicroDriverLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.MacroMicroDriverLocalServiceImpl
 * @generated
 */
public class MacroMicroDriverLocalServiceUtil {
    private static MacroMicroDriverLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.hp.omp.service.impl.MacroMicroDriverLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the macro micro driver to the database. Also notifies the appropriate model listeners.
    *
    * @param macroMicroDriver the macro micro driver
    * @return the macro micro driver that was added
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver addMacroMicroDriver(
        com.hp.omp.model.MacroMicroDriver macroMicroDriver)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addMacroMicroDriver(macroMicroDriver);
    }

    /**
    * Creates a new macro micro driver with the primary key. Does not add the macro micro driver to the database.
    *
    * @param macroMicroDriverId the primary key for the new macro micro driver
    * @return the new macro micro driver
    */
    public static com.hp.omp.model.MacroMicroDriver createMacroMicroDriver(
        long macroMicroDriverId) {
        return getService().createMacroMicroDriver(macroMicroDriverId);
    }

    /**
    * Deletes the macro micro driver with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param macroMicroDriverId the primary key of the macro micro driver
    * @return the macro micro driver that was removed
    * @throws PortalException if a macro micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver deleteMacroMicroDriver(
        long macroMicroDriverId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteMacroMicroDriver(macroMicroDriverId);
    }

    /**
    * Deletes the macro micro driver from the database. Also notifies the appropriate model listeners.
    *
    * @param macroMicroDriver the macro micro driver
    * @return the macro micro driver that was removed
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver deleteMacroMicroDriver(
        com.hp.omp.model.MacroMicroDriver macroMicroDriver)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteMacroMicroDriver(macroMicroDriver);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    public static com.hp.omp.model.MacroMicroDriver fetchMacroMicroDriver(
        long macroMicroDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchMacroMicroDriver(macroMicroDriverId);
    }

    /**
    * Returns the macro micro driver with the primary key.
    *
    * @param macroMicroDriverId the primary key of the macro micro driver
    * @return the macro micro driver
    * @throws PortalException if a macro micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver getMacroMicroDriver(
        long macroMicroDriverId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getMacroMicroDriver(macroMicroDriverId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the macro micro drivers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of macro micro drivers
    * @param end the upper bound of the range of macro micro drivers (not inclusive)
    * @return the range of macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MacroMicroDriver> getMacroMicroDrivers(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getMacroMicroDrivers(start, end);
    }

    /**
    * Returns the number of macro micro drivers.
    *
    * @return the number of macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static int getMacroMicroDriversCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getMacroMicroDriversCount();
    }

    /**
    * Updates the macro micro driver in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param macroMicroDriver the macro micro driver
    * @return the macro micro driver that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver updateMacroMicroDriver(
        com.hp.omp.model.MacroMicroDriver macroMicroDriver)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateMacroMicroDriver(macroMicroDriver);
    }

    /**
    * Updates the macro micro driver in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param macroMicroDriver the macro micro driver
    * @param merge whether to merge the macro micro driver with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the macro micro driver that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver updateMacroMicroDriver(
        com.hp.omp.model.MacroMicroDriver macroMicroDriver, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateMacroMicroDriver(macroMicroDriver, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static java.util.List<com.hp.omp.model.MacroMicroDriver> getMacroMicroDriverByMacroDriverId(
        java.lang.Long macroDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getMacroMicroDriverByMacroDriverId(macroDriverId);
    }

    public static java.util.List<com.hp.omp.model.MacroMicroDriver> getMacroMicroDriverByMicroDriverId(
        java.lang.Long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getMacroMicroDriverByMicroDriverId(microDriverId);
    }

    public static java.util.List<com.hp.omp.model.MacroMicroDriver> getMacroMicroDriverByMacroDriverIdAndMicroDriverId(
        java.lang.Long macroDriverId, java.lang.Long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .getMacroMicroDriverByMacroDriverIdAndMicroDriverId(macroDriverId,
            microDriverId);
    }

    public static java.util.List<com.hp.omp.model.MacroDriver> getMacroDriversByMicroDriverId(
        java.lang.Long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getMacroDriversByMicroDriverId(microDriverId);
    }

    public static void clearService() {
        _service = null;
    }

    public static MacroMicroDriverLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    MacroMicroDriverLocalService.class.getName());

            if (invokableLocalService instanceof MacroMicroDriverLocalService) {
                _service = (MacroMicroDriverLocalService) invokableLocalService;
            } else {
                _service = new MacroMicroDriverLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(MacroMicroDriverLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated
     */
    public void setService(MacroMicroDriverLocalService service) {
    }
}
