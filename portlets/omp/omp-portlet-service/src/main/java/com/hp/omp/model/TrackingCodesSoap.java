package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class TrackingCodesSoap implements Serializable {
    private long _trackingCodeId;
    private String _trackingCodeName;
    private String _trackingCodeDescription;
    private String _projectId;
    private String _subProjectId;
    private String _budgetCategoryId;
    private String _budgetSubCategoryId;
    private String _macroDriverId;
    private String _microDriverId;
    private String _odnpNameId;
    private String _wbsCodeId;
    private boolean _display;

    public TrackingCodesSoap() {
    }

    public static TrackingCodesSoap toSoapModel(TrackingCodes model) {
        TrackingCodesSoap soapModel = new TrackingCodesSoap();

        soapModel.setTrackingCodeId(model.getTrackingCodeId());
        soapModel.setTrackingCodeName(model.getTrackingCodeName());
        soapModel.setTrackingCodeDescription(model.getTrackingCodeDescription());
        soapModel.setProjectId(model.getProjectId());
        soapModel.setSubProjectId(model.getSubProjectId());
        soapModel.setBudgetCategoryId(model.getBudgetCategoryId());
        soapModel.setBudgetSubCategoryId(model.getBudgetSubCategoryId());
        soapModel.setMacroDriverId(model.getMacroDriverId());
        soapModel.setMicroDriverId(model.getMicroDriverId());
        soapModel.setOdnpNameId(model.getOdnpNameId());
        soapModel.setWbsCodeId(model.getWbsCodeId());
        soapModel.setDisplay(model.getDisplay());

        return soapModel;
    }

    public static TrackingCodesSoap[] toSoapModels(TrackingCodes[] models) {
        TrackingCodesSoap[] soapModels = new TrackingCodesSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static TrackingCodesSoap[][] toSoapModels(TrackingCodes[][] models) {
        TrackingCodesSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new TrackingCodesSoap[models.length][models[0].length];
        } else {
            soapModels = new TrackingCodesSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static TrackingCodesSoap[] toSoapModels(List<TrackingCodes> models) {
        List<TrackingCodesSoap> soapModels = new ArrayList<TrackingCodesSoap>(models.size());

        for (TrackingCodes model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new TrackingCodesSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _trackingCodeId;
    }

    public void setPrimaryKey(long pk) {
        setTrackingCodeId(pk);
    }

    public long getTrackingCodeId() {
        return _trackingCodeId;
    }

    public void setTrackingCodeId(long trackingCodeId) {
        _trackingCodeId = trackingCodeId;
    }

    public String getTrackingCodeName() {
        return _trackingCodeName;
    }

    public void setTrackingCodeName(String trackingCodeName) {
        _trackingCodeName = trackingCodeName;
    }

    public String getTrackingCodeDescription() {
        return _trackingCodeDescription;
    }

    public void setTrackingCodeDescription(String trackingCodeDescription) {
        _trackingCodeDescription = trackingCodeDescription;
    }

    public String getProjectId() {
        return _projectId;
    }

    public void setProjectId(String projectId) {
        _projectId = projectId;
    }

    public String getSubProjectId() {
        return _subProjectId;
    }

    public void setSubProjectId(String subProjectId) {
        _subProjectId = subProjectId;
    }

    public String getBudgetCategoryId() {
        return _budgetCategoryId;
    }

    public void setBudgetCategoryId(String budgetCategoryId) {
        _budgetCategoryId = budgetCategoryId;
    }

    public String getBudgetSubCategoryId() {
        return _budgetSubCategoryId;
    }

    public void setBudgetSubCategoryId(String budgetSubCategoryId) {
        _budgetSubCategoryId = budgetSubCategoryId;
    }

    public String getMacroDriverId() {
        return _macroDriverId;
    }

    public void setMacroDriverId(String macroDriverId) {
        _macroDriverId = macroDriverId;
    }

    public String getMicroDriverId() {
        return _microDriverId;
    }

    public void setMicroDriverId(String microDriverId) {
        _microDriverId = microDriverId;
    }

    public String getOdnpNameId() {
        return _odnpNameId;
    }

    public void setOdnpNameId(String odnpNameId) {
        _odnpNameId = odnpNameId;
    }

    public String getWbsCodeId() {
        return _wbsCodeId;
    }

    public void setWbsCodeId(String wbsCodeId) {
        _wbsCodeId = wbsCodeId;
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;
    }
}
