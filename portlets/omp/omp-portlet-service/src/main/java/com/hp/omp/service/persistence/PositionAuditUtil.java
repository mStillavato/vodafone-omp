package com.hp.omp.service.persistence;

import com.hp.omp.model.PositionAudit;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the position audit service. This utility wraps {@link PositionAuditPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see PositionAuditPersistence
 * @see PositionAuditPersistenceImpl
 * @generated
 */
public class PositionAuditUtil {
    private static PositionAuditPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(PositionAudit positionAudit) {
        getPersistence().clearCache(positionAudit);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<PositionAudit> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<PositionAudit> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<PositionAudit> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static PositionAudit update(PositionAudit positionAudit,
        boolean merge) throws SystemException {
        return getPersistence().update(positionAudit, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static PositionAudit update(PositionAudit positionAudit,
        boolean merge, ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(positionAudit, merge, serviceContext);
    }

    /**
    * Caches the position audit in the entity cache if it is enabled.
    *
    * @param positionAudit the position audit
    */
    public static void cacheResult(com.hp.omp.model.PositionAudit positionAudit) {
        getPersistence().cacheResult(positionAudit);
    }

    /**
    * Caches the position audits in the entity cache if it is enabled.
    *
    * @param positionAudits the position audits
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.PositionAudit> positionAudits) {
        getPersistence().cacheResult(positionAudits);
    }

    /**
    * Creates a new position audit with the primary key. Does not add the position audit to the database.
    *
    * @param auditId the primary key for the new position audit
    * @return the new position audit
    */
    public static com.hp.omp.model.PositionAudit create(long auditId) {
        return getPersistence().create(auditId);
    }

    /**
    * Removes the position audit with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param auditId the primary key of the position audit
    * @return the position audit that was removed
    * @throws com.hp.omp.NoSuchPositionAuditException if a position audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.PositionAudit remove(long auditId)
        throws com.hp.omp.NoSuchPositionAuditException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(auditId);
    }

    public static com.hp.omp.model.PositionAudit updateImpl(
        com.hp.omp.model.PositionAudit positionAudit, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(positionAudit, merge);
    }

    /**
    * Returns the position audit with the primary key or throws a {@link com.hp.omp.NoSuchPositionAuditException} if it could not be found.
    *
    * @param auditId the primary key of the position audit
    * @return the position audit
    * @throws com.hp.omp.NoSuchPositionAuditException if a position audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.PositionAudit findByPrimaryKey(long auditId)
        throws com.hp.omp.NoSuchPositionAuditException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(auditId);
    }

    /**
    * Returns the position audit with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param auditId the primary key of the position audit
    * @return the position audit, or <code>null</code> if a position audit with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.PositionAudit fetchByPrimaryKey(long auditId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(auditId);
    }

    /**
    * Returns all the position audits.
    *
    * @return the position audits
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.PositionAudit> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the position audits.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of position audits
    * @param end the upper bound of the range of position audits (not inclusive)
    * @return the range of position audits
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.PositionAudit> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the position audits.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of position audits
    * @param end the upper bound of the range of position audits (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of position audits
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.PositionAudit> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the position audits from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of position audits.
    *
    * @return the number of position audits
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static PositionAuditPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (PositionAuditPersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    PositionAuditPersistence.class.getName());

            ReferenceRegistry.registerReference(PositionAuditUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(PositionAuditPersistence persistence) {
    }
}
