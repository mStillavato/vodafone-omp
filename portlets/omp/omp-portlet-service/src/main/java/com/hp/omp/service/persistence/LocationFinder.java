package com.hp.omp.service.persistence;

import java.util.List;

import com.hp.omp.model.custom.LocationDTO;

public interface LocationFinder {
    
    public List<LocationDTO> getLocationsList()
            throws com.liferay.portal.kernel.exception.SystemException;
    
    public long getLocationsCountList(String searchFilter)
            throws com.liferay.portal.kernel.exception.SystemException;
    
    public LocationDTO getLocationByTargaTecnica(String targaTecnica)
            throws com.liferay.portal.kernel.exception.SystemException;

	public LocationDTO getLocationByLocationEvo(String locationEvo)
			throws com.liferay.portal.kernel.exception.SystemException;
}
