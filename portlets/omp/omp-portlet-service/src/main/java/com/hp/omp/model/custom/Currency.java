package com.hp.omp.model.custom;

import java.util.HashMap;
import java.util.Map;

public enum Currency {

	
	USD(0,"Dollar"),
	EUR(1,"Euro"),
	ALL(2,"All");
	
	private int code;
	private String label;
	
	
	
	private Currency(int code, String label){
		this.code = code;
		this.label = label;
	}
	
	
	  private static Map<Integer, Currency> codeToStatusMapping;
		 
	    
	 
	    public static Currency getStatus(int i) {
	        if (codeToStatusMapping == null) {
	            initMapping();
	        }
	        return codeToStatusMapping.get(i);
	    }
	 
	    private static void initMapping() {
	        codeToStatusMapping = new HashMap<Integer, Currency>();
	        for (Currency s : values()) {
	            codeToStatusMapping.put(s.code, s);
	        }
	    }
	public int getCode(){
		return code;
	}
	
	public String getLabel(){
		return label;
	}
}
