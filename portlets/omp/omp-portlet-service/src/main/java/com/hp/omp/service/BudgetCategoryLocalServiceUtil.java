package com.hp.omp.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * The utility for the budget category local service. This utility wraps {@link com.hp.omp.service.impl.BudgetCategoryLocalServiceImpl} and is the primary access point for service operations in application layer code running on the local server.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see BudgetCategoryLocalService
 * @see com.hp.omp.service.base.BudgetCategoryLocalServiceBaseImpl
 * @see com.hp.omp.service.impl.BudgetCategoryLocalServiceImpl
 * @generated
 */
public class BudgetCategoryLocalServiceUtil {
    private static BudgetCategoryLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link com.hp.omp.service.impl.BudgetCategoryLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the budget category to the database. Also notifies the appropriate model listeners.
    *
    * @param budgetCategory the budget category
    * @return the budget category that was added
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.BudgetCategory addBudgetCategory(
        com.hp.omp.model.BudgetCategory budgetCategory)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addBudgetCategory(budgetCategory);
    }

    /**
    * Creates a new budget category with the primary key. Does not add the budget category to the database.
    *
    * @param budgetCategoryId the primary key for the new budget category
    * @return the new budget category
    */
    public static com.hp.omp.model.BudgetCategory createBudgetCategory(
        long budgetCategoryId) {
        return getService().createBudgetCategory(budgetCategoryId);
    }

    /**
    * Deletes the budget category with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param budgetCategoryId the primary key of the budget category
    * @return the budget category that was removed
    * @throws PortalException if a budget category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.BudgetCategory deleteBudgetCategory(
        long budgetCategoryId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteBudgetCategory(budgetCategoryId);
    }

    /**
    * Deletes the budget category from the database. Also notifies the appropriate model listeners.
    *
    * @param budgetCategory the budget category
    * @return the budget category that was removed
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.BudgetCategory deleteBudgetCategory(
        com.hp.omp.model.BudgetCategory budgetCategory)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteBudgetCategory(budgetCategory);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    public static com.hp.omp.model.BudgetCategory fetchBudgetCategory(
        long budgetCategoryId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchBudgetCategory(budgetCategoryId);
    }

    /**
    * Returns the budget category with the primary key.
    *
    * @param budgetCategoryId the primary key of the budget category
    * @return the budget category
    * @throws PortalException if a budget category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.BudgetCategory getBudgetCategory(
        long budgetCategoryId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getBudgetCategory(budgetCategoryId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the budget categories.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of budget categories
    * @param end the upper bound of the range of budget categories (not inclusive)
    * @return the range of budget categories
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.BudgetCategory> getBudgetCategories(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getBudgetCategories(start, end);
    }

    /**
    * Returns the number of budget categories.
    *
    * @return the number of budget categories
    * @throws SystemException if a system exception occurred
    */
    public static int getBudgetCategoriesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getBudgetCategoriesCount();
    }

    /**
    * Updates the budget category in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param budgetCategory the budget category
    * @return the budget category that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.BudgetCategory updateBudgetCategory(
        com.hp.omp.model.BudgetCategory budgetCategory)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateBudgetCategory(budgetCategory);
    }

    /**
    * Updates the budget category in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param budgetCategory the budget category
    * @param merge whether to merge the budget category with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the budget category that was updated
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.BudgetCategory updateBudgetCategory(
        com.hp.omp.model.BudgetCategory budgetCategory, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateBudgetCategory(budgetCategory, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static com.hp.omp.model.BudgetCategory getBudgetCategoryByName(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getBudgetCategoryByName(name);
    }

    public static java.util.List<com.hp.omp.model.BudgetCategory> getBudgetCategoryList(
    		java.lang.Integer pageNumber, java.lang.Integer pageSize, java.lang.String searchFilter)
            throws com.liferay.portal.kernel.exception.SystemException {
            return getService().getBudgetCategoryList(pageNumber, pageSize, searchFilter);
    }
    
    public static void clearService() {
        _service = null;
    }

    public static BudgetCategoryLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    BudgetCategoryLocalService.class.getName());

            if (invokableLocalService instanceof BudgetCategoryLocalService) {
                _service = (BudgetCategoryLocalService) invokableLocalService;
            } else {
                _service = new BudgetCategoryLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(BudgetCategoryLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated
     */
    public void setService(BudgetCategoryLocalService service) {
    }
}
