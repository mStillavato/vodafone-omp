package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class BudgetSubCategorySoap implements Serializable {
    private long _budgetSubCategoryId;
    private String _budgetSubCategoryName;
    private boolean _display;

    public BudgetSubCategorySoap() {
    }

    public static BudgetSubCategorySoap toSoapModel(BudgetSubCategory model) {
        BudgetSubCategorySoap soapModel = new BudgetSubCategorySoap();

        soapModel.setBudgetSubCategoryId(model.getBudgetSubCategoryId());
        soapModel.setBudgetSubCategoryName(model.getBudgetSubCategoryName());
        soapModel.setDisplay(model.getDisplay());

        return soapModel;
    }

    public static BudgetSubCategorySoap[] toSoapModels(
        BudgetSubCategory[] models) {
        BudgetSubCategorySoap[] soapModels = new BudgetSubCategorySoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static BudgetSubCategorySoap[][] toSoapModels(
        BudgetSubCategory[][] models) {
        BudgetSubCategorySoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new BudgetSubCategorySoap[models.length][models[0].length];
        } else {
            soapModels = new BudgetSubCategorySoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static BudgetSubCategorySoap[] toSoapModels(
        List<BudgetSubCategory> models) {
        List<BudgetSubCategorySoap> soapModels = new ArrayList<BudgetSubCategorySoap>(models.size());

        for (BudgetSubCategory model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new BudgetSubCategorySoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _budgetSubCategoryId;
    }

    public void setPrimaryKey(long pk) {
        setBudgetSubCategoryId(pk);
    }

    public long getBudgetSubCategoryId() {
        return _budgetSubCategoryId;
    }

    public void setBudgetSubCategoryId(long budgetSubCategoryId) {
        _budgetSubCategoryId = budgetSubCategoryId;
    }

    public String getBudgetSubCategoryName() {
        return _budgetSubCategoryName;
    }

    public void setBudgetSubCategoryName(String budgetSubCategoryName) {
        _budgetSubCategoryName = budgetSubCategoryName;
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;
    }
}
