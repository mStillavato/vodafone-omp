package com.hp.omp.service.persistence;

public interface ProjectFinder {
    
    public java.util.List<com.hp.omp.model.Project> getProjectsList(
    		int start, int end, String searchFilter)
            throws com.liferay.portal.kernel.exception.SystemException;
    
    public int getProjectsCountList(String searchFilter)
            throws com.liferay.portal.kernel.exception.SystemException;
}
