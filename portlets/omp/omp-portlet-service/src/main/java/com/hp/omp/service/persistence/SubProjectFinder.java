package com.hp.omp.service.persistence;

public interface SubProjectFinder {
    public java.util.List<com.hp.omp.model.SubProject> getSubProjectByProjectId(
        java.lang.Long projectId)
        throws com.liferay.portal.kernel.exception.SystemException;
    
    public java.util.List<com.hp.omp.model.SubProject> getSubProjectsList(
    		int start, int end, String searchFilter)
            throws com.liferay.portal.kernel.exception.SystemException;
}
