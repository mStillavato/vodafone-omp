/**
 * 
 */
package com.hp.omp.model.custom;

import java.io.Serializable;
import java.util.List;

import com.hp.omp.model.CostCenter;
import com.hp.omp.model.Project;
import com.hp.omp.model.SubProject;
import com.hp.omp.model.WbsCode;

/**
 * @author gewaly
 *
 */
public class SearchCatalogoDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7684541966359000646L;
	
	private String vendor  = null;
	private String matCatLvl4 = null;
	private String codMateriale = null;
	private String codFornitore = null;
	private String descMateriale = null;
	private double prezzo = 0;
	private String globalSearchKeyWord = null;
	private String miniSearchValue = null;
	private boolean miniSeach;
	private Integer pageNumber = 1;
	private Integer pageSize = 20;
	private String vendorNotView  = null;
	
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getMatCatLvl4() {
		return matCatLvl4;
	}
	public void setMatCatLvl4(String matCatLvl4) {
		this.matCatLvl4 = matCatLvl4;
	}
	public String getCodMateriale() {
		return codMateriale;
	}
	public void setCodMateriale(String codMateriale) {
		this.codMateriale = codMateriale;
	}
	public String getCodFornitore() {
		return codFornitore;
	}
	public void setCodFornitore(String codFornitore) {
		this.codFornitore = codFornitore;
	}
	public String getDescMateriale() {
		return descMateriale;
	}
	public void setDescMateriale(String descMateriale) {
		this.descMateriale = descMateriale;
	}
	public String getMiniSearchValue() {
		return miniSearchValue;
	}
	public void setMiniSearchValue(String miniSearchValue) {
		this.miniSearchValue = miniSearchValue;
	}
	public boolean isMiniSeach() {
		return miniSeach;
	}
	public void setMiniSeach(boolean miniSeach) {
		this.miniSeach = miniSeach;
	}
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public String getGlobalSearchKeyWord() {
		return globalSearchKeyWord;
	}
	public void setGlobalSearchKeyWord(String globalSearchKeyWord) {
		this.globalSearchKeyWord = globalSearchKeyWord;
	}
	public String getVendorNotView() {
		return vendorNotView;
	}
	public void setVendorNotView(String vendorNotView) {
		this.vendorNotView = vendorNotView;
	}
	public double getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}
	
}