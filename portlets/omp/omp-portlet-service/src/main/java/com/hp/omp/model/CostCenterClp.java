package com.hp.omp.model;

import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.CostCenterLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class CostCenterClp extends BaseModelImpl<CostCenter>
    implements CostCenter {
    private long _costCenterId;
    private String _costCenterName;
    private String _description;
    private boolean _display;
    private int _gruppoUsers;
    private BaseModel<?> _costCenterRemoteModel;

    public CostCenterClp() {
    }

    public Class<?> getModelClass() {
        return CostCenter.class;
    }

    public String getModelClassName() {
        return CostCenter.class.getName();
    }

    public long getPrimaryKey() {
        return _costCenterId;
    }

    public void setPrimaryKey(long primaryKey) {
        setCostCenterId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_costCenterId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("costCenterId", getCostCenterId());
        attributes.put("costCenterName", getCostCenterName());
        attributes.put("description", getDescription());
        attributes.put("display", getDisplay());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long costCenterId = (Long) attributes.get("costCenterId");

        if (costCenterId != null) {
            setCostCenterId(costCenterId);
        }

        String costCenterName = (String) attributes.get("costCenterName");

        if (costCenterName != null) {
            setCostCenterName(costCenterName);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
    }

    public long getCostCenterId() {
        return _costCenterId;
    }

    public void setCostCenterId(long costCenterId) {
        _costCenterId = costCenterId;

        if (_costCenterRemoteModel != null) {
            try {
                Class<?> clazz = _costCenterRemoteModel.getClass();

                Method method = clazz.getMethod("setCostCenterId", long.class);

                method.invoke(_costCenterRemoteModel, costCenterId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getCostCenterName() {
        return _costCenterName;
    }

    public void setCostCenterName(String costCenterName) {
        _costCenterName = costCenterName;

        if (_costCenterRemoteModel != null) {
            try {
                Class<?> clazz = _costCenterRemoteModel.getClass();

                Method method = clazz.getMethod("setCostCenterName",
                        String.class);

                method.invoke(_costCenterRemoteModel, costCenterName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;

        if (_costCenterRemoteModel != null) {
            try {
                Class<?> clazz = _costCenterRemoteModel.getClass();

                Method method = clazz.getMethod("setDescription", String.class);

                method.invoke(_costCenterRemoteModel, description);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;

        if (_costCenterRemoteModel != null) {
            try {
                Class<?> clazz = _costCenterRemoteModel.getClass();

                Method method = clazz.getMethod("setDisplay", boolean.class);

                method.invoke(_costCenterRemoteModel, display);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getCostCenterRemoteModel() {
        return _costCenterRemoteModel;
    }

    public void setCostCenterRemoteModel(BaseModel<?> costCenterRemoteModel) {
        _costCenterRemoteModel = costCenterRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _costCenterRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_costCenterRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            CostCenterLocalServiceUtil.addCostCenter(this);
        } else {
            CostCenterLocalServiceUtil.updateCostCenter(this);
        }
    }

    @Override
    public CostCenter toEscapedModel() {
        return (CostCenter) ProxyUtil.newProxyInstance(CostCenter.class.getClassLoader(),
            new Class[] { CostCenter.class }, new AutoEscapeBeanHandler(this));
    }

    public CostCenter toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        CostCenterClp clone = new CostCenterClp();

        clone.setCostCenterId(getCostCenterId());
        clone.setCostCenterName(getCostCenterName());
        clone.setDescription(getDescription());
        clone.setDisplay(getDisplay());

        return clone;
    }

    public int compareTo(CostCenter costCenter) {
        long primaryKey = costCenter.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof CostCenterClp)) {
            return false;
        }

        CostCenterClp costCenter = (CostCenterClp) obj;

        long primaryKey = costCenter.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(9);

        sb.append("{costCenterId=");
        sb.append(getCostCenterId());
        sb.append(", costCenterName=");
        sb.append(getCostCenterName());
        sb.append(", description=");
        sb.append(getDescription());
        sb.append(", display=");
        sb.append(getDisplay());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(16);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.CostCenter");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>costCenterId</column-name><column-value><![CDATA[");
        sb.append(getCostCenterId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>costCenterName</column-name><column-value><![CDATA[");
        sb.append(getCostCenterName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>description</column-name><column-value><![CDATA[");
        sb.append(getDescription());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>display</column-name><column-value><![CDATA[");
        sb.append(getDisplay());
        sb.append("]]></column-value></column>");
        sb.append(
                "<column><column-name>gruppoUsers</column-name><column-value><![CDATA[");
        sb.append(getGruppoUsers());
        sb.append("]]></column-value></column>");
        sb.append("</model>");

        return sb.toString();
    }
    
	public int getGruppoUsers() {
		return _gruppoUsers;
	}

	public void setGruppoUsers(int gruppoUsers) {
        _gruppoUsers = gruppoUsers;

        if (_costCenterRemoteModel != null) {
            try {
                Class<?> clazz = _costCenterRemoteModel.getClass();

                Method method = clazz.getMethod("setGruppoUsers", int.class);

                method.invoke(_costCenterRemoteModel, gruppoUsers);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
	}
}
