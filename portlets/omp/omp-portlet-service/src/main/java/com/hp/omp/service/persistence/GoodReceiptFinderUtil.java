package com.hp.omp.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class GoodReceiptFinderUtil {
    private static GoodReceiptFinder _finder;

    public static java.lang.Long getOpenedGoodReceiptByCostCenterCount(
        long costCenterId) throws java.lang.Exception {
        return getFinder().getOpenedGoodReceiptByCostCenterCount(costCenterId);
    }

    public static java.lang.Double getSumCompeleteGRForPO(
        long purchaseOrderId, boolean isClosed) {
        return getFinder().getSumCompeleteGRForPO(purchaseOrderId, isClosed);
    }

    public static java.lang.Double getSumCompeleteGRForPR(
        long purchaseRequestId) {
        return getFinder().getSumCompeleteGRForPR(purchaseRequestId);
    }

    public static java.lang.Double getSumValuePositionForPO(
        long purchaseOrderId) {
        return getFinder().getSumValuePositionForPO(purchaseOrderId);
    }

    public static java.lang.Double getSumValuePositionForPR(
        long purchaseRequestId) {
        return getFinder().getSumValuePositionForPR(purchaseRequestId);
    }

    public static java.lang.Long getOpenedGoodReceiptCount()
        throws java.lang.Exception {
        return getFinder().getOpenedGoodReceiptCount();
    }

    public static java.util.List<com.hp.omp.model.custom.ListDTO> getOpenedGoodReceiptByCostCenter(
        long costCenterId, int start, int end) throws java.lang.Exception {
        return getFinder()
                   .getOpenedGoodReceiptByCostCenter(costCenterId, start, end);
    }

    public static java.util.List<com.hp.omp.model.custom.ListDTO> getOpenedGoodReceipt(
        int start, int end) throws java.lang.Exception {
        return getFinder().getOpenedGoodReceipt(start, end);
    }

    public static GoodReceiptFinder getFinder() {
        if (_finder == null) {
            _finder = (GoodReceiptFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    GoodReceiptFinder.class.getName());

            ReferenceRegistry.registerReference(GoodReceiptFinderUtil.class,
                "_finder");
        }

        return _finder;
    }

    public void setFinder(GoodReceiptFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(GoodReceiptFinderUtil.class,
            "_finder");
    }
}
