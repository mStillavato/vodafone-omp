package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the BudgetCategory service. Represents a row in the &quot;BUDGET_CATEGORY&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see BudgetCategoryModel
 * @see com.hp.omp.model.impl.BudgetCategoryImpl
 * @see com.hp.omp.model.impl.BudgetCategoryModelImpl
 * @generated
 */
public interface BudgetCategory extends BudgetCategoryModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.BudgetCategoryImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
