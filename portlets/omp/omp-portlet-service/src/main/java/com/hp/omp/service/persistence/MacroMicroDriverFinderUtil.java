package com.hp.omp.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class MacroMicroDriverFinderUtil {
    private static MacroMicroDriverFinder _finder;

    public static java.util.List<com.hp.omp.model.MacroDriver> getMacroDriversByMicroDriverId(
        java.lang.Long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getFinder().getMacroDriversByMicroDriverId(microDriverId);
    }

    public static MacroMicroDriverFinder getFinder() {
        if (_finder == null) {
            _finder = (MacroMicroDriverFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    MacroMicroDriverFinder.class.getName());

            ReferenceRegistry.registerReference(MacroMicroDriverFinderUtil.class,
                "_finder");
        }

        return _finder;
    }

    public void setFinder(MacroMicroDriverFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(MacroMicroDriverFinderUtil.class,
            "_finder");
    }
}
