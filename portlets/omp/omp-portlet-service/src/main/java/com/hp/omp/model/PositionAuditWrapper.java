package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link PositionAudit}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       PositionAudit
 * @generated
 */
public class PositionAuditWrapper implements PositionAudit,
    ModelWrapper<PositionAudit> {
    private PositionAudit _positionAudit;

    public PositionAuditWrapper(PositionAudit positionAudit) {
        _positionAudit = positionAudit;
    }

    public Class<?> getModelClass() {
        return PositionAudit.class;
    }

    public String getModelClassName() {
        return PositionAudit.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("auditId", getAuditId());
        attributes.put("positionId", getPositionId());
        attributes.put("status", getStatus());
        attributes.put("userId", getUserId());
        attributes.put("changeDate", getChangeDate());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long auditId = (Long) attributes.get("auditId");

        if (auditId != null) {
            setAuditId(auditId);
        }

        Long positionId = (Long) attributes.get("positionId");

        if (positionId != null) {
            setPositionId(positionId);
        }

        Integer status = (Integer) attributes.get("status");

        if (status != null) {
            setStatus(status);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        Date changeDate = (Date) attributes.get("changeDate");

        if (changeDate != null) {
            setChangeDate(changeDate);
        }
    }

    /**
    * Returns the primary key of this position audit.
    *
    * @return the primary key of this position audit
    */
    public long getPrimaryKey() {
        return _positionAudit.getPrimaryKey();
    }

    /**
    * Sets the primary key of this position audit.
    *
    * @param primaryKey the primary key of this position audit
    */
    public void setPrimaryKey(long primaryKey) {
        _positionAudit.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the audit ID of this position audit.
    *
    * @return the audit ID of this position audit
    */
    public long getAuditId() {
        return _positionAudit.getAuditId();
    }

    /**
    * Sets the audit ID of this position audit.
    *
    * @param auditId the audit ID of this position audit
    */
    public void setAuditId(long auditId) {
        _positionAudit.setAuditId(auditId);
    }

    /**
    * Returns the position ID of this position audit.
    *
    * @return the position ID of this position audit
    */
    public java.lang.Long getPositionId() {
        return _positionAudit.getPositionId();
    }

    /**
    * Sets the position ID of this position audit.
    *
    * @param positionId the position ID of this position audit
    */
    public void setPositionId(java.lang.Long positionId) {
        _positionAudit.setPositionId(positionId);
    }

    /**
    * Returns the status of this position audit.
    *
    * @return the status of this position audit
    */
    public int getStatus() {
        return _positionAudit.getStatus();
    }

    /**
    * Sets the status of this position audit.
    *
    * @param status the status of this position audit
    */
    public void setStatus(int status) {
        _positionAudit.setStatus(status);
    }

    /**
    * Returns the user ID of this position audit.
    *
    * @return the user ID of this position audit
    */
    public java.lang.Long getUserId() {
        return _positionAudit.getUserId();
    }

    /**
    * Sets the user ID of this position audit.
    *
    * @param userId the user ID of this position audit
    */
    public void setUserId(java.lang.Long userId) {
        _positionAudit.setUserId(userId);
    }

    /**
    * Returns the change date of this position audit.
    *
    * @return the change date of this position audit
    */
    public java.util.Date getChangeDate() {
        return _positionAudit.getChangeDate();
    }

    /**
    * Sets the change date of this position audit.
    *
    * @param changeDate the change date of this position audit
    */
    public void setChangeDate(java.util.Date changeDate) {
        _positionAudit.setChangeDate(changeDate);
    }

    public boolean isNew() {
        return _positionAudit.isNew();
    }

    public void setNew(boolean n) {
        _positionAudit.setNew(n);
    }

    public boolean isCachedModel() {
        return _positionAudit.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _positionAudit.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _positionAudit.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _positionAudit.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _positionAudit.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _positionAudit.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _positionAudit.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new PositionAuditWrapper((PositionAudit) _positionAudit.clone());
    }

    public int compareTo(PositionAudit positionAudit) {
        return _positionAudit.compareTo(positionAudit);
    }

    @Override
    public int hashCode() {
        return _positionAudit.hashCode();
    }

    public com.liferay.portal.model.CacheModel<PositionAudit> toCacheModel() {
        return _positionAudit.toCacheModel();
    }

    public PositionAudit toEscapedModel() {
        return new PositionAuditWrapper(_positionAudit.toEscapedModel());
    }

    public PositionAudit toUnescapedModel() {
        return new PositionAuditWrapper(_positionAudit.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _positionAudit.toString();
    }

    public java.lang.String toXmlString() {
        return _positionAudit.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _positionAudit.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PositionAuditWrapper)) {
            return false;
        }

        PositionAuditWrapper positionAuditWrapper = (PositionAuditWrapper) obj;

        if (Validator.equals(_positionAudit, positionAuditWrapper._positionAudit)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public PositionAudit getWrappedPositionAudit() {
        return _positionAudit;
    }

    public PositionAudit getWrappedModel() {
        return _positionAudit;
    }

    public void resetOriginalValues() {
        _positionAudit.resetOriginalValues();
    }
}
