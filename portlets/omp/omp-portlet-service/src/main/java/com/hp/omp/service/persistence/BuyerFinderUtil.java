package com.hp.omp.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class BuyerFinderUtil {
    private static BuyerFinder _finder;

    public static java.util.List<com.hp.omp.model.Buyer> getBuyersList(
        int start, int end, String searchFilter, int gruppoUsers)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getFinder().getBuyersList(start, end, searchFilter, gruppoUsers);
    }
    
	public static int getBuyersCountList(String searchFilter, int gruppoUsers)
			throws com.liferay.portal.kernel.exception.SystemException {
		return getFinder().getBuyersCountList(searchFilter, gruppoUsers);
}
    
    public static BuyerFinder getFinder() {
        if (_finder == null) {
            _finder = (BuyerFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
            		BuyerFinder.class.getName());

            ReferenceRegistry.registerReference(BuyerFinderUtil.class,
                "_finder");
        }

        return _finder;
    }

    public void setFinder(BuyerFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(BuyerFinderUtil.class,
            "_finder");
    }
}
