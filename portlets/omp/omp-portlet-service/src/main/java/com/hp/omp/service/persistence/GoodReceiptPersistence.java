package com.hp.omp.service.persistence;

import com.hp.omp.model.GoodReceipt;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the good receipt service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see GoodReceiptPersistenceImpl
 * @see GoodReceiptUtil
 * @generated
 */
public interface GoodReceiptPersistence extends BasePersistence<GoodReceipt> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link GoodReceiptUtil} to access the good receipt persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the good receipt in the entity cache if it is enabled.
    *
    * @param goodReceipt the good receipt
    */
    public void cacheResult(com.hp.omp.model.GoodReceipt goodReceipt);

    /**
    * Caches the good receipts in the entity cache if it is enabled.
    *
    * @param goodReceipts the good receipts
    */
    public void cacheResult(
        java.util.List<com.hp.omp.model.GoodReceipt> goodReceipts);

    /**
    * Creates a new good receipt with the primary key. Does not add the good receipt to the database.
    *
    * @param goodReceiptId the primary key for the new good receipt
    * @return the new good receipt
    */
    public com.hp.omp.model.GoodReceipt create(long goodReceiptId);

    /**
    * Removes the good receipt with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param goodReceiptId the primary key of the good receipt
    * @return the good receipt that was removed
    * @throws com.hp.omp.NoSuchGoodReceiptException if a good receipt with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.GoodReceipt remove(long goodReceiptId)
        throws com.hp.omp.NoSuchGoodReceiptException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.GoodReceipt updateImpl(
        com.hp.omp.model.GoodReceipt goodReceipt, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the good receipt with the primary key or throws a {@link com.hp.omp.NoSuchGoodReceiptException} if it could not be found.
    *
    * @param goodReceiptId the primary key of the good receipt
    * @return the good receipt
    * @throws com.hp.omp.NoSuchGoodReceiptException if a good receipt with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.GoodReceipt findByPrimaryKey(long goodReceiptId)
        throws com.hp.omp.NoSuchGoodReceiptException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the good receipt with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param goodReceiptId the primary key of the good receipt
    * @return the good receipt, or <code>null</code> if a good receipt with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.GoodReceipt fetchByPrimaryKey(long goodReceiptId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the good receipts where positionId = &#63;.
    *
    * @param positionId the position ID
    * @return the matching good receipts
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.GoodReceipt> findByPositionGoodReceipts(
        java.lang.String positionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the good receipts where positionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param positionId the position ID
    * @param start the lower bound of the range of good receipts
    * @param end the upper bound of the range of good receipts (not inclusive)
    * @return the range of matching good receipts
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.GoodReceipt> findByPositionGoodReceipts(
        java.lang.String positionId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the good receipts where positionId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param positionId the position ID
    * @param start the lower bound of the range of good receipts
    * @param end the upper bound of the range of good receipts (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching good receipts
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.GoodReceipt> findByPositionGoodReceipts(
        java.lang.String positionId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first good receipt in the ordered set where positionId = &#63;.
    *
    * @param positionId the position ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching good receipt
    * @throws com.hp.omp.NoSuchGoodReceiptException if a matching good receipt could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.GoodReceipt findByPositionGoodReceipts_First(
        java.lang.String positionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchGoodReceiptException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first good receipt in the ordered set where positionId = &#63;.
    *
    * @param positionId the position ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching good receipt, or <code>null</code> if a matching good receipt could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.GoodReceipt fetchByPositionGoodReceipts_First(
        java.lang.String positionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last good receipt in the ordered set where positionId = &#63;.
    *
    * @param positionId the position ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching good receipt
    * @throws com.hp.omp.NoSuchGoodReceiptException if a matching good receipt could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.GoodReceipt findByPositionGoodReceipts_Last(
        java.lang.String positionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchGoodReceiptException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last good receipt in the ordered set where positionId = &#63;.
    *
    * @param positionId the position ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching good receipt, or <code>null</code> if a matching good receipt could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.GoodReceipt fetchByPositionGoodReceipts_Last(
        java.lang.String positionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the good receipts before and after the current good receipt in the ordered set where positionId = &#63;.
    *
    * @param goodReceiptId the primary key of the current good receipt
    * @param positionId the position ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next good receipt
    * @throws com.hp.omp.NoSuchGoodReceiptException if a good receipt with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.GoodReceipt[] findByPositionGoodReceipts_PrevAndNext(
        long goodReceiptId, java.lang.String positionId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchGoodReceiptException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the good receipts.
    *
    * @return the good receipts
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.GoodReceipt> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the good receipts.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of good receipts
    * @param end the upper bound of the range of good receipts (not inclusive)
    * @return the range of good receipts
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.GoodReceipt> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the good receipts.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of good receipts
    * @param end the upper bound of the range of good receipts (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of good receipts
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.GoodReceipt> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the good receipts where positionId = &#63; from the database.
    *
    * @param positionId the position ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByPositionGoodReceipts(java.lang.String positionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the good receipts from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of good receipts where positionId = &#63;.
    *
    * @param positionId the position ID
    * @return the number of matching good receipts
    * @throws SystemException if a system exception occurred
    */
    public int countByPositionGoodReceipts(java.lang.String positionId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of good receipts.
    *
    * @return the number of good receipts
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
