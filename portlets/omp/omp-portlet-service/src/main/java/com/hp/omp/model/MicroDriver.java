package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the MicroDriver service. Represents a row in the &quot;MICRO_DRIVER&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see MicroDriverModel
 * @see com.hp.omp.model.impl.MicroDriverImpl
 * @see com.hp.omp.model.impl.MicroDriverModelImpl
 * @generated
 */
public interface MicroDriver extends MicroDriverModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.MicroDriverImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
