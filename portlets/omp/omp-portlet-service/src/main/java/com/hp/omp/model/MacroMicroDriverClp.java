package com.hp.omp.model;

import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.MacroMicroDriverLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class MacroMicroDriverClp extends BaseModelImpl<MacroMicroDriver>
    implements MacroMicroDriver {
    private long _macroMicroDriverId;
    private long _macroDriverId;
    private long _microDriverId;
    private BaseModel<?> _macroMicroDriverRemoteModel;

    public MacroMicroDriverClp() {
    }

    public Class<?> getModelClass() {
        return MacroMicroDriver.class;
    }

    public String getModelClassName() {
        return MacroMicroDriver.class.getName();
    }

    public long getPrimaryKey() {
        return _macroMicroDriverId;
    }

    public void setPrimaryKey(long primaryKey) {
        setMacroMicroDriverId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_macroMicroDriverId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("macroMicroDriverId", getMacroMicroDriverId());
        attributes.put("macroDriverId", getMacroDriverId());
        attributes.put("microDriverId", getMicroDriverId());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long macroMicroDriverId = (Long) attributes.get("macroMicroDriverId");

        if (macroMicroDriverId != null) {
            setMacroMicroDriverId(macroMicroDriverId);
        }

        Long macroDriverId = (Long) attributes.get("macroDriverId");

        if (macroDriverId != null) {
            setMacroDriverId(macroDriverId);
        }

        Long microDriverId = (Long) attributes.get("microDriverId");

        if (microDriverId != null) {
            setMicroDriverId(microDriverId);
        }
    }

    public long getMacroMicroDriverId() {
        return _macroMicroDriverId;
    }

    public void setMacroMicroDriverId(long macroMicroDriverId) {
        _macroMicroDriverId = macroMicroDriverId;

        if (_macroMicroDriverRemoteModel != null) {
            try {
                Class<?> clazz = _macroMicroDriverRemoteModel.getClass();

                Method method = clazz.getMethod("setMacroMicroDriverId",
                        long.class);

                method.invoke(_macroMicroDriverRemoteModel, macroMicroDriverId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public long getMacroDriverId() {
        return _macroDriverId;
    }

    public void setMacroDriverId(long macroDriverId) {
        _macroDriverId = macroDriverId;

        if (_macroMicroDriverRemoteModel != null) {
            try {
                Class<?> clazz = _macroMicroDriverRemoteModel.getClass();

                Method method = clazz.getMethod("setMacroDriverId", long.class);

                method.invoke(_macroMicroDriverRemoteModel, macroDriverId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public long getMicroDriverId() {
        return _microDriverId;
    }

    public void setMicroDriverId(long microDriverId) {
        _microDriverId = microDriverId;

        if (_macroMicroDriverRemoteModel != null) {
            try {
                Class<?> clazz = _macroMicroDriverRemoteModel.getClass();

                Method method = clazz.getMethod("setMicroDriverId", long.class);

                method.invoke(_macroMicroDriverRemoteModel, microDriverId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getMacroMicroDriverRemoteModel() {
        return _macroMicroDriverRemoteModel;
    }

    public void setMacroMicroDriverRemoteModel(
        BaseModel<?> macroMicroDriverRemoteModel) {
        _macroMicroDriverRemoteModel = macroMicroDriverRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _macroMicroDriverRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_macroMicroDriverRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            MacroMicroDriverLocalServiceUtil.addMacroMicroDriver(this);
        } else {
            MacroMicroDriverLocalServiceUtil.updateMacroMicroDriver(this);
        }
    }

    @Override
    public MacroMicroDriver toEscapedModel() {
        return (MacroMicroDriver) ProxyUtil.newProxyInstance(MacroMicroDriver.class.getClassLoader(),
            new Class[] { MacroMicroDriver.class },
            new AutoEscapeBeanHandler(this));
    }

    public MacroMicroDriver toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        MacroMicroDriverClp clone = new MacroMicroDriverClp();

        clone.setMacroMicroDriverId(getMacroMicroDriverId());
        clone.setMacroDriverId(getMacroDriverId());
        clone.setMicroDriverId(getMicroDriverId());

        return clone;
    }

    public int compareTo(MacroMicroDriver macroMicroDriver) {
        long primaryKey = macroMicroDriver.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof MacroMicroDriverClp)) {
            return false;
        }

        MacroMicroDriverClp macroMicroDriver = (MacroMicroDriverClp) obj;

        long primaryKey = macroMicroDriver.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{macroMicroDriverId=");
        sb.append(getMacroMicroDriverId());
        sb.append(", macroDriverId=");
        sb.append(getMacroDriverId());
        sb.append(", microDriverId=");
        sb.append(getMicroDriverId());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(13);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.MacroMicroDriver");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>macroMicroDriverId</column-name><column-value><![CDATA[");
        sb.append(getMacroMicroDriverId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>macroDriverId</column-name><column-value><![CDATA[");
        sb.append(getMacroDriverId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>microDriverId</column-name><column-value><![CDATA[");
        sb.append(getMicroDriverId());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
