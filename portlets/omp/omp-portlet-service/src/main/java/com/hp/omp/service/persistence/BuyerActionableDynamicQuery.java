package com.hp.omp.service.persistence;

import com.hp.omp.model.Buyer;
import com.hp.omp.service.BuyerLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class BuyerActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public BuyerActionableDynamicQuery() throws SystemException {
        setBaseLocalService(BuyerLocalServiceUtil.getService());
        setClass(Buyer.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("buyerId");
    }
}
