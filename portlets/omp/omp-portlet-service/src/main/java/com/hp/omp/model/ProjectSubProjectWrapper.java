package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link ProjectSubProject}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       ProjectSubProject
 * @generated
 */
public class ProjectSubProjectWrapper implements ProjectSubProject,
    ModelWrapper<ProjectSubProject> {
    private ProjectSubProject _projectSubProject;

    public ProjectSubProjectWrapper(ProjectSubProject projectSubProject) {
        _projectSubProject = projectSubProject;
    }

    public Class<?> getModelClass() {
        return ProjectSubProject.class;
    }

    public String getModelClassName() {
        return ProjectSubProject.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("projectSubProjectId", getProjectSubProjectId());
        attributes.put("projectId", getProjectId());
        attributes.put("subProjectId", getSubProjectId());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long projectSubProjectId = (Long) attributes.get("projectSubProjectId");

        if (projectSubProjectId != null) {
            setProjectSubProjectId(projectSubProjectId);
        }

        Long projectId = (Long) attributes.get("projectId");

        if (projectId != null) {
            setProjectId(projectId);
        }

        Long subProjectId = (Long) attributes.get("subProjectId");

        if (subProjectId != null) {
            setSubProjectId(subProjectId);
        }
    }

    /**
    * Returns the primary key of this project sub project.
    *
    * @return the primary key of this project sub project
    */
    public long getPrimaryKey() {
        return _projectSubProject.getPrimaryKey();
    }

    /**
    * Sets the primary key of this project sub project.
    *
    * @param primaryKey the primary key of this project sub project
    */
    public void setPrimaryKey(long primaryKey) {
        _projectSubProject.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the project sub project ID of this project sub project.
    *
    * @return the project sub project ID of this project sub project
    */
    public long getProjectSubProjectId() {
        return _projectSubProject.getProjectSubProjectId();
    }

    /**
    * Sets the project sub project ID of this project sub project.
    *
    * @param projectSubProjectId the project sub project ID of this project sub project
    */
    public void setProjectSubProjectId(long projectSubProjectId) {
        _projectSubProject.setProjectSubProjectId(projectSubProjectId);
    }

    /**
    * Returns the project ID of this project sub project.
    *
    * @return the project ID of this project sub project
    */
    public long getProjectId() {
        return _projectSubProject.getProjectId();
    }

    /**
    * Sets the project ID of this project sub project.
    *
    * @param projectId the project ID of this project sub project
    */
    public void setProjectId(long projectId) {
        _projectSubProject.setProjectId(projectId);
    }

    /**
    * Returns the sub project ID of this project sub project.
    *
    * @return the sub project ID of this project sub project
    */
    public long getSubProjectId() {
        return _projectSubProject.getSubProjectId();
    }

    /**
    * Sets the sub project ID of this project sub project.
    *
    * @param subProjectId the sub project ID of this project sub project
    */
    public void setSubProjectId(long subProjectId) {
        _projectSubProject.setSubProjectId(subProjectId);
    }

    public boolean isNew() {
        return _projectSubProject.isNew();
    }

    public void setNew(boolean n) {
        _projectSubProject.setNew(n);
    }

    public boolean isCachedModel() {
        return _projectSubProject.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _projectSubProject.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _projectSubProject.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _projectSubProject.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _projectSubProject.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _projectSubProject.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _projectSubProject.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new ProjectSubProjectWrapper((ProjectSubProject) _projectSubProject.clone());
    }

    public int compareTo(ProjectSubProject projectSubProject) {
        return _projectSubProject.compareTo(projectSubProject);
    }

    @Override
    public int hashCode() {
        return _projectSubProject.hashCode();
    }

    public com.liferay.portal.model.CacheModel<ProjectSubProject> toCacheModel() {
        return _projectSubProject.toCacheModel();
    }

    public ProjectSubProject toEscapedModel() {
        return new ProjectSubProjectWrapper(_projectSubProject.toEscapedModel());
    }

    public ProjectSubProject toUnescapedModel() {
        return new ProjectSubProjectWrapper(_projectSubProject.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _projectSubProject.toString();
    }

    public java.lang.String toXmlString() {
        return _projectSubProject.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _projectSubProject.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ProjectSubProjectWrapper)) {
            return false;
        }

        ProjectSubProjectWrapper projectSubProjectWrapper = (ProjectSubProjectWrapper) obj;

        if (Validator.equals(_projectSubProject,
                    projectSubProjectWrapper._projectSubProject)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public ProjectSubProject getWrappedProjectSubProject() {
        return _projectSubProject;
    }

    public ProjectSubProject getWrappedModel() {
        return _projectSubProject;
    }

    public void resetOriginalValues() {
        _projectSubProject.resetOriginalValues();
    }
}
