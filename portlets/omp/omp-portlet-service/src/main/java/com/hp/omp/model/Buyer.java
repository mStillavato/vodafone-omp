package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Buyer service. Represents a row in the &quot;BUYER&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see BuyerModel
 * @see com.hp.omp.model.impl.BuyerImpl
 * @see com.hp.omp.model.impl.BuyerModelImpl
 * @generated
 */
public interface Buyer extends BuyerModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.BuyerImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
