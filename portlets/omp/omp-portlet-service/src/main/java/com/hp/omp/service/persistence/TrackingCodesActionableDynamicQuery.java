package com.hp.omp.service.persistence;

import com.hp.omp.model.TrackingCodes;
import com.hp.omp.service.TrackingCodesLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author HP Egypt team
 * @generated
 */
public abstract class TrackingCodesActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public TrackingCodesActionableDynamicQuery() throws SystemException {
        setBaseLocalService(TrackingCodesLocalServiceUtil.getService());
        setClass(TrackingCodes.class);

        setClassLoader(com.hp.omp.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("trackingCodeId");
    }
}
