package com.hp.omp.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class PurchaseOrderFinderUtil {
    private static PurchaseOrderFinder _finder;

    public static long getPurchaseOrdersGRRequestedCount(
        com.hp.omp.model.custom.SearchDTO searchDto) {
        return getFinder().getPurchaseOrdersGRRequestedCount(searchDto);
    }

    public static long getPurchaseOrdersGRClosedCount(
        com.hp.omp.model.custom.SearchDTO searchDto) {
        return getFinder().getPurchaseOrdersGRClosedCount(searchDto);
    }

    public static java.util.List<com.hp.omp.model.custom.PoListDTO> getPurchaseOrdersGRRequested(
        com.hp.omp.model.custom.SearchDTO searchDto) {
        return getFinder().getPurchaseOrdersGRRequested(searchDto);
    }

    public static java.util.List<com.hp.omp.model.custom.PoListDTO> getPurchaseOrdersGRClosed(
        com.hp.omp.model.custom.SearchDTO searchDto) {
        return getFinder().getPurchaseOrdersGRClosed(searchDto);
    }

    public static java.util.List<java.lang.Long> getPONumbers()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getFinder().getPONumbers();
    }

    public static java.util.List<java.lang.String> getAllPOProjects()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getFinder().getAllPOProjects();
    }

    public static java.util.List<java.lang.String> getSubProjects(
        java.lang.String projectName)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getFinder().getSubProjects(projectName);
    }

    public static java.util.List<com.hp.omp.model.custom.PoListDTO> getAdvancedSearchResults(
        com.hp.omp.model.custom.SearchDTO searchDTO, int start, int end,
        java.lang.String orderby, java.lang.String order)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getFinder().getAdvancedSearchResults(searchDTO, start, end, orderby, order);
    }

    public static java.lang.Long getAdvancedSearchResultsCount(
        com.hp.omp.model.custom.SearchDTO searchDTO)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getFinder().getAdvancedSearchResultsCount(searchDTO);
    }

    public static java.util.List<com.hp.omp.model.custom.PoListDTO> getAdvancedSearchGrRequestedResults(
        com.hp.omp.model.custom.SearchDTO searchDTO, int start, int end,
        java.lang.String orderby, java.lang.String order)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getFinder()
                   .getAdvancedSearchGrRequestedResults(searchDTO, start, end,
            orderby, order);
    }

    public static java.lang.Long getAdvancedSearchGrRequestedResultsCount(
        com.hp.omp.model.custom.SearchDTO searchDTO)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getFinder().getAdvancedSearchGrRequestedResultsCount(searchDTO);
    }

    public static PurchaseOrderFinder getFinder() {
        if (_finder == null) {
            _finder = (PurchaseOrderFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    PurchaseOrderFinder.class.getName());

            ReferenceRegistry.registerReference(PurchaseOrderFinderUtil.class,
                "_finder");
        }

        return _finder;
    }

    public void setFinder(PurchaseOrderFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(PurchaseOrderFinderUtil.class,
            "_finder");
    }
}
