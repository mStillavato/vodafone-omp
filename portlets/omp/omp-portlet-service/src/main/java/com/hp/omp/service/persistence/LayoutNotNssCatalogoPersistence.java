/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service.persistence;

import com.hp.omp.model.LayoutNotNssCatalogo;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the layoutNotNssCatalogo service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see LayoutNotNssCatalogoPersistenceImpl
 * @see LayoutNotNssCatalogoUtil
 * @generated
 */
public interface LayoutNotNssCatalogoPersistence extends BasePersistence<LayoutNotNssCatalogo> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LayoutNotNssCatalogoUtil} to access the layoutNotNssCatalogo persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the layoutNotNssCatalogo in the entity cache if it is enabled.
	*
	* @param layoutNotNssCatalogo the layoutNotNssCatalogo
	*/
	public void cacheResult(com.hp.omp.model.LayoutNotNssCatalogo layoutNotNssCatalogo);

	/**
	* Caches the catalogos in the entity cache if it is enabled.
	*
	* @param catalogos the catalogos
	*/
	public void cacheResult(java.util.List<com.hp.omp.model.LayoutNotNssCatalogo> layoutNotNssCatalogos);

	/**
	* Creates a new layoutNotNssCatalogo with the primary key. Does not add the layoutNotNssCatalogo to the database.
	*
	* @param layoutNotNssCatalogoId the primary key for the new layoutNotNssCatalogo
	* @return the new layoutNotNssCatalogo
	*/
	public com.hp.omp.model.LayoutNotNssCatalogo create(long layoutNotNssCatalogoId);

	/**
	* Removes the layoutNotNssCatalogo with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param layoutNotNssCatalogoId the primary key of the layoutNotNssCatalogo
	* @return the layoutNotNssCatalogo that was removed
	* @throws com.hp.omp.NoSuchLayoutNotNssCatalogoException if a layoutNotNssCatalogo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.hp.omp.model.LayoutNotNssCatalogo remove(long layoutNotNssCatalogoId)
		throws com.hp.omp.NoSuchLayoutNotNssCatalogoException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.hp.omp.model.LayoutNotNssCatalogo updateImpl(
		com.hp.omp.model.LayoutNotNssCatalogo layoutNotNssCatalogo, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the layoutNotNssCatalogo with the primary key or throws a {@link com.hp.omp.NoSuchLayoutNotNssCatalogoException} if it could not be found.
	*
	* @param layoutNotNssCatalogoId the primary key of the layoutNotNssCatalogo
	* @return the layoutNotNssCatalogo
	* @throws com.hp.omp.NoSuchLayoutNotNssCatalogoException if a layoutNotNssCatalogo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.hp.omp.model.LayoutNotNssCatalogo findByPrimaryKey(long layoutNotNssCatalogoId)
		throws com.hp.omp.NoSuchLayoutNotNssCatalogoException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the layoutNotNssCatalogo with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param layoutNotNssCatalogoId the primary key of the layoutNotNssCatalogo
	* @return the layoutNotNssCatalogo, or <code>null</code> if a layoutNotNssCatalogo with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.hp.omp.model.LayoutNotNssCatalogo fetchByPrimaryKey(long layoutNotNssCatalogoId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the catalogos.
	*
	* @return the catalogos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.hp.omp.model.LayoutNotNssCatalogo> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the catalogos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of catalogos
	* @param end the upper bound of the range of catalogos (not inclusive)
	* @return the range of catalogos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.hp.omp.model.LayoutNotNssCatalogo> findAll(int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the catalogos.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of catalogos
	* @param end the upper bound of the range of catalogos (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of catalogos
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.hp.omp.model.LayoutNotNssCatalogo> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the catalogos from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of catalogos.
	*
	* @return the number of catalogos
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}