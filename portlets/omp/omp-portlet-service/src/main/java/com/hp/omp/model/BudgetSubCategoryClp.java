package com.hp.omp.model;

import com.hp.omp.service.BudgetSubCategoryLocalServiceUtil;
import com.hp.omp.service.ClpSerializer;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class BudgetSubCategoryClp extends BaseModelImpl<BudgetSubCategory>
    implements BudgetSubCategory {
    private long _budgetSubCategoryId;
    private String _budgetSubCategoryName;
    private boolean _display;
    private BaseModel<?> _budgetSubCategoryRemoteModel;

    public BudgetSubCategoryClp() {
    }

    public Class<?> getModelClass() {
        return BudgetSubCategory.class;
    }

    public String getModelClassName() {
        return BudgetSubCategory.class.getName();
    }

    public long getPrimaryKey() {
        return _budgetSubCategoryId;
    }

    public void setPrimaryKey(long primaryKey) {
        setBudgetSubCategoryId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_budgetSubCategoryId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("budgetSubCategoryId", getBudgetSubCategoryId());
        attributes.put("budgetSubCategoryName", getBudgetSubCategoryName());
        attributes.put("display", getDisplay());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long budgetSubCategoryId = (Long) attributes.get("budgetSubCategoryId");

        if (budgetSubCategoryId != null) {
            setBudgetSubCategoryId(budgetSubCategoryId);
        }

        String budgetSubCategoryName = (String) attributes.get(
                "budgetSubCategoryName");

        if (budgetSubCategoryName != null) {
            setBudgetSubCategoryName(budgetSubCategoryName);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
    }

    public long getBudgetSubCategoryId() {
        return _budgetSubCategoryId;
    }

    public void setBudgetSubCategoryId(long budgetSubCategoryId) {
        _budgetSubCategoryId = budgetSubCategoryId;

        if (_budgetSubCategoryRemoteModel != null) {
            try {
                Class<?> clazz = _budgetSubCategoryRemoteModel.getClass();

                Method method = clazz.getMethod("setBudgetSubCategoryId",
                        long.class);

                method.invoke(_budgetSubCategoryRemoteModel, budgetSubCategoryId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getBudgetSubCategoryName() {
        return _budgetSubCategoryName;
    }

    public void setBudgetSubCategoryName(String budgetSubCategoryName) {
        _budgetSubCategoryName = budgetSubCategoryName;

        if (_budgetSubCategoryRemoteModel != null) {
            try {
                Class<?> clazz = _budgetSubCategoryRemoteModel.getClass();

                Method method = clazz.getMethod("setBudgetSubCategoryName",
                        String.class);

                method.invoke(_budgetSubCategoryRemoteModel,
                    budgetSubCategoryName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;

        if (_budgetSubCategoryRemoteModel != null) {
            try {
                Class<?> clazz = _budgetSubCategoryRemoteModel.getClass();

                Method method = clazz.getMethod("setDisplay", boolean.class);

                method.invoke(_budgetSubCategoryRemoteModel, display);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getBudgetSubCategoryRemoteModel() {
        return _budgetSubCategoryRemoteModel;
    }

    public void setBudgetSubCategoryRemoteModel(
        BaseModel<?> budgetSubCategoryRemoteModel) {
        _budgetSubCategoryRemoteModel = budgetSubCategoryRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _budgetSubCategoryRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_budgetSubCategoryRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            BudgetSubCategoryLocalServiceUtil.addBudgetSubCategory(this);
        } else {
            BudgetSubCategoryLocalServiceUtil.updateBudgetSubCategory(this);
        }
    }

    @Override
    public BudgetSubCategory toEscapedModel() {
        return (BudgetSubCategory) ProxyUtil.newProxyInstance(BudgetSubCategory.class.getClassLoader(),
            new Class[] { BudgetSubCategory.class },
            new AutoEscapeBeanHandler(this));
    }

    public BudgetSubCategory toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        BudgetSubCategoryClp clone = new BudgetSubCategoryClp();

        clone.setBudgetSubCategoryId(getBudgetSubCategoryId());
        clone.setBudgetSubCategoryName(getBudgetSubCategoryName());
        clone.setDisplay(getDisplay());

        return clone;
    }

    public int compareTo(BudgetSubCategory budgetSubCategory) {
        long primaryKey = budgetSubCategory.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof BudgetSubCategoryClp)) {
            return false;
        }

        BudgetSubCategoryClp budgetSubCategory = (BudgetSubCategoryClp) obj;

        long primaryKey = budgetSubCategory.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{budgetSubCategoryId=");
        sb.append(getBudgetSubCategoryId());
        sb.append(", budgetSubCategoryName=");
        sb.append(getBudgetSubCategoryName());
        sb.append(", display=");
        sb.append(getDisplay());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(13);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.BudgetSubCategory");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>budgetSubCategoryId</column-name><column-value><![CDATA[");
        sb.append(getBudgetSubCategoryId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>budgetSubCategoryName</column-name><column-value><![CDATA[");
        sb.append(getBudgetSubCategoryName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>display</column-name><column-value><![CDATA[");
        sb.append(getDisplay());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
