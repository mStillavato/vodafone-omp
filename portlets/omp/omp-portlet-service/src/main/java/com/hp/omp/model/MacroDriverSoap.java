package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class MacroDriverSoap implements Serializable {
    private long _macroDriverId;
    private String _macroDriverName;
    private boolean _display;

    public MacroDriverSoap() {
    }

    public static MacroDriverSoap toSoapModel(MacroDriver model) {
        MacroDriverSoap soapModel = new MacroDriverSoap();

        soapModel.setMacroDriverId(model.getMacroDriverId());
        soapModel.setMacroDriverName(model.getMacroDriverName());
        soapModel.setDisplay(model.getDisplay());

        return soapModel;
    }

    public static MacroDriverSoap[] toSoapModels(MacroDriver[] models) {
        MacroDriverSoap[] soapModels = new MacroDriverSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static MacroDriverSoap[][] toSoapModels(MacroDriver[][] models) {
        MacroDriverSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new MacroDriverSoap[models.length][models[0].length];
        } else {
            soapModels = new MacroDriverSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static MacroDriverSoap[] toSoapModels(List<MacroDriver> models) {
        List<MacroDriverSoap> soapModels = new ArrayList<MacroDriverSoap>(models.size());

        for (MacroDriver model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new MacroDriverSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _macroDriverId;
    }

    public void setPrimaryKey(long pk) {
        setMacroDriverId(pk);
    }

    public long getMacroDriverId() {
        return _macroDriverId;
    }

    public void setMacroDriverId(long macroDriverId) {
        _macroDriverId = macroDriverId;
    }

    public String getMacroDriverName() {
        return _macroDriverName;
    }

    public void setMacroDriverName(String macroDriverName) {
        _macroDriverName = macroDriverName;
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;
    }
}
