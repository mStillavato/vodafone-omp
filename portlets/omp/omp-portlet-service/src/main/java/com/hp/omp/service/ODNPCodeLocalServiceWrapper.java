package com.hp.omp.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link ODNPCodeLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       ODNPCodeLocalService
 * @generated
 */
public class ODNPCodeLocalServiceWrapper implements ODNPCodeLocalService,
    ServiceWrapper<ODNPCodeLocalService> {
    private ODNPCodeLocalService _odnpCodeLocalService;

    public ODNPCodeLocalServiceWrapper(
        ODNPCodeLocalService odnpCodeLocalService) {
        _odnpCodeLocalService = odnpCodeLocalService;
    }

    /**
    * Adds the o d n p code to the database. Also notifies the appropriate model listeners.
    *
    * @param odnpCode the o d n p code
    * @return the o d n p code that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ODNPCode addODNPCode(
        com.hp.omp.model.ODNPCode odnpCode)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _odnpCodeLocalService.addODNPCode(odnpCode);
    }

    /**
    * Creates a new o d n p code with the primary key. Does not add the o d n p code to the database.
    *
    * @param odnpCodeId the primary key for the new o d n p code
    * @return the new o d n p code
    */
    public com.hp.omp.model.ODNPCode createODNPCode(long odnpCodeId) {
        return _odnpCodeLocalService.createODNPCode(odnpCodeId);
    }

    /**
    * Deletes the o d n p code with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param odnpCodeId the primary key of the o d n p code
    * @return the o d n p code that was removed
    * @throws PortalException if a o d n p code with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ODNPCode deleteODNPCode(long odnpCodeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _odnpCodeLocalService.deleteODNPCode(odnpCodeId);
    }

    /**
    * Deletes the o d n p code from the database. Also notifies the appropriate model listeners.
    *
    * @param odnpCode the o d n p code
    * @return the o d n p code that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ODNPCode deleteODNPCode(
        com.hp.omp.model.ODNPCode odnpCode)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _odnpCodeLocalService.deleteODNPCode(odnpCode);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _odnpCodeLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _odnpCodeLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _odnpCodeLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _odnpCodeLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _odnpCodeLocalService.dynamicQueryCount(dynamicQuery);
    }

    public com.hp.omp.model.ODNPCode fetchODNPCode(long odnpCodeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _odnpCodeLocalService.fetchODNPCode(odnpCodeId);
    }

    /**
    * Returns the o d n p code with the primary key.
    *
    * @param odnpCodeId the primary key of the o d n p code
    * @return the o d n p code
    * @throws PortalException if a o d n p code with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ODNPCode getODNPCode(long odnpCodeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _odnpCodeLocalService.getODNPCode(odnpCodeId);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _odnpCodeLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the o d n p codes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of o d n p codes
    * @param end the upper bound of the range of o d n p codes (not inclusive)
    * @return the range of o d n p codes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.ODNPCode> getODNPCodes(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _odnpCodeLocalService.getODNPCodes(start, end);
    }

    /**
    * Returns the number of o d n p codes.
    *
    * @return the number of o d n p codes
    * @throws SystemException if a system exception occurred
    */
    public int getODNPCodesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _odnpCodeLocalService.getODNPCodesCount();
    }

    /**
    * Updates the o d n p code in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param odnpCode the o d n p code
    * @return the o d n p code that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ODNPCode updateODNPCode(
        com.hp.omp.model.ODNPCode odnpCode)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _odnpCodeLocalService.updateODNPCode(odnpCode);
    }

    /**
    * Updates the o d n p code in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param odnpCode the o d n p code
    * @param merge whether to merge the o d n p code with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the o d n p code that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ODNPCode updateODNPCode(
        com.hp.omp.model.ODNPCode odnpCode, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _odnpCodeLocalService.updateODNPCode(odnpCode, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _odnpCodeLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _odnpCodeLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _odnpCodeLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public ODNPCodeLocalService getWrappedODNPCodeLocalService() {
        return _odnpCodeLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedODNPCodeLocalService(
        ODNPCodeLocalService odnpCodeLocalService) {
        _odnpCodeLocalService = odnpCodeLocalService;
    }

    public ODNPCodeLocalService getWrappedService() {
        return _odnpCodeLocalService;
    }

    public void setWrappedService(ODNPCodeLocalService odnpCodeLocalService) {
        _odnpCodeLocalService = odnpCodeLocalService;
    }
}
