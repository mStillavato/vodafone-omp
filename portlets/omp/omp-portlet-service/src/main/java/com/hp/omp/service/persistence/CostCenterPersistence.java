package com.hp.omp.service.persistence;

import com.hp.omp.model.CostCenter;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the cost center service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see CostCenterPersistenceImpl
 * @see CostCenterUtil
 * @generated
 */
public interface CostCenterPersistence extends BasePersistence<CostCenter> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link CostCenterUtil} to access the cost center persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the cost center in the entity cache if it is enabled.
    *
    * @param costCenter the cost center
    */
    public void cacheResult(com.hp.omp.model.CostCenter costCenter);

    /**
    * Caches the cost centers in the entity cache if it is enabled.
    *
    * @param costCenters the cost centers
    */
    public void cacheResult(
        java.util.List<com.hp.omp.model.CostCenter> costCenters);

    /**
    * Creates a new cost center with the primary key. Does not add the cost center to the database.
    *
    * @param costCenterId the primary key for the new cost center
    * @return the new cost center
    */
    public com.hp.omp.model.CostCenter create(long costCenterId);

    /**
    * Removes the cost center with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param costCenterId the primary key of the cost center
    * @return the cost center that was removed
    * @throws com.hp.omp.NoSuchCostCenterException if a cost center with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.CostCenter remove(long costCenterId)
        throws com.hp.omp.NoSuchCostCenterException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.CostCenter updateImpl(
        com.hp.omp.model.CostCenter costCenter, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the cost center with the primary key or throws a {@link com.hp.omp.NoSuchCostCenterException} if it could not be found.
    *
    * @param costCenterId the primary key of the cost center
    * @return the cost center
    * @throws com.hp.omp.NoSuchCostCenterException if a cost center with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.CostCenter findByPrimaryKey(long costCenterId)
        throws com.hp.omp.NoSuchCostCenterException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the cost center with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param costCenterId the primary key of the cost center
    * @return the cost center, or <code>null</code> if a cost center with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.CostCenter fetchByPrimaryKey(long costCenterId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the cost centers.
    *
    * @return the cost centers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.CostCenter> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the cost centers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of cost centers
    * @param end the upper bound of the range of cost centers (not inclusive)
    * @return the range of cost centers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.CostCenter> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the cost centers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of cost centers
    * @param end the upper bound of the range of cost centers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of cost centers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.CostCenter> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the cost centers from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of cost centers.
    *
    * @return the number of cost centers
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
