package com.hp.omp.model.custom;

import java.util.HashMap;
import java.util.Map;

public enum PositionStatus {
	NEW(1, "NEW"),
	ASSET_RECEIVED(3,"ASSET_RECEIVED"),
	SC_RECEIVED(4,"SC_RECEIVED"),
	PO_RECEIVED(5,"PO_RECEIVED"),
	PO_CLOSED(6,"PO_CLOSED"),
	PO_DELETED(7,"PO_DELETED"),
	DELETED(8,"DELETED"),
	PENDING_IC_APPROVAL(9, "Pending I&C approval"),
	IC_APPROVED(10,"Approved"),
	SUBMITTED(11,"Submitted"),
	ASSIGNED(12,"Assigned"),
	GR_REQUESTED(13,"GR Requested"),
	GR_CLOSED(14,"GR Closed"),;
	private int code;
	private String label;
	
	
	
	private PositionStatus(int code, String label){
		this.code = code;
		this.label = label;
	}
	
	
	  private static Map<Integer, PositionStatus> codeToStatusMapping;
		 
	    
	 
	    public static PositionStatus getStatus(int i) {
	        if (codeToStatusMapping == null) {
	            initMapping();
	        }
	        return codeToStatusMapping.get(i);
	    }
	 
	    private static void initMapping() {
	        codeToStatusMapping = new HashMap<Integer, PositionStatus>();
	        for (PositionStatus s : values()) {
	            codeToStatusMapping.put(s.code, s);
	        }
	    }
	public int getCode(){
		return code;
	}
	
	public String getLabel(){
		return label;
	}
}
