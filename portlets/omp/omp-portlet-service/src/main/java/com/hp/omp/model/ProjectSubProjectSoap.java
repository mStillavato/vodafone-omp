package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class ProjectSubProjectSoap implements Serializable {
    private long _projectSubProjectId;
    private long _projectId;
    private long _subProjectId;

    public ProjectSubProjectSoap() {
    }

    public static ProjectSubProjectSoap toSoapModel(ProjectSubProject model) {
        ProjectSubProjectSoap soapModel = new ProjectSubProjectSoap();

        soapModel.setProjectSubProjectId(model.getProjectSubProjectId());
        soapModel.setProjectId(model.getProjectId());
        soapModel.setSubProjectId(model.getSubProjectId());

        return soapModel;
    }

    public static ProjectSubProjectSoap[] toSoapModels(
        ProjectSubProject[] models) {
        ProjectSubProjectSoap[] soapModels = new ProjectSubProjectSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static ProjectSubProjectSoap[][] toSoapModels(
        ProjectSubProject[][] models) {
        ProjectSubProjectSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new ProjectSubProjectSoap[models.length][models[0].length];
        } else {
            soapModels = new ProjectSubProjectSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static ProjectSubProjectSoap[] toSoapModels(
        List<ProjectSubProject> models) {
        List<ProjectSubProjectSoap> soapModels = new ArrayList<ProjectSubProjectSoap>(models.size());

        for (ProjectSubProject model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new ProjectSubProjectSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _projectSubProjectId;
    }

    public void setPrimaryKey(long pk) {
        setProjectSubProjectId(pk);
    }

    public long getProjectSubProjectId() {
        return _projectSubProjectId;
    }

    public void setProjectSubProjectId(long projectSubProjectId) {
        _projectSubProjectId = projectSubProjectId;
    }

    public long getProjectId() {
        return _projectId;
    }

    public void setProjectId(long projectId) {
        _projectId = projectId;
    }

    public long getSubProjectId() {
        return _subProjectId;
    }

    public void setSubProjectId(long subProjectId) {
        _subProjectId = subProjectId;
    }
}
