package com.hp.omp.model;

import com.hp.omp.service.ClpSerializer;
import com.hp.omp.service.LocationLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class LocationClp extends BaseModelImpl<Location> implements Location {
	
    private long _locationTargaId;
    private String _codiceOffice;
    private String _targaTecnicaSap;
    private String _location;
    private String _pr;
    private Date _dataOnAir;
    private BaseModel<?> _locationRemoteModel;

    public LocationClp() {
    }

    public Class<?> getModelClass() {
        return Location.class;
    }

    public String getModelClassName() {
        return Location.class.getName();
    }

    public long getPrimaryKey() {
        return _locationTargaId;
    }

    public void setPrimaryKey(long primaryKey) {
        setLocationTargaId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_locationTargaId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("locationTargaId", getLocationTargaId());
        attributes.put("codiceOffice", getCodiceOffice());
        attributes.put("targaTecnicaSap", getTargaTecnicaSap());
        attributes.put("location", getLocation());
        attributes.put("pr", getPr());
        attributes.put("dataOnAir", getDataOnAir());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
    	
        Long locationTargaId = (Long) attributes.get("locationTargaId");
        if (locationTargaId != null) {
            setLocationTargaId(locationTargaId);
        }

        String codiceOffice = (String) attributes.get("codiceOffice");
        if (codiceOffice != null) {
            setCodiceOffice(codiceOffice);
        }

        String targaTecnicaSap = (String) attributes.get("targaTecnicaSap");
        if (targaTecnicaSap != null) {
            setTargaTecnicaSap(targaTecnicaSap);
        }

        String location = (String) attributes.get("location");
        if (location != null) {
            setLocation(location);
        }
        
        String pr = (String) attributes.get("pr");
        if (pr != null) {
            setPr(pr);
        }

		Date dataOnAir = (Date)attributes.get("dataOnAir");
		if (dataOnAir != null) {
			setDataOnAir(dataOnAir);
		}
    }

    public long getLocationTargaId() {
        return _locationTargaId;
    }

    public void setLocationTargaId(long locationTargaId) {
        _locationTargaId = locationTargaId;

        if (_locationRemoteModel != null) {
            try {
                Class<?> clazz = _locationRemoteModel.getClass();

                Method method = clazz.getMethod("setLocationTargaId", long.class);

                method.invoke(_locationRemoteModel, locationTargaId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getCodiceOffice() {
        return _codiceOffice;
    }

    public void setCodiceOffice(String codiceOffice) {
        _codiceOffice = codiceOffice;

        if (_locationRemoteModel != null) {
            try {
                Class<?> clazz = _locationRemoteModel.getClass();

                Method method = clazz.getMethod("setCodiceOffice", String.class);

                method.invoke(_locationRemoteModel, codiceOffice);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getTargaTecnicaSap() {
        return _targaTecnicaSap;
    }

    public void setTargaTecnicaSap(String targaTecnicaSap) {
        _targaTecnicaSap = targaTecnicaSap;

        if (_locationRemoteModel != null) {
            try {
                Class<?> clazz = _locationRemoteModel.getClass();

                Method method = clazz.getMethod("setTargaTecnicaSap", String.class);

                method.invoke(_locationRemoteModel, targaTecnicaSap);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getLocation() {
        return _location;
    }

    public void setLocation(String location) {
        _location = location;

        if (_locationRemoteModel != null) {
            try {
                Class<?> clazz = _locationRemoteModel.getClass();

                Method method = clazz.getMethod("setLocation", String.class);

                method.invoke(_locationRemoteModel, location);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }
    
    public String getPr() {
        return _pr;
    }

    public void setPr(String pr) {
        _pr = pr;

        if (_locationRemoteModel != null) {
            try {
                Class<?> clazz = _locationRemoteModel.getClass();

                Method method = clazz.getMethod("setPr", String.class);

                method.invoke(_locationRemoteModel, pr);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }  
    
	public Date getDataOnAir() {
		return _dataOnAir;
	}

	public void setDataOnAir(Date dataOnAir) {
		_dataOnAir = dataOnAir;

		if (_locationRemoteModel != null) {
			try {
				Class<?> clazz = _locationRemoteModel.getClass();

				Method method = clazz.getMethod("setDataOnAir", Date.class);

				method.invoke(_locationRemoteModel, dataOnAir);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}
    

    public BaseModel<?> getLocationRemoteModel() {
        return _locationRemoteModel;
    }

    public void setLocationRemoteModel(BaseModel<?> locationRemoteModel) {
        _locationRemoteModel = locationRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _locationRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_locationRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            LocationLocalServiceUtil.addLocation(this);
        } else {
            LocationLocalServiceUtil.updateLocation(this);
        }
    }

    @Override
    public Location toEscapedModel() {
        return (Location) ProxyUtil.newProxyInstance(Location.class.getClassLoader(),
            new Class[] { Location.class }, new AutoEscapeBeanHandler(this));
    }

    public Location toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        LocationClp clone = new LocationClp();

        clone.setLocationTargaId(getLocationTargaId());
        clone.setCodiceOffice(getCodiceOffice());
        clone.setTargaTecnicaSap(getTargaTecnicaSap());
        clone.setLocation(getLocation());
        clone.setPr(getPr());
        clone.setDataOnAir(getDataOnAir());

        return clone;
    }

    public int compareTo(Location location) {
        long primaryKey = location.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof LocationClp)) {
            return false;
        }

        LocationClp location = (LocationClp) obj;

        long primaryKey = location.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(9);

        sb.append("{locationTargaId=");
        sb.append(getLocationTargaId());
        sb.append(", codiceOffice=");
        sb.append(getCodiceOffice());
        sb.append(", targaTecnicaSap=");
        sb.append(getTargaTecnicaSap());
        sb.append(", location=");
        sb.append(getLocation());
        sb.append(", pr=");
        sb.append(getPr());
        sb.append(", dataOnAir=");
        sb.append(getDataOnAir());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(16);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.Location");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>locationTargaId</column-name><column-value><![CDATA[");
        sb.append(getLocationTargaId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>codiceOffice</column-name><column-value><![CDATA[");
        sb.append(getCodiceOffice());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>targaTecnicaSap</column-name><column-value><![CDATA[");
        sb.append(getTargaTecnicaSap());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>location</column-name><column-value><![CDATA[");
        sb.append(getLocation());
        sb.append("]]></column-value></column>");
        sb.append(
                "<column><column-name>pr</column-name><column-value><![CDATA[");
        sb.append(getPr());
        sb.append("]]></column-value></column>");
        sb.append(
                "<column><column-name>dataOnAir</column-name><column-value><![CDATA[");
            sb.append(getDataOnAir());
            sb.append("]]></column-value></column>");
        sb.append("</model>");

        return sb.toString();
    }
}
