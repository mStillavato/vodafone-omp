package com.hp.omp.service.persistence;

public interface MacroDriverFinder {
    
    public java.util.List<com.hp.omp.model.MacroDriver> getMacroDrivers(
    		int start, int end, String searchFilter)
            throws com.liferay.portal.kernel.exception.SystemException;
}
