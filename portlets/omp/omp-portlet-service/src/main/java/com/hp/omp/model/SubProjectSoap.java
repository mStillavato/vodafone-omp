package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class SubProjectSoap implements Serializable {
    private long _subProjectId;
    private String _subProjectName;
    private String _description;
    private boolean _display;

    public SubProjectSoap() {
    }

    public static SubProjectSoap toSoapModel(SubProject model) {
        SubProjectSoap soapModel = new SubProjectSoap();

        soapModel.setSubProjectId(model.getSubProjectId());
        soapModel.setSubProjectName(model.getSubProjectName());
        soapModel.setDescription(model.getDescription());
        soapModel.setDisplay(model.getDisplay());

        return soapModel;
    }

    public static SubProjectSoap[] toSoapModels(SubProject[] models) {
        SubProjectSoap[] soapModels = new SubProjectSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static SubProjectSoap[][] toSoapModels(SubProject[][] models) {
        SubProjectSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new SubProjectSoap[models.length][models[0].length];
        } else {
            soapModels = new SubProjectSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static SubProjectSoap[] toSoapModels(List<SubProject> models) {
        List<SubProjectSoap> soapModels = new ArrayList<SubProjectSoap>(models.size());

        for (SubProject model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new SubProjectSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _subProjectId;
    }

    public void setPrimaryKey(long pk) {
        setSubProjectId(pk);
    }

    public long getSubProjectId() {
        return _subProjectId;
    }

    public void setSubProjectId(long subProjectId) {
        _subProjectId = subProjectId;
    }

    public String getSubProjectName() {
        return _subProjectName;
    }

    public void setSubProjectName(String subProjectName) {
        _subProjectName = subProjectName;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;
    }
}
