package com.hp.omp.model;

import com.hp.omp.service.BuyerLocalServiceUtil;
import com.hp.omp.service.ClpSerializer;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class BuyerClp extends BaseModelImpl<Buyer> implements Buyer {
    private long _buyerId;
    private String _name;
    private boolean _display;
    private int _gruppoUsers;
    private BaseModel<?> _buyerRemoteModel;

    public BuyerClp() {
    }

    public Class<?> getModelClass() {
        return Buyer.class;
    }

    public String getModelClassName() {
        return Buyer.class.getName();
    }

    public long getPrimaryKey() {
        return _buyerId;
    }

    public void setPrimaryKey(long primaryKey) {
        setBuyerId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_buyerId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("buyerId", getBuyerId());
        attributes.put("name", getName());
        attributes.put("display", getDisplay());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long buyerId = (Long) attributes.get("buyerId");

        if (buyerId != null) {
            setBuyerId(buyerId);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
    }

    public long getBuyerId() {
        return _buyerId;
    }

    public void setBuyerId(long buyerId) {
        _buyerId = buyerId;

        if (_buyerRemoteModel != null) {
            try {
                Class<?> clazz = _buyerRemoteModel.getClass();

                Method method = clazz.getMethod("setBuyerId", long.class);

                method.invoke(_buyerRemoteModel, buyerId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;

        if (_buyerRemoteModel != null) {
            try {
                Class<?> clazz = _buyerRemoteModel.getClass();

                Method method = clazz.getMethod("setName", String.class);

                method.invoke(_buyerRemoteModel, name);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;

        if (_buyerRemoteModel != null) {
            try {
                Class<?> clazz = _buyerRemoteModel.getClass();

                Method method = clazz.getMethod("setDisplay", boolean.class);

                method.invoke(_buyerRemoteModel, display);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getBuyerRemoteModel() {
        return _buyerRemoteModel;
    }

    public void setBuyerRemoteModel(BaseModel<?> buyerRemoteModel) {
        _buyerRemoteModel = buyerRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _buyerRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_buyerRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    public void persist() throws SystemException {
        if (this.isNew()) {
            BuyerLocalServiceUtil.addBuyer(this);
        } else {
            BuyerLocalServiceUtil.updateBuyer(this);
        }
    }

    @Override
    public Buyer toEscapedModel() {
        return (Buyer) ProxyUtil.newProxyInstance(Buyer.class.getClassLoader(),
            new Class[] { Buyer.class }, new AutoEscapeBeanHandler(this));
    }

    public Buyer toUnescapedModel() {
        return this;
    }

    @Override
    public Object clone() {
        BuyerClp clone = new BuyerClp();

        clone.setBuyerId(getBuyerId());
        clone.setName(getName());
        clone.setDisplay(getDisplay());

        return clone;
    }

    public int compareTo(Buyer buyer) {
        long primaryKey = buyer.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof BuyerClp)) {
            return false;
        }

        BuyerClp buyer = (BuyerClp) obj;

        long primaryKey = buyer.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{buyerId=");
        sb.append(getBuyerId());
        sb.append(", name=");
        sb.append(getName());
        sb.append(", display=");
        sb.append(getDisplay());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(13);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.Buyer");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>buyerId</column-name><column-value><![CDATA[");
        sb.append(getBuyerId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>name</column-name><column-value><![CDATA[");
        sb.append(getName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>display</column-name><column-value><![CDATA[");
        sb.append(getDisplay());
        sb.append("]]></column-value></column>");
        sb.append(
                "<column><column-name>gruppoUsers</column-name><column-value><![CDATA[");
        sb.append(getGruppoUsers());
        sb.append("]]></column-value></column>");
        sb.append("</model>");

        return sb.toString();
    }
    
	public int getGruppoUsers() {
		return _gruppoUsers;
	}

	public void setGruppoUsers(int gruppoUsers) {
        _gruppoUsers = gruppoUsers;

        if (_buyerRemoteModel != null) {
            try {
                Class<?> clazz = _buyerRemoteModel.getClass();

                Method method = clazz.getMethod("setGruppoUsers", int.class);

                method.invoke(_buyerRemoteModel, gruppoUsers);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
	}
}
