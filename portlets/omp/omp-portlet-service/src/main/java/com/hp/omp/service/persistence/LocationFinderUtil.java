package com.hp.omp.service.persistence;

import com.hp.omp.model.custom.LocationDTO;
import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class LocationFinderUtil {
	private static LocationFinder _finder;

	public static java.util.List<LocationDTO> getLocationsList()
					throws com.liferay.portal.kernel.exception.SystemException {
		return getFinder().getLocationsList();
	}
	
	public static long getLocationsCountList(String searchFilter)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getFinder().getLocationsCountList(searchFilter);
	}
	
	public static LocationDTO getLocationByTargaTecnica(String targaTecnica)
					throws com.liferay.portal.kernel.exception.SystemException {
		return getFinder().getLocationByTargaTecnica(targaTecnica);
	}
	
	public static LocationDTO getLocationByLocationEvo(String locationEvo) 
					throws com.liferay.portal.kernel.exception.SystemException {
		return getFinder().getLocationByLocationEvo(locationEvo);
	}

	public static LocationFinder getFinder() {
		if (_finder == null) {
			_finder = (LocationFinder) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
					LocationFinder.class.getName());

			ReferenceRegistry.registerReference(LocationFinderUtil.class,
					"_finder");
		}

		return _finder;
	}

	public void setFinder(LocationFinder finder) {
		_finder = finder;

		ReferenceRegistry.registerReference(LocationFinderUtil.class,
				"_finder");
	}

}
