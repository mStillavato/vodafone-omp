package com.hp.omp.service.persistence;

import com.hp.omp.model.CostCenterUser;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the cost center user service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see CostCenterUserPersistenceImpl
 * @see CostCenterUserUtil
 * @generated
 */
public interface CostCenterUserPersistence extends BasePersistence<CostCenterUser> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link CostCenterUserUtil} to access the cost center user persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the cost center user in the entity cache if it is enabled.
    *
    * @param costCenterUser the cost center user
    */
    public void cacheResult(com.hp.omp.model.CostCenterUser costCenterUser);

    /**
    * Caches the cost center users in the entity cache if it is enabled.
    *
    * @param costCenterUsers the cost center users
    */
    public void cacheResult(
        java.util.List<com.hp.omp.model.CostCenterUser> costCenterUsers);

    /**
    * Creates a new cost center user with the primary key. Does not add the cost center user to the database.
    *
    * @param costCenterUserId the primary key for the new cost center user
    * @return the new cost center user
    */
    public com.hp.omp.model.CostCenterUser create(long costCenterUserId);

    /**
    * Removes the cost center user with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param costCenterUserId the primary key of the cost center user
    * @return the cost center user that was removed
    * @throws com.hp.omp.NoSuchCostCenterUserException if a cost center user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.CostCenterUser remove(long costCenterUserId)
        throws com.hp.omp.NoSuchCostCenterUserException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.CostCenterUser updateImpl(
        com.hp.omp.model.CostCenterUser costCenterUser, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the cost center user with the primary key or throws a {@link com.hp.omp.NoSuchCostCenterUserException} if it could not be found.
    *
    * @param costCenterUserId the primary key of the cost center user
    * @return the cost center user
    * @throws com.hp.omp.NoSuchCostCenterUserException if a cost center user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.CostCenterUser findByPrimaryKey(
        long costCenterUserId)
        throws com.hp.omp.NoSuchCostCenterUserException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the cost center user with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param costCenterUserId the primary key of the cost center user
    * @return the cost center user, or <code>null</code> if a cost center user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.CostCenterUser fetchByPrimaryKey(
        long costCenterUserId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the cost center users.
    *
    * @return the cost center users
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.CostCenterUser> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the cost center users.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of cost center users
    * @param end the upper bound of the range of cost center users (not inclusive)
    * @return the range of cost center users
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.CostCenterUser> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the cost center users.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of cost center users
    * @param end the upper bound of the range of cost center users (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of cost center users
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.CostCenterUser> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the cost center users from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of cost center users.
    *
    * @return the number of cost center users
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
