package com.hp.omp.service;

import java.util.List;

import com.hp.omp.model.BudgetSubCategory;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link BudgetSubCategoryLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       BudgetSubCategoryLocalService
 * @generated
 */
public class BudgetSubCategoryLocalServiceWrapper
    implements BudgetSubCategoryLocalService,
        ServiceWrapper<BudgetSubCategoryLocalService> {
    private BudgetSubCategoryLocalService _budgetSubCategoryLocalService;

    public BudgetSubCategoryLocalServiceWrapper(
        BudgetSubCategoryLocalService budgetSubCategoryLocalService) {
        _budgetSubCategoryLocalService = budgetSubCategoryLocalService;
    }

    /**
    * Adds the budget sub category to the database. Also notifies the appropriate model listeners.
    *
    * @param budgetSubCategory the budget sub category
    * @return the budget sub category that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.BudgetSubCategory addBudgetSubCategory(
        com.hp.omp.model.BudgetSubCategory budgetSubCategory)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetSubCategoryLocalService.addBudgetSubCategory(budgetSubCategory);
    }

    /**
    * Creates a new budget sub category with the primary key. Does not add the budget sub category to the database.
    *
    * @param budgetSubCategoryId the primary key for the new budget sub category
    * @return the new budget sub category
    */
    public com.hp.omp.model.BudgetSubCategory createBudgetSubCategory(
        long budgetSubCategoryId) {
        return _budgetSubCategoryLocalService.createBudgetSubCategory(budgetSubCategoryId);
    }

    /**
    * Deletes the budget sub category with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param budgetSubCategoryId the primary key of the budget sub category
    * @return the budget sub category that was removed
    * @throws PortalException if a budget sub category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.BudgetSubCategory deleteBudgetSubCategory(
        long budgetSubCategoryId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _budgetSubCategoryLocalService.deleteBudgetSubCategory(budgetSubCategoryId);
    }

    /**
    * Deletes the budget sub category from the database. Also notifies the appropriate model listeners.
    *
    * @param budgetSubCategory the budget sub category
    * @return the budget sub category that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.BudgetSubCategory deleteBudgetSubCategory(
        com.hp.omp.model.BudgetSubCategory budgetSubCategory)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetSubCategoryLocalService.deleteBudgetSubCategory(budgetSubCategory);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _budgetSubCategoryLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetSubCategoryLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetSubCategoryLocalService.dynamicQuery(dynamicQuery, start,
            end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetSubCategoryLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetSubCategoryLocalService.dynamicQueryCount(dynamicQuery);
    }

    public com.hp.omp.model.BudgetSubCategory fetchBudgetSubCategory(
        long budgetSubCategoryId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetSubCategoryLocalService.fetchBudgetSubCategory(budgetSubCategoryId);
    }

    /**
    * Returns the budget sub category with the primary key.
    *
    * @param budgetSubCategoryId the primary key of the budget sub category
    * @return the budget sub category
    * @throws PortalException if a budget sub category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.BudgetSubCategory getBudgetSubCategory(
        long budgetSubCategoryId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _budgetSubCategoryLocalService.getBudgetSubCategory(budgetSubCategoryId);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _budgetSubCategoryLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the budget sub categories.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of budget sub categories
    * @param end the upper bound of the range of budget sub categories (not inclusive)
    * @return the range of budget sub categories
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.BudgetSubCategory> getBudgetSubCategories(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetSubCategoryLocalService.getBudgetSubCategories(start, end);
    }

    /**
    * Returns the number of budget sub categories.
    *
    * @return the number of budget sub categories
    * @throws SystemException if a system exception occurred
    */
    public int getBudgetSubCategoriesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetSubCategoryLocalService.getBudgetSubCategoriesCount();
    }

    /**
    * Updates the budget sub category in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param budgetSubCategory the budget sub category
    * @return the budget sub category that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.BudgetSubCategory updateBudgetSubCategory(
        com.hp.omp.model.BudgetSubCategory budgetSubCategory)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetSubCategoryLocalService.updateBudgetSubCategory(budgetSubCategory);
    }

    /**
    * Updates the budget sub category in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param budgetSubCategory the budget sub category
    * @param merge whether to merge the budget sub category with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the budget sub category that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.BudgetSubCategory updateBudgetSubCategory(
        com.hp.omp.model.BudgetSubCategory budgetSubCategory, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetSubCategoryLocalService.updateBudgetSubCategory(budgetSubCategory,
            merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _budgetSubCategoryLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _budgetSubCategoryLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _budgetSubCategoryLocalService.invokeMethod(name,
            parameterTypes, arguments);
    }

    public com.hp.omp.model.BudgetSubCategory getBudgetSubCategoryByName(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _budgetSubCategoryLocalService.getBudgetSubCategoryByName(name);
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public BudgetSubCategoryLocalService getWrappedBudgetSubCategoryLocalService() {
        return _budgetSubCategoryLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedBudgetSubCategoryLocalService(
        BudgetSubCategoryLocalService budgetSubCategoryLocalService) {
        _budgetSubCategoryLocalService = budgetSubCategoryLocalService;
    }

    public BudgetSubCategoryLocalService getWrappedService() {
        return _budgetSubCategoryLocalService;
    }

    public void setWrappedService(
        BudgetSubCategoryLocalService budgetSubCategoryLocalService) {
        _budgetSubCategoryLocalService = budgetSubCategoryLocalService;
    }

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<BudgetSubCategory> getBudgetSubCategoryList(Integer pageNumber,
			Integer pageSize, String searchFilter) throws SystemException {
		return _budgetSubCategoryLocalService.getBudgetSubCategoryList(pageNumber, pageSize, searchFilter);
	}
}
