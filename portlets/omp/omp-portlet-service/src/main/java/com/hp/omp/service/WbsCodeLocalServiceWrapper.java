package com.hp.omp.service;

import java.io.OutputStream;
import java.util.List;

import com.hp.omp.model.WbsCode;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link WbsCodeLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       WbsCodeLocalService
 * @generated
 */
public class WbsCodeLocalServiceWrapper implements WbsCodeLocalService,
    ServiceWrapper<WbsCodeLocalService> {
    private WbsCodeLocalService _wbsCodeLocalService;

    public WbsCodeLocalServiceWrapper(WbsCodeLocalService wbsCodeLocalService) {
        _wbsCodeLocalService = wbsCodeLocalService;
    }

    /**
    * Adds the wbs code to the database. Also notifies the appropriate model listeners.
    *
    * @param wbsCode the wbs code
    * @return the wbs code that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.WbsCode addWbsCode(com.hp.omp.model.WbsCode wbsCode)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _wbsCodeLocalService.addWbsCode(wbsCode);
    }

    /**
    * Creates a new wbs code with the primary key. Does not add the wbs code to the database.
    *
    * @param wbsCodeId the primary key for the new wbs code
    * @return the new wbs code
    */
    public com.hp.omp.model.WbsCode createWbsCode(long wbsCodeId) {
        return _wbsCodeLocalService.createWbsCode(wbsCodeId);
    }

    /**
    * Deletes the wbs code with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param wbsCodeId the primary key of the wbs code
    * @return the wbs code that was removed
    * @throws PortalException if a wbs code with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.WbsCode deleteWbsCode(long wbsCodeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _wbsCodeLocalService.deleteWbsCode(wbsCodeId);
    }

    /**
    * Deletes the wbs code from the database. Also notifies the appropriate model listeners.
    *
    * @param wbsCode the wbs code
    * @return the wbs code that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.WbsCode deleteWbsCode(
        com.hp.omp.model.WbsCode wbsCode)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _wbsCodeLocalService.deleteWbsCode(wbsCode);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _wbsCodeLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _wbsCodeLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _wbsCodeLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _wbsCodeLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _wbsCodeLocalService.dynamicQueryCount(dynamicQuery);
    }

    public com.hp.omp.model.WbsCode fetchWbsCode(long wbsCodeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _wbsCodeLocalService.fetchWbsCode(wbsCodeId);
    }

    /**
    * Returns the wbs code with the primary key.
    *
    * @param wbsCodeId the primary key of the wbs code
    * @return the wbs code
    * @throws PortalException if a wbs code with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.WbsCode getWbsCode(long wbsCodeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _wbsCodeLocalService.getWbsCode(wbsCodeId);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _wbsCodeLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the wbs codes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of wbs codes
    * @param end the upper bound of the range of wbs codes (not inclusive)
    * @return the range of wbs codes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.WbsCode> getWbsCodes(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _wbsCodeLocalService.getWbsCodes(start, end);
    }

    /**
    * Returns the number of wbs codes.
    *
    * @return the number of wbs codes
    * @throws SystemException if a system exception occurred
    */
    public int getWbsCodesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _wbsCodeLocalService.getWbsCodesCount();
    }

    /**
    * Updates the wbs code in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param wbsCode the wbs code
    * @return the wbs code that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.WbsCode updateWbsCode(
        com.hp.omp.model.WbsCode wbsCode)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _wbsCodeLocalService.updateWbsCode(wbsCode);
    }

    /**
    * Updates the wbs code in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param wbsCode the wbs code
    * @param merge whether to merge the wbs code with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the wbs code that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.WbsCode updateWbsCode(
        com.hp.omp.model.WbsCode wbsCode, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _wbsCodeLocalService.updateWbsCode(wbsCode, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _wbsCodeLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _wbsCodeLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _wbsCodeLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    public com.hp.omp.model.WbsCode getWbsCodeByName(java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _wbsCodeLocalService.getWbsCodeByName(name);
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public WbsCodeLocalService getWrappedWbsCodeLocalService() {
        return _wbsCodeLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedWbsCodeLocalService(
        WbsCodeLocalService wbsCodeLocalService) {
        _wbsCodeLocalService = wbsCodeLocalService;
    }

    public WbsCodeLocalService getWrappedService() {
        return _wbsCodeLocalService;
    }

    public void setWrappedService(WbsCodeLocalService wbsCodeLocalService) {
        _wbsCodeLocalService = wbsCodeLocalService;
    }

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<WbsCode> getWbsCodesList(int start, int end, String searchFilter)
			throws SystemException {
		return _wbsCodeLocalService.getWbsCodesList(start, end, searchFilter);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getWbsCodesCountList(String searchFilter) throws SystemException {
		return _wbsCodeLocalService.getWbsCodesCountList(searchFilter);
	}

	public void exportWbsCodesAsExcel(List<WbsCode> listWbsCodes,
			List<Object> header, OutputStream out) throws SystemException {
		_wbsCodeLocalService.exportWbsCodesAsExcel(listWbsCodes, header, out);
	}
}
