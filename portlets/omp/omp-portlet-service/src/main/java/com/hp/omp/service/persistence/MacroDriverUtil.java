package com.hp.omp.service.persistence;

import com.hp.omp.model.MacroDriver;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the macro driver service. This utility wraps {@link MacroDriverPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see MacroDriverPersistence
 * @see MacroDriverPersistenceImpl
 * @generated
 */
public class MacroDriverUtil {
    private static MacroDriverPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(MacroDriver macroDriver) {
        getPersistence().clearCache(macroDriver);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<MacroDriver> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<MacroDriver> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<MacroDriver> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static MacroDriver update(MacroDriver macroDriver, boolean merge)
        throws SystemException {
        return getPersistence().update(macroDriver, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static MacroDriver update(MacroDriver macroDriver, boolean merge,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(macroDriver, merge, serviceContext);
    }

    /**
    * Caches the macro driver in the entity cache if it is enabled.
    *
    * @param macroDriver the macro driver
    */
    public static void cacheResult(com.hp.omp.model.MacroDriver macroDriver) {
        getPersistence().cacheResult(macroDriver);
    }

    /**
    * Caches the macro drivers in the entity cache if it is enabled.
    *
    * @param macroDrivers the macro drivers
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.MacroDriver> macroDrivers) {
        getPersistence().cacheResult(macroDrivers);
    }

    /**
    * Creates a new macro driver with the primary key. Does not add the macro driver to the database.
    *
    * @param macroDriverId the primary key for the new macro driver
    * @return the new macro driver
    */
    public static com.hp.omp.model.MacroDriver create(long macroDriverId) {
        return getPersistence().create(macroDriverId);
    }

    /**
    * Removes the macro driver with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param macroDriverId the primary key of the macro driver
    * @return the macro driver that was removed
    * @throws com.hp.omp.NoSuchMacroDriverException if a macro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroDriver remove(long macroDriverId)
        throws com.hp.omp.NoSuchMacroDriverException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(macroDriverId);
    }

    public static com.hp.omp.model.MacroDriver updateImpl(
        com.hp.omp.model.MacroDriver macroDriver, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(macroDriver, merge);
    }

    /**
    * Returns the macro driver with the primary key or throws a {@link com.hp.omp.NoSuchMacroDriverException} if it could not be found.
    *
    * @param macroDriverId the primary key of the macro driver
    * @return the macro driver
    * @throws com.hp.omp.NoSuchMacroDriverException if a macro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroDriver findByPrimaryKey(
        long macroDriverId)
        throws com.hp.omp.NoSuchMacroDriverException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(macroDriverId);
    }

    /**
    * Returns the macro driver with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param macroDriverId the primary key of the macro driver
    * @return the macro driver, or <code>null</code> if a macro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroDriver fetchByPrimaryKey(
        long macroDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(macroDriverId);
    }

    /**
    * Returns all the macro drivers.
    *
    * @return the macro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MacroDriver> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the macro drivers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of macro drivers
    * @param end the upper bound of the range of macro drivers (not inclusive)
    * @return the range of macro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MacroDriver> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the macro drivers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of macro drivers
    * @param end the upper bound of the range of macro drivers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of macro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MacroDriver> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the macro drivers from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of macro drivers.
    *
    * @return the number of macro drivers
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static MacroDriverPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (MacroDriverPersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    MacroDriverPersistence.class.getName());

            ReferenceRegistry.registerReference(MacroDriverUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(MacroDriverPersistence persistence) {
    }
}
