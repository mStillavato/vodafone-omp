package com.hp.omp.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link CostCenterUserLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       CostCenterUserLocalService
 * @generated
 */
public class CostCenterUserLocalServiceWrapper
    implements CostCenterUserLocalService,
        ServiceWrapper<CostCenterUserLocalService> {
    private CostCenterUserLocalService _costCenterUserLocalService;

    public CostCenterUserLocalServiceWrapper(
        CostCenterUserLocalService costCenterUserLocalService) {
        _costCenterUserLocalService = costCenterUserLocalService;
    }

    /**
    * Adds the cost center user to the database. Also notifies the appropriate model listeners.
    *
    * @param costCenterUser the cost center user
    * @return the cost center user that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.CostCenterUser addCostCenterUser(
        com.hp.omp.model.CostCenterUser costCenterUser)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterUserLocalService.addCostCenterUser(costCenterUser);
    }

    /**
    * Creates a new cost center user with the primary key. Does not add the cost center user to the database.
    *
    * @param costCenterUserId the primary key for the new cost center user
    * @return the new cost center user
    */
    public com.hp.omp.model.CostCenterUser createCostCenterUser(
        long costCenterUserId) {
        return _costCenterUserLocalService.createCostCenterUser(costCenterUserId);
    }

    /**
    * Deletes the cost center user with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param costCenterUserId the primary key of the cost center user
    * @return the cost center user that was removed
    * @throws PortalException if a cost center user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.CostCenterUser deleteCostCenterUser(
        long costCenterUserId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _costCenterUserLocalService.deleteCostCenterUser(costCenterUserId);
    }

    /**
    * Deletes the cost center user from the database. Also notifies the appropriate model listeners.
    *
    * @param costCenterUser the cost center user
    * @return the cost center user that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.CostCenterUser deleteCostCenterUser(
        com.hp.omp.model.CostCenterUser costCenterUser)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterUserLocalService.deleteCostCenterUser(costCenterUser);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _costCenterUserLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterUserLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterUserLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterUserLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterUserLocalService.dynamicQueryCount(dynamicQuery);
    }

    public com.hp.omp.model.CostCenterUser fetchCostCenterUser(
        long costCenterUserId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterUserLocalService.fetchCostCenterUser(costCenterUserId);
    }

    /**
    * Returns the cost center user with the primary key.
    *
    * @param costCenterUserId the primary key of the cost center user
    * @return the cost center user
    * @throws PortalException if a cost center user with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.CostCenterUser getCostCenterUser(
        long costCenterUserId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _costCenterUserLocalService.getCostCenterUser(costCenterUserId);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _costCenterUserLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the cost center users.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of cost center users
    * @param end the upper bound of the range of cost center users (not inclusive)
    * @return the range of cost center users
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.CostCenterUser> getCostCenterUsers(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterUserLocalService.getCostCenterUsers(start, end);
    }

    /**
    * Returns the number of cost center users.
    *
    * @return the number of cost center users
    * @throws SystemException if a system exception occurred
    */
    public int getCostCenterUsersCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterUserLocalService.getCostCenterUsersCount();
    }

    /**
    * Updates the cost center user in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param costCenterUser the cost center user
    * @return the cost center user that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.CostCenterUser updateCostCenterUser(
        com.hp.omp.model.CostCenterUser costCenterUser)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterUserLocalService.updateCostCenterUser(costCenterUser);
    }

    /**
    * Updates the cost center user in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param costCenterUser the cost center user
    * @param merge whether to merge the cost center user with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the cost center user that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.CostCenterUser updateCostCenterUser(
        com.hp.omp.model.CostCenterUser costCenterUser, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterUserLocalService.updateCostCenterUser(costCenterUser,
            merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _costCenterUserLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _costCenterUserLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _costCenterUserLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    public com.hp.omp.model.CostCenterUser getCostCenterUserByCostCenterAndUser(
        long costCenterId, long userId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterUserLocalService.getCostCenterUserByCostCenterAndUser(costCenterId,
            userId);
    }

    public com.hp.omp.model.CostCenterUser getCostCenterUserByUserId(
        long userId) throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterUserLocalService.getCostCenterUserByUserId(userId);
    }

    public com.hp.omp.model.CostCenterUser getCostCenterUserByCostCenterId(
        long costCenterId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _costCenterUserLocalService.getCostCenterUserByCostCenterId(costCenterId);
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public CostCenterUserLocalService getWrappedCostCenterUserLocalService() {
        return _costCenterUserLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedCostCenterUserLocalService(
        CostCenterUserLocalService costCenterUserLocalService) {
        _costCenterUserLocalService = costCenterUserLocalService;
    }

    public CostCenterUserLocalService getWrappedService() {
        return _costCenterUserLocalService;
    }

    public void setWrappedService(
        CostCenterUserLocalService costCenterUserLocalService) {
        _costCenterUserLocalService = costCenterUserLocalService;
    }
}
