package com.hp.omp.service.persistence;

import com.hp.omp.model.MacroMicroDriver;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the macro micro driver service. This utility wraps {@link MacroMicroDriverPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see MacroMicroDriverPersistence
 * @see MacroMicroDriverPersistenceImpl
 * @generated
 */
public class MacroMicroDriverUtil {
    private static MacroMicroDriverPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(MacroMicroDriver macroMicroDriver) {
        getPersistence().clearCache(macroMicroDriver);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<MacroMicroDriver> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<MacroMicroDriver> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<MacroMicroDriver> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static MacroMicroDriver update(MacroMicroDriver macroMicroDriver,
        boolean merge) throws SystemException {
        return getPersistence().update(macroMicroDriver, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static MacroMicroDriver update(MacroMicroDriver macroMicroDriver,
        boolean merge, ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(macroMicroDriver, merge, serviceContext);
    }

    /**
    * Caches the macro micro driver in the entity cache if it is enabled.
    *
    * @param macroMicroDriver the macro micro driver
    */
    public static void cacheResult(
        com.hp.omp.model.MacroMicroDriver macroMicroDriver) {
        getPersistence().cacheResult(macroMicroDriver);
    }

    /**
    * Caches the macro micro drivers in the entity cache if it is enabled.
    *
    * @param macroMicroDrivers the macro micro drivers
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.MacroMicroDriver> macroMicroDrivers) {
        getPersistence().cacheResult(macroMicroDrivers);
    }

    /**
    * Creates a new macro micro driver with the primary key. Does not add the macro micro driver to the database.
    *
    * @param macroMicroDriverId the primary key for the new macro micro driver
    * @return the new macro micro driver
    */
    public static com.hp.omp.model.MacroMicroDriver create(
        long macroMicroDriverId) {
        return getPersistence().create(macroMicroDriverId);
    }

    /**
    * Removes the macro micro driver with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param macroMicroDriverId the primary key of the macro micro driver
    * @return the macro micro driver that was removed
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a macro micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver remove(
        long macroMicroDriverId)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(macroMicroDriverId);
    }

    public static com.hp.omp.model.MacroMicroDriver updateImpl(
        com.hp.omp.model.MacroMicroDriver macroMicroDriver, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(macroMicroDriver, merge);
    }

    /**
    * Returns the macro micro driver with the primary key or throws a {@link com.hp.omp.NoSuchMacroMicroDriverException} if it could not be found.
    *
    * @param macroMicroDriverId the primary key of the macro micro driver
    * @return the macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a macro micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver findByPrimaryKey(
        long macroMicroDriverId)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(macroMicroDriverId);
    }

    /**
    * Returns the macro micro driver with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param macroMicroDriverId the primary key of the macro micro driver
    * @return the macro micro driver, or <code>null</code> if a macro micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver fetchByPrimaryKey(
        long macroMicroDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(macroMicroDriverId);
    }

    /**
    * Returns all the macro micro drivers where macroDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @return the matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MacroMicroDriver> findByMacroDriverId(
        long macroDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByMacroDriverId(macroDriverId);
    }

    /**
    * Returns a range of all the macro micro drivers where macroDriverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param macroDriverId the macro driver ID
    * @param start the lower bound of the range of macro micro drivers
    * @param end the upper bound of the range of macro micro drivers (not inclusive)
    * @return the range of matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MacroMicroDriver> findByMacroDriverId(
        long macroDriverId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByMacroDriverId(macroDriverId, start, end);
    }

    /**
    * Returns an ordered range of all the macro micro drivers where macroDriverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param macroDriverId the macro driver ID
    * @param start the lower bound of the range of macro micro drivers
    * @param end the upper bound of the range of macro micro drivers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MacroMicroDriver> findByMacroDriverId(
        long macroDriverId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByMacroDriverId(macroDriverId, start, end,
            orderByComparator);
    }

    /**
    * Returns the first macro micro driver in the ordered set where macroDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver findByMacroDriverId_First(
        long macroDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByMacroDriverId_First(macroDriverId, orderByComparator);
    }

    /**
    * Returns the first macro micro driver in the ordered set where macroDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching macro micro driver, or <code>null</code> if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver fetchByMacroDriverId_First(
        long macroDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByMacroDriverId_First(macroDriverId, orderByComparator);
    }

    /**
    * Returns the last macro micro driver in the ordered set where macroDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver findByMacroDriverId_Last(
        long macroDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByMacroDriverId_Last(macroDriverId, orderByComparator);
    }

    /**
    * Returns the last macro micro driver in the ordered set where macroDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching macro micro driver, or <code>null</code> if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver fetchByMacroDriverId_Last(
        long macroDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByMacroDriverId_Last(macroDriverId, orderByComparator);
    }

    /**
    * Returns the macro micro drivers before and after the current macro micro driver in the ordered set where macroDriverId = &#63;.
    *
    * @param macroMicroDriverId the primary key of the current macro micro driver
    * @param macroDriverId the macro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a macro micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver[] findByMacroDriverId_PrevAndNext(
        long macroMicroDriverId, long macroDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByMacroDriverId_PrevAndNext(macroMicroDriverId,
            macroDriverId, orderByComparator);
    }

    /**
    * Returns all the macro micro drivers where microDriverId = &#63;.
    *
    * @param microDriverId the micro driver ID
    * @return the matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MacroMicroDriver> findByMicroDriverId(
        long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByMicroDriverId(microDriverId);
    }

    /**
    * Returns a range of all the macro micro drivers where microDriverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param microDriverId the micro driver ID
    * @param start the lower bound of the range of macro micro drivers
    * @param end the upper bound of the range of macro micro drivers (not inclusive)
    * @return the range of matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MacroMicroDriver> findByMicroDriverId(
        long microDriverId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByMicroDriverId(microDriverId, start, end);
    }

    /**
    * Returns an ordered range of all the macro micro drivers where microDriverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param microDriverId the micro driver ID
    * @param start the lower bound of the range of macro micro drivers
    * @param end the upper bound of the range of macro micro drivers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MacroMicroDriver> findByMicroDriverId(
        long microDriverId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByMicroDriverId(microDriverId, start, end,
            orderByComparator);
    }

    /**
    * Returns the first macro micro driver in the ordered set where microDriverId = &#63;.
    *
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver findByMicroDriverId_First(
        long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByMicroDriverId_First(microDriverId, orderByComparator);
    }

    /**
    * Returns the first macro micro driver in the ordered set where microDriverId = &#63;.
    *
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching macro micro driver, or <code>null</code> if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver fetchByMicroDriverId_First(
        long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByMicroDriverId_First(microDriverId, orderByComparator);
    }

    /**
    * Returns the last macro micro driver in the ordered set where microDriverId = &#63;.
    *
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver findByMicroDriverId_Last(
        long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByMicroDriverId_Last(microDriverId, orderByComparator);
    }

    /**
    * Returns the last macro micro driver in the ordered set where microDriverId = &#63;.
    *
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching macro micro driver, or <code>null</code> if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver fetchByMicroDriverId_Last(
        long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByMicroDriverId_Last(microDriverId, orderByComparator);
    }

    /**
    * Returns the macro micro drivers before and after the current macro micro driver in the ordered set where microDriverId = &#63;.
    *
    * @param macroMicroDriverId the primary key of the current macro micro driver
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a macro micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver[] findByMicroDriverId_PrevAndNext(
        long macroMicroDriverId, long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByMicroDriverId_PrevAndNext(macroMicroDriverId,
            microDriverId, orderByComparator);
    }

    /**
    * Returns all the macro micro drivers where macroDriverId = &#63; and microDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @return the matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MacroMicroDriver> findByMacroDriverIdAndMicroDriverId(
        long macroDriverId, long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByMacroDriverIdAndMicroDriverId(macroDriverId,
            microDriverId);
    }

    /**
    * Returns a range of all the macro micro drivers where macroDriverId = &#63; and microDriverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @param start the lower bound of the range of macro micro drivers
    * @param end the upper bound of the range of macro micro drivers (not inclusive)
    * @return the range of matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MacroMicroDriver> findByMacroDriverIdAndMicroDriverId(
        long macroDriverId, long microDriverId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByMacroDriverIdAndMicroDriverId(macroDriverId,
            microDriverId, start, end);
    }

    /**
    * Returns an ordered range of all the macro micro drivers where macroDriverId = &#63; and microDriverId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @param start the lower bound of the range of macro micro drivers
    * @param end the upper bound of the range of macro micro drivers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MacroMicroDriver> findByMacroDriverIdAndMicroDriverId(
        long macroDriverId, long microDriverId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByMacroDriverIdAndMicroDriverId(macroDriverId,
            microDriverId, start, end, orderByComparator);
    }

    /**
    * Returns the first macro micro driver in the ordered set where macroDriverId = &#63; and microDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver findByMacroDriverIdAndMicroDriverId_First(
        long macroDriverId, long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByMacroDriverIdAndMicroDriverId_First(macroDriverId,
            microDriverId, orderByComparator);
    }

    /**
    * Returns the first macro micro driver in the ordered set where macroDriverId = &#63; and microDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching macro micro driver, or <code>null</code> if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver fetchByMacroDriverIdAndMicroDriverId_First(
        long macroDriverId, long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByMacroDriverIdAndMicroDriverId_First(macroDriverId,
            microDriverId, orderByComparator);
    }

    /**
    * Returns the last macro micro driver in the ordered set where macroDriverId = &#63; and microDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver findByMacroDriverIdAndMicroDriverId_Last(
        long macroDriverId, long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByMacroDriverIdAndMicroDriverId_Last(macroDriverId,
            microDriverId, orderByComparator);
    }

    /**
    * Returns the last macro micro driver in the ordered set where macroDriverId = &#63; and microDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching macro micro driver, or <code>null</code> if a matching macro micro driver could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver fetchByMacroDriverIdAndMicroDriverId_Last(
        long macroDriverId, long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByMacroDriverIdAndMicroDriverId_Last(macroDriverId,
            microDriverId, orderByComparator);
    }

    /**
    * Returns the macro micro drivers before and after the current macro micro driver in the ordered set where macroDriverId = &#63; and microDriverId = &#63;.
    *
    * @param macroMicroDriverId the primary key of the current macro micro driver
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next macro micro driver
    * @throws com.hp.omp.NoSuchMacroMicroDriverException if a macro micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.MacroMicroDriver[] findByMacroDriverIdAndMicroDriverId_PrevAndNext(
        long macroMicroDriverId, long macroDriverId, long microDriverId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchMacroMicroDriverException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByMacroDriverIdAndMicroDriverId_PrevAndNext(macroMicroDriverId,
            macroDriverId, microDriverId, orderByComparator);
    }

    /**
    * Returns all the macro micro drivers.
    *
    * @return the macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MacroMicroDriver> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the macro micro drivers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of macro micro drivers
    * @param end the upper bound of the range of macro micro drivers (not inclusive)
    * @return the range of macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MacroMicroDriver> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the macro micro drivers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of macro micro drivers
    * @param end the upper bound of the range of macro micro drivers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.MacroMicroDriver> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the macro micro drivers where macroDriverId = &#63; from the database.
    *
    * @param macroDriverId the macro driver ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByMacroDriverId(long macroDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByMacroDriverId(macroDriverId);
    }

    /**
    * Removes all the macro micro drivers where microDriverId = &#63; from the database.
    *
    * @param microDriverId the micro driver ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByMicroDriverId(long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByMicroDriverId(microDriverId);
    }

    /**
    * Removes all the macro micro drivers where macroDriverId = &#63; and microDriverId = &#63; from the database.
    *
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByMacroDriverIdAndMicroDriverId(
        long macroDriverId, long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByMacroDriverIdAndMicroDriverId(macroDriverId, microDriverId);
    }

    /**
    * Removes all the macro micro drivers from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of macro micro drivers where macroDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @return the number of matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static int countByMacroDriverId(long macroDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByMacroDriverId(macroDriverId);
    }

    /**
    * Returns the number of macro micro drivers where microDriverId = &#63;.
    *
    * @param microDriverId the micro driver ID
    * @return the number of matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static int countByMicroDriverId(long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByMicroDriverId(microDriverId);
    }

    /**
    * Returns the number of macro micro drivers where macroDriverId = &#63; and microDriverId = &#63;.
    *
    * @param macroDriverId the macro driver ID
    * @param microDriverId the micro driver ID
    * @return the number of matching macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static int countByMacroDriverIdAndMicroDriverId(long macroDriverId,
        long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByMacroDriverIdAndMicroDriverId(macroDriverId,
            microDriverId);
    }

    /**
    * Returns the number of macro micro drivers.
    *
    * @return the number of macro micro drivers
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static MacroMicroDriverPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (MacroMicroDriverPersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    MacroMicroDriverPersistence.class.getName());

            ReferenceRegistry.registerReference(MacroMicroDriverUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(MacroMicroDriverPersistence persistence) {
    }
}
