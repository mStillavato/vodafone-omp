package com.hp.omp.service.persistence;

import com.hp.omp.model.Buyer;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the buyer service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see BuyerPersistenceImpl
 * @see BuyerUtil
 * @generated
 */
public interface BuyerPersistence extends BasePersistence<Buyer> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link BuyerUtil} to access the buyer persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the buyer in the entity cache if it is enabled.
    *
    * @param buyer the buyer
    */
    public void cacheResult(com.hp.omp.model.Buyer buyer);

    /**
    * Caches the buyers in the entity cache if it is enabled.
    *
    * @param buyers the buyers
    */
    public void cacheResult(java.util.List<com.hp.omp.model.Buyer> buyers);

    /**
    * Creates a new buyer with the primary key. Does not add the buyer to the database.
    *
    * @param buyerId the primary key for the new buyer
    * @return the new buyer
    */
    public com.hp.omp.model.Buyer create(long buyerId);

    /**
    * Removes the buyer with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param buyerId the primary key of the buyer
    * @return the buyer that was removed
    * @throws com.hp.omp.NoSuchBuyerException if a buyer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Buyer remove(long buyerId)
        throws com.hp.omp.NoSuchBuyerException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.Buyer updateImpl(com.hp.omp.model.Buyer buyer,
        boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the buyer with the primary key or throws a {@link com.hp.omp.NoSuchBuyerException} if it could not be found.
    *
    * @param buyerId the primary key of the buyer
    * @return the buyer
    * @throws com.hp.omp.NoSuchBuyerException if a buyer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Buyer findByPrimaryKey(long buyerId)
        throws com.hp.omp.NoSuchBuyerException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the buyer with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param buyerId the primary key of the buyer
    * @return the buyer, or <code>null</code> if a buyer with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.Buyer fetchByPrimaryKey(long buyerId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the buyers.
    *
    * @return the buyers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.Buyer> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the buyers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of buyers
    * @param end the upper bound of the range of buyers (not inclusive)
    * @return the range of buyers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.Buyer> findAll(int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the buyers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of buyers
    * @param end the upper bound of the range of buyers (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of buyers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.Buyer> findAll(int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the buyers from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of buyers.
    *
    * @return the number of buyers
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
