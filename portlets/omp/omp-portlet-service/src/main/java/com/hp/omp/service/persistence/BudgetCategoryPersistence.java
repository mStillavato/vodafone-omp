package com.hp.omp.service.persistence;

import com.hp.omp.model.BudgetCategory;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the budget category service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see BudgetCategoryPersistenceImpl
 * @see BudgetCategoryUtil
 * @generated
 */
public interface BudgetCategoryPersistence extends BasePersistence<BudgetCategory> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link BudgetCategoryUtil} to access the budget category persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the budget category in the entity cache if it is enabled.
    *
    * @param budgetCategory the budget category
    */
    public void cacheResult(com.hp.omp.model.BudgetCategory budgetCategory);

    /**
    * Caches the budget categories in the entity cache if it is enabled.
    *
    * @param budgetCategories the budget categories
    */
    public void cacheResult(
        java.util.List<com.hp.omp.model.BudgetCategory> budgetCategories);

    /**
    * Creates a new budget category with the primary key. Does not add the budget category to the database.
    *
    * @param budgetCategoryId the primary key for the new budget category
    * @return the new budget category
    */
    public com.hp.omp.model.BudgetCategory create(long budgetCategoryId);

    /**
    * Removes the budget category with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param budgetCategoryId the primary key of the budget category
    * @return the budget category that was removed
    * @throws com.hp.omp.NoSuchBudgetCategoryException if a budget category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.BudgetCategory remove(long budgetCategoryId)
        throws com.hp.omp.NoSuchBudgetCategoryException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.BudgetCategory updateImpl(
        com.hp.omp.model.BudgetCategory budgetCategory, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the budget category with the primary key or throws a {@link com.hp.omp.NoSuchBudgetCategoryException} if it could not be found.
    *
    * @param budgetCategoryId the primary key of the budget category
    * @return the budget category
    * @throws com.hp.omp.NoSuchBudgetCategoryException if a budget category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.BudgetCategory findByPrimaryKey(
        long budgetCategoryId)
        throws com.hp.omp.NoSuchBudgetCategoryException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the budget category with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param budgetCategoryId the primary key of the budget category
    * @return the budget category, or <code>null</code> if a budget category with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.BudgetCategory fetchByPrimaryKey(
        long budgetCategoryId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the budget categories.
    *
    * @return the budget categories
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.BudgetCategory> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the budget categories.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of budget categories
    * @param end the upper bound of the range of budget categories (not inclusive)
    * @return the range of budget categories
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.BudgetCategory> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the budget categories.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of budget categories
    * @param end the upper bound of the range of budget categories (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of budget categories
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.BudgetCategory> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the budget categories from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of budget categories.
    *
    * @return the number of budget categories
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
