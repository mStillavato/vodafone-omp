package com.hp.omp.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author    HP Egypt team
 * @generated
 */
public class CostCenterSoap implements Serializable {
    private long _costCenterId;
    private String _costCenterName;
    private String _description;
    private boolean _display;

    public CostCenterSoap() {
    }

    public static CostCenterSoap toSoapModel(CostCenter model) {
        CostCenterSoap soapModel = new CostCenterSoap();

        soapModel.setCostCenterId(model.getCostCenterId());
        soapModel.setCostCenterName(model.getCostCenterName());
        soapModel.setDescription(model.getDescription());
        soapModel.setDisplay(model.getDisplay());

        return soapModel;
    }

    public static CostCenterSoap[] toSoapModels(CostCenter[] models) {
        CostCenterSoap[] soapModels = new CostCenterSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static CostCenterSoap[][] toSoapModels(CostCenter[][] models) {
        CostCenterSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new CostCenterSoap[models.length][models[0].length];
        } else {
            soapModels = new CostCenterSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static CostCenterSoap[] toSoapModels(List<CostCenter> models) {
        List<CostCenterSoap> soapModels = new ArrayList<CostCenterSoap>(models.size());

        for (CostCenter model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new CostCenterSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _costCenterId;
    }

    public void setPrimaryKey(long pk) {
        setCostCenterId(pk);
    }

    public long getCostCenterId() {
        return _costCenterId;
    }

    public void setCostCenterId(long costCenterId) {
        _costCenterId = costCenterId;
    }

    public String getCostCenterName() {
        return _costCenterName;
    }

    public void setCostCenterName(String costCenterName) {
        _costCenterName = costCenterName;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;
    }
}
