package com.hp.omp.service.persistence;

import com.hp.omp.model.ODNPName;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the o d n p name service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see ODNPNamePersistenceImpl
 * @see ODNPNameUtil
 * @generated
 */
public interface ODNPNamePersistence extends BasePersistence<ODNPName> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link ODNPNameUtil} to access the o d n p name persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the o d n p name in the entity cache if it is enabled.
    *
    * @param odnpName the o d n p name
    */
    public void cacheResult(com.hp.omp.model.ODNPName odnpName);

    /**
    * Caches the o d n p names in the entity cache if it is enabled.
    *
    * @param odnpNames the o d n p names
    */
    public void cacheResult(java.util.List<com.hp.omp.model.ODNPName> odnpNames);

    /**
    * Creates a new o d n p name with the primary key. Does not add the o d n p name to the database.
    *
    * @param odnpNameId the primary key for the new o d n p name
    * @return the new o d n p name
    */
    public com.hp.omp.model.ODNPName create(long odnpNameId);

    /**
    * Removes the o d n p name with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param odnpNameId the primary key of the o d n p name
    * @return the o d n p name that was removed
    * @throws com.hp.omp.NoSuchODNPNameException if a o d n p name with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ODNPName remove(long odnpNameId)
        throws com.hp.omp.NoSuchODNPNameException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.ODNPName updateImpl(
        com.hp.omp.model.ODNPName odnpName, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the o d n p name with the primary key or throws a {@link com.hp.omp.NoSuchODNPNameException} if it could not be found.
    *
    * @param odnpNameId the primary key of the o d n p name
    * @return the o d n p name
    * @throws com.hp.omp.NoSuchODNPNameException if a o d n p name with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ODNPName findByPrimaryKey(long odnpNameId)
        throws com.hp.omp.NoSuchODNPNameException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the o d n p name with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param odnpNameId the primary key of the o d n p name
    * @return the o d n p name, or <code>null</code> if a o d n p name with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.ODNPName fetchByPrimaryKey(long odnpNameId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the o d n p names.
    *
    * @return the o d n p names
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.ODNPName> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the o d n p names.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of o d n p names
    * @param end the upper bound of the range of o d n p names (not inclusive)
    * @return the range of o d n p names
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.ODNPName> findAll(int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the o d n p names.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of o d n p names
    * @param end the upper bound of the range of o d n p names (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of o d n p names
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.ODNPName> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the o d n p names from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of o d n p names.
    *
    * @return the number of o d n p names
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
