package com.hp.omp.service;

import java.util.List;

import com.hp.omp.model.MacroDriver;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link MacroDriverLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       MacroDriverLocalService
 * @generated
 */
public class MacroDriverLocalServiceWrapper implements MacroDriverLocalService,
    ServiceWrapper<MacroDriverLocalService> {
    private MacroDriverLocalService _macroDriverLocalService;

    public MacroDriverLocalServiceWrapper(
        MacroDriverLocalService macroDriverLocalService) {
        _macroDriverLocalService = macroDriverLocalService;
    }

    /**
    * Adds the macro driver to the database. Also notifies the appropriate model listeners.
    *
    * @param macroDriver the macro driver
    * @return the macro driver that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroDriver addMacroDriver(
        com.hp.omp.model.MacroDriver macroDriver)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroDriverLocalService.addMacroDriver(macroDriver);
    }

    /**
    * Creates a new macro driver with the primary key. Does not add the macro driver to the database.
    *
    * @param macroDriverId the primary key for the new macro driver
    * @return the new macro driver
    */
    public com.hp.omp.model.MacroDriver createMacroDriver(long macroDriverId) {
        return _macroDriverLocalService.createMacroDriver(macroDriverId);
    }

    /**
    * Deletes the macro driver with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param macroDriverId the primary key of the macro driver
    * @return the macro driver that was removed
    * @throws PortalException if a macro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroDriver deleteMacroDriver(long macroDriverId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _macroDriverLocalService.deleteMacroDriver(macroDriverId);
    }

    /**
    * Deletes the macro driver from the database. Also notifies the appropriate model listeners.
    *
    * @param macroDriver the macro driver
    * @return the macro driver that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroDriver deleteMacroDriver(
        com.hp.omp.model.MacroDriver macroDriver)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroDriverLocalService.deleteMacroDriver(macroDriver);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _macroDriverLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroDriverLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _macroDriverLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroDriverLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroDriverLocalService.dynamicQueryCount(dynamicQuery);
    }

    public com.hp.omp.model.MacroDriver fetchMacroDriver(long macroDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroDriverLocalService.fetchMacroDriver(macroDriverId);
    }

    /**
    * Returns the macro driver with the primary key.
    *
    * @param macroDriverId the primary key of the macro driver
    * @return the macro driver
    * @throws PortalException if a macro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroDriver getMacroDriver(long macroDriverId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _macroDriverLocalService.getMacroDriver(macroDriverId);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _macroDriverLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the macro drivers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of macro drivers
    * @param end the upper bound of the range of macro drivers (not inclusive)
    * @return the range of macro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MacroDriver> getMacroDrivers(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroDriverLocalService.getMacroDrivers(start, end);
    }

    /**
    * Returns the number of macro drivers.
    *
    * @return the number of macro drivers
    * @throws SystemException if a system exception occurred
    */
    public int getMacroDriversCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroDriverLocalService.getMacroDriversCount();
    }

    /**
    * Updates the macro driver in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param macroDriver the macro driver
    * @return the macro driver that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroDriver updateMacroDriver(
        com.hp.omp.model.MacroDriver macroDriver)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroDriverLocalService.updateMacroDriver(macroDriver);
    }

    /**
    * Updates the macro driver in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param macroDriver the macro driver
    * @param merge whether to merge the macro driver with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the macro driver that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MacroDriver updateMacroDriver(
        com.hp.omp.model.MacroDriver macroDriver, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroDriverLocalService.updateMacroDriver(macroDriver, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _macroDriverLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _macroDriverLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _macroDriverLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    public com.hp.omp.model.MacroDriver getMacroDriverByName(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _macroDriverLocalService.getMacroDriverByName(name);
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public MacroDriverLocalService getWrappedMacroDriverLocalService() {
        return _macroDriverLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedMacroDriverLocalService(
        MacroDriverLocalService macroDriverLocalService) {
        _macroDriverLocalService = macroDriverLocalService;
    }

    public MacroDriverLocalService getWrappedService() {
        return _macroDriverLocalService;
    }

    public void setWrappedService(
        MacroDriverLocalService macroDriverLocalService) {
        _macroDriverLocalService = macroDriverLocalService;
    }

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<MacroDriver> getMacroDrivers(int start, int end,
			String searchFilter) throws SystemException {
        return _macroDriverLocalService.getMacroDrivers(start, end, searchFilter);
	}
}
