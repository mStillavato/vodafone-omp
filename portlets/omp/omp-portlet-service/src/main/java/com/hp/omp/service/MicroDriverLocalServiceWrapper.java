package com.hp.omp.service;

import java.util.List;

import com.hp.omp.model.MacroDriver;
import com.hp.omp.model.MicroDriver;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link MicroDriverLocalService}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       MicroDriverLocalService
 * @generated
 */
public class MicroDriverLocalServiceWrapper implements MicroDriverLocalService,
    ServiceWrapper<MicroDriverLocalService> {
    private MicroDriverLocalService _microDriverLocalService;

    public MicroDriverLocalServiceWrapper(
        MicroDriverLocalService microDriverLocalService) {
        _microDriverLocalService = microDriverLocalService;
    }

    /**
    * Adds the micro driver to the database. Also notifies the appropriate model listeners.
    *
    * @param microDriver the micro driver
    * @return the micro driver that was added
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MicroDriver addMicroDriver(
        com.hp.omp.model.MicroDriver microDriver)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _microDriverLocalService.addMicroDriver(microDriver);
    }

    /**
    * Creates a new micro driver with the primary key. Does not add the micro driver to the database.
    *
    * @param microDriverId the primary key for the new micro driver
    * @return the new micro driver
    */
    public com.hp.omp.model.MicroDriver createMicroDriver(long microDriverId) {
        return _microDriverLocalService.createMicroDriver(microDriverId);
    }

    /**
    * Deletes the micro driver with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param microDriverId the primary key of the micro driver
    * @return the micro driver that was removed
    * @throws PortalException if a micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MicroDriver deleteMicroDriver(long microDriverId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _microDriverLocalService.deleteMicroDriver(microDriverId);
    }

    /**
    * Deletes the micro driver from the database. Also notifies the appropriate model listeners.
    *
    * @param microDriver the micro driver
    * @return the micro driver that was removed
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MicroDriver deleteMicroDriver(
        com.hp.omp.model.MicroDriver microDriver)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _microDriverLocalService.deleteMicroDriver(microDriver);
    }

    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _microDriverLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _microDriverLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _microDriverLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _microDriverLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _microDriverLocalService.dynamicQueryCount(dynamicQuery);
    }

    public com.hp.omp.model.MicroDriver fetchMicroDriver(long microDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _microDriverLocalService.fetchMicroDriver(microDriverId);
    }

    /**
    * Returns the micro driver with the primary key.
    *
    * @param microDriverId the primary key of the micro driver
    * @return the micro driver
    * @throws PortalException if a micro driver with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MicroDriver getMicroDriver(long microDriverId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _microDriverLocalService.getMicroDriver(microDriverId);
    }

    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _microDriverLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the micro drivers.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of micro drivers
    * @param end the upper bound of the range of micro drivers (not inclusive)
    * @return the range of micro drivers
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.MicroDriver> getMicroDrivers(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _microDriverLocalService.getMicroDrivers(start, end);
    }

    /**
    * Returns the number of micro drivers.
    *
    * @return the number of micro drivers
    * @throws SystemException if a system exception occurred
    */
    public int getMicroDriversCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _microDriverLocalService.getMicroDriversCount();
    }

    /**
    * Updates the micro driver in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param microDriver the micro driver
    * @return the micro driver that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MicroDriver updateMicroDriver(
        com.hp.omp.model.MicroDriver microDriver)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _microDriverLocalService.updateMicroDriver(microDriver);
    }

    /**
    * Updates the micro driver in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param microDriver the micro driver
    * @param merge whether to merge the micro driver with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
    * @return the micro driver that was updated
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.MicroDriver updateMicroDriver(
        com.hp.omp.model.MicroDriver microDriver, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _microDriverLocalService.updateMicroDriver(microDriver, merge);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier() {
        return _microDriverLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _microDriverLocalService.setBeanIdentifier(beanIdentifier);
    }

    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _microDriverLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    public java.util.List<com.hp.omp.model.MicroDriver> getMicroDriverByMacroDriverId(
        java.lang.Long macroDriverId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _microDriverLocalService.getMicroDriverByMacroDriverId(macroDriverId);
    }

    public com.hp.omp.model.MicroDriver getMicroDriverByName(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _microDriverLocalService.getMicroDriverByName(name);
    }

    /**
     * @deprecated Renamed to {@link #getWrappedService}
     */
    public MicroDriverLocalService getWrappedMicroDriverLocalService() {
        return _microDriverLocalService;
    }

    /**
     * @deprecated Renamed to {@link #setWrappedService}
     */
    public void setWrappedMicroDriverLocalService(
        MicroDriverLocalService microDriverLocalService) {
        _microDriverLocalService = microDriverLocalService;
    }

    public MicroDriverLocalService getWrappedService() {
        return _microDriverLocalService;
    }

    public void setWrappedService(
        MicroDriverLocalService microDriverLocalService) {
        _microDriverLocalService = microDriverLocalService;
    }
    
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<MicroDriver> getMicroDriversList(int start, int end,
			String searchFilter) throws SystemException {
        return _microDriverLocalService.getMicroDriversList(start, end, searchFilter);
	}
}
