package com.hp.omp.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the ODNPName service. Represents a row in the &quot;ODNP_NAME&quot; database table, with each column mapped to a property of this class.
 *
 * @author HP Egypt team
 * @see ODNPNameModel
 * @see com.hp.omp.model.impl.ODNPNameImpl
 * @see com.hp.omp.model.impl.ODNPNameModelImpl
 * @generated
 */
public interface ODNPName extends ODNPNameModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link com.hp.omp.model.impl.ODNPNameImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
