package com.hp.omp.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link TrackingCodes}.
 * </p>
 *
 * @author    HP Egypt team
 * @see       TrackingCodes
 * @generated
 */
public class TrackingCodesWrapper implements TrackingCodes,
    ModelWrapper<TrackingCodes> {
    private TrackingCodes _trackingCodes;

    public TrackingCodesWrapper(TrackingCodes trackingCodes) {
        _trackingCodes = trackingCodes;
    }

    public Class<?> getModelClass() {
        return TrackingCodes.class;
    }

    public String getModelClassName() {
        return TrackingCodes.class.getName();
    }

    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("trackingCodeId", getTrackingCodeId());
        attributes.put("trackingCodeName", getTrackingCodeName());
        attributes.put("trackingCodeDescription", getTrackingCodeDescription());
        attributes.put("projectId", getProjectId());
        attributes.put("subProjectId", getSubProjectId());
        attributes.put("budgetCategoryId", getBudgetCategoryId());
        attributes.put("budgetSubCategoryId", getBudgetSubCategoryId());
        attributes.put("macroDriverId", getMacroDriverId());
        attributes.put("microDriverId", getMicroDriverId());
        attributes.put("odnpNameId", getOdnpNameId());
        attributes.put("wbsCodeId", getWbsCodeId());
        attributes.put("display", getDisplay());

        return attributes;
    }

    public void setModelAttributes(Map<String, Object> attributes) {
        Long trackingCodeId = (Long) attributes.get("trackingCodeId");

        if (trackingCodeId != null) {
            setTrackingCodeId(trackingCodeId);
        }

        String trackingCodeName = (String) attributes.get("trackingCodeName");

        if (trackingCodeName != null) {
            setTrackingCodeName(trackingCodeName);
        }

        String trackingCodeDescription = (String) attributes.get(
                "trackingCodeDescription");

        if (trackingCodeDescription != null) {
            setTrackingCodeDescription(trackingCodeDescription);
        }

        String projectId = (String) attributes.get("projectId");

        if (projectId != null) {
            setProjectId(projectId);
        }

        String subProjectId = (String) attributes.get("subProjectId");

        if (subProjectId != null) {
            setSubProjectId(subProjectId);
        }

        String budgetCategoryId = (String) attributes.get("budgetCategoryId");

        if (budgetCategoryId != null) {
            setBudgetCategoryId(budgetCategoryId);
        }

        String budgetSubCategoryId = (String) attributes.get(
                "budgetSubCategoryId");

        if (budgetSubCategoryId != null) {
            setBudgetSubCategoryId(budgetSubCategoryId);
        }

        String macroDriverId = (String) attributes.get("macroDriverId");

        if (macroDriverId != null) {
            setMacroDriverId(macroDriverId);
        }

        String microDriverId = (String) attributes.get("microDriverId");

        if (microDriverId != null) {
            setMicroDriverId(microDriverId);
        }

        String odnpNameId = (String) attributes.get("odnpNameId");

        if (odnpNameId != null) {
            setOdnpNameId(odnpNameId);
        }

        String wbsCodeId = (String) attributes.get("wbsCodeId");

        if (wbsCodeId != null) {
            setWbsCodeId(wbsCodeId);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
    }

    /**
    * Returns the primary key of this tracking codes.
    *
    * @return the primary key of this tracking codes
    */
    public long getPrimaryKey() {
        return _trackingCodes.getPrimaryKey();
    }

    /**
    * Sets the primary key of this tracking codes.
    *
    * @param primaryKey the primary key of this tracking codes
    */
    public void setPrimaryKey(long primaryKey) {
        _trackingCodes.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the tracking code ID of this tracking codes.
    *
    * @return the tracking code ID of this tracking codes
    */
    public long getTrackingCodeId() {
        return _trackingCodes.getTrackingCodeId();
    }

    /**
    * Sets the tracking code ID of this tracking codes.
    *
    * @param trackingCodeId the tracking code ID of this tracking codes
    */
    public void setTrackingCodeId(long trackingCodeId) {
        _trackingCodes.setTrackingCodeId(trackingCodeId);
    }

    /**
    * Returns the tracking code name of this tracking codes.
    *
    * @return the tracking code name of this tracking codes
    */
    public java.lang.String getTrackingCodeName() {
        return _trackingCodes.getTrackingCodeName();
    }

    /**
    * Sets the tracking code name of this tracking codes.
    *
    * @param trackingCodeName the tracking code name of this tracking codes
    */
    public void setTrackingCodeName(java.lang.String trackingCodeName) {
        _trackingCodes.setTrackingCodeName(trackingCodeName);
    }

    /**
    * Returns the tracking code description of this tracking codes.
    *
    * @return the tracking code description of this tracking codes
    */
    public java.lang.String getTrackingCodeDescription() {
        return _trackingCodes.getTrackingCodeDescription();
    }

    /**
    * Sets the tracking code description of this tracking codes.
    *
    * @param trackingCodeDescription the tracking code description of this tracking codes
    */
    public void setTrackingCodeDescription(
        java.lang.String trackingCodeDescription) {
        _trackingCodes.setTrackingCodeDescription(trackingCodeDescription);
    }

    /**
    * Returns the project ID of this tracking codes.
    *
    * @return the project ID of this tracking codes
    */
    public java.lang.String getProjectId() {
        return _trackingCodes.getProjectId();
    }

    /**
    * Sets the project ID of this tracking codes.
    *
    * @param projectId the project ID of this tracking codes
    */
    public void setProjectId(java.lang.String projectId) {
        _trackingCodes.setProjectId(projectId);
    }

    /**
    * Returns the sub project ID of this tracking codes.
    *
    * @return the sub project ID of this tracking codes
    */
    public java.lang.String getSubProjectId() {
        return _trackingCodes.getSubProjectId();
    }

    /**
    * Sets the sub project ID of this tracking codes.
    *
    * @param subProjectId the sub project ID of this tracking codes
    */
    public void setSubProjectId(java.lang.String subProjectId) {
        _trackingCodes.setSubProjectId(subProjectId);
    }

    /**
    * Returns the budget category ID of this tracking codes.
    *
    * @return the budget category ID of this tracking codes
    */
    public java.lang.String getBudgetCategoryId() {
        return _trackingCodes.getBudgetCategoryId();
    }

    /**
    * Sets the budget category ID of this tracking codes.
    *
    * @param budgetCategoryId the budget category ID of this tracking codes
    */
    public void setBudgetCategoryId(java.lang.String budgetCategoryId) {
        _trackingCodes.setBudgetCategoryId(budgetCategoryId);
    }

    /**
    * Returns the budget sub category ID of this tracking codes.
    *
    * @return the budget sub category ID of this tracking codes
    */
    public java.lang.String getBudgetSubCategoryId() {
        return _trackingCodes.getBudgetSubCategoryId();
    }

    /**
    * Sets the budget sub category ID of this tracking codes.
    *
    * @param budgetSubCategoryId the budget sub category ID of this tracking codes
    */
    public void setBudgetSubCategoryId(java.lang.String budgetSubCategoryId) {
        _trackingCodes.setBudgetSubCategoryId(budgetSubCategoryId);
    }

    /**
    * Returns the macro driver ID of this tracking codes.
    *
    * @return the macro driver ID of this tracking codes
    */
    public java.lang.String getMacroDriverId() {
        return _trackingCodes.getMacroDriverId();
    }

    /**
    * Sets the macro driver ID of this tracking codes.
    *
    * @param macroDriverId the macro driver ID of this tracking codes
    */
    public void setMacroDriverId(java.lang.String macroDriverId) {
        _trackingCodes.setMacroDriverId(macroDriverId);
    }

    /**
    * Returns the micro driver ID of this tracking codes.
    *
    * @return the micro driver ID of this tracking codes
    */
    public java.lang.String getMicroDriverId() {
        return _trackingCodes.getMicroDriverId();
    }

    /**
    * Sets the micro driver ID of this tracking codes.
    *
    * @param microDriverId the micro driver ID of this tracking codes
    */
    public void setMicroDriverId(java.lang.String microDriverId) {
        _trackingCodes.setMicroDriverId(microDriverId);
    }

    /**
    * Returns the odnp name ID of this tracking codes.
    *
    * @return the odnp name ID of this tracking codes
    */
    public java.lang.String getOdnpNameId() {
        return _trackingCodes.getOdnpNameId();
    }

    /**
    * Sets the odnp name ID of this tracking codes.
    *
    * @param odnpNameId the odnp name ID of this tracking codes
    */
    public void setOdnpNameId(java.lang.String odnpNameId) {
        _trackingCodes.setOdnpNameId(odnpNameId);
    }

    /**
    * Returns the wbs code ID of this tracking codes.
    *
    * @return the wbs code ID of this tracking codes
    */
    public java.lang.String getWbsCodeId() {
        return _trackingCodes.getWbsCodeId();
    }

    /**
    * Sets the wbs code ID of this tracking codes.
    *
    * @param wbsCodeId the wbs code ID of this tracking codes
    */
    public void setWbsCodeId(java.lang.String wbsCodeId) {
        _trackingCodes.setWbsCodeId(wbsCodeId);
    }

    /**
    * Returns the display of this tracking codes.
    *
    * @return the display of this tracking codes
    */
    public boolean getDisplay() {
        return _trackingCodes.getDisplay();
    }

    /**
    * Returns <code>true</code> if this tracking codes is display.
    *
    * @return <code>true</code> if this tracking codes is display; <code>false</code> otherwise
    */
    public boolean isDisplay() {
        return _trackingCodes.isDisplay();
    }

    /**
    * Sets whether this tracking codes is display.
    *
    * @param display the display of this tracking codes
    */
    public void setDisplay(boolean display) {
        _trackingCodes.setDisplay(display);
    }

    public boolean isNew() {
        return _trackingCodes.isNew();
    }

    public void setNew(boolean n) {
        _trackingCodes.setNew(n);
    }

    public boolean isCachedModel() {
        return _trackingCodes.isCachedModel();
    }

    public void setCachedModel(boolean cachedModel) {
        _trackingCodes.setCachedModel(cachedModel);
    }

    public boolean isEscapedModel() {
        return _trackingCodes.isEscapedModel();
    }

    public java.io.Serializable getPrimaryKeyObj() {
        return _trackingCodes.getPrimaryKeyObj();
    }

    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _trackingCodes.setPrimaryKeyObj(primaryKeyObj);
    }

    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _trackingCodes.getExpandoBridge();
    }

    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _trackingCodes.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new TrackingCodesWrapper((TrackingCodes) _trackingCodes.clone());
    }

    public int compareTo(TrackingCodes trackingCodes) {
        return _trackingCodes.compareTo(trackingCodes);
    }

    @Override
    public int hashCode() {
        return _trackingCodes.hashCode();
    }

    public com.liferay.portal.model.CacheModel<TrackingCodes> toCacheModel() {
        return _trackingCodes.toCacheModel();
    }

    public TrackingCodes toEscapedModel() {
        return new TrackingCodesWrapper(_trackingCodes.toEscapedModel());
    }

    public TrackingCodes toUnescapedModel() {
        return new TrackingCodesWrapper(_trackingCodes.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _trackingCodes.toString();
    }

    public java.lang.String toXmlString() {
        return _trackingCodes.toXmlString();
    }

    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _trackingCodes.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof TrackingCodesWrapper)) {
            return false;
        }

        TrackingCodesWrapper trackingCodesWrapper = (TrackingCodesWrapper) obj;

        if (Validator.equals(_trackingCodes, trackingCodesWrapper._trackingCodes)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated Renamed to {@link #getWrappedModel}
     */
    public TrackingCodes getWrappedTrackingCodes() {
        return _trackingCodes;
    }

    public TrackingCodes getWrappedModel() {
        return _trackingCodes;
    }

    public void resetOriginalValues() {
        _trackingCodes.resetOriginalValues();
    }
}
