/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.model;

import com.hp.omp.service.CatalogoLocalServiceUtil;
import com.hp.omp.service.ClpSerializer;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author HP Egypt team
 */
public class CatalogoClp extends BaseModelImpl<Catalogo> implements Catalogo {
	public CatalogoClp() {
	}

	public Class<?> getModelClass() {
		return Catalogo.class;
	}

	public String getModelClassName() {
		return Catalogo.class.getName();
	}

	public long getPrimaryKey() {
		return _catalogoId;
	}

	public void setPrimaryKey(long primaryKey) {
		setCatalogoId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_catalogoId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("catalogoId", getCatalogoId());
		attributes.put("chiaveInforecord", getChiaveInforecord());
		attributes.put("dataCreazione", getDataCreazione());
		attributes.put("dataFineValidita", getDataFineValidita());
		attributes.put("divisione", getDivisione());
		attributes.put("flgCanc", getFlgCanc());
		attributes.put("fornFinCode", getFornFinCode());
		attributes.put("fornitoreCode", getFornitoreCode());
		attributes.put("fornitoreDesc", getFornitoreDesc());
		attributes.put("matCatLvl2", getMatCatLvl2());
		attributes.put("matCatLvl2Desc", getMatCatLvl2Desc());
		attributes.put("matCatLvl4", getMatCatLvl4());
		attributes.put("matCatLvl4Desc", getMatCatLvl4Desc());
		attributes.put("matCod", getMatCod());
		attributes.put("matCodFornFinale", getMatCodFornFinale());
		attributes.put("matDesc", getMatDesc());
		attributes.put("prezzo", getPrezzo());
		attributes.put("purchOrganiz", getPurchOrganiz());
		attributes.put("um", getUm());
		attributes.put("umPrezzo", getUmPrezzo());
		attributes.put("umPrezzoPo", getUmPrezzoPo());
		attributes.put("valuta", getValuta());
		attributes.put("upperMatDesc", getUpperMatDesc());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long catalogoId = (Long)attributes.get("catalogoId");

		if (catalogoId != null) {
			setCatalogoId(catalogoId);
		}

		String chiaveInforecord = (String)attributes.get("chiaveInforecord");

		if (chiaveInforecord != null) {
			setChiaveInforecord(chiaveInforecord);
		}

		Date dataCreazione = (Date)attributes.get("dataCreazione");

		if (dataCreazione != null) {
			setDataCreazione(dataCreazione);
		}

		Date dataFineValidita = (Date)attributes.get("dataFineValidita");

		if (dataFineValidita != null) {
			setDataFineValidita(dataFineValidita);
		}

		String divisione = (String)attributes.get("divisione");

		if (divisione != null) {
			setDivisione(divisione);
		}

		Boolean flgCanc = (Boolean)attributes.get("flgCanc");

		if (flgCanc != null) {
			setFlgCanc(flgCanc);
		}

		String fornFinCode = (String)attributes.get("fornFinCode");

		if (fornFinCode != null) {
			setFornFinCode(fornFinCode);
		}

		String fornitoreCode = (String)attributes.get("fornitoreCode");

		if (fornitoreCode != null) {
			setFornitoreCode(fornitoreCode);
		}

		String fornitoreDesc = (String)attributes.get("fornitoreDesc");

		if (fornitoreDesc != null) {
			setFornitoreDesc(fornitoreDesc);
		}

		String matCatLvl2 = (String)attributes.get("matCatLvl2");

		if (matCatLvl2 != null) {
			setMatCatLvl2(matCatLvl2);
		}

		String matCatLvl2Desc = (String)attributes.get("matCatLvl2Desc");

		if (matCatLvl2Desc != null) {
			setMatCatLvl2Desc(matCatLvl2Desc);
		}

		String matCatLvl4 = (String)attributes.get("matCatLvl4");

		if (matCatLvl4 != null) {
			setMatCatLvl4(matCatLvl4);
		}

		String matCatLvl4Desc = (String)attributes.get("matCatLvl4Desc");

		if (matCatLvl4Desc != null) {
			setMatCatLvl4Desc(matCatLvl4Desc);
		}

		String matCod = (String)attributes.get("matCod");

		if (matCod != null) {
			setMatCod(matCod);
		}

		String matCodFornFinale = (String)attributes.get("matCodFornFinale");

		if (matCodFornFinale != null) {
			setMatCodFornFinale(matCodFornFinale);
		}

		String matDesc = (String)attributes.get("matDesc");

		if (matDesc != null) {
			setMatDesc(matDesc);
		}

		String prezzo = (String)attributes.get("prezzo");

		if (prezzo != null) {
			setPrezzo(prezzo);
		}

		String purchOrganiz = (String)attributes.get("purchOrganiz");

		if (purchOrganiz != null) {
			setPurchOrganiz(purchOrganiz);
		}

		String um = (String)attributes.get("um");

		if (um != null) {
			setUm(um);
		}

		String umPrezzo = (String)attributes.get("umPrezzo");

		if (umPrezzo != null) {
			setUmPrezzo(umPrezzo);
		}

		String umPrezzoPo = (String)attributes.get("umPrezzoPo");

		if (umPrezzoPo != null) {
			setUmPrezzoPo(umPrezzoPo);
		}

		String valuta = (String)attributes.get("valuta");

		if (valuta != null) {
			setValuta(valuta);
		}

		String upperMatDesc = (String)attributes.get("upperMatDesc");

		if (upperMatDesc != null) {
			setUpperMatDesc(upperMatDesc);
		}
	}

	public long getCatalogoId() {
		return _catalogoId;
	}

	public void setCatalogoId(long catalogoId) {
		_catalogoId = catalogoId;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setCatalogoId", long.class);

				method.invoke(_catalogoRemoteModel, catalogoId);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getChiaveInforecord() {
		return _chiaveInforecord;
	}

	public void setChiaveInforecord(String chiaveInforecord) {
		_chiaveInforecord = chiaveInforecord;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setChiaveInforecord",
						String.class);

				method.invoke(_catalogoRemoteModel, chiaveInforecord);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public Date getDataCreazione() {
		return _dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		_dataCreazione = dataCreazione;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setDataCreazione", Date.class);

				method.invoke(_catalogoRemoteModel, dataCreazione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public Date getDataFineValidita() {
		return _dataFineValidita;
	}

	public void setDataFineValidita(Date dataFineValidita) {
		_dataFineValidita = dataFineValidita;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setDataFineValidita",
						Date.class);

				method.invoke(_catalogoRemoteModel, dataFineValidita);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getDivisione() {
		return _divisione;
	}

	public void setDivisione(String divisione) {
		_divisione = divisione;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setDivisione", String.class);

				method.invoke(_catalogoRemoteModel, divisione);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public boolean getFlgCanc() {
		return _flgCanc;
	}

	public boolean isFlgCanc() {
		return _flgCanc;
	}

	public void setFlgCanc(boolean flgCanc) {
		_flgCanc = flgCanc;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setFlgCanc", boolean.class);

				method.invoke(_catalogoRemoteModel, flgCanc);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getFornFinCode() {
		return _fornFinCode;
	}

	public void setFornFinCode(String fornFinCode) {
		_fornFinCode = fornFinCode;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setFornFinCode", String.class);

				method.invoke(_catalogoRemoteModel, fornFinCode);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getFornitoreCode() {
		return _fornitoreCode;
	}

	public void setFornitoreCode(String fornitoreCode) {
		_fornitoreCode = fornitoreCode;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setFornitoreCode", String.class);

				method.invoke(_catalogoRemoteModel, fornitoreCode);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getFornitoreDesc() {
		return _fornitoreDesc;
	}

	public void setFornitoreDesc(String fornitoreDesc) {
		_fornitoreDesc = fornitoreDesc;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setFornitoreDesc", String.class);

				method.invoke(_catalogoRemoteModel, fornitoreDesc);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getMatCatLvl2() {
		return _matCatLvl2;
	}

	public void setMatCatLvl2(String matCatLvl2) {
		_matCatLvl2 = matCatLvl2;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setMatCatLvl2", String.class);

				method.invoke(_catalogoRemoteModel, matCatLvl2);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getMatCatLvl2Desc() {
		return _matCatLvl2Desc;
	}

	public void setMatCatLvl2Desc(String matCatLvl2Desc) {
		_matCatLvl2Desc = matCatLvl2Desc;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setMatCatLvl2Desc",
						String.class);

				method.invoke(_catalogoRemoteModel, matCatLvl2Desc);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getMatCatLvl4() {
		return _matCatLvl4;
	}

	public void setMatCatLvl4(String matCatLvl4) {
		_matCatLvl4 = matCatLvl4;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setMatCatLvl4", String.class);

				method.invoke(_catalogoRemoteModel, matCatLvl4);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getMatCatLvl4Desc() {
		return _matCatLvl4Desc;
	}

	public void setMatCatLvl4Desc(String matCatLvl4Desc) {
		_matCatLvl4Desc = matCatLvl4Desc;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setMatCatLvl4Desc",
						String.class);

				method.invoke(_catalogoRemoteModel, matCatLvl4Desc);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getMatCod() {
		return _matCod;
	}

	public void setMatCod(String matCod) {
		_matCod = matCod;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setMatCod", String.class);

				method.invoke(_catalogoRemoteModel, matCod);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getMatCodFornFinale() {
		return _matCodFornFinale;
	}

	public void setMatCodFornFinale(String matCodFornFinale) {
		_matCodFornFinale = matCodFornFinale;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setMatCodFornFinale",
						String.class);

				method.invoke(_catalogoRemoteModel, matCodFornFinale);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getMatDesc() {
		return _matDesc;
	}

	public void setMatDesc(String matDesc) {
		_matDesc = matDesc;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setMatDesc", String.class);

				method.invoke(_catalogoRemoteModel, matDesc);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getPrezzo() {
		return _prezzo;
	}

	public void setPrezzo(String prezzo) {
		_prezzo = prezzo;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setPrezzo", String.class);

				method.invoke(_catalogoRemoteModel, prezzo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getPurchOrganiz() {
		return _purchOrganiz;
	}

	public void setPurchOrganiz(String purchOrganiz) {
		_purchOrganiz = purchOrganiz;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setPurchOrganiz", String.class);

				method.invoke(_catalogoRemoteModel, purchOrganiz);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getUm() {
		return _um;
	}

	public void setUm(String um) {
		_um = um;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setUm", String.class);

				method.invoke(_catalogoRemoteModel, um);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getUmPrezzo() {
		return _umPrezzo;
	}

	public void setUmPrezzo(String umPrezzo) {
		_umPrezzo = umPrezzo;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setUmPrezzo", String.class);

				method.invoke(_catalogoRemoteModel, umPrezzo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getUmPrezzoPo() {
		return _umPrezzoPo;
	}

	public void setUmPrezzoPo(String umPrezzoPo) {
		_umPrezzoPo = umPrezzoPo;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setUmPrezzoPo", String.class);

				method.invoke(_catalogoRemoteModel, umPrezzoPo);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getValuta() {
		return _valuta;
	}

	public void setValuta(String valuta) {
		_valuta = valuta;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setValuta", String.class);

				method.invoke(_catalogoRemoteModel, valuta);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public String getUpperMatDesc() {
		return _upperMatDesc;
	}

	public void setUpperMatDesc(String upperMatDesc) {
		_upperMatDesc = upperMatDesc;

		if (_catalogoRemoteModel != null) {
			try {
				Class<?> clazz = _catalogoRemoteModel.getClass();

				Method method = clazz.getMethod("setUpperMatDesc", String.class);

				method.invoke(_catalogoRemoteModel, upperMatDesc);
			}
			catch (Exception e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}

	public BaseModel<?> getCatalogoRemoteModel() {
		return _catalogoRemoteModel;
	}

	public void setCatalogoRemoteModel(BaseModel<?> catalogoRemoteModel) {
		_catalogoRemoteModel = catalogoRemoteModel;
	}

	public Object invokeOnRemoteModel(String methodName,
		Class<?>[] parameterTypes, Object[] parameterValues)
		throws Exception {
		Object[] remoteParameterValues = new Object[parameterValues.length];

		for (int i = 0; i < parameterValues.length; i++) {
			if (parameterValues[i] != null) {
				remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
			}
		}

		Class<?> remoteModelClass = _catalogoRemoteModel.getClass();

		ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

		Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

		for (int i = 0; i < parameterTypes.length; i++) {
			if (parameterTypes[i].isPrimitive()) {
				remoteParameterTypes[i] = parameterTypes[i];
			}
			else {
				String parameterTypeName = parameterTypes[i].getName();

				remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
			}
		}

		Method method = remoteModelClass.getMethod(methodName,
				remoteParameterTypes);

		Object returnValue = method.invoke(_catalogoRemoteModel,
				remoteParameterValues);

		if (returnValue != null) {
			returnValue = ClpSerializer.translateOutput(returnValue);
		}

		return returnValue;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			CatalogoLocalServiceUtil.addCatalogo(this);
		}
		else {
			CatalogoLocalServiceUtil.updateCatalogo(this);
		}
	}

	@Override
	public Catalogo toEscapedModel() {
		return (Catalogo)ProxyUtil.newProxyInstance(Catalogo.class.getClassLoader(),
			new Class[] { Catalogo.class }, new AutoEscapeBeanHandler(this));
	}

	public Catalogo toUnescapedModel() {
		return this;
	}

	@Override
	public Object clone() {
		CatalogoClp clone = new CatalogoClp();

		clone.setCatalogoId(getCatalogoId());
		clone.setChiaveInforecord(getChiaveInforecord());
		clone.setDataCreazione(getDataCreazione());
		clone.setDataFineValidita(getDataFineValidita());
		clone.setDivisione(getDivisione());
		clone.setFlgCanc(getFlgCanc());
		clone.setFornFinCode(getFornFinCode());
		clone.setFornitoreCode(getFornitoreCode());
		clone.setFornitoreDesc(getFornitoreDesc());
		clone.setMatCatLvl2(getMatCatLvl2());
		clone.setMatCatLvl2Desc(getMatCatLvl2Desc());
		clone.setMatCatLvl4(getMatCatLvl4());
		clone.setMatCatLvl4Desc(getMatCatLvl4Desc());
		clone.setMatCod(getMatCod());
		clone.setMatCodFornFinale(getMatCodFornFinale());
		clone.setMatDesc(getMatDesc());
		clone.setPrezzo(getPrezzo());
		clone.setPurchOrganiz(getPurchOrganiz());
		clone.setUm(getUm());
		clone.setUmPrezzo(getUmPrezzo());
		clone.setUmPrezzoPo(getUmPrezzoPo());
		clone.setValuta(getValuta());
		clone.setUpperMatDesc(getUpperMatDesc());

		return clone;
	}

	public int compareTo(Catalogo catalogo) {
		long primaryKey = catalogo.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CatalogoClp)) {
			return false;
		}

		CatalogoClp catalogo = (CatalogoClp)obj;

		long primaryKey = catalogo.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(47);

		sb.append("{catalogoId=");
		sb.append(getCatalogoId());
		sb.append(", chiaveInforecord=");
		sb.append(getChiaveInforecord());
		sb.append(", dataCreazione=");
		sb.append(getDataCreazione());
		sb.append(", dataFineValidita=");
		sb.append(getDataFineValidita());
		sb.append(", divisione=");
		sb.append(getDivisione());
		sb.append(", flgCanc=");
		sb.append(getFlgCanc());
		sb.append(", fornFinCode=");
		sb.append(getFornFinCode());
		sb.append(", fornitoreCode=");
		sb.append(getFornitoreCode());
		sb.append(", fornitoreDesc=");
		sb.append(getFornitoreDesc());
		sb.append(", matCatLvl2=");
		sb.append(getMatCatLvl2());
		sb.append(", matCatLvl2Desc=");
		sb.append(getMatCatLvl2Desc());
		sb.append(", matCatLvl4=");
		sb.append(getMatCatLvl4());
		sb.append(", matCatLvl4Desc=");
		sb.append(getMatCatLvl4Desc());
		sb.append(", matCod=");
		sb.append(getMatCod());
		sb.append(", matCodFornFinale=");
		sb.append(getMatCodFornFinale());
		sb.append(", matDesc=");
		sb.append(getMatDesc());
		sb.append(", prezzo=");
		sb.append(getPrezzo());
		sb.append(", purchOrganiz=");
		sb.append(getPurchOrganiz());
		sb.append(", um=");
		sb.append(getUm());
		sb.append(", umPrezzo=");
		sb.append(getUmPrezzo());
		sb.append(", umPrezzoPo=");
		sb.append(getUmPrezzoPo());
		sb.append(", valuta=");
		sb.append(getValuta());
		sb.append(", upperMatDesc=");
		sb.append(getUpperMatDesc());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(73);

		sb.append("<model><model-name>");
		sb.append("com.hp.omp.model.Catalogo");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>catalogoId</column-name><column-value><![CDATA[");
		sb.append(getCatalogoId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>chiaveInforecord</column-name><column-value><![CDATA[");
		sb.append(getChiaveInforecord());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataCreazione</column-name><column-value><![CDATA[");
		sb.append(getDataCreazione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>dataFineValidita</column-name><column-value><![CDATA[");
		sb.append(getDataFineValidita());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>divisione</column-name><column-value><![CDATA[");
		sb.append(getDivisione());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>flgCanc</column-name><column-value><![CDATA[");
		sb.append(getFlgCanc());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fornFinCode</column-name><column-value><![CDATA[");
		sb.append(getFornFinCode());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fornitoreCode</column-name><column-value><![CDATA[");
		sb.append(getFornitoreCode());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>fornitoreDesc</column-name><column-value><![CDATA[");
		sb.append(getFornitoreDesc());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>matCatLvl2</column-name><column-value><![CDATA[");
		sb.append(getMatCatLvl2());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>matCatLvl2Desc</column-name><column-value><![CDATA[");
		sb.append(getMatCatLvl2Desc());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>matCatLvl4</column-name><column-value><![CDATA[");
		sb.append(getMatCatLvl4());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>matCatLvl4Desc</column-name><column-value><![CDATA[");
		sb.append(getMatCatLvl4Desc());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>matCod</column-name><column-value><![CDATA[");
		sb.append(getMatCod());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>matCodFornFinale</column-name><column-value><![CDATA[");
		sb.append(getMatCodFornFinale());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>matDesc</column-name><column-value><![CDATA[");
		sb.append(getMatDesc());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>prezzo</column-name><column-value><![CDATA[");
		sb.append(getPrezzo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>purchOrganiz</column-name><column-value><![CDATA[");
		sb.append(getPurchOrganiz());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>um</column-name><column-value><![CDATA[");
		sb.append(getUm());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>umPrezzo</column-name><column-value><![CDATA[");
		sb.append(getUmPrezzo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>umPrezzoPo</column-name><column-value><![CDATA[");
		sb.append(getUmPrezzoPo());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>valuta</column-name><column-value><![CDATA[");
		sb.append(getValuta());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>upperMatDesc</column-name><column-value><![CDATA[");
		sb.append(getUpperMatDesc());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _catalogoId;
	private String _chiaveInforecord;
	private Date _dataCreazione;
	private Date _dataFineValidita;
	private String _divisione;
	private boolean _flgCanc;
	private String _fornFinCode;
	private String _fornitoreCode;
	private String _fornitoreDesc;
	private String _matCatLvl2;
	private String _matCatLvl2Desc;
	private String _matCatLvl4;
	private String _matCatLvl4Desc;
	private String _matCod;
	private String _matCodFornFinale;
	private String _matDesc;
	private String _prezzo;
	private String _purchOrganiz;
	private String _um;
	private String _umPrezzo;
	private String _umPrezzoPo;
	private String _valuta;
	private String _upperMatDesc;
	private BaseModel<?> _catalogoRemoteModel;
}