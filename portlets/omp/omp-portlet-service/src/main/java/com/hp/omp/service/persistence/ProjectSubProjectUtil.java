package com.hp.omp.service.persistence;

import com.hp.omp.model.ProjectSubProject;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the project sub project service. This utility wraps {@link ProjectSubProjectPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see ProjectSubProjectPersistence
 * @see ProjectSubProjectPersistenceImpl
 * @generated
 */
public class ProjectSubProjectUtil {
    private static ProjectSubProjectPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(ProjectSubProject projectSubProject) {
        getPersistence().clearCache(projectSubProject);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<ProjectSubProject> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<ProjectSubProject> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<ProjectSubProject> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
     */
    public static ProjectSubProject update(
        ProjectSubProject projectSubProject, boolean merge)
        throws SystemException {
        return getPersistence().update(projectSubProject, merge);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
     */
    public static ProjectSubProject update(
        ProjectSubProject projectSubProject, boolean merge,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(projectSubProject, merge, serviceContext);
    }

    /**
    * Caches the project sub project in the entity cache if it is enabled.
    *
    * @param projectSubProject the project sub project
    */
    public static void cacheResult(
        com.hp.omp.model.ProjectSubProject projectSubProject) {
        getPersistence().cacheResult(projectSubProject);
    }

    /**
    * Caches the project sub projects in the entity cache if it is enabled.
    *
    * @param projectSubProjects the project sub projects
    */
    public static void cacheResult(
        java.util.List<com.hp.omp.model.ProjectSubProject> projectSubProjects) {
        getPersistence().cacheResult(projectSubProjects);
    }

    /**
    * Creates a new project sub project with the primary key. Does not add the project sub project to the database.
    *
    * @param projectSubProjectId the primary key for the new project sub project
    * @return the new project sub project
    */
    public static com.hp.omp.model.ProjectSubProject create(
        long projectSubProjectId) {
        return getPersistence().create(projectSubProjectId);
    }

    /**
    * Removes the project sub project with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param projectSubProjectId the primary key of the project sub project
    * @return the project sub project that was removed
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a project sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject remove(
        long projectSubProjectId)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(projectSubProjectId);
    }

    public static com.hp.omp.model.ProjectSubProject updateImpl(
        com.hp.omp.model.ProjectSubProject projectSubProject, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(projectSubProject, merge);
    }

    /**
    * Returns the project sub project with the primary key or throws a {@link com.hp.omp.NoSuchProjectSubProjectException} if it could not be found.
    *
    * @param projectSubProjectId the primary key of the project sub project
    * @return the project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a project sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject findByPrimaryKey(
        long projectSubProjectId)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(projectSubProjectId);
    }

    /**
    * Returns the project sub project with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param projectSubProjectId the primary key of the project sub project
    * @return the project sub project, or <code>null</code> if a project sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject fetchByPrimaryKey(
        long projectSubProjectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(projectSubProjectId);
    }

    /**
    * Returns all the project sub projects where projectId = &#63;.
    *
    * @param projectId the project ID
    * @return the matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ProjectSubProject> findByProjectId(
        long projectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByProjectId(projectId);
    }

    /**
    * Returns a range of all the project sub projects where projectId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param projectId the project ID
    * @param start the lower bound of the range of project sub projects
    * @param end the upper bound of the range of project sub projects (not inclusive)
    * @return the range of matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ProjectSubProject> findByProjectId(
        long projectId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByProjectId(projectId, start, end);
    }

    /**
    * Returns an ordered range of all the project sub projects where projectId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param projectId the project ID
    * @param start the lower bound of the range of project sub projects
    * @param end the upper bound of the range of project sub projects (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ProjectSubProject> findByProjectId(
        long projectId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByProjectId(projectId, start, end, orderByComparator);
    }

    /**
    * Returns the first project sub project in the ordered set where projectId = &#63;.
    *
    * @param projectId the project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject findByProjectId_First(
        long projectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByProjectId_First(projectId, orderByComparator);
    }

    /**
    * Returns the first project sub project in the ordered set where projectId = &#63;.
    *
    * @param projectId the project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching project sub project, or <code>null</code> if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject fetchByProjectId_First(
        long projectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByProjectId_First(projectId, orderByComparator);
    }

    /**
    * Returns the last project sub project in the ordered set where projectId = &#63;.
    *
    * @param projectId the project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject findByProjectId_Last(
        long projectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByProjectId_Last(projectId, orderByComparator);
    }

    /**
    * Returns the last project sub project in the ordered set where projectId = &#63;.
    *
    * @param projectId the project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching project sub project, or <code>null</code> if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject fetchByProjectId_Last(
        long projectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByProjectId_Last(projectId, orderByComparator);
    }

    /**
    * Returns the project sub projects before and after the current project sub project in the ordered set where projectId = &#63;.
    *
    * @param projectSubProjectId the primary key of the current project sub project
    * @param projectId the project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a project sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject[] findByProjectId_PrevAndNext(
        long projectSubProjectId, long projectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByProjectId_PrevAndNext(projectSubProjectId, projectId,
            orderByComparator);
    }

    /**
    * Returns all the project sub projects where subProjectId = &#63;.
    *
    * @param subProjectId the sub project ID
    * @return the matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ProjectSubProject> findBySubProjectId(
        long subProjectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBySubProjectId(subProjectId);
    }

    /**
    * Returns a range of all the project sub projects where subProjectId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param subProjectId the sub project ID
    * @param start the lower bound of the range of project sub projects
    * @param end the upper bound of the range of project sub projects (not inclusive)
    * @return the range of matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ProjectSubProject> findBySubProjectId(
        long subProjectId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBySubProjectId(subProjectId, start, end);
    }

    /**
    * Returns an ordered range of all the project sub projects where subProjectId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param subProjectId the sub project ID
    * @param start the lower bound of the range of project sub projects
    * @param end the upper bound of the range of project sub projects (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ProjectSubProject> findBySubProjectId(
        long subProjectId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBySubProjectId(subProjectId, start, end,
            orderByComparator);
    }

    /**
    * Returns the first project sub project in the ordered set where subProjectId = &#63;.
    *
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject findBySubProjectId_First(
        long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBySubProjectId_First(subProjectId, orderByComparator);
    }

    /**
    * Returns the first project sub project in the ordered set where subProjectId = &#63;.
    *
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching project sub project, or <code>null</code> if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject fetchBySubProjectId_First(
        long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchBySubProjectId_First(subProjectId, orderByComparator);
    }

    /**
    * Returns the last project sub project in the ordered set where subProjectId = &#63;.
    *
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject findBySubProjectId_Last(
        long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBySubProjectId_Last(subProjectId, orderByComparator);
    }

    /**
    * Returns the last project sub project in the ordered set where subProjectId = &#63;.
    *
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching project sub project, or <code>null</code> if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject fetchBySubProjectId_Last(
        long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchBySubProjectId_Last(subProjectId, orderByComparator);
    }

    /**
    * Returns the project sub projects before and after the current project sub project in the ordered set where subProjectId = &#63;.
    *
    * @param projectSubProjectId the primary key of the current project sub project
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a project sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject[] findBySubProjectId_PrevAndNext(
        long projectSubProjectId, long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBySubProjectId_PrevAndNext(projectSubProjectId,
            subProjectId, orderByComparator);
    }

    /**
    * Returns all the project sub projects where projectId = &#63; and subProjectId = &#63;.
    *
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @return the matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ProjectSubProject> findByProjectIdAndSubProjectId(
        long projectId, long subProjectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByProjectIdAndSubProjectId(projectId, subProjectId);
    }

    /**
    * Returns a range of all the project sub projects where projectId = &#63; and subProjectId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @param start the lower bound of the range of project sub projects
    * @param end the upper bound of the range of project sub projects (not inclusive)
    * @return the range of matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ProjectSubProject> findByProjectIdAndSubProjectId(
        long projectId, long subProjectId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByProjectIdAndSubProjectId(projectId, subProjectId,
            start, end);
    }

    /**
    * Returns an ordered range of all the project sub projects where projectId = &#63; and subProjectId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @param start the lower bound of the range of project sub projects
    * @param end the upper bound of the range of project sub projects (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ProjectSubProject> findByProjectIdAndSubProjectId(
        long projectId, long subProjectId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByProjectIdAndSubProjectId(projectId, subProjectId,
            start, end, orderByComparator);
    }

    /**
    * Returns the first project sub project in the ordered set where projectId = &#63; and subProjectId = &#63;.
    *
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject findByProjectIdAndSubProjectId_First(
        long projectId, long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByProjectIdAndSubProjectId_First(projectId,
            subProjectId, orderByComparator);
    }

    /**
    * Returns the first project sub project in the ordered set where projectId = &#63; and subProjectId = &#63;.
    *
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching project sub project, or <code>null</code> if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject fetchByProjectIdAndSubProjectId_First(
        long projectId, long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByProjectIdAndSubProjectId_First(projectId,
            subProjectId, orderByComparator);
    }

    /**
    * Returns the last project sub project in the ordered set where projectId = &#63; and subProjectId = &#63;.
    *
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject findByProjectIdAndSubProjectId_Last(
        long projectId, long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByProjectIdAndSubProjectId_Last(projectId,
            subProjectId, orderByComparator);
    }

    /**
    * Returns the last project sub project in the ordered set where projectId = &#63; and subProjectId = &#63;.
    *
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching project sub project, or <code>null</code> if a matching project sub project could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject fetchByProjectIdAndSubProjectId_Last(
        long projectId, long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByProjectIdAndSubProjectId_Last(projectId,
            subProjectId, orderByComparator);
    }

    /**
    * Returns the project sub projects before and after the current project sub project in the ordered set where projectId = &#63; and subProjectId = &#63;.
    *
    * @param projectSubProjectId the primary key of the current project sub project
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next project sub project
    * @throws com.hp.omp.NoSuchProjectSubProjectException if a project sub project with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static com.hp.omp.model.ProjectSubProject[] findByProjectIdAndSubProjectId_PrevAndNext(
        long projectSubProjectId, long projectId, long subProjectId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.hp.omp.NoSuchProjectSubProjectException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByProjectIdAndSubProjectId_PrevAndNext(projectSubProjectId,
            projectId, subProjectId, orderByComparator);
    }

    /**
    * Returns all the project sub projects.
    *
    * @return the project sub projects
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ProjectSubProject> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the project sub projects.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of project sub projects
    * @param end the upper bound of the range of project sub projects (not inclusive)
    * @return the range of project sub projects
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ProjectSubProject> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the project sub projects.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of project sub projects
    * @param end the upper bound of the range of project sub projects (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of project sub projects
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<com.hp.omp.model.ProjectSubProject> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the project sub projects where projectId = &#63; from the database.
    *
    * @param projectId the project ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByProjectId(long projectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByProjectId(projectId);
    }

    /**
    * Removes all the project sub projects where subProjectId = &#63; from the database.
    *
    * @param subProjectId the sub project ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeBySubProjectId(long subProjectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeBySubProjectId(subProjectId);
    }

    /**
    * Removes all the project sub projects where projectId = &#63; and subProjectId = &#63; from the database.
    *
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByProjectIdAndSubProjectId(long projectId,
        long subProjectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence()
            .removeByProjectIdAndSubProjectId(projectId, subProjectId);
    }

    /**
    * Removes all the project sub projects from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of project sub projects where projectId = &#63;.
    *
    * @param projectId the project ID
    * @return the number of matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public static int countByProjectId(long projectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByProjectId(projectId);
    }

    /**
    * Returns the number of project sub projects where subProjectId = &#63;.
    *
    * @param subProjectId the sub project ID
    * @return the number of matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public static int countBySubProjectId(long subProjectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countBySubProjectId(subProjectId);
    }

    /**
    * Returns the number of project sub projects where projectId = &#63; and subProjectId = &#63;.
    *
    * @param projectId the project ID
    * @param subProjectId the sub project ID
    * @return the number of matching project sub projects
    * @throws SystemException if a system exception occurred
    */
    public static int countByProjectIdAndSubProjectId(long projectId,
        long subProjectId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByProjectIdAndSubProjectId(projectId, subProjectId);
    }

    /**
    * Returns the number of project sub projects.
    *
    * @return the number of project sub projects
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static ProjectSubProjectPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (ProjectSubProjectPersistence) PortletBeanLocatorUtil.locate(com.hp.omp.service.ClpSerializer.getServletContextName(),
                    ProjectSubProjectPersistence.class.getName());

            ReferenceRegistry.registerReference(ProjectSubProjectUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated
     */
    public void setPersistence(ProjectSubProjectPersistence persistence) {
    }
}
