package com.hp.omp.service.persistence;

import com.hp.omp.model.PurchaseRequest;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the purchase request service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see PurchaseRequestPersistenceImpl
 * @see PurchaseRequestUtil
 * @generated
 */
public interface PurchaseRequestPersistence extends BasePersistence<PurchaseRequest> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link PurchaseRequestUtil} to access the purchase request persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the purchase request in the entity cache if it is enabled.
    *
    * @param purchaseRequest the purchase request
    */
    public void cacheResult(com.hp.omp.model.PurchaseRequest purchaseRequest);

    /**
    * Caches the purchase requests in the entity cache if it is enabled.
    *
    * @param purchaseRequests the purchase requests
    */
    public void cacheResult(
        java.util.List<com.hp.omp.model.PurchaseRequest> purchaseRequests);

    /**
    * Creates a new purchase request with the primary key. Does not add the purchase request to the database.
    *
    * @param purchaseRequestId the primary key for the new purchase request
    * @return the new purchase request
    */
    public com.hp.omp.model.PurchaseRequest create(long purchaseRequestId);

    /**
    * Removes the purchase request with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param purchaseRequestId the primary key of the purchase request
    * @return the purchase request that was removed
    * @throws com.hp.omp.NoSuchPurchaseRequestException if a purchase request with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseRequest remove(long purchaseRequestId)
        throws com.hp.omp.NoSuchPurchaseRequestException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.hp.omp.model.PurchaseRequest updateImpl(
        com.hp.omp.model.PurchaseRequest purchaseRequest, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the purchase request with the primary key or throws a {@link com.hp.omp.NoSuchPurchaseRequestException} if it could not be found.
    *
    * @param purchaseRequestId the primary key of the purchase request
    * @return the purchase request
    * @throws com.hp.omp.NoSuchPurchaseRequestException if a purchase request with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseRequest findByPrimaryKey(
        long purchaseRequestId)
        throws com.hp.omp.NoSuchPurchaseRequestException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the purchase request with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param purchaseRequestId the primary key of the purchase request
    * @return the purchase request, or <code>null</code> if a purchase request with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public com.hp.omp.model.PurchaseRequest fetchByPrimaryKey(
        long purchaseRequestId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the purchase requests.
    *
    * @return the purchase requests
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.PurchaseRequest> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the purchase requests.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of purchase requests
    * @param end the upper bound of the range of purchase requests (not inclusive)
    * @return the range of purchase requests
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.PurchaseRequest> findAll(int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the purchase requests.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
    * </p>
    *
    * @param start the lower bound of the range of purchase requests
    * @param end the upper bound of the range of purchase requests (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of purchase requests
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<com.hp.omp.model.PurchaseRequest> findAll(int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the purchase requests from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of purchase requests.
    *
    * @return the number of purchase requests
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
