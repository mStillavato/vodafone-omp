<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<script type="text/javascript" src="../../omp_main-theme/js/jquery-1.10.2.min.js"></script>    
<link href="../../omp_main-theme/css/admin.css" rel="stylesheet"> 
<portlet:defineObjects />

<div id="<portlet:namespace/>microDriverTableList"  class="listingcontainer"></div>
<div class="loading" style='display:none;font-size:30px; color:#eee;margin-top:60px; text-align: center;'>Loading...</div>
<div>
<portlet:renderURL var="addmicroDriverURL">
    <portlet:param name="jspPage" value="/html/microdriveradmin/edit.jsp" />
</portlet:renderURL>

<p><a href="<%= addmicroDriverURL %>&action=add" class="actionbutn">add new microDriver</a></p>

<portlet:resourceURL var="deletemicroDriverURL">
	<portlet:param name="action" value="delete"/>
</portlet:resourceURL>
<portlet:resourceURL var="paginationURL">
</portlet:resourceURL>
 <script type="text/javascript">
 function removeMicroDriver(microDriverId, microDriverName){
	 if(confirm("Do you want to delete this microDriver ["+microDriverName+"]?")) {
		 var url="${deletemicroDriverURL}&microDriverId="+microDriverId+"&microDriverName="+microDriverName;
			jQuery.get(url,function( data ) {
				  jQuery( "#<portlet:namespace/>microDriverTableList" ).html( data );
				});
			}
 		}
 
 
 function <portlet:namespace/>ompListing(url){
		jQuery("#<portlet:namespace/>microDriverTableList").fadeOut( "fast" );
		jQuery(".loading").fadeIn( "normal" );
		jQuery.post(url+"&action=pagination",
				 function(data) {
				   jQuery("#<portlet:namespace/>microDriverTableList").html(data);
				   jQuery(".loading").fadeOut( "fast" );
				   jQuery("#<portlet:namespace/>microDriverTableList").fadeIn( "fast" );
				 }
			);
		}
 
 
 
 <portlet:namespace/>ompListing('${paginationURL}');
 </script>
</div>