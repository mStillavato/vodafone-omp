<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ page import="javax.portlet.PortletPreferences" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">  
<portlet:defineObjects />

<portlet:actionURL var="subprojectadminURL">
<portlet:param name="jspPage" value="/html/subprojectadmin/view.jsp" />
</portlet:actionURL>
<div class="adminform">
<aui:form method="post" action="<%=subprojectadminURL%>">
	<b>Project Name: </b><div>
	<select name="project">
		<c:forEach items="${projectList}" var="projects">
		<c:if test="${projects.display eq true || projects.projectId eq projectId}">
			<c:choose>
				<c:when test="${projects.projectId eq projectId}">
					<option selected value="${projects.projectId}">${projects.projectName}</option>
				</c:when>
				<c:otherwise>
					<option value="${projects.projectId}">${projects.projectName}</option>
				</c:otherwise>
			</c:choose>
			</c:if>
		</c:forEach>
	</select>
<aui:input label="Sub Project Name: " name="subProjectName" type="text" value="${subProjectName}">
	<aui:validator name="required"/>
</aui:input>
<aui:input label="Sub Project Description: " name="subProjectDesc" type="textarea" value="${subProjectDesc}"/> 
<aui:input name="action" type="hidden" value="${action}"/>

<c:if test="${not empty subProjectId}">
<aui:input name="subProjectId" type="hidden" value="${subProjectId}"/>
</c:if>
<c:if test="${not empty projectId}">
<aui:input name="oldProjectId" type="hidden" value="${projectId}"/>
</c:if>
<aui:input inlineLabel="top" label="Display" name="display" type="checkbox" checked="${display}" />
<aui:button type="submit" /> 
</aui:form> 
</div>