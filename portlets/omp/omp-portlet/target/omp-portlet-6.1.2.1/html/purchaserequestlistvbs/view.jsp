<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://hp.com/vfItaly/jsp/taglib" prefix="hp" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>


<liferay-theme:defineObjects />
<portlet:defineObjects />		
<div class="listingtoplinksarea"> 
	<hp:if-hasrole userId="${user.userId}" roleName="OMP_CONTROLLER_VBS,OMP_ENG_VBS">
		<a href="/group/vodafone/purchase-request-vbs?automatic=false&redirect=/group/vodafone/home?automatic=false" class="listingtoplink">New VBS Request</a>
	</hp:if-hasrole>
	<div class="clear"></div>
</div>
<div class="clear"></div>
<portlet:resourceURL var="searchURL"/>
 <ul class="nav nav-tabs" id="myTab">
 <div class="filter">Filtered by</div>
<c:forEach items="${statusList}" var="status" varStatus="loopStatus">
<c:choose>
	<c:when test="${loopStatus.first}">
	
		<li class="active"><a href="javascript:void(null)" onclick="<portlet:namespace/>statusSelected('${searchURL}','${status.code}')" data-toggle="tab" class="created">${status.label}</a> <div class="nav-tabs-arrow"></div></li>
	</c:when>
	<c:when test="${loopStatus.count == 2 }">
			<li class="dropdown" ><a href="javascript:void(null)" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown"><span >Others</span><b class="caret"></b></a><div class="nav-tabs-arrow"></div> 
			<ul class="dropdown-menu" role="menu" aria-labelledby="myTabDrop1">
			<li><a href="javascript:void(null)" onclick="<portlet:namespace/>statusSelected('${searchURL}','${status.code}')" data-toggle="tab">${status.label}</a></li>
			
	</c:when>
	<c:otherwise>
		<li><a href="javascript:void(null)" onclick="<portlet:namespace/>statusSelected('${searchURL}','${status.code}')"  data-toggle="tab">${status.label}</a></li>
	</c:otherwise>
</c:choose>
</c:forEach>
</ul>
</li>

<%@include file="prsearch.jsp"%>
<div class="clear"></div>
</ul>
<div id="<portlet:namespace/>prListDiv" class="listingcontainer"></div>
<div class="loading" style='display:none;font-size:30px; color:#eee;margin-top:60px; text-align: center;'>Loading...</div>
<script type="text/javascript">
var <portlet:namespace/>status = "${createStatus.code}";
var <portlet:namespace/>idOrder = "desc"; 
var <portlet:namespace/>requesterOrder = "desc"; 
var <portlet:namespace/>recieverOrder = "desc";
var <portlet:namespace/>vendorOrder = "desc"; 
var <portlet:namespace/>subCategoryOrder = "desc"; 
var <portlet:namespace/>totalValueOrder = "desc"; 
var <portlet:namespace/>activityDescriptionOrder = "desc";
var <portlet:namespace/>automaticOrder = "desc";
var <portlet:namespace/>fiscalYearOrder = "desc";
var <portlet:namespace/>orderby = "purchaseRequestId"; 
var <portlet:namespace/>order = "desc";
var <portlet:namespace/>isMini = "true";


<portlet:namespace/>ompListing('${searchURL}');

function <portlet:namespace/>ompListing(url){
	var prId = jQuery("#<portlet:namespace/>prNumber").val();
	var costCenter = jQuery("#<portlet:namespace/>costCenter").val();
	var prStatus = jQuery("#<portlet:namespace/>prStatus").val();
	var fYear = jQuery("#<portlet:namespace/>fYear").val();
	var project = jQuery("#<portlet:namespace/>project").val();
	var subProject = jQuery("#<portlet:namespace/>subProject").val();
	var wbsCode = jQuery("#<portlet:namespace/>wbsCode").val();
	var searchKeyWord = jQuery("#<portlet:namespace/>searchKeyWord").val();
	
	jQuery("#<portlet:namespace/>prListDiv").fadeOut( "fast" );
	jQuery(".loading").fadeIn( "normal" );
	jQuery.post(url,{isMini:<portlet:namespace/>isMini,prId:prId,costCenter:costCenter,prStatus:prStatus,fYear:fYear,project:project,subProject:subProject,wbsCode:wbsCode,searchKeyWord:searchKeyWord,status:<portlet:namespace/>status,orderby:<portlet:namespace/>orderby, order:<portlet:namespace/>order},
			 function(data) {
			   jQuery("#<portlet:namespace/>prListDiv").html(data);
			   jQuery(".loading").fadeOut( "fast" );
			   jQuery("#<portlet:namespace/>prListDiv").fadeIn( "fast" );
			 }
		);
	}
	
function <portlet:namespace/>prSearch(url){
	
	var searchPrValue = jQuery("#<portlet:namespace/>searchPr").val();
	jQuery("#<portlet:namespace/>prListDiv").fadeOut( "fast" );
	jQuery(".loading").fadeIn( "normal" );
	jQuery.post(url,{searchPrValue:searchPrValue},
			 function(data) {
			   jQuery("#<portlet:namespace/>prListDiv").html(data);
			   jQuery(".loading").fadeOut( "fast" );
			   jQuery("#<portlet:namespace/>prListDiv").fadeIn( "fast" );
			 }
		);
	}
	
function <portlet:namespace/>search(url,isMini){
	if(isMini=="false"){
		jQuery("#<portlet:namespace/>searchKeyWord").val("");
	}
	<portlet:namespace/>isMini=isMini;
	<portlet:namespace/>ompListing(url);
}


function <portlet:namespace/>statusSelected(url,status){
	<portlet:namespace/>status = status;
	<portlet:namespace/>ompListing(url);
}
function orderRequesterPRList(orderby){
	<portlet:namespace/>orderby = orderby.id;
	if(orderby.id == "purchaseRequestId"){
		if (<portlet:namespace/>idOrder == "desc") {
			<portlet:namespace/>order="asc"; <portlet:namespace/>idOrder = "asc"; 
		} else {
			<portlet:namespace/>order="desc";<portlet:namespace/>idOrder = "desc";
		}
	} else if(orderby.id == "screenNameRequester"){
		if (<portlet:namespace/>requesterOrder == "desc") {
			<portlet:namespace/>order="asc"; <portlet:namespace/>requesterOrder = "asc";
		} else {
			<portlet:namespace/>order="desc"; <portlet:namespace/>requesterOrder = "desc";
		}
	} else if(orderby.id == "screenNameReciever"){
		if (<portlet:namespace/>recieverOrder == "desc") {
			<portlet:namespace/>order="asc"; <portlet:namespace/>recieverOrder = "asc";
		} else {
			<portlet:namespace/>order="desc"; <portlet:namespace/>recieverOrder = "desc";
		}
	} else if(orderby.id == "vendorName") {
		if (<portlet:namespace/>vendorOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>vendorOrder = "asc";
		} else {
			<portlet:namespace/>order="desc"; <portlet:namespace/>vendorOrder = "desc";	
		}
	}else if(orderby.id == "totalValue") { 
		if (<portlet:namespace/>totalValueOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>totalValueOrder = "asc";
		} else {
			<portlet:namespace/>order="desc";<portlet:namespace/>totalValueOrder = "desc";
		}
	}else if(orderby.id == "activityDescription") {
		if (<portlet:namespace/>activityDescriptionOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>activityDescriptionOrder = "asc";
		} else {
			<portlet:namespace/>order="desc";<portlet:namespace/>activityDescriptionOrder = "desc";
		}
	}else if(orderby.id == "automatic") { 
		if (<portlet:namespace/>automaticOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>automaticOrder = "asc";
		} else {
			<portlet:namespace/>order="desc";<portlet:namespace/>automaticOrder = "desc";
		}
	}else if(orderby.id == "fiscalYear") { 
		if (<portlet:namespace/>fiscalYearOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>fiscalYearOrder = "asc";
		} else {
			<portlet:namespace/>order="desc";<portlet:namespace/>fiscalYearOrder = "desc";
		}
}
	<portlet:namespace/>ompListing('${searchURL}');
}
</script>
