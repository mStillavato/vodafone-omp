<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<liferay-theme:defineObjects />
<portlet:defineObjects />

<c:if test="${numberOfPositions > 0 }">
	<form id='positionsTableForm'>
	
		<display:table decorator="com.hp.omp.decorator.LayoutNotNssCatalogoListDecorator" name="layoutNotNssCatalogoList" list="layoutNotNssCatalogoList" id="catalogo">
	 		<display:column property="lineNumber" title="Line Number" />
		 	<display:column property="materialId" title="Material ID" />
		 	<display:column property="materialDesc" title="Material Desc" />
		 	<display:column property="quantity" title="PO Quantity" />
		 	<display:column property="netPrice" title="Net Price" />
		 	<display:column property="currency" title="Currency" />
		 	<display:column property="deliveryDate" title="Delivery Date" />
		 	<display:column property="deliveryAddress" title="Delivery Address" />
		 	<display:column property="itemText" title="Item Text" />
		 	<display:column property="locationEvo" title="Location EVO" />
		 	<display:column property="targaTecnica" title="Targa Tecnica" />
		</display:table>
	
	</form>
</c:if>

<script type="text/javascript">
		//$("#totalvalue").val("${prTotalValue}");
		$("#numberOfPositions").val("${numberOfPositions}");
		</script>
