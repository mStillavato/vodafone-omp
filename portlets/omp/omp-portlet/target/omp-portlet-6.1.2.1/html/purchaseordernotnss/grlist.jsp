<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://hp.com/vfItaly/jsp/taglib" prefix="hp" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<c:forEach var="gr" items="${grList}">
	<%@include file="gritem.jsp"%>
</c:forEach>
      
<portlet:resourceURL var="saveGR" />

<portlet:renderURL var="getGRList" windowState="exclusive">
	<portlet:param name="positionId" value="${selectedposition.positionId}"/>
	<portlet:param name="jspPage" value="/html/purchaseordernotnss/grlist.jsp"/>
	<portlet:param name="GET_CMD" value="grList"/>
</portlet:renderURL>

<!-- Add condition to in position impl ot get the sum of the position's gr  -->
<hp:if-hasrole userId="${user.userId}" roleName="OMP_CONTROLLER,OMP_ENG">
	<c:if test="${empty position.sumPositionGRPercentage || position.sumPositionGRPercentage < 100}">
		<portlet:renderURL var="getGRForm" windowState="exclusive">
			<portlet:param name="jspPage" value="/html/purchaseordernotnss/grform.jsp"/>
			<portlet:param name="positionId" value="${position.positionId}"/>
			<portlet:param name="GET_CMD" value="grForm"/>
		</portlet:renderURL>
		<div class="addposition">
			<a href="javascript:void(0)" onclick="<portlet:namespace/>getGoodReceiptForm('${getGRForm}')" data-toggle='modal' data-target='#addeditgr'>add Good Receipt</a>
		</div>
	</c:if>
</hp:if-hasrole> 