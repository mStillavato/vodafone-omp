<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<portlet:defineObjects />

<li class="searcharea">
	<div class="advancedsearchcontainer"><a href="javascript:void(0)" data-toggle="popover" data-placement="left" data-content="" class="advancedsearchlink">Advanced Search</a>
		<div class="advancedsearchcontent" id="searchpopover">
		
			<form method="post" id="advanced">
				<portlet:resourceURL var="searchURL"/>
				<portlet:resourceURL id="searchPr" var="searchPrURL"/>
				
				<div class="advancedsearchform">
					<div class="formItem">
						<div class="formLabel"><label>Vendor</label></div>
						<div class="clear"></div>
						<div class="formField">
							<input name="vendor" id="<portlet:namespace/>vendor" placeholder="Vendor..." type="text"/>
						</div>
						<div class="clear"></div>
					</div>
						
					<div class="formItem advreqnum">
						<div class="formLabel"><label>Mat Cat Lvl4</label></div>
						<div class="clear"></div>
						<div class="formField">
							<input name="matCatLvl4" id="<portlet:namespace/>matCatLvl4" placeholder="Mat Cat Lvl4..." type="text"/>
						</div>
						<div class="clear"></div>
					</div>
					 
					<div class="formItem">
						<div class="formLabel"><label>Codice Materiale</label></div>
						<div class="clear"></div>
						<div class="formField">
							<input name="codMateriale" id="<portlet:namespace/>codMateriale" type="text" placeholder="Codice Materiale..."/>
						</div>
						<div class="clear"></div>
					</div>
					 
					<div class="formItem">
						<div class="formLabel"><label>Codice Fornitore</label></div>
						<div class="clear"></div>
						<div class="formField">
							<input name="codFornitore" id="<portlet:namespace/>codFornitore" type="text" placeholder="Codice Fornitore..."/>
						</div>
						<div class="clear"></div>
					</div>
					 
					<div class="formItem">
						<div class="formLabel"><label>Descrizione Materiale</label></div>
						<div class="clear"></div>
						<div class="formField">
							<input name="descMateriale" id="<portlet:namespace/>descMateriale" type="text" placeholder="Descrizione Materiale..."/>
						</div>
						<div class="clear"></div>
					</div>
					
					<div class="formItem">
						<div class="formLabel"><label>Prezzo</label></div>
						<div class="clear"></div>
						<div class="formField">
							<input name="prezzo" id="<portlet:namespace/>prezzo" type="text" placeholder="Prezzo..."/>
						</div>
						<div class="clear"></div>
					</div>
					 
					<div class="clear"></div>
					  
					<input type="button" value="Search" class="submitbtn clssrch" onclick="<portlet:namespace/>search('${searchURL}','false')"/>
					<div class="clear"></div>
				</div>
			</form>
		</div>
	</div>
	
	<div class="normalsearchcontainer">
		<form method="post" id="global">
			<input name="searchKeyWord" id="<portlet:namespace/>searchKeyWord" type="text" placeholder="Search Vendor..."/>
			<input type="button" value="Submit" class="submitbtn" onclick="<portlet:namespace/>search('${searchURL}','true')"/>
		</form> 
	</div>

	<div class="clear"></div>
</li>


<script>
 $(".idinput").keyup(function (e) {
 $('.idinput').val($('.idinput').val().replace(/[^\d\.]/g,""));
 });
 $(".idinput").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
(e.keyCode == 67 && e.ctrlKey === true) ||
(e.keyCode == 86 && e.ctrlKey === true) ||			
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
				 $('.idinput').val($('.idinput').val().replace(/[^\d\.]/g,""));
        }
		
		if (
            (e.keyCode == 13)) {  
        
				  
        }
		

        if (
            (e.keyCode == 110)) {  
                  e.preventDefault();


        }
        
   
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
			
        }
    });
</script>

