<%@include file="init.jsp"%>
<%@include file="purchaseRequestJs.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://hp.com/vfItaly/jsp/taglib" prefix="hp"%>

<portlet:defineObjects />

<liferay-ui:error key="error-handling"
	message="Sorry, an error prevented handling your purchase request" />
<liferay-ui:error key="error-costcenter"
	message="Sorry, the logged inuser or the PR has no cost center" />
<liferay-ui:error key="error-deleting"
	message="Sorry, an error prevented deleting your purchase request" />
<liferay-ui:error key="error-submitting"
	message="Sorry, an error prevented submitting your purchase request" />


<div class="ompformcontainer">
	<div class="ompformtitle">Purchase Request VBS</div>
	<div class="clear"></div>
	<div id="purchaseRequestSuccessMSG"></div>
	<c:if test="${prDisabled==''}">

		<form method="post" enctype="multipart/form-data" name="form" id=" ">
			<div class="uploadFileWrapper">
				<div class="popup_row2 browseFileWrapper">
					<div class="uploading">
						<img id="loading"
							src="<%=request.getContextPath()%>/js/upload Ajax/loading.gif"
							style=""> Uploading...
					</div>
					<div class="popup_row2_browse popup_add_bottomBtns"
						id="uploadFileDiv">
						<input type="file" name="fileName" id="fileName"
							value="Select File" class="input" required="required" />
					</div>
				</div>
				<div class="popup_row3 uploadBtn">
					<input type="button" name="upload" id="upload" value="Upload"
						class="noText_shadow" onclick="checkFileExist()" />
				</div>
			</div>
		</form>
	</c:if>

	<div ${pr.hasFile? "class='hasfiles'":"class='displaynone'"}
		id="uploadedFile">

		<%@include file="filelist.jsp"%>
	</div>

	<!--  	<div id="uploadedFile">
	<c:if test="${hasUploadedFile==true}">
		<a href="javascript:viewUploadedfile()">view Uploaded File</a> 
	</c:if>
	</div>-->

	<form action="" method="post" id="editpurchaseRequestForm">
		<input type="hidden" id="numberOfPositions" name="numberOfPositions"
			value="${numberOfPositions}" /> <input type="hidden"
			id="actionStatus" name="actionStatus" value="${actionStatus}" /> <input
			type="hidden" name="automatic" id="automatic" value="${pr.automatic}" />
		<input type="hidden" id="hasUploadedFile" name="hasUploadedFile"
			value="${hasUploadedFile}" /> <input type="hidden" id="userType"
			name="userType" value="${userType}" /> <input type="hidden"
			name="redirect" id="redirect" value="${redirect}" />
		<c:if test="${pr.purchaseRequestId == 0 }">
			<input type="hidden" name="purchaseRequestId" id="purchaseRequestId"
				value="" />
		</c:if>
		<c:if test="${pr.purchaseRequestId != 0 }">
			<input type="hidden" name="purchaseRequestId" id="purchaseRequestId"
				value="${pr.purchaseRequestId}" />
		</c:if>

		<div class="ompformwrapper">
			<div class="ompformlable">
				<label for="costcenter">Profit Center</label>
			</div>
			<div class="ompforminput">
				<select class="combobox" name="costcenter" id="costcenter"
					<option selected value="">Select cost center..</option>
					<c:forEach items="${allCostCenters}" var="costcenter">
						<c:if
							test="${costcenter.display eq true || costcenter.costCenterId eq pr.costCenterId}">
							<c:choose>
								<c:when test="${costcenter.costCenterId eq pr.costCenterId}">
									<option selected value="${costcenter.costCenterId}">${costcenter.costCenterName}</option>
								</c:when>
								<c:otherwise>
									<option value="${costcenter.costCenterId}">${costcenter.costCenterName}</option>
								</c:otherwise>
							</c:choose>
						</c:if>
					</c:forEach>
				</select>
			</div>
			<div class="ompformlable">
				<label for="ola">OLA</label>
			</div>
			<div class="ompforminput">
				<input name="ola" id="ola" type="text" value="${pr.ola}"
					${prDisabled} placeholder="OLA" />
			</div>

			<div class="ompformlable">
				<label for="buyer">Buyer</label>
			</div>
			<div class="ompforminput">
				<select class="combobox" id="buyer" name="buyer" ${prDisabled}>
					<option selected value="">Select Buyer..</option>
					<c:forEach items="${allBuyers}" var="buyer">
						<c:if
							test="${buyer.display eq true || buyer.buyerId eq pr.buyerId}">
							<c:choose>
								<c:when test="${buyer.buyerId eq pr.buyerId}">
									<option selected value="${buyer.buyerId}">${buyer.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${buyer.buyerId}">${buyer.name}</option>
								</c:otherwise>
							</c:choose>
						</c:if>
					</c:forEach>
				</select>
			</div>

			<div class="ompformlable">
				<label for="vendor">Vendor</label>
			</div>
			<div class="ompforminput trackingrelatedDiv">
				<select class="combobox" name="vendor" id="vendor" ${prDisabled}>
					<option selected value="">Select Vendor..</option>
					<c:forEach items="${allVendors}" var="vendor">
						<c:if
							test="${vendor.display eq true || vendor.vendorId eq pr.vendorId}">
							<c:choose>
								<c:when test="${vendor.vendorId eq pr.vendorId}">
									<option selected value="${vendor.vendorId}">${vendor.vendorName}</option>
								</c:when>
								<c:otherwise>
									<option value="${vendor.vendorId}">${vendor.vendorName}</option>
								</c:otherwise>
							</c:choose>
						</c:if>
					</c:forEach>
				</select>
			</div>

			<div class="clear"></div>
			<div class="ompformlable">
				<label for="totalvalue">Total Value</label>
			</div>
			<div class="ompforminput">
				<input name="totalvalue" id="totalvalue" type="text"
					value="${prTotalValue}" readonly />
			</div>
			<div class="ompformlable">
				<label for="prCurrency">Currency</label>
			</div>
			<div class="ompforminput">
				<select name="prCurrency" class="combobox" id="prCurrency"
					${prDisabled}>
					<option value="">Select Currency..</option>
					<option selected value="1" ${euroSelected}>Euro</option>
					<option value="0" ${dollarSelected}>Dollar</option>
				</select>
			</div>

			<div class="clear"></div>
			<div class="ompformlable">
				<label for="activitydescription">Activity Description</label>
			</div>
			<div class="ompforminputtextarea">
				<div class="counter"></div>
				<textarea class="txtareamax" rows="4" cols="50"
					name="activitydescription" id="activitydescription" ${prDisabled}>${pr.activityDescription}</textarea>
			</div>

			<div class="clear"></div>
			<div class="ompformlable">
				<label for="rejectionCause">Rejection Cause</label>
			</div>
			<div class="ompforminputtextarea">
				<div class="counter"></div>
				<textarea class="txtareamax" rows="4" cols="50"
					name="rejectionCause" id="rejectionCause" ${rejCauseDisabled}>${pr.rejectionCause}</textarea>
			</div>
			<div class="clear"></div>

		</div>
	</form>

	<div id="positionsContent">
		<%@include file="positionlist.jsp"%>
	</div>

	<div class="addposition">
		<c:if test="${prDisabled==''}">
			<button type="button" onclick="setPositionFormInAddMode();"
				value="Add Position" data-toggle="modal"
				data-target="#addpositionmodal">Add Position</button>
		</c:if>
	</div>

	<div class="formactionbuttons">
		<c:if test="${prDisabled==''}">
			<button type="button" onclick="addUpdatepurchaseRequest();"
				class="savebtn" value="Save">Save</button>
			<button type="button" onclick="submitpurchaseRequest();"
				value="Submit" class="submitbtn">Submit</button>
		</c:if>

		<c:if
			test="${(userType == 'ANTEX' ||userType == 'CONTROLLER_VBS') && actionStatus=='Submitted' }">
			<button type="button" class="submitbtn"
				onclick="startPurchaseRequest();" value="Start/Assign">Start/Assign</button>
			<button type="button" class="submitbtn"
				onclick="rejectPurchaseRequest();" value="Reject">Reject</button>
		</c:if>

		<c:if
			test="${(userType =='APPROVER_VBS' || userType == 'CONTROLLER_VBS') && actionStatus=='Pending IC approval' }">
			<button type="button" id="rejectPurchaseRequestBtn" class="submitbtn"
				onclick="rejectPurchaseRequest();" value="Reject">Reject</button>
		</c:if>

		<c:if test="${prDisabled==''}">
			<button type="button" value="cancel" onclick="cancelPR();"
				class="cancelbtn">Cancel</button>

		</c:if>
		<div id="deleteBtnDiv">
			<c:if test="${pr.purchaseRequestId != 0 && prDeleteBtn =='yes'}">
				<button type="button" class="cancelbtn"
					onclick="deletepurchaseRequest();" value="delete">delete</button>
			</c:if>
		</div>

	</div>

<script type="text/javascript">
	$(document).ready(function () {
	    $('#deliverydate').datepicker(
		{
		    showOn: "both",
		      buttonImage: "../../omp_main-theme/images/datepicker.png",
		      buttonImageOnly: true,
		      dateFormat: 'dd/mm/yy',
				onSelect: function(dateText) {
	          var deliveryDate= $(this).datepicker('getDate');
	          var year=$.datepicker.formatDate('yy', deliveryDate);
	          var comparedDate = $.datepicker.parseDate('dd/mm/yy',"01/04/"+year);
	          if(deliveryDate < comparedDate){
	              var newYear=Number(year)-1;
	              $('#fiscalyear').val(newYear+"/"+year);
	        }else{
	        	var newYear=Number(year)+1;
	        	 $('#fiscalyear').val(year+"/"+newYear);
	        }
	         
	    }});
	    
	    $('#deliverydate').blur(function (e) 
	    		{
	    	var deliveryDate= $(this).datepicker('getDate');
	        var year=$.datepicker.formatDate('yy', deliveryDate);
	        var comparedDate = $.datepicker.parseDate('dd/mm/yy',"01/04/"+year);
	        if(deliveryDate < comparedDate){
	            var newYear=Number(year)-1;
	            $('#fiscalyear').val(newYear+"/"+year);
	      }else{
	      	var newYear=Number(year)+1;
	      	 $('#fiscalyear').val(year+"/"+newYear);
	      }
	    	
	    		});
	
	    $(".number").keydown(function (e) {
	        if (e.shiftKey) e.preventDefault();
	        else {
	            var nKeyCode = e.keyCode;
	            //Ignore Backspace and Tab keys and comma
	            if (nKeyCode == 8 || nKeyCode == 9 || (nKeyCode == 188 && this.value.split(',').length <= 1) ) return;
	            if (nKeyCode < 95) {
	                if (nKeyCode < 48 || nKeyCode > 57) e.preventDefault();
	            } else {
	                if (nKeyCode < 96 || nKeyCode > 105) e.preventDefault();
	            }
	        }
	    });
	});

</script>


	<br>
	<div class="modal fade" id="addpositionmodal" tabindex="-1"
		role="dialog" aria-labelledby="addpositionmodalLabel"
		aria-hidden="true">
		<div class="modal-dialog">

			<form name="positionForm" id="positionForm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">
							Position No: <span id="labelNumberText"></span>
						</h4>
					</div>
					<div class="modal-body">
						<div id="positionFormContainer" style="display: none;">
							<div id="positionErrorMSG"></div>

							<input type="hidden" name="positionId" id="positionId" /> <input
								type="hidden" name="positionStatus" id="positionStatus" value="" />
							<input name="labelNumber" id="labelNumber" type="hidden"
								value="10" />

							<div class="ompformlable">
								<label for="iandc">I&C</label>
							</div>
							<div class="ompforminput">
								<select name="iandc" id="iandc" ${prDisabled}>
									<option selected value="3">Servizi</option>
									<option value="" ></option>
								</select>
							</div>
							<div class="ompformlable">
								<label for="positionStatusLabel">Position Status</label>
							</div>
							<div class="ompforminput">
								<input name="positionStatusLabel" id="positionStatusLabel"
									type="text" value="New" disabled />
							</div>
							<div class="ompformlable">
								<label for="approverName">Approver Name</label>
							</div>
							<div class="ompforminput">
								<input name="approverName" id="approverName" type="text"
									value="N/A" disabled />
							</div>
							<div class="ompformlable">
								<label for="fiscalyear">Fiscal Year</label>
							</div>
							<div class="ompforminput">
								<input name="fiscalyear" id="fiscalyear" type="text" value=""
									disabled />
							</div>
							<div class="ompformlable">
								<label for="categorycode">Category Code</label>
							</div>
							<div class="ompforminput">
								<input name="categorycode" id="categorycode" type="text"
									value="" ${prDisabled} />
							</div>

							<div class="ompformlable">
								<label for="offernumber">Offer Number</label>
							</div>
							<div class="ompforminput">
								<input name="offernumber" id="offernumber" type="text" value=""
									${prDisabled} />
							</div>

							<div class="ompformlable">
								<label for="deliveryadress">Indirizzo di esecuzione attivit�</label>
							</div>
							<div class="ompforminput">
								<input name="deliveryadress" id="deliveryadress" type="text"
									value="" ${prDisabled} />
							</div>
							<div class="ompformlable">
								<label for="asset-number">Asset Number</label>
							</div>
							<div class="ompforminput">
								<input name="asset-number" id="asset-number" type="text"
									value="" ${assetNumberDisabled} />
							</div>
							
							<div class="clear"></div>
							<div class="ompformlable">
								<label for="prcs-number">PR/SC Number</label>
							</div>
							<div class="ompforminput">
								<input name="prcs-number" id="prcs-number" type="text" value=""
									${prcsNumberDisabled} />
							</div>
							<div class="ompformlable">
								<label for="po-number">PO Number</label>
							</div>
							<div class="ompforminput">
								<input name="po-number" id="po-number" type="text" value=""
									${poNumberDisabled} />
							</div>

							<div class="clear"></div>
							<div class="ompformlable">
								<label for="quantity">Quantity</label>
							</div>
							<div class="ompforminput">
								<input name="quantity" id="quantity" type="text" value=""
									class="number" ${prDisabled} />
							</div>
							<div class="ompformlable">
								<label for="actual-unit-cost">Actual Unit Cost</label>
							</div>
							<div class="ompforminput">
								<input name="actual-unit-cost" id="actual-unit-cost" type="text"
									value="" data-toggle="tooltip" data-placement="top"
									data-original-title='Only " , " allowed as a decimal seperator'
									class="number tltp" ${prDisabled} />
							</div>


							<div class="clear"></div>
							<div class="ompformlable">
								<label for="position-description">Description</label>
							</div>
							<div class="ompforminput">
								<div class="counter"></div>
								<textarea class="txtareamax" rows="4" cols="30"
									name="position-description" id="position-description"
									${prDisabled}></textarea>
							</div>
							<div class="clear"></div>

						</div>
					</div>
					<div class="modal-footer">
						<div class="formactionbuttons">
							<c:if test="${userType != 'OBSERVER' && userType != 'APPROVER_VBS'}">
								<input type="button" id="editPositionBtn" class="submitbtn"
									value="Update"
									onClick="addUpdatePosition('addUpdate');return false;">
							</c:if>
							<hp:if-hasrole userId="${user.userId}" roleName="OMP_ENG_VBS">
								<c:if test="${userType == 'OBSERVER' }">
									<input type="button" id="editPositionBtn" class="submitbtn"
										value="Update"
										onClick="addUpdatePosition('addUpdate');return false;">
								</c:if>
							</hp:if-hasrole>

							<input type="button" id="cancelEditPositionBtn" class="cancelbtn"
								value="Cancel" data-dismiss="modal"
								onClick="resetPositionForm();">
							<c:if test="${userType == 'CONTROLLER_VBS' || (userType == 'ENG_VBS' && (actionStatus=='Created'|| actionStatus=='Rejected') )}">
								<input type="button" id="deletePositionBtn" value="Delete"
									class="cancelbtn"
									onClick="addUpdatePosition('delete');return false;">
							</c:if>

							<input type="button" id="approvePositionBtn" value="Approve"
								class="submitbtn" style="display: none"
								onClick="addUpdatePosition('approve');return false;">

						</div>
					</div>
				</div>
				<!-- /.modal-content -->
			</form>
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->