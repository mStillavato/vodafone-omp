<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
	
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>

<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">  

<portlet:actionURL name="searchFilter" var="searchFilter" >
	<portlet:param name="redirect" value="${redirect}" />
</portlet:actionURL>

<aui:form  class="cmxform" action="${searchFilter}" method="post" id="searchFilterForm">
	<table>
		<tr>
			<td>
				<aui:input label="Tracking Code or Description" name="trackingCodeName" type="text" value=""></aui:input>
			</td>
			<td>
				<br>
				<aui:button value="Search" type="submit" />
			</td>
		</tr>
	</table>				
</aui:form>

<div id="trackingCodeList">
	<%@include file="/html/trackingcodeadmin/trackingtable.jsp"%>
</div>

<portlet:renderURL var="addTrackingCode">
    <portlet:param name="jspPage" value="/html/trackingcodeadmin/trackingform.jsp" />
</portlet:renderURL>
<a href="${addTrackingCode}" class="actionbutn" >Add New Tracking Code</a>

<portlet:renderURL var="batchUploadTrackingCode">
    <portlet:param name="jspPage" value="/html/trackingcodeadmin/trackinguploadform.jsp" />
</portlet:renderURL>
<a href="${batchUploadTrackingCode}" class="actionbutn">Upload batch</a>

<portlet:resourceURL var="exportTrackingCodeUrl" id="exportTrackingCode" escapeXml="false" >
	<portlet:param name="action" value="exportTrackingCode"/>
</portlet:resourceURL>
<a href="javascript:void(null);" class="actionbutn" onclick="<portlet:namespace/>exportTrackingCode()">Export Tracking Code</a>

<script type="text/javascript">
	function <portlet:namespace/>deleteTracking(url){
	
		if(confirm("Do you want to delete this Tracing Code?")) {
			jQuery.get(url, function( data ) {
				  jQuery( "#trackingCodeList").html( data );
				 
					  jQuery("#statusMessage").html("<div><font color='green'>"+jQuery(data).filter("#trackingStatusMessage").html()+"</font</div>");
				  
				  if(jQuery("#statusMessage #totalRecords").length) {
					  jQuery("#statusMessage #totalRecords").remove();
				  }
				});
		}
	}
	
	function <portlet:namespace/>ompListing(url){
		jQuery.get(url,
				 function(data) {
				   jQuery("#trackingCodeList").html(data);
				 }
		);
	}
	
	function <portlet:namespace/>exportTrackingCode(){
		document.location.href ='${exportTrackingCodeUrl}';
	}
</script>