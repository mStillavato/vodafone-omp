<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>   
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
           
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">  
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
        <script type="text/javascript">
<!--
jQuery(document).ready(function() {
	jQuery("#trackingForm").validate();
});
//-->
</script>  

<portlet:actionURL name="addTrackingCode" var="addTrackingCodeURL" >
	<portlet:param name="redirect" value="${redirect}" />
</portlet:actionURL>
  
<div class="adminform">         
	<aui:form  class="cmxform" action="${addTrackingCodeURL}" method="post" id="trackingForm">
		<input type="hidden" name="trackingCodeId" value="${trackingCode.trackingCodeId}"/>
		
		<liferay-ui:error key="duplicate tracking code" 		message="This tracking code is used"/>
		<liferay-ui:error key="duplicate tracking input fields" message="This tracking fields are exist"/>
		
		<aui:input label="Tracking Code " name="trackingCodeName" type="text" value="${trackingCode.trackingCodeName}">
			<aui:validator name="required"/>
		</aui:input>
		<liferay-ui:error key="name-required" message="This field is required"/>
		
		<aui:input label="Tracking Description " name="trackingCodeDescription" type="text" value="${trackingCode.trackingCodeDescription}">
			<aui:validator name="required"/>
		</aui:input>
		<liferay-ui:error key="description-required" message="This field is required"/>
		
		<div class="clear"></div>
		<div class="ompformlable"><label for="projectname">Project Name</label></div>
	    <div class="ompforminput">
			<select class="combobox pproject" id="projectname" name="projectname" required>
				<option  value="">Select Project..</option>
				<c:forEach items="${allProjects}" var="project">
					<c:if test="${project.display eq true || project.projectId eq trackingCode.projectId}">
						<c:choose>
							<c:when test="${project.projectId eq trackingCode.projectId}">
								<option selected value="${project.projectId}">${project.projectName}</option>
							</c:when>
							<c:otherwise>
								<option value="${project.projectId}">${project.projectName}</option>
							</c:otherwise>
						</c:choose>
					</c:if>
				</c:forEach>
			</select>
			<liferay-ui:error key="project-required" message="This field is required"/>
	    </div>
	    
        <div class="clear"></div>
        <div class="ompforminput">
			<div id="wbscodeSelectboxDiv" ><div class="ompformlable"><label for="wbscode">WBS Code</label></div>
				<div class="ompforminput">
					<select name="wbscode" id="wbscode" required>
						<option selected value="">Select WBS Code ..</option>
						<c:forEach items="${allWbsCodes}" var="wbscode">
							<c:if test="${wbscode.display eq true || wbscode.wbsCodeId eq trackingCode.wbsCodeId}">
								<c:choose>
									<c:when test="${wbscode.wbsCodeId eq trackingCode.wbsCodeId}">
										<option selected value="${wbscode.wbsCodeId}">${wbscode.wbsCodeName}</option>
									</c:when>
									<c:otherwise>
										<option value="${wbscode.wbsCodeId}">${wbscode.wbsCodeName}</option>
									</c:otherwise>
								</c:choose>
							</c:if>
						</c:forEach>
					</select>
				</div>
				<liferay-ui:error key="wbscode-required" message="This field is required"/>
			</div>
        </div>
        
		<aui:input inlineLabel="top" label="Display" name="display" type="checkbox" checked="${trackingCode.display}" />
		
		<input type="submit" value="Save"/>
	</aui:form>
</div>
