<%@include file="init.jsp"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://hp.com/vfItaly/jsp/taglib" prefix="hp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<portlet:defineObjects />
<portlet:actionURL name="selectRole"  var="selectRoleURL" >	</portlet:actionURL>
This is the <b>Role Selector</b> portlet in View mode.


<!-- <c:if test="${fn:length(userRoles) == 1}">
	<script type="text/javascript">
	document.location.href = '${sendRedirect}';
	</script>
</c:if>-->

<!--<c:if test="${fn:length(userRoles) gt 1}"> -->
	<form action="${selectRoleURL}" name="formName" method="post">
		<c:forEach items="${userRoles}" var="userRole" varStatus="status">
			<input type="radio" name="userSelectedRole" value="${userRole.name}"  <c:if test="${status.first}">checked</c:if>>${userRole.name}<br>
		</c:forEach>
	<input type="submit" />	
	</form>
<!--</c:if> -->
