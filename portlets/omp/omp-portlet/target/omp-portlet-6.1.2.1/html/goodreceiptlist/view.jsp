<%@include file="init.jsp"%>



<portlet:resourceURL var="searchURL"/>
<portlet:resourceURL id="doSearch" var="doSearchURL"/>
<portlet:resourceURL id="getPOSubProjects" var="getPOSubProjectUrl"/>

<ul class="nav nav-tabs" id="myTab">
	<c:if test="${mode eq requesterMode}">
 		<div class="filter">Filtered by</div>
		<li class="active"><a data-toggle="tab" href="javascript:void(null)" onclick="<portlet:namespace/>statusSelected('${searchURL}', 'po-received')">PO Received</a> <div class="nav-tabs-arrow"></div></li>
		<li><a data-toggle="tab" href="javascript:void(null)" onclick="<portlet:namespace/>statusSelected('${searchURL}', 'gr-requested')" >GR Requested</a><div class="nav-tabs-arrow"></div></li>
	</c:if>
	
	<%@include file="minisearch.jsp"%>
</ul>

<div class="clear"></div>

<div id="<portlet:namespace/>prListDiv" class="listingcontainer"></div>
<div class="loading" style='display:none;font-size:30px; color:#eee;margin-top:60px; text-align: center;'>Loading...</div>

<portlet:renderURL var="batchUploadTrackingCode">
    <portlet:param name="jspPage" value="/html/goodreceiptlist/gruploadform.jsp" />
</portlet:renderURL>
<div class="listingtoplinksarea"> 
	<hp:if-hasrole userId="${user.userId}" roleName="OMP_CONTROLLER,OMP_ENG">
		<a href="${batchUploadTrackingCode}" class="listingtoplink">Upload Excel GR</a>
	</hp:if-hasrole>
</div>
 		
<script type="text/javascript">

var <portlet:namespace/>gr_view = "po-received";
var <portlet:namespace/>idOrder = "desc"; 
var <portlet:namespace/>screenNameRequesterOrder = "desc"; 
var <portlet:namespace/>screenNameRecieverOrder = "desc";
var <portlet:namespace/>vendorOrder = "desc"; 
var <portlet:namespace/>subCategoryOrder = "desc"; 
var <portlet:namespace/>totalValueOrder = "desc"; 
var <portlet:namespace/>activityDescriptionOrder = "desc"; 
var <portlet:namespace/>fiscalYearOrder = "desc";
var <portlet:namespace/>orderby = "purchaseOrderId"; 
var <portlet:namespace/>order = "desc"; 
<portlet:namespace/>ompListing('${searchURL}');

function <portlet:namespace/>ompListing(url){
	if("on" == advancedSearch){// defined in searchfilters.jsp
		var formData = <portlet:namespace/>poSearchFormData("<portlet:namespace/>poSearchForm");
		 url = url + "&searchAction=doPoSearch&gr_view="+<portlet:namespace/>gr_view+"&"+formData;
	}
	jQuery("#<portlet:namespace/>prListDiv").fadeOut( "fast" );
	jQuery(".loading").fadeIn( "normal" );
	jQuery.post(url,{gr_view:<portlet:namespace/>gr_view,orderby:<portlet:namespace/>orderby, order:<portlet:namespace/>order},
			 function(data) {
			   jQuery("#<portlet:namespace/>prListDiv").html(data);
			   jQuery(".loading").fadeOut( "fast" );
			   jQuery("#<portlet:namespace/>prListDiv").fadeIn( "fast" );
			 }
		);
	}


function <portlet:namespace/>statusSelected(url,status){
	advancedSearch = "off";
	<portlet:namespace/>gr_view = status;
	<portlet:namespace/>ompListing(url);
}
function orderPOList(orderby){
	<portlet:namespace/>orderby = orderby.id;
	if(orderby.id == "purchaseOrderId"){
		if (<portlet:namespace/>idOrder == "desc") {
			<portlet:namespace/>order="asc"; <portlet:namespace/>idOrder = "asc"; 
		} else {
			<portlet:namespace/>order="desc";<portlet:namespace/>idOrder = "desc";
		}
	} else if(orderby.id == "screenNameRequester"){
		if (<portlet:namespace/>screenNameRequesterOrder == "desc") {
			<portlet:namespace/>order="asc"; <portlet:namespace/>screenNameRequesterOrder = "asc";
		} else {
			<portlet:namespace/>order="desc"; <portlet:namespace/>screenNameRequesterOrder = "desc";
		}
	} else if(orderby.id == "screenNameReciever"){
		if (<portlet:namespace/>screenNameRecieverOrder == "desc") {
			<portlet:namespace/>order="asc"; <portlet:namespace/>screenNameRecieverOrder = "asc";
		} else {
			<portlet:namespace/>order="desc"; <portlet:namespace/>screenNameRecieverOrder = "desc";
		}
	}else if(orderby.id == "vendorName") {
		if (<portlet:namespace/>vendorOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>vendorOrder = "asc";
		} else {
			<portlet:namespace/>order="desc"; <portlet:namespace/>vendorOrder = "desc";	
		}
	}else if(orderby.id == "totalValue") { 
		if (<portlet:namespace/>totalValueOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>totalValueOrder = "asc";
		} else {
			<portlet:namespace/>order="desc";<portlet:namespace/>totalValueOrder = "desc";
		}
	}else if(orderby.id == "activityDescription") { 
		if (<portlet:namespace/>activityDescriptionOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>activityDescriptionOrder = "asc";
		} else {
			<portlet:namespace/>order="desc";<portlet:namespace/>activityDescriptionOrder = "desc";
		}
	}
	else if(orderby.id == "fiscalYear") { 
		if (<portlet:namespace/>fiscalYearOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>fiscalYearOrder = "asc";
		} else {
			<portlet:namespace/>order="desc";<portlet:namespace/>fiscalYearOrder = "desc";
		}
	}
	<portlet:namespace/>ompListing('${searchURL}');
}
</script>