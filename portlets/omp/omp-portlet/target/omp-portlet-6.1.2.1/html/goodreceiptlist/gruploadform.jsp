<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<link href="../../omp_main-theme/css/admin.css" rel="stylesheet"> 

<portlet:actionURL name="uploadBatchExcelFile" var="uploadGRExcelFile" >
	<portlet:param name="redirect" value="${redirect}" />
</portlet:actionURL>

<div class="adminform">
	<form method="post" enctype="multipart/form-data" name="form" action="${uploadGRExcelFile}">
		<input type="file" name="fileName" id="fileName" value="Select File" class="input" required="required"/>
		<input type="submit" name="upload" id="upload" value="Upload" />
 	</form>
 	
	<c:if test="${not empty uploadedBatchErrors }">
		<c:if test="${totalRecords > 0 }">
	 		<div>The total Number of Records is ${totalRecords}<br /> ${successRecords } records updated successfuly <br /> ${faliedRecords} records have errors</div>
	 	</c:if>
	</c:if>
 		
 	<div class="uploaderrors" style="float:left;">
 		<c:if test="${not empty uploadedBatchErrors}">
 			<c:forEach  items="${uploadedBatchErrors}" var="error">
 				<div class="portlet-msg-error"><span style="padding-right:10px;">Row #${error.key}:</span> ${error.value }</div>
 			</c:forEach>
 		</c:if>
	</div>	
	
	<div class="clear"></div>
</div>
