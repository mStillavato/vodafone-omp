<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>

<div>
	<font color="green">${successMSG}</font> <br/> <font color="red">${errorMSG}</font>
</div>
<div class="filter">Filtered by</div>
<div class="idsearch">
	<div class="normalsearchcontainer">
		<form method="post" id="global">
			<input  type="text" class="idinput" name="IdSearchfield" id="<portlet:namespace/>searchPr" placeholder="Search Vendor..."/>
			<input type="button" value="Submit" class="submitbtn" onclick="<portlet:namespace/>prSearch('${searchPrURL}')"/>
		</form> 
	</div>
</div>
	
<display:table decorator="com.hp.omp.decorator.CatalogoListDecorator" name="catalogoList" list="catalogoList" id="catalogo">
  <display:column property="chiaveInforecord" title="Chiave Inforecord" />
  <display:column property="fornitoreDesc" title="Vendor" />
</display:table>
<%@include file="/html/tablepaging/paging.jsp"%>