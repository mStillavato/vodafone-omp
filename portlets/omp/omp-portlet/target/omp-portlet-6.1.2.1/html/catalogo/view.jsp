<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<script type="text/javascript" src="../../omp_main-theme/js/jquery-1.10.2.min.js"></script>  
<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">  
<portlet:defineObjects />

<div id="<portlet:namespace/>budgetCategoryTableList"  class="listingcontainer"></div>
<div class="loading" style='display:none;font-size:30px; color:#eee;margin-top:60px; text-align: center;'>Loading...</div>
<div>
<portlet:renderURL var="addbudgetCategoryURL">
    <portlet:param name="jspPage" value="/html/budgetcategoryadmin/edit.jsp" />
</portlet:renderURL>

<portlet:resourceURL var="deletebudgetCategoryURL">
	<portlet:param name="action" value="delete"/>
</portlet:resourceURL>
<portlet:resourceURL var="paginationURL">
</portlet:resourceURL>
 <script type="text/javascript">
 function removeBudgetCategory(budgetCategoryId, budgetCategoryName){
	 if(confirm("Do you want to delete this budgetCategory ["+budgetCategoryName+"]?")) {
		 var url="${deletebudgetCategoryURL}&budgetCategoryId="+budgetCategoryId+"&budgetCategoryName="+budgetCategoryName;
			jQuery.get(url,function( data ) {
				  jQuery( "#<portlet:namespace/>budgetCategoryTableList" ).html( data );
				});
			}
 		}
 
 
 function <portlet:namespace/>ompListing(url){
		jQuery("#<portlet:namespace/>budgetCategoryTableList").fadeOut( "fast" );
		jQuery(".loading").fadeIn( "normal" );
		jQuery.post(url+"&action=pagination",
				 function(data) {
				   jQuery("#<portlet:namespace/>budgetCategoryTableList").html(data);
				   jQuery(".loading").fadeOut( "fast" );
				   jQuery("#<portlet:namespace/>budgetCategoryTableList").fadeIn( "fast" );
				 }
			);
		}
 
 
 
 <portlet:namespace/>ompListing('${paginationURL}');
 </script>
</div>