<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>

<div>
	<font color="green">${successMSG}</font> <br/> <font color="red">${errorMSG}</font>
</div>

<portlet:actionURL name="searchFilter" var="searchFilter" >
	<portlet:param name="redirect" value="${redirect}" />
</portlet:actionURL>  
<aui:form  class="cmxform" action="${searchFilter}" method="post" id="searchFilterForm">
	<table>
		<tr>
			<td>
				<aui:input label="Wbs Name " name="wbsName" type="text" value=""></aui:input>
			</td>
			<td>
				<br>				
				<aui:button value="Search" type="submit" />
			</td>
		</tr>
	</table>				
</aui:form>
<br>

<display:table decorator="com.hp.omp.decorator.WbsCodeListDecorator" name="wbsCodeList" list="wbsCodeList" id="wbsCode">
	<display:column property="wbsCodeId"  title="Delete" class="deleteicon"/>
  	<display:column title="Name" >
	  	<portlet:renderURL var="editwbsCode">
		    <portlet:param name="jspPage" value="/html/wbscodeadmin/edit.jsp" />
		    <portlet:param name="wbsCodeId" value="${wbsCode.wbsCodeId}" />
		    <portlet:param name="action" value="update" />
		</portlet:renderURL>
		
	  	<a href="${editwbsCode}">${wbsCode.wbsCodeName}</a>
  	</display:column>
  	<display:column property="description" title="Description" />
  	<display:column property="wbsOwnerName" title="WbsOwnerName" />
  	<display:column property="display" title="Display" />
</display:table>

<%@include file="/html/tablepaging/paging.jsp"%>