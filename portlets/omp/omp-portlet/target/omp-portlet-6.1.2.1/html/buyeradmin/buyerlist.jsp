<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
	
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>

<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">  

<portlet:actionURL name="searchFilter" var="searchFilter" >
	<portlet:param name="redirect" value="${redirect}" />
</portlet:actionURL>

<aui:form  class="cmxform" action="${searchFilter}" method="post" id="searchFilterForm">
	<table>
		<tr>
			<td>
				<aui:input label="Buyer Name" name="buyerName" type="text" value=""></aui:input>
			</td>
			<td>
				<br>
				<aui:button value="Search" type="submit" />
			</td>
		</tr>
	</table>				
</aui:form>

<div id="buyerList">
<%@include file="/html/buyeradmin/buyertable.jsp"%>
</div>

<portlet:renderURL var="addBuyer">
    <portlet:param name="jspPage" value="/html/buyeradmin/buyerform.jsp" />
</portlet:renderURL>
<a href="${addBuyer}" class="actionbutn" >Add New Buyer</a>

<portlet:resourceURL var="exportListUrl" id="exportList" escapeXml="false" >
	<portlet:param name="action" value="exportList"/>
</portlet:resourceURL>
<a href="javascript:void(null);" class="actionbutn" onclick="<portlet:namespace/>exportList()">Export Buyer</a>

<script type="text/javascript">

	function <portlet:namespace/>deleteBuyer(url){
	
		if(confirm("Do you want to delete this Buyer?")) {
			jQuery.get(url, function( data ) {
				  jQuery("#buyerList").html( data );
				 
					  jQuery("#statusMessage").html("<div><font color='green'>"+jQuery(data).filter("#buyerStatusMessage").html()+"</font></div>");
				});
		}
	}

	function <portlet:namespace/>ompListing(url){
		jQuery.get(url,
			 function(data) {
			   jQuery("#buyerList").html(data);
			 }
		);
	}
	
	function <portlet:namespace/>exportList(){
		document.location.href ='${exportListUrl}';
	}
</script>