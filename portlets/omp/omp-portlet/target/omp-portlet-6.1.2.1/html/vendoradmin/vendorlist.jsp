<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>

<div>
	<font color="green">${successMSG}</font> <br/> <font color="red">${errorMSG}</font>
</div>

<portlet:actionURL name="searchFilter" var="searchFilter" >
	<portlet:param name="redirect" value="${redirect}" />
</portlet:actionURL>  
<aui:form  class="cmxform" action="${searchFilter}" method="post" id="searchFilterForm">
	<table>
		<tr>
			<td>
				<aui:input label="Vendor Name " name="vendorName" type="text" value=""></aui:input>
			</td>
			<td>
				<br>				
				<aui:button value="Search" type="submit" />
			</td>
		</tr>
	</table>				
</aui:form>
<br>

<display:table decorator="com.hp.omp.decorator.VendorListDecorator" name="vendorList" list="vendorList" id="vendor">
  <display:column property="vendorId"  title="Delete" class="deleteicon" />
  <display:column  title="Name" >
  
   <portlet:renderURL var="editVendor">
	    <portlet:param name="jspPage" value="/html/vendoradmin/edit.jsp" />
	    <portlet:param name="vendorId" value="${vendor.vendorId}" />
	    <portlet:param name="action" value="update" />
	</portlet:renderURL>
	
  	<a href="${editVendor}">${vendor.vendorName}</a>
  
  </display:column>
  <display:column property="supplier" title="Supplier" />
  <display:column property="supplierCode" title="SupplierCode" />
    <display:column property="display" title="Display" />
  
</display:table>
<%@include file="/html/tablepaging/paging.jsp"%>