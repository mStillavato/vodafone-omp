<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ page import="javax.portlet.PortletPreferences" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<portlet:defineObjects />
<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">  
<portlet:actionURL var="costcenteradminURL">
<portlet:param name="jspPage" value="/html/costcenteradmin/view.jsp" />
</portlet:actionURL>
<div class="adminform">
<aui:form method="post" action="<%=costcenteradminURL%>">
<aui:input label="Cost Center Name: " name="costCenterName" type="text" value="${costCenterName}">
	<aui:validator name="required"/>
</aui:input>
<aui:input label="Cost Center Description: " name="costCenterDesc" type="textarea" value="${costCenterDesc}"/> 

<aui:input name="action" type="hidden" value="${action}"/>

<c:if test="${not empty costCenterId}">
<aui:input name="costCenterId" type="hidden" value="${costCenterId}"/>
</c:if>

<aui:input inlineLabel="top" label="Display" name="display" type="checkbox" checked="${display}" />

<aui:button type="submit" /> 
</aui:form> 
</div>