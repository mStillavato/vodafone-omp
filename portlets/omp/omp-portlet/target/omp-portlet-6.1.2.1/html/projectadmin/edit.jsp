<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ page import="javax.portlet.PortletPreferences" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">  
<portlet:defineObjects />

<portlet:actionURL var="projectadminURL">
<portlet:param name="jspPage" value="/html/projectadmin/view.jsp" />
</portlet:actionURL>
<div class="adminform">
<aui:form method="post" action="<%=projectadminURL%>">
<aui:input label="Project Name: " name="projectName" type="text" value="${projectName}">
	<aui:validator name="required"/>
</aui:input> 
<aui:input label="Project Description: " name="projectDesc" type="textarea" value="${projectDesc}"/> 
<aui:input name="action" type="hidden" value="${action}"/>

<c:if test="${not empty projectId}">
<aui:input name="projectId" type="hidden" value="${projectId}"/>
</c:if>
<aui:input inlineLabel="top" label="Display" name="display" type="checkbox" checked="${display}" />
<aui:button type="submit" /> 
</aui:form> 
</div>