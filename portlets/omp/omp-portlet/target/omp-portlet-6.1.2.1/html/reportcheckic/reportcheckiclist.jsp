<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<portlet:resourceURL var="writeExcelReportCheckIcUrl" id="writeExcelReportCheckIc" escapeXml="false" >
	<portlet:param name="action" value="writeExcelReportCheckIc"/>
</portlet:resourceURL>

<portlet:resourceURL var="searchURL"/>
<div id="<portlet:namespace/>prListDiv" class="listingcontainer">
<div class="loading" style='display:none;font-size:30px; color:#eee;margin-top:60px; text-align: center;'>Loading...</div>

<form name="formReportCheckIc" id="formReportCheckIc" method="post">
	<display:table decorator="com.hp.omp.decorator.ReportCheckIcDecorator" name="reportCheckIcList" list="reportCheckIcList" id="reportCheckIc">
	 	<display:column property="purchaseRequestId" title="OMP ID" />
	 	<display:column property="fiscalYear" title="Fiscal Year" />
	 	<display:column property="purchaseOrderId" title="PO Number" />
	 	<display:column property="evoLocation" title="Evo Location" />
	 	<display:column property="targaTecnica" title="Targa Tecnica" />
	 	<display:column property="icApprover" title="Approver" />
	 	<display:column property="grRequestor" title="GR Requestor" />
	 	<display:column property="grRequestDate" title="GR Request Date" />
	 </display:table>
</form>

<%@include file="/html/tablepaging/paging.jsp"%>

<div class="formactionbuttons">
   	<button type="button" class="exportStyle" onclick="<portlet:namespace/>exportReport();" value="Write">Export Report</button>
</div>
	
</div>

<script type="text/javascript">
	
	function <portlet:namespace/>exportReport() {
		var actionURL;
		actionURL =  '${writeExcelReportCheckIcUrl}';
		document.getElementById("formReportCheckIc").action = actionURL;
		document.getElementById("formReportCheckIc").submit();
	}
	
	function <portlet:namespace/>ompListing(url){
		jQuery("#<portlet:namespace/>prListDiv").fadeOut( "fast" );
		jQuery(".loading").fadeIn( "normal" );
		jQuery.post(url,
				 function(data) {
				   jQuery("#<portlet:namespace/>prListDiv").html(data);
				   jQuery(".loading").fadeOut( "fast" );
				   jQuery("#<portlet:namespace/>prListDiv").fadeIn( "fast" );
				 }
			);
		}
	
</script>
