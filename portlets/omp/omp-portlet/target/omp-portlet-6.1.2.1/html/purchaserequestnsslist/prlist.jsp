<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

	<liferay-ui:error key="Unassgined-CostCenter" message="You don't have a Cost Center number. Please contact the administratro to assign you to a cost center." />

	<display:table decorator="com.hp.omp.decorator.PRListDecorator"  name="prList" id="pr">
	
	
 <display:column property="purchaseRequestId" class="rqstnum" title="<a href='javascript:void(null)' id='purchaseRequestId' onclick='orderRequesterPRList(this)'>OMP Request #</a>" />
 	  <display:column property="screenNameRequester" title="<a href='javascript:void(null)' id='screenNameRequester' onclick='orderRequesterPRList(this)'>Requestor</a>" class="substring"/>
 	  <display:column property="screenNameReciever" title="<a href='javascript:void(null)' id='screenNameReciever' onclick='orderRequesterPRList(this)'>Receiver</a>" />
	  <display:column property="vendorName" title="<a href='javascript:void(null)' id='vendorName' onclick='orderRequesterPRList(this)'>Vendor</a>" class="substring"/>
	  <display:column property="budgetSubCategoryName" title="<a href='javascript:void(null)' id='budgetSubCategoryName' onclick='orderRequesterPRList(this)'>Sub Category</a>" class="substring"/> 
	  <display:column property="foramttedTotalValue" title="<a href='javascript:void(null)' id='totalValue' onclick='orderRequesterPRList(this)'>Total Value</a>" class="substring"/>
	  <display:column property="fiscalYear" title="<a href='javascript:void(null)' id='fiscalYear' onclick='orderRequesterPRList(this)'>Fiscal Year</a>" />
	  <display:column property="activityDescription" title="<a href='javascript:void(null)' id='activityDescription' onclick='orderRequesterPRList(this)'>Description</a>" class="substring"/>
	  <display:column  title="<a href='javascript:void(null)' id='automatic' onclick='orderRequesterPRList(this)'>Type</a>" class="substring">
	  <c:choose>
	  	<c:when test="${pr.automatic}">
	  		Automatic
	  	</c:when>
	  	<c:otherwise>
	  		Manual
	  	</c:otherwise>
	  </c:choose>
	  </display:column>
	  <display:column property="status" title="Status"/>
	</display:table>

<%@include file="/html/tablepaging/paging.jsp"%>
