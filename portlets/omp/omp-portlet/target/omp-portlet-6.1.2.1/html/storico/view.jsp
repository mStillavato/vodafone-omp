<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<portlet:defineObjects />
<%@ taglib uri="http://hp.com/vfItaly/jsp/taglib" prefix="hp" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:actionURL name="generateReport" var="generateReport" />
    
<div class="clear"></div>
<div class="formFrame" >
	<div class=" titleHolder">
	   <div class="blockTitle"><label >Storico</label></div>
	</div>
	
	<form action="" method="post" id="reportForm">
		<div id="reportFailureMSG"></div>
		
		<div class="formItem">
			<div class="formLabel"><label >Fiscal Year</label></div>
			<div class="formField"><select name="fiscalYear" id="fiscalYear" class="combobox">
			<option selected value="">Select Fiscal Year..</option>
				<c:forEach items="${reportDTO.allFiscalYears}" var="fiscalYear">
					<c:choose>
						<c:when test="${fiscalYear eq reportDTO.fiscalYear}">
							<option selected value="${fiscalYear}">${fiscalYear}</option>
						</c:when>
						<c:otherwise>
							<option value="${fiscalYear}">${fiscalYear}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select></div>
		</div>
			
		<div class="clearBoth"></div>
		<div class="btnContainer">
			<button type="button" class="submitbtn" value="Submit" onclick="getReport();">Archivia</button>
		</div>
	</form>
</div>	

<portlet:resourceURL id="exportReport" var="exportReportUrl"/>
<portlet:resourceURL id="exportDetailsReport" var="exportDetailsReportUrl"/>

<script type="text/javascript"> 


function <portlet:namespace/>exportReport(formId){
	var formData = $("#"+formId).serialize();
	var url = '${exportReportUrl}'+'&'+formData;
	window.location = url;
}
<c:if test="${reportDTO.prReport}">
	function <portlet:namespace/>exportDetailsReport(formId){
		var formData = $("#"+formId).serialize();
		var url = '${exportDetailsReportUrl}'+'&'+formData;
		window.location = url;
	}
</c:if>




function getReport(){
	var actionURL;
	actionURL='${generateReport}';
	if(validateOneSelectedListAtLeast()){
			document.getElementById("reportForm").action=actionURL;
			document.getElementById("reportForm").submit();
			
	} else{
		document.getElementById("reportFailureMSG").innerHTML='<div class="alert alert-danger" style="margin:0 20px 20px; color:red;">You should select at least one of the following lists </div>';
	}
	
}

function validateOneSelectedListAtLeast(){
	
	var result = false;
	
	var fYear = document.getElementById("fiscalYear").value;
	var wCode = document.getElementById("wbsCode").value;
	var proj = document.getElementById("<portlet:namespace/>projects").value;
	var targaTecnica = document.getElementById("targaTecnica").value;
	var cCenter = document.getElementById("costCenter").value;
	var currency = document.getElementById("<portlet:namespace/>currency-select").value;
	var prStatus = document.getElementById("<portlet:namespace/>prStatus-select").value;
	
	//alert('fYear '+ fYear+'  wCode '+ wCode +'  proj '+proj +' sProject '+sProject + ' cCenter '+cCenter +' currency '+currency);
	
	if(currency !="" || fYear !="" || wCode !="" || proj !="" || targaTecnica !="" || cCenter !=""){
		result = true;
	} 

	return result;
}


</script>

