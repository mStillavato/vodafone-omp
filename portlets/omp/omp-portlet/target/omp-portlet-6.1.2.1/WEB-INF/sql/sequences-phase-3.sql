 create sequence WBS_CODE_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER WBS_CODE_ID_TRIG
  before insert on WBS_CODE              
  for each row
begin   
  if :NEW.wbsCodeId is null then 
    select WBS_CODE_ID_SEQ.nextval into :NEW.wbsCodeId from dual; 
  end if; 
end;
/


create sequence PROJECT_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER PROJECT_ID_TRIG
  before insert on PROJECT              
  for each row
begin   
  if :NEW.projectId is null then 
    select PROJECT_ID_SEQ.nextval into :NEW.projectId from dual; 
  end if; 
end;

/

--==========================================

create sequence SUB_PROJECT_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER SUB_PROJECT_ID_TRIG
  before insert on SUB_PROJECT              
  for each row
begin   
  if :NEW.subprojectId is null then 
    select SUB_PROJECT_ID_SEQ.nextval into :NEW.subprojectId from dual; 
  end if; 
end;

/

  --==========================================
  create sequence PROJECT_SUB_PROJECT_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER PROJECT_SUB_PROJECT_ID_TRIG
  before insert on PROJECT_SUB_PROJECT              
  for each row
begin   
  if :NEW.projectSubprojectId is null then 
    select PROJECT_SUB_PROJECT_ID_SEQ.nextval into :NEW.projectSubprojectId from dual; 
  end if; 
end;
/

--==========================================

create sequence BUDGET_CATEGORY_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER BUDGET_CATEGORY_ID_TRIG
  before insert on BUDGET_CATEGORY             
  for each row
begin   
  if :NEW.budgetCategoryId is null then 
    select BUDGET_CATEGORY_ID_SEQ.nextval into :NEW.budgetCategoryId from dual; 
  end if; 
end;
/

--==========================================

create sequence BUDGET_SUB_CATEGORY_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER BUDGET_SUB_CATEGORY_ID_TRIG
  before insert on BUDGET_SUB_CATEGORY             
  for each row
begin   
  if :NEW.budgetSubCategoryId is null then 
    select BUDGET_SUB_CATEGORY_ID_SEQ.nextval into :NEW.budgetSubCategoryId from dual; 
  end if; 
end;
/

--==========================================

create sequence VENDOR_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER VENDOR_ID_SEQ_TRIG
  before insert on VENDOR            
  for each row
begin   
  if :NEW.vendorId is null then 
    select VENDOR_ID_SEQ.nextval into :NEW.vendorId from dual; 
  end if; 
end;
/

--==========================================

create sequence MACRO_DRIVER_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER MACRO_DRIVER_ID_SEQ_TRIG
  before insert on MACRO_DRIVER            
  for each row
begin   
  if :NEW.macroDriverId is null then 
    select MACRO_DRIVER_ID_SEQ.nextval into :NEW.macroDriverId from dual; 
  end if; 
end;
/

--==========================================


create sequence MICRO_DRIVER_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER MICRO_DRIVER_ID_SEQ_TRIG
  before insert on MICRO_DRIVER            
  for each row
begin   
  if :NEW.microDriverId is null then 
    select MICRO_DRIVER_ID_SEQ.nextval into :NEW.microDriverId from dual; 
  end if; 
end;
/

  --==========================================
create sequence MACRO_MICRO_DRIVER_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER MACRO_MICRO_DRIVER_ID_SEQ_TRIG
  before insert on MACRO_MICRO_DRIVER            
  for each row
begin   
  if :NEW.macroMicroDriverId is null then 
    select MACRO_MICRO_DRIVER_ID_SEQ.nextval into :NEW.macroMicroDriverId from dual; 
  end if; 
end;
/

--==========================================

create sequence ODNP_NAME_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER ODNP_NAME_ID_SEQ_TRIG
  before insert on ODNP_NAME            
  for each row
begin   
  if :NEW.odnpNameId is null then 
    select ODNP_NAME_ID_SEQ.nextval into :NEW.odnpNameId from dual; 
  end if; 
end;
/