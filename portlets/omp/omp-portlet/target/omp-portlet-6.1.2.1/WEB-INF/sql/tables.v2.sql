alter table GOOD_RECEIPT drop column createdDate;
alter table GOOD_RECEIPT modify requestDate default sysdate;
alter table GOOD_RECEIPT drop column competenceDate;
alter table GOOD_RECEIPT drop column status;
alter TABLE  GOOD_RECEIPT modify (percentage decimal(3,2));