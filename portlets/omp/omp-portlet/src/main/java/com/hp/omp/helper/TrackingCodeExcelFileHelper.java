package com.hp.omp.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import com.hp.omp.model.custom.GrExcelRow;
import com.hp.omp.model.custom.TrackingCodeExcelRow;

public class TrackingCodeExcelFileHelper {
	
	public static List<TrackingCodeExcelRow> readExcelFile(File inputWorkbook) throws IOException  {
		
		FileInputStream file = new FileInputStream(inputWorkbook);
		 
        //Create Workbook instance holding reference to .xlsx file
        XSSFWorkbook workbook = new XSSFWorkbook(file);

        //Get first/desired sheet from the workbook
        XSSFSheet sheet = workbook.getSheetAt(0);

        List<TrackingCodeExcelRow> trackingList = new ArrayList<TrackingCodeExcelRow>(); 
        for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
        	 XSSFRow row = sheet.getRow(rowIndex);
        	 TrackingCodeExcelRow trackingCode = new TrackingCodeExcelRow();
        	  if (row != null) {
        		  for(int columNum = 0 ; columNum < row.getPhysicalNumberOfCells(); columNum++) {
        			  switch (columNum) {
        			  case 0:
        				  if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
        					  trackingCode.setTrackingCode(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
        				  } else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
        					  trackingCode.setTrackingCode(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
        				  }
        				  break;
        			  case 1:
        				  trackingCode.setTrackingDescription(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
        				  break;
        			  case 2:
        				  trackingCode.setProject(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
        				  break;
        			  case 3:
        				  trackingCode.setWbsCode(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
        				  break;
        		  }
        	  }
        	}
        	  trackingList.add(trackingCode);
        }
        file.close();
        return trackingList;
	}
}
