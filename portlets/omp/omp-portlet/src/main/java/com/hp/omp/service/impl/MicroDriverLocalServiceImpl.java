package com.hp.omp.service.impl;

import java.util.List;

import com.hp.omp.model.MacroDriver;
import com.hp.omp.model.MicroDriver;
import com.hp.omp.service.base.MicroDriverLocalServiceBaseImpl;
import com.hp.omp.service.persistence.MacroDriverFinderUtil;
import com.hp.omp.service.persistence.MicroDriverFinderUtil;
import com.hp.omp.service.persistence.MicroDriverUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

/**
 * The implementation of the micro driver local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.MicroDriverLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Ahmed Kamel
 * @see com.hp.omp.service.base.MicroDriverLocalServiceBaseImpl
 * @see com.hp.omp.service.MicroDriverLocalServiceUtil
 */
public class MicroDriverLocalServiceImpl extends MicroDriverLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.hp.omp.service.MicroDriverLocalServiceUtil} to access the micro driver local service.
     */
	
	public List<MicroDriver> getMicroDriverByMacroDriverId(Long macroDriverId) throws SystemException{
		return MicroDriverFinderUtil.getMicroDriverByMacroDriverId(macroDriverId);
		
	}
	
	public MicroDriver getMicroDriverByName(String name) throws SystemException{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(MicroDriver.class);
		dynamicQuery.add(RestrictionsFactoryUtil.ilike("microDriverName",name));
		 
		MicroDriver microDriver = null;
		List<MicroDriver>  microDriverList =MicroDriverUtil.findWithDynamicQuery(dynamicQuery);
		if(microDriverList != null && ! (microDriverList.isEmpty()) ){
			microDriver = microDriverList.get(0);
		}
		
		return microDriver;
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<MicroDriver> getMicroDriversList(int start, int end,
			String searchFilter) throws SystemException {
		List<MicroDriver> resultQuery = null;
		resultQuery= MicroDriverFinderUtil.getMicroDriversList(start, end, searchFilter);
		return resultQuery;
	}
}
