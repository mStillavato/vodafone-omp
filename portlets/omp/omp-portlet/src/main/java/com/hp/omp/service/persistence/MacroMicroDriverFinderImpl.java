package com.hp.omp.service.persistence;

import java.util.ArrayList;
import java.util.List;

import com.hp.omp.model.MacroDriver;
import com.hp.omp.model.MicroDriver;
import com.hp.omp.model.SubProject;
import com.hp.omp.model.impl.MacroDriverImpl;
import com.hp.omp.model.impl.MicroDriverImpl;
import com.hp.omp.model.impl.SubProjectImpl;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class MacroMicroDriverFinderImpl extends MacroMicroDriverPersistenceImpl implements MacroMicroDriverFinder{
	
	private static Log _log = LogFactoryUtil.getLog(MacroMicroDriverFinderImpl.class);
	static final String GET_MACRO_DRIVERS_FOR_MICRO = MacroMicroDriverFinderImpl.class.getName()+".getMacroDriversForMicro";

	@SuppressWarnings("unchecked")
	public List<MacroDriver> getMacroDriversByMicroDriverId(Long microDriverId) throws SystemException {
		Session session = null;
		List<MacroDriver> list = new ArrayList<MacroDriver>();

		try {
			session = openSession();
			
			String sql = CustomSQLUtil.get(GET_MACRO_DRIVERS_FOR_MICRO);
			if(_log.isDebugEnabled()){
				_log.debug("SQL=["+ sql+"]");
				_log.debug("value=["+ microDriverId+"]");
			}
			SQLQuery query = session.createSQLQuery(sql);
			
			if(query == null){
				_log.error("get Macro drivers error, no sql found");
				return null;
			}
			query.setLong(0, microDriverId);
			query.addEntity("MacroDriver", MacroDriverImpl.class);
			
			list = (List<MacroDriver>)QueryUtil.list(query, getDialect(),  QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			
		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
		
		return list;
	}

}
