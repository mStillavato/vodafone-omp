package com.hp.omp.service.impl;

import java.util.List;

import com.hp.omp.model.MacroDriver;
import com.hp.omp.service.base.MacroDriverLocalServiceBaseImpl;
import com.hp.omp.service.persistence.MacroDriverFinderUtil;
import com.hp.omp.service.persistence.MacroDriverUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

/**
 * The implementation of the macro driver local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.MacroDriverLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Ahmed Kamel
 * @see com.hp.omp.service.base.MacroDriverLocalServiceBaseImpl
 * @see com.hp.omp.service.MacroDriverLocalServiceUtil
 */
public class MacroDriverLocalServiceImpl extends MacroDriverLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.hp.omp.service.MacroDriverLocalServiceUtil} to access the macro driver local service.
     */
	
	public MacroDriver getMacroDriverByName(String name) throws SystemException{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(MacroDriver.class);
		dynamicQuery.add(RestrictionsFactoryUtil.ilike("macroDriverName",name));
		 
		MacroDriver macroDriver = null;
		List<MacroDriver>  macroDriverList =MacroDriverUtil.findWithDynamicQuery(dynamicQuery);
		if(macroDriverList != null && ! (macroDriverList.isEmpty()) ){
			macroDriver = macroDriverList.get(0);
		}
		
		return macroDriver;
	}
	

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<MacroDriver> getMacroDrivers(int start, int end,
			String searchFilter) throws SystemException {
		List<MacroDriver> resultQuery = null;
		resultQuery= MacroDriverFinderUtil.getMacroDrivers(start, end, searchFilter);
		return resultQuery; 
	}
}
