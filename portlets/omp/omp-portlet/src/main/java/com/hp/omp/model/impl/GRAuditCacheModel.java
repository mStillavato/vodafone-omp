package com.hp.omp.model.impl;

import com.hp.omp.model.GRAudit;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing GRAudit in entity cache.
 *
 * @author HP Egypt team
 * @see GRAudit
 * @generated
 */
public class GRAuditCacheModel implements CacheModel<GRAudit>, Serializable {
    public long auditId;
    public Long goodReceiptId;
    public int status;
    public Long userId;
    public long changeDate;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{auditId=");
        sb.append(auditId);
        sb.append(", goodReceiptId=");
        sb.append(goodReceiptId);
        sb.append(", status=");
        sb.append(status);
        sb.append(", userId=");
        sb.append(userId);
        sb.append(", changeDate=");
        sb.append(changeDate);
        sb.append("}");

        return sb.toString();
    }

    public GRAudit toEntityModel() {
        GRAuditImpl grAuditImpl = new GRAuditImpl();

        grAuditImpl.setAuditId(auditId);
        grAuditImpl.setGoodReceiptId(goodReceiptId);
        grAuditImpl.setStatus(status);
        grAuditImpl.setUserId(userId);

        if (changeDate == Long.MIN_VALUE) {
            grAuditImpl.setChangeDate(null);
        } else {
            grAuditImpl.setChangeDate(new Date(changeDate));
        }

        grAuditImpl.resetOriginalValues();

        return grAuditImpl;
    }
}
