package com.hp.omp.portlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.exceptions.UserHasNoCostCenterException;
import com.hp.omp.helper.OmpHelper;
import com.hp.omp.model.CostCenter;
import com.hp.omp.model.CostCenterUser;
import com.hp.omp.model.Project;
import com.hp.omp.model.Vendor;
import com.hp.omp.model.WbsCode;
import com.hp.omp.model.custom.Currency;
import com.hp.omp.model.custom.GruppoUsers;
import com.hp.omp.model.custom.OmpFormatterUtil;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.custom.PurchaseRequestStatus;
import com.hp.omp.model.custom.ReportDTO;
import com.hp.omp.service.CostCenterLocalServiceUtil;
import com.hp.omp.service.CostCenterUserLocalServiceUtil;
import com.hp.omp.service.ProjectLocalServiceUtil;
import com.hp.omp.service.ReportLocalServiceUtil;
import com.hp.omp.service.VendorLocalServiceUtil;
import com.hp.omp.service.WbsCodeLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class ReportPortlet
 */
public class ReportPortlet extends MVCPortlet {
	
	Logger logger = LoggerFactory.getLogger(ReportPortlet.class);
	
	@ProcessAction(name="generateReport")
	public void generateReport(ActionRequest actionRequest, ActionResponse actionResponse){
		try {
			ReportDTO reportDTO = getReportDTO(actionRequest);
			if(reportDTO == null){
				actionRequest.setAttribute("reportDTO", null);
				return;
			}
			
			Double closed = ReportLocalServiceUtil.generateAggregateReportClosed(reportDTO);
			Double closing = ReportLocalServiceUtil.generateAggregateReportClosing(reportDTO);
			Double total = ReportLocalServiceUtil.generateAggregateReportTotalPoistionValues(reportDTO);
			
			Double comitted = total - (closed + closing);
			
			Double prTotal=ReportLocalServiceUtil.generateAggregatePrPositionsWithoutPoNumberValue(reportDTO);
			Double aggregateTotal=total+prTotal;
			
			if(Currency.EUR.getCode() == reportDTO.getCurrency()){
				reportDTO.setClosedEUR(OmpFormatterUtil.formatNumberByLocale(closed, Locale.ITALY));
				reportDTO.setClosingEUR(OmpFormatterUtil.formatNumberByLocale(closing, Locale.ITALY));
				reportDTO.setComittedEUR(OmpFormatterUtil.formatNumberByLocale(comitted, Locale.ITALY));
				reportDTO.setTotalEUR(OmpFormatterUtil.formatNumberByLocale(total, Locale.ITALY));
				reportDTO.setPrTotalEUR(OmpFormatterUtil.formatNumberByLocale(prTotal, Locale.ITALY));
				reportDTO.setAggregateTotalEUR(OmpFormatterUtil.formatNumberByLocale(aggregateTotal, Locale.ITALY));
				
				actionRequest.setAttribute("EUR", "selected");
			} else if(Currency.USD.getCode() == reportDTO.getCurrency()){
				//Default USD
				reportDTO.setClosedUSD(OmpFormatterUtil.formatNumberByLocale(closed, Locale.ITALY));
				reportDTO.setClosingUSD(OmpFormatterUtil.formatNumberByLocale(closing, Locale.ITALY));
				reportDTO.setComittedUSD(OmpFormatterUtil.formatNumberByLocale(comitted, Locale.ITALY));
				reportDTO.setTotalUSD(OmpFormatterUtil.formatNumberByLocale(total, Locale.ITALY));
				reportDTO.setPrTotalUSD(OmpFormatterUtil.formatNumberByLocale(prTotal, Locale.ITALY));
				reportDTO.setAggregateTotalUSD(OmpFormatterUtil.formatNumberByLocale(aggregateTotal, Locale.ITALY));
				
				actionRequest.setAttribute("USD", "selected");
			} else{
				//Default ALL
				reportDTO.setClosedEUR(OmpFormatterUtil.formatNumberByLocale(closed, Locale.ITALY));
				reportDTO.setClosingEUR(OmpFormatterUtil.formatNumberByLocale(closing, Locale.ITALY));
				reportDTO.setComittedEUR(OmpFormatterUtil.formatNumberByLocale(comitted, Locale.ITALY));
				reportDTO.setTotalEUR(OmpFormatterUtil.formatNumberByLocale(total, Locale.ITALY));
				reportDTO.setPrTotalEUR(OmpFormatterUtil.formatNumberByLocale(prTotal, Locale.ITALY));
				reportDTO.setAggregateTotalEUR(OmpFormatterUtil.formatNumberByLocale(aggregateTotal, Locale.ITALY));
				
				actionRequest.setAttribute("EUR", "selected");
			}

			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			
			reportDTO = populate(reportDTO,themeDisplay);
			actionRequest.setAttribute("reportDTO", reportDTO);
			

		} catch (SystemException e) {
			processException(e);
		}
	}
	

	private List<Object> getDetailsReportHeader() {
		List<Object> header = new ArrayList<Object>();

		header.add("OMP_PURCHASE_REQUEST");		//00
		header.add("STATUS PR");				//01
		header.add("FISCALYEAR");				//02
		header.add("REQUESTOR");				//03
		header.add("CDC");						//04
		header.add("PR_SC_NUMBER");				//05
		header.add("PR_POSITION");				//06
		header.add("VALUTA");					//07
		header.add("PR_TOTAL_VALUE");			//08
		header.add("PONUMBER");					//09
		header.add("PO_POSITION");				//10
		header.add("QUANTITY");					//11
		header.add("ACTUALUNITCOST");			//12
		header.add("PO_VALUE");					//13
		header.add("WBS_CODE");					//14
		header.add("RECEIVER");					//15
		header.add("TRACKING NUMBER");			//16
		header.add("EVOCODMATERIALE");			//17
		header.add("EVODESMATERIALE");			//18
		header.add("PR_DESCRIPTION");			//19
		header.add("POS_DESCRIPTION");			//20
		header.add("VENDOR");					//21
		header.add("PROJECT");					//22
		header.add("GR_NUMBER");				//23
		header.add("GR_REQUEST_DATE");			//24
		header.add("POSITION_COST");			//25
		header.add("PERCENTAGE");				//26
		header.add("GRVALUE");					//27
		header.add("GR_REGISTRATION_DATE");		//28
		header.add("CATEGORYCODE");				//29
		header.add("OFFERNUMBER");				//30
		header.add("OFFERDATE");				//31
		header.add("DELIVERYDATE");				//32
		header.add("DELIVERYADDRESS");			//33
		header.add("ASSETNUMBER");				//34
		header.add("EVOLOCATION");				//35
		header.add("PLANT");					//36
		header.add("TARGATECNICA");				//37
		header.add("IC_FLAG");					//38
		header.add("APPROVED");					//39
		header.add("IC_APPROVER");				//40
		header.add("BUYER");					//41
		header.add("OLA");						//42
		header.add("GR REQUESTER");				//43
		header.add("PR_CREATED_DATE");			//44
		header.add("WBS_OWNER");				//45
		return header;
	}
	
	private List<Object> getReportHeader() {
		List<Object> header = new ArrayList<Object>();
		header.add("PR NUMBER");//0
		header.add("PR POSITION");//1
		header.add("OMP ID");//2
		header.add("PO ID");//3
		header.add("PO POSITION");//4
		header.add("GR Number");//5
		header.add("GR Registeration Date");//6
		header.add("PO DATA");//7
		header.add("CDC");//8
		header.add("Requestor");//9
		header.add("Tracking number");//10
		header.add("WBS");//11
		header.add("Vendor");//12
		header.add("GR Value");//13
		header.add("GR Percentage");//14
		header.add("Position Value");//15
		header.add("Position Value Committed"); //16
		header.add("DELIVERY DATE");//17
		header.add("Fiscal Year");//18
		header.add("Description");//19
		header.add("Status");//20
		return header;
	}


	private ReportDTO getReportDTO(PortletRequest request) {
		ReportDTO reportDTO = new ReportDTO();
		boolean emptyAttributes = true;
		
		String fiscalYear = ParamUtil.getString(request, "fiscalYear");
		String wbsCode = ParamUtil.getString(request, "wbsCode");
		Long projectId = ParamUtil.getLong(request, "project");
		String costCenter = ParamUtil.getString(request, "costCenter");
		String currency = ParamUtil.getString(request, "currency");
		String targaTecnica = ParamUtil.getString(request, "targaTecnica");
		String prStatus = ParamUtil.getString(request, "prStatus");
		Long vendorId = ParamUtil.getLong(request, "vendor");

		if(!"".equalsIgnoreCase(prStatus)){
			reportDTO.setPrStatus(ParamUtil.getInteger(request, "prStatus"));
			emptyAttributes = false;
		}
		
		if(!"".equalsIgnoreCase(currency)){
			reportDTO.setCurrency(ParamUtil.getInteger(request, "currency"));
			emptyAttributes = false;
		}else{
			reportDTO.setCurrency(Currency.ALL.getCode());
			emptyAttributes = false;
		}
		
		if (!"".equalsIgnoreCase(fiscalYear)) {
			reportDTO.setFiscalYear(fiscalYear);
			emptyAttributes = false;
		}
		if (!"".equalsIgnoreCase(wbsCode)) {
			reportDTO.setWbsCode(wbsCode);
			try {
				WbsCode wbsCodeName = WbsCodeLocalServiceUtil.getWbsCode(Long.valueOf(wbsCode));
				reportDTO.setWbsCodeName(wbsCodeName.getWbsCodeName());
				
			} catch (NumberFormatException e) {
					_log.error("error in get reprot DTO",e);
			} catch (SystemException e) {
				_log.error("error in get reprot DTO",e);
			} catch (PortalException e) {
				_log.error("error in get reprot DTO",e);
			}
			emptyAttributes = false;
		}
		if (projectId != null && projectId !=0) {
			reportDTO.setProjectId(projectId);
			try {
				Project project = ProjectLocalServiceUtil.getProject(Long.valueOf(projectId));
				reportDTO.setProjectName(project.getProjectName());
				
			} catch (NumberFormatException e) {
					_log.error("error in get reprot DTO",e);
			} catch (SystemException e) {
				_log.error("error in get reprot DTO",e);
			} catch (PortalException e) {
				_log.error("error in get reprot DTO",e);
			}
			emptyAttributes = false;
		}
		
		if (vendorId != null && vendorId !=0) {
			reportDTO.setVendorId(vendorId);
			try {
				Vendor vendor = VendorLocalServiceUtil.getVendor(Long.valueOf(vendorId));
				reportDTO.setVendorName(vendor.getVendorName());
				
			} catch (NumberFormatException e) {
					_log.error("error in get reprot DTO",e);
			} catch (SystemException e) {
				_log.error("error in get reprot DTO",e);
			} catch (PortalException e) {
				_log.error("error in get reprot DTO",e);
			}
			emptyAttributes = false;
		}
		

		if (!"".equalsIgnoreCase(costCenter)) {
			reportDTO.setCostCenterId(Long.valueOf(costCenter));
			try {
				CostCenter cc = CostCenterLocalServiceUtil.getCostCenter(Long.valueOf(costCenter));
				reportDTO.setCostCenterName(cc.getCostCenterName());
				
			} catch (NumberFormatException e) {
					_log.error("error in get reprot DTO",e);
			} catch (SystemException e) {
				_log.error("error in get reprot DTO",e);
			} catch (PortalException e) {
				_log.error("error in get reprot DTO",e);
			}
			emptyAttributes = false;
		}
		
		if(!"".equalsIgnoreCase(targaTecnica)){
			reportDTO.setTargaTecnica(ParamUtil.getString(request, "targaTecnica"));
			emptyAttributes = false;
		}

		if(emptyAttributes){
			return null;
		}
		
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			long userId = themeDisplay.getUserId();
			reportDTO.setUserType((OmpHelper.getUserRole(userId)).getLabel());
		} catch (PortalException e) {
			_log.error("error in get reprot DTO", e);
		} catch (SystemException e) {
			_log.error("error in get reprot DTO", e);
		}
		
		return reportDTO;
	}


	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		ReportDTO reportDTO = null;
		Object reportDTOObject = renderRequest.getAttribute("reportDTO");
		if(reportDTOObject != null && reportDTOObject instanceof ReportDTO){
			reportDTO = (ReportDTO)reportDTOObject;
		}else{
			reportDTO = new ReportDTO();
		}
		
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		
		
		reportDTO = populate(reportDTO,themeDisplay);
		renderRequest.setAttribute("reportDTO", reportDTO);
		
		super.doView(renderRequest, renderResponse);
	}

	private ReportDTO populate(ReportDTO reportDTO,ThemeDisplay themeDisplay) {
		try {
			
			String userType = OmpHelper.getUserRoles(themeDisplay.getUserId());
    		int gruppoUser = GruppoUsers.GNED.getCode();
    		if(userType.equals(OmpRoles.OMP_CONTROLLER_VBS.getLabel())
    				|| userType.equals(OmpRoles.OMP_APPROVER_VBS.getLabel())
    				|| userType.equals(OmpRoles.OMP_ENG_VBS.getLabel())){
    			gruppoUser = GruppoUsers.VBS.getCode();
    		}
			List<CostCenter> costCentersList = CostCenterLocalServiceUtil.getCostCentersList(0, 
					CostCenterLocalServiceUtil.getCostCentersCount(), null, gruppoUser);
			
			//List<CostCenter> costCentersList = CostCenterLocalServiceUtil.getCostCenters(0,CostCenterLocalServiceUtil.getCostCentersCount());
			
			reportDTO.setAllCostCenters(costCentersList);
			CostCenter costCenter = null;
			if (! OmpRoles.OMP_CONTROLLER.getLabel().equals(userType) 
					&& ! OmpRoles.OMP_ANTEX.getLabel().equals(userType) 
					&& ! OmpRoles.OMP_NDSO.getLabel().equals(userType) 
					&& ! OmpRoles.OMP_TC.getLabel().equals(userType)) {
				
				try {
					CostCenterUser costCenterUser = CostCenterUserLocalServiceUtil.getCostCenterUserByUserId(themeDisplay.getUserId());
					if (costCenterUser != null) {
						costCenter = CostCenterLocalServiceUtil.getCostCenter(costCenterUser.getCostCenterId());
					} else {
						throw new UserHasNoCostCenterException("the logged in user has't cost center");
					}
				} catch (Exception e) {
					logger.error("Erro in populate ReportDTO", e);
				}
			}
			
			reportDTO.setPrReport(true);
			
			if (costCenter != null) {
				reportDTO.setCostCenterId(costCenter.getCostCenterId());
			}
			
			List<String> fiscalYears = ReportLocalServiceUtil.getAllFiscalYears();
			reportDTO.setAllFiscalYears(fiscalYears);
			
			List<WbsCode> wbsCodeList = WbsCodeLocalServiceUtil.getWbsCodesList(0, WbsCodeLocalServiceUtil.getWbsCodesCount(), null);
			reportDTO.setAllWbsCodes(wbsCodeList);
			
			List<Project> projectsList = ProjectLocalServiceUtil.getProjectsList(0, ProjectLocalServiceUtil.getProjectsCount(), null);
			reportDTO.setAllProjects(projectsList);

			List<Vendor> vendorsList = VendorLocalServiceUtil.getVendorsList(0, VendorLocalServiceUtil.getVendorsCount(), null, GruppoUsers.GNED.getCode());
			reportDTO.setAllVendors(vendorsList);
			
			List<PurchaseRequestStatus> prStatusList = getPRListStatus(); 
			reportDTO.setAllPrStatus(prStatusList);
			
			reportDTO.setUserType(userType);
			
			reportDTO.setTargaTecnica(reportDTO.getTargaTecnica());
			
		} catch (SystemException e) {
			processException(e);
		}
		return reportDTO;
	}
	
	private List<PurchaseRequestStatus> getPRListStatus(){
		List<PurchaseRequestStatus> statusList = new ArrayList<PurchaseRequestStatus>();
		statusList.add(PurchaseRequestStatus.CREATED);
		statusList.add(PurchaseRequestStatus.SUBMITTED);
		statusList.add(PurchaseRequestStatus.PR_ASSIGNED);
		statusList.add(PurchaseRequestStatus.ASSET_RECEIVED);
		statusList.add(PurchaseRequestStatus.SC_RECEIVED);
		statusList.add(PurchaseRequestStatus.PO_RECEIVED);
		statusList.add(PurchaseRequestStatus.PENDING_IC_APPROVAL);
		statusList.add(PurchaseRequestStatus.REJECTED);
		
		return statusList;
	}
	
	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		_log.debug("serverResource");
		if("exportReport".equals(resourceRequest.getResourceID())){
			debug("serverResource: exportReport");
			exportReport(resourceRequest,resourceResponse);
		}else if("exportDetailsReport".equals(resourceRequest.getResourceID())){
			debug("serverResource: exportDetailsReport");
			exportDetailsReport(resourceRequest,resourceResponse);
		}
	}

	private void exportReport(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) {
		try {
			
			ReportDTO reportDTO = getReportDTO(resourceRequest);
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmss");
			String fileName = "OMP_Report_"+formatter.format(date)+".xls";
			resourceResponse.setContentType("application/vnd.ms-excel");
			resourceResponse.addProperty(HttpHeaders.CONTENT_DISPOSITION,
					"attachment; filename=\""+fileName+"\"");			
			resourceResponse.addProperty(HttpHeaders.CACHE_CONTROL,"max-age=3600, must-revalidate");

			debug("exporting.."+fileName);
			HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(resourceResponse);
			ReportLocalServiceUtil.exportReportAsExcel(reportDTO, getReportHeader(), httpServletResponse.getOutputStream());
		} catch (Exception e) {
			_log.error("error in exportReport",e);
		}
	}
	
	private void exportDetailsReport(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) {
		try {

			ReportDTO reportDTO = getReportDTO(resourceRequest);
			reportDTO.setPrReport(true);
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmss");
			String fileName = "OMP_Report_Details_" + formatter.format(date) + ".xls";
			resourceResponse.setContentType("application/vnd.ms-excel");
			resourceResponse.addProperty(HttpHeaders.CONTENT_DISPOSITION,
					"attachment; filename=\"" + fileName + "\"");
			resourceResponse.addProperty(HttpHeaders.CACHE_CONTROL,
					"max-age=3600, must-revalidate");

			debug("exporting.." + fileName);
			HttpServletResponse httpServletResponse = PortalUtil
					.getHttpServletResponse(resourceResponse);
			ReportLocalServiceUtil.exportDetailsReportAsExcel(reportDTO,
					getDetailsReportHeader(), httpServletResponse.getOutputStream());
		} catch (Exception e) {
			_log.error("error in exportReport", e);
		}
	}

	private void processException(Exception e) {
		_log.error("Caught unexpected exception " + e.getStackTrace());
		if (_log.isDebugEnabled()) {
			_log.debug(e.getMessage(), e);
		}
	}
	
	private static void debug(String msg){
		if(_log.isDebugEnabled()){
			_log.debug(msg);
		}
	}

	private static Logger _log = LoggerFactory.getLogger(ReportPortlet.class);
}

