package com.hp.omp.service.persistence;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.hp.omp.model.ODNPName;
import com.hp.omp.model.impl.ODNPNameImpl;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class ODNPNameFinderImpl extends ODNPNamePersistenceImpl implements ODNPNameFinder{

	private static Log _log = LogFactoryUtil.getLog(ODNPNameFinderImpl.class);
	static final String GET_ODNP_NAME_LIST = ODNPNameFinderImpl.class.getName()+".getODNPNamesList";
	
	@SuppressWarnings("unchecked")
	public List<ODNPName> getODNPNamesList(int start, int end,
			String searchFilter) throws SystemException {
		Session session=null;
		List<ODNPName> list = new ArrayList<ODNPName>();
		try {
		session  = openSession();
		
		String sql = null;
		
		if(StringUtils.isEmpty(searchFilter))
			searchFilter = "%%";
		
		sql = CustomSQLUtil.get(GET_ODNP_NAME_LIST);
		SQLQuery query = session.createSQLQuery(sql);
		
		if(query == null){
			_log.error("get odnp name list error, no sql found");
			return null;
		}
		
		query.setString(0, searchFilter);
		
		query.addEntity("OdnpNameList", ODNPNameImpl.class);
		list = (List<ODNPName>)QueryUtil.list(query, getDialect(), start, end);
		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
		return list;
	}
}
