package com.hp.omp.service.base;

import com.hp.omp.service.PurchaseRequestLocalServiceUtil;

import java.util.Arrays;


public class PurchaseRequestLocalServiceClpInvoker {
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName2;
    private String[] _methodParameterTypes2;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;
    private String _methodName9;
    private String[] _methodParameterTypes9;
    private String _methodName10;
    private String[] _methodParameterTypes10;
    private String _methodName11;
    private String[] _methodParameterTypes11;
    private String _methodName12;
    private String[] _methodParameterTypes12;
    private String _methodName13;
    private String[] _methodParameterTypes13;
    private String _methodName14;
    private String[] _methodParameterTypes14;
    private String _methodName15;
    private String[] _methodParameterTypes15;
    private String _methodName146;
    private String[] _methodParameterTypes146;
    private String _methodName147;
    private String[] _methodParameterTypes147;
    private String _methodName152;
    private String[] _methodParameterTypes152;
    private String _methodName153;
    private String[] _methodParameterTypes153;
    private String _methodName154;
    private String[] _methodParameterTypes154;
    private String _methodName155;
    private String[] _methodParameterTypes155;
    private String _methodName156;
    private String[] _methodParameterTypes156;
    private String _methodName158;
    private String[] _methodParameterTypes158;
    private String _methodName159;
    private String[] _methodParameterTypes159;
    private String _methodName160;
    private String[] _methodParameterTypes160;
    private String _methodName161;
    private String[] _methodParameterTypes161;
    private String _methodName162;
    private String[] _methodParameterTypes162;
    private String _methodName163;
    private String[] _methodParameterTypes163;
    private String _methodName164;
    private String[] _methodParameterTypes164;
    private String _methodName165;
    private String[] _methodParameterTypes165;
    private String _methodName166;
    private String[] _methodParameterTypes166;
    private String _methodName167;
    private String[] _methodParameterTypes167;
    private String _methodName168;
    private String[] _methodParameterTypes168;
    private String _methodName169;
    private String[] _methodParameterTypes169;
    private String _methodName170;
    private String[] _methodParameterTypes170;
    private String _methodName171;
    private String[] _methodParameterTypes171;
    private String _methodName172;
    private String[] _methodParameterTypes172;
    private String _methodName173;
    private String[] _methodParameterTypes173;
    private String _methodName175;
    private String[] _methodParameterTypes175;
    private String _methodName176;
    private String[] _methodParameterTypes176;
    private String _methodName177;
    private String[] _methodParameterTypes177;
    private String _methodName178;
    private String[] _methodParameterTypes178;
    private String _methodName179;
    private String[] _methodParameterTypes179;
    private String _methodName180;
    private String[] _methodParameterTypes180;
    private String _methodName181;
    private String[] _methodParameterTypes181;
    private String _methodName182;
    private String[] _methodParameterTypes182;

    public PurchaseRequestLocalServiceClpInvoker() {
        _methodName0 = "addPurchaseRequest";

        _methodParameterTypes0 = new String[] { "com.hp.omp.model.PurchaseRequest" };

        _methodName1 = "createPurchaseRequest";

        _methodParameterTypes1 = new String[] { "long" };

        _methodName2 = "deletePurchaseRequest";

        _methodParameterTypes2 = new String[] { "long" };

        _methodName3 = "deletePurchaseRequest";

        _methodParameterTypes3 = new String[] { "com.hp.omp.model.PurchaseRequest" };

        _methodName4 = "dynamicQuery";

        _methodParameterTypes4 = new String[] {  };

        _methodName5 = "dynamicQuery";

        _methodParameterTypes5 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName6 = "dynamicQuery";

        _methodParameterTypes6 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
            };

        _methodName7 = "dynamicQuery";

        _methodParameterTypes7 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName8 = "dynamicQueryCount";

        _methodParameterTypes8 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName9 = "fetchPurchaseRequest";

        _methodParameterTypes9 = new String[] { "long" };

        _methodName10 = "getPurchaseRequest";

        _methodParameterTypes10 = new String[] { "long" };

        _methodName11 = "getPersistedModel";

        _methodParameterTypes11 = new String[] { "java.io.Serializable" };

        _methodName12 = "getPurchaseRequests";

        _methodParameterTypes12 = new String[] { "int", "int" };

        _methodName13 = "getPurchaseRequestsCount";

        _methodParameterTypes13 = new String[] {  };

        _methodName14 = "updatePurchaseRequest";

        _methodParameterTypes14 = new String[] {
                "com.hp.omp.model.PurchaseRequest"
            };

        _methodName15 = "updatePurchaseRequest";

        _methodParameterTypes15 = new String[] {
                "com.hp.omp.model.PurchaseRequest", "boolean"
            };

        _methodName146 = "getBeanIdentifier";

        _methodParameterTypes146 = new String[] {  };

        _methodName147 = "setBeanIdentifier";

        _methodParameterTypes147 = new String[] { "java.lang.String" };

        _methodName152 = "addPurchaseRequest";

        _methodParameterTypes152 = new String[] {
                "com.hp.omp.model.PurchaseRequest"
            };

        _methodName153 = "deletePurchaseRequest";

        _methodParameterTypes153 = new String[] { "long" };

        _methodName154 = "deletePurchaseRequest";

        _methodParameterTypes154 = new String[] {
                "com.hp.omp.model.PurchaseRequest"
            };

        _methodName155 = "updatePurchaseRequest";

        _methodParameterTypes155 = new String[] {
                "com.hp.omp.model.PurchaseRequest"
            };

        _methodName156 = "updatePurchaseRequest";

        _methodParameterTypes156 = new String[] {
                "com.hp.omp.model.PurchaseRequest", "boolean"
            };

        _methodName158 = "getPurchaseRequestByStatus";

        _methodParameterTypes158 = new String[] { "long", "int" };

        _methodName159 = "getPurchaseRequests";

        _methodParameterTypes159 = new String[] { "java.util.List" };

        _methodName160 = "getPurchaseRequesListByStatus";

        _methodParameterTypes160 = new String[] { "int" };

        _methodName161 = "getPurchaseRequesListByStatus";

        _methodParameterTypes161 = new String[] {
                "int", "int", "int", "java.lang.String", "java.lang.String"
            };

        _methodName162 = "getPurchaseRequesListByStatusCount";

        _methodParameterTypes162 = new String[] { "int", "boolean" };

        _methodName163 = "getPurchaseRequesListByStatusAndType";

        _methodParameterTypes163 = new String[] {
                "int", "boolean", "int", "int", "java.lang.String",
                "java.lang.String"
            };

        _methodName164 = "getPurchaseRequesListByPositionStatusAndType";

        _methodParameterTypes164 = new String[] {
                "java.util.Set", "boolean", "int", "int", "java.lang.String",
                "java.lang.String"
            };

        _methodName165 = "getPurchaseRequestListByPositionStatusAndType";

        _methodParameterTypes165 = new String[] {
                "int", "boolean", "int", "int", "java.lang.String",
                "java.lang.String"
            };

        _methodName166 = "getPurchaseRequestListByPositionStatusAndTypeCount";

        _methodParameterTypes166 = new String[] { "int", "boolean" };

        _methodName167 = "getPRListByStatusAndCostCenter";

        _methodParameterTypes167 = new String[] {
                "int", "long", "int", "int", "int", "java.lang.String",
                "java.lang.String"
            };

        _methodName168 = "getPRListByStatusAndCostCenterCount";

        _methodParameterTypes168 = new String[] { "int", "long", "int" };

        _methodName169 = "getPurchaseRequestList";

        _methodParameterTypes169 = new String[] {
                "com.hp.omp.model.custom.SearchDTO", "int", "long"
            };

        _methodName170 = "prSearch";

        _methodParameterTypes170 = new String[] {
                "com.hp.omp.model.custom.SearchDTO", "int", "long"
            };

        _methodName171 = "getPurchaseRequestCount";

        _methodParameterTypes171 = new String[] {
                "com.hp.omp.model.custom.SearchDTO", "int", "long"
            };

        _methodName172 = "getAntexPurchaseRequestList";

        _methodParameterTypes172 = new String[] {
                "com.hp.omp.model.custom.SearchDTO"
            };

        _methodName173 = "getAntexPurchaseRequestCount";

        _methodParameterTypes173 = new String[] {
                "com.hp.omp.model.custom.SearchDTO"
            };

        _methodName175 = "getPurchaseRequestByProjectId";

        _methodParameterTypes175 = new String[] { "java.lang.String" };

        _methodName176 = "getPurchaseRequestBySubProjectId";

        _methodParameterTypes176 = new String[] { "java.lang.String" };

        _methodName177 = "getPurchaseRequestByVendorId";

        _methodParameterTypes177 = new String[] { "java.lang.String" };

        _methodName178 = "getPurchaseRequestByBudgetCategoryId";

        _methodParameterTypes178 = new String[] { "java.lang.String" };

        _methodName179 = "getPurchaseRequestByBudgetSubCategoryId";

        _methodParameterTypes179 = new String[] { "java.lang.String" };

        _methodName180 = "getPurchaseRequestByOdnpNameId";

        _methodParameterTypes180 = new String[] { "java.lang.String" };

        _methodName181 = "getPurchaseRequestByMacroDriverId";

        _methodParameterTypes181 = new String[] { "java.lang.String" };

        _methodName182 = "getPurchaseRequestByMicroDriverId";

        _methodParameterTypes182 = new String[] { "java.lang.String" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName0.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.addPurchaseRequest((com.hp.omp.model.PurchaseRequest) arguments[0]);
        }

        if (_methodName1.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.createPurchaseRequest(((Long) arguments[0]).longValue());
        }

        if (_methodName2.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.deletePurchaseRequest(((Long) arguments[0]).longValue());
        }

        if (_methodName3.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.deletePurchaseRequest((com.hp.omp.model.PurchaseRequest) arguments[0]);
        }

        if (_methodName4.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.dynamicQuery();
        }

        if (_methodName5.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName6.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName7.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[3]);
        }

        if (_methodName8.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName9.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.fetchPurchaseRequest(((Long) arguments[0]).longValue());
        }

        if (_methodName10.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequest(((Long) arguments[0]).longValue());
        }

        if (_methodName11.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPersistedModel((java.io.Serializable) arguments[0]);
        }

        if (_methodName12.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequests(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName13.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequestsCount();
        }

        if (_methodName14.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.updatePurchaseRequest((com.hp.omp.model.PurchaseRequest) arguments[0]);
        }

        if (_methodName15.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.updatePurchaseRequest((com.hp.omp.model.PurchaseRequest) arguments[0],
                ((Boolean) arguments[1]).booleanValue());
        }

        if (_methodName146.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes146, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getBeanIdentifier();
        }

        if (_methodName147.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes147, parameterTypes)) {
            PurchaseRequestLocalServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName152.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes152, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.addPurchaseRequest((com.hp.omp.model.PurchaseRequest) arguments[0]);
        }

        if (_methodName153.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes153, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.deletePurchaseRequest(((Long) arguments[0]).longValue());
        }

        if (_methodName154.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes154, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.deletePurchaseRequest((com.hp.omp.model.PurchaseRequest) arguments[0]);
        }

        if (_methodName155.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes155, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.updatePurchaseRequest((com.hp.omp.model.PurchaseRequest) arguments[0]);
        }

        if (_methodName156.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes156, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.updatePurchaseRequest((com.hp.omp.model.PurchaseRequest) arguments[0],
                ((Boolean) arguments[1]).booleanValue());
        }

        if (_methodName158.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes158, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequestByStatus(((Long) arguments[0]).longValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName159.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes159, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequests((java.util.List<java.lang.Long>) arguments[0]);
        }

        if (_methodName160.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes160, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequesListByStatus(((Integer) arguments[0]).intValue());
        }

        if (_methodName161.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes161, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequesListByStatus(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                (java.lang.String) arguments[3], (java.lang.String) arguments[4]);
        }

        if (_methodName162.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes162, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequesListByStatusCount(((Integer) arguments[0]).intValue(),
                ((Boolean) arguments[1]).booleanValue());
        }

        if (_methodName163.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes163, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequesListByStatusAndType(((Integer) arguments[0]).intValue(),
                ((Boolean) arguments[1]).booleanValue(),
                ((Integer) arguments[2]).intValue(),
                ((Integer) arguments[3]).intValue(),
                (java.lang.String) arguments[4], (java.lang.String) arguments[5]);
        }

        if (_methodName164.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes164, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequesListByPositionStatusAndType((java.util.Set<java.lang.Long>) arguments[0],
                ((Boolean) arguments[1]).booleanValue(),
                ((Integer) arguments[2]).intValue(),
                ((Integer) arguments[3]).intValue(),
                (java.lang.String) arguments[4], (java.lang.String) arguments[5]);
        }

        if (_methodName165.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes165, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequestListByPositionStatusAndType(((Integer) arguments[0]).intValue(),
                ((Boolean) arguments[1]).booleanValue(),
                ((Integer) arguments[2]).intValue(),
                ((Integer) arguments[3]).intValue(),
                (java.lang.String) arguments[4], (java.lang.String) arguments[5]);
        }

        if (_methodName166.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes166, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequestListByPositionStatusAndTypeCount(((Integer) arguments[0]).intValue(),
                ((Boolean) arguments[1]).booleanValue());
        }

        if (_methodName167.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes167, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPRListByStatusAndCostCenter(((Integer) arguments[0]).intValue(),
                ((Long) arguments[1]).longValue(),
                ((Integer) arguments[2]).intValue(),
                ((Integer) arguments[3]).intValue(),
                ((Integer) arguments[4]).intValue(),
                (java.lang.String) arguments[5], (java.lang.String) arguments[6]);
        }

        if (_methodName168.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes168, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPRListByStatusAndCostCenterCount(((Integer) arguments[0]).intValue(),
                ((Long) arguments[1]).longValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName169.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes169, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequestList((com.hp.omp.model.custom.SearchDTO) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Long) arguments[2]).longValue());
        }

        if (_methodName170.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes170, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.prSearch((com.hp.omp.model.custom.SearchDTO) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Long) arguments[2]).longValue());
        }

        if (_methodName171.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes171, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequestCount((com.hp.omp.model.custom.SearchDTO) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Long) arguments[2]).longValue());
        }

        if (_methodName172.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes172, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getAntexPurchaseRequestList((com.hp.omp.model.custom.SearchDTO) arguments[0]);
        }

        if (_methodName173.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes173, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getAntexPurchaseRequestCount((com.hp.omp.model.custom.SearchDTO) arguments[0]);
        }

        if (_methodName175.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes175, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequestByProjectId((java.lang.String) arguments[0]);
        }

        if (_methodName176.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes176, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequestBySubProjectId((java.lang.String) arguments[0]);
        }

        if (_methodName177.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes177, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequestByVendorId((java.lang.String) arguments[0]);
        }

        if (_methodName178.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes178, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequestByBudgetCategoryId((java.lang.String) arguments[0]);
        }

        if (_methodName179.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes179, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequestByBudgetSubCategoryId((java.lang.String) arguments[0]);
        }

        if (_methodName180.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes180, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequestByOdnpNameId((java.lang.String) arguments[0]);
        }

        if (_methodName181.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes181, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequestByMacroDriverId((java.lang.String) arguments[0]);
        }

        if (_methodName182.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes182, parameterTypes)) {
            return PurchaseRequestLocalServiceUtil.getPurchaseRequestByMicroDriverId((java.lang.String) arguments[0]);
        }

        throw new UnsupportedOperationException();
    }
}
