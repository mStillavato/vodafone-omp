package com.hp.omp.service.persistence;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.hp.omp.model.TrackingCodes;
import com.hp.omp.model.impl.TrackingCodesImpl;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class TrackingCodesFinderImpl extends TrackingCodesPersistenceImpl implements TrackingCodesFinder{

	private static Log _log = LogFactoryUtil.getLog(TrackingCodesFinderImpl.class);
	static final String GET_CODES_ASC = TrackingCodesFinderImpl.class.getName()+".getCodesOrderedASC";
	static final String GET_CODES_WHERE_ASC = TrackingCodesFinderImpl.class.getName()+".getCodesWhereOrderedASC";
	static final String GET_CODES_COUNT_LIST = TrackingCodesFinderImpl.class.getName()+".getTrackingCodesCountList";
	
	@SuppressWarnings("unchecked")
	public List<TrackingCodes> getTrackingCodesOrderedAsc(int start , int end, String searchTCN) throws SystemException
	{
		Session session=null;
		List<TrackingCodes> list = new ArrayList<TrackingCodes>();
		try {
		session  = openSession();
		
		String sql = null;
		
		if(StringUtils.isEmpty(searchTCN))
			searchTCN = "%%";
		
		sql = CustomSQLUtil.get(GET_CODES_WHERE_ASC);
		SQLQuery query = session.createSQLQuery(sql);
		
		if(query == null){
			_log.error("get tracking codes error, no sql found");
			return null;
		}
		
		query.setString(0, searchTCN);
		query.setString(1, searchTCN);
		
		query.addEntity("TrackingCodes", TrackingCodesImpl.class);
		list = (List<TrackingCodes>)QueryUtil.list(query, getDialect(),  start, end);
		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
		return list;
	}
	
	public int getTrackingCodesCountList(String searchFilter) throws SystemException {
		Session session = null; 
		try {
			session = openSession();
			
			String sql = null;
			
			if(StringUtils.isEmpty(searchFilter))
				searchFilter = "%%";
			
			sql = CustomSQLUtil.get(GET_CODES_COUNT_LIST);
			SQLQuery query = session.createSQLQuery(sql);
			
			if(query == null){
				_log.error("get Tracking Codes count list error, no sql found");
				return 0;
			}
			
			query.setString(0, searchFilter);
			query.setString(1, searchFilter);
			
			query.addScalar("COUNT_VALUE", Type.INTEGER);

			@SuppressWarnings("unchecked")
			List<Integer> list = query.list();
			if(list != null && list.size() > 0) {
				return list.get(0);
			}
			return 0;
		} finally {
			if(session != null) {
				closeSession(session);
			}
		}
	}
}
