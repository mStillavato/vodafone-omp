/**
 * 
 */
package com.hp.omp.model.custom;

import java.util.HashMap;
import java.util.Map;

/**
 * @author gewaly
 *
 */
public enum OmpRoles {
	
	OMP_ENG(1,"ENG"),
	OMP_ANTEX(2,"ANTEX"),
	OMP_CONTROLLER(3,"CONTROLLER"),
	OMP_OBSERVER(4,"OBSERVER"),
	OMP_NDSO(4,"NDSO"),
	OMP_TC(4,"TC"),
	OMP_ENG_VBS(5,"ENG_VBS"),
	OMP_CONTROLLER_VBS(6,"CONTROLLER_VBS"),
	OMP_APPROVER_VBS(7,"APPROVER_VBS");
	
	private int code;
	private String label;
	
	private OmpRoles(int code, String label){
		this.code = code;
		this.label = label;
	}
	
	
	  private static Map<Integer, OmpRoles> codeToStatusMapping;
		 
	 
	    public static OmpRoles getStatus(int i) {
	        if (codeToStatusMapping == null) {
	            initMapping();
	        }
	        return codeToStatusMapping.get(i);
	    }
	 
	    private static void initMapping() {
	        codeToStatusMapping = new HashMap<Integer, OmpRoles>();
	        for (OmpRoles s : values()) {
	            codeToStatusMapping.put(s.code, s);
	        }
	    }
	public int getCode(){
		return code;
	}
	
	public String getLabel(){
		return label;
	}
}

