package com.hp.omp.portlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.model.PurchaseOrder;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.Vendor;
import com.hp.omp.model.custom.GruppoUsers;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.impl.VendorImpl;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.hp.omp.service.VendorLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class VendorAdminPortlet
 */
public class VendorAdminPortlet extends MVCPortlet {
 
	private transient Logger logger = LoggerFactory.getLogger(VendorAdminPortlet.class);
	private String errorMSG = "";
	private String successMSG = "";
	private String ACTION_PARAM= "action";
	private String ADD_ACTION= "add";
	private String UPDATE_ACTION= "update";
	private String DELETE_ACTION= "delete";
	private String PAGINATION_ACTION= "pagination";
	private String EXPORT_LIST= "exportList";


	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		super.doView(renderRequest, renderResponse);
	}
	
	
	/* (non-Javadoc)
	 * @see javax.portlet.GenericPortlet#render(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		viewAddVendor(request, response);
		super.render(request, response);
	}
	
	public void viewAddVendor(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		String action = renderRequest.getParameter(ACTION_PARAM);
		
		if(ADD_ACTION.equals(action)){
			renderRequest.setAttribute(ACTION_PARAM, ADD_ACTION);
			renderRequest.setAttribute("display",true);
		}else if(UPDATE_ACTION.equals(action)){
			String vendorId = renderRequest.getParameter("vendorId");
			Vendor vendor = getVendor(vendorId);
			
			logger.debug("render update action for : "+vendor.getVendorName());
			logger.debug("Display = "+vendor.getDisplay());
			
			renderRequest.setAttribute(ACTION_PARAM, UPDATE_ACTION);
			renderRequest.setAttribute("vendorId", vendor.getVendorId());
			renderRequest.setAttribute("vendorName", vendor.getVendorName());
			renderRequest.setAttribute("supplier", vendor.getSupplier());
			renderRequest.setAttribute("supplierCode", vendor.getSupplierCode());
			renderRequest.setAttribute("display", vendor.getDisplay());
		}
	}
	
	@Override
    public void serveResource(ResourceRequest resourceRequest,
                  ResourceResponse resourceResponse) throws IOException,
                  PortletException {
		String action=resourceRequest.getParameter(ACTION_PARAM);
		if(DELETE_ACTION.equals(action)){
			deleteVendor(resourceRequest, resourceResponse);
		} else if(PAGINATION_ACTION.equals(action)){
			paginationList(resourceRequest, resourceResponse);
		} else if(EXPORT_LIST.equals(action)){
			exportList(resourceRequest, resourceResponse);
		}
		
		clearMessages();
           super.serveResource(resourceRequest, resourceResponse);
    }
	
	
	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#processAction(javax.portlet.ActionRequest, javax.portlet.ActionResponse)
	 */
	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {

		clearMessages();
		String action = actionRequest.getParameter(ACTION_PARAM);
		
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String userType = OmpHelper.getUserRoles(themeDisplay.getUserId());
		
		String supplier = actionRequest.getParameter("supplier");
		String supplierCode = actionRequest.getParameter("supplierCode");
		String vendorName = supplier +" - " +supplierCode;
		boolean display=ParamUtil.getBoolean(actionRequest,"display");
		String operation = "";
		
		logger.debug("vendorName="+vendorName);
		logger.debug("supplier="+supplier);
		logger.debug("supplierCode="+supplierCode);
		logger.debug("display = "+display);
		
		logger.debug("creating Vendor object ...");
		Vendor newVendor = new VendorImpl();
		newVendor.setVendorName(vendorName);
		newVendor.setSupplier(supplier);
		newVendor.setSupplierCode(supplierCode);
		newVendor.setDisplay(display);
		
		int gruppoUser = GruppoUsers.GNED.getCode();
		if(userType.equals(OmpRoles.OMP_CONTROLLER_VBS.getLabel())
				|| userType.equals(OmpRoles.OMP_APPROVER_VBS.getLabel())
				|| userType.equals(OmpRoles.OMP_ENG_VBS.getLabel())){
			gruppoUser = GruppoUsers.VBS.getCode();
			
		}
		newVendor.setGruppoUsers(gruppoUser);
		
		logger.debug("Vendor object created !!");
		try {
			if(ADD_ACTION.equals(action)){
				operation = "added";
				logger.debug("adding new Vendor ["+vendorName+"] in database ...");
				Vendor vendor = VendorLocalServiceUtil.getVendorByName(vendorName, gruppoUser);
				if(vendor == null){
					VendorLocalServiceUtil.addVendor(newVendor);
					successMSG = "Vendor name ["+vendorName+"] successfully Added";
				}else {
					errorMSG = "Vendor name ["+vendorName+"] already existing";
				}
			}else if(UPDATE_ACTION.equals(action)){
				operation = "updated";
				String vendorId = actionRequest.getParameter("vendorId");
				
				newVendor.setVendorId(Integer.valueOf(vendorId));
				
				if(! (isVendorNameExists(newVendor, vendorName, gruppoUser))){
					
					logger.debug("updating  Vendor ["+vendorId+"] in database ...");
					
					
					VendorLocalServiceUtil.updateVendor(newVendor);
					successMSG = "Vendor Name ["+vendorName+"] successfully Updated";
				}else{
					errorMSG = "Vendor name ["+vendorName+"] already existing";
				}
				
			}
			
			
		} catch (SystemException e) {
			errorMSG = "Vendor ["+vendorName+"] not "+operation+".";
			logger.error("error happened while "+operation+" Vendor ["+vendorName+"] in database.");
			logger.error(e.getMessage(), e);
		}catch (NumberFormatException nfe) {
			errorMSG = "Vendor ["+vendorName+"] not "+operation+".";
			logger.error("error happened while "+operation+" Vendor ["+vendorName+"] in database.");
			logger.error(nfe.getMessage(), nfe);
		}
		
		actionRequest.setAttribute("errorMSG", errorMSG);
		actionRequest.setAttribute("successMSG", successMSG);
		super.processAction(actionRequest, actionResponse);
	}
	
	private void retrieveVendorListToRequest(PortletRequest renderRequest) throws SystemException{
		try {
			String searchFilter = ParamUtil.getString(renderRequest, "searchFilter","").toUpperCase();
			
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userType = OmpHelper.getUserRoles(themeDisplay.getUserId());
			
			int gruppoUser = GruppoUsers.GNED.getCode();
			if(userType.equals(OmpRoles.OMP_CONTROLLER_VBS.getLabel())
					|| userType.equals(OmpRoles.OMP_APPROVER_VBS.getLabel())
					|| userType.equals(OmpRoles.OMP_ENG_VBS.getLabel())){
				gruppoUser = GruppoUsers.VBS.getCode();
				
			}
			
			Integer pageNumber = ParamUtil.getInteger(renderRequest, "pageNumber", 1);
			Integer pageSize = OmpHelper.PAGE_SIZE;
			int start = (pageNumber - 1) * pageSize;
			int end = start + pageSize;
			
			//List<Vendor> vendorList = VendorLocalServiceUtil.getVendors(start, end);
			List<Vendor> vendorList = VendorLocalServiceUtil.getVendorsList(start, end, searchFilter, gruppoUser);
			renderRequest.setAttribute("vendorList", vendorList);
			
//			int totalCount = 0;
//			if (searchFilter == null || searchFilter.trim().length() == 0)
//				totalCount = VendorLocalServiceUtil.getVendorsCount();
//			else
//				totalCount = VendorLocalServiceUtil.getVendorsCountList(searchFilter, gruppoUser);

			int totalCount = VendorLocalServiceUtil.getVendorsCountList(searchFilter, gruppoUser);
			logger.debug("Vendors Total Count = " + totalCount);

			renderRequest.setAttribute("numberOfPages", getTotalNumberOfPages(totalCount, pageSize));
			renderRequest.setAttribute("pageNumber", pageNumber);

		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			errorMSG = "can not retrieve Wbs Code list";
			renderRequest.setAttribute("errorMSG", errorMSG);
		}
		
	}
	
	public void deleteVendor(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse)
			throws PortletException, IOException {
		clearMessages();
		
		String vendorId = resourceRequest.getParameter("vendorId");
		String vendorName = resourceRequest.getParameter("vendorName");
//		String supplier = resourceRequest.getParameter("supplier");
//		String supplierCode = resourceRequest.getParameter("supplierCode");
		String action=resourceRequest.getParameter("action");
		String jspPage = "/html/vendoradmin/vendorlist.jsp";
		
		if(Validator.isBlank(action)){
			errorMSG = "No Action in the request";
		}else if(Validator.isBlank(vendorId)){
			errorMSG = "No vendor Id in the request";
		}else{
			
			try {
				List<PurchaseOrder> purchaseOrderList = PurchaseOrderLocalServiceUtil.getPurchaseOrderByVendorId(vendorId);
				List<PurchaseRequest> purchaseRequestList = PurchaseRequestLocalServiceUtil.getPurchaseRequestByVendorId(vendorId);
				
				if(purchaseOrderList != null && ! (purchaseOrderList.isEmpty())){
					errorMSG = "Can Not remove Vendor assigned to Purchase Order, Vendor name ["+vendorName+"]";
				}else if(purchaseRequestList != null && ! (purchaseRequestList.isEmpty())){
					errorMSG = "Can Not remove Vendor assigned to Purchase Request, Vendor name ["+vendorName+"]";
				}else{
					VendorLocalServiceUtil.deleteVendor(Long.valueOf(vendorId));
					successMSG = "vendor name ["+vendorName+"] successfully deleted";
				}
				
			} catch (NumberFormatException e) {
				logger.error(e.getMessage());
				errorMSG = "vendor Id is invalid";
			} catch (PortalException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove vendor";
			} catch (SystemException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove Vendor [most probably Vendor is assigned.]";
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove vendor";
			}
			
		}
		
		//retrieve updated list
		try {
			retrieveVendorListToRequest(resourceRequest);
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			errorMSG = "can not retrieve vendor list";
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
		
	}
	
	private Vendor getVendor(String vendortId){
		Vendor vendor = null;
		try {
			vendor = VendorLocalServiceUtil.getVendor(Integer.valueOf(vendortId));
		} catch (NumberFormatException e) {
			logger.error("vendor Id has wrong format", e);
		} catch (PortalException e) {
			logger.error("unexpected error", e);
		} catch (SystemException e) {
			logger.error("unexpected error", e);
		}
		
		return vendor;
	}
	
	private boolean isVendorNameExists(Vendor newVendor, String vendorName, int gruppoUser) throws SystemException{
		
		Vendor oldVendor = VendorLocalServiceUtil.getVendorByName(vendorName, gruppoUser);
		boolean isExists = false;
		
		if(oldVendor != null && 
				(newVendor.getVendorId() != oldVendor.getVendorId()) ){
			isExists = true;
		}
		return isExists;
	}
	
	private void paginationList(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse) throws PortletException, IOException{
		
		String jspPage = "/html/vendoradmin/vendorlist.jsp";
		try {
			retrieveVendorListToRequest(resourceRequest);
		} catch (SystemException e) {
		logger.error("error happened in pagination ... ",e);
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
	}
	
	private Integer getTotalNumberOfPages(Integer totalNubmerOfResult, Integer pageSize) {
        return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
	}
	
	private void clearMessages(){
		errorMSG = "";
		successMSG = "";
	}
	
	public void searchFilter(ActionRequest request, ActionResponse response) throws IOException, PortletException{
		response.setRenderParameter("searchFilter", "%" + ParamUtil.getString(request, "vendorName") + "%");  
	}
	
	private void exportList(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException{
		logger.info("inside exportList");  
		
		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String userType = OmpHelper.getUserRoles(themeDisplay.getUserId());
		
		int gruppoUser = GruppoUsers.GNED.getCode();
		if(userType.equals(OmpRoles.OMP_CONTROLLER_VBS.getLabel())
				|| userType.equals(OmpRoles.OMP_APPROVER_VBS.getLabel())
				|| userType.equals(OmpRoles.OMP_ENG_VBS.getLabel())){
			gruppoUser = GruppoUsers.VBS.getCode();
			
		}
		
		String searchFilter = ParamUtil.getString(resourceRequest, "searchFilter","").toUpperCase();
		
		logger.info("searchTCN = " + searchFilter);
		
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		
		String fileName = "OMP_Vendor_"+formatter.format(date)+".xls";
		resourceResponse.setContentType("application/vnd.ms-excel");
		
		resourceResponse.addProperty(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=\""+fileName+"\"");			
		resourceResponse.addProperty(HttpHeaders.CACHE_CONTROL,"max-age=3600, must-revalidate");

		logger.debug("exporting.." + fileName);
		HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(resourceResponse);
		try {
			int totalCount = 0;
			
			if (searchFilter == null || searchFilter.trim().length() == 0)
				totalCount = VendorLocalServiceUtil.getVendorsCount();
			else
				totalCount = VendorLocalServiceUtil.getVendorsCountList(searchFilter, gruppoUser);

			logger.debug("Vendor Total Count = " + totalCount);
			List<com.hp.omp.model.Vendor> VendorsList = VendorLocalServiceUtil.getVendorsList(0, totalCount, searchFilter, gruppoUser);
			VendorLocalServiceUtil.exportVendorsAsExcel(VendorsList, getVendorReportHeader(), httpServletResponse.getOutputStream());

		} catch (SystemException e) {
			logger.error("Error in exportVendor", e);
			e.printStackTrace();
		}
	}
	
	private List<Object> getVendorReportHeader() {
		List<Object> header = new ArrayList<Object>();
		header.add("NAME");			//0
		header.add("SUPPLIER");		//1
		header.add("SUPPLIER CODE");//2
		header.add("DISPLAY");		//3
		
		return header;
	}
}
