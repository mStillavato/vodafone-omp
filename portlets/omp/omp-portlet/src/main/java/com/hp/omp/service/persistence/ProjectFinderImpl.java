package com.hp.omp.service.persistence;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.hp.omp.model.MacroDriver;
import com.hp.omp.model.Project;
import com.hp.omp.model.impl.ProjectImpl;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class ProjectFinderImpl extends ProjectPersistenceImpl implements ProjectFinder{

	private static Log _log = LogFactoryUtil.getLog(ProjectFinderImpl.class);
	static final String GET_PROJECTS_LIST = ProjectFinderImpl.class.getName()+".getProjectsList";
	static final String GET_PROJECTS_COUNT_LIST = ProjectFinderImpl.class.getName()+".getProjectsCountList";
	
	@SuppressWarnings("unchecked")
	public List<Project> getProjectsList(int start, int end,
			String searchFilter) throws SystemException {
		Session session=null;
		List<Project> list = new ArrayList<Project>();
		try {
		session  = openSession();
		
		String sql = null;
		
		if(StringUtils.isEmpty(searchFilter))
			searchFilter = "%%";
		
		sql = CustomSQLUtil.get(GET_PROJECTS_LIST);
		SQLQuery query = session.createSQLQuery(sql);
		
		if(query == null){
			_log.error("get project list error, no sql found");
			return null;
		}
		
		query.setString(0, searchFilter);
		query.setString(1, searchFilter);
		
		query.addEntity("ProjectList", ProjectImpl.class);
		list = (List<Project>)QueryUtil.list(query, getDialect(), start, end);
		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
		return list;
	}

	public int getProjectsCountList(String searchFilter) throws SystemException {
		
		Session session = null; 
		try {
			session = openSession();
			
			String sql = null;
			
			if(StringUtils.isEmpty(searchFilter))
				searchFilter = "%%";
			
			sql = CustomSQLUtil.get(GET_PROJECTS_COUNT_LIST);
			SQLQuery query = session.createSQLQuery(sql);
			
			if(query == null){
				_log.error("get project count list error, no sql found");
				return 0;
			}
			
			query.setString(0, searchFilter);
			query.setString(1, searchFilter);
			
			query.addScalar("COUNT_VALUE", Type.INTEGER);

			@SuppressWarnings("unchecked")
			List<Integer> list = query.list();
			if(list != null && list.size() > 0) {
				return list.get(0);
			}
			return 0;
		} finally {
			if(session != null) {
				closeSession(session);
			}
		}
	}
}
