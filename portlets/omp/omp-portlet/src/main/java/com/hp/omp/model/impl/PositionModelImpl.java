package com.hp.omp.model.impl;

import com.hp.omp.model.Position;
import com.hp.omp.model.PositionModel;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.util.PortalUtil;

import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.util.ExpandoBridgeFactoryUtil;

import java.io.Serializable;

import java.sql.Types;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * The base model implementation for the Position service. Represents a row in the &quot;POSITION&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link com.hp.omp.model.PositionModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link PositionImpl}.
 * </p>
 *
 * @author HP Egypt team
 * @see PositionImpl
 * @see com.hp.omp.model.Position
 * @see com.hp.omp.model.PositionModel
 * @generated
 */
public class PositionModelImpl extends BaseModelImpl<Position>
    implements PositionModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. All methods that expect a position model instance should use the {@link com.hp.omp.model.Position} interface instead.
     */
    public static final String TABLE_NAME = "POSITION";
    public static final Object[][] TABLE_COLUMNS = {
            { "positionId", Types.BIGINT },
            { "fiscalYear", Types.VARCHAR },
            { "description", Types.VARCHAR },
            { "categoryCode", Types.VARCHAR },
            { "offerNumber", Types.VARCHAR },
            { "offerDate", Types.TIMESTAMP },
            { "quatity", Types.VARCHAR },
            { "deliveryDate", Types.TIMESTAMP },
            { "deliveryAddress", Types.VARCHAR },
            { "actualUnitCost", Types.VARCHAR },
            { "numberOfUnit", Types.VARCHAR },
            { "labelNumber", Types.BIGINT },
            { "poLabelNumber", Types.BIGINT },
            { "targaTecnica", Types.VARCHAR },
            { "approved", Types.BOOLEAN },
            { "icApproval", Types.VARCHAR },
            { "approver", Types.VARCHAR },
            { "assetNumber", Types.VARCHAR },
            { "shoppingCart", Types.VARCHAR },
            { "poNumber", Types.VARCHAR },
            { "evoLocation", Types.VARCHAR },
            { "wbsCodeId", Types.VARCHAR },
            { "purchaseRequestId", Types.VARCHAR },
            { "purchaseOrderId", Types.VARCHAR },
            { "createdDate", Types.TIMESTAMP },
            { "createdUserId", Types.BIGINT },
            { "evoCodMateriale", Types.VARCHAR },
            { "evoDesMateriale", Types.VARCHAR }
        };
    public static final String TABLE_SQL_CREATE = "create table POSITION (positionId LONG not null primary key,fiscalYear VARCHAR(75) null,description VARCHAR(75) null,categoryCode VARCHAR(75) null,offerNumber VARCHAR(75) null,offerDate DATE null,quatity VARCHAR(75) null,deliveryDate DATE null,deliveryAddress VARCHAR(75) null,actualUnitCost VARCHAR(75) null,numberOfUnit VARCHAR(75) null,labelNumber LONG,poLabelNumber LONG,targaTecnica VARCHAR(75) null,approved BOOLEAN,icApproval VARCHAR(75) null,approver VARCHAR(75) null,assetNumber VARCHAR(75) null,shoppingCart VARCHAR(75) null,poNumber VARCHAR(75) null,evoLocation VARCHAR(75) null,wbsCodeId VARCHAR(75) null,purchaseRequestId VARCHAR(75) null,purchaseOrderId VARCHAR(75) null,createdDate DATE null,createdUserId LONG,evoCodMateriale VARCHAR(75) null,evoDesMateriale VARCHAR(75) null)";
    public static final String TABLE_SQL_DROP = "drop table POSITION";
    public static final String DATA_SOURCE = "ompPortletDataSource";
    public static final String SESSION_FACTORY = "ompPortletSessionFactory";
    public static final String TX_MANAGER = "ompPortletTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.entity.cache.enabled.com.hp.omp.model.Position"),
            true);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.finder.cache.enabled.com.hp.omp.model.Position"),
            true);
    public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.column.bitmask.enabled.com.hp.omp.model.Position"),
            true);
    public static long PURCHASEORDERID_COLUMN_BITMASK = 1L;
    public static long PURCHASEREQUESTID_COLUMN_BITMASK = 2L;
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
                "lock.expiration.time.com.hp.omp.model.Position"));
    private static ClassLoader _classLoader = Position.class.getClassLoader();
    private static Class<?>[] _escapedModelInterfaces = new Class[] {
            Position.class
        };
    private long _positionId;
    private String _fiscalYear;
    private String _description;
    private String _categoryCode;
    private String _offerNumber;
    private Date _offerDate;
    private String _quatity;
    private Date _deliveryDate;
    private String _deliveryAddress;
    private String _actualUnitCost;
    private String _numberOfUnit;
    private long _labelNumber;
    private long _poLabelNumber;
    private String _targaTecnica;
    private boolean _approved;
    private String _icApproval;
    private String _approver;
    private String _assetNumber;
    private String _shoppingCart;
    private String _poNumber;
    private String _evoLocation;
    private String _wbsCodeId;
    private String _purchaseRequestId;
    private String _originalPurchaseRequestId;
    private String _purchaseOrderId;
    private String _originalPurchaseOrderId;
    private Date _createdDate;
    private long _createdUserId;
    private String _createdUserUuid;
    private long _columnBitmask;
    private Position _escapedModel;
    private String _evoCodMateriale;
    private String _evoDesMateriale;
    
    public PositionModelImpl() {
    }

    public long getPrimaryKey() {
        return _positionId;
    }

    public void setPrimaryKey(long primaryKey) {
        setPositionId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_positionId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    public Class<?> getModelClass() {
        return Position.class;
    }

    public String getModelClassName() {
        return Position.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("positionId", getPositionId());
        attributes.put("fiscalYear", getFiscalYear());
        attributes.put("description", getDescription());
        attributes.put("categoryCode", getCategoryCode());
        attributes.put("offerNumber", getOfferNumber());
        attributes.put("offerDate", getOfferDate());
        attributes.put("quatity", getQuatity());
        attributes.put("deliveryDate", getDeliveryDate());
        attributes.put("deliveryAddress", getDeliveryAddress());
        attributes.put("actualUnitCost", getActualUnitCost());
        attributes.put("numberOfUnit", getNumberOfUnit());
        attributes.put("labelNumber", getLabelNumber());
        attributes.put("poLabelNumber", getPoLabelNumber());
        attributes.put("targaTecnica", getTargaTecnica());
        attributes.put("approved", getApproved());
        attributes.put("icApproval", getIcApproval());
        attributes.put("approver", getApprover());
        attributes.put("assetNumber", getAssetNumber());
        attributes.put("shoppingCart", getShoppingCart());
        attributes.put("poNumber", getPoNumber());
        attributes.put("evoLocation", getEvoLocation());
        attributes.put("wbsCodeId", getWbsCodeId());
        attributes.put("purchaseRequestId", getPurchaseRequestId());
        attributes.put("purchaseOrderId", getPurchaseOrderId());
        attributes.put("createdDate", getCreatedDate());
        attributes.put("createdUserId", getCreatedUserId());
        attributes.put("evoCodMateriale", getEvoCodMateriale());
        attributes.put("evoDesMateriale", getEvoDesMateriale());
        
        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long positionId = (Long) attributes.get("positionId");

        if (positionId != null) {
            setPositionId(positionId);
        }

        String fiscalYear = (String) attributes.get("fiscalYear");

        if (fiscalYear != null) {
            setFiscalYear(fiscalYear);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        String categoryCode = (String) attributes.get("categoryCode");

        if (categoryCode != null) {
            setCategoryCode(categoryCode);
        }

        String offerNumber = (String) attributes.get("offerNumber");

        if (offerNumber != null) {
            setOfferNumber(offerNumber);
        }

        Date offerDate = (Date) attributes.get("offerDate");

        if (offerDate != null) {
            setOfferDate(offerDate);
        }

        String quatity = (String) attributes.get("quatity");

        if (quatity != null) {
            setQuatity(quatity);
        }

        Date deliveryDate = (Date) attributes.get("deliveryDate");

        if (deliveryDate != null) {
            setDeliveryDate(deliveryDate);
        }

        String deliveryAddress = (String) attributes.get("deliveryAddress");

        if (deliveryAddress != null) {
            setDeliveryAddress(deliveryAddress);
        }

        String actualUnitCost = (String) attributes.get("actualUnitCost");

        if (actualUnitCost != null) {
            setActualUnitCost(actualUnitCost);
        }

        String numberOfUnit = (String) attributes.get("numberOfUnit");

        if (numberOfUnit != null) {
            setNumberOfUnit(numberOfUnit);
        }

        Long labelNumber = (Long) attributes.get("labelNumber");

        if (labelNumber != null) {
            setLabelNumber(labelNumber);
        }

        Long poLabelNumber = (Long) attributes.get("poLabelNumber");

        if (poLabelNumber != null) {
            setPoLabelNumber(poLabelNumber);
        }

        String targaTecnica = (String) attributes.get("targaTecnica");

        if (targaTecnica != null) {
            setTargaTecnica(targaTecnica);
        }

        Boolean approved = (Boolean) attributes.get("approved");

        if (approved != null) {
            setApproved(approved);
        }

        String icApproval = (String) attributes.get("icApproval");

        if (icApproval != null) {
            setIcApproval(icApproval);
        }

        String approver = (String) attributes.get("approver");

        if (approver != null) {
            setApprover(approver);
        }

        String assetNumber = (String) attributes.get("assetNumber");

        if (assetNumber != null) {
            setAssetNumber(assetNumber);
        }

        String shoppingCart = (String) attributes.get("shoppingCart");

        if (shoppingCart != null) {
            setShoppingCart(shoppingCart);
        }

        String poNumber = (String) attributes.get("poNumber");

        if (poNumber != null) {
            setPoNumber(poNumber);
        }

        String evoLocation = (String) attributes.get("evoLocation");

        if (evoLocation != null) {
            setEvoLocation(evoLocation);
        }

        String wbsCodeId = (String) attributes.get("wbsCodeId");

        if (wbsCodeId != null) {
            setWbsCodeId(wbsCodeId);
        }

        String purchaseRequestId = (String) attributes.get("purchaseRequestId");

        if (purchaseRequestId != null) {
            setPurchaseRequestId(purchaseRequestId);
        }

        String purchaseOrderId = (String) attributes.get("purchaseOrderId");

        if (purchaseOrderId != null) {
            setPurchaseOrderId(purchaseOrderId);
        }

        Date createdDate = (Date) attributes.get("createdDate");

        if (createdDate != null) {
            setCreatedDate(createdDate);
        }

        Long createdUserId = (Long) attributes.get("createdUserId");

        if (createdUserId != null) {
            setCreatedUserId(createdUserId);
        }
        
        String evoCodMateriale = (String) attributes.get("evoCodMateriale");

        if (evoCodMateriale != null) {
            setEvoCodMateriale(evoCodMateriale);
        }
        
        String evoDesMateriale = (String) attributes.get("evoDesMateriale");

        if (evoDesMateriale != null) {
            setEvoDesMateriale(evoDesMateriale);
        }

    }

    public long getPositionId() {
        return _positionId;
    }

    public void setPositionId(long positionId) {
        _positionId = positionId;
    }

    public String getFiscalYear() {
        if (_fiscalYear == null) {
            return StringPool.BLANK;
        } else {
            return _fiscalYear;
        }
    }

    public void setFiscalYear(String fiscalYear) {
        _fiscalYear = fiscalYear;
    }

    public String getDescription() {
        if (_description == null) {
            return StringPool.BLANK;
        } else {
            return _description;
        }
    }

    public void setDescription(String description) {
        _description = description;
    }

    public String getCategoryCode() {
        if (_categoryCode == null) {
            return StringPool.BLANK;
        } else {
            return _categoryCode;
        }
    }

    public void setCategoryCode(String categoryCode) {
        _categoryCode = categoryCode;
    }

    public String getOfferNumber() {
        if (_offerNumber == null) {
            return StringPool.BLANK;
        } else {
            return _offerNumber;
        }
    }

    public void setOfferNumber(String offerNumber) {
        _offerNumber = offerNumber;
    }

    public Date getOfferDate() {
        return _offerDate;
    }

    public void setOfferDate(Date offerDate) {
        _offerDate = offerDate;
    }

    public String getQuatity() {
        if (_quatity == null) {
            return StringPool.BLANK;
        } else {
            return _quatity;
        }
    }

    public void setQuatity(String quatity) {
        _quatity = quatity;
    }

    public Date getDeliveryDate() {
        return _deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        _deliveryDate = deliveryDate;
    }

    public String getDeliveryAddress() {
        if (_deliveryAddress == null) {
            return StringPool.BLANK;
        } else {
            return _deliveryAddress;
        }
    }

    public void setDeliveryAddress(String deliveryAddress) {
        _deliveryAddress = deliveryAddress;
    }

    public String getActualUnitCost() {
        if (_actualUnitCost == null) {
            return StringPool.BLANK;
        } else {
            return _actualUnitCost;
        }
    }

    public void setActualUnitCost(String actualUnitCost) {
        _actualUnitCost = actualUnitCost;
    }

    public String getNumberOfUnit() {
        if (_numberOfUnit == null) {
            return StringPool.BLANK;
        } else {
            return _numberOfUnit;
        }
    }

    public void setNumberOfUnit(String numberOfUnit) {
        _numberOfUnit = numberOfUnit;
    }

    public long getLabelNumber() {
        return _labelNumber;
    }

    public void setLabelNumber(long labelNumber) {
        _labelNumber = labelNumber;
    }

    public long getPoLabelNumber() {
        return _poLabelNumber;
    }

    public void setPoLabelNumber(long poLabelNumber) {
        _poLabelNumber = poLabelNumber;
    }

    public String getTargaTecnica() {
        if (_targaTecnica == null) {
            return StringPool.BLANK;
        } else {
            return _targaTecnica;
        }
    }

    public void setTargaTecnica(String targaTecnica) {
        _targaTecnica = targaTecnica;
    }

    public boolean getApproved() {
        return _approved;
    }

    public boolean isApproved() {
        return _approved;
    }

    public void setApproved(boolean approved) {
        _approved = approved;
    }

    public String getIcApproval() {
        if (_icApproval == null) {
            return StringPool.BLANK;
        } else {
            return _icApproval;
        }
    }

    public void setIcApproval(String icApproval) {
        _icApproval = icApproval;
    }

    public String getApprover() {
        if (_approver == null) {
            return StringPool.BLANK;
        } else {
            return _approver;
        }
    }

    public void setApprover(String approver) {
        _approver = approver;
    }

    public String getAssetNumber() {
        if (_assetNumber == null) {
            return StringPool.BLANK;
        } else {
            return _assetNumber;
        }
    }

    public void setAssetNumber(String assetNumber) {
        _assetNumber = assetNumber;
    }

    public String getShoppingCart() {
        if (_shoppingCart == null) {
            return StringPool.BLANK;
        } else {
            return _shoppingCart;
        }
    }

    public void setShoppingCart(String shoppingCart) {
        _shoppingCart = shoppingCart;
    }

    public String getPoNumber() {
        if (_poNumber == null) {
            return StringPool.BLANK;
        } else {
            return _poNumber;
        }
    }

    public void setPoNumber(String poNumber) {
        _poNumber = poNumber;
    }

    public String getEvoLocation() {
        if (_evoLocation == null) {
            return StringPool.BLANK;
        } else {
            return _evoLocation;
        }
    }

    public void setEvoLocation(String evoLocation) {
        _evoLocation = evoLocation;
    }

    public String getWbsCodeId() {
        if (_wbsCodeId == null) {
            return StringPool.BLANK;
        } else {
            return _wbsCodeId;
        }
    }

    public void setWbsCodeId(String wbsCodeId) {
        _wbsCodeId = wbsCodeId;
    }

    public String getPurchaseRequestId() {
        if (_purchaseRequestId == null) {
            return StringPool.BLANK;
        } else {
            return _purchaseRequestId;
        }
    }

    public void setPurchaseRequestId(String purchaseRequestId) {
        _columnBitmask |= PURCHASEREQUESTID_COLUMN_BITMASK;

        if (_originalPurchaseRequestId == null) {
            _originalPurchaseRequestId = _purchaseRequestId;
        }

        _purchaseRequestId = purchaseRequestId;
    }

    public String getOriginalPurchaseRequestId() {
        return GetterUtil.getString(_originalPurchaseRequestId);
    }

    public String getPurchaseOrderId() {
        if (_purchaseOrderId == null) {
            return StringPool.BLANK;
        } else {
            return _purchaseOrderId;
        }
    }

    public void setPurchaseOrderId(String purchaseOrderId) {
        _columnBitmask |= PURCHASEORDERID_COLUMN_BITMASK;

        if (_originalPurchaseOrderId == null) {
            _originalPurchaseOrderId = _purchaseOrderId;
        }

        _purchaseOrderId = purchaseOrderId;
    }

    public String getOriginalPurchaseOrderId() {
        return GetterUtil.getString(_originalPurchaseOrderId);
    }

    public Date getCreatedDate() {
        return _createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        _createdDate = createdDate;
    }

    public long getCreatedUserId() {
        return _createdUserId;
    }

    public void setCreatedUserId(long createdUserId) {
        _createdUserId = createdUserId;
    }

    public String getCreatedUserUuid() throws SystemException {
        return PortalUtil.getUserValue(getCreatedUserId(), "uuid",
            _createdUserUuid);
    }

    public void setCreatedUserUuid(String createdUserUuid) {
        _createdUserUuid = createdUserUuid;
    }

    public String getEvoCodMateriale() {
        if (_evoCodMateriale == null) {
            return StringPool.BLANK;
        } else {
            return _evoCodMateriale;
        }
    }

    public void setEvoCodMateriale(String evoCodMateriale) {
        _evoCodMateriale = evoCodMateriale;
    }
    
    public String getEvoDesMateriale() {
        if (_evoDesMateriale == null) {
            return StringPool.BLANK;
        } else {
            return _evoDesMateriale;
        }
    }

    public void setEvoDesMateriale(String evoDesMateriale) {
        _evoDesMateriale = evoDesMateriale;
    }
    
    public long getColumnBitmask() {
        return _columnBitmask;
    }

    @Override
    public ExpandoBridge getExpandoBridge() {
        return ExpandoBridgeFactoryUtil.getExpandoBridge(0,
            Position.class.getName(), getPrimaryKey());
    }

    @Override
    public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
        ExpandoBridge expandoBridge = getExpandoBridge();

        expandoBridge.setAttributes(serviceContext);
    }

    @Override
    public Position toEscapedModel() {
        if (_escapedModel == null) {
            _escapedModel = (Position) ProxyUtil.newProxyInstance(_classLoader,
                    _escapedModelInterfaces, new AutoEscapeBeanHandler(this));
        }

        return _escapedModel;
    }

    public Position toUnescapedModel() {
        return (Position) this;
    }

    @Override
    public Object clone() {
        PositionImpl positionImpl = new PositionImpl();

        positionImpl.setPositionId(getPositionId());
        positionImpl.setFiscalYear(getFiscalYear());
        positionImpl.setDescription(getDescription());
        positionImpl.setCategoryCode(getCategoryCode());
        positionImpl.setOfferNumber(getOfferNumber());
        positionImpl.setOfferDate(getOfferDate());
        positionImpl.setQuatity(getQuatity());
        positionImpl.setDeliveryDate(getDeliveryDate());
        positionImpl.setDeliveryAddress(getDeliveryAddress());
        positionImpl.setActualUnitCost(getActualUnitCost());
        positionImpl.setNumberOfUnit(getNumberOfUnit());
        positionImpl.setLabelNumber(getLabelNumber());
        positionImpl.setPoLabelNumber(getPoLabelNumber());
        positionImpl.setTargaTecnica(getTargaTecnica());
        positionImpl.setApproved(getApproved());
        positionImpl.setIcApproval(getIcApproval());
        positionImpl.setApprover(getApprover());
        positionImpl.setAssetNumber(getAssetNumber());
        positionImpl.setShoppingCart(getShoppingCart());
        positionImpl.setPoNumber(getPoNumber());
        positionImpl.setEvoLocation(getEvoLocation());
        positionImpl.setWbsCodeId(getWbsCodeId());
        positionImpl.setPurchaseRequestId(getPurchaseRequestId());
        positionImpl.setPurchaseOrderId(getPurchaseOrderId());
        positionImpl.setCreatedDate(getCreatedDate());
        positionImpl.setCreatedUserId(getCreatedUserId());
        positionImpl.setEvoCodMateriale(getEvoCodMateriale());
        positionImpl.setEvoDesMateriale(getEvoDesMateriale());

        positionImpl.resetOriginalValues();

        return positionImpl;
    }

    public int compareTo(Position position) {
        long primaryKey = position.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof Position)) {
            return false;
        }

        Position position = (Position) obj;

        long primaryKey = position.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public void resetOriginalValues() {
        PositionModelImpl positionModelImpl = this;

        positionModelImpl._originalPurchaseRequestId = positionModelImpl._purchaseRequestId;

        positionModelImpl._originalPurchaseOrderId = positionModelImpl._purchaseOrderId;

        positionModelImpl._columnBitmask = 0;
    }

    @Override
    public CacheModel<Position> toCacheModel() {
        PositionCacheModel positionCacheModel = new PositionCacheModel();

        positionCacheModel.positionId = getPositionId();

        positionCacheModel.fiscalYear = getFiscalYear();

        String fiscalYear = positionCacheModel.fiscalYear;

        if ((fiscalYear != null) && (fiscalYear.length() == 0)) {
            positionCacheModel.fiscalYear = null;
        }

        positionCacheModel.description = getDescription();

        String description = positionCacheModel.description;

        if ((description != null) && (description.length() == 0)) {
            positionCacheModel.description = null;
        }

        positionCacheModel.categoryCode = getCategoryCode();

        String categoryCode = positionCacheModel.categoryCode;

        if ((categoryCode != null) && (categoryCode.length() == 0)) {
            positionCacheModel.categoryCode = null;
        }

        positionCacheModel.offerNumber = getOfferNumber();

        String offerNumber = positionCacheModel.offerNumber;

        if ((offerNumber != null) && (offerNumber.length() == 0)) {
            positionCacheModel.offerNumber = null;
        }

        Date offerDate = getOfferDate();

        if (offerDate != null) {
            positionCacheModel.offerDate = offerDate.getTime();
        } else {
            positionCacheModel.offerDate = Long.MIN_VALUE;
        }

        positionCacheModel.quatity = getQuatity();

        String quatity = positionCacheModel.quatity;

        if ((quatity != null) && (quatity.length() == 0)) {
            positionCacheModel.quatity = null;
        }

        Date deliveryDate = getDeliveryDate();

        if (deliveryDate != null) {
            positionCacheModel.deliveryDate = deliveryDate.getTime();
        } else {
            positionCacheModel.deliveryDate = Long.MIN_VALUE;
        }

        positionCacheModel.deliveryAddress = getDeliveryAddress();

        String deliveryAddress = positionCacheModel.deliveryAddress;

        if ((deliveryAddress != null) && (deliveryAddress.length() == 0)) {
            positionCacheModel.deliveryAddress = null;
        }

        positionCacheModel.actualUnitCost = getActualUnitCost();

        String actualUnitCost = positionCacheModel.actualUnitCost;

        if ((actualUnitCost != null) && (actualUnitCost.length() == 0)) {
            positionCacheModel.actualUnitCost = null;
        }

        positionCacheModel.numberOfUnit = getNumberOfUnit();

        String numberOfUnit = positionCacheModel.numberOfUnit;

        if ((numberOfUnit != null) && (numberOfUnit.length() == 0)) {
            positionCacheModel.numberOfUnit = null;
        }

        positionCacheModel.labelNumber = getLabelNumber();

        positionCacheModel.poLabelNumber = getPoLabelNumber();

        positionCacheModel.targaTecnica = getTargaTecnica();

        String targaTecnica = positionCacheModel.targaTecnica;

        if ((targaTecnica != null) && (targaTecnica.length() == 0)) {
            positionCacheModel.targaTecnica = null;
        }

        positionCacheModel.approved = getApproved();

        positionCacheModel.icApproval = getIcApproval();

        String icApproval = positionCacheModel.icApproval;

        if ((icApproval != null) && (icApproval.length() == 0)) {
            positionCacheModel.icApproval = null;
        }

        positionCacheModel.approver = getApprover();

        String approver = positionCacheModel.approver;

        if ((approver != null) && (approver.length() == 0)) {
            positionCacheModel.approver = null;
        }

        positionCacheModel.assetNumber = getAssetNumber();

        String assetNumber = positionCacheModel.assetNumber;

        if ((assetNumber != null) && (assetNumber.length() == 0)) {
            positionCacheModel.assetNumber = null;
        }

        positionCacheModel.shoppingCart = getShoppingCart();

        String shoppingCart = positionCacheModel.shoppingCart;

        if ((shoppingCart != null) && (shoppingCart.length() == 0)) {
            positionCacheModel.shoppingCart = null;
        }

        positionCacheModel.poNumber = getPoNumber();

        String poNumber = positionCacheModel.poNumber;

        if ((poNumber != null) && (poNumber.length() == 0)) {
            positionCacheModel.poNumber = null;
        }

        positionCacheModel.evoLocation = getEvoLocation();

        String evoLocation = positionCacheModel.evoLocation;

        if ((evoLocation != null) && (evoLocation.length() == 0)) {
            positionCacheModel.evoLocation = null;
        }

        positionCacheModel.evoCodMateriale = getEvoCodMateriale();

        String evoCodMateriale = positionCacheModel.evoCodMateriale;

        if ((evoCodMateriale != null) && (evoCodMateriale.length() == 0)) {
            positionCacheModel.evoCodMateriale = null;
        }

        positionCacheModel.evoDesMateriale = getEvoDesMateriale();

        String evoDesMateriale = positionCacheModel.evoDesMateriale;

        if ((evoDesMateriale != null) && (evoDesMateriale.length() == 0)) {
            positionCacheModel.evoDesMateriale = null;
        }
        
        positionCacheModel.wbsCodeId = getWbsCodeId();

        String wbsCodeId = positionCacheModel.wbsCodeId;

        if ((wbsCodeId != null) && (wbsCodeId.length() == 0)) {
            positionCacheModel.wbsCodeId = null;
        }

        positionCacheModel.purchaseRequestId = getPurchaseRequestId();

        String purchaseRequestId = positionCacheModel.purchaseRequestId;

        if ((purchaseRequestId != null) && (purchaseRequestId.length() == 0)) {
            positionCacheModel.purchaseRequestId = null;
        }

        positionCacheModel.purchaseOrderId = getPurchaseOrderId();

        String purchaseOrderId = positionCacheModel.purchaseOrderId;

        if ((purchaseOrderId != null) && (purchaseOrderId.length() == 0)) {
            positionCacheModel.purchaseOrderId = null;
        }

        Date createdDate = getCreatedDate();

        if (createdDate != null) {
            positionCacheModel.createdDate = createdDate.getTime();
        } else {
            positionCacheModel.createdDate = Long.MIN_VALUE;
        }

        positionCacheModel.createdUserId = getCreatedUserId();

        return positionCacheModel;
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(53);

        sb.append("{positionId=");
        sb.append(getPositionId());
        sb.append(", fiscalYear=");
        sb.append(getFiscalYear());
        sb.append(", description=");
        sb.append(getDescription());
        sb.append(", categoryCode=");
        sb.append(getCategoryCode());
        sb.append(", offerNumber=");
        sb.append(getOfferNumber());
        sb.append(", offerDate=");
        sb.append(getOfferDate());
        sb.append(", quatity=");
        sb.append(getQuatity());
        sb.append(", deliveryDate=");
        sb.append(getDeliveryDate());
        sb.append(", deliveryAddress=");
        sb.append(getDeliveryAddress());
        sb.append(", actualUnitCost=");
        sb.append(getActualUnitCost());
        sb.append(", numberOfUnit=");
        sb.append(getNumberOfUnit());
        sb.append(", labelNumber=");
        sb.append(getLabelNumber());
        sb.append(", poLabelNumber=");
        sb.append(getPoLabelNumber());
        sb.append(", targaTecnica=");
        sb.append(getTargaTecnica());
        sb.append(", approved=");
        sb.append(getApproved());
        sb.append(", icApproval=");
        sb.append(getIcApproval());
        sb.append(", approver=");
        sb.append(getApprover());
        sb.append(", assetNumber=");
        sb.append(getAssetNumber());
        sb.append(", shoppingCart=");
        sb.append(getShoppingCart());
        sb.append(", poNumber=");
        sb.append(getPoNumber());
        sb.append(", evoLocation=");
        sb.append(getEvoLocation());
        sb.append(", wbsCodeId=");
        sb.append(getWbsCodeId());
        sb.append(", purchaseRequestId=");
        sb.append(getPurchaseRequestId());
        sb.append(", purchaseOrderId=");
        sb.append(getPurchaseOrderId());
        sb.append(", createdDate=");
        sb.append(getCreatedDate());
        sb.append(", createdUserId=");
        sb.append(getCreatedUserId());
        sb.append(", evoCodMateriale=");
        sb.append(getEvoCodMateriale());
        sb.append(", evoDesMateriale=");
        sb.append(getEvoDesMateriale());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(82);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.Position");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>positionId</column-name><column-value><![CDATA[");
        sb.append(getPositionId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>fiscalYear</column-name><column-value><![CDATA[");
        sb.append(getFiscalYear());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>description</column-name><column-value><![CDATA[");
        sb.append(getDescription());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>categoryCode</column-name><column-value><![CDATA[");
        sb.append(getCategoryCode());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>offerNumber</column-name><column-value><![CDATA[");
        sb.append(getOfferNumber());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>offerDate</column-name><column-value><![CDATA[");
        sb.append(getOfferDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>quatity</column-name><column-value><![CDATA[");
        sb.append(getQuatity());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>deliveryDate</column-name><column-value><![CDATA[");
        sb.append(getDeliveryDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>deliveryAddress</column-name><column-value><![CDATA[");
        sb.append(getDeliveryAddress());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>actualUnitCost</column-name><column-value><![CDATA[");
        sb.append(getActualUnitCost());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>numberOfUnit</column-name><column-value><![CDATA[");
        sb.append(getNumberOfUnit());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>labelNumber</column-name><column-value><![CDATA[");
        sb.append(getLabelNumber());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>poLabelNumber</column-name><column-value><![CDATA[");
        sb.append(getPoLabelNumber());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>targaTecnica</column-name><column-value><![CDATA[");
        sb.append(getTargaTecnica());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>approved</column-name><column-value><![CDATA[");
        sb.append(getApproved());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>icApproval</column-name><column-value><![CDATA[");
        sb.append(getIcApproval());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>approver</column-name><column-value><![CDATA[");
        sb.append(getApprover());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>assetNumber</column-name><column-value><![CDATA[");
        sb.append(getAssetNumber());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>shoppingCart</column-name><column-value><![CDATA[");
        sb.append(getShoppingCart());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>poNumber</column-name><column-value><![CDATA[");
        sb.append(getPoNumber());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>evoLocation</column-name><column-value><![CDATA[");
        sb.append(getEvoLocation());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>wbsCodeId</column-name><column-value><![CDATA[");
        sb.append(getWbsCodeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>purchaseRequestId</column-name><column-value><![CDATA[");
        sb.append(getPurchaseRequestId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>purchaseOrderId</column-name><column-value><![CDATA[");
        sb.append(getPurchaseOrderId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>createdDate</column-name><column-value><![CDATA[");
        sb.append(getCreatedDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>createdUserId</column-name><column-value><![CDATA[");
        sb.append(getCreatedUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>evoCodMateriale</column-name><column-value><![CDATA[");
        sb.append(getEvoCodMateriale());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>evoDesMateriale</column-name><column-value><![CDATA[");
        sb.append(getEvoDesMateriale());
        sb.append("]]></column-value></column>");
        
        sb.append("</model>");

        return sb.toString();
    }
}
