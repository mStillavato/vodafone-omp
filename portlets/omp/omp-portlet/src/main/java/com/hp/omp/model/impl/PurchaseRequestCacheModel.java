package com.hp.omp.model.impl;

import com.hp.omp.model.PurchaseRequest;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing PurchaseRequest in entity cache.
 *
 * @author HP Egypt team
 * @see PurchaseRequest
 * @generated
 */
public class PurchaseRequestCacheModel implements CacheModel<PurchaseRequest>,
    Serializable {
    public long purchaseRequestId;
    public String costCenterId;
    public String vendorId;
    public String buyer;
    public String ola;
    public String projectId;
    public String subProjectId;
    public String budgetCategoryId;
    public String budgetSubCategoryId;
    public String activityDescription;
    public String macroDriverId;
    public String microDriverId;
    public String odnpCodeId;
    public String odnpNameId;
    public String totalValue;
    public String currency;
    public String receiverUserId;
    public String fiscalYear;
    public String screenNameRequester;
    public String screenNameReciever;
    public boolean automatic;
    public int status;
    public long createdDate;
    public long createdUserId;
    public String rejectionCause;
    public String wbsCodeId;
    public String trackingCode;
    public String buyerId;
    public String targaTecnica;
    public String evoLocationId;
    public int tipoPr;
    public String plant;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(65);

        sb.append("{purchaseRequestId=");
        sb.append(purchaseRequestId);
        sb.append(", costCenterId=");
        sb.append(costCenterId);
        sb.append(", vendorId=");
        sb.append(vendorId);
        sb.append(", buyer=");
        sb.append(buyer);
        sb.append(", ola=");
        sb.append(ola);
        sb.append(", projectId=");
        sb.append(projectId);
        sb.append(", subProjectId=");
        sb.append(subProjectId);
        sb.append(", budgetCategoryId=");
        sb.append(budgetCategoryId);
        sb.append(", budgetSubCategoryId=");
        sb.append(budgetSubCategoryId);
        sb.append(", activityDescription=");
        sb.append(activityDescription);
        sb.append(", macroDriverId=");
        sb.append(macroDriverId);
        sb.append(", microDriverId=");
        sb.append(microDriverId);
        sb.append(", odnpCodeId=");
        sb.append(odnpCodeId);
        sb.append(", odnpNameId=");
        sb.append(odnpNameId);
        sb.append(", totalValue=");
        sb.append(totalValue);
        sb.append(", currency=");
        sb.append(currency);
        sb.append(", receiverUserId=");
        sb.append(receiverUserId);
        sb.append(", fiscalYear=");
        sb.append(fiscalYear);
        sb.append(", screenNameRequester=");
        sb.append(screenNameRequester);
        sb.append(", screenNameReciever=");
        sb.append(screenNameReciever);
        sb.append(", automatic=");
        sb.append(automatic);
        sb.append(", status=");
        sb.append(status);
        sb.append(", createdDate=");
        sb.append(createdDate);
        sb.append(", createdUserId=");
        sb.append(createdUserId);
        sb.append(", rejectionCause=");
        sb.append(rejectionCause);
        sb.append(", wbsCodeId=");
        sb.append(wbsCodeId);
        sb.append(", trackingCode=");
        sb.append(trackingCode);
        sb.append(", buyerId=");
        sb.append(buyerId);
        sb.append(", targaTecnica=");
        sb.append(targaTecnica);
        sb.append(", evoLocationId=");
        sb.append(evoLocationId);
        sb.append(", tipoPr=");
        sb.append(tipoPr);
        sb.append(", plant=");
        sb.append(plant);
        sb.append("}");

        return sb.toString();
    }

    public PurchaseRequest toEntityModel() {
        PurchaseRequestImpl purchaseRequestImpl = new PurchaseRequestImpl();

        purchaseRequestImpl.setPurchaseRequestId(purchaseRequestId);

        if (costCenterId == null) {
            purchaseRequestImpl.setCostCenterId(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setCostCenterId(costCenterId);
        }

        if (vendorId == null) {
            purchaseRequestImpl.setVendorId(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setVendorId(vendorId);
        }

        if (buyer == null) {
            purchaseRequestImpl.setBuyer(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setBuyer(buyer);
        }

        if (ola == null) {
            purchaseRequestImpl.setOla(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setOla(ola);
        }

        if (projectId == null) {
            purchaseRequestImpl.setProjectId(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setProjectId(projectId);
        }

        if (subProjectId == null) {
            purchaseRequestImpl.setSubProjectId(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setSubProjectId(subProjectId);
        }

        if (budgetCategoryId == null) {
            purchaseRequestImpl.setBudgetCategoryId(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setBudgetCategoryId(budgetCategoryId);
        }

        if (budgetSubCategoryId == null) {
            purchaseRequestImpl.setBudgetSubCategoryId(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setBudgetSubCategoryId(budgetSubCategoryId);
        }

        if (activityDescription == null) {
            purchaseRequestImpl.setActivityDescription(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setActivityDescription(activityDescription);
        }

        if (macroDriverId == null) {
            purchaseRequestImpl.setMacroDriverId(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setMacroDriverId(macroDriverId);
        }

        if (microDriverId == null) {
            purchaseRequestImpl.setMicroDriverId(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setMicroDriverId(microDriverId);
        }

        if (odnpCodeId == null) {
            purchaseRequestImpl.setOdnpCodeId(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setOdnpCodeId(odnpCodeId);
        }

        if (odnpNameId == null) {
            purchaseRequestImpl.setOdnpNameId(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setOdnpNameId(odnpNameId);
        }

        if (totalValue == null) {
            purchaseRequestImpl.setTotalValue(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setTotalValue(totalValue);
        }

        if (currency == null) {
            purchaseRequestImpl.setCurrency(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setCurrency(currency);
        }

        if (receiverUserId == null) {
            purchaseRequestImpl.setReceiverUserId(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setReceiverUserId(receiverUserId);
        }

        if (fiscalYear == null) {
            purchaseRequestImpl.setFiscalYear(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setFiscalYear(fiscalYear);
        }

        if (screenNameRequester == null) {
            purchaseRequestImpl.setScreenNameRequester(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setScreenNameRequester(screenNameRequester);
        }

        if (screenNameReciever == null) {
            purchaseRequestImpl.setScreenNameReciever(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setScreenNameReciever(screenNameReciever);
        }

        purchaseRequestImpl.setAutomatic(automatic);
        purchaseRequestImpl.setStatus(status);

        if (createdDate == Long.MIN_VALUE) {
            purchaseRequestImpl.setCreatedDate(null);
        } else {
            purchaseRequestImpl.setCreatedDate(new Date(createdDate));
        }

        purchaseRequestImpl.setCreatedUserId(createdUserId);

        if (rejectionCause == null) {
            purchaseRequestImpl.setRejectionCause(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setRejectionCause(rejectionCause);
        }

        if (wbsCodeId == null) {
            purchaseRequestImpl.setWbsCodeId(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setWbsCodeId(wbsCodeId);
        }

        if (trackingCode == null) {
            purchaseRequestImpl.setTrackingCode(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setTrackingCode(trackingCode);
        }

        if (buyerId == null) {
            purchaseRequestImpl.setBuyerId(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setBuyerId(buyerId);
        }

        if (targaTecnica == null) {
            purchaseRequestImpl.setTargaTecnica(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setTargaTecnica(targaTecnica);
        }
        
        if (evoLocationId == null) {
            purchaseRequestImpl.setEvoLocationId(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setEvoLocationId(evoLocationId);
        }
        
        purchaseRequestImpl.setTipoPr(tipoPr);
        
        if (plant == null) {
            purchaseRequestImpl.setPlant(StringPool.BLANK);
        } else {
            purchaseRequestImpl.setPlant(plant);
        }
        
        purchaseRequestImpl.resetOriginalValues();

        return purchaseRequestImpl;
    }
}
