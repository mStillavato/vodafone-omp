package com.hp.omp.model.impl;

import com.hp.omp.model.Location;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;
import java.util.Date;

/**
 * The cache model class for representing Location in entity cache.
 *
 * @author HP Egypt team
 * @see Location
 * @generated
 */
public class LocationCacheModel implements CacheModel<Location>, Serializable {
    public long locationTargaId;
    public String codiceOffice;
    public String targaTecnicaSap;
    public String location;
    public String pr;
    public long dataOnAir;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(9);

        sb.append("{locationTargaId=");
        sb.append(locationTargaId);
        sb.append(", codiceOffice=");
        sb.append(codiceOffice);
        sb.append(", targaTecnicaSap=");
        sb.append(targaTecnicaSap);
        sb.append(", location=");
        sb.append(location);
        sb.append(", pr=");
        sb.append(pr);
        sb.append(", dataOnAir=");
        sb.append(dataOnAir);
        sb.append("}");

        return sb.toString();
    }

    public Location toEntityModel() {
        LocationImpl locationImpl = new LocationImpl();

        locationImpl.setLocationTargaId(locationTargaId);

        if (codiceOffice == null) {
            locationImpl.setCodiceOffice(StringPool.BLANK);
        } else {
            locationImpl.setCodiceOffice(codiceOffice);
        }

        if (targaTecnicaSap == null) {
            locationImpl.setTargaTecnicaSap(StringPool.BLANK);
        } else {
            locationImpl.setTargaTecnicaSap(targaTecnicaSap);
        }
        
        if (location == null) {
            locationImpl.setLocation(StringPool.BLANK);
        } else {
            locationImpl.setLocation(location);
        }
        
        if (pr == null) {
            locationImpl.setPr(StringPool.BLANK);
        } else {
            locationImpl.setPr(pr);
        }
        
		if (dataOnAir == Long.MIN_VALUE) {
			locationImpl.setDataOnAir(null);
		}
		else {
			locationImpl.setDataOnAir(new Date(dataOnAir));
		}

        locationImpl.resetOriginalValues();

        return locationImpl;
    }
}
