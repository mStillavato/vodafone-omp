package com.hp.omp.service.persistence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.hp.omp.model.custom.SearchDTO;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.Type;


public class PositionFinderImpl extends PositionPersistenceImpl implements PositionFinder{
	
	private static Log _log = LogFactoryUtil.getLog(PositionFinderImpl.class);

	public List<String> getAllFiscalYears() throws SystemException {
		return getAllValues(GET_ALL_FISCALYEARS);
	}
	
	public List<String> getAllWBSCodes() throws SystemException {
		return getAllValues(GET_ALL_WBS_CODES);
	}
	
	
	private List<String> getAllValues(String sqlID) throws SystemException{
		List<String> allValues = new ArrayList<String>();
		String value = null;
		Session session = null;
		try {
			session = openSession();
			String hql = CustomSQLUtil.get(sqlID);
			Query query = session.createQuery(hql);
			@SuppressWarnings("unchecked")
			Iterator<String> iterate = (Iterator<String>)QueryUtil.iterate(query, getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			while(iterate.hasNext()){
				value = iterate.next();
				allValues.add(value);
			}
			return allValues;
		} catch (ORMException e) {
			_log.error(e.getMessage(), e);
			throw new SystemException(e);
		} finally{
			closeSession(session);
		}
	}
	
	
	public Long getIAndCPositionsCount(String purchaseRequestId) throws SystemException {

		Long count = 0L;
		String subProjectName = null;
		Session session = null;
		try {
			session = openSession();
			String hql = CustomSQLUtil.get(GET_I_AND_C_POSITIONS_COUNT);
			@SuppressWarnings("unchecked")
			Query query = session.createQuery(hql);		
			QueryPos queryPos = QueryPos.getInstance(query);
			queryPos.add(purchaseRequestId);
			List<Object> list= (List<Object>) QueryUtil.list(query, getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			
			count = (Long) list.get(0);
			
			return count;
		} catch (ORMException e) {
			_log.error(e.getMessage(), e);
			throw new SystemException(e);
		} finally{
			closeSession(session);
		}
	
	}
	
	
	static final String GET_ALL_FISCALYEARS = PositionFinderImpl.class.getName()+".getAllFiscalYears";
	static final String GET_ALL_WBS_CODES = PositionFinderImpl.class.getName()+".getAllWBSCodes";
	static final String GET_I_AND_C_POSITIONS_COUNT = PositionFinderImpl.class.getName()+".getIAndCPositionsCount";

}
