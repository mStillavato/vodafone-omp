package com.hp.omp.portlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.exceptions.PRHasNoCostCenterException;
import com.hp.omp.helper.OmpHelper;
import com.hp.omp.helper.PurchaseRequestPortletHelper;
import com.hp.omp.helper.UploadDownloadHelper;
import com.hp.omp.model.Buyer;
import com.hp.omp.model.CostCenter;
import com.hp.omp.model.GoodReceipt;
import com.hp.omp.model.MacroDriver;
import com.hp.omp.model.MicroDriver;
import com.hp.omp.model.Position;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.TrackingCodes;
import com.hp.omp.model.Vendor;
import com.hp.omp.model.custom.Currency;
import com.hp.omp.model.custom.GruppoUsers;
import com.hp.omp.model.custom.IAndCType;
import com.hp.omp.model.custom.OmpFormatterUtil;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.custom.PurchaseRequestStatus;
import com.hp.omp.model.custom.TipoPr;
import com.hp.omp.model.impl.PositionImpl;
import com.hp.omp.model.impl.PurchaseRequestImpl;
import com.hp.omp.service.BuyerLocalServiceUtil;
import com.hp.omp.service.CostCenterLocalServiceUtil;
import com.hp.omp.service.GoodReceiptLocalServiceUtil;
import com.hp.omp.service.MacroDriverLocalServiceUtil;
import com.hp.omp.service.MicroDriverLocalServiceUtil;
import com.hp.omp.service.PositionLocalServiceUtil;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.hp.omp.service.TrackingCodesLocalServiceUtil;
import com.hp.omp.service.VendorLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * @author sami.basyoni
 * 
 */
public class PurchaseRequestVbsPortlet extends MVCPortlet{


	Logger logger = LoggerFactory.getLogger(PurchaseOrderPortlet.class);
	public static final String DATE_FORMAT = "MM/dd/yyyy"; 


	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		try {
			List<Vendor> allVendorsList = VendorLocalServiceUtil.getVendorsList(0, 
					VendorLocalServiceUtil.getVendorsCountList(null, GruppoUsers.VBS.getCode()), 
					null, 
					GruppoUsers.VBS.getCode());
			
			renderRequest.setAttribute("allVendors", allVendorsList);
			logger.debug("VendorsCount = " + allVendorsList.size());

			List<IAndCType> allICTypes = new ArrayList<IAndCType>();
			allICTypes.add(IAndCType.ICSERVIZI);
			renderRequest.setAttribute("allICTypes", allICTypes);

//			List<CostCenter> costCentersList = CostCenterLocalServiceUtil.getCostCenters(0,CostCenterLocalServiceUtil.getCostCentersCount());
//			List<CostCenter> costCentersListVbs = new ArrayList<CostCenter>();
//			for(CostCenter cc : costCentersList){
//				if (cc.getDescription().contains("VBS")){
//					costCentersListVbs.add(cc);
//				}
//			}
//			renderRequest.setAttribute("allCostCenters",costCentersListVbs);
			
			List<CostCenter> costCenterList = CostCenterLocalServiceUtil.getCostCentersList(0, 
					CostCenterLocalServiceUtil.getCostCentersCount(), null, GruppoUsers.VBS.getCode());
			renderRequest.setAttribute("allCostCenters",costCenterList);

			List<Buyer> buyersList = BuyerLocalServiceUtil.getBuyersList(0, 
					BuyerLocalServiceUtil.getBuyersCountList(null, GruppoUsers.VBS.getCode()), 
					null, 
					GruppoUsers.VBS.getCode());
			renderRequest.setAttribute("allBuyers",buyersList);
			
		} catch (SystemException e) {
			logger.error("error happened during retrieving WBS code/Projects list",e);
		}
		super.doView(renderRequest, renderResponse);
	}

	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {

		handlePRForm(request,response);

		super.render(request, response);
	}

	private void handlePRForm(RenderRequest request, RenderResponse response) {
		PurchaseRequest purchaseRequest = null;
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		HttpServletRequest httpServletRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(request));

		String purchaseRequestID =httpServletRequest.getParameter("purchaseRequestID");//ParamUtil.getLong(request, "purchaseRequestID");
		String actionStatus =httpServletRequest.getParameter("actionStatus");//ParamUtil.getString(request, "actionStatus");
		String automatic= httpServletRequest.getParameter("automatic");
		String redirect=httpServletRequest.getParameter("redirect");

		if(redirect==null || "".equals(redirect)){
			redirect ="/group/vodafone/home"; 
		}

		String dollarSelected="";
		String euroSelected="";
		boolean hasUploadedFile=false;

		if (actionStatus == null || "".equals(actionStatus)) {
			actionStatus = PurchaseRequestStatus.CREATED.getLabel();
		}

		logger.debug("actionStatus = " + actionStatus);
		try {
			String userType =OmpHelper.getUserRoles(themeDisplay.getUserId());//OmpHelper.getUserGroup(request);
			logger.debug("userType = " + userType);

			List<Position> positions = new ArrayList<Position>();
			List<String> uploadedFiles = new ArrayList<String>();
			int numberOfPositions = 0;
			int positionStatus = PurchaseRequestPortletHelper.getPositionStatusfromActionStatus(actionStatus);
			if (purchaseRequestID != null && ! "".equals(purchaseRequestID)) {
				logger.debug("positionStatus =  " + positionStatus);
				purchaseRequest = PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.valueOf(purchaseRequestID));
			} else {
				purchaseRequest = purchaseRequestFromRequest(request);

			}
			if(purchaseRequest.getPurchaseRequestId() > 0){
				if(userType.equals(OmpRoles.OMP_APPROVER_VBS.getLabel())) {
					positions = PositionLocalServiceUtil.getPositionsByIAndCType(purchaseRequest.getPurchaseRequestId(), IAndCType.ICSERVIZI.getLabel());
				}else {	
					positions = PositionLocalServiceUtil.getPositionsByPrId(purchaseRequest.getPurchaseRequestId());
				}
				if (positions != null) {
					PurchaseRequestPortletHelper.AssignPositionsEditBtton(actionStatus,userType,positions);
					numberOfPositions = positions.size();
					//purchaseRequest.setTotalValue(String.valueOf(calculatePrTotalValue(positions)));
				}
				logger.debug("numberOfPositions =  " + numberOfPositions);
				uploadedFiles=UploadDownloadHelper.getPRUploadedFilesNames(purchaseRequest.getPurchaseRequestId());
				logger.debug("uploadedFiles size=  " + uploadedFiles.size());
			}
			else{
				purchaseRequest.setAutomatic(Boolean.valueOf(automatic));
				purchaseRequest.setTotalValue("0");
			}

			boolean nssFromFileExcel = false;
			if(purchaseRequest.getTipoPr() == 1 && purchaseRequest.isHasFile())
				nssFromFileExcel = true;

			String[] disableEnable = PurchaseRequestPortletHelper.getDiableEnableView(userType, actionStatus, purchaseRequest.getAutomatic(), nssFromFileExcel);

			String prDisabled = disableEnable[0];
			String assetNumberDisabled = disableEnable[1];
			String prcsNumberDisabled = disableEnable[2];
			String poNumberDisabled = disableEnable[3];
			String evoDisabled=disableEnable[4];
			String editPositionDisabled=disableEnable[5];
			userType = disableEnable[6];

			String poTargaTecnicaDisabled= disableEnable[7];
			String prDeleteBtn=disableEnable[8];
			String rejCauseDisabled= disableEnable[9];

			if(purchaseRequest.getCurrency().equals(Currency.USD.getCode()+"")){
				dollarSelected="selected";
			}else if(purchaseRequest.getCurrency().equals(Currency.EUR.getCode()+"")){
				euroSelected="selected";
			}

//			if ((purchaseRequest.getCostCenterId() == null || "".equals(purchaseRequest.getCostCenterId()))	&& ! OmpRoles.OMP_CONTROLLER.getLabel().equals(userType)) {
//
//				CostCenter costCenter = null;
//				try {
//					CostCenterUser costCenterUser = CostCenterUserLocalServiceUtil.getCostCenterUserByUserId(themeDisplay.getUserId());
//					if (costCenterUser != null) {
//						costCenter = CostCenterLocalServiceUtil.getCostCenter(costCenterUser.getCostCenterId());
//					} else {
//						throw new UserHasNoCostCenterException("the logged in user has't cost center");
//					}
//				} catch (Exception e) {
//					logger.error("Erro in handle PR Form", e);
//					SessionErrors.add(request, "error-costcenter");
//				}
//
//				if (costCenter != null) {
//					purchaseRequest.setCostCenterId(String.valueOf(costCenter.getCostCenterId()));
//				}
//			}



			logger.debug("prDisabled = " + prDisabled);
			logger.debug("assetNumberDisabled = " + assetNumberDisabled);
			logger.debug("prcsNumberDisabled = " + prcsNumberDisabled);
			logger.debug("poNumberDisabled = " + poNumberDisabled);
			logger.debug("new userType = " + userType);
			logger.debug("dollarSelected =  " + dollarSelected);
			logger.debug("euroSelected =  " + euroSelected);
			logger.debug("poTargaTecnicaDisabled =  " + poTargaTecnicaDisabled);
			logger.debug("prDeleteBtn =" + prDeleteBtn);
			logger.debug("rejCauseDisabled =  " + rejCauseDisabled);


			hasUploadedFile= UploadDownloadHelper.isPRHasUploadedFiles(purchaseRequest.getPurchaseRequestId());

			request.setAttribute("pr", purchaseRequest);
			request.setAttribute("positions", positions);
			request.setAttribute("uploadedFiles", uploadedFiles);
			request.setAttribute("actionStatus", actionStatus);
			request.setAttribute("numberOfPositions", numberOfPositions);
			request.setAttribute("prDisabled", prDisabled);
			request.setAttribute("evoDisabled", evoDisabled);
			request.setAttribute("editPositionDisabled", editPositionDisabled);
			request.setAttribute("assetNumberDisabled", assetNumberDisabled);
			request.setAttribute("prcsNumberDisabled", prcsNumberDisabled);
			request.setAttribute("poNumberDisabled", poNumberDisabled);
			request.setAttribute("userType", userType);
			request.setAttribute("dollarSelected", dollarSelected);
			request.setAttribute("euroSelected", euroSelected);
			request.setAttribute("hasUploadedFile", hasUploadedFile);
			request.setAttribute("redirect",redirect);
			request.setAttribute("userType",userType);
			request.setAttribute("poTargaTecnicaDisabled",poTargaTecnicaDisabled);
			if(purchaseRequest.getTotalValue() !=null && ! "".equals(purchaseRequest.getTotalValue())) {
				request.setAttribute("prTotalValue", OmpFormatterUtil.formatNumberByLocale(Double.valueOf(purchaseRequest.getTotalValue()),Locale.ITALY));
			}else {
				request.setAttribute("prTotalValue","0");
			}
			request.setAttribute("prDeleteBtn", prDeleteBtn);
			request.setAttribute("rejCauseDisabled", rejCauseDisabled);

			logger.debug("automatic = " + purchaseRequest.getAutomatic());
			logger.debug("hasUploadedFile = " + hasUploadedFile);

		} catch (PortalException e) {
			logger.error("Erro in handle PR Form", e);
			SessionErrors.add(request, "error-handling");
		} catch (SystemException e) {
			logger.error("Erro in handle PR Form", e);
			SessionErrors.add(request, "error-handling");
		}catch (PRHasNoCostCenterException e) {
			logger.error("Erro in handle PR Form", e);
			SessionErrors.add(request, "error-costcenter");
		}catch (Exception e) {
			logger.error("Erro in handle PR Form", e);
			SessionErrors.add(request, "error-handling");
		}
	}

	public void serveResource(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		String action=request.getParameter("action");
		logger.debug("action "+action);
		if("addPurchaseRequest".equals(action)){
			addPurchaseRequest(request, response);
		}else if("updatePurchaseRequest".equals(action)){
			updatePurchaseRequest(request, response);
		}else if("addPosition".equals(action)){
			addPosition(request, response);
		}
		else if("updatePosition".equals(action)){
			updatePosition(request, response);
		}else if("deletePosition".equals(action)){
			deletePosition(request, response);
		}else if("downloadFile".equals(action)){
			downloadFile(request, response);
		}else if("downloadAllFiles".equals(action)){
			downloadAllFiles(request, response);
		}else if("listFiles".equals(action)){
			listUploadedFiles(request, response);
		}else if("uploadFile".equals(action)){
			uploadFile(request, response);
		}else if("deleteFile".equals(action)){
			deleteFile(request, response);
		}else if("approvePosition".equals(action)){
			approvePosition(request, response);
		}else if("getLabelNumber".equals(action)){
			getLabelNumber(request, response);
		}


	}

	/**
	 * Adds a new purchase order to the database.
	 * 
	 */
	private void addPurchaseRequest(ResourceRequest request, ResourceResponse response) throws PortletException, IOException{ 

		logger.debug(" inside addPurchaseRequest ");
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		PrintWriter writer = response.getWriter();

		try{
			PurchaseRequest purchaseRequest = purchaseRequestFromRequest(request);
			User user = themeDisplay.getUser();
			purchaseRequest.setCreatedUserId(user.getUserId());
			purchaseRequest.setScreenNameRequester(user.getScreenName());
			purchaseRequest.setStatus(PurchaseRequestStatus.CREATED.getCode());
			purchaseRequest.setCreatedDate(new Date(System.currentTimeMillis()));
			purchaseRequest.setTipoPr(TipoPr.VBS.getCode());
			purchaseRequest = PurchaseRequestLocalServiceUtil.addPurchaseRequest(purchaseRequest);

			copyWbsFromPRToPositions(purchaseRequest);

			logger.debug("PR No: "+purchaseRequest.getPurchaseRequestId()+" is added");
			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("status","success");
			json.put("purchaseRequestID",purchaseRequest.getPurchaseRequestId());//print purchaseRequest ID

			logger.debug(" Purchase Request "+json);
			writer.print(json);
		}
		catch(Exception e)
		{
			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("status","failure");
			logger.debug(" Purchase Request "+json);
			logger.error("Error in add purchase Request ", e);
			writer.print(json);
		}
	}

	private void updatePurchaseRequest(ResourceRequest request, ResourceResponse response) throws PortletException, IOException{ 
		logger.debug("inside updatePurchaseRequest ");
		PurchaseRequest purchaseRequest=purchaseRequestFromRequest(request);

		PrintWriter writer = response.getWriter();
		try{

			PurchaseRequest existingPR=PurchaseRequestLocalServiceUtil.getPurchaseRequest(purchaseRequest.getPurchaseRequestId());
			purchaseRequest.setStatus(existingPR.getStatus());
			purchaseRequest.setCreatedDate(existingPR.getCreatedDate());
			purchaseRequest.setCreatedUserId(existingPR.getCreatedUserId());
			purchaseRequest.setScreenNameRequester(existingPR.getScreenNameRequester());
			purchaseRequest.setAutomatic(existingPR.getAutomatic());
			purchaseRequest.setFiscalYear(existingPR.getFiscalYear());
			purchaseRequest.setReceiverUserId(existingPR.getReceiverUserId());
			purchaseRequest.setScreenNameReciever(existingPR.getScreenNameReciever());
			purchaseRequest.setRejectionCause(existingPR.getRejectionCause());
			purchaseRequest.setTipoPr(TipoPr.VBS.getCode());

			purchaseRequest = PurchaseRequestLocalServiceUtil.updatePurchaseRequest(purchaseRequest);
			logger.debug("PR No: "+purchaseRequest.getPurchaseRequestId()+" is updated");

			copyWbsFromPRToPositions(purchaseRequest);

			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("status","success");
			json.put("purchaseRequestID",purchaseRequest.getPurchaseRequestId());
			logger.debug(" Purchase Request "+json);
			writer.print(json);
		}catch(Exception e){
			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("status","failure");
			logger.debug(" Purchase Request "+json);
			logger.error("Error in update purchase Request ", e);
			writer.print(json);
		}


	}

	public void deletePurchaseRequest(ActionRequest request, ActionResponse response)
			throws Exception {
		logger.debug(" inside deletePurchaseRequest");
		PurchaseRequest purchaseRequest=purchaseRequestFromRequest(request);

		try{
			Set<String> purchasOrderSet=new HashSet<String>();
			List<Position> positions=new ArrayList<Position>();
			positions=PositionLocalServiceUtil.getPurchaseRequestPositions(String.valueOf(purchaseRequest.getPurchaseRequestId()));

			for(Position position :positions){
				purchasOrderSet.add(position.getPurchaseOrderId());
			}
			if(purchasOrderSet.size()>1){ // can't delete cause the positions in different POs
				//you can't delete
				logger.debug("you can't delete cause your positions is distributed between"+purchasOrderSet.size()+"  POs");
			}
			else{ //start delete
				logger.debug("you can delete");
				List<GoodReceipt> goodReceiptList=new ArrayList<GoodReceipt>();
				for(Position position :positions){
					goodReceiptList=GoodReceiptLocalServiceUtil.getPositionGoodReceipts(String.valueOf(position.getPositionId()));
					for(GoodReceipt goodReceipt : goodReceiptList ){
						GoodReceiptLocalServiceUtil.deleteGoodReceipt(goodReceipt); //delete GR
						logger.debug("GR number "+goodReceipt.getGoodReceiptId()+" deleted");
					}
					PositionLocalServiceUtil.deletePosition(position); //delete position
					logger.debug("position number "+position.getPositionId()+" deleted");
				}
				for(String po : purchasOrderSet){
					if(!"".equals(po)&&po!=null){
						PurchaseOrderLocalServiceUtil.deletePurchaseOrder(Long.parseLong(po));//delete PO
						logger.debug("PO number "+po+" deleted");
					}
				}
				PurchaseRequestLocalServiceUtil.deletePurchaseRequest(purchaseRequest.getPurchaseRequestId());  //delete PR
				logger.debug("PR number "+purchaseRequest.getPurchaseRequestId()+" deleted");
				UploadDownloadHelper.deletePRFileDirectory(purchaseRequest.getPurchaseRequestId());
			}
			String redirect = ParamUtil.getString(request,"redirect");

			sendRedirect(request, response);
		}catch(Exception e){
			logger.error("Error in delete purchase Request ", e);
			SessionErrors.add(request, "error-deleting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaserequestvbs/view.jsp");
		}
	}


	public void submitPurchaseRequest(ActionRequest request, ActionResponse response)
			throws Exception {
		try{
			logger.debug("inside submitPurchaseRequest");
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			PurchaseRequest purchaseRequest=purchaseRequestFromRequest(request);
			long pendingApproval=PositionLocalServiceUtil.getIAndCPositionsCount(String.valueOf(purchaseRequest.getPurchaseRequestId()));
			logger.debug("pendingApproval count= "+pendingApproval);		
			if(purchaseRequest.getPurchaseRequestId() > 0){
				logger.debug("inside update while submit");
				logger.debug("purchase request id = "+purchaseRequest.getPurchaseRequestId());
				PurchaseRequest existingPR=PurchaseRequestLocalServiceUtil.getPurchaseRequest(purchaseRequest.getPurchaseRequestId());
				logger.debug("existing PR  id = "+existingPR.getPurchaseRequestId() );
				purchaseRequest.setCreatedDate(existingPR.getCreatedDate());
				purchaseRequest.setCreatedUserId(existingPR.getCreatedUserId());
				purchaseRequest.setScreenNameRequester(existingPR.getScreenNameRequester());
				purchaseRequest.setAutomatic(existingPR.getAutomatic());
				purchaseRequest.setFiscalYear(existingPR.getFiscalYear());
				purchaseRequest.setRejectionCause(existingPR.getRejectionCause());

				if(pendingApproval==0) {
					purchaseRequest.setStatus(PurchaseRequestStatus.SUBMITTED.getCode());
				}else {
					purchaseRequest.setStatus(PurchaseRequestStatus.PENDING_IC_APPROVAL.getCode());
				}

				logger.debug("existingPR status"+existingPR.getStatus());
				purchaseRequest.setTipoPr(TipoPr.VBS.getCode());
				PurchaseRequestLocalServiceUtil.updatePurchaseRequest(purchaseRequest,true);
				logger.debug("PR is updated and submitted");
				logger.debug("new PR status"+purchaseRequest.getStatus());
			}else{
				logger.debug("inside insert while submit");
				purchaseRequest.setCreatedUserId(themeDisplay.getUserId());
				purchaseRequest.setCreatedDate(new Date(System.currentTimeMillis()));

				if(pendingApproval==0) {
					purchaseRequest.setStatus(PurchaseRequestStatus.SUBMITTED.getCode());
				}else {
					purchaseRequest.setStatus(PurchaseRequestStatus.PENDING_IC_APPROVAL.getCode());
				}
				purchaseRequest.setTipoPr(TipoPr.VBS.getCode());
				purchaseRequest=PurchaseRequestLocalServiceUtil.addPurchaseRequest(purchaseRequest);
				logger.debug("PR is inserted and submitted");
			}
			copyWbsFromPRToPositions(purchaseRequest);

			sendRedirect(request, response);

		}catch(Exception e){
			logger.error("Error in submit purchase Request ", e);
			SessionErrors.add(request, "error-submitting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaserequestvbs/view.jsp");
		}


	}

	public void startPurchaseRequest(ActionRequest request, ActionResponse response)
			throws Exception {
		try{
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			PurchaseRequest purchaseRequest = purchaseRequestFromRequest(request);
			logger.debug("inside sartPurchaseRequest");		
			if(purchaseRequest.getPurchaseRequestId() > 0){
				purchaseRequest = PurchaseRequestLocalServiceUtil.getPurchaseRequest(purchaseRequest.getPurchaseRequestId());
				User user = themeDisplay.getUser();
				purchaseRequest.setReceiverUserId(String.valueOf(user.getUserId()));
				purchaseRequest.setScreenNameReciever(user.getScreenName());
				//purchaseRequest.setStatus(PurchaseRequestStatus.PR_ASSIGNED.getCode());
				purchaseRequest.setStatus(PurchaseRequestStatus.ASSET_RECEIVED.getCode());
				purchaseRequest.setTipoPr(TipoPr.VBS.getCode());

				purchaseRequest=PurchaseRequestLocalServiceUtil.updatePurchaseRequest(purchaseRequest);
				logger.debug("PR no "+purchaseRequest.getPurchaseRequestId() + " is passed on Asset Received");
			}
			sendRedirect(request, response);

		}catch(Exception e){
			logger.error("Error in assign purchase Request ", e);
			SessionErrors.add(request, "error-submitting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaserequestvbs/view.jsp");
		}


	}



	private void addPosition(ResourceRequest request, ResourceResponse response) throws PortletException, IOException{

		logger.debug("inside add Position");
		String jspPage = "/html/purchaserequestvbs/positionlist.jsp";
		String actionStatus=ParamUtil.getString(request, "actionStatus");
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		logger.debug("actionStatus = "+actionStatus);
		PrintWriter writer = response.getWriter();
		Position position=positionFromRequest(request);
		try{
			position.setCreatedUserId(themeDisplay.getUserId());
			position.setCreatedDate(new Date(System.currentTimeMillis()));
			position=PositionLocalServiceUtil.addPosition(position);
			logger.debug("Position no "+position.getPositionId()+" added successfuly");
		}
		catch(Exception e){
			logger.error("Error in add new position ", e);
		}
		try{
			String userType =OmpHelper.getUserRoles(themeDisplay.getUserId());
			List<Position> positions=new ArrayList<Position>();
			int positionStatus=PurchaseRequestPortletHelper.getPositionStatusfromActionStatus(actionStatus);
			logger.debug("positionStatus "+positionStatus);
			logger.debug("position.getPurchaseRequestId"+position.getPurchaseRequestId());
			logger.debug("position.getPurchaseRequestId trimeed"+Long.parseLong(position.getPurchaseRequestId().trim()));
			double prTotalValue=0;
			PurchaseRequest pr=PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.parseLong(position.getPurchaseRequestId().trim()));
			if(pr.getTotalValue()!=null && ! "".equals(pr.getTotalValue())) {
				prTotalValue=Double.valueOf(pr.getTotalValue());
			}

			positions=PositionLocalServiceUtil.getPositionsByPrId(Long.parseLong(position.getPurchaseRequestId().trim()));
			PurchaseRequestPortletHelper.AssignPositionsEditBtton(actionStatus,userType,positions);
			logger.debug("retieved all positions :positions count is "+positions.size());
			logger.debug("retieved all positions :positions count is "+positions.size());
			request.setAttribute("positions", positions);
			request.setAttribute("numberOfPositions", positions.size());
			request.setAttribute("prTotalValue", OmpFormatterUtil.formatNumberByLocale(prTotalValue,Locale.ITALY));
			getPortletContext().getRequestDispatcher(jspPage).include(request, response);
			//writer.write(createPositionsJson(positions,prTotalValue));
		}
		catch(Exception e){
			logger.error("Error in listing the positions in add new position ", e);
		}


	}

	private void updatePosition(ResourceRequest request, ResourceResponse response) throws PortletException, IOException{
		logger.debug("inside update Position");
		String jspPage = "/html/purchaserequestvbs/positionlist.jsp";
		String actionStatus=ParamUtil.getString(request, "actionStatus");
		logger.debug("actionStatus = "+actionStatus);
		PrintWriter writer = response.getWriter();
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		try{
			String userType = OmpHelper.getUserRoles(themeDisplay.getUserId());
			Position position = positionFromRequest(request);
			Position oldPosition = PositionLocalServiceUtil.getPosition(position.getPositionId());
			position.setCreatedDate(oldPosition.getCreatedDate());
			position.setCreatedUserId(oldPosition.getCreatedUserId());
			position.setApprover(oldPosition.getApprover());
			position.setApproved(oldPosition.getApproved());
			position.setPurchaseOrderId(oldPosition.getPurchaseOrderId());
			position.setPoLabelNumber(oldPosition.getPoLabelNumber());
			PositionLocalServiceUtil.updatePosition(position);
			logger.debug("Position no " + position.getPositionId() + " updated successfuly");
			List<Position> positions = new ArrayList<Position>();
			int positionStatus = PurchaseRequestPortletHelper.getPositionStatusfromActionStatus(actionStatus);
			double prTotalValue = 0;
			PurchaseRequest pr = PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.parseLong(position.getPurchaseRequestId().trim()));
			if(pr.getTotalValue() != null && ! "".equals(pr.getTotalValue())) {
				prTotalValue = Double.valueOf(pr.getTotalValue());
			}
			positions = PositionLocalServiceUtil.getPositionsByPrId(Long.parseLong(position.getPurchaseRequestId()));
			PurchaseRequestPortletHelper.AssignPositionsEditBtton(actionStatus,userType,positions);
			logger.debug("retieved all positions :positions count is " + positions.size());
			request.setAttribute("positions", positions);
			request.setAttribute("numberOfPositions", positions.size());
			request.setAttribute("prTotalValue", OmpFormatterUtil.formatNumberByLocale(prTotalValue,Locale.ITALY));
			getPortletContext().getRequestDispatcher(jspPage).include(request, response);
			//.writer.write(createPositionsJson(positions,prTotalValue));
		}
		catch(Exception e){
			logger.error("Error in update position ", e);
		}

	}

	private void approvePosition(ResourceRequest request, ResourceResponse response) throws PortletException, IOException{
		logger.debug("inside approvePosition Position");
		String jspPage = "/html/purchaserequestvbs/positionlist.jsp";
		String actionStatus=ParamUtil.getString(request, "actionStatus");
		logger.debug("actionStatus = "+actionStatus);
		PrintWriter writer = response.getWriter();
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		try{
			String userType = OmpHelper.getUserRoles(themeDisplay.getUserId());
			Position position = PositionLocalServiceUtil.getPosition(positionFromRequest(request).getPositionId());
			position.setApproved(true);
			position.setApprover(String.valueOf(themeDisplay.getUserId()));
			PositionLocalServiceUtil.updatePosition(position,true);

			PurchaseRequest pr=PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.parseLong(position.getPurchaseRequestId().trim()));
			List<Position> positions=new ArrayList<Position>();
			List<Position> remainingPositions=new ArrayList<Position>();
			double prTotalValue=0;
			if(pr.getTotalValue()!=null && ! "".equals(pr.getTotalValue())) {
				prTotalValue=Double.valueOf(pr.getTotalValue());
			}

			if(userType.equals(OmpRoles.OMP_APPROVER_VBS.getLabel())) {
				positions = PositionLocalServiceUtil.getPositionsByIAndCType(pr.getPurchaseRequestId(), IAndCType.ICSERVIZI.getLabel());
				remainingPositions = PositionLocalServiceUtil.getPositionsByIAndCTypeAndApprovalStatus
						(pr.getPurchaseRequestId(),  IAndCType.ICSERVIZI.getLabel(), false);
				if(remainingPositions.size() ==0 || remainingPositions.get(0) == null) {
					request.setAttribute("hideReject", "true");
				}
			}else if(userType.equals(OmpRoles.OMP_CONTROLLER_VBS.getLabel())){
				positions = PositionLocalServiceUtil.getPositionsByPrId(pr.getPurchaseRequestId());
			}
			
			PurchaseRequestPortletHelper.AssignPositionsEditBtton(actionStatus,userType,positions);
			logger.debug("retieved all positions :positions count is "+positions.size());
			request.setAttribute("positions", positions);
			request.setAttribute("numberOfPositions", positions.size());
			request.setAttribute("prTotalValue", OmpFormatterUtil.formatNumberByLocale(prTotalValue,Locale.ITALY));
			getPortletContext().getRequestDispatcher(jspPage).include(request, response);
			//writer.write(createPositionsJson(positions,prTotalValue));
		}
		catch(Exception e){
			logger.error("Error in update position ", e);
		}

	}


	private void deletePosition(ResourceRequest request, ResourceResponse response) throws PortletException, IOException{
		logger.debug("inside delete position");
		String jspPage = "/html/purchaserequestvbs/positionlist.jsp";
		String actionStatus=ParamUtil.getString(request, "actionStatus");
		logger.debug("actionStatus = "+actionStatus);
		PrintWriter writer = response.getWriter();
		List<GoodReceipt> goodReceiptList=new ArrayList<GoodReceipt>();
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		try{
			String userType =OmpHelper.getUserRoles(themeDisplay.getUserId());
			Position position=positionFromRequest(request);
			goodReceiptList=GoodReceiptLocalServiceUtil.getPositionGoodReceipts(String.valueOf(position.getPositionId()));
			for(GoodReceipt goodReceipt : goodReceiptList ){
				GoodReceiptLocalServiceUtil.deleteGoodReceipt(goodReceipt); //delete GR
				logger.debug("GR number "+goodReceipt.getGoodReceiptId()+" deleted");
			}
			PositionLocalServiceUtil.deletePosition(position);
			logger.debug("Position no "+position.getPositionId()+" deleted successfuly");

			double prTotalValue=0;
			PurchaseRequest pr=PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.parseLong(position.getPurchaseRequestId().trim()));
			if(pr.getTotalValue()!=null && ! "".equals(pr.getTotalValue())) {
				prTotalValue=Double.valueOf(pr.getTotalValue());
			}
			List<Position> positions=new ArrayList<Position>();
			int positionStatus=PurchaseRequestPortletHelper.getPositionStatusfromActionStatus(actionStatus); 
			positions=PositionLocalServiceUtil.getPositionsByPrId(Long.parseLong(position.getPurchaseRequestId()));
			PurchaseRequestPortletHelper.AssignPositionsEditBtton(actionStatus,userType,positions);
			logger.debug("retieved all positions :positions count is "+positions.size());
			request.setAttribute("positions", positions);
			request.setAttribute("numberOfPositions", positions.size());
			request.setAttribute("prTotalValue", OmpFormatterUtil.formatNumberByLocale(prTotalValue,Locale.ITALY));
			getPortletContext().getRequestDispatcher(jspPage).include(request, response);
			//writer.write(createPositionsJson(positions,prTotalValue));
		}
		catch(Exception e){
			logger.error("Error in deleting position ", e);
		}
	}


	public void uploadFile(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		logger.debug("inside upload File");
		long purchaseRequestId = ParamUtil.getLong(request, "purchaseRequestId");
		boolean automatic = ParamUtil.getBoolean(request, "automatic");
		boolean multipleUpload;
		try {
			logger.debug("file to view is " + purchaseRequestId);
			if(automatic) {
				multipleUpload = false;
			}else {
				multipleUpload = true;
			}
			UploadDownloadHelper.uploadFile(purchaseRequestId, multipleUpload,  request);
			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("name","success");
			json.put("id",purchaseRequestId);

			logger.debug(" Purchase Request "+json);
			response.getWriter().print(json);
		} catch (UploadException ue) {
			logger.error(ue.getMessage());
			response.getWriter().write("failed");

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
	}

	public void downloadFile(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		logger.debug("inside download File");
		long purchaseRequestId = ParamUtil.getLong(request, "purchaseRequestId");
		String filename=ParamUtil.getString(request, "fileName");
		try {
			logger.debug("file to view is " + purchaseRequestId+"/"+filename);
			// Download action
			UploadDownloadHelper.downloadFileByName(purchaseRequestId,filename,response);
		} catch (UploadException ue) {
			logger.error(ue.getMessage());
			response.getWriter().write("failed");

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
	}

	public void downloadAllFiles(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		logger.debug("inside download All Files");
		long purchaseRequestId = ParamUtil.getLong(request, "purchaseRequestId");
		try {
			logger.debug("Zip file to view is " + purchaseRequestId);
			// Download action
			UploadDownloadHelper.downloadZipFile(purchaseRequestId,response);
		} catch (UploadException ue) {
			logger.error(ue.getMessage());
			response.getWriter().write("failed");

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
	}
	public void deleteFile(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		logger.debug("inside delete File");
		long purchaseRequestId = ParamUtil.getLong(request, "purchaseRequestId");
		String filename=ParamUtil.getString(request, "fileName");
		try {
			logger.debug("file to delete is " + purchaseRequestId+"/"+filename);
			// Download action
			UploadDownloadHelper.deleteFileByName(purchaseRequestId,filename);
			listUploadedFiles(request, response);
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
	}

	public void getLabelNumber(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		logger.debug("inside getLabelNumber");
		long purchaseRequestId = ParamUtil.getLong(request, "purchaseRequestId");
		try {
			long newMaxLabel=0;
			long maxLabel=0;
			List<Long> maxLabelList=PositionLocalServiceUtil.getPositionsMaxLabelNumber(purchaseRequestId);
			if(maxLabelList.size() >0 && maxLabelList.get(0)!=null) {
				maxLabel=maxLabelList.get(0);
				logger.debug("maxLabel Number="+maxLabel);
			}
			newMaxLabel=maxLabel+10;
			logger.debug("new maxLabel Number="+newMaxLabel);
			response.getWriter().write(String.valueOf(newMaxLabel));
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
	}

	public void listUploadedFiles(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		logger.debug("inside listUploadedFiles");
		long purchaseRequestId = ParamUtil.getLong(request, "purchaseRequestId");
		String jspPage = "/html/purchaserequestvbs/filelist.jsp";
		try {
			logger.debug("list files for PR ID " + purchaseRequestId);
			List<String> uploadedFiles = new ArrayList<String>();
			uploadedFiles=UploadDownloadHelper.getPRUploadedFilesNames(purchaseRequestId);
			request.setAttribute("uploadedFiles", uploadedFiles);
			getPortletContext().getRequestDispatcher(jspPage).include(request, response);
		}catch (Exception e) {
			logger.error("error while retrieving the files list",e);
		}
	}


	private String createPositionsJson(List<Position> positions,double prTotalValue){

		String jsonPositions="[";
		for(int i = 0; i < positions.size();i++) {
			Position po = positions.get(i);
			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("positionId", po.getPositionId());
			json.put("positionStatus", po.getPositionStatus().getLabel());
			json.put("labelNumber", po.getLabelNumber());
			json.put("fiscalyear", po.getFiscalYear());
			json.put("categorycode", po.getCategoryCode());
			json.put("offernumber", po.getOfferNumber());
			json.put("offerdate", DateToString(po.getOfferDate()));
			json.put("quantity", po.getQuatity());
			json.put("deliverydate", DateToString(po.getDeliveryDate()));
			json.put("deliveryadress", po.getDeliveryAddress());
			//json.put("wbscode", po.getWbsCodeId());
			json.put("locationEvo", po.getEvoLocation());
			json.put("assetnumber", po.getAssetNumber());
			json.put("prcsnumber", po.getShoppingCart());
			json.put("ponumber", po.getPoNumber());
			json.put("actualunitcost", po.getActualUnitCost());
			json.put("positiondescription", po.getDescription());
			json.put("positionEditBtton", String.valueOf(po.getPositionEditBtton()));

			json.put("iandc",po.getIcApproval());
			//json.put("trackingCode",po.getTrackingCode());

			//CR5.1
			json.put("targaTecnica", po.getTargaTecnica());

			json.put("prTotalValue",prTotalValue);

			jsonPositions+=json.toString();
			if(i != (positions.size() -1))
				jsonPositions+=",";
		}

		jsonPositions+="]";
		logger.debug("positions Json= "+jsonPositions);
		return jsonPositions;

	}

	private PurchaseRequest purchaseRequestFromRequest(PortletRequest request) {


		PurchaseRequest purchaseRequest=new PurchaseRequestImpl();

		purchaseRequest.setPurchaseRequestId(ParamUtil.getLong(request,"purchaseRequestId"));
		try {
			PurchaseRequest oldPurchaseRequest = PurchaseRequestLocalServiceUtil.getPurchaseRequest(purchaseRequest.getPurchaseRequestId());
			if(oldPurchaseRequest !=null) {
				purchaseRequest.setTotalValue(oldPurchaseRequest.getTotalValue());
			}
		} catch (Exception e) {

		}
		purchaseRequest.setCostCenterId(ParamUtil.getString(request,"costcenter"));
		purchaseRequest.setOla(ParamUtil.getString(request,"ola"));
		purchaseRequest.setBuyerId(ParamUtil.getString(request,"buyer"));
		purchaseRequest.setVendorId(ParamUtil.getString(request,"vendor"));
		String projectId = ParamUtil.getString(request,"projectname");

		purchaseRequest.setProjectId(projectId);

		purchaseRequest.setSubProjectId(ParamUtil.getString(request,"subproject"));
		purchaseRequest.setBudgetCategoryId(ParamUtil.getString(request,"budgetCategory"));
		purchaseRequest.setBudgetSubCategoryId(ParamUtil.getString(request,"budgetSubCategory"));
		purchaseRequest.setMacroDriverId(ParamUtil.getString(request,"macrodriver"));
		purchaseRequest.setMicroDriverId(ParamUtil.getString(request,"microdriver"));
		purchaseRequest.setOdnpCodeId(ParamUtil.getString(request,"odnpcode"));
		purchaseRequest.setOdnpNameId(ParamUtil.getString(request,"odnpname"));

		purchaseRequest.setCurrency(ParamUtil.getString(request,"prCurrency"));
		purchaseRequest.setActivityDescription(ParamUtil.getString(request,"activitydescription"));
		purchaseRequest.setAutomatic(ParamUtil.getBoolean(request,"automatic"));

		//CR for tracking code
		purchaseRequest.setTrackingCode(ParamUtil.getString(request,"trackingcode"));
		purchaseRequest.setWbsCodeId(ParamUtil.getString(request,"prwbscode"));
		
		purchaseRequest.setFiscalYear(ParamUtil.getString(request, "fiscalyear"));

		logger.debug("purchaseRequest From Request is " + purchaseRequest.toString());
		return purchaseRequest;

	}

	private Position positionFromRequest(PortletRequest request) {

		Position position=new PositionImpl();
		position.setPositionId(ParamUtil.getLong(request, "positionId"));
		position.setLabelNumber(ParamUtil.getLong(request, "labelNumber"));
		position.setPurchaseRequestId(ParamUtil.getString(request, "purchaseRequestId"));
		position.setFiscalYear(ParamUtil.getString(request, "fiscalyear"));
		position.setCategoryCode(ParamUtil.getString(request, "categorycode"));
		position.setOfferNumber(ParamUtil.getString(request, "offernumber"));
		position.setOfferDate(stringToDate(ParamUtil.getString(request,"offerdate")));

		String quatity=ParamUtil.getString(request, "quantity");
		if(quatity !=null) {
			quatity=quatity.replace(",", ".");
		}
		position.setQuatity(quatity);
		position.setDeliveryDate(stringToDate(ParamUtil.getString(request, "deliverydate")));
		position.setDeliveryAddress(ParamUtil.getString(request, "deliveryadress"));
		position.setAssetNumber(ParamUtil.getString(request, "asset-number"));
		position.setShoppingCart(ParamUtil.getString(request, "prcs-number"));
		position.setPoNumber(ParamUtil.getString(request, "po-number"));

		String actualUnitCost=ParamUtil.getString(request, "actual-unit-cost");
		if(actualUnitCost !=null) {
			actualUnitCost=actualUnitCost.replace(",", ".");
		}
		position.setActualUnitCost(actualUnitCost);
		position.setDescription(ParamUtil.getString(request, "position-description"));

		//CR5.1
		position.setTargaTecnica(ParamUtil.getString(request, "targaTecnica"));

		position.setIcApproval(ParamUtil.getString(request, "iandc"));
		//position.setTrackingCode(ParamUtil.getString(request, "trackingCode"));

		logger.debug("position From Request is " + position.toString());
		return position;

	}

	public long calculatePrTotalValue(List<Position> positions) {
		long totalValue=0;
		try {
			for(Position position:positions)
			{
				if(position.getActualUnitCost()!=null && ! "".equals(position.getActualUnitCost())
						&&position.getQuatity()!=null && !"".equals(position.getQuatity()))
					totalValue=totalValue+(Integer.parseInt(position.getActualUnitCost())*Integer.parseInt(position.getQuatity()));
			}
		}catch(Exception e) {
			logger.error("you have unitcost or quantity is not number");
			return 0;
		}

		return totalValue;
	}

	private Date stringToDate(String date){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy"); 
		try {
			return df.parse(date);
		} catch (ParseException e) {
			return null;
		}
	}

	private String DateToString(Date date){
		try{
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy"); 
			return df.format(date);
		}catch(Exception e){
			return "";
		}
	}


	private void getMicroDrivers(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws SystemException, IOException {

		try {
			JSONObject optionsJSON = JSONFactoryUtil.createJSONObject();
			Long macroId = ParamUtil.getLong(resourceRequest, "macroId");
			Long microDriverId = ParamUtil.getLong(resourceRequest, "microDriverId");

			List<MicroDriver> microDriversList= null;
			if(macroId !=null && macroId !=0) {
				MacroDriver macroDriver=MacroDriverLocalServiceUtil.getMacroDriver(macroId);
				if(macroDriver.getDisplay()) {
					microDriversList = MicroDriverLocalServiceUtil.getMicroDriverByMacroDriverId(macroId);
				}else {
					if(microDriverId !=null && microDriverId !=0) {
						MicroDriver microDriver=MicroDriverLocalServiceUtil.getMicroDriver(microDriverId);
						microDriversList=new ArrayList<MicroDriver>();
						microDriversList.add(microDriver);
					}
				}
			}

			if(microDriversList == null){
				microDriversList = new ArrayList<MicroDriver>();
			}
			JSONArray jsonmicroDriverArray = JSONFactoryUtil.createJSONArray();
			for (MicroDriver microDriver : microDriversList) {
				JSONObject json = JSONFactoryUtil.createJSONObject();
				json.put("id", microDriver.getMicroDriverId());
				json.put("name", microDriver.getMicroDriverName());
				json.put("display", microDriver.getDisplay());

				jsonmicroDriverArray.put(json);
			}
			optionsJSON.put("microDrivers", jsonmicroDriverArray);
			PrintWriter writer = resourceResponse.getWriter();
			writer.write(optionsJSON.toString());
		}catch(Exception e) {
			logger.error("error while getMicroDrivers Ajax", e);
		}
	}

	private void getTrackingCodeValues(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws SystemException, IOException,PortalException {
		JSONObject optionsJSON = JSONFactoryUtil.createJSONObject();
		String selectedTrackingCode = ParamUtil.getString(resourceRequest, "selectedTrackingCode");
		logger.info("selected tracking code is {}",selectedTrackingCode);
		if(selectedTrackingCode.contains("||")) {

			selectedTrackingCode = selectedTrackingCode.trim().split("\\|\\|")[0].trim();
			logger.info("||||splitted tracking code {}",selectedTrackingCode);
		}
		TrackingCodes trackingCodesValues= TrackingCodesLocalServiceUtil.getTrackingCodeByName(selectedTrackingCode);
		//optionsJSON.put("trackingCodesValues", JSONFactoryUtil.serialize(trackingCodesValues));
		if(trackingCodesValues != null) {
			logger.info("tracking code not equal null");
			optionsJSON.put("budgetCategoryId",trackingCodesValues.getBudgetCategoryId());
			optionsJSON.put("budgetSubCategoryId",trackingCodesValues.getBudgetSubCategoryId());
			optionsJSON.put("macroDriverId",trackingCodesValues.getMacroDriverId());
			optionsJSON.put("microDriverId",trackingCodesValues.getMicroDriverId());
			optionsJSON.put("odnpNameId",trackingCodesValues.getOdnpNameId());
			optionsJSON.put("projectId",trackingCodesValues.getProjectId());
			optionsJSON.put("subProjectId",trackingCodesValues.getSubProjectId());
			optionsJSON.put("wbsCodeId",trackingCodesValues.getWbsCodeId());
		} else {
			logger.info("tracking code is null");
			optionsJSON.put("empty","true");
		}


		PrintWriter writer = resourceResponse.getWriter();
		writer.write(optionsJSON.toString());
	}



	public void rejectPurchaseRequest(ActionRequest request, ActionResponse response)
			throws Exception {
		try{
			logger.debug("rejectPurchaseRequest()");
			long prId = ParamUtil.getLong(request,"purchaseRequestId");
			logger.debug("Rejected Purchase Request Id "+ prId);
			String rejCause = ParamUtil.getString(request,"rejectionCause");
			logger.debug("Rejection Cause "+rejCause);

			PurchaseRequest purchaseRequest=PurchaseRequestLocalServiceUtil.getPurchaseRequest(prId);
			if(purchaseRequest.getPurchaseRequestId() > 0){
				purchaseRequest.setStatus(PurchaseRequestStatus.REJECTED.getCode());
				purchaseRequest.setRejectionCause(rejCause);
				purchaseRequest.setTipoPr(TipoPr.VBS.getCode());
				purchaseRequest = PurchaseRequestLocalServiceUtil.updatePurchaseRequest(purchaseRequest);
				logger.debug("PR no "+purchaseRequest.getPurchaseRequestId()+"has been rejected");

				List<Position> iAndCPositions=PositionLocalServiceUtil.getIAndCPositions(purchaseRequest.getPurchaseRequestId());
				if(iAndCPositions.size() >0 &&iAndCPositions.get(0) !=null) {
					for(Position pos :iAndCPositions) {
						pos.setApproved(false);
						PositionLocalServiceUtil.updatePositionWithoutHocks(pos, true);
					}
				}
			}
			sendRedirect(request, response);

		}catch(Exception e){
			logger.error("Error in assign purchase Request ", e);
			SessionErrors.add(request, "error-submitting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaserequestvbs/view.jsp");
		}
	}

	public void copyWbsFromPRToPositions(PurchaseRequest pr) throws SystemException{
		logger.debug("inside copyWbsFromPRToPositions");
		logger.debug("pr wbs code ="+pr.getWbsCodeId());
		if(pr.getWbsCodeId()!=null || ! "".equals(pr.getWbsCodeId())) {
			List<Position> positions= PositionLocalServiceUtil.getPositionsByPrId(pr.getPurchaseRequestId());
			if(positions !=null) {
				for(Position position :positions) {
					position.setWbsCodeId(pr.getWbsCodeId());
					PositionLocalServiceUtil.updatePosition(position,true);
				}
			}
		}
	}



}
