package com.hp.omp.service.persistence;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.hp.omp.model.Buyer;
import com.hp.omp.model.impl.BuyerImpl;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class BuyerFinderImpl extends BuyerPersistenceImpl implements BuyerFinder{

	private static Log _log = LogFactoryUtil.getLog(BuyerFinderImpl.class);
	static final String GET_BUYER_LIST = BuyerFinderImpl.class.getName()+".getBuyersList";
	static final String GET_BUYER_COUNT_LIST = BuyerFinderImpl.class.getName()+".getBuyersCountList";
	
	@SuppressWarnings("unchecked")
	public List<Buyer> getBuyersList(int start , 
			int end, 
			String searchFilter,
			int gruppoUsers) throws SystemException {
		
		Session session = null;
		List<Buyer> list = new ArrayList<Buyer>();
		try {
		session  = openSession();
		
		String sql = null;
		
		if(StringUtils.isEmpty(searchFilter))
			searchFilter = "%%";
		
		sql = CustomSQLUtil.get(GET_BUYER_LIST);
		SQLQuery query = session.createSQLQuery(sql);
		
		if(query == null){
			_log.error("get buyer error, no sql found");
			return null;
		}
		
		query.setString(0, searchFilter);
		query.setInteger(1, gruppoUsers);
		
		query.addEntity("Buyer", BuyerImpl.class);
		list = (List<Buyer>)QueryUtil.list(query, getDialect(),  start, end);
		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
		return list;
	}
	
	public int getBuyersCountList(String searchFilter, int gruppoUsers) throws SystemException {
		Session session = null; 
		try {
			session = openSession();
			
			String sql = null;
			
			if(StringUtils.isEmpty(searchFilter))
				searchFilter = "%%";
			
			sql = CustomSQLUtil.get(GET_BUYER_COUNT_LIST);
			SQLQuery query = session.createSQLQuery(sql);
			
			if(query == null){
				_log.error("get Buyer count list error, no sql found");
				return 0;
			}
			
			query.setString(0, searchFilter);
			query.setInteger(1, gruppoUsers);
			
			query.addScalar("COUNT_VALUE", Type.INTEGER);

			@SuppressWarnings("unchecked")
			List<Integer> list = query.list();
			if(list != null && list.size() > 0) {
				return list.get(0);
			}
			return 0;
		} finally {
			if(session != null) {
				closeSession(session);
			}
		}
	}
}
