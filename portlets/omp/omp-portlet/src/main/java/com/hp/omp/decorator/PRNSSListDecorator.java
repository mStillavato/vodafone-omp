package com.hp.omp.decorator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;

import javax.portlet.PortletModeException;
import javax.portlet.WindowStateException;

import org.displaytag.decorator.TableDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.model.Position;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.custom.Currency;
import com.hp.omp.model.custom.PrListDTO;
import com.hp.omp.model.custom.PurchaseRequestStatus;
import com.hp.omp.service.PositionLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;

public class PRNSSListDecorator extends TableDecorator implements Serializable {
	
	Logger logger = LoggerFactory.getLogger(PRNSSListDecorator.class);


	
	public String getPurchaseRequestId() throws PortalException, SystemException, WindowStateException, PortletModeException {
		PrListDTO item = (PrListDTO) this.getCurrentRowObject();
        
        logger.info("purchaseRequestID: "+ item.getPurchaseRequestId());
        
        return createURL(item, item.getPurchaseRequestId()).toString();
    }
	
	private StringBuilder createURL(PrListDTO item,Object value) {
		StringBuilder sb = new StringBuilder();
        sb.append("<a HREF=\"");
        sb.append("/group/vodafone/purchase-request?automatic=");
        sb.append(item.isAutomatic());
        sb.append("&purchaseRequestID=").append(item.getPurchaseRequestId());
        sb.append("&actionStatus=").append(item.getStatus());
        sb.append("&redirect=").append("/group/vodafone/home");
        sb.append("\">");
        sb.append(value);
        sb.append("</a>");
		return sb;
	}
}
