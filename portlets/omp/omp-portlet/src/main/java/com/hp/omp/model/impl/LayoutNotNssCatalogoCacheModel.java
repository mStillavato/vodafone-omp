/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.model.impl;

import java.io.Serializable;
import java.util.Date;

import com.hp.omp.model.LayoutNotNssCatalogo;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

/**
 * The cache model class for representing LayoutNotNssCatalogo in entity cache.
 *
 * @author HP Egypt team
 * @see LayoutNotNssCatalogo
 * @generated
 */
public class LayoutNotNssCatalogoCacheModel implements CacheModel<LayoutNotNssCatalogo>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(47);

		sb.append("{layoutNotNssCatalogoId=");
		sb.append(layoutNotNssCatalogoId);
		sb.append(", lineNumber=");
		sb.append(lineNumber);
		sb.append(", materialId=");
		sb.append(materialId);
		sb.append(", materialDesc=");
		sb.append(materialDesc);
		sb.append(", quantity=");
		sb.append(quantity);
		sb.append(", netPrice=");
		sb.append(netPrice);
		sb.append(", currency=");
		sb.append(currency);
		sb.append(", deliveryDate=");
		sb.append(deliveryDate);
		sb.append(", deliveryAddress=");
		sb.append(deliveryAddress);
		sb.append(", itemText=");
		sb.append(itemText);
		sb.append(", locationEvo=");
		sb.append(locationEvo);
		sb.append(", targaTecnica=");
		sb.append(targaTecnica);
		sb.append(", user_=");
		sb.append(user_);
		sb.append(", vendorCode=");
		sb.append(vendorCode);
		sb.append(", catalogoId=");
		sb.append(catalogoId);
        sb.append(", icApproval=");
        sb.append(icApproval);
        sb.append(", categoryCode=");
        sb.append(categoryCode);
        sb.append(", offerNumber=");
        sb.append(offerNumber);
		sb.append("}");

		return sb.toString();
	}

	public LayoutNotNssCatalogo toEntityModel() {
		LayoutNotNssCatalogoImpl layoutNotNssCatalogoImpl = new LayoutNotNssCatalogoImpl();

		layoutNotNssCatalogoImpl.setLayoutNotNssCatalogoId(layoutNotNssCatalogoId);
		layoutNotNssCatalogoImpl.setLineNumber(lineNumber);

		if (materialId == null) {
			layoutNotNssCatalogoImpl.setMaterialId(StringPool.BLANK);
		}
		else {
			layoutNotNssCatalogoImpl.setMaterialId(materialId);
		}
		
		if (materialDesc == null) {
			layoutNotNssCatalogoImpl.setMaterialDesc(StringPool.BLANK);
		}
		else {
			layoutNotNssCatalogoImpl.setMaterialDesc(materialDesc);
		}
		
		if (quantity == null) {
			layoutNotNssCatalogoImpl.setQuantity(StringPool.BLANK);
		}
		else {
			layoutNotNssCatalogoImpl.setQuantity(quantity);
		}
		
		if (netPrice == null) {
			layoutNotNssCatalogoImpl.setNetPrice(StringPool.BLANK);
		}
		else {
			layoutNotNssCatalogoImpl.setNetPrice(netPrice);
		}
		
		if (currency == null) {
			layoutNotNssCatalogoImpl.setCurrency(StringPool.BLANK);
		}
		else {
			layoutNotNssCatalogoImpl.setCurrency(currency);
		}

		if (deliveryDate == Long.MIN_VALUE) {
			layoutNotNssCatalogoImpl.setDeliveryDate(null);
		}
		else {
			layoutNotNssCatalogoImpl.setDeliveryDate(new Date(deliveryDate));
		}

		if (deliveryAddress == null) {
			layoutNotNssCatalogoImpl.setDeliveryAddress(StringPool.BLANK);
		}
		else {
			layoutNotNssCatalogoImpl.setDeliveryAddress(deliveryAddress);
		}

		if (itemText == null) {
			layoutNotNssCatalogoImpl.setItemText(StringPool.BLANK);
		}
		else {
			layoutNotNssCatalogoImpl.setItemText(itemText);
		}

		if (locationEvo == null) {
			layoutNotNssCatalogoImpl.setLocationEvo(StringPool.BLANK);
		}
		else {
			layoutNotNssCatalogoImpl.setLocationEvo(locationEvo);
		}

		if (targaTecnica == null) {
			layoutNotNssCatalogoImpl.setTargaTecnica(StringPool.BLANK);
		}
		else {
			layoutNotNssCatalogoImpl.setTargaTecnica(targaTecnica);
		}

		if (user_ == null) {
			layoutNotNssCatalogoImpl.setUser_(StringPool.BLANK);
		}
		else {
			layoutNotNssCatalogoImpl.setUser_(user_);
		}
		
		if (vendorCode == null) {
			layoutNotNssCatalogoImpl.setVendorCode(StringPool.BLANK);
		}
		else {
			layoutNotNssCatalogoImpl.setVendorCode(vendorCode);
		}
		
		layoutNotNssCatalogoImpl.setCatalogoId(catalogoId);
		
        if (icApproval == null) {
        	layoutNotNssCatalogoImpl.setIcApproval(StringPool.BLANK);
        } else {
        	layoutNotNssCatalogoImpl.setIcApproval(icApproval);
        }
		
        if (categoryCode == null) {
        	layoutNotNssCatalogoImpl.setCategoryCode(StringPool.BLANK);
        } else {
        	layoutNotNssCatalogoImpl.setCategoryCode(categoryCode);
        }

        if (offerNumber == null) {
        	layoutNotNssCatalogoImpl.setOfferNumber(StringPool.BLANK);
        } else {
        	layoutNotNssCatalogoImpl.setOfferNumber(offerNumber);
        }
        

		layoutNotNssCatalogoImpl.resetOriginalValues();

		return layoutNotNssCatalogoImpl;
	}

	public long layoutNotNssCatalogoId;
	public long lineNumber;
	public String materialId;
	public String materialDesc;
	public String quantity;
	public String netPrice;
	public String currency;
	public long deliveryDate;
	public String deliveryAddress;
	public String itemText;
	public String locationEvo;
	public String targaTecnica;
	public String user_;
	public String vendorCode;
	public long catalogoId;
	public String icApproval;
    public String categoryCode;
    public String offerNumber;
}