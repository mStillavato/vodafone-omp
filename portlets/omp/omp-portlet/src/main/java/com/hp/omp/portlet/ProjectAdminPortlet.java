package com.hp.omp.portlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.model.Project;
import com.hp.omp.model.ProjectSubProject;
import com.hp.omp.model.PurchaseOrder;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.custom.GruppoUsers;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.impl.ProjectImpl;
import com.hp.omp.service.ProjectLocalServiceUtil;
import com.hp.omp.service.ProjectSubProjectLocalServiceUtil;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class ProjectAdminPortlet
 */
public class ProjectAdminPortlet extends MVCPortlet {
 
	private transient Logger logger = LoggerFactory.getLogger(ProjectAdminPortlet.class);
	private String errorMSG = "";
	private String successMSG = "";
	private String ACTION_PARAM= "action";
	private String ADD_ACTION= "add";
	private String UPDATE_ACTION= "update";
	private String DELETE_ACTION= "delete";
	private String PAGINATION_ACTION= "pagination";
	private String EXPORT_LIST= "exportList";
	
	

	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		super.doView(renderRequest, renderResponse);
	}
	
	
	/* (non-Javadoc)
	 * @see javax.portlet.GenericPortlet#render(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		viewAddPeojects(request, response);
		super.render(request, response);
	}
	
	public void viewAddPeojects(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		String action = renderRequest.getParameter(ACTION_PARAM);
		
		
		if(ADD_ACTION.equals(action)){
			renderRequest.setAttribute(ACTION_PARAM, ADD_ACTION);
			renderRequest.setAttribute("display",true);
		}else if(UPDATE_ACTION.equals(action)){
			String projectId = renderRequest.getParameter("projectId");
			Project project = getProject(projectId);
			
			logger.debug("render update action for : "+project.getProjectName());
			logger.debug("Display = "+project.getDisplay());
			
			renderRequest.setAttribute(ACTION_PARAM, UPDATE_ACTION);
			renderRequest.setAttribute("projectId", project.getProjectId());
			renderRequest.setAttribute("projectName", project.getProjectName());
			renderRequest.setAttribute("projectDesc", project.getDescription());
			renderRequest.setAttribute("display", project.getDisplay());
		}
	}
	
	@Override
    public void serveResource(ResourceRequest resourceRequest,
                  ResourceResponse resourceResponse) throws IOException,
                  PortletException {
		String action=resourceRequest.getParameter(ACTION_PARAM);
		if(DELETE_ACTION.equals(action)){
			deleteProject(resourceRequest, resourceResponse);
		} else if(PAGINATION_ACTION.equals(action)){
			paginationList(resourceRequest, resourceResponse);
		} else if(EXPORT_LIST.equals(action)){
			exportList(resourceRequest, resourceResponse);
		}
		
		clearMessages();
           super.serveResource(resourceRequest, resourceResponse);
    }

	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#processAction(javax.portlet.ActionRequest, javax.portlet.ActionResponse)
	 */
	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {

		clearMessages();
		String action = actionRequest.getParameter(ACTION_PARAM);
		String projectName = actionRequest.getParameter("projectName");
		String projectDesc = actionRequest.getParameter("projectDesc");
		boolean display=ParamUtil.getBoolean(actionRequest,"display");
		String operation = "";
		
		logger.debug("projectName="+projectName);
		logger.debug("projectDesc="+projectDesc);
		logger.debug("display = "+display);
		
		logger.debug("creating project object ...");
		Project newProject = new ProjectImpl();
		newProject.setProjectName(projectName);
		newProject.setDescription(projectDesc);
		newProject.setDisplay(display);
		logger.debug("project object created !!");
		try {
			if(ADD_ACTION.equals(action)){
				operation = "added";
				logger.debug("adding new project ["+projectName+"] in database ...");
				if(ProjectLocalServiceUtil.getProjectByName(projectName) == null){
					
					ProjectLocalServiceUtil.addProject(newProject);
					
					successMSG = "project name ["+projectName+"] successfully Added";
					
				}else{
					errorMSG = "project name ["+projectName+"] already existing";
				}
			}else if(UPDATE_ACTION.equals(action)){
				operation = "updated";
				String projectId = actionRequest.getParameter("projectId");
				
				newProject.setProjectId(Integer.valueOf(projectId));
				
				if(! (isProjectNameExists(newProject, projectName))){
					
					logger.debug("updating  project ["+projectId+"] in database ...");
					
					
					ProjectLocalServiceUtil.updateProject(newProject);
					successMSG = "project Name ["+projectName+"] successfully Updated";
				}else{
					errorMSG = "project name ["+projectName+"] already existing";
				}
				
			}
			
			
		} catch (SystemException e) {
			errorMSG = "Project ["+projectName+"] not "+operation+".";
			logger.error("error happened while "+operation+" project ["+projectName+"] in database.");
			logger.error(e.getMessage(), e);
		}catch (NumberFormatException nfe) {
			errorMSG = "Project ["+projectName+"] not "+operation+".";
			logger.error("error happened while "+operation+" project ["+projectName+"] in database.");
			logger.error(nfe.getMessage(), nfe);
		}
		
		actionRequest.setAttribute("errorMSG", errorMSG);
		actionRequest.setAttribute("successMSG", successMSG);
		super.processAction(actionRequest, actionResponse);
	}
	
	private void retrieveProjectListToRequest(PortletRequest renderRequest) throws SystemException{
		
		try {
			String searchFilter = ParamUtil.getString(renderRequest, "searchFilter","").toUpperCase();
			
			int totalCount = 0;
			
			if (searchFilter == null || searchFilter.trim().length() == 0)
				totalCount = ProjectLocalServiceUtil.getProjectsCount();
			else
				totalCount = ProjectLocalServiceUtil.getProjectsCountList(searchFilter);

			logger.debug("Project Total Count = " + totalCount);
			
			Integer pageNumber = ParamUtil.getInteger(renderRequest, "pageNumber", 1);
			Integer pageSize = OmpHelper.PAGE_SIZE;
			int start = (pageNumber - 1) * pageSize;
			int end = start + pageSize;

			renderRequest.setAttribute("numberOfPages", getTotalNumberOfPages(totalCount, pageSize));
			renderRequest.setAttribute("pageNumber", pageNumber);


			//List<Project> projectList = ProjectLocalServiceUtil.getProjects(start, end);
			List<Project> projectList = ProjectLocalServiceUtil.getProjectsList(start, end, searchFilter);

			renderRequest.setAttribute("projectList", projectList);
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			errorMSG = "can not retrieve project list";
			renderRequest.setAttribute("errorMSG", errorMSG);
		}

	}
	
	public void deleteProject(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse)
			throws PortletException, IOException {
		clearMessages();
		
		String projectId=resourceRequest.getParameter("projectId");
		String projectName=resourceRequest.getParameter("projectName");
		String action=resourceRequest.getParameter("action");
		String jspPage = "/html/projectadmin/projectlist.jsp";
		
		if(Validator.isBlank(action)){
			errorMSG = "No Action in the request";
		}else if(Validator.isBlank(projectId)){
			errorMSG = "No project Id in the request";
		}else{
			
			try {
				List<PurchaseOrder> purchaseOrderList = PurchaseOrderLocalServiceUtil.getPurchaseOrderByProjectId(projectId);
				List<PurchaseRequest> purchaseRequestList = PurchaseRequestLocalServiceUtil.getPurchaseRequestByProjectId(projectId);
				List<ProjectSubProject> projectSubProjects = ProjectSubProjectLocalServiceUtil.getProjectSubProjectByProjectId(Long.valueOf(projectId));
				
				if(purchaseOrderList != null && ! (purchaseOrderList.isEmpty())){
					errorMSG = "Can Not remove Project assigned to Purchase Order, Project name ["+projectName+"]";
				}else if(purchaseRequestList != null && ! (purchaseRequestList.isEmpty())){
					errorMSG = "Can Not remove Project assigned to Purchase Request, Project name ["+projectName+"]";
				}else if(projectSubProjects != null && !(projectSubProjects.isEmpty())){
					errorMSG = "Can Not remove Project has sub projects";
				}
				else{
					ProjectLocalServiceUtil.deleteProject(Long.valueOf(projectId));
					successMSG = "project name ["+projectName+"] successfully deleted";
				}
				
			} catch (NumberFormatException e) {
				logger.error(e.getMessage());
				errorMSG = "project Id is invalid";
			} catch (PortalException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove project";
			} catch (SystemException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove project [most probably project is assigned.]";
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove project";
			}
			
		}
		
		//retrieve updated list
		try {
			retrieveProjectListToRequest(resourceRequest);
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			errorMSG = "can not retrieve project list";
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
		
	}
	
	private Project getProject(String projectId){
		Project project = null;
		try {
			project = ProjectLocalServiceUtil.getProject(Integer.valueOf(projectId));
		} catch (NumberFormatException e) {
			logger.error("project Id has wrong format", e);
		} catch (PortalException e) {
			logger.error("unexpected error", e);
		} catch (SystemException e) {
			logger.error("unexpected error", e);
		}
		
		return project;
	}
	
	private boolean isProjectNameExists(Project newProject, String projectName) throws SystemException{
		
		Project oldProject = ProjectLocalServiceUtil.getProjectByName(projectName);
		boolean isExists = false;
		
		if(oldProject != null && 
				(newProject.getProjectId() != oldProject.getProjectId()) ){
			isExists = true;
		}
		return isExists;
	}
	
	private void paginationList(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse) throws PortletException, IOException{
		
		String jspPage = "/html/projectadmin/projectlist.jsp";
		try {
			retrieveProjectListToRequest(resourceRequest);
		} catch (SystemException e) {
		logger.error("error happened in pagination ... ",e);
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
	}
	
	private Integer getTotalNumberOfPages(Integer totalNubmerOfResult, Integer pageSize) {
        return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
	}
	
	private void clearMessages(){
		errorMSG = "";
		successMSG = "";
	}

	public void searchFilter(ActionRequest request, ActionResponse response) throws IOException, PortletException{
		response.setRenderParameter("searchFilter", "%" + ParamUtil.getString(request, "projectName") + "%");  
	}
	
	private void exportList(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException{
		logger.info("inside exportList");  
		
		String searchFilter = ParamUtil.getString(resourceRequest, "searchFilter","").toUpperCase();
		
		logger.info("searchTCN = " + searchFilter);
		
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		
		String fileName = "OMP_Project_"+formatter.format(date)+".xls";
		resourceResponse.setContentType("application/vnd.ms-excel");
		
		resourceResponse.addProperty(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=\""+fileName+"\"");			
		resourceResponse.addProperty(HttpHeaders.CACHE_CONTROL,"max-age=3600, must-revalidate");

		logger.debug("exporting.." + fileName);
		HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(resourceResponse);
		try {
			int totalCount = 0;
			
			if (searchFilter == null || searchFilter.trim().length() == 0)
				totalCount = ProjectLocalServiceUtil.getProjectsCount();
			else
				totalCount = ProjectLocalServiceUtil.getProjectsCountList(searchFilter);

			logger.debug("Project Total Count = " + totalCount);
			List<com.hp.omp.model.Project> projectsList = ProjectLocalServiceUtil.getProjectsList(0, totalCount, searchFilter);
			ProjectLocalServiceUtil.exportProjectsAsExcel(projectsList, getProjectReportHeader(), httpServletResponse.getOutputStream());

		} catch (SystemException e) {
			logger.error("Error in exportProject", e);
			e.printStackTrace();
		}
	}
	
	private List<Object> getProjectReportHeader() {
		List<Object> header = new ArrayList<Object>();
		header.add("NAME");//0
		header.add("DESCRIPTION");//1
		header.add("DISPLAY");//2
		
		return header;
	}
}
