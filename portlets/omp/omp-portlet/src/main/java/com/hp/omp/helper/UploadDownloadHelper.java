/**
 * 
 */
package com.hp.omp.helper;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.custom.FileUpload;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadException;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.util.PortalUtil;

/**
 * @author gewaly
 *
 */
public class UploadDownloadHelper {
	
	private transient final static Logger LOGGER = LoggerFactory.getLogger(UploadDownloadHelper.class);
	private static String USER_HOME = System.getProperty("user.home");
	private static String UPLOAD_FOLDER_KEY = "opm.upload.filesystem.directory";
	private static String UPLOAD_FOLDER_NAME = "omp_uploads";
	private static String FILE_SEPARATOR = System.getProperty("file.separator");
	public static final String UPLOAD_ACTION = "upload";
	public static final String DOWNLOAD_ACTION = "download";
	

	/**
	 * @param pruchaseRequestId
	 * @param multipleUpload
	 * @param resourceRequest
	 * @throws UploadException
	 */
	public static void uploadFile(long pruchaseRequestId, boolean multipleUpload,
			ResourceRequest resourceRequest) throws UploadException {

		String filePath = getPRDirectoryPath(pruchaseRequestId);
		String fieldName = "fileName";

		try {

			LOGGER.info("reading the file from liferay upload util");
			UploadPortletRequest uploadRequest = PortalUtil
					.getUploadPortletRequest(resourceRequest);
			debug("file Size: " + uploadRequest.getSize(fieldName));

			if (Validator.isBlank(fieldName)) {
				SessionErrors.add(resourceRequest, "error");
				LOGGER.error("file name is empty");
				throw new FileNotFoundException("file name is empty");
			}
			// aui fieldName in jsp
			String sourceFileName = uploadRequest.getFileName(fieldName);
			File file = uploadRequest.getFile(fieldName);

			debug("Name file:" + uploadRequest.getFileName(fieldName));

			// create new file in desired file location
			File newFile = null;
			newFile = new File(filePath + sourceFileName);
			debug("New file name: " + newFile.getName());
			debug("New file path: " + newFile.getPath());

			// create file path if not exists, and delete it and recreate it if
			// exists
			File parent = newFile.getParentFile();
			// delete parent directory if need to upload single file for each directory
			if(! (multipleUpload)){
				FileUtils.deleteDirectory(parent);
			}
			//create PR ID directory if not exist
			if(parent != null){
				if(!(parent.isDirectory())){
					parent.mkdirs();
				}
			}

			// writing file in file system
			InputStream in = new BufferedInputStream(
					uploadRequest.getFileAsStream(fieldName));
			FileInputStream fis = new FileInputStream(file);
			FileOutputStream fos = new FileOutputStream(newFile);

			byte[] bytes_ = FileUtil.getBytes(in);
			int i = fis.read(bytes_);

			while (i != -1) {
				fos.write(bytes_, 0, i);
				i = fis.read(bytes_);
			}
			fis.close();
			fos.close();
			Float size = (float) newFile.length();
			debug("file size bytes:" + size);
			debug("file size Mb:" + size / 1048576);

			debug("File created: " + newFile.getName());

		} catch (FileNotFoundException fof) {
			LOGGER.error("File Not Found.");
			throw new UploadException("File Not Found", fof);
		} catch (IOException ioe) {
			LOGGER.error("Error Reading The File.", ioe);
			throw new UploadException("Error Reading The File.");
		} catch (Exception e) {
			LOGGER.error("unexpected exception", e);
			throw new UploadException("unexpected exception", e);
		}

	}

	/**
	 * @param pruchaseRequestId
	 * @param resourceResponse
	 * @throws IOException 
	 * @throws Exception 
	 */
	public static void downloadFile(long pruchaseRequestId, ResourceResponse resourceResponse) throws IOException, Exception {
		try {
			final File prIdFolder = new File(getPRDirectoryPath(pruchaseRequestId));
			if(prIdFolder != null){
				File[] fileList = prIdFolder.listFiles();
				if(fileList.length > 0){
					//will download only first file
					LOGGER.warn("will download first file only");
					File prIdFile = fileList[0];
					if(prIdFile != null){
//						debug("file path=" + prIdFile.getPath());
//						debug("file name=" + prIdFile.getName());
//						debug("file size=" + prIdFile.length());
						FileInputStream prIdFileStream = new FileInputStream(prIdFile);

						String mimeType = MimeTypesUtil.getContentType(prIdFile);

						// Writing file to output
						resourceResponse.setContentType(mimeType);
						resourceResponse.addProperty(HttpHeaders.CONTENT_DISPOSITION,
								"attachment; filename=\"" + prIdFile.getName() + "\"");
						resourceResponse.addProperty(HttpHeaders.CACHE_CONTROL,
								"max-age=3600, must-revalidate");
						resourceResponse.addProperty(HttpHeaders.CONTENT_LENGTH,
								"" + prIdFile.length());

						OutputStream outputStream = resourceResponse.getPortletOutputStream();

						int x = IOUtils.copy(prIdFileStream, outputStream);
						debug("copied file size =" + x);
						outputStream.flush();
						outputStream.close();
						prIdFileStream.close();
					}
				}
			}
		} catch (IOException ioe) {
			LOGGER.error(ioe.getMessage(),ioe);
			throw new IOException(ioe);
		}catch(Exception e){
			LOGGER.error("unexpected error");
			throw new Exception("unexpected error", e);
		}
	}


	/**
	 * @param pruchaseRequestId
	 * @param fileName
	 * @param resourceResponse
	 * @throws IOException
	 * @throws Exception
	 */
	public static void downloadFileByName(long pruchaseRequestId, String fileName, ResourceResponse resourceResponse) throws IOException, Exception {
		try {
			debug("Pruchase Request Id = " + pruchaseRequestId + " - file name = "+fileName);

			final File prFile = new File(getPRFilePath(pruchaseRequestId, fileName));
			if(prFile != null){
//				debug("file path=" + prFile.getPath());
//				debug("file name=" + prFile.getName());
//				debug("file size=" + prFile.length());
				FileInputStream prIdFileStream = new FileInputStream(prFile);

				String mimeType = MimeTypesUtil.getContentType(prFile);

				// Writing file to output
				resourceResponse.setContentType(mimeType);
				resourceResponse.addProperty(HttpHeaders.CONTENT_DISPOSITION,
						"attachment; filename=\"" + prFile.getName() + "\"");
				resourceResponse.addProperty(HttpHeaders.CACHE_CONTROL,
						"max-age=3600, must-revalidate");
				resourceResponse.addProperty(HttpHeaders.CONTENT_LENGTH,
						"" + prFile.length());

				OutputStream outputStream = resourceResponse.getPortletOutputStream();

				int x = IOUtils.copy(prIdFileStream, outputStream);
				debug("copied file size =" + x);
				outputStream.flush();
				outputStream.close();
				prIdFileStream.close();
			}
		} catch (IOException ioe) {
			LOGGER.error(ioe.getMessage(),ioe);
			throw new IOException(ioe);
		}catch(Exception e){
			LOGGER.error("unexpected error");
			throw new Exception("unexpected error", e);
		}
	}

	/**
	 * @param pruchaseRequestId
	 * @param resourceResponse
	 * @throws IOException 
	 * @throws Exception 
	 */
	public static void downloadZipFile(long pruchaseRequestId, ResourceResponse resourceResponse) throws IOException, Exception {
		try {
			debug("Pruchase Request Id = " + pruchaseRequestId);
			
			String ZIP_FILE_NAME = "Pruchase_Request_"+pruchaseRequestId+"_All_files.zip";

			final File prIdFolder = new File(getPRDirectoryPath(pruchaseRequestId));
			ZipHelper zipHelper = new ZipHelper();
			byte[] zip= zipHelper.zipDir(getPRDirectoryPath(pruchaseRequestId), ZIP_FILE_NAME);
					if(zip != null){
//						debug("zip file path=" + prIdFolder.getPath());
//						debug("zip file name=" + prIdFolder.getName());
//						debug("zip file size=" + prIdFolder.length());

						// Writing file to output
						resourceResponse.setContentType("application/zip");
						resourceResponse.addProperty(HttpHeaders.CONTENT_DISPOSITION,
								"attachment; filename=\"" + ZIP_FILE_NAME + "\"");
						resourceResponse.addProperty(HttpHeaders.CACHE_CONTROL,
								"max-age=3600, must-revalidate");
						resourceResponse.addProperty(HttpHeaders.CONTENT_LENGTH,
								"" + zip.length);

						OutputStream outputStream = resourceResponse.getPortletOutputStream();
						
						outputStream.write(zip);
						outputStream.flush();
						outputStream.close();
			}
		} catch (IOException ioe) {
			LOGGER.error(ioe.getMessage(),ioe);
			throw new IOException(ioe);
		}catch(Exception e){
			LOGGER.error("unexpected error");
			throw new Exception("unexpected error", e);
		}
	}
	
	/**
	 * @param pruchaseRequestId
	 * @return List of file names
	 */
	public static List<String> getPRUploadedFilesNames(long pruchaseRequestId){
		debug("Pruchase Request Id = " + pruchaseRequestId);
		List<String> fileNameList = new ArrayList<String>();

		final File prIdFolder = new File(getPRDirectoryPath(pruchaseRequestId));
		if(prIdFolder != null && prIdFolder.isDirectory()){
			File[] fileList = prIdFolder.listFiles();
			for (File file : fileList) {					
				fileNameList.add(file.getName().replaceAll(StringPool.SPACE, "&nbsp;"));
			}
		}
		
		return fileNameList;
}
	
	public static List<String> getGRUploadedFilesNames(long pruchaseRequestId){
		debug("Pruchase Order Id = " + pruchaseRequestId);

		List<String> fileNameList = new ArrayList<String>();

		final File prIdFolder = new File(getPRDirectoryPath(pruchaseRequestId));
		if(prIdFolder != null && prIdFolder.isDirectory()){
			File[] fileList = prIdFolder.listFiles();
			for (File file : fileList) {					
				fileNameList.add(file.getName().replaceAll(StringPool.SPACE, "&nbsp;"));
			}
		}
		
		return fileNameList;
	}
	
	public static List<FileUpload> OLDgetGRUploadedFilesNames(long pruchaseRequestId){
		debug("Pruchase Request Id = " + pruchaseRequestId);
		

		List<FileUpload> fileNameList = new ArrayList<FileUpload>();

		final File prIdFolder = new File(getPRDirectoryPath(pruchaseRequestId));
		if(prIdFolder != null && prIdFolder.isDirectory()){
			File[] fileList = prIdFolder.listFiles();
			for (File file : fileList) {
				Path filePath = file.toPath();
				BasicFileAttributes attributes = null;
		        try {
		            attributes = Files.readAttributes(filePath, BasicFileAttributes.class);
		        } catch (IOException exception) {
		            System.out.println("Exception handled when trying to get file " +
		                    "attributes: " + exception.getMessage());
		        }
		        
		        long milliseconds = attributes.creationTime().to(TimeUnit.MILLISECONDS);
		        Date creationDate = new Date(attributes.creationTime().to(TimeUnit.MILLISECONDS));
		        String dataFile = "";
		        String minuti = "";
		        if((milliseconds > Long.MIN_VALUE) && (milliseconds < Long.MAX_VALUE)){
		            dataFile = creationDate.getDate() + "/" +
		                    (creationDate.getMonth() + 1) + "/" +
		                    (creationDate.getYear() + 1900) + " - " +
		                    (creationDate.getHours() + 2 + ":") ;
		            
		            minuti = ("00" + creationDate.getMinutes());
		            dataFile = dataFile + minuti.substring(minuti.length() - 2);
		            
		            System.out.println("File " + filePath.toString() + " created " + dataFile);
		        }
		        
		        FileUpload fu = new FileUpload();
		        fu.setFileName(file.getName().replaceAll(StringPool.SPACE, "&nbsp;"));
		        fu.setFileNameDisplayed(file.getName().replaceAll(StringPool.SPACE, "&nbsp;") + " - " + dataFile);
		        Long fileData = Long.valueOf("" + (creationDate.getYear() + 1900) + (creationDate.getMonth() + 1) + creationDate.getDate() + (creationDate.getHours() + 2) + (minuti.substring(minuti.length() - 2)));
		        fu.setFileDate(fileData);
		        
				fileNameList.add(fu);
			}
			
			if(fileNameList.size() > 0){
				// Sorting
				Collections.sort(fileNameList, new Comparator<FileUpload>() {
				        public int compare(FileUpload fUp2, FileUpload fUp1){
				            return  fUp1.getFileDate().compareTo(fUp2.getFileDate());
				        }
				    });
			}
		}
		
		return fileNameList;
}

	/**
	 * @param pruchaseRequestId
	 * @return
	 * @throws IOException
	 */
	public static boolean deletePRFileDirectory(long pruchaseRequestId) throws IOException{
		boolean deleted = false;
		try {
			debug("Pruchase Request Id = " + pruchaseRequestId);
			final File prIdFolder = new File(getPRDirectoryPath(pruchaseRequestId));

			FileUtils.deleteDirectory(prIdFolder);
			
			LOGGER.info("Pruchase Request Id ["+pruchaseRequestId +"] Directory has been deleted.");
			
			deleted = true;
		}catch (IOException e1) {
			LOGGER.error("Error while deleting Pruchase Request Id ["+pruchaseRequestId +"] Directory", e1);
		}
		
		return deleted;
	}
	
	/**
	 * @param pruchaseRequestId
	 * @param fileName
	 * @return
	 */
	public static boolean deleteFileByName(long pruchaseRequestId, String fileName){
			
		debug("Pruchase Request Id = " + pruchaseRequestId);
		final File prIdFile = new File(getPRFilePath(pruchaseRequestId, fileName));
		boolean deleted = prIdFile.delete();
		if(deleted){
			LOGGER.info("Pruchase Request Id ["+pruchaseRequestId +"],  File ["+fileName+"] has been deleted.");
			}else{
				LOGGER.info("Pruchase Request Id ["+pruchaseRequestId +"] File ["+fileName+"]  NOT  deleted.");
				}
			
		return deleted;
	}

	/**
	 * @param pruchaseRequestId
	 * @return
	 */
	public static boolean isPRHasUploadedFiles(long pruchaseRequestId) throws IOException{
			
		boolean hasFiles = false;
		try{
			final File prIdFolder = new File(getPRDirectoryPath(pruchaseRequestId));
			if(prIdFolder != null && prIdFolder.isDirectory()){
				
				if(prIdFolder.listFiles().length > 0){
					
					if(LOGGER.isDebugEnabled()){
						File[] fileList = prIdFolder.listFiles();
						File prIdFile = fileList[0];
//						debug("file path=" + prIdFile.getPath());
//						debug("file name=" + prIdFile.getName());
//						debug("file size=" + prIdFile.length());
					}
					// Pruchase Request has files
					hasFiles = true;
					
				}else{
					LOGGER.warn("Pruchase Request Id ["+pruchaseRequestId +"] has empty Directory.");
				}
				
			}
		}catch (Exception e1) {
			LOGGER.error("Error while checking if Pruchase Request Id ["+pruchaseRequestId +"] has files", e1);
		}	
			
		return hasFiles;
	}
	
	/**
	 * @param pruchaseRequestId
	 * @return file path
	 */
	private static String getPRDirectoryPath(long pruchaseRequestId) {
		String directoryPath = "";
		String errorMSG = "Couldn't locate upload directory Key["+UPLOAD_FOLDER_KEY+"], will return the default path ["+getDefualtDirectory()+"]";

		try {
			directoryPath = PrefsPropsUtil.getString(UPLOAD_FOLDER_KEY);
		} catch (SystemException e) {
			LOGGER.error(errorMSG, e);
		}
		
		if(StringUtils.isBlank(directoryPath)){
			directoryPath = getDefualtDirectory() ;
		}
		
		if(!(directoryPath.endsWith(FILE_SEPARATOR))){
			directoryPath += FILE_SEPARATOR;
		}
		
		//add PR id
		directoryPath += pruchaseRequestId + FILE_SEPARATOR;
				
//		if(LOGGER.isDebugEnabled()){
//			LOGGER.debug("Upload path is ["+ directoryPath  +"]");
//		}
		
		return directoryPath;
	}
	
	private static String getDefualtDirectory(){
		return USER_HOME + FILE_SEPARATOR + UPLOAD_FOLDER_NAME + FILE_SEPARATOR;
	}
	private static String getPRFilePath(long pruchaseRequestId, String fileName){
		return getPRDirectoryPath(pruchaseRequestId) + fileName;
	}
	
	/**
	 * @param msg
	 */
	private static void debug(String msg){
		 if(LOGGER.isDebugEnabled()){
			 LOGGER.debug(msg);
		 }
	}
	
	public static void uploadFilePR(UploadPortletRequest uploadRequest, long pruchaseRequestId, 
			ResourceRequest resourceRequest) throws UploadException {

		String filePath = getPRDirectoryPath(pruchaseRequestId);
		String fieldName = "fileName";

		try {

			if (Validator.isBlank(fieldName)) {
				SessionErrors.add(resourceRequest, "error");
				LOGGER.error("file name is empty");
				throw new FileNotFoundException("file name is empty");
			}
			
			String sourceFileName = uploadRequest.getFileName(fieldName);
			File file = uploadRequest.getFile(fieldName);

			debug("Name file:" + uploadRequest.getFileName(fieldName));

			// create new file in desired file location
			File newFile = null;
			newFile = new File(filePath + sourceFileName);
			debug("New file name: " + newFile.getName());
			debug("New file path: " + newFile.getPath());

			// create file path if not exists, and delete it and recreate it if
			// exists
			File parent = newFile.getParentFile();
			
			FileUtils.deleteDirectory(parent);
			
			//create PR ID directory if not exist
			if(parent != null){
				if(!(parent.isDirectory())){
					parent.mkdirs();
				}
			}

			// writing file in file system
			InputStream in = new BufferedInputStream(
					uploadRequest.getFileAsStream(fieldName));
			FileInputStream fis = new FileInputStream(file);
			FileOutputStream fos = new FileOutputStream(newFile);

			byte[] bytes_ = FileUtil.getBytes(in);
			int i = fis.read(bytes_);

			while (i != -1) {
				fos.write(bytes_, 0, i);
				i = fis.read(bytes_);
			}
			fis.close();
			fos.close();
			Float size = (float) newFile.length();
			debug("file size bytes:" + size);
			debug("file size Mb:" + size / 1048576);

			debug("File created: " + newFile.getName());

		} catch (FileNotFoundException fof) {
			LOGGER.error("File Not Found.");
			throw new UploadException("File Not Found", fof);
		} catch (IOException ioe) {
			LOGGER.error("Error Reading The File.", ioe);
			throw new UploadException("Error Reading The File.");
		} catch (Exception e) {
			LOGGER.error("unexpected exception", e);
			throw new UploadException("unexpected exception", e);
		}

	}
	
}
