package com.hp.omp.service.persistence;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.hp.omp.model.CostCenter;
import com.hp.omp.model.impl.CostCenterImpl;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class CostCenterFinderImpl extends CostCenterPersistenceImpl implements CostCenterFinder{

	private static Log _log = LogFactoryUtil.getLog(CostCenterFinderImpl.class);
	static final String GET_COST_CENTERS_LIST = CostCenterFinderImpl.class.getName()+".getCostCentersList";

	@SuppressWarnings("unchecked")
	public List<CostCenter> getCostCentersList(int start, int end,
			String searchFilter, int gruppoUsers) throws SystemException {
		Session session=null;
		List<CostCenter> list = new ArrayList<CostCenter>();
		try {
			session  = openSession();

			String sql = null;

			if(StringUtils.isEmpty(searchFilter))
				searchFilter = "%%";

			sql = CustomSQLUtil.get(GET_COST_CENTERS_LIST);
			SQLQuery query = session.createSQLQuery(sql);

			if(query == null){
				_log.error("get cost center list error, no sql found");
				return null;
			}

			query.setString(0, searchFilter);
			query.setInteger(1, gruppoUsers);

			query.addEntity("CostCenterList", CostCenterImpl.class);
			list = (List<CostCenter>)QueryUtil.list(query, getDialect(), start, end);
		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
		return list;
	}
}
