package com.hp.omp.decorator;

import java.io.Serializable;

import org.displaytag.decorator.TableDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.BudgetCategory;
import com.hp.omp.model.BudgetSubCategory;
import com.hp.omp.model.MacroDriver;
import com.hp.omp.model.MicroDriver;
import com.hp.omp.model.ODNPName;
import com.hp.omp.model.Project;
import com.hp.omp.model.SubProject;
import com.hp.omp.model.TrackingCodes;
import com.hp.omp.model.WbsCode;
import com.hp.omp.service.BudgetCategoryLocalServiceUtil;
import com.hp.omp.service.BudgetSubCategoryLocalServiceUtil;
import com.hp.omp.service.MacroDriverLocalServiceUtil;
import com.hp.omp.service.MicroDriverLocalServiceUtil;
import com.hp.omp.service.ODNPNameLocalServiceUtil;
import com.hp.omp.service.ProjectLocalServiceUtil;
import com.hp.omp.service.SubProjectLocalServiceUtil;
import com.hp.omp.service.WbsCodeLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

public class TrackingCodeListDecorator extends TableDecorator implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 359382163722978963L;
	Logger logger = LoggerFactory.getLogger(TrackingCodeListDecorator.class);

	
	public String getProjectName() {
		TrackingCodes trackingCode = (TrackingCodes) this.getCurrentRowObject();
		try {
			Project project = ProjectLocalServiceUtil.getProject(Long.valueOf(trackingCode.getProjectId()));
			return project.getProjectName();
		} catch (NumberFormatException e) {
			logger.error("Error in get project",e);
		} catch (PortalException e) {
			logger.error("Error in get project",e);
		} catch (SystemException e) {
			logger.error("Error in get project",e);
		}
		return "";
	}
	
	public String getSubProjectName() {
		TrackingCodes trackingCode = (TrackingCodes) this.getCurrentRowObject();
		try {
			SubProject subProject = SubProjectLocalServiceUtil.getSubProject(Long.valueOf(trackingCode.getSubProjectId()));
			return subProject.getSubProjectName();
		} catch (NumberFormatException e) {
			logger.error("Error in get subproject",e);
		} catch (PortalException e) {
			logger.error("Error in get subproject",e);
		} catch (SystemException e) {
			logger.error("Error in get subproject",e);
		}
		return "";
	}
	public String getBudgetCategoryName() {
		TrackingCodes trackingCode = (TrackingCodes) this.getCurrentRowObject();
		try {
			BudgetCategory budgetCategory = BudgetCategoryLocalServiceUtil.getBudgetCategory(Long.valueOf(trackingCode.getBudgetCategoryId()));
			return budgetCategory.getBudgetCategoryName();
		} catch (NumberFormatException e) {
			logger.error("Error in get budget Category",e);
		} catch (PortalException e) {
			logger.error("Error in get budget Category",e);
		} catch (SystemException e) {
			logger.error("Error in get budget Category",e);
		}
		return "";
	}
	public String getBudgetSubCategoryName() {
		TrackingCodes trackingCode = (TrackingCodes) this.getCurrentRowObject();
		try {
			BudgetSubCategory budgetSubCategory = BudgetSubCategoryLocalServiceUtil.getBudgetSubCategory(Long.valueOf(trackingCode.getBudgetSubCategoryId()));
			return budgetSubCategory.getBudgetSubCategoryName();
		} catch (NumberFormatException e) {
			logger.error("Error in get budget subcategory",e);
		} catch (PortalException e) {
			logger.error("Error in get budget subcategory",e);
		} catch (SystemException e) {
			logger.error("Error in get budget subcategory",e);
		}
		return "";
	}
	public String getMacroDriverName() {
		TrackingCodes trackingCode = (TrackingCodes) this.getCurrentRowObject();
		try {
			MacroDriver macroDriver = MacroDriverLocalServiceUtil.getMacroDriver(Long.valueOf(trackingCode.getMacroDriverId()));
			return macroDriver.getMacroDriverName();
		} catch (NumberFormatException e) {
			logger.error("Error in get macro driver",e);
		} catch (PortalException e) {
			logger.error("Error in get macro driver",e);
		} catch (SystemException e) {
			logger.error("Error in get macro driver",e);
		}
		return "";
	}
	public String getMicroDriverName() {
		TrackingCodes trackingCode = (TrackingCodes) this.getCurrentRowObject();
		try {
			MicroDriver microDriver = MicroDriverLocalServiceUtil.getMicroDriver(Long.valueOf(trackingCode.getMicroDriverId()));
			return microDriver.getMicroDriverName();
		} catch (NumberFormatException e) {
			logger.error("Error in get micro driver",e);
		} catch (PortalException e) {
			logger.error("Error in get micro driver",e);
		} catch (SystemException e) {
			logger.error("Error in get micro driver",e);
		}
		return "";
	}
	public String getOdnpName() {
		TrackingCodes trackingCode = (TrackingCodes) this.getCurrentRowObject();
		try {
			ODNPName odnpName = ODNPNameLocalServiceUtil.getODNPName(Long.valueOf(trackingCode.getOdnpNameId()));
			return odnpName.getOdnpNameName();
		} catch (NumberFormatException e) {
			logger.error("Error in get odnp",e);
		} catch (PortalException e) {
			logger.error("Error in get odnp",e);
		} catch (SystemException e) {
			logger.error("Error in get odnp",e);
		}
		return "";
	}
	public String getWbsCodeName() {
		TrackingCodes trackingCode = (TrackingCodes) this.getCurrentRowObject();
		try {
			WbsCode wbsCode = WbsCodeLocalServiceUtil.getWbsCode(Long.valueOf(trackingCode.getWbsCodeId()));
			return wbsCode.getWbsCodeName();
		} catch (NumberFormatException e) {
			logger.error("Error in get wbs code",e);
		} catch (PortalException e) {
			logger.error("Error in get wbs code",e);
		} catch (SystemException e) {
			logger.error("Error in get wbs code",e);
		}
		return "";
	}
	
	
	
}
