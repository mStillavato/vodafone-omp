package com.hp.omp.decorator;

import java.io.Serializable;

import org.displaytag.decorator.TableDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.CostCenter;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.WbsCode;
import com.hp.omp.model.custom.PurchaseRequestStatus;
import com.hp.omp.portlet.CostCenterAdminPortlet;

public class CostCenterListDecorator extends TableDecorator implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2685221218697856230L;

	Logger logger = LoggerFactory.getLogger(CostCenterListDecorator.class);
	public String getCostCenterId() {
		
        return getRemoveAction();
    }

	public String getCostCenterName() {
		
        return getUpdateAction();
    }
	
	
	public String getRemoveAction() {
		
		StringBuilder sb = new StringBuilder();
        CostCenter item = (CostCenter) this.getCurrentRowObject();
      
        long costCenterId = item.getCostCenterId();
        String costCenterName = item.getCostCenterName();
         //getPortletContext().getRequestDispatcher(response.encodeURL(Constants.PATH_TO_JSP_PAGE + jspPage)).include(request, response);
        sb.append("<a href=\"javascript:removeCostCenter("+costCenterId+",'"+costCenterName+"')\"");
        sb.append("\">");
        sb.append(" Remove ");
        sb.append("</a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
    }	
	
	public String getUpdateAction() {
		
		CostCenter item = (CostCenter) this.getCurrentRowObject();
		StringBuilder sb = new StringBuilder();
       
      
        long costCenterId = item.getCostCenterId();
        String costCenterName = item.getCostCenterName();
        
        sb.append("<a onclick=\"javascript:updateCostCenter(this, "+costCenterId+")\" href='"+costCenterName+"'");
        sb.append("\"> ");
        sb.append(costCenterName);
        sb.append(" </a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
        
    }
}
