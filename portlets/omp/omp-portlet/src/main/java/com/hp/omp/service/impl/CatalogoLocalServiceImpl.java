/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service.impl;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;

import com.hp.omp.model.Catalogo;
import com.hp.omp.model.ExcelReport;
import com.hp.omp.model.ExcelStyler;
import com.hp.omp.model.ExcelTable;
import com.hp.omp.model.custom.CatalogoListDTO;
import com.hp.omp.model.custom.ExcelTableRow;
import com.hp.omp.model.custom.LayoutNotNssCatalogoListDTO;
import com.hp.omp.model.custom.OMPAggregateReportExcelRow;
import com.hp.omp.model.custom.SearchCatalogoDTO;
import com.hp.omp.model.impl.ExcelDefaultStyler;
import com.hp.omp.model.impl.ExcelReportImpl;
import com.hp.omp.model.impl.ExcelTableImpl;
import com.hp.omp.service.base.CatalogoLocalServiceBaseImpl;
import com.hp.omp.service.persistence.CatalogoFinderUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

/**
 * The implementation of the catalogo local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.CatalogoLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see com.hp.omp.service.base.CatalogoLocalServiceBaseImpl
 * @see com.hp.omp.service.CatalogoLocalServiceUtil
 */
public class CatalogoLocalServiceImpl extends CatalogoLocalServiceBaseImpl {

	public List<CatalogoListDTO> getCatalogoList(SearchCatalogoDTO searchDTO) {
		return CatalogoFinderUtil.getCatalogoList(searchDTO);
	}

	public Long getCatalogoListCount(SearchCatalogoDTO searchDTO) {
		return CatalogoFinderUtil.getCatalogoListCount(searchDTO);
	}
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.hp.omp.service.CatalogoLocalServiceUtil} to access the catalogo local service.
	 */

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Catalogo> getCatalogoForVendor(int start, int end, String vendorCode)
			throws SystemException {
		List<Catalogo> resultQuery = null;
		resultQuery= CatalogoFinderUtil.getCatalogoForVendor(start, end, vendorCode);
		return resultQuery;
	}

	public void exportReportAsExcel(List<LayoutNotNssCatalogoListDTO> listPoCatalogNotNssDTO, List<Object> header, OutputStream out) throws SystemException {

		ExcelReport excelReport = new ExcelReportImpl();
		ExcelStyler styler = new ExcelDefaultStyler(excelReport.getWorkBook());
		excelReport.setStyler(styler);

		List<ExcelTableRow> poCatalogNotNssList = new ArrayList<ExcelTableRow>();
		int lineNumber = 0;
		for(LayoutNotNssCatalogoListDTO poCatalogNotNssDTO : listPoCatalogNotNssDTO){
			OMPAggregateReportExcelRow tableRow = new OMPAggregateReportExcelRow();
			tableRow.setFieldAtIndex(++lineNumber, 0);
			tableRow.setFieldAtIndex(poCatalogNotNssDTO.getMaterialId(), 1);
			tableRow.setFieldAtIndex(poCatalogNotNssDTO.getMaterialDesc(), 2);
			tableRow.setFieldAtIndex(poCatalogNotNssDTO.getQuantity(), 3);
			tableRow.setFieldAtIndex(poCatalogNotNssDTO.getNetPrice(), 4);
			tableRow.setFieldAtIndex(poCatalogNotNssDTO.getCurrency(), 5);
			tableRow.setFieldAtIndex(poCatalogNotNssDTO.getDeliveryDate(), 6);
			tableRow.setFieldAtIndex(poCatalogNotNssDTO.getDeliveryAddress(), 7);
			tableRow.setFieldAtIndex(poCatalogNotNssDTO.getItemText(), 8);
			tableRow.setFieldAtIndex(poCatalogNotNssDTO.getLocationEvo(), 9);
			tableRow.setFieldAtIndex(poCatalogNotNssDTO.getTargaTecnica(), 10);
			tableRow.setFieldAtIndex(poCatalogNotNssDTO.getVendorCode(), 11);
			if(poCatalogNotNssDTO.getIcApproval() != null && !("NULL".equals(poCatalogNotNssDTO.getIcApproval().toUpperCase())))
				tableRow.setFieldAtIndex(poCatalogNotNssDTO.getIcApproval(), 12);
			else
				tableRow.setFieldAtIndex("", 12);
			
			if(poCatalogNotNssDTO.getCategoryCode() != null && !("NULL".equals(poCatalogNotNssDTO.getCategoryCode().toUpperCase())))
				tableRow.setFieldAtIndex(poCatalogNotNssDTO.getCategoryCode(), 13);
			else
				tableRow.setFieldAtIndex("", 13);
			
			if(poCatalogNotNssDTO.getOfferNumber() != null && !("NULL".equals(poCatalogNotNssDTO.getOfferNumber().toUpperCase())))
				tableRow.setFieldAtIndex(poCatalogNotNssDTO.getOfferNumber(), 14);
			else
				tableRow.setFieldAtIndex("", 14);
			
			poCatalogNotNssList.add(tableRow);
		}

		ExcelTable euroTable = new ExcelTableImpl(header, poCatalogNotNssList, "Euro Details");
		excelReport.addExcelTable(euroTable, 0);

		Workbook exelWorkBook = excelReport.createExelWorkBook();
		try {
			exelWorkBook.write(out);
		} catch (IOException e) {
		} finally{
			try {
				out.flush();
				out.close();
			} catch (IOException e) {
			}
		}
	}
	
	private Date stringToDate(String date){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy"); 
		try {
			return df.parse(date);
		} catch (ParseException e) {
			return null;
		}
	}
	
	private String formatDate(String date){
		try{
			String anno = date.substring(0, 4);
			String mese = date.substring(5, 7);
			String giorno = date.substring(8, 10);
			 
			return (giorno + '.' + mese + '.' + anno);
		}catch(Exception e){
			return "";
		}
	}

	public void exportCatalogoAsExcel(List<com.hp.omp.model.custom.CatalogoExportListDTO> listCatalog,
			List<Object> header, OutputStream out) throws SystemException {

		ExcelReport excelReport = new ExcelReportImpl();
		ExcelStyler styler = new ExcelDefaultStyler(excelReport.getWorkBook());
		excelReport.setStyler(styler);

		List<ExcelTableRow> poCatalogNotNssList = new ArrayList<ExcelTableRow>();
		for(com.hp.omp.model.custom.CatalogoExportListDTO catalogo : listCatalog){
			OMPAggregateReportExcelRow tableRow = new OMPAggregateReportExcelRow();
			tableRow.setFieldAtIndex(catalogo.getChiaveInforecord(), 0);
			tableRow.setFieldAtIndex(catalogo.getDataCreazione(), 1);
			tableRow.setFieldAtIndex(catalogo.getDataFineValidita(), 2);
			tableRow.setFieldAtIndex(catalogo.getDivisione(), 3);
			tableRow.setFieldAtIndex(catalogo.getFlgCanc(), 4);
			tableRow.setFieldAtIndex(catalogo.getFornFinCode(), 5);
			tableRow.setFieldAtIndex(catalogo.getFornitoreCode(), 6);
			tableRow.setFieldAtIndex(catalogo.getFornitoreDesc(), 7);
			tableRow.setFieldAtIndex(catalogo.getMatCatLvl2(), 8);
			tableRow.setFieldAtIndex(catalogo.getMatCatLvl2Desc(), 9);
			tableRow.setFieldAtIndex(catalogo.getMatCatLvl4(), 10);
			tableRow.setFieldAtIndex(catalogo.getMatCatLvl4Desc(), 11);
			tableRow.setFieldAtIndex(catalogo.getMatCod(), 12);
			tableRow.setFieldAtIndex(catalogo.getMatCodFornFinale(), 13);
			tableRow.setFieldAtIndex(catalogo.getMatDesc(), 14);
			tableRow.setFieldAtIndex(catalogo.getPrezzo(), 15);
			tableRow.setFieldAtIndex(catalogo.getPurchOrganiz(), 16);
			tableRow.setFieldAtIndex(catalogo.getUm(), 17);
			tableRow.setFieldAtIndex(catalogo.getUmPrezzo(), 18);
			tableRow.setFieldAtIndex(catalogo.getUmPrezzoPo(), 19);
			tableRow.setFieldAtIndex(catalogo.getValuta(), 20);
			tableRow.setFieldAtIndex(catalogo.getUpperMatDesc(), 21);
			
			poCatalogNotNssList.add(tableRow);
		}

		ExcelTable euroTable = new ExcelTableImpl(header, poCatalogNotNssList, "Catalogo");
		excelReport.addExcelTable(euroTable, 0);

		Workbook exelWorkBook = excelReport.createExelWorkBook();
		try {
			exelWorkBook.write(out);
		} catch (IOException e) {
		} finally{
			try {
				out.flush();
				out.close();
			} catch (IOException e) {
			}
		}
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<com.hp.omp.model.custom.CatalogoExportListDTO> getExportCatalogoList(
			SearchCatalogoDTO searchDTO) {
		return CatalogoFinderUtil.getExportCatalogoList(searchDTO);
	}
}