package com.hp.omp.model.custom;

public class PurchaseRequestExcelRow {

	private String vendorCode;
	private String evoLocationId;
	private String evoCodMateriale;
	private String evoDesMateriale;
	private String poQuantity;
	private String deliveryDate;
	private String netPrice;
	private String plant;
	private String wbsCodeName;
	private String addressNumber;
	private String itemText;
	private String trackingNumber;
	private String targaTecnica;
	private String currency;
	private String icApproval;
	private String categoryCode;
	private String offerNumber;
	
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	public String getEvoLocationId() {
		return evoLocationId;
	}
	public void setEvoLocationId(String evoLocationId) {
		this.evoLocationId = evoLocationId;
	}
	public String getEvoCodMateriale() {
		return evoCodMateriale;
	}
	public void setEvoCodMateriale(String evoCodMateriale) {
		this.evoCodMateriale = evoCodMateriale;
	}
	public String getEvoDesMateriale() {
		return evoDesMateriale;
	}
	public void setEvoDesMateriale(String evoDesMateriale) {
		this.evoDesMateriale = evoDesMateriale;
	}
	public String getPoQuantity() {
		return poQuantity;
	}
	public void setPoQuantity(String poQuantity) {
		this.poQuantity = poQuantity;
	}
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getNetPrice() {
		return netPrice;
	}
	public void setNetPrice(String netPrice) {
		this.netPrice = netPrice;
	}
	public String getPlant() {
		return plant;
	}
	public void setPlant(String plant) {
		this.plant = plant;
	}
	public String getWbsCodeName() {
		return wbsCodeName;
	}
	public void setWbsCodeName(String wbsCodeName) {
		this.wbsCodeName = wbsCodeName;
	}
	public String getAddressNumber() {
		return addressNumber;
	}
	public void setAddressNumber(String addressNumber) {
		this.addressNumber = addressNumber;
	}
	public String getItemText() {
		return itemText;
	}
	public void setItemText(String itemText) {
		this.itemText = itemText;
	}
	public String getTrackingNumber() {
		return trackingNumber;
	}
	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}
	public String getTargaTecnica() {
		return targaTecnica;
	}
	public void setTargaTecnica(String targaTecnica) {
		this.targaTecnica = targaTecnica;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getIcApproval() {
		return icApproval;
	}
	public void setIcApproval(String icApproval) {
		this.icApproval = icApproval;
	}
	public String getCategoryCode() {
		return categoryCode;
	}
	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}
	public String getOfferNumber() {
		return offerNumber;
	}
	public void setOfferNumber(String offerNumber) {
		this.offerNumber = offerNumber;
	}

}
