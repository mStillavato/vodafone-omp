package com.hp.omp.decorator;

import java.io.Serializable;

import org.displaytag.decorator.TableDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.BudgetSubCategory;

public class BudgetSubCategoryListDecorator extends TableDecorator implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2685221218697856230L;

	Logger logger = LoggerFactory.getLogger(BudgetSubCategoryListDecorator.class);
	
	public String getBudgetSubCategoryId() {
		
        return getRemoveAction();
    }
	
	public String getBudgetSubCategoryName() {
		
        return getUpdateAction();
    }
	
	public String getRemoveAction() {
		
        StringBuilder sb = new StringBuilder();
        BudgetSubCategory item = (BudgetSubCategory) this.getCurrentRowObject();
      
        long budgetSubCategoryId = item.getBudgetSubCategoryId();
        String budgetSubCategoryName = item.getBudgetSubCategoryName();
        sb.append("<a href=\"javascript:removeBudgetSubCategory("+budgetSubCategoryId+",'"+budgetSubCategoryName+"')\"");
        sb.append("\">");
        sb.append(" Remove ");
        sb.append("</a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
    }	
	
	public String getUpdateAction() {
		
		StringBuilder sb = new StringBuilder();
        BudgetSubCategory item = (BudgetSubCategory) this.getCurrentRowObject();
      
        long budgetSubCategoryId = item.getBudgetSubCategoryId();
        String budgetSubCategoryName = item.getBudgetSubCategoryName();
        sb.append("<a onclick=\"javascript:updateBudgetSubCategory(this, "+budgetSubCategoryId+")\" href='"+budgetSubCategoryName+"'");
        sb.append("\"> ");
        sb.append(budgetSubCategoryName);
        sb.append(" </a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
        
    }

}
