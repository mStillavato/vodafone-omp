package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchTrackingCodesException;
import com.hp.omp.model.TrackingCodes;
import com.hp.omp.model.impl.TrackingCodesImpl;
import com.hp.omp.model.impl.TrackingCodesModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the tracking codes service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see TrackingCodesPersistence
 * @see TrackingCodesUtil
 * @generated
 */
public class TrackingCodesPersistenceImpl extends BasePersistenceImpl<TrackingCodes>
    implements TrackingCodesPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link TrackingCodesUtil} to access the tracking codes persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = TrackingCodesImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TrackingCodesModelImpl.ENTITY_CACHE_ENABLED,
            TrackingCodesModelImpl.FINDER_CACHE_ENABLED,
            TrackingCodesImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TrackingCodesModelImpl.ENTITY_CACHE_ENABLED,
            TrackingCodesModelImpl.FINDER_CACHE_ENABLED,
            TrackingCodesImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TrackingCodesModelImpl.ENTITY_CACHE_ENABLED,
            TrackingCodesModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_TRACKINGCODES = "SELECT trackingCodes FROM TrackingCodes trackingCodes";
    private static final String _SQL_COUNT_TRACKINGCODES = "SELECT COUNT(trackingCodes) FROM TrackingCodes trackingCodes";
    private static final String _ORDER_BY_ENTITY_ALIAS = "trackingCodes.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No TrackingCodes exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(TrackingCodesPersistenceImpl.class);
    private static TrackingCodes _nullTrackingCodes = new TrackingCodesImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<TrackingCodes> toCacheModel() {
                return _nullTrackingCodesCacheModel;
            }
        };

    private static CacheModel<TrackingCodes> _nullTrackingCodesCacheModel = new CacheModel<TrackingCodes>() {
            public TrackingCodes toEntityModel() {
                return _nullTrackingCodes;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the tracking codes in the entity cache if it is enabled.
     *
     * @param trackingCodes the tracking codes
     */
    public void cacheResult(TrackingCodes trackingCodes) {
        EntityCacheUtil.putResult(TrackingCodesModelImpl.ENTITY_CACHE_ENABLED,
            TrackingCodesImpl.class, trackingCodes.getPrimaryKey(),
            trackingCodes);

        trackingCodes.resetOriginalValues();
    }

    /**
     * Caches the tracking codeses in the entity cache if it is enabled.
     *
     * @param trackingCodeses the tracking codeses
     */
    public void cacheResult(List<TrackingCodes> trackingCodeses) {
        for (TrackingCodes trackingCodes : trackingCodeses) {
            if (EntityCacheUtil.getResult(
                        TrackingCodesModelImpl.ENTITY_CACHE_ENABLED,
                        TrackingCodesImpl.class, trackingCodes.getPrimaryKey()) == null) {
                cacheResult(trackingCodes);
            } else {
                trackingCodes.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all tracking codeses.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(TrackingCodesImpl.class.getName());
        }

        EntityCacheUtil.clearCache(TrackingCodesImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the tracking codes.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(TrackingCodes trackingCodes) {
        EntityCacheUtil.removeResult(TrackingCodesModelImpl.ENTITY_CACHE_ENABLED,
            TrackingCodesImpl.class, trackingCodes.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<TrackingCodes> trackingCodeses) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (TrackingCodes trackingCodes : trackingCodeses) {
            EntityCacheUtil.removeResult(TrackingCodesModelImpl.ENTITY_CACHE_ENABLED,
                TrackingCodesImpl.class, trackingCodes.getPrimaryKey());
        }
    }

    /**
     * Creates a new tracking codes with the primary key. Does not add the tracking codes to the database.
     *
     * @param trackingCodeId the primary key for the new tracking codes
     * @return the new tracking codes
     */
    public TrackingCodes create(long trackingCodeId) {
        TrackingCodes trackingCodes = new TrackingCodesImpl();

        trackingCodes.setNew(true);
        trackingCodes.setPrimaryKey(trackingCodeId);

        return trackingCodes;
    }

    /**
     * Removes the tracking codes with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param trackingCodeId the primary key of the tracking codes
     * @return the tracking codes that was removed
     * @throws com.hp.omp.NoSuchTrackingCodesException if a tracking codes with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public TrackingCodes remove(long trackingCodeId)
        throws NoSuchTrackingCodesException, SystemException {
        return remove(Long.valueOf(trackingCodeId));
    }

    /**
     * Removes the tracking codes with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the tracking codes
     * @return the tracking codes that was removed
     * @throws com.hp.omp.NoSuchTrackingCodesException if a tracking codes with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrackingCodes remove(Serializable primaryKey)
        throws NoSuchTrackingCodesException, SystemException {
        Session session = null;

        try {
            session = openSession();

            TrackingCodes trackingCodes = (TrackingCodes) session.get(TrackingCodesImpl.class,
                    primaryKey);

            if (trackingCodes == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchTrackingCodesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(trackingCodes);
        } catch (NoSuchTrackingCodesException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected TrackingCodes removeImpl(TrackingCodes trackingCodes)
        throws SystemException {
        trackingCodes = toUnwrappedModel(trackingCodes);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, trackingCodes);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(trackingCodes);

        return trackingCodes;
    }

    @Override
    public TrackingCodes updateImpl(
        com.hp.omp.model.TrackingCodes trackingCodes, boolean merge)
        throws SystemException {
        trackingCodes = toUnwrappedModel(trackingCodes);

        boolean isNew = trackingCodes.isNew();

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, trackingCodes, merge);

            trackingCodes.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(TrackingCodesModelImpl.ENTITY_CACHE_ENABLED,
            TrackingCodesImpl.class, trackingCodes.getPrimaryKey(),
            trackingCodes);

        return trackingCodes;
    }

    protected TrackingCodes toUnwrappedModel(TrackingCodes trackingCodes) {
        if (trackingCodes instanceof TrackingCodesImpl) {
            return trackingCodes;
        }

        TrackingCodesImpl trackingCodesImpl = new TrackingCodesImpl();

        trackingCodesImpl.setNew(trackingCodes.isNew());
        trackingCodesImpl.setPrimaryKey(trackingCodes.getPrimaryKey());

        trackingCodesImpl.setTrackingCodeId(trackingCodes.getTrackingCodeId());
        trackingCodesImpl.setTrackingCodeName(trackingCodes.getTrackingCodeName());
        trackingCodesImpl.setTrackingCodeDescription(trackingCodes.getTrackingCodeDescription());
        trackingCodesImpl.setProjectId(trackingCodes.getProjectId());
        trackingCodesImpl.setSubProjectId(trackingCodes.getSubProjectId());
        trackingCodesImpl.setBudgetCategoryId(trackingCodes.getBudgetCategoryId());
        trackingCodesImpl.setBudgetSubCategoryId(trackingCodes.getBudgetSubCategoryId());
        trackingCodesImpl.setMacroDriverId(trackingCodes.getMacroDriverId());
        trackingCodesImpl.setMicroDriverId(trackingCodes.getMicroDriverId());
        trackingCodesImpl.setOdnpNameId(trackingCodes.getOdnpNameId());
        trackingCodesImpl.setWbsCodeId(trackingCodes.getWbsCodeId());
        trackingCodesImpl.setDisplay(trackingCodes.isDisplay());

        return trackingCodesImpl;
    }

    /**
     * Returns the tracking codes with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the tracking codes
     * @return the tracking codes
     * @throws com.liferay.portal.NoSuchModelException if a tracking codes with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrackingCodes findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the tracking codes with the primary key or throws a {@link com.hp.omp.NoSuchTrackingCodesException} if it could not be found.
     *
     * @param trackingCodeId the primary key of the tracking codes
     * @return the tracking codes
     * @throws com.hp.omp.NoSuchTrackingCodesException if a tracking codes with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public TrackingCodes findByPrimaryKey(long trackingCodeId)
        throws NoSuchTrackingCodesException, SystemException {
        TrackingCodes trackingCodes = fetchByPrimaryKey(trackingCodeId);

        if (trackingCodes == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + trackingCodeId);
            }

            throw new NoSuchTrackingCodesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                trackingCodeId);
        }

        return trackingCodes;
    }

    /**
     * Returns the tracking codes with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the tracking codes
     * @return the tracking codes, or <code>null</code> if a tracking codes with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public TrackingCodes fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the tracking codes with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param trackingCodeId the primary key of the tracking codes
     * @return the tracking codes, or <code>null</code> if a tracking codes with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public TrackingCodes fetchByPrimaryKey(long trackingCodeId)
        throws SystemException {
        TrackingCodes trackingCodes = (TrackingCodes) EntityCacheUtil.getResult(TrackingCodesModelImpl.ENTITY_CACHE_ENABLED,
                TrackingCodesImpl.class, trackingCodeId);

        if (trackingCodes == _nullTrackingCodes) {
            return null;
        }

        if (trackingCodes == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                trackingCodes = (TrackingCodes) session.get(TrackingCodesImpl.class,
                        Long.valueOf(trackingCodeId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (trackingCodes != null) {
                    cacheResult(trackingCodes);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(TrackingCodesModelImpl.ENTITY_CACHE_ENABLED,
                        TrackingCodesImpl.class, trackingCodeId,
                        _nullTrackingCodes);
                }

                closeSession(session);
            }
        }

        return trackingCodes;
    }

    /**
     * Returns all the tracking codeses.
     *
     * @return the tracking codeses
     * @throws SystemException if a system exception occurred
     */
    public List<TrackingCodes> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the tracking codeses.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of tracking codeses
     * @param end the upper bound of the range of tracking codeses (not inclusive)
     * @return the range of tracking codeses
     * @throws SystemException if a system exception occurred
     */
    public List<TrackingCodes> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the tracking codeses.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of tracking codeses
     * @param end the upper bound of the range of tracking codeses (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of tracking codeses
     * @throws SystemException if a system exception occurred
     */
    public List<TrackingCodes> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<TrackingCodes> list = (List<TrackingCodes>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_TRACKINGCODES);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_TRACKINGCODES;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<TrackingCodes>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<TrackingCodes>) QueryUtil.list(q,
                            getDialect(), start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the tracking codeses from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (TrackingCodes trackingCodes : findAll()) {
            remove(trackingCodes);
        }
    }

    /**
     * Returns the number of tracking codeses.
     *
     * @return the number of tracking codeses
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_TRACKINGCODES);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the tracking codes persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.TrackingCodes")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<TrackingCodes>> listenersList = new ArrayList<ModelListener<TrackingCodes>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<TrackingCodes>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(TrackingCodesImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
