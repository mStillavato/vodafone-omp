/**
 * 
 */
package com.hp.omp.helper;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;

import javax.portlet.ActionRequest;
import javax.portlet.RenderRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.custom.OmpGroups;
import com.hp.omp.model.custom.OmpRoles;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserGroup;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;

/**
 * @author gewaly
 * 
 */
public class OmpHelper {
	private transient final static Logger LOGGER = LoggerFactory.getLogger(OmpHelper.class);
	public static final int PAGE_SIZE = 20;
	
	
	/**
	 * @param actionRequest
	 * @return user group
	 * @throws Exception
	 */
	public static String getUserGroup(RenderRequest request) throws Exception{
		User currentUser = PortalUtil.getUser(request);
		List<UserGroup> userGroups = getUserGroups(request);
		String gName="";
		if (userGroups == null || userGroups.isEmpty()){
			throw new Exception("User ["+currentUser.getFullName()+"] doesn't have any groups");
		}
		List<String> groupList = new ArrayList<String>();
		for (UserGroup userGroup : userGroups) {
			String groupName = userGroup.getName();
			if(isOMPGroup(groupName)){
				groupList.add(groupName);
			}
		}
		
		if(groupList.isEmpty()){
			throw new Exception("User ["+currentUser.getFullName()+"] doesn't have any OMP groups");
		}else{
			for (String group : groupList) {
				gName = group;
					if(gName.equalsIgnoreCase(OmpGroups.CONTROLLER.name()) ){
						break;
					}
					if(gName.equalsIgnoreCase(OmpGroups.OBSERVER.name()) && 
							( !groupList.contains(OmpGroups.CONTROLLER.name().toUpperCase()) && !groupList.contains(OmpGroups.CONTROLLER.name().toLowerCase()) ) ){
						break;
					}
			}
		}
		debug("user :"+currentUser.getFullName() +" Master groups is "+gName);
		return gName.toUpperCase();
	}
	
	public static OmpRoles getUserRole(Long userId) throws PortalException, SystemException{
		Company company = CompanyLocalServiceUtil.getCompanyByMx(PropsUtil.get(PropsKeys.COMPANY_DEFAULT_WEB_ID));
		Role engRole = RoleLocalServiceUtil.getRole(company.getCompanyId(), "OMP_ENG");
		Role antexRole = RoleLocalServiceUtil.getRole(company.getCompanyId(), "OMP_ANTEX");
		Role controllerRole = RoleLocalServiceUtil.getRole(company.getCompanyId(), "OMP_CONTROLLER");
		Role observerRole = RoleLocalServiceUtil.getRole(company.getCompanyId(), "OMP_OBSERVER");
		Role ndsoRole = RoleLocalServiceUtil.getRole(company.getCompanyId(), "OMP_NDSO");
		Role tcRole = RoleLocalServiceUtil.getRole(company.getCompanyId(), "OMP_TC");
		
		Role engRoleVbs = RoleLocalServiceUtil.getRole(company.getCompanyId(), "OMP_ENG_VBS");
		Role controllerRoleVbs = RoleLocalServiceUtil.getRole(company.getCompanyId(), "OMP_CONTROLLER_VBS");
		Role approverRoleVbs = RoleLocalServiceUtil.getRole(company.getCompanyId(), "OMP_APPROVER_VBS");
		
		boolean eng = UserLocalServiceUtil.hasRoleUser(engRole.getRoleId(), userId);
		boolean antex = UserLocalServiceUtil.hasRoleUser(antexRole.getRoleId(), userId);
		boolean controller = UserLocalServiceUtil.hasRoleUser(controllerRole.getRoleId(), userId);
		boolean observer = UserLocalServiceUtil.hasRoleUser(observerRole.getRoleId(), userId);
		boolean ndso = UserLocalServiceUtil.hasRoleUser(ndsoRole.getRoleId(), userId);
		boolean tc = UserLocalServiceUtil.hasRoleUser(tcRole.getRoleId(), userId);
		
		boolean engVbs = UserLocalServiceUtil.hasRoleUser(engRoleVbs.getRoleId(), userId);
		boolean controllerVbs = UserLocalServiceUtil.hasRoleUser(controllerRoleVbs.getRoleId(), userId);
		boolean approverVbs = UserLocalServiceUtil.hasRoleUser(approverRoleVbs.getRoleId(), userId);
		
		if(controller) {
			return OmpRoles.OMP_CONTROLLER;
		} else if(eng) {
			return OmpRoles.OMP_ENG;
		} else if(antex) {
			return OmpRoles.OMP_ANTEX;
		} else if (observer) {
			return OmpRoles.OMP_OBSERVER;
		}else if (ndso) {
			return OmpRoles.OMP_NDSO;
		}else if (tc) {
			return OmpRoles.OMP_TC;
		}else if (engVbs) {
			return OmpRoles.OMP_ENG_VBS;
		}else if (controllerVbs) {
			return OmpRoles.OMP_CONTROLLER_VBS;
		}else if (approverVbs) {
			return OmpRoles.OMP_APPROVER_VBS;	
		}
		return null;
	}
	
	
	public static String getUserRoles(Long userId) {
		//Assumption we have only one company in the portal.
		boolean eng = false;
		boolean controller = false;
		boolean antex = false;
		boolean observer = false;
		boolean ndso = false;
		boolean tc = false;
		
		boolean engVbs = false;
		boolean controllerVbs = false;
		boolean approverVbs = false;
		
		Company company = getDefaultCompany();
		
		if(company != null){
			Role controllerRole = getOMPRole(company, OmpRoles.OMP_CONTROLLER.name());
			Role engRole = getOMPRole(company, OmpRoles.OMP_ENG.name());
			Role antexRole = getOMPRole(company, OmpRoles.OMP_ANTEX.name());
			Role observerRole = getOMPRole(company, OmpRoles.OMP_OBSERVER.name());
			Role ndsoRole = getOMPRole(company, OmpRoles.OMP_NDSO.name());
			Role tcRole = getOMPRole(company, OmpRoles.OMP_TC.name());
			
			Role engRoleVbs = getOMPRole(company, OmpRoles.OMP_ENG_VBS.name());
			Role controllerRoleVbs = getOMPRole(company, OmpRoles.OMP_CONTROLLER_VBS.name());
			Role approverRoleVbs = getOMPRole(company, OmpRoles.OMP_APPROVER_VBS.name());
			
			if(controllerRole != null){
				controller = hasRole(userId, controllerRole.getRoleId());
			}
			if(engRole != null){
				eng = hasRole(userId, engRole.getRoleId());
			}
			if(antexRole != null){
				antex = hasRole(userId, antexRole.getRoleId());
			}
			if(observerRole != null){
				observer = hasRole(userId, observerRole.getRoleId());
			}	
			if(ndsoRole != null){
				ndso = hasRole(userId, ndsoRole.getRoleId());
			}
			if(tcRole != null){
				tc = hasRole(userId, tcRole.getRoleId());
			}
			
			if(engRoleVbs != null){
				engVbs = hasRole(userId, engRoleVbs.getRoleId());
			}
			if(controllerRoleVbs != null){
				controllerVbs = hasRole(userId, controllerRoleVbs.getRoleId());
			}
			if(approverRoleVbs != null){
				approverVbs = hasRole(userId, approverRoleVbs.getRoleId());
			}
			
			if(controller) {
				return OmpRoles.OMP_CONTROLLER.getLabel();
			} else if (eng) {
				return OmpRoles.OMP_ENG.getLabel();
			} else if (antex) {
				return OmpRoles.OMP_ANTEX.getLabel();
			} else if (observer) {
				return OmpRoles.OMP_OBSERVER.getLabel();
			} else if (ndso) {
				return OmpRoles.OMP_NDSO.getLabel();
			} else if (tc) {
				return OmpRoles.OMP_TC.getLabel();
			} else if (engVbs) {
				return OmpRoles.OMP_ENG_VBS.getLabel();
			} else if(controllerVbs) {
				return OmpRoles.OMP_CONTROLLER_VBS.getLabel();
			} else if(approverVbs) {
				return OmpRoles.OMP_APPROVER_VBS.getLabel();
			}
		}
		
		return "";
	}
	
	private static Company getDefaultCompany(){
		Company company = null;
		try {
			//Assumption we have only one company in the portal.
			company = CompanyLocalServiceUtil.getCompanyByMx(PropsUtil.get(PropsKeys.COMPANY_DEFAULT_WEB_ID));
		} catch (PortalException e) {
			LOGGER.error("PortalException, error while getting defualt company - "+e.getMessage());
		} catch (SystemException e) {
			LOGGER.error("SystemException, error while getting defualt company - "+e.getMessage());
		}
		
		return company;
	}
	private static Role getOMPRole(Company company, String roleName){
		Role role = null;
		try {
			role = RoleLocalServiceUtil.getRole(company.getCompanyId(), roleName);
		} catch (PortalException e) {
			LOGGER.error("PortalException, there is no role wih name["+roleName+"] - "+e.getMessage());
		} catch (SystemException e) {
			LOGGER.error("SystemException, there is no role wih name["+roleName+"] - "+e.getMessage());
		}
		
		return role;
	}
	
	private static boolean hasRole(long userId, long roleId){
		boolean hasRole = false;
		try {
			hasRole = RoleLocalServiceUtil.hasUserRole(userId, roleId);
		} catch (SystemException e) {
			LOGGER.error("SystemException, "+e.getMessage());
		}
		
		return hasRole;
	}

	private static boolean isOMPGroup(String groupName){
		return (groupName.equalsIgnoreCase(OmpGroups.ENG.name()) || 
				groupName.equalsIgnoreCase(OmpGroups.ANTEX.name()) || 
				groupName.equalsIgnoreCase(OmpGroups.CONTROLLER.name()) ||
				groupName.equalsIgnoreCase(OmpGroups.OBSERVER.name()) || 
				groupName.equalsIgnoreCase(OmpGroups.NDSO.name()) ||
				groupName.equalsIgnoreCase(OmpGroups.TC.name()) ||
				groupName.equalsIgnoreCase(OmpGroups.ENG_VBS.name()) ||
				groupName.equalsIgnoreCase(OmpGroups.CONTROLLER_VBS.name()) ||
				groupName.equalsIgnoreCase(OmpGroups.APPROVER_VBS.name())); 
	}	
	
	public static boolean isOMPRole(String roleName){
		return (roleName.equalsIgnoreCase(OmpRoles.OMP_ENG.name()) || 
				roleName.equalsIgnoreCase(OmpRoles.OMP_ANTEX.name()) || 
				roleName.equalsIgnoreCase(OmpRoles.OMP_CONTROLLER.name()) || 
				roleName.equalsIgnoreCase(OmpRoles.OMP_OBSERVER.name()) || 
				roleName.equalsIgnoreCase(OmpRoles.OMP_NDSO.name()) ||
				roleName.equalsIgnoreCase(OmpRoles.OMP_TC.name()) ||
				roleName.equalsIgnoreCase(OmpRoles.OMP_ENG_VBS.name()) ||
				roleName.equalsIgnoreCase(OmpRoles.OMP_CONTROLLER_VBS.name()) ||
				roleName.equalsIgnoreCase(OmpRoles.OMP_APPROVER_VBS.name())); 
	}
	
	public static List<UserGroup> getUserGroups(RenderRequest request) throws Exception{
		 User currentUser = PortalUtil.getUser(request);
		 if(currentUser == null){
			 throw new Exception("there is no logged in user");
		 }
		 debug("user :"+currentUser.getFullName() +" have "+currentUser.getUserGroups().size()+" Groups");
			
		 return currentUser.getUserGroups();
	}
	
	public static List<Role> getUserRoles(ActionRequest actionRequest) throws Exception{
		 User currentUser = PortalUtil.getUser(actionRequest);
		 if(currentUser == null){
			 throw new Exception("there is no logged in user");
		 }
		
		 debug("user :"+currentUser.getFullName() +" have "+currentUser.getRoles().size()+" Roles");
		 
		 return currentUser.getRoles();
	}
	

	public static boolean isController(Long userId){
		return (OmpRoles.OMP_CONTROLLER.getLabel().equals(getUserRoles(userId)));
	}
	public static boolean isObserver(Long userId){
		return (OmpRoles.OMP_OBSERVER.getLabel().equals(getUserRoles(userId)));
	}
	public static boolean isEng(Long userId){
		return (OmpRoles.OMP_ENG.getLabel().equals(getUserRoles(userId)));
	}
	public static boolean isAntex(Long userId){
		return (OmpRoles.OMP_ANTEX.getLabel().equals(getUserRoles(userId)));
	}
	public static boolean isNdso(Long userId){
		return (OmpRoles.OMP_NDSO.getLabel().equals(getUserRoles(userId)));
	}
	public static boolean isTc(Long userId){
		return (OmpRoles.OMP_TC.getLabel().equals(getUserRoles(userId)));
	}
	
	public static boolean isEngVbs(Long userId){
		return (OmpRoles.OMP_ENG_VBS.getLabel().equals(getUserRoles(userId)));
	}
	public static boolean isControllerVbs(Long userId){
		return (OmpRoles.OMP_CONTROLLER_VBS.getLabel().equals(getUserRoles(userId)));
	}
	public static boolean isApproverVbs(Long userId){
		return (OmpRoles.OMP_APPROVER_VBS.getLabel().equals(getUserRoles(userId)));
	}
	
	public static int getFirstYearInFiscalYear(String value) {
		try {
			StringTokenizer st = new StringTokenizer(value, "/");
			if (st.hasMoreTokens()) {
				return Integer.parseInt(st.nextToken());
			}
			return 0;
		} catch (Exception e) {
			LOGGER.error("can't parse fiscal year, "+e);
			return 0;
		}
	}
	
	public static String calculateFiscalYear(Date requiredDate) {
		String fiscalYear="";
		Calendar myDate = new GregorianCalendar();
		myDate.setTime(requiredDate);
		Calendar compareDate = new GregorianCalendar(myDate.get(Calendar.YEAR),3, 1);
		if (myDate.before(compareDate)) {
			fiscalYear = (myDate.get(Calendar.YEAR) - 1) + "/"	+ myDate.get(Calendar.YEAR);
		} else {
			fiscalYear = myDate.get(Calendar.YEAR) + "/" + (myDate.get(Calendar.YEAR) + 1);
		}
		return fiscalYear;
	}
	
	public static String formatNumberByLocale(double number,Locale locale) {
		NumberFormat nf = NumberFormat.getInstance(locale);
		nf.setMaximumFractionDigits(2);
		return nf.format(number);
	}
	
	

	/**
	 * @param msg
	 */
	private static void debug(String msg){
		 if(LOGGER.isDebugEnabled()){
			 LOGGER.debug(msg);
		 }
	}
	
}
