package com.hp.omp.service.persistence;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.hp.omp.model.custom.GoodReceiptListDTO;
import com.hp.omp.model.custom.ListDTO;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class GoodReceiptFinderImpl extends GoodReceiptPersistenceImpl implements GoodReceiptFinder{
	
	private static Log _log = LogFactoryUtil.getLog(GoodReceiptFinderImpl.class);
	
	private static final String GET_GR_REQUESTED = GoodReceiptFinderImpl.class.getName()+".getGRRequested";
	private static final String GET_GR_REQUESTED_COUNT = GoodReceiptFinderImpl.class.getName()+".getGRRequestedCount";
	private static final String COST_CENTER_CLAUSE = GoodReceiptFinderImpl.class.getName()+".costCenterClause";
	
	public Long getOpenedGoodReceiptByCostCenterCount(long costCenterId) throws Exception{
		
		_log.info("INFO: public Long getOpenedGoodReceiptByCostCenterCount() - Start");
		
		Session session = null;
		Long count = 0L;
		try {
			session = openSession();
			String hql = CustomSQLUtil.get(GET_GR_REQUESTED_COUNT);
			hql = hql +" "+CustomSQLUtil.get(COST_CENTER_CLAUSE);
			Query query = session.createQuery(hql);
			query.setLong("costCenterId", costCenterId);
			@SuppressWarnings("unchecked")
			List<Object> list = (List<Object>)QueryUtil.list(query, getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			count = (Long)list.get(0); 			
		} catch (Exception e) {
			_log.error(e.getMessage());
		}finally {
			closeSession(session);
			_log.info("INFO: public Long getOpenedGoodReceiptByCostCenterCount() - End");
		}
		return count;
	}

	
	public Double getSumCompeleteGRForPO(long purchaseOrderId, boolean isClosed) {

		_log.info("INFO: public Double getSumCompeleteGRForPO() - Start");
		
		Session session = null;
		try {
			session = openSession();
			StringBuilder sb = new StringBuilder ("select sum(GR.grvalue) from Good_Receipt GR, position p, purchase_order po "+
					" where po.purchaseorderId=? and po.purchaseorderId = p.purchaseorderId and gr.positionId = p.positionId ");
			if(isClosed) {
				sb.append(" and GR.REGISTERATIONDATE is not null and GR.goodReceiptNumber is not null ");
			} 
			_log.info(sb.toString());
			SQLQuery query = session.createSQLQuery(sb.toString());
			query.setString(0, String.valueOf(purchaseOrderId));
			@SuppressWarnings("unchecked")
			List<BigDecimal> list = query.list();
			if(list != null && list.size()>0) {
				return list.get(0) == null? 0L:list.get(0).doubleValue();
			}
			
			return 0d;
		}finally {
			closeSession(session);
			_log.info("INFO: public Double getSumCompeleteGRForPO() - End");
		}
	}
	
	
	public Double getSumCompeleteGRForPR(long purchaseRequestId) {
		
		_log.info("INFO: public Double getSumCompeleteGRForPR() - Start");
		
		Session session = null;
		try {
			session = openSession();
			StringBuilder sb = new StringBuilder ("select sum(GR.grvalue) from Good_Receipt GR, position p, purchase_request pr "+
					" where pr.purchaserequestId=? and pr.purchaserequestId = p.purchaserequestId and gr.positionId = p.positionId " +
					" and GR.REGISTERATIONDATE is not null and GR.goodReceiptNumber is not null ");

			SQLQuery query = session.createSQLQuery(sb.toString());
			query.setString(0, String.valueOf(purchaseRequestId));
			@SuppressWarnings("unchecked")
			List<BigDecimal> list = query.list();
			if(list != null && list.size()>0) {
				return list.get(0) == null? 0L:list.get(0).doubleValue();
			}
			return 0d;
		}finally {
			closeSession(session);
			_log.info("INFO: public Double getSumCompeleteGRForPR() - End");
		}
	}
	
	
	public Double getSumValuePositionForPO(long purchaseOrderId) {
		
		_log.info("INFO: public Double getSumValuePositionForPO() - Start");
		
		Session session = null;
		try {
			session = openSession();
			StringBuilder sb = new StringBuilder ("select sum(p.quatity*p.actualUnitCost) from  position p, purchase_order po "+
					" where po.purchaseorderId=? and po.purchaseorderId = p.purchaseorderId ");
			 
			SQLQuery query = session.createSQLQuery(sb.toString());
			query.setString(0, String.valueOf(purchaseOrderId));
			@SuppressWarnings("unchecked")
			List<BigDecimal> list = query.list();
			if(list != null && list.size()>0) {
				return list.get(0) == null? 0L:list.get(0).doubleValue();
			}
			return 0d;
		}finally {
			closeSession(session);
			_log.info("INFO: public Double getSumValuePositionForPO() - End");
		}
	}
	
	
	public Double getSumValuePositionForPR(long purchaseRequestId) {
		
		_log.info("INFO: public Double getSumValuePositionForPR() - Start");
		
		Session session = null;
		try {
			session = openSession();
			StringBuilder sb = new StringBuilder ("select sum(p.quatity*p.actualUnitCost) from  position p, purchase_request pr "+
					" where pr.purchaserequestId=? and pr.purchaserequestId = p.purchaserequestId ");
			
			SQLQuery query = session.createSQLQuery(sb.toString());
			query.setString(0, String.valueOf(purchaseRequestId));
			@SuppressWarnings("unchecked")
			List<BigDecimal> list = query.list();
			if(list != null && list.size()>0) {
				return list.get(0) == null? 0L:list.get(0).doubleValue();
			}
			return 0d;
		}finally {
			closeSession(session);
			_log.info("INFO: public Double getSumValuePositionForPR() - End");
		}
	}
	
	public Long getOpenedGoodReceiptCount() throws Exception{
		
		_log.info("INFO: public Long getOpenedGoodReceiptCount() - Start");
		
		Session session = null;
		Long count = 0L;
		try {
			session = openSession();
			String hql = CustomSQLUtil.get(GET_GR_REQUESTED_COUNT);
			Query query = session.createQuery(hql);
			@SuppressWarnings("unchecked")
			List<Object> list = (List<Object>) QueryUtil.list(query,
					getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			count = (Long) list.get(0);
		} catch (Exception e) {
			_log.error(e.getMessage());
		} finally {
			closeSession(session);
			_log.info("INFO: public Long getOpenedGoodReceiptCount() - End");
		}
		return count;
	}
	
	public List<ListDTO> getOpenedGoodReceiptByCostCenter(long costCenterId, int start, int end) throws Exception{
		
		_log.info("INFO: public List<ListDTO> getOpenedGoodReceiptByCostCenter() - Start");
		
		Session session = null;
		List<ListDTO> result = null;
		try {
			session = openSession();
			String hql = CustomSQLUtil.get(GET_GR_REQUESTED);
			hql = hql+" "+CustomSQLUtil.get(COST_CENTER_CLAUSE);
			Query query = session.createQuery(hql);
			query.setLong("costCenterId", costCenterId);
			result = new ArrayList<ListDTO>();
			@SuppressWarnings("unchecked")
			List<Object[]> list = (List<Object[]>)QueryUtil.list(query,getDialect(), start, end);
			for (Object[] objects : list) {
				ListDTO dto = getGoodReceiptListDTO(objects);
				result.add(dto);
			}
			return result;
		} catch (Exception e) {
			_log.error(e.getMessage());
			throw e;
		} finally {
			closeSession(session);
			_log.info("INFO: public List<ListDTO> getOpenedGoodReceiptByCostCenter() - End");
		}
	}
	
	public List<ListDTO> getOpenedGoodReceipt(int start, int end) throws Exception{
		
		_log.info("INFO: public List<ListDTO> getOpenedGoodReceipt() - Start");
		
		Session session = null;
		List<ListDTO> result = null;
		try {
			session = openSession();
			String hql = CustomSQLUtil.get(GET_GR_REQUESTED);
			Query query = session.createQuery(hql);
			result = new ArrayList<ListDTO>();
			@SuppressWarnings("unchecked")
			List<Object[]> list = (List<Object[]>)QueryUtil.list(query,getDialect(), start, end);
			for (Object[] objects : list) {
				ListDTO dto = getGoodReceiptListDTO(objects);
				result.add(dto);
			}
			return result;
		} catch (Exception e) {
			_log.error(e.getMessage());
			throw e;
		} finally {
			closeSession(session);
			_log.info("INFO: public List<ListDTO> getOpenedGoodReceipt() - End");
		}
	}
	
	private ListDTO getGoodReceiptListDTO(Object[] objects) {
		
		_log.info("INFO: private ListDTO getGoodReceiptListDTO() - Start");
		
		ListDTO grDTO = new GoodReceiptListDTO();
		
		grDTO.setIdentifier(getString(objects[0]));
		
		if(objects[1] != null){
			try {
				Long userId = (Long) objects[1];
				User user = UserLocalServiceUtil.getUserById(userId);
				grDTO.setRequestorID(userId);
				grDTO.setRequestorName(user.getScreenName());
			} catch (Exception e) {
				debug("Cannor get User: "+objects[1]);
				debug(e.getMessage());
			} 
		}
		
		grDTO.setVendor(getString(objects[2]));		
		grDTO.setSubCategory(getString(objects[3]));
		grDTO.setTotalValue(getString(objects[4]));
		grDTO.setFiscalYear(getString(objects[5]));
		grDTO.setDescription(getString(objects[6]));
		if(objects[7] != null &&  objects[8] != null){
			grDTO.setStatus("Closed");
		}else{
			grDTO.setStatus("Requested");
		}
		if(objects[9] != null){
			try {
				Integer currency = Integer.parseInt(getString(objects[9]));
				grDTO.setCurrency(currency);
			} catch (NumberFormatException e) {
				debug(e.getMessage());
			}
		}
		
		_log.info("INFO: private ListDTO getGoodReceiptListDTO() - End");
		
		return grDTO;
	}
	
	private String getString(Object object){
		if(object == null){
			return null;
		}else{
			return object.toString();
		}
	}

	private static void debug(Object msg){
		if(_log.isDebugEnabled()){
			_log.debug(msg);
		}
	}
	
}
