/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchCatalogoException;
import com.hp.omp.model.Catalogo;
import com.hp.omp.model.impl.CatalogoImpl;
import com.hp.omp.model.impl.CatalogoModelImpl;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the catalogo service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see CatalogoPersistence
 * @see CatalogoUtil
 * @generated
 */
public class CatalogoPersistenceImpl extends BasePersistenceImpl<Catalogo>
	implements CatalogoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link CatalogoUtil} to access the catalogo persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = CatalogoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_CATALOGOVENDOR =
		new FinderPath(CatalogoModelImpl.ENTITY_CACHE_ENABLED,
			CatalogoModelImpl.FINDER_CACHE_ENABLED, CatalogoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByCatalogoVendor",
			new String[] {
				String.class.getName(),
				
			"java.lang.Integer", "java.lang.Integer",
				"com.liferay.portal.kernel.util.OrderByComparator"
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATALOGOVENDOR =
		new FinderPath(CatalogoModelImpl.ENTITY_CACHE_ENABLED,
			CatalogoModelImpl.FINDER_CACHE_ENABLED, CatalogoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByCatalogoVendor",
			new String[] { String.class.getName() },
			CatalogoModelImpl.FORNITOREDESC_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_CATALOGOVENDOR = new FinderPath(CatalogoModelImpl.ENTITY_CACHE_ENABLED,
			CatalogoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCatalogoVendor",
			new String[] { String.class.getName() });
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CatalogoModelImpl.ENTITY_CACHE_ENABLED,
			CatalogoModelImpl.FINDER_CACHE_ENABLED, CatalogoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CatalogoModelImpl.ENTITY_CACHE_ENABLED,
			CatalogoModelImpl.FINDER_CACHE_ENABLED, CatalogoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CatalogoModelImpl.ENTITY_CACHE_ENABLED,
			CatalogoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the catalogo in the entity cache if it is enabled.
	 *
	 * @param catalogo the catalogo
	 */
	public void cacheResult(Catalogo catalogo) {
		EntityCacheUtil.putResult(CatalogoModelImpl.ENTITY_CACHE_ENABLED,
			CatalogoImpl.class, catalogo.getPrimaryKey(), catalogo);

		catalogo.resetOriginalValues();
	}

	/**
	 * Caches the catalogos in the entity cache if it is enabled.
	 *
	 * @param catalogos the catalogos
	 */
	public void cacheResult(List<Catalogo> catalogos) {
		for (Catalogo catalogo : catalogos) {
			if (EntityCacheUtil.getResult(
						CatalogoModelImpl.ENTITY_CACHE_ENABLED,
						CatalogoImpl.class, catalogo.getPrimaryKey()) == null) {
				cacheResult(catalogo);
			}
			else {
				catalogo.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all catalogos.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(CatalogoImpl.class.getName());
		}

		EntityCacheUtil.clearCache(CatalogoImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the catalogo.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Catalogo catalogo) {
		EntityCacheUtil.removeResult(CatalogoModelImpl.ENTITY_CACHE_ENABLED,
			CatalogoImpl.class, catalogo.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Catalogo> catalogos) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Catalogo catalogo : catalogos) {
			EntityCacheUtil.removeResult(CatalogoModelImpl.ENTITY_CACHE_ENABLED,
				CatalogoImpl.class, catalogo.getPrimaryKey());
		}
	}

	/**
	 * Creates a new catalogo with the primary key. Does not add the catalogo to the database.
	 *
	 * @param catalogoId the primary key for the new catalogo
	 * @return the new catalogo
	 */
	public Catalogo create(long catalogoId) {
		Catalogo catalogo = new CatalogoImpl();

		catalogo.setNew(true);
		catalogo.setPrimaryKey(catalogoId);

		return catalogo;
	}

	/**
	 * Removes the catalogo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param catalogoId the primary key of the catalogo
	 * @return the catalogo that was removed
	 * @throws com.hp.omp.NoSuchCatalogoException if a catalogo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Catalogo remove(long catalogoId)
		throws NoSuchCatalogoException, SystemException {
		return remove(Long.valueOf(catalogoId));
	}

	/**
	 * Removes the catalogo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the catalogo
	 * @return the catalogo that was removed
	 * @throws com.hp.omp.NoSuchCatalogoException if a catalogo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalogo remove(Serializable primaryKey)
		throws NoSuchCatalogoException, SystemException {
		Session session = null;

		try {
			session = openSession();

			Catalogo catalogo = (Catalogo)session.get(CatalogoImpl.class,
					primaryKey);

			if (catalogo == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchCatalogoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(catalogo);
		}
		catch (NoSuchCatalogoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Catalogo removeImpl(Catalogo catalogo) throws SystemException {
		catalogo = toUnwrappedModel(catalogo);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, catalogo);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(catalogo);

		return catalogo;
	}

	@Override
	public Catalogo updateImpl(com.hp.omp.model.Catalogo catalogo, boolean merge)
		throws SystemException {
		catalogo = toUnwrappedModel(catalogo);

		boolean isNew = catalogo.isNew();

		CatalogoModelImpl catalogoModelImpl = (CatalogoModelImpl)catalogo;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, catalogo, merge);

			catalogo.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !CatalogoModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		else {
			if ((catalogoModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATALOGOVENDOR.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						catalogoModelImpl.getOriginalFornitoreDesc()
					};

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CATALOGOVENDOR,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATALOGOVENDOR,
					args);

				args = new Object[] { catalogoModelImpl.getFornitoreDesc() };

				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CATALOGOVENDOR,
					args);
				FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATALOGOVENDOR,
					args);
			}
		}

		EntityCacheUtil.putResult(CatalogoModelImpl.ENTITY_CACHE_ENABLED,
			CatalogoImpl.class, catalogo.getPrimaryKey(), catalogo);

		return catalogo;
	}

	protected Catalogo toUnwrappedModel(Catalogo catalogo) {
		if (catalogo instanceof CatalogoImpl) {
			return catalogo;
		}

		CatalogoImpl catalogoImpl = new CatalogoImpl();

		catalogoImpl.setNew(catalogo.isNew());
		catalogoImpl.setPrimaryKey(catalogo.getPrimaryKey());

		catalogoImpl.setCatalogoId(catalogo.getCatalogoId());
		catalogoImpl.setChiaveInforecord(catalogo.getChiaveInforecord());
		catalogoImpl.setDataCreazione(catalogo.getDataCreazione());
		catalogoImpl.setDataFineValidita(catalogo.getDataFineValidita());
		catalogoImpl.setDivisione(catalogo.getDivisione());
		catalogoImpl.setFlgCanc(catalogo.isFlgCanc());
		catalogoImpl.setFornFinCode(catalogo.getFornFinCode());
		catalogoImpl.setFornitoreCode(catalogo.getFornitoreCode());
		catalogoImpl.setFornitoreDesc(catalogo.getFornitoreDesc());
		catalogoImpl.setMatCatLvl2(catalogo.getMatCatLvl2());
		catalogoImpl.setMatCatLvl2Desc(catalogo.getMatCatLvl2Desc());
		catalogoImpl.setMatCatLvl4(catalogo.getMatCatLvl4());
		catalogoImpl.setMatCatLvl4Desc(catalogo.getMatCatLvl4Desc());
		catalogoImpl.setMatCod(catalogo.getMatCod());
		catalogoImpl.setMatCodFornFinale(catalogo.getMatCodFornFinale());
		catalogoImpl.setMatDesc(catalogo.getMatDesc());
		catalogoImpl.setPrezzo(catalogo.getPrezzo());
		catalogoImpl.setPurchOrganiz(catalogo.getPurchOrganiz());
		catalogoImpl.setUm(catalogo.getUm());
		catalogoImpl.setUmPrezzo(catalogo.getUmPrezzo());
		catalogoImpl.setUmPrezzoPo(catalogo.getUmPrezzoPo());
		catalogoImpl.setValuta(catalogo.getValuta());
		catalogoImpl.setUpperMatDesc(catalogo.getUpperMatDesc());

		return catalogoImpl;
	}

	/**
	 * Returns the catalogo with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the catalogo
	 * @return the catalogo
	 * @throws com.liferay.portal.NoSuchModelException if a catalogo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalogo findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the catalogo with the primary key or throws a {@link com.hp.omp.NoSuchCatalogoException} if it could not be found.
	 *
	 * @param catalogoId the primary key of the catalogo
	 * @return the catalogo
	 * @throws com.hp.omp.NoSuchCatalogoException if a catalogo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Catalogo findByPrimaryKey(long catalogoId)
		throws NoSuchCatalogoException, SystemException {
		Catalogo catalogo = fetchByPrimaryKey(catalogoId);

		if (catalogo == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + catalogoId);
			}

			throw new NoSuchCatalogoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				catalogoId);
		}

		return catalogo;
	}

	/**
	 * Returns the catalogo with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the catalogo
	 * @return the catalogo, or <code>null</code> if a catalogo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public Catalogo fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the catalogo with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param catalogoId the primary key of the catalogo
	 * @return the catalogo, or <code>null</code> if a catalogo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Catalogo fetchByPrimaryKey(long catalogoId)
		throws SystemException {
		Catalogo catalogo = (Catalogo)EntityCacheUtil.getResult(CatalogoModelImpl.ENTITY_CACHE_ENABLED,
				CatalogoImpl.class, catalogoId);

		if (catalogo == _nullCatalogo) {
			return null;
		}

		if (catalogo == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				catalogo = (Catalogo)session.get(CatalogoImpl.class,
						Long.valueOf(catalogoId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (catalogo != null) {
					cacheResult(catalogo);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(CatalogoModelImpl.ENTITY_CACHE_ENABLED,
						CatalogoImpl.class, catalogoId, _nullCatalogo);
				}

				closeSession(session);
			}
		}

		return catalogo;
	}

	/**
	 * Returns all the catalogos where fornitoreDesc = &#63;.
	 *
	 * @param fornitoreDesc the fornitore desc
	 * @return the matching catalogos
	 * @throws SystemException if a system exception occurred
	 */
	public List<Catalogo> findByCatalogoVendor(String fornitoreDesc)
		throws SystemException {
		return findByCatalogoVendor(fornitoreDesc, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the catalogos where fornitoreDesc = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param fornitoreDesc the fornitore desc
	 * @param start the lower bound of the range of catalogos
	 * @param end the upper bound of the range of catalogos (not inclusive)
	 * @return the range of matching catalogos
	 * @throws SystemException if a system exception occurred
	 */
	public List<Catalogo> findByCatalogoVendor(String fornitoreDesc, int start,
		int end) throws SystemException {
		return findByCatalogoVendor(fornitoreDesc, start, end, null);
	}

	/**
	 * Returns an ordered range of all the catalogos where fornitoreDesc = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param fornitoreDesc the fornitore desc
	 * @param start the lower bound of the range of catalogos
	 * @param end the upper bound of the range of catalogos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching catalogos
	 * @throws SystemException if a system exception occurred
	 */
	public List<Catalogo> findByCatalogoVendor(String fornitoreDesc, int start,
		int end, OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_CATALOGOVENDOR;
			finderArgs = new Object[] { fornitoreDesc };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_CATALOGOVENDOR;
			finderArgs = new Object[] {
					fornitoreDesc,
					
					start, end, orderByComparator
				};
		}

		List<Catalogo> list = (List<Catalogo>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if ((list != null) && !list.isEmpty()) {
			for (Catalogo catalogo : list) {
				if (!Validator.equals(fornitoreDesc, catalogo.getFornitoreDesc())) {
					list = null;

					break;
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 3));
			}
			else {
				query = new StringBundler(2);
			}

			query.append(_SQL_SELECT_CATALOGO_WHERE);

			if (fornitoreDesc == null) {
				query.append(_FINDER_COLUMN_CATALOGOVENDOR_FORNITOREDESC_1);
			}
			else {
				if (fornitoreDesc.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_CATALOGOVENDOR_FORNITOREDESC_3);
				}
				else {
					query.append(_FINDER_COLUMN_CATALOGOVENDOR_FORNITOREDESC_2);
				}
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (fornitoreDesc != null) {
					qPos.add(fornitoreDesc);
				}

				list = (List<Catalogo>)QueryUtil.list(q, getDialect(), start,
						end);
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first catalogo in the ordered set where fornitoreDesc = &#63;.
	 *
	 * @param fornitoreDesc the fornitore desc
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching catalogo
	 * @throws com.hp.omp.NoSuchCatalogoException if a matching catalogo could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Catalogo findByCatalogoVendor_First(String fornitoreDesc,
		OrderByComparator orderByComparator)
		throws NoSuchCatalogoException, SystemException {
		Catalogo catalogo = fetchByCatalogoVendor_First(fornitoreDesc,
				orderByComparator);

		if (catalogo != null) {
			return catalogo;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("fornitoreDesc=");
		msg.append(fornitoreDesc);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCatalogoException(msg.toString());
	}

	/**
	 * Returns the first catalogo in the ordered set where fornitoreDesc = &#63;.
	 *
	 * @param fornitoreDesc the fornitore desc
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching catalogo, or <code>null</code> if a matching catalogo could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Catalogo fetchByCatalogoVendor_First(String fornitoreDesc,
		OrderByComparator orderByComparator) throws SystemException {
		List<Catalogo> list = findByCatalogoVendor(fornitoreDesc, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last catalogo in the ordered set where fornitoreDesc = &#63;.
	 *
	 * @param fornitoreDesc the fornitore desc
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching catalogo
	 * @throws com.hp.omp.NoSuchCatalogoException if a matching catalogo could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Catalogo findByCatalogoVendor_Last(String fornitoreDesc,
		OrderByComparator orderByComparator)
		throws NoSuchCatalogoException, SystemException {
		Catalogo catalogo = fetchByCatalogoVendor_Last(fornitoreDesc,
				orderByComparator);

		if (catalogo != null) {
			return catalogo;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("fornitoreDesc=");
		msg.append(fornitoreDesc);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchCatalogoException(msg.toString());
	}

	/**
	 * Returns the last catalogo in the ordered set where fornitoreDesc = &#63;.
	 *
	 * @param fornitoreDesc the fornitore desc
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching catalogo, or <code>null</code> if a matching catalogo could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Catalogo fetchByCatalogoVendor_Last(String fornitoreDesc,
		OrderByComparator orderByComparator) throws SystemException {
		int count = countByCatalogoVendor(fornitoreDesc);

		List<Catalogo> list = findByCatalogoVendor(fornitoreDesc, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the catalogos before and after the current catalogo in the ordered set where fornitoreDesc = &#63;.
	 *
	 * @param catalogoId the primary key of the current catalogo
	 * @param fornitoreDesc the fornitore desc
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next catalogo
	 * @throws com.hp.omp.NoSuchCatalogoException if a catalogo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public Catalogo[] findByCatalogoVendor_PrevAndNext(long catalogoId,
		String fornitoreDesc, OrderByComparator orderByComparator)
		throws NoSuchCatalogoException, SystemException {
		Catalogo catalogo = findByPrimaryKey(catalogoId);

		Session session = null;

		try {
			session = openSession();

			Catalogo[] array = new CatalogoImpl[3];

			array[0] = getByCatalogoVendor_PrevAndNext(session, catalogo,
					fornitoreDesc, orderByComparator, true);

			array[1] = catalogo;

			array[2] = getByCatalogoVendor_PrevAndNext(session, catalogo,
					fornitoreDesc, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Catalogo getByCatalogoVendor_PrevAndNext(Session session,
		Catalogo catalogo, String fornitoreDesc,
		OrderByComparator orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(6 +
					(orderByComparator.getOrderByFields().length * 6));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CATALOGO_WHERE);

		if (fornitoreDesc == null) {
			query.append(_FINDER_COLUMN_CATALOGOVENDOR_FORNITOREDESC_1);
		}
		else {
			if (fornitoreDesc.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_CATALOGOVENDOR_FORNITOREDESC_3);
			}
			else {
				query.append(_FINDER_COLUMN_CATALOGOVENDOR_FORNITOREDESC_2);
			}
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (fornitoreDesc != null) {
			qPos.add(fornitoreDesc);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(catalogo);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Catalogo> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Returns all the catalogos.
	 *
	 * @return the catalogos
	 * @throws SystemException if a system exception occurred
	 */
	public List<Catalogo> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the catalogos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of catalogos
	 * @param end the upper bound of the range of catalogos (not inclusive)
	 * @return the range of catalogos
	 * @throws SystemException if a system exception occurred
	 */
	public List<Catalogo> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the catalogos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of catalogos
	 * @param end the upper bound of the range of catalogos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of catalogos
	 * @throws SystemException if a system exception occurred
	 */
	public List<Catalogo> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Catalogo> list = (List<Catalogo>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CATALOGO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CATALOGO;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<Catalogo>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<Catalogo>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the catalogos where fornitoreDesc = &#63; from the database.
	 *
	 * @param fornitoreDesc the fornitore desc
	 * @throws SystemException if a system exception occurred
	 */
	public void removeByCatalogoVendor(String fornitoreDesc)
		throws SystemException {
		for (Catalogo catalogo : findByCatalogoVendor(fornitoreDesc)) {
			remove(catalogo);
		}
	}

	/**
	 * Removes all the catalogos from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (Catalogo catalogo : findAll()) {
			remove(catalogo);
		}
	}

	/**
	 * Returns the number of catalogos where fornitoreDesc = &#63;.
	 *
	 * @param fornitoreDesc the fornitore desc
	 * @return the number of matching catalogos
	 * @throws SystemException if a system exception occurred
	 */
	public int countByCatalogoVendor(String fornitoreDesc)
		throws SystemException {
		Object[] finderArgs = new Object[] { fornitoreDesc };

		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_CATALOGOVENDOR,
				finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CATALOGO_WHERE);

			if (fornitoreDesc == null) {
				query.append(_FINDER_COLUMN_CATALOGOVENDOR_FORNITOREDESC_1);
			}
			else {
				if (fornitoreDesc.equals(StringPool.BLANK)) {
					query.append(_FINDER_COLUMN_CATALOGOVENDOR_FORNITOREDESC_3);
				}
				else {
					query.append(_FINDER_COLUMN_CATALOGOVENDOR_FORNITOREDESC_2);
				}
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (fornitoreDesc != null) {
					qPos.add(fornitoreDesc);
				}

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CATALOGOVENDOR,
					finderArgs, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Returns the number of catalogos.
	 *
	 * @return the number of catalogos
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CATALOGO);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the catalogo persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.hp.omp.model.Catalogo")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<Catalogo>> listenersList = new ArrayList<ModelListener<Catalogo>>();

				for (String listenerClassName : listenerClassNames) {
					Class<?> clazz = getClass();

					listenersList.add((ModelListener<Catalogo>)InstanceFactory.newInstance(
							clazz.getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(CatalogoImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = CatalogoPersistence.class)
	protected CatalogoPersistence catalogoPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_CATALOGO = "SELECT catalogo FROM Catalogo catalogo";
	private static final String _SQL_SELECT_CATALOGO_WHERE = "SELECT catalogo FROM Catalogo catalogo WHERE ";
	private static final String _SQL_COUNT_CATALOGO = "SELECT COUNT(catalogo) FROM Catalogo catalogo";
	private static final String _SQL_COUNT_CATALOGO_WHERE = "SELECT COUNT(catalogo) FROM Catalogo catalogo WHERE ";
	private static final String _FINDER_COLUMN_CATALOGOVENDOR_FORNITOREDESC_1 = "catalogo.fornitoreDesc IS NULL";
	private static final String _FINDER_COLUMN_CATALOGOVENDOR_FORNITOREDESC_2 = "catalogo.fornitoreDesc = ?";
	private static final String _FINDER_COLUMN_CATALOGOVENDOR_FORNITOREDESC_3 = "(catalogo.fornitoreDesc IS NULL OR catalogo.fornitoreDesc = ?)";
	private static final String _ORDER_BY_ENTITY_ALIAS = "catalogo.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Catalogo exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Catalogo exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(CatalogoPersistenceImpl.class);
	private static Catalogo _nullCatalogo = new CatalogoImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<Catalogo> toCacheModel() {
				return _nullCatalogoCacheModel;
			}
		};

	private static CacheModel<Catalogo> _nullCatalogoCacheModel = new CacheModel<Catalogo>() {
			public Catalogo toEntityModel() {
				return _nullCatalogo;
			}
		};
}