package com.hp.omp.portlet;

import java.io.IOException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.model.CostCenter;
import com.hp.omp.model.CostCenterUser;
import com.hp.omp.model.custom.GruppoUsers;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.impl.CostCenterImpl;
import com.hp.omp.service.CostCenterLocalServiceUtil;
import com.hp.omp.service.CostCenterUserLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class CostCenterAdminPortlet
 */
public class ProfitCenterAdminPortlet extends MVCPortlet {

	private transient Logger logger = LoggerFactory.getLogger(ProfitCenterAdminPortlet.class);
	private String errorMSG = "";
	private String successMSG = "";
	private String ACTION_PARAM= "action";
	private String ADD_ACTION= "add";
	private String UPDATE_ACTION= "update";
	private String DELETE_ACTION= "delete";
	private String PAGINATION_ACTION= "pagination";
	
	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		super.doView(renderRequest, renderResponse);
	}
	
	/* (non-Javadoc)
	 * @see javax.portlet.GenericPortlet#render(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		viewAddCostCenter(request, response);
		super.render(request, response);
	}
	
	public void viewAddCostCenter(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		String action = renderRequest.getParameter(ACTION_PARAM);
		
		if(ADD_ACTION.equals(action)){
			renderRequest.setAttribute(ACTION_PARAM, ADD_ACTION);
			renderRequest.setAttribute("display",true);
		}else if(UPDATE_ACTION.equals(action)){
			String costCenterId = renderRequest.getParameter("costCenterId");
			CostCenter costCenter = getCostCenter(costCenterId);
			
			logger.debug("render update action for : "+costCenter.getCostCenterName());
			logger.debug("Display = "+costCenter.getDisplay());
			
			renderRequest.setAttribute(ACTION_PARAM, UPDATE_ACTION);
			renderRequest.setAttribute("costCenterId", costCenter.getCostCenterId());
			renderRequest.setAttribute("costCenterName", costCenter.getCostCenterName());
			renderRequest.setAttribute("costCenterDesc", costCenter.getDescription());
			renderRequest.setAttribute("display", costCenter.getDisplay());
		}
	}
	
	private void retrieveCostCenterListToRequest(PortletRequest renderRequest) throws SystemException{
		try {
			String searchFilter = ParamUtil.getString(renderRequest, "searchFilter","");

			int totalCount = CostCenterLocalServiceUtil.getCostCentersCount();

			Integer pageNumber = ParamUtil.getInteger(renderRequest, "pageNumber", 1);
			Integer pageSize = OmpHelper.PAGE_SIZE;
			int start = (pageNumber - 1) * pageSize;
			int end = start + pageSize;

			renderRequest.setAttribute("numberOfPages", getTotalNumberOfPages(totalCount, pageSize));
			renderRequest.setAttribute("pageNumber", pageNumber);

			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userType = OmpHelper.getUserRoles(themeDisplay.getUserId());
    		int gruppoUser = GruppoUsers.GNED.getCode();
    		if(userType.equals(OmpRoles.OMP_CONTROLLER_VBS.getLabel())
    				|| userType.equals(OmpRoles.OMP_APPROVER_VBS.getLabel())
    				|| userType.equals(OmpRoles.OMP_ENG_VBS.getLabel())){
    			gruppoUser = GruppoUsers.VBS.getCode();
    		}
			List<CostCenter> costCenterList = CostCenterLocalServiceUtil.getCostCentersList(start, end, searchFilter, gruppoUser);
			renderRequest.setAttribute("costCenterList", costCenterList);
			
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			errorMSG = "can not retrieve cost center list";
			renderRequest.setAttribute("errorMSG", errorMSG);
		}

	}

	
	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#processAction(javax.portlet.ActionRequest, javax.portlet.ActionResponse)
	 */
	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {

		clearMessages();
		String action = actionRequest.getParameter(ACTION_PARAM);
		String costCenterName = actionRequest.getParameter("costCenterName");
		String costCenterDesc = actionRequest.getParameter("costCenterDesc");
		boolean display=ParamUtil.getBoolean(actionRequest,"display");
		
		String operation = "";
		
		logger.debug("costCenterName="+costCenterName);
		logger.debug("costCenterDesc="+costCenterDesc);
		logger.debug("display = "+display);
		
		logger.debug("creating cost center object ...");
		CostCenter newCostCenter = new CostCenterImpl();
		newCostCenter.setCostCenterName(costCenterName);
		newCostCenter.setDescription(costCenterDesc);
		newCostCenter.setDisplay(display);
		
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String userType = OmpHelper.getUserRoles(themeDisplay.getUserId());
		int gruppoUser = GruppoUsers.GNED.getCode();
		if(userType.equals(OmpRoles.OMP_CONTROLLER_VBS.getLabel())
				|| userType.equals(OmpRoles.OMP_APPROVER_VBS.getLabel())
				|| userType.equals(OmpRoles.OMP_ENG_VBS.getLabel())){
			gruppoUser = GruppoUsers.VBS.getCode();
		}
		newCostCenter.setGruppoUsers(gruppoUser);
		
		logger.debug("cost center object created !!");
		try {
			if(ADD_ACTION.equals(action)){
				operation = "added";
				logger.debug("adding new cost center ["+costCenterName+"] in database ...");
				
				if(CostCenterLocalServiceUtil.getCostCenterByName(costCenterName, gruppoUser) == null){
					CostCenterLocalServiceUtil.addCostCenter(newCostCenter);
					successMSG = "cost center name ["+costCenterName+"] successfully Added";
				}else{
					errorMSG = "cost center name ["+costCenterName+"] already existing";
				}
			}else if(UPDATE_ACTION.equals(action)){
				
				operation = "updated";
				String costCenterId = actionRequest.getParameter("costCenterId");
				newCostCenter.setCostCenterId(Integer.valueOf(costCenterId));
				CostCenterUser costCenterUser = CostCenterUserLocalServiceUtil.getCostCenterUserByCostCenterId(Long.valueOf(costCenterId));
				if(display || costCenterUser ==null) {
				 if(! (isCostCenterCodeNameExists(newCostCenter , costCenterName, gruppoUser))){
					logger.debug("updating the cost center ["+costCenterId+"] in database ...");
					
					CostCenterLocalServiceUtil.updateCostCenter(newCostCenter);
					
					successMSG = "cost center Name ["+costCenterName+"] successfully Updated";
				}else{
					errorMSG = "cost center name ["+costCenterName+"] already existing";
				}
			}else {
				errorMSG = "cost center ["+costCenterName+"] is assigned to user, you can't disable it";
				}
				
				
			}
			
			
		} catch (SystemException e) {
			errorMSG = "Cost center ["+costCenterName+"] not "+operation+".";
			logger.error("error happened while "+operation+" new cost center in database.");
			logger.error(e.getMessage(), e);
		}catch (NumberFormatException nfe) {
			errorMSG = "Cost center ["+costCenterName+"] not "+operation+".";
			logger.error("error happened while "+operation+" Cost center ["+costCenterName+"] in database.");
			logger.error(nfe.getMessage(), nfe);
		}
		
		actionRequest.setAttribute("errorMSG", errorMSG);
		actionRequest.setAttribute("successMSG", successMSG);
		super.processAction(actionRequest, actionResponse);
	}
	
	
	public void deleteCostCenter(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse)
			throws PortletException, IOException {
		clearMessages();
		
		String costCenterId=resourceRequest.getParameter("costCenterId");
		String costCenterName=resourceRequest.getParameter("costCenterName");
		String action=resourceRequest.getParameter("action");
		String jspPage = "/html/costcenteradmin/costcenterlist.jsp";
		
		if(Validator.isBlank(action)){
			errorMSG = "No Action in the request";
		}else if(Validator.isBlank(costCenterId)){
			errorMSG = "No Cost Center Id in the request";
		}else{
			
			try {
				//check first if the cost center is assigned on users
				CostCenterUser costCenterUser = CostCenterUserLocalServiceUtil.getCostCenterUserByCostCenterId(Long.valueOf(costCenterId));
				if(costCenterUser == null){
					CostCenterLocalServiceUtil.deleteCostCenter(Long.valueOf(costCenterId));
					successMSG = "cost center name ["+costCenterName+"] successfully deleted";
				}else{
					errorMSG = "Can NOT delete Cost Center assigned to users";
				}
				
			} catch (NumberFormatException e) {
				logger.error(e.getMessage());
				errorMSG = "Cost Center Id is invalid";
			} catch (PortalException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove Cost Center";
			} catch (SystemException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove Cost Center [most probably cost center assigned to users]";
			}
			
		}
		
		//retrieve updated list
		try {
			retrieveCostCenterListToRequest(resourceRequest);
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			errorMSG = "can not retrieve cost center list";
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
		
	}

	@Override
    public void serveResource(ResourceRequest resourceRequest,
                  ResourceResponse resourceResponse) throws IOException,
                  PortletException {
		String action=resourceRequest.getParameter("action");
		if("delete".equals(action)){
			deleteCostCenter(resourceRequest, resourceResponse);
		}else if("pagination".equals(action)){
			paginationList(resourceRequest, resourceResponse);
		}
		
		clearMessages();
           super.serveResource(resourceRequest, resourceResponse);
    }
	
	private CostCenter getCostCenter(String costCenterId){
		CostCenter costCenter = null;
		try {
			costCenter = CostCenterLocalServiceUtil.getCostCenter(Integer.valueOf(costCenterId));
		} catch (NumberFormatException e) {
			logger.error("cost center Id has wrong format", e);
		} catch (PortalException e) {
			logger.error("unexpected error", e);
		} catch (SystemException e) {
			logger.error("unexpected error", e);
		}
		
		return costCenter;
	}
	
	
	private boolean isCostCenterCodeNameExists(CostCenter newCostCenter, 
			String costNameName, 
			int gruppoUser) throws SystemException{
			
		CostCenter oldWCostCenter = CostCenterLocalServiceUtil.getCostCenterByName(costNameName, gruppoUser);
		boolean isExists = false;
		
		if( oldWCostCenter != null && 
				(newCostCenter.getCostCenterId() != oldWCostCenter.getCostCenterId()) ){
			isExists = true;
		}
		return isExists;
		}

	private void paginationList(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse) throws PortletException, IOException{
		
		String jspPage = "/html/costcenteradmin/costcenterlist.jsp";
		try {
			retrieveCostCenterListToRequest(resourceRequest);
		} catch (SystemException e) {
		logger.error("error happened in pagination ... ",e);
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
	}
	
	private Integer getTotalNumberOfPages(Integer totalNubmerOfResult, Integer pageSize) {
        return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
}
	
	private void clearMessages(){
		errorMSG = "";
		successMSG = "";
	}

	public void searchFilter(ActionRequest request, ActionResponse response) throws IOException, PortletException{
		response.setRenderParameter("searchFilter", "%" + ParamUtil.getString(request, "costCenterName") + "%");  
	}
}
