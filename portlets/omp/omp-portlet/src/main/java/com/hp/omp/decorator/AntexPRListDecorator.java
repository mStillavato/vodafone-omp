package com.hp.omp.decorator;

import java.io.IOException;
import java.io.Serializable;

import org.displaytag.decorator.TableDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.UploadDownloadHelper;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.custom.Currency;
import com.hp.omp.model.custom.PurchaseRequestStatus;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;

public class AntexPRListDecorator extends TableDecorator implements Serializable {
}
