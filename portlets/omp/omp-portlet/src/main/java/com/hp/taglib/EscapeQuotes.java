package com.hp.taglib;

import java.io.IOException;
import java.io.StringWriter;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class EscapeQuotes extends SimpleTagSupport {
	private String value;

	   public void setValue(String value) {
	      this.value = value;
	   }

	   StringWriter sw = new StringWriter();

	   public void doTag()
	      throws JspException, IOException
	    {
	       if (value != null) {
	          /* Use message from attribute */
	          JspWriter out = getJspContext().getOut();
	          out.print( value.replace("\"", "\\\"").replace("'","\\'") );
	       }
	       
	   }
}
