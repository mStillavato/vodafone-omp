package com.hp.omp.service.impl;

import java.util.List;

import com.hp.omp.model.BudgetCategory;
import com.hp.omp.service.base.BudgetCategoryLocalServiceBaseImpl;
import com.hp.omp.service.persistence.BudgetCategoryFinderUtil;
import com.hp.omp.service.persistence.BudgetCategoryUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the budget category local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.BudgetCategoryLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Ahmed Kamel
 * @see com.hp.omp.service.base.BudgetCategoryLocalServiceBaseImpl
 * @see com.hp.omp.service.BudgetCategoryLocalServiceUtil
 */
public class BudgetCategoryLocalServiceImpl
    extends BudgetCategoryLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.hp.omp.service.BudgetCategoryLocalServiceUtil} to access the budget category local service.
     */
	
	public BudgetCategory getBudgetCategoryByName(String name) throws SystemException{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(BudgetCategory.class);
		dynamicQuery.add(RestrictionsFactoryUtil.ilike("budgetCategoryName",name));
		 
		BudgetCategory budgetCategory = null;
		List<BudgetCategory>  budgetCategoryList = BudgetCategoryUtil.findWithDynamicQuery(dynamicQuery);
		if(budgetCategoryList != null && ! (budgetCategoryList.isEmpty()) ){
			budgetCategory = budgetCategoryList.get(0);
		}
		
		return budgetCategory;
	}

	public List<BudgetCategory> getBudgetCategoryList(Integer pageNumber, Integer pageSize, String searchFilter)
			throws SystemException {
//		Integer start = (pageNumber - 1) * pageSize;
//		Integer end = start + pageSize;	
		List<BudgetCategory> resultQuery = null;
		resultQuery= BudgetCategoryFinderUtil.getBudgetCategoryList(pageNumber, pageSize, searchFilter);
		return resultQuery; 
	}
}
