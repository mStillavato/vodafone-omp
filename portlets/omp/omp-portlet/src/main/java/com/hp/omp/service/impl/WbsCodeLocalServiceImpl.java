/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service.impl;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;

import com.hp.omp.model.ExcelReport;
import com.hp.omp.model.ExcelStyler;
import com.hp.omp.model.ExcelTable;
import com.hp.omp.model.WbsCode;
import com.hp.omp.model.custom.ExcelTableRow;
import com.hp.omp.model.custom.OMPAggregateReportExcelRow;
import com.hp.omp.model.impl.ExcelDefaultStyler;
import com.hp.omp.model.impl.ExcelReportImpl;
import com.hp.omp.model.impl.ExcelTableImpl;
import com.hp.omp.service.base.WbsCodeLocalServiceBaseImpl;
import com.hp.omp.service.persistence.ProjectFinderUtil;
import com.hp.omp.service.persistence.WbsCodeFinderUtil;
import com.hp.omp.service.persistence.WbsCodeUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

/**
 * The implementation of the wbs code local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.WbsCodeLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Ahmed Gewaly
 * @see com.hp.omp.service.base.WbsCodeLocalServiceBaseImpl
 * @see com.hp.omp.service.WbsCodeLocalServiceUtil
 */
public class WbsCodeLocalServiceImpl extends WbsCodeLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.hp.omp.service.WbsCodeLocalServiceUtil} to access the wbs code local service.
	 */
	
	public WbsCode getWbsCodeByName(String name) throws SystemException{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(WbsCode.class);
		dynamicQuery.add(RestrictionsFactoryUtil.ilike("wbsCodeName", name));
		 
		WbsCode wbsCode = null;
		List<WbsCode>  wbsCodeList = WbsCodeUtil.findWithDynamicQuery(dynamicQuery);
		if(wbsCodeList != null && ! (wbsCodeList.isEmpty()) ){
			wbsCode = wbsCodeList.get(0);
		}
		
		return wbsCode;
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<WbsCode> getWbsCodesList(int start, int end, String searchFilter)
			throws SystemException {
		List<WbsCode> resultQuery = null;
		resultQuery= WbsCodeFinderUtil.getWbsCodesList(start, end, searchFilter);
		return resultQuery; 
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getWbsCodesCountList(String searchFilter) throws SystemException {
		int countList = 0;
		countList = WbsCodeFinderUtil.getWbsCodesCountList(searchFilter);
		return countList;
	}

	public void exportWbsCodesAsExcel(List<WbsCode> listWbsCodes,
			List<Object> header, OutputStream out) throws SystemException {
		ExcelReport excelReport = new ExcelReportImpl();
		ExcelStyler styler = new ExcelDefaultStyler(excelReport.getWorkBook());
		excelReport.setStyler(styler);

		List<ExcelTableRow> wbsCodesList = new ArrayList<ExcelTableRow>();
		for(WbsCode wbsCode : listWbsCodes){
			OMPAggregateReportExcelRow tableRow = new OMPAggregateReportExcelRow();
			tableRow.setFieldAtIndex(wbsCode.getWbsCodeName(), 0);
			tableRow.setFieldAtIndex(wbsCode.getDescription(), 1);
			tableRow.setFieldAtIndex(wbsCode.getDisplay(), 2);

			wbsCodesList.add(tableRow);
		}

		ExcelTable euroTable = new ExcelTableImpl(header, wbsCodesList, "WbsCodes");
		excelReport.addExcelTable(euroTable, 0);

		Workbook exelWorkBook = excelReport.createExelWorkBook();
		try {
			exelWorkBook.write(out);
		} catch (IOException e) {
		} finally{
			try {
				out.flush();
				out.close();
			} catch (IOException e) {
			}
		}
	}
}