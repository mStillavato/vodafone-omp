package com.hp.omp.portlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.PortletURL;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.helper.POSearchHelper;
import com.hp.omp.helper.PurchaseRequestExcelFileHelper;
import com.hp.omp.model.CostCenterUser;
import com.hp.omp.model.GoodReceipt;
import com.hp.omp.model.Position;
import com.hp.omp.model.custom.GrExcelRow;
import com.hp.omp.model.custom.IAndCType;
import com.hp.omp.model.custom.ListDTO;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.custom.PoListDTO;
import com.hp.omp.model.custom.PurchaseOrderStatus;
import com.hp.omp.model.custom.SearchDTO;
import com.hp.omp.model.custom.TipoPr;
import com.hp.omp.model.impl.GoodReceiptImpl;
import com.hp.omp.service.CostCenterUserLocalServiceUtil;
import com.hp.omp.service.GoodReceiptLocalServiceUtil;
import com.hp.omp.service.PositionLocalServiceUtil;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.Role;
import com.liferay.portal.service.CompanyLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletURLFactoryUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class GoodReceiptListVbsPortlet extends MVCPortlet {

	private static String REQUESTER_MODE = "0";
	private static String RECEIVER_MODE = "1";
	private static String CONTROLLER_MODE = "2";
	private String poRequesterTableID = "poRequesterTable";
	private String poReceiverTableID = "poReceiverTable";

	private static Log _log = LogFactoryUtil.getLog(GoodReceiptListVbsPortlet.class);
	Logger logger = LoggerFactory.getLogger(GoodReceiptListVbsPortlet.class);
	public static final String DATE_FORMAT = "dd/MM/yyyy";
	private Object lock  = new Object();

	@Override
	public void doEdit(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		PortletPreferences preferences = renderRequest.getPreferences();
		String mode = preferences.getValue("mode", REQUESTER_MODE);
		renderRequest.setAttribute("mode", mode);
		super.doEdit(renderRequest, renderResponse);
	}

	@ProcessAction(name="setPortletMode")
	public void setPortletMode(ActionRequest request, ActionResponse response){
		PortletPreferences preferences = null;
		try {
			String modeString = request.getParameter("mode");
			preferences = request.getPreferences();
			if( !(REQUESTER_MODE.equals(modeString))  && !(RECEIVER_MODE.equals(modeString)) && !(CONTROLLER_MODE.equals(modeString))){
				preferences.setValue("mode", REQUESTER_MODE);
			}else{
				preferences.setValue("mode", modeString);
			}
			preferences.store();
			response.setPortletMode(PortletMode.VIEW);
		} catch (Exception e) {
			_log.error("Unable to Set Prefernces", e);
		}
	}

	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		PortletPreferences preferences = renderRequest.getPreferences();
		String mode = preferences.getValue("mode", REQUESTER_MODE);

		renderRequest.setAttribute("requesterMode", REQUESTER_MODE);
		renderRequest.setAttribute("receiverMode", RECEIVER_MODE);
		renderRequest.setAttribute("controllerMode", CONTROLLER_MODE);

		try {
			if(RECEIVER_MODE.equals(mode)){
				renderRequest.setAttribute("mode", RECEIVER_MODE);
				//processReceiverMode(renderRequest, renderResponse);
			} else if(CONTROLLER_MODE.equals(mode)){
				renderRequest.setAttribute("mode", CONTROLLER_MODE);
			}else{
				renderRequest.setAttribute("mode", REQUESTER_MODE);
				//processRequesterMode(renderRequest, renderResponse);
			}


			//Search
			SearchDTO searchDTO = null;
			Object searchDTOObject = renderRequest.getAttribute("searchDTO");
			if(searchDTOObject != null && searchDTOObject instanceof SearchDTO){
				searchDTO = (SearchDTO)searchDTOObject;
			}else{
				searchDTO = new SearchDTO();
			}
			POSearchHelper poSearchHelper = new POSearchHelper();
			ThemeDisplay themeDisplay = (ThemeDisplay)renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			long userId = themeDisplay.getUserId();
			searchDTO = poSearchHelper.populatePO(searchDTO, userId);
			renderRequest.setAttribute("searchDTO", searchDTO);

		} catch (Exception e) {
			logException(e);
		} 



		super.doView(renderRequest, renderResponse);
	}


	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		PortletPreferences preferences = resourceRequest.getPreferences();
		String mode = preferences.getValue("mode", REQUESTER_MODE);
		String searchAction = resourceRequest.getParameter("searchAction");
		try {
			POSearchHelper poSearchHelper = new POSearchHelper();
			if("doPoSearch".equals(searchAction)){
				debug("serverResource: doSearch");
				SearchDTO searchDTO = poSearchHelper.getSearchDTO(resourceRequest);
				if(searchDTO != null){
					processAdvancedSearch(resourceRequest, resourceResponse, searchDTO, mode);
				}
			}else if("getSubProjects".equals(resourceRequest.getResourceID())){
				poSearchHelper.getSubProjects(resourceRequest,resourceResponse);
			}else{
				processDefault(resourceRequest, resourceResponse, mode);
			}

		} catch (Exception e) {
			logException(e);
		} 
	}

	private void processDefault(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse, String mode) throws IOException, PortletException, PortalException, SystemException{
		if(RECEIVER_MODE.equals(mode)){
			processReceiverMode(resourceRequest, resourceResponse);
		} else if(CONTROLLER_MODE.equals(mode)){
			processControllerMode(resourceRequest, resourceResponse);
		}else{
			processRequesterMode(resourceRequest, resourceResponse);
		}
	}

	private void processAdvancedSearch(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse, SearchDTO searchDTO, String mode){
		if(RECEIVER_MODE.equals(mode)){
			processPOSearchRecieverMode(resourceRequest, resourceResponse, searchDTO, OmpHelper.PAGE_SIZE);
		}else if(CONTROLLER_MODE.equals(mode)){
			processPOSearchControllerMode(resourceRequest, resourceResponse, searchDTO, OmpHelper.PAGE_SIZE);
		}else{
			processPOSearchRequestoreMode(resourceRequest, resourceResponse, searchDTO, OmpHelper.PAGE_SIZE);
		}
	}
	private void processRequesterMode(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException, PortletException, PortalException, SystemException {
		String grSubPage = ParamUtil.get(resourceRequest, "gr_view", "po-received");
		String jspPage = "/html/goodreceiptlistvbs/requester_mode.jsp";

		resourceRequest.setAttribute("grSubPage", grSubPage);
		//		long plid = getPlid(renderRequest, true, purchaseOrderFriendlyURL);
		//		renderRequest.setAttribute("poPlid", plid);
		//		renderRequest.setAttribute("poPortletName", purchaseOrderPortletName);
		int status = 0;
		if("po-received".equals(grSubPage)){
			status = PurchaseOrderStatus.PO_RECEIVED.getCode();
		}else if("gr-requested".equals(grSubPage)){
			status = PurchaseOrderStatus.GR_RECEIVED.getCode();
		}else{
			processEmptyPORequesterList(resourceRequest,resourceResponse);
			return;
		}
		ThemeDisplay themeDisplay = (ThemeDisplay)resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long userId = themeDisplay.getUserId();
		//boolean filterByCostCenter = shouldFilterByCostCenter(userId);
		boolean filterByCostCenter = false;
		if(filterByCostCenter){
			try {
				CostCenterUser costCenterUser = CostCenterUserLocalServiceUtil.getCostCenterUserByUserId(userId);
				if(costCenterUser == null){
					processEmptyPORequesterList(resourceRequest,resourceResponse);
				}else{
					Long costCenterId = costCenterUser.getCostCenterId();
					processRequesterPOList(resourceRequest,resourceResponse,status,OmpHelper.PAGE_SIZE,poRequesterTableID,costCenterId,userId);
				}
			} catch (SystemException e) {
				_log.error(e);
			}
		}else{
			processRequesterPOList(resourceRequest,resourceResponse,status,OmpHelper.PAGE_SIZE,poRequesterTableID,null,userId);
		}
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);

	}



	private void processReceiverMode(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException, PortletException {
		String jspPage = "/html/goodreceiptlistvbs/receiver_mode.jsp";
		processReceiverPOList(resourceRequest,resourceResponse,OmpHelper.PAGE_SIZE,poReceiverTableID,null);
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);

	}

	private void processControllerMode(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException, PortletException {
		String jspPage = "/html/goodreceiptlistvbs/receiver_mode.jsp";
		processControllerPOList(resourceRequest,resourceResponse,OmpHelper.PAGE_SIZE,poReceiverTableID,null);
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);

	}

	private boolean shouldFilterByCostCenter(long userId) {
		try {
			Company company = CompanyLocalServiceUtil.getCompanyByMx(PropsUtil.get(PropsKeys.COMPANY_DEFAULT_WEB_ID));

			Role controllerRole = RoleLocalServiceUtil.getRole(company.getCompanyId(), "OMP_"+OmpRoles.OMP_CONTROLLER_VBS.getLabel());
			Role engRole = RoleLocalServiceUtil.getRole(company.getCompanyId(), "OMP_"+OmpRoles.OMP_ENG_VBS.getLabel());
			Role approverRole = RoleLocalServiceUtil.getRole(company.getCompanyId(), "OMP_"+OmpRoles.OMP_APPROVER_VBS.getLabel());

			if(RoleLocalServiceUtil.hasUserRole(userId, controllerRole.getRoleId())){
				return false;
			}
			if(RoleLocalServiceUtil.hasUserRole(userId, approverRole.getRoleId())){
				return false;
			}
			if(RoleLocalServiceUtil.hasUserRole(userId, engRole.getRoleId())){
				return true;
			}

			return true;
		} catch (Exception e) {
			_log.error(e);
			return true;
		}
	}


	private void processReceiverPOList(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse, int grListpageSize, String tableId, Long costCenterId) {
		try {


			ThemeDisplay themeDisplay = (ThemeDisplay)resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
			long userId = themeDisplay.getUserId();

			Integer pageNumber = ParamUtil.getInteger(resourceRequest, "pageNumber", 1);

			Integer start = getStartPosition(resourceRequest, grListpageSize);
			Integer end = start + grListpageSize;
			debug("Start: "+start+" End: "+end);
			String orderby = ParamUtil.getString(resourceRequest, "orderby", "purchaseOrderId");
			String order = ParamUtil.getString(resourceRequest, "order", "desc");

			SearchDTO searchDto = new SearchDTO();
			searchDto.setOrderby(orderby);
			searchDto.setOrder(order);
			searchDto.setStart(start);
			searchDto.setEnd(end);
			searchDto.setUserRole((OmpHelper.getUserRole(userId)).getLabel());
			Long poListCountLong = PurchaseOrderLocalServiceUtil.getPurchaseOrdersGRRequestedCount(searchDto);

			List<PoListDTO> poList = PurchaseOrderLocalServiceUtil.getPurchaseOrdersGRRequested(searchDto);

			_log.debug("Count: "+poListCountLong);
			_log.debug("poList: "+poList.size());
			Integer poListCount = poListCountLong.intValue();
			debug(poListCount);
			//resourceRequest.setAttribute("poList", poList);
			//resourceRequest.setAttribute("poListCount", poListCount);
			//resourceRequest.setAttribute("poListpageSize", grListpageSize);

			resourceRequest.setAttribute("numberOfPages", getTotalNumberOfPages(poListCountLong, grListpageSize));
			resourceRequest.setAttribute("pageNumber", pageNumber);
			resourceRequest.setAttribute("poList", poList);
			resourceRequest.setAttribute("redirect", "/group/vodafone/gr-closure");
		} catch (Exception e) {
			logException(e);
			processEmptyPOReceiverList(resourceRequest,resourceResponse);
		}

	}


	private void processControllerPOList(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse, int grListpageSize, String tableId, Long costCenterId) {
		try {
			Integer pageNumber = ParamUtil.getInteger(resourceRequest, "pageNumber", 1);

			Integer start = getStartPosition(resourceRequest, grListpageSize);
			Integer end = start + grListpageSize;
			debug("Start: "+start+" End: "+end);
			String orderby = ParamUtil.getString(resourceRequest, "orderby", "purchaseOrderId");
			String order = ParamUtil.getString(resourceRequest, "order", "desc");

			SearchDTO searchDto = new SearchDTO();
			searchDto.setOrderby(orderby);
			searchDto.setOrder(order);
			searchDto.setStart(start);
			searchDto.setEnd(end);
			searchDto.setTipoPr(TipoPr.VBS.getCode());

			Long poListCountLong = PurchaseOrderLocalServiceUtil.getPurchaseOrdersGRClosedCount(searchDto);
			List<PoListDTO> poList = PurchaseOrderLocalServiceUtil.getPurchaseOrdersGRClosed(searchDto);

			_log.debug("Count: "+poListCountLong);
			_log.debug("poList: "+poList.size());
			Integer poListCount = poListCountLong.intValue();
			debug(poListCount);
			//resourceRequest.setAttribute("poList", poList);
			//resourceRequest.setAttribute("poListCount", poListCount);
			//resourceRequest.setAttribute("poListpageSize", grListpageSize);


			Integer totalNumberOfPages = getTotalNumberOfPages(poListCountLong, grListpageSize);
			if(totalNumberOfPages > 5) {
				totalNumberOfPages = 5;
			}
			resourceRequest.setAttribute("numberOfPages", totalNumberOfPages);
			resourceRequest.setAttribute("pageNumber", pageNumber);
			resourceRequest.setAttribute("poList", poList);
			resourceRequest.setAttribute("redirect", "/group/vodafone/po-closed");
		} catch (Exception e) {
			logException(e);
			processEmptyPOReceiverList(resourceRequest,resourceResponse);
		}

	}

	private Integer getTotalNumberOfPages(Long totalNubmerOfResult, Integer pageSize) {
		return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
	}

	private void processRequesterPOList(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse, int status,int poListpageSize, String tableId, Long costCenterId, Long userId) {
		try {
			Integer pageNumber = ParamUtil.getInteger(resourceRequest, "pageNumber", 1);
			String orderby = ParamUtil.getString(resourceRequest, "orderby", "purchaseOrderId");
			String order = ParamUtil.getString(resourceRequest, "order", "desc");
			Integer start = getStartPosition(resourceRequest, poListpageSize);
			Integer end = start + poListpageSize;

			debug("Start: "+start+" End: "+end + " orderby: " + orderby + " order: " + order);
			Long poListCountLong = null;
			List<PoListDTO> poList = null;

			SearchDTO searchDTO = new SearchDTO();
			searchDTO.setPoStatus(status);
			searchDTO.setOrderby(orderby);
			searchDTO.setOrder(order);
			searchDTO.setStart(start);
			searchDTO.setEnd(end);
			searchDTO.setCostCenterId(costCenterId);
			searchDTO.setUserRole((OmpHelper.getUserRole(userId)).getLabel());
			searchDTO.setTipoPr(TipoPr.VBS.getCode());

			if(OmpHelper.isApproverVbs(userId)){
				searchDTO.setPositionICApproval(Integer.toString(IAndCType.ICSERVIZI.getCode()));
			}

			poListCountLong = PurchaseOrderLocalServiceUtil.getPurchaseOrdersGRRequestedCount(searchDTO);
			poList = PurchaseOrderLocalServiceUtil.getPurchaseOrdersGRRequested(searchDTO);

			Integer poListCount = poListCountLong.intValue();
			debug(poListCount);
			/*resourceRequest.setAttribute("poList", poList);
			resourceRequest.setAttribute("poListCount", poListCount);
			resourceRequest.setAttribute("poListpageSize", poListpageSize);*/
			resourceRequest.setAttribute("numberOfPages", getTotalNumberOfPages(poListCountLong, poListpageSize));
			resourceRequest.setAttribute("pageNumber", pageNumber);
			resourceRequest.setAttribute("poList", poList);
			resourceRequest.setAttribute("redirect", "/group/vodafone/gr-request");
		} catch (Exception e) {
			logException(e);
			processEmptyPORequesterList(resourceRequest,resourceResponse);
		}

	}


	private void processPOSearchRequestoreMode(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse, SearchDTO searchDTO, int poListpageSize) {
		try {
			if(searchDTO == null){
				processEmptyPORequesterList(resourceRequest,resourceResponse);
				return;
			}

			String jspPage = "/html/goodreceiptlistvbs/requester_mode.jsp";

			//set the status
			String grSubPage = ParamUtil.get(resourceRequest, "gr_view", "po-received");
			resourceRequest.setAttribute("grSubPage", grSubPage);
			int status = 0;
			if("po-received".equals(grSubPage)){
				status = PurchaseOrderStatus.PO_RECEIVED.getCode();
			}else if("gr-requested".equals(grSubPage)){
				status = PurchaseOrderStatus.GR_RECEIVED.getCode();
			}				
			searchDTO.setPoStatus(status);
			searchDTO.setTipoPr(TipoPr.VBS.getCode());

			ThemeDisplay themeDisplay = (ThemeDisplay)resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
			long userId = themeDisplay.getUserId();

			if(OmpHelper.isApproverVbs(userId)){
				searchDTO.setPositionICApproval(Integer.toString(IAndCType.ICSERVIZI.getCode()));
			}

			searchDTO.setUserRole((OmpHelper.getUserRole(userId)).getLabel());
			Integer pageNumber = ParamUtil.getInteger(resourceRequest, "pageNumber", 1);
			String orderby = ParamUtil.getString(resourceRequest, "orderby", "purchaseOrderId");
			String order = ParamUtil.getString(resourceRequest, "order", "desc");
			Integer start = getStartPosition(resourceRequest, poListpageSize);
			Integer end = start + poListpageSize;

			debug("Start: "+start+" End: "+end + " orderby: " + orderby + " order: " + order);

			if(PurchaseOrderStatus.GR_RECEIVED.getCode() == status){
				Long poListCountLong = PurchaseOrderLocalServiceUtil.getAdvancedSearchGrRequestedResultsCount(searchDTO);

				if(poListCountLong != null && poListCountLong > 0){
					List<PoListDTO>	poList = PurchaseOrderLocalServiceUtil.getAdvancedSearchGrRequestedResults(searchDTO, start, end, orderby, order);

					Integer poListCount = poListCountLong.intValue();
					debug(poListCount.toString());

					resourceRequest.setAttribute("numberOfPages", getTotalNumberOfPages(poListCountLong, poListpageSize));
					resourceRequest.setAttribute("pageNumber", pageNumber);
					resourceRequest.setAttribute("poList", poList);
					resourceRequest.setAttribute("redirect", "/group/vodafone/gr-request");
				}else{
					processEmptyPORequesterList(resourceRequest,resourceResponse);
				}

			} else {
				Long poListCountLong = PurchaseOrderLocalServiceUtil.getAdvancedSearchResultsCount(searchDTO);

				if(poListCountLong != null && poListCountLong > 0){
					List<PoListDTO>	poList = PurchaseOrderLocalServiceUtil.getAdvancedSearchResults(searchDTO, start, end, orderby, order);

					Integer poListCount = poListCountLong.intValue();
					debug(poListCount.toString());

					resourceRequest.setAttribute("numberOfPages", getTotalNumberOfPages(poListCountLong, poListpageSize));
					resourceRequest.setAttribute("pageNumber", pageNumber);
					resourceRequest.setAttribute("poList", poList);
					resourceRequest.setAttribute("redirect", "/group/vodafone/gr-request");
				}else{
					processEmptyPORequesterList(resourceRequest,resourceResponse);
				}
			}

			getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);

		} catch (Exception e) {
			logException(e);
			processEmptyPORequesterList(resourceRequest,resourceResponse);
		}

	}

	private void processPOSearchRecieverMode(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse, SearchDTO searchDTO, int poListpageSize) {
		try {
			if(searchDTO == null){
				processEmptyPORequesterList(resourceRequest,resourceResponse);
				return;
			}

			String jspPage = "/html/goodreceiptlistvbs/receiver_mode.jsp";

			//set the status
			String grSubPage = ParamUtil.get(resourceRequest, "gr_view", "po-received");
			resourceRequest.setAttribute("grSubPage", grSubPage);

			Integer pageNumber = ParamUtil.getInteger(resourceRequest, "pageNumber", 1);
			String orderby = ParamUtil.getString(resourceRequest, "orderby", "purchaseOrderId");
			String order = ParamUtil.getString(resourceRequest, "order", "desc");
			Integer start = getStartPosition(resourceRequest, poListpageSize);
			Integer end = start + poListpageSize;

			debug("Start: "+start+" End: "+end + " orderby: " + orderby + " order: " + order);

			//searchDTO.setPoStatus(PurchaseOrderStatus.GR_RECEIVED.getCode());
			searchDTO.setUserRole(OmpRoles.OMP_ANTEX.getLabel());

			Long poListCountLong = PurchaseOrderLocalServiceUtil.getAdvancedSearchGrRequestedResultsCount(searchDTO);

			if(poListCountLong != null && poListCountLong > 0){

				List<PoListDTO> poList = PurchaseOrderLocalServiceUtil.getAdvancedSearchGrRequestedResults(searchDTO, start, end, orderby, order);

				Integer poListCount = poListCountLong.intValue();
				debug(poListCount.toString());

				resourceRequest.setAttribute("numberOfPages", getTotalNumberOfPages(poListCountLong, poListpageSize));
				resourceRequest.setAttribute("pageNumber", pageNumber);
				resourceRequest.setAttribute("poList", poList);
				resourceRequest.setAttribute("redirect", "/group/vodafone/gr-closure");
			}else{
				processEmptyPORequesterList(resourceRequest,resourceResponse);
			}

			getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);

		} catch (Exception e) {
			logException(e);
			processEmptyPORequesterList(resourceRequest,resourceResponse);
		}

	}

	private void processPOSearchControllerMode(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse, SearchDTO searchDTO, int poListpageSize) {
		try {
			if(searchDTO == null){
				processEmptyPORequesterList(resourceRequest,resourceResponse);
				return;
			}

			String jspPage = "/html/goodreceiptlistvbs/receiver_mode.jsp";

			Integer pageNumber = ParamUtil.getInteger(resourceRequest, "pageNumber", 1);
			String orderby = ParamUtil.getString(resourceRequest, "orderby", "purchaseOrderId");
			String order = ParamUtil.getString(resourceRequest, "order", "desc");
			Integer start = getStartPosition(resourceRequest, poListpageSize);
			Integer end = start + poListpageSize;

			searchDTO.setPoStatus(PurchaseOrderStatus.PO_CLOSED.getCode());
			searchDTO.setUserRole(OmpRoles.OMP_CONTROLLER_VBS.getLabel());
			searchDTO.setTipoPr(TipoPr.VBS.getCode());
			
			debug("Start: "+start+" End: "+end + " orderby: " + orderby + " order: " + order);

			Long poListCountLong = PurchaseOrderLocalServiceUtil.getAdvancedSearchResultsCount(searchDTO);

			if(poListCountLong != null && poListCountLong > 0){

				List<PoListDTO> poList = PurchaseOrderLocalServiceUtil.getAdvancedSearchResults(searchDTO, start, end, orderby, order);

				Integer poListCount = poListCountLong.intValue();
				debug(poListCount.toString());

				Integer totalNumberOfPages = getTotalNumberOfPages(poListCountLong, poListpageSize);
				if(totalNumberOfPages > 5) {
					totalNumberOfPages = 5;
				}
				resourceRequest.setAttribute("numberOfPages", totalNumberOfPages);
				resourceRequest.setAttribute("pageNumber", pageNumber);
				resourceRequest.setAttribute("poList", poList);
				resourceRequest.setAttribute("redirect", "/group/vodafone/po-closed");
			}else{
				processEmptyPORequesterList(resourceRequest,resourceResponse);
			}

			getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);

		} catch (Exception e) {
			logException(e);
			processEmptyPORequesterList(resourceRequest,resourceResponse);
		}

	}

	private void processEmptyPOReceiverList(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) {
		List<ListDTO> poList = new ArrayList<ListDTO>();
		resourceRequest.setAttribute("poList", poList);
		resourceRequest.setAttribute("numberOfPages", 0);
		resourceRequest.setAttribute("pageNumber", 1);

	}


	private void processEmptyPORequesterList(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) {
		List<PoListDTO> poList = new ArrayList<PoListDTO>(); 
		resourceRequest.setAttribute("poList", poList);
		resourceRequest.setAttribute("numberOfPages", 0);
		resourceRequest.setAttribute("pageNumber", 1);
	}




	private Integer getStartPosition(PortletRequest request, int pageSize){
		Integer pageNumber = ParamUtil.getInteger(request, "pageNumber", 1);
		Integer startIndex = (pageNumber-1) * pageSize;
		return startIndex;

	}


	private void logException(Exception e) {
		_log.error(e.getMessage());
		if(_log.isDebugEnabled()){
			_log.debug("Error in the PO List", e);
		}
	}

	private static void debug(Object msg){
		if(_log.isDebugEnabled()){
			_log.debug(msg);
		}
	}

	public void uploadBatchExcelFile(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException{
		logger.info("Start uploading excel GR file");

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		long userId = themeDisplay.getUserId();

		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(actionRequest);

		Map<Integer, String> errors = new LinkedHashMap<Integer, String>();
		if (uploadRequest.getSize("fileName")==0) {
			errors.put(1,"Empty file");
			SessionErrors.add(actionRequest, "Empty File, Please upload the right excel file");
		}
		List<GrExcelRow> readExcelFile = new ArrayList<GrExcelRow>();
		try {
			readExcelFile = PurchaseRequestExcelFileHelper.readExcelFileGr(uploadRequest.getFile("fileName"));
		} catch (Exception e) {
			errors.put(1, "Wrong file, please upload the right excel file");
			logger.error("Error in reading excel file",e);
		}

		insertBatchList(readExcelFile, errors, userId);

		if (readExcelFile.size()==0) {
			errors.put(1,"Empty file");
			SessionErrors.add(actionRequest, "Empty File, Please upload the right excel file");
		}
		if(errors.size() == 0 ) {
			SessionMessages.add(actionRequest, "trackingcode-saved");
			//actionResponse.setRenderParameter("totalRecords", readExcelFile.size()+"");
			String portletName = (String)actionRequest.getAttribute(WebKeys.PORTLET_ID);
			PortletURL redirectURL = PortletURLFactoryUtil.create(PortalUtil.getHttpServletRequest(actionRequest),portletName,
					themeDisplay.getLayout().getPlid(), PortletRequest.RENDER_PHASE);
			redirectURL.setParameter("totalRecords", readExcelFile.size()+""); // set required parameter that you need in doView or render Method
			actionResponse.sendRedirect(redirectURL.toString()); 
			// sendRedirect(actionRequest, actionResponse);

		} else {
			actionResponse.setRenderParameter("totalRecords", readExcelFile.size()+"");
			SessionErrors.add(actionRequest, "Error in the uploaded file");
			actionResponse.setRenderParameter("validationError", "true");
			actionResponse.setRenderParameter("jspPage", "/html/goodreceiptlistvbs/gruploadform.jsp");
			PortletSession portletSession = actionRequest.getPortletSession();
			logger.debug(" Set Error map size {}",errors.size());
			portletSession.setAttribute("uploadedBatch", errors);
		}

		_log.info("End uploading excel file");
	}

	private void insertBatchList(List<GrExcelRow> grRows, Map<Integer, String> errors, long userId) {

		for(int row = 0 ; row < grRows.size(); row++) {

			long poOrderId = Long.parseLong(grRows.get(row).getPoOrderId());
			long labelNumber = Long.parseLong(grRows.get(row).getPosition());
			long positionId = 0;

			try {
				List<Position> positionsList = PositionLocalServiceUtil.getPositionsByPurchaseOrderId(poOrderId);
				if(positionsList != null && positionsList.size() > 0){
					boolean positionFound = false;
					for(Position p: positionsList){
						if(labelNumber == p.getLabelNumber()){
							positionId = p.getPositionId();
							positionFound = true;
							break;
						}
					}

					if(!positionFound){
						errors.put(row+1, "Position not found for order " + poOrderId);
						continue;
					}

					double totPerc = 0;
					List <GoodReceipt> goodReceiptList = GoodReceiptLocalServiceUtil.getPositionGoodReceipts(String.valueOf(positionId));
					if(goodReceiptList != null && goodReceiptList.size() > 0){
						for(GoodReceipt gr: goodReceiptList){
							totPerc = totPerc + gr.getPercentage();
						}
					}

					if(totPerc == 100){
						errors.put(row+1, "Percentage already 100 for order " + poOrderId + "; Position " + labelNumber);
						continue;
					}

					try {
						GoodReceipt goodReceipt = bindGoodReceipt(String.valueOf(positionId), grRows.get(row));
						Position position = PositionLocalServiceUtil.getPosition(Long.valueOf(positionId));
						logger.debug("*******Position gr percentages is {} **************", position.getSumPositionGRPercentage());
						synchronized (lock) {
							if((position.getSumPositionGRPercentage() + goodReceipt.getPercentage()) <= 100 ) {
								goodReceipt.setRequestDate(new Date());
								goodReceipt.setCreatedUserId(String.valueOf(userId));
								GoodReceiptLocalServiceUtil.addGoodReceipt(goodReceipt);
							}
						}
					} catch(SystemException e) {
						errors.put(row+1, "Error in save or update Good Receipt " + poOrderId + "; Position " + labelNumber);
						logger.error("Error in save or update Good Receipt", e);
					}
				} else {
					errors.put(row+1, "Position not found for order " + poOrderId  + "; Position " + labelNumber);
					continue;
				}

			} catch (Exception e) {
				errors.put(row+1, "Position not found for order " + poOrderId + "; Position " + labelNumber);
				logger.error("Error in getPositionsByPurchaseOrderId", e);
				continue;
			}



		}
	}

	private GoodReceipt bindGoodReceipt(String positionId, GrExcelRow gr) throws NumberFormatException, PortalException, SystemException {
		
		Position position = PositionLocalServiceUtil.getPosition(Long.parseLong(positionId));
		
		GoodReceipt goodReceipt = new GoodReceiptImpl();
		goodReceipt.setPositionId(positionId);
		double percentage = Double.parseDouble(gr.getPercentualeRichiesta());
		goodReceipt.setPercentage(percentage);
		
		double total = Double.valueOf(position.getActualUnitCost()) * Double.valueOf(position.getQuatity());
		
		// 26.06.2019
		// In alcuni casi non arrotondava a 2 cifre decimali ma di più è sballava il totale
		// e quindi lo stato poi della PO
		double t = arrotonda(total, 2);
		
		double value = (double)(t * percentage)/100;
		goodReceipt.setGrValue(String.valueOf(value));
		
		goodReceipt.setGoodReceiptNumber("");

//		Date dataRegistrazione = new Date();
//		DateFormat df = new SimpleDateFormat(DATE_FORMAT);
//		df.format(dataRegistrazione);
//
//		goodReceipt.setRegisterationDate(df.getCalendar().getTime());

		return goodReceipt;
	}

	private static double arrotonda(double value, int numCifreDecimali) {
		double temp = Math.pow(10, numCifreDecimali);
		return Math.round(value * temp) / temp; 
	}
	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws PortletException, IOException {
		String jspPage = ParamUtil.getString(renderRequest, "jspPage","/html/goodreceiptlistvbs/receiver_mode.jsp");
		logger.info("jspPage is {}",jspPage);
		
		if("/html/goodreceiptlistvbs/gruploadform.jsp".equals(jspPage)) {
			if("true".equals(ParamUtil.getString(renderRequest,"validationError", "false"))) {
				PortletSession portletSession = renderRequest.getPortletSession();
				Map<Integer,String> errors = (Map<Integer,String>)portletSession.getAttribute("uploadedBatch");
				logger.debug("Get error map size {}",errors.size());
				int totalRecords = ParamUtil.getInteger(renderRequest,"totalRecords", 0);
				renderRequest.setAttribute("totalRecords", totalRecords);
				renderRequest.setAttribute("faliedRecords", errors.size());
				renderRequest.setAttribute("successRecords", totalRecords - errors.size());
				renderRequest.setAttribute("uploadedBatchErrors", errors);
			}

		}
		super.render(renderRequest, renderResponse);
	}
}
