package com.hp.omp.service.persistence;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.hp.omp.model.BudgetSubCategory;
import com.hp.omp.model.impl.BudgetSubCategoryImpl;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class BudgetSubCategoryFinderImpl extends BudgetSubCategoryPersistenceImpl implements BudgetSubCategoryFinder{

	private static Log _log = LogFactoryUtil.getLog(BudgetSubCategoryFinderImpl.class);
	static final String GET_BUDGET_SUB_CATEGORY_LIST = BudgetSubCategoryFinderImpl.class.getName()+".getBudgetSubCategoryList";
	@SuppressWarnings("unchecked")
	public List<BudgetSubCategory> getBudgetSubCategoryList(int start , int end, String searchFilter) throws SystemException
	{
		Session session=null;
		List<BudgetSubCategory> list = new ArrayList<BudgetSubCategory>();
		try {
		session  = openSession();
		
		String sql = null;
		
		if(StringUtils.isEmpty(searchFilter))
			searchFilter = "%%";
		
		sql = CustomSQLUtil.get(GET_BUDGET_SUB_CATEGORY_LIST);
		SQLQuery query = session.createSQLQuery(sql);
		
		if(query == null){
			_log.error("get budget sub category list error, no sql found");
			return null;
		}
		
		query.setString(0, searchFilter);
		
		query.addEntity("BudgetSubCategoryList", BudgetSubCategoryImpl.class);
		list = (List<BudgetSubCategory>)QueryUtil.list(query, getDialect(), start, end);
		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
		return list;
	}
}
