package com.hp.omp.model.impl;

import com.hp.omp.model.PurchaseOrder;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing PurchaseOrder in entity cache.
 *
 * @author HP Egypt team
 * @see PurchaseOrder
 * @generated
 */
public class PurchaseOrderCacheModel implements CacheModel<PurchaseOrder>,
    Serializable {
    public long purchaseOrderId;
    public String costCenterId;
    public String vendorId;
    public String buyer;
    public String ola;
    public String projectId;
    public String subProjectId;
    public String budgetCategoryId;
    public String budgetSubCategoryId;
    public String activityDescription;
    public String macroDriverId;
    public String microDriverId;
    public String odnpCodeId;
    public String odnpNameId;
    public String totalValue;
    public String currency;
    public String receiverUserId;
    public String fiscalYear;
    public String screenNameRequester;
    public String screenNameReciever;
    public boolean automatic;
    public int status;
    public long createdDate;
    public long createdUserId;
    public String wbsCodeId;
    public String trackingCode;
    public String buyerId;
    public String targaTecnica;
    public String evoLocationId;
    public int tipoPr;
    public String plant;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(55);

        sb.append("{purchaseOrderId=");
        sb.append(purchaseOrderId);
        sb.append(", costCenterId=");
        sb.append(costCenterId);
        sb.append(", vendorId=");
        sb.append(vendorId);
        sb.append(", buyer=");
        sb.append(buyer);
        sb.append(", ola=");
        sb.append(ola);
        sb.append(", projectId=");
        sb.append(projectId);
        sb.append(", subProjectId=");
        sb.append(subProjectId);
        sb.append(", budgetCategoryId=");
        sb.append(budgetCategoryId);
        sb.append(", budgetSubCategoryId=");
        sb.append(budgetSubCategoryId);
        sb.append(", activityDescription=");
        sb.append(activityDescription);
        sb.append(", macroDriverId=");
        sb.append(macroDriverId);
        sb.append(", microDriverId=");
        sb.append(microDriverId);
        sb.append(", odnpCodeId=");
        sb.append(odnpCodeId);
        sb.append(", odnpNameId=");
        sb.append(odnpNameId);
        sb.append(", totalValue=");
        sb.append(totalValue);
        sb.append(", currency=");
        sb.append(currency);
        sb.append(", receiverUserId=");
        sb.append(receiverUserId);
        sb.append(", fiscalYear=");
        sb.append(fiscalYear);
        sb.append(", screenNameRequester=");
        sb.append(screenNameRequester);
        sb.append(", screenNameReciever=");
        sb.append(screenNameReciever);
        sb.append(", automatic=");
        sb.append(automatic);
        sb.append(", status=");
        sb.append(status);
        sb.append(", createdDate=");
        sb.append(createdDate);
        sb.append(", createdUserId=");
        sb.append(createdUserId);
        sb.append(", wbsCodeId=");
        sb.append(wbsCodeId);
        sb.append(", trackingCode=");
        sb.append(trackingCode);
        sb.append(", buyerId=");
        sb.append(buyerId);
        sb.append(", targaTecnica=");
        sb.append(targaTecnica);
        sb.append(", evoLocationId=");
        sb.append(evoLocationId);
        sb.append(", tipoPr=");
        sb.append(tipoPr);
        sb.append(", plant=");
        sb.append(plant);
        sb.append("}");

        return sb.toString();
    }

    public PurchaseOrder toEntityModel() {
        PurchaseOrderImpl purchaseOrderImpl = new PurchaseOrderImpl();

        purchaseOrderImpl.setPurchaseOrderId(purchaseOrderId);

        if (costCenterId == null) {
            purchaseOrderImpl.setCostCenterId(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setCostCenterId(costCenterId);
        }

        if (vendorId == null) {
            purchaseOrderImpl.setVendorId(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setVendorId(vendorId);
        }

        if (buyer == null) {
            purchaseOrderImpl.setBuyer(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setBuyer(buyer);
        }

        if (ola == null) {
            purchaseOrderImpl.setOla(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setOla(ola);
        }

        if (projectId == null) {
            purchaseOrderImpl.setProjectId(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setProjectId(projectId);
        }

        if (subProjectId == null) {
            purchaseOrderImpl.setSubProjectId(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setSubProjectId(subProjectId);
        }

        if (budgetCategoryId == null) {
            purchaseOrderImpl.setBudgetCategoryId(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setBudgetCategoryId(budgetCategoryId);
        }

        if (budgetSubCategoryId == null) {
            purchaseOrderImpl.setBudgetSubCategoryId(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setBudgetSubCategoryId(budgetSubCategoryId);
        }

        if (activityDescription == null) {
            purchaseOrderImpl.setActivityDescription(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setActivityDescription(activityDescription);
        }

        if (macroDriverId == null) {
            purchaseOrderImpl.setMacroDriverId(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setMacroDriverId(macroDriverId);
        }

        if (microDriverId == null) {
            purchaseOrderImpl.setMicroDriverId(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setMicroDriverId(microDriverId);
        }

        if (odnpCodeId == null) {
            purchaseOrderImpl.setOdnpCodeId(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setOdnpCodeId(odnpCodeId);
        }

        if (odnpNameId == null) {
            purchaseOrderImpl.setOdnpNameId(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setOdnpNameId(odnpNameId);
        }

        if (totalValue == null) {
            purchaseOrderImpl.setTotalValue(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setTotalValue(totalValue);
        }

        if (currency == null) {
            purchaseOrderImpl.setCurrency(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setCurrency(currency);
        }

        if (receiverUserId == null) {
            purchaseOrderImpl.setReceiverUserId(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setReceiverUserId(receiverUserId);
        }

        if (fiscalYear == null) {
            purchaseOrderImpl.setFiscalYear(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setFiscalYear(fiscalYear);
        }

        if (screenNameRequester == null) {
            purchaseOrderImpl.setScreenNameRequester(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setScreenNameRequester(screenNameRequester);
        }

        if (screenNameReciever == null) {
            purchaseOrderImpl.setScreenNameReciever(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setScreenNameReciever(screenNameReciever);
        }

        purchaseOrderImpl.setAutomatic(automatic);
        purchaseOrderImpl.setStatus(status);

        if (createdDate == Long.MIN_VALUE) {
            purchaseOrderImpl.setCreatedDate(null);
        } else {
            purchaseOrderImpl.setCreatedDate(new Date(createdDate));
        }

        purchaseOrderImpl.setCreatedUserId(createdUserId);

        if (wbsCodeId == null) {
            purchaseOrderImpl.setWbsCodeId(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setWbsCodeId(wbsCodeId);
        }

        if (trackingCode == null) {
            purchaseOrderImpl.setTrackingCode(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setTrackingCode(trackingCode);
        }

        if (buyerId == null) {
            purchaseOrderImpl.setBuyerId(StringPool.BLANK);
        } else {
            purchaseOrderImpl.setBuyerId(buyerId);
        }

        if (targaTecnica == null) {
        	purchaseOrderImpl.setTargaTecnica(StringPool.BLANK);
        } else {
        	purchaseOrderImpl.setTargaTecnica(targaTecnica);
        }
        
        if (evoLocationId == null) {
        	purchaseOrderImpl.setEvoLocationId(StringPool.BLANK);
        } else {
        	purchaseOrderImpl.setEvoLocationId(evoLocationId);
        }
        
        purchaseOrderImpl.setTipoPr(tipoPr);
        
        if (plant == null) {
        	purchaseOrderImpl.setPlant(StringPool.BLANK);
        } else {
        	purchaseOrderImpl.setPlant(plant);
        }        
        
        purchaseOrderImpl.resetOriginalValues();

        return purchaseOrderImpl;
    }
}
