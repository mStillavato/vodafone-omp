/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service.impl;

import java.util.List;

import com.hp.omp.model.ProjectSubProject;
import com.hp.omp.service.base.ProjectSubProjectLocalServiceBaseImpl;
import com.hp.omp.service.persistence.ProjectSubProjectUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the project sub project local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.ProjectSubProjectLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Ahmed Kamel
 * @see com.hp.omp.service.base.ProjectSubProjectLocalServiceBaseImpl
 * @see com.hp.omp.service.ProjectSubProjectLocalServiceUtil
 */
public class ProjectSubProjectLocalServiceImpl
	extends ProjectSubProjectLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.hp.omp.service.ProjectSubProjectLocalServiceUtil} to access the project sub project local service.
	 */
	public List<ProjectSubProject> getProjectSubProjectByProjectId(Long projectId) throws SystemException{
		return ProjectSubProjectUtil.findByProjectId(projectId);
	}
	
	public List<ProjectSubProject> getProjectSubProjectBySubProjectId(Long subProjectId) throws SystemException{
		return ProjectSubProjectUtil.findBySubProjectId(subProjectId);
	}
	
	public List<ProjectSubProject> getProjectSubProjectByProjectIdAndSubProjectId(Long projectId, Long subProjectId) throws SystemException{
		return ProjectSubProjectUtil.findByProjectIdAndSubProjectId(projectId, subProjectId);
	}
}