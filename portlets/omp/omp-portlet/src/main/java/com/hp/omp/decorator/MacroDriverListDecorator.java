package com.hp.omp.decorator;

import java.io.Serializable;

import org.displaytag.decorator.TableDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.MacroDriver;

public class MacroDriverListDecorator extends TableDecorator implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2685221218697856230L;

	Logger logger = LoggerFactory.getLogger(MacroDriverListDecorator.class);
	
	public String getMacroDriverId() {
		
        return getRemoveAction();
    }
	
	public String getMacroDriverName() {
		
        return getUpdateAction();
    }
	
	public String getRemoveAction() {
		
        StringBuilder sb = new StringBuilder();
        MacroDriver item = (MacroDriver) this.getCurrentRowObject();
      
        long macroDriverId = item.getMacroDriverId();
        String macroDriverName = item.getMacroDriverName();
        sb.append("<a href=\"javascript:removeMacroDriver("+macroDriverId+",'"+macroDriverName+"')\"");
        sb.append("\">");
        sb.append(" Remove ");
        sb.append("</a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
    }	
	
	public String getUpdateAction() {
		
		MacroDriver item = (MacroDriver) this.getCurrentRowObject();
		StringBuilder sb = new StringBuilder();
       
      
		long macroDriverId = item.getMacroDriverId();
        String macroDriverName = item.getMacroDriverName();
        sb.append("<a onclick=\"javascript:updateMacroDriver(this, "+macroDriverId+")\" href='"+macroDriverName+"'");
        sb.append("\"> ");
        sb.append(macroDriverName);
        sb.append(" </a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
        
    }

}
