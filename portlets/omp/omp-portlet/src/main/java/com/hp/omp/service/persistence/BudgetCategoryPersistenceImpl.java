package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchBudgetCategoryException;
import com.hp.omp.model.BudgetCategory;
import com.hp.omp.model.impl.BudgetCategoryImpl;
import com.hp.omp.model.impl.BudgetCategoryModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the budget category service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see BudgetCategoryPersistence
 * @see BudgetCategoryUtil
 * @generated
 */
public class BudgetCategoryPersistenceImpl extends BasePersistenceImpl<BudgetCategory>
    implements BudgetCategoryPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link BudgetCategoryUtil} to access the budget category persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = BudgetCategoryImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(BudgetCategoryModelImpl.ENTITY_CACHE_ENABLED,
            BudgetCategoryModelImpl.FINDER_CACHE_ENABLED,
            BudgetCategoryImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(BudgetCategoryModelImpl.ENTITY_CACHE_ENABLED,
            BudgetCategoryModelImpl.FINDER_CACHE_ENABLED,
            BudgetCategoryImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(BudgetCategoryModelImpl.ENTITY_CACHE_ENABLED,
            BudgetCategoryModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_BUDGETCATEGORY = "SELECT budgetCategory FROM BudgetCategory budgetCategory";
    private static final String _SQL_COUNT_BUDGETCATEGORY = "SELECT COUNT(budgetCategory) FROM BudgetCategory budgetCategory";
    private static final String _ORDER_BY_ENTITY_ALIAS = "budgetCategory.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No BudgetCategory exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(BudgetCategoryPersistenceImpl.class);
    private static BudgetCategory _nullBudgetCategory = new BudgetCategoryImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<BudgetCategory> toCacheModel() {
                return _nullBudgetCategoryCacheModel;
            }
        };

    private static CacheModel<BudgetCategory> _nullBudgetCategoryCacheModel = new CacheModel<BudgetCategory>() {
            public BudgetCategory toEntityModel() {
                return _nullBudgetCategory;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the budget category in the entity cache if it is enabled.
     *
     * @param budgetCategory the budget category
     */
    public void cacheResult(BudgetCategory budgetCategory) {
        EntityCacheUtil.putResult(BudgetCategoryModelImpl.ENTITY_CACHE_ENABLED,
            BudgetCategoryImpl.class, budgetCategory.getPrimaryKey(),
            budgetCategory);

        budgetCategory.resetOriginalValues();
    }

    /**
     * Caches the budget categories in the entity cache if it is enabled.
     *
     * @param budgetCategories the budget categories
     */
    public void cacheResult(List<BudgetCategory> budgetCategories) {
        for (BudgetCategory budgetCategory : budgetCategories) {
            if (EntityCacheUtil.getResult(
                        BudgetCategoryModelImpl.ENTITY_CACHE_ENABLED,
                        BudgetCategoryImpl.class, budgetCategory.getPrimaryKey()) == null) {
                cacheResult(budgetCategory);
            } else {
                budgetCategory.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all budget categories.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(BudgetCategoryImpl.class.getName());
        }

        EntityCacheUtil.clearCache(BudgetCategoryImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the budget category.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(BudgetCategory budgetCategory) {
        EntityCacheUtil.removeResult(BudgetCategoryModelImpl.ENTITY_CACHE_ENABLED,
            BudgetCategoryImpl.class, budgetCategory.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<BudgetCategory> budgetCategories) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (BudgetCategory budgetCategory : budgetCategories) {
            EntityCacheUtil.removeResult(BudgetCategoryModelImpl.ENTITY_CACHE_ENABLED,
                BudgetCategoryImpl.class, budgetCategory.getPrimaryKey());
        }
    }

    /**
     * Creates a new budget category with the primary key. Does not add the budget category to the database.
     *
     * @param budgetCategoryId the primary key for the new budget category
     * @return the new budget category
     */
    public BudgetCategory create(long budgetCategoryId) {
        BudgetCategory budgetCategory = new BudgetCategoryImpl();

        budgetCategory.setNew(true);
        budgetCategory.setPrimaryKey(budgetCategoryId);

        return budgetCategory;
    }

    /**
     * Removes the budget category with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param budgetCategoryId the primary key of the budget category
     * @return the budget category that was removed
     * @throws com.hp.omp.NoSuchBudgetCategoryException if a budget category with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public BudgetCategory remove(long budgetCategoryId)
        throws NoSuchBudgetCategoryException, SystemException {
        return remove(Long.valueOf(budgetCategoryId));
    }

    /**
     * Removes the budget category with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the budget category
     * @return the budget category that was removed
     * @throws com.hp.omp.NoSuchBudgetCategoryException if a budget category with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BudgetCategory remove(Serializable primaryKey)
        throws NoSuchBudgetCategoryException, SystemException {
        Session session = null;

        try {
            session = openSession();

            BudgetCategory budgetCategory = (BudgetCategory) session.get(BudgetCategoryImpl.class,
                    primaryKey);

            if (budgetCategory == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchBudgetCategoryException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(budgetCategory);
        } catch (NoSuchBudgetCategoryException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected BudgetCategory removeImpl(BudgetCategory budgetCategory)
        throws SystemException {
        budgetCategory = toUnwrappedModel(budgetCategory);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, budgetCategory);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(budgetCategory);

        return budgetCategory;
    }

    @Override
    public BudgetCategory updateImpl(
        com.hp.omp.model.BudgetCategory budgetCategory, boolean merge)
        throws SystemException {
        budgetCategory = toUnwrappedModel(budgetCategory);

        boolean isNew = budgetCategory.isNew();

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, budgetCategory, merge);

            budgetCategory.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(BudgetCategoryModelImpl.ENTITY_CACHE_ENABLED,
            BudgetCategoryImpl.class, budgetCategory.getPrimaryKey(),
            budgetCategory);

        return budgetCategory;
    }

    protected BudgetCategory toUnwrappedModel(BudgetCategory budgetCategory) {
        if (budgetCategory instanceof BudgetCategoryImpl) {
            return budgetCategory;
        }

        BudgetCategoryImpl budgetCategoryImpl = new BudgetCategoryImpl();

        budgetCategoryImpl.setNew(budgetCategory.isNew());
        budgetCategoryImpl.setPrimaryKey(budgetCategory.getPrimaryKey());

        budgetCategoryImpl.setBudgetCategoryId(budgetCategory.getBudgetCategoryId());
        budgetCategoryImpl.setBudgetCategoryName(budgetCategory.getBudgetCategoryName());
        budgetCategoryImpl.setDisplay(budgetCategory.isDisplay());

        return budgetCategoryImpl;
    }

    /**
     * Returns the budget category with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the budget category
     * @return the budget category
     * @throws com.liferay.portal.NoSuchModelException if a budget category with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BudgetCategory findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the budget category with the primary key or throws a {@link com.hp.omp.NoSuchBudgetCategoryException} if it could not be found.
     *
     * @param budgetCategoryId the primary key of the budget category
     * @return the budget category
     * @throws com.hp.omp.NoSuchBudgetCategoryException if a budget category with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public BudgetCategory findByPrimaryKey(long budgetCategoryId)
        throws NoSuchBudgetCategoryException, SystemException {
        BudgetCategory budgetCategory = fetchByPrimaryKey(budgetCategoryId);

        if (budgetCategory == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + budgetCategoryId);
            }

            throw new NoSuchBudgetCategoryException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                budgetCategoryId);
        }

        return budgetCategory;
    }

    /**
     * Returns the budget category with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the budget category
     * @return the budget category, or <code>null</code> if a budget category with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BudgetCategory fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the budget category with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param budgetCategoryId the primary key of the budget category
     * @return the budget category, or <code>null</code> if a budget category with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public BudgetCategory fetchByPrimaryKey(long budgetCategoryId)
        throws SystemException {
        BudgetCategory budgetCategory = (BudgetCategory) EntityCacheUtil.getResult(BudgetCategoryModelImpl.ENTITY_CACHE_ENABLED,
                BudgetCategoryImpl.class, budgetCategoryId);

        if (budgetCategory == _nullBudgetCategory) {
            return null;
        }

        if (budgetCategory == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                budgetCategory = (BudgetCategory) session.get(BudgetCategoryImpl.class,
                        Long.valueOf(budgetCategoryId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (budgetCategory != null) {
                    cacheResult(budgetCategory);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(BudgetCategoryModelImpl.ENTITY_CACHE_ENABLED,
                        BudgetCategoryImpl.class, budgetCategoryId,
                        _nullBudgetCategory);
                }

                closeSession(session);
            }
        }

        return budgetCategory;
    }

    /**
     * Returns all the budget categories.
     *
     * @return the budget categories
     * @throws SystemException if a system exception occurred
     */
    public List<BudgetCategory> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the budget categories.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of budget categories
     * @param end the upper bound of the range of budget categories (not inclusive)
     * @return the range of budget categories
     * @throws SystemException if a system exception occurred
     */
    public List<BudgetCategory> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the budget categories.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of budget categories
     * @param end the upper bound of the range of budget categories (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of budget categories
     * @throws SystemException if a system exception occurred
     */
    public List<BudgetCategory> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<BudgetCategory> list = (List<BudgetCategory>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_BUDGETCATEGORY);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_BUDGETCATEGORY;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<BudgetCategory>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<BudgetCategory>) QueryUtil.list(q,
                            getDialect(), start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the budget categories from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (BudgetCategory budgetCategory : findAll()) {
            remove(budgetCategory);
        }
    }

    /**
     * Returns the number of budget categories.
     *
     * @return the number of budget categories
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_BUDGETCATEGORY);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the budget category persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.BudgetCategory")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<BudgetCategory>> listenersList = new ArrayList<ModelListener<BudgetCategory>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<BudgetCategory>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(BudgetCategoryImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
