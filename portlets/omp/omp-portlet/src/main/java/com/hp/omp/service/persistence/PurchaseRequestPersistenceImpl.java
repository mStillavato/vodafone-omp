package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchPurchaseRequestException;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.impl.PurchaseRequestImpl;
import com.hp.omp.model.impl.PurchaseRequestModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the purchase request service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see PurchaseRequestPersistence
 * @see PurchaseRequestUtil
 * @generated
 */
public class PurchaseRequestPersistenceImpl extends BasePersistenceImpl<PurchaseRequest>
    implements PurchaseRequestPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link PurchaseRequestUtil} to access the purchase request persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = PurchaseRequestImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PurchaseRequestModelImpl.ENTITY_CACHE_ENABLED,
            PurchaseRequestModelImpl.FINDER_CACHE_ENABLED,
            PurchaseRequestImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PurchaseRequestModelImpl.ENTITY_CACHE_ENABLED,
            PurchaseRequestModelImpl.FINDER_CACHE_ENABLED,
            PurchaseRequestImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PurchaseRequestModelImpl.ENTITY_CACHE_ENABLED,
            PurchaseRequestModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_PURCHASEREQUEST = "SELECT purchaseRequest FROM PurchaseRequest purchaseRequest";
    private static final String _SQL_COUNT_PURCHASEREQUEST = "SELECT COUNT(purchaseRequest) FROM PurchaseRequest purchaseRequest";
    private static final String _ORDER_BY_ENTITY_ALIAS = "purchaseRequest.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No PurchaseRequest exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(PurchaseRequestPersistenceImpl.class);
    private static PurchaseRequest _nullPurchaseRequest = new PurchaseRequestImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<PurchaseRequest> toCacheModel() {
                return _nullPurchaseRequestCacheModel;
            }
        };

    private static CacheModel<PurchaseRequest> _nullPurchaseRequestCacheModel = new CacheModel<PurchaseRequest>() {
            public PurchaseRequest toEntityModel() {
                return _nullPurchaseRequest;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the purchase request in the entity cache if it is enabled.
     *
     * @param purchaseRequest the purchase request
     */
    public void cacheResult(PurchaseRequest purchaseRequest) {
        EntityCacheUtil.putResult(PurchaseRequestModelImpl.ENTITY_CACHE_ENABLED,
            PurchaseRequestImpl.class, purchaseRequest.getPrimaryKey(),
            purchaseRequest);

        purchaseRequest.resetOriginalValues();
    }

    /**
     * Caches the purchase requests in the entity cache if it is enabled.
     *
     * @param purchaseRequests the purchase requests
     */
    public void cacheResult(List<PurchaseRequest> purchaseRequests) {
        for (PurchaseRequest purchaseRequest : purchaseRequests) {
            if (EntityCacheUtil.getResult(
                        PurchaseRequestModelImpl.ENTITY_CACHE_ENABLED,
                        PurchaseRequestImpl.class,
                        purchaseRequest.getPrimaryKey()) == null) {
                cacheResult(purchaseRequest);
            } else {
                purchaseRequest.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all purchase requests.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(PurchaseRequestImpl.class.getName());
        }

        EntityCacheUtil.clearCache(PurchaseRequestImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the purchase request.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(PurchaseRequest purchaseRequest) {
        EntityCacheUtil.removeResult(PurchaseRequestModelImpl.ENTITY_CACHE_ENABLED,
            PurchaseRequestImpl.class, purchaseRequest.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<PurchaseRequest> purchaseRequests) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (PurchaseRequest purchaseRequest : purchaseRequests) {
            EntityCacheUtil.removeResult(PurchaseRequestModelImpl.ENTITY_CACHE_ENABLED,
                PurchaseRequestImpl.class, purchaseRequest.getPrimaryKey());
        }
    }

    /**
     * Creates a new purchase request with the primary key. Does not add the purchase request to the database.
     *
     * @param purchaseRequestId the primary key for the new purchase request
     * @return the new purchase request
     */
    public PurchaseRequest create(long purchaseRequestId) {
        PurchaseRequest purchaseRequest = new PurchaseRequestImpl();

        purchaseRequest.setNew(true);
        purchaseRequest.setPrimaryKey(purchaseRequestId);

        return purchaseRequest;
    }

    /**
     * Removes the purchase request with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param purchaseRequestId the primary key of the purchase request
     * @return the purchase request that was removed
     * @throws com.hp.omp.NoSuchPurchaseRequestException if a purchase request with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public PurchaseRequest remove(long purchaseRequestId)
        throws NoSuchPurchaseRequestException, SystemException {
        return remove(Long.valueOf(purchaseRequestId));
    }

    /**
     * Removes the purchase request with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the purchase request
     * @return the purchase request that was removed
     * @throws com.hp.omp.NoSuchPurchaseRequestException if a purchase request with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PurchaseRequest remove(Serializable primaryKey)
        throws NoSuchPurchaseRequestException, SystemException {
        Session session = null;

        try {
            session = openSession();

            PurchaseRequest purchaseRequest = (PurchaseRequest) session.get(PurchaseRequestImpl.class,
                    primaryKey);

            if (purchaseRequest == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchPurchaseRequestException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(purchaseRequest);
        } catch (NoSuchPurchaseRequestException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected PurchaseRequest removeImpl(PurchaseRequest purchaseRequest)
        throws SystemException {
        purchaseRequest = toUnwrappedModel(purchaseRequest);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, purchaseRequest);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(purchaseRequest);

        return purchaseRequest;
    }

    @Override
    public PurchaseRequest updateImpl(
        com.hp.omp.model.PurchaseRequest purchaseRequest, boolean merge)
        throws SystemException {
        purchaseRequest = toUnwrappedModel(purchaseRequest);

        boolean isNew = purchaseRequest.isNew();

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, purchaseRequest, merge);

            purchaseRequest.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(PurchaseRequestModelImpl.ENTITY_CACHE_ENABLED,
            PurchaseRequestImpl.class, purchaseRequest.getPrimaryKey(),
            purchaseRequest);

        return purchaseRequest;
    }

    protected PurchaseRequest toUnwrappedModel(PurchaseRequest purchaseRequest) {
        if (purchaseRequest instanceof PurchaseRequestImpl) {
            return purchaseRequest;
        }

        PurchaseRequestImpl purchaseRequestImpl = new PurchaseRequestImpl();

        purchaseRequestImpl.setNew(purchaseRequest.isNew());
        purchaseRequestImpl.setPrimaryKey(purchaseRequest.getPrimaryKey());

        purchaseRequestImpl.setPurchaseRequestId(purchaseRequest.getPurchaseRequestId());
        purchaseRequestImpl.setCostCenterId(purchaseRequest.getCostCenterId());
        purchaseRequestImpl.setVendorId(purchaseRequest.getVendorId());
        purchaseRequestImpl.setBuyer(purchaseRequest.getBuyer());
        purchaseRequestImpl.setOla(purchaseRequest.getOla());
        purchaseRequestImpl.setProjectId(purchaseRequest.getProjectId());
        purchaseRequestImpl.setSubProjectId(purchaseRequest.getSubProjectId());
        purchaseRequestImpl.setBudgetCategoryId(purchaseRequest.getBudgetCategoryId());
        purchaseRequestImpl.setBudgetSubCategoryId(purchaseRequest.getBudgetSubCategoryId());
        purchaseRequestImpl.setActivityDescription(purchaseRequest.getActivityDescription());
        purchaseRequestImpl.setMacroDriverId(purchaseRequest.getMacroDriverId());
        purchaseRequestImpl.setMicroDriverId(purchaseRequest.getMicroDriverId());
        purchaseRequestImpl.setOdnpCodeId(purchaseRequest.getOdnpCodeId());
        purchaseRequestImpl.setOdnpNameId(purchaseRequest.getOdnpNameId());
        purchaseRequestImpl.setTotalValue(purchaseRequest.getTotalValue());
        purchaseRequestImpl.setCurrency(purchaseRequest.getCurrency());
        purchaseRequestImpl.setReceiverUserId(purchaseRequest.getReceiverUserId());
        purchaseRequestImpl.setFiscalYear(purchaseRequest.getFiscalYear());
        purchaseRequestImpl.setScreenNameRequester(purchaseRequest.getScreenNameRequester());
        purchaseRequestImpl.setScreenNameReciever(purchaseRequest.getScreenNameReciever());
        purchaseRequestImpl.setAutomatic(purchaseRequest.isAutomatic());
        purchaseRequestImpl.setStatus(purchaseRequest.getStatus());
        purchaseRequestImpl.setCreatedDate(purchaseRequest.getCreatedDate());
        purchaseRequestImpl.setCreatedUserId(purchaseRequest.getCreatedUserId());
        purchaseRequestImpl.setRejectionCause(purchaseRequest.getRejectionCause());
        purchaseRequestImpl.setWbsCodeId(purchaseRequest.getWbsCodeId());
        purchaseRequestImpl.setTrackingCode(purchaseRequest.getTrackingCode());
        purchaseRequestImpl.setBuyerId(purchaseRequest.getBuyerId());
        purchaseRequestImpl.setTargaTecnica(purchaseRequest.getTargaTecnica());
        purchaseRequestImpl.setEvoLocationId(purchaseRequest.getEvoLocationId());
        purchaseRequestImpl.setTipoPr(purchaseRequest.getTipoPr());
        purchaseRequestImpl.setPlant(purchaseRequest.getPlant());

        return purchaseRequestImpl;
    }

    /**
     * Returns the purchase request with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the purchase request
     * @return the purchase request
     * @throws com.liferay.portal.NoSuchModelException if a purchase request with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PurchaseRequest findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the purchase request with the primary key or throws a {@link com.hp.omp.NoSuchPurchaseRequestException} if it could not be found.
     *
     * @param purchaseRequestId the primary key of the purchase request
     * @return the purchase request
     * @throws com.hp.omp.NoSuchPurchaseRequestException if a purchase request with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public PurchaseRequest findByPrimaryKey(long purchaseRequestId)
        throws NoSuchPurchaseRequestException, SystemException {
        PurchaseRequest purchaseRequest = fetchByPrimaryKey(purchaseRequestId);

        if (purchaseRequest == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + purchaseRequestId);
            }

            throw new NoSuchPurchaseRequestException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                purchaseRequestId);
        }

        return purchaseRequest;
    }

    /**
     * Returns the purchase request with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the purchase request
     * @return the purchase request, or <code>null</code> if a purchase request with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PurchaseRequest fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the purchase request with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param purchaseRequestId the primary key of the purchase request
     * @return the purchase request, or <code>null</code> if a purchase request with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public PurchaseRequest fetchByPrimaryKey(long purchaseRequestId)
        throws SystemException {
        PurchaseRequest purchaseRequest = (PurchaseRequest) EntityCacheUtil.getResult(PurchaseRequestModelImpl.ENTITY_CACHE_ENABLED,
                PurchaseRequestImpl.class, purchaseRequestId);

        if (purchaseRequest == _nullPurchaseRequest) {
            return null;
        }

        if (purchaseRequest == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                purchaseRequest = (PurchaseRequest) session.get(PurchaseRequestImpl.class,
                        Long.valueOf(purchaseRequestId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (purchaseRequest != null) {
                    cacheResult(purchaseRequest);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(PurchaseRequestModelImpl.ENTITY_CACHE_ENABLED,
                        PurchaseRequestImpl.class, purchaseRequestId,
                        _nullPurchaseRequest);
                }

                closeSession(session);
            }
        }

        return purchaseRequest;
    }

    /**
     * Returns all the purchase requests.
     *
     * @return the purchase requests
     * @throws SystemException if a system exception occurred
     */
    public List<PurchaseRequest> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the purchase requests.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of purchase requests
     * @param end the upper bound of the range of purchase requests (not inclusive)
     * @return the range of purchase requests
     * @throws SystemException if a system exception occurred
     */
    public List<PurchaseRequest> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the purchase requests.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of purchase requests
     * @param end the upper bound of the range of purchase requests (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of purchase requests
     * @throws SystemException if a system exception occurred
     */
    public List<PurchaseRequest> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<PurchaseRequest> list = (List<PurchaseRequest>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_PURCHASEREQUEST);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_PURCHASEREQUEST.concat(PurchaseRequestModelImpl.ORDER_BY_JPQL);
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<PurchaseRequest>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<PurchaseRequest>) QueryUtil.list(q,
                            getDialect(), start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the purchase requests from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (PurchaseRequest purchaseRequest : findAll()) {
            remove(purchaseRequest);
        }
    }

    /**
     * Returns the number of purchase requests.
     *
     * @return the number of purchase requests
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_PURCHASEREQUEST);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the purchase request persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.PurchaseRequest")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<PurchaseRequest>> listenersList = new ArrayList<ModelListener<PurchaseRequest>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<PurchaseRequest>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(PurchaseRequestImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
