package com.hp.omp.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.helper.UploadDownloadHelper;
import com.hp.omp.model.CostCenter;
import com.hp.omp.model.CostCenterUser;
import com.hp.omp.model.Project;
import com.hp.omp.model.SubProject;
import com.hp.omp.model.WbsCode;
import com.hp.omp.model.custom.GruppoUsers;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.custom.PrListDTO;
import com.hp.omp.model.custom.PurchaseRequestStatus;
import com.hp.omp.model.custom.SearchDTO;
import com.hp.omp.service.CostCenterLocalServiceUtil;
import com.hp.omp.service.CostCenterUserLocalServiceUtil;
import com.hp.omp.service.ProjectLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.hp.omp.service.SubProjectLocalServiceUtil;
import com.hp.omp.service.WbsCodeLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class PurchaseRequestListPortlet extends MVCPortlet {

	public static final Logger logger = LoggerFactory.getLogger(PurchaseRequestListPortlet.class);
	boolean created = false;
	
	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		try {
			List<WbsCode> wbsCodeList = WbsCodeLocalServiceUtil.getWbsCodes(0, WbsCodeLocalServiceUtil.getWbsCodesCount());
			renderRequest.setAttribute("allWbsCodes", wbsCodeList);
			
			List<Project> projectsList = ProjectLocalServiceUtil.getProjects(0, ProjectLocalServiceUtil.getProjectsCount());
			renderRequest.setAttribute("allProjects", projectsList);
			
		} catch (SystemException e) {
			logger.error("error happened during retrieving WBS code/Projects list",e);
		}
		super.doView(renderRequest, renderResponse);
	}
	
	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
		List<PurchaseRequestStatus> status = getPRListStatus(); 
		request.setAttribute("statusList", status);
		request.setAttribute("createStatus", PurchaseRequestStatus.CREATED);
		request.setAttribute("costCentersList", getCostCentersList(themeDisplay.getUserId()));
		String userType =OmpHelper.getUserRoles(themeDisplay.getUserId());
		request.setAttribute("userType",userType);
		request.setAttribute("userCostCenterId",getUserCostCenterId(themeDisplay.getUserId()));
		
		super.render(request, response);
	}
	
	private List<PurchaseRequestStatus> getPRListStatus(){
		List<PurchaseRequestStatus> statusList = new ArrayList<PurchaseRequestStatus>();
		statusList.add(PurchaseRequestStatus.CREATED);
		statusList.add(PurchaseRequestStatus.SUBMITTED);
		statusList.add(PurchaseRequestStatus.PR_ASSIGNED);
		statusList.add(PurchaseRequestStatus.ASSET_RECEIVED);
		statusList.add(PurchaseRequestStatus.SC_RECEIVED);
		statusList.add(PurchaseRequestStatus.PO_RECEIVED);
		statusList.add(PurchaseRequestStatus.PENDING_IC_APPROVAL);
		statusList.add(PurchaseRequestStatus.REJECTED);
		return statusList;
	}
	
	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		
		 if("searchPr".equals(resourceRequest.getResourceID())) {
			 searchPR(resourceRequest, resourceResponse);
		 }else {
	     String jspPage = "/html/purchaserequestlist/prlist.jsp";
	     
	     try {
	    	 if("getSubProjects".equals(resourceRequest.getResourceID())){
					getSubProjects(resourceRequest,resourceResponse);
	    	 }else{
	    		 ThemeDisplay themeDisplay = (ThemeDisplay)resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
	    	     Integer statusCode = ParamUtil.getInteger(resourceRequest, "status");	
	    	     Integer pageNumber = ParamUtil.getInteger(resourceRequest, "pageNumber", 1);
	    	     String orderby = ParamUtil.getString(resourceRequest, "orderby", "purchaseRequestId");
	    	     String order = ParamUtil.getString(resourceRequest, "order", "desc");
	    	     
	    	     //get search parameters from the request
	    	     long prId = ParamUtil.getLong(resourceRequest, "prId");
	    	     long costCentId = ParamUtil.getLong(resourceRequest, "costCenter");
	    	     String fYear = ParamUtil.getString(resourceRequest, "fYear");
	    	     String project = ParamUtil.getString(resourceRequest, "project");
	    	     String subProject = ParamUtil.getString(resourceRequest, "subProject");
	    	     String wbsCode = ParamUtil.getString(resourceRequest, "wbsCode");
	    	     String searchKeyWord = ParamUtil.getString(resourceRequest, "searchKeyWord");
	    	     boolean miniSeach = ParamUtil.getBoolean(resourceRequest, "isMini",true);
	    	     //fill in SearchDTO with  search parameters
	    	     SearchDTO searchDTO = new SearchDTO();
	    	     searchDTO.setPrId(prId);
	    	     searchDTO.setCostCenterId(costCentId);
	    	     searchDTO.setPrStatus(statusCode);
	    	     searchDTO.setFiscalYear(fYear);
	    	     searchDTO.setProjectName(project);
	    	     searchDTO.setSubProjectName(subProject);
	    	     searchDTO.setWbsCode(wbsCode);
	    	     searchDTO.setGlobalSearchKeyWord(searchKeyWord);
	    	     searchDTO.setPageNumber(pageNumber);
	    	     searchDTO.setPageSize(OmpHelper.PAGE_SIZE);
	    	     searchDTO.setOrder(order);
	    	     searchDTO.setOrderby(orderby);
	    	     searchDTO.setMiniSeach(miniSeach);
	    	     searchDTO.setMiniSearchValue(searchKeyWord);
	    	    	 OmpRoles userRole = OmpHelper.getUserRole(themeDisplay.getUserId());
	    	    	/* long costCenterId = 0;
	    	    	 if(OmpRoles.OMP_ENG.equals(userRole)) {
	    	    		 CostCenterUser costCenterUser = CostCenterUserLocalServiceUtil.getCostCenterUserByUserId(themeDisplay.getUserId());
	    	    		 costCenterId = costCenterUser.getCostCenterId();
	    	    	 }*/
	    			List<PrListDTO> prList = PurchaseRequestLocalServiceUtil.getPurchaseRequestList(searchDTO,userRole.getCode(),themeDisplay.getUserId());
	    			Long totalCount = PurchaseRequestLocalServiceUtil.getPurchaseRequestCount(searchDTO,userRole.getCode(),themeDisplay.getUserId());//getPRListByStatusAndCostCenterCount(statusCode, costCenterId, userRole.getCode());
	    			resourceRequest.setAttribute("numberOfPages", getTotalNumberOfPages(totalCount, OmpHelper.PAGE_SIZE));
	    			resourceRequest.setAttribute("pageNumber", pageNumber);
	    			resourceRequest.setAttribute("prList", prList);
	    			
	    			 getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
	    	 }
	     
	     } catch(Exception e) {
	    	 logger.error("Error in get PRL for Enginner",e );
	    	 SessionErrors.add(resourceRequest, "Unassgined-CostCenter");
	     }
		 }
		
	}
	
	public void searchPR(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws PortletException, IOException {

		String jspPage = "/html/purchaserequestlist/prlist.jsp";
	     
	     try {
	    	
	    		 ThemeDisplay themeDisplay = (ThemeDisplay)resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
    	     
	    	     String prSearchValue = ParamUtil.getString(resourceRequest, "searchPrValue");
	    	     SearchDTO searchDTO = new SearchDTO();
	    	     searchDTO.setPrSearchValue(prSearchValue);
	    	    	 OmpRoles userRole = OmpHelper.getUserRole(themeDisplay.getUserId());
	    			List<PrListDTO> prList = PurchaseRequestLocalServiceUtil.prSearch(searchDTO,userRole.getCode(),themeDisplay.getUserId());
	    			resourceRequest.setAttribute("prList", prList);
	    			
	    			 getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
	    	 
	     
	     } catch(Exception e) {
	    	 logger.error("Error in get search result  for Enginner",e );
	     }
	}
	
	
	private Integer getTotalNumberOfPages(Long totalNubmerOfResult, Integer pageSize) {
        return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
}

	private List<CostCenter> getCostCentersList(long userId)
			throws PortletException {

		List<CostCenter> allCostCenters = new ArrayList<CostCenter>();
		try {
			OmpRoles userRole = OmpHelper.getUserRole(userId);

			if ("OMP_CONTROLLER".equalsIgnoreCase(userRole.name())) {
				//allCostCenters = CostCenterLocalServiceUtil.getCostCenters(0,CostCenterLocalServiceUtil.getCostCentersCount());
				allCostCenters = CostCenterLocalServiceUtil.getCostCentersList(0,
						CostCenterLocalServiceUtil.getCostCentersCount(), 
						null, 
						GruppoUsers.GNED.getCode());
			} else {
				CostCenterUser costCenterUser = CostCenterUserLocalServiceUtil.getCostCenterUserByUserId(userId);
				CostCenter costCenter = CostCenterLocalServiceUtil.getCostCenter(costCenterUser.getCostCenterId());
				allCostCenters.add(costCenter);
			}
		} catch (SystemException e) {
			throw new PortletException(e);
		} catch (PortalException e) {
			throw new PortletException(e);
		}

		return allCostCenters;
	}
	
	
	private long getUserCostCenterId(long userId)
			throws PortletException {
			
			long ccId =0;
		
		try {
			OmpRoles userRole = OmpHelper.getUserRole(userId);

			if ("OMP_ENG".equalsIgnoreCase(userRole.name()) || "OMP_OBSERVER".equalsIgnoreCase(userRole.name())) {
				CostCenterUser costCenterUser = CostCenterUserLocalServiceUtil.getCostCenterUserByUserId(userId);
				if(costCenterUser != null){
					ccId = costCenterUser.getCostCenterId();
				}
				
			}
			
		} catch (SystemException e) {
			throw new PortletException(e);
		} catch (PortalException e) {
			throw new PortletException(e);
		}

		return ccId;
	}
	 
	public void getSubProjects(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws SystemException, IOException {
		JSONObject optionsJSON = JSONFactoryUtil.createJSONObject();
		Long project = ParamUtil.getLong(resourceRequest, "project");
		List<SubProject> subProjectList = getSubProjects(project);
		if(subProjectList == null){
			subProjectList = new ArrayList<SubProject>();
		}
		JSONArray jsonsubProjectsArray = JSONFactoryUtil.createJSONArray();
		for (SubProject subProject : subProjectList) {
			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("id", subProject.getSubProjectId());
			json.put("name", subProject.getSubProjectName());
			
			jsonsubProjectsArray.put(json);
		}
		optionsJSON.put("subProjects", jsonsubProjectsArray);
		PrintWriter writer = resourceResponse.getWriter();
		writer.write(optionsJSON.toString());
	}
	
	private List<SubProject> getSubProjects(Long projectId) throws SystemException, NumberFormatException{
		return SubProjectLocalServiceUtil.getSubProjectByProjectId(Long.valueOf(projectId));
	}
}

