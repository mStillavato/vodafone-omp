package com.hp.omp.model.custom;

import java.util.ArrayList;
import java.util.List;

public class OMPAggregateReportExcelRow implements ExcelTableRow {
	
	List<Object> fieldList = null;
	int fieldListSize = 0;
	
	public OMPAggregateReportExcelRow(){
		fieldList = new ArrayList<Object>();
	}
	
	public Object getFieldAtIndex(int index) {
		Object field = fieldList.get(index);
		if(field == null){
			return "";
		}
		return field;
	}

	public void setFieldAtIndex(Object field, int index) {
		fieldList.add(index, field);
		fieldListSize = fieldList.size();
	}

	public int getTotalNumberFields() {
		if(fieldListSize >0 ){
			return fieldListSize;
		}
		return fieldList.size();
	}

}
