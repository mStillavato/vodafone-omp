package com.hp.omp.portlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.model.Buyer;
import com.hp.omp.model.custom.GruppoUsers;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.impl.BuyerImpl;
import com.hp.omp.service.BuyerLocalServiceUtil;
import com.hp.omp.validation.BuyerValidator;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class BuyerAdminPortlet extends MVCPortlet{
	Logger logger = LoggerFactory.getLogger(BuyerAdminPortlet.class);


	
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws PortletException, IOException {
		String jspPage = ParamUtil.getString(renderRequest, "jspPage","/html/buyeradmin/buyerlist.jsp");
		String searchFilter = ParamUtil.getString(renderRequest, "searchFilter","").toUpperCase();
		logger.info("jspPage is {}",jspPage);
		
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String userType = OmpHelper.getUserRoles(themeDisplay.getUserId());
		
		int gruppoUser = GruppoUsers.GNED.getCode();
		if(userType.equals(OmpRoles.OMP_CONTROLLER_VBS.getLabel())
				|| userType.equals(OmpRoles.OMP_APPROVER_VBS.getLabel())
				|| userType.equals(OmpRoles.OMP_ENG_VBS.getLabel())){
			gruppoUser = GruppoUsers.VBS.getCode();
		}
		
		if("/html/buyeradmin/buyerform.jsp".equals(jspPage)) {
	        String buyerId = ParamUtil.getString(renderRequest,"buyerId", "");
			String validationError = ParamUtil.getString(renderRequest,"validationError", "false");
			if((buyerId != null && !"".equals(buyerId)) || "true".equals(validationError )) {
				try {
					Buyer buyer;
					if("true".equals(validationError)) {
						buyer = getBuyerFromRequest(renderRequest);  
					} else {
						
						buyer = BuyerLocalServiceUtil.getBuyer(Long.valueOf(buyerId));
					}
					logger.debug("Buyer with Id: {}", buyer.getBuyerId());
					renderRequest.setAttribute("buyer", buyer);
					
				} catch (NumberFormatException e) {
					logger.error("Error in buyer Id. ",e);
				} catch (PortalException e) {
					logger.error("Error in get buyer with Id {}",buyerId,e);
				} catch (SystemException e) {
					logger.error("Error in get buyer with Id {}",buyerId,e);
				}
			} else {
				Buyer buyer = new BuyerImpl();
				buyer.setDisplay(true);
				buyer.setGruppoUsers(gruppoUser);
				
				renderRequest.setAttribute("buyer", buyer);
			}
			
		} else if("/html/buyeradmin/buyerlist.jsp".equals(jspPage)){
			try {
				Integer pageNumber = ParamUtil.getInteger(renderRequest, "pageNumber", 1);
				Integer pageSize = OmpHelper.PAGE_SIZE;
				int start = (pageNumber - 1) * pageSize;
				int end = start + pageSize;
				
				List<Buyer> buyerList = BuyerLocalServiceUtil.getBuyersList(start, end, searchFilter, gruppoUser);
				renderRequest.setAttribute("buyerList", buyerList);
				
				int totalCount = BuyerLocalServiceUtil.getBuyersCountList(searchFilter, gruppoUser);
				logger.debug("Buyers Total Count = " + totalCount);
				
//				if (searchFilter == null || searchFilter.trim().length() == 0)
//					totalCount = BuyerLocalServiceUtil.getBuyersCount();
//				else
//					totalCount = BuyerLocalServiceUtil.getBuyersCountList(searchFilter, gruppoUser);
				
				renderRequest.setAttribute("numberOfPages", getTotalNumberOfPages(totalCount, OmpHelper.PAGE_SIZE));
				renderRequest.setAttribute("pageNumber", 1);
			} catch (SystemException e) {
				logger.error("Error in get total buyer counts.",e);
			}  
		} 
		super.render(renderRequest, renderResponse);
	}
	
	
	

	
	

	
	public void addBuyer(ActionRequest request, ActionResponse response) throws IOException, PortletException{
		
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		String userType = OmpHelper.getUserRoles(themeDisplay.getUserId());
		
        Buyer buyer = getBuyerFromRequest(request);
        List<String> errors = new ArrayList<String>();
        boolean validBuyer = false;
		try {
			validBuyer = BuyerValidator.validateBuyer(buyer, errors);
			logger.info("Buyer validation result: {}", validBuyer);
		} catch (SystemException e) {
			errors.add("validation-exception");
			logger.error("Error in Validating Buyer.", e);
		}  
        if(validBuyer) {
    		int gruppoUser = GruppoUsers.GNED.getCode();
    		if(userType.equals(OmpRoles.OMP_CONTROLLER_VBS.getLabel())
    				|| userType.equals(OmpRoles.OMP_APPROVER_VBS.getLabel())
    				|| userType.equals(OmpRoles.OMP_ENG_VBS.getLabel())){
    			gruppoUser = GruppoUsers.VBS.getCode();
    			
    		}
    		buyer.setGruppoUsers(gruppoUser);
    		
	        if(buyer.getBuyerId() != 0) {
	        	try {
					BuyerLocalServiceUtil.updateBuyer(buyer);
					//SessionMessages.add(request, "Buyer-updated");
					request.setAttribute("successMSG", "Buyer name [" + buyer.getName() + "] successfully updated");
				} catch (SystemException e) {
					errors.add("updating-exception");
					request.setAttribute("errorMSG", "Error in update buyer ["+buyer.getName()+"]");
					logger.error("Error in update Buyer with Id {}", buyer.getBuyerId(), e);
				}
	        } else {
	        	try {
	        		BuyerLocalServiceUtil.addBuyer(buyer);
					//SessionMessages.add(request, "Buyer-saved");
					request.setAttribute("successMSG", "Buyer name [" + buyer.getName() + "] successfully saved");

				} catch (SystemException e) {
					errors.add("saveing-exception");
					request.setAttribute("errorMSG", "Error in update buyer ["+buyer.getName()+"]");
					logger.error("Error in add new Buyer",e);
				}
	        }
	        sendRedirect(request, response);
        } else {
        	for (String error : errors) {
				SessionErrors.add(request, error);
			}
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("validationError", "true");
			response.setRenderParameter("jspPage", "/html/buyeradmin/buyerform.jsp");
        }
	}

	private Buyer getBuyerFromRequest(PortletRequest request) {
		Buyer buyer = new BuyerImpl();
		buyer.setBuyerId(ParamUtil.getLong(request, "buyerId"));
		buyer.setName(ParamUtil.getString(request, "name"));
		buyer.setDisplay(ParamUtil.getBoolean(request, "display"));
		return buyer;
	}
	
	@Override
    public void serveResource(ResourceRequest resourceRequest,
                  ResourceResponse resourceResponse) throws IOException,
                  PortletException {
		
		String action = resourceRequest.getParameter("action");
		
		if("exportList".equals(action)){
			exportList(resourceRequest, resourceResponse);
			
		} else {
			
			if("deleteBuyer".equals(resourceRequest.getResourceID())) {
				try {
					Buyer buyer = BuyerLocalServiceUtil.getBuyer(ParamUtil.getLong(resourceRequest, "buyerId"));
					BuyerLocalServiceUtil.deleteBuyer(ParamUtil.getLong(resourceRequest, "buyerId"));
					resourceRequest.setAttribute("statusMessage", "Buyer name ["+buyer.getName()+"] deleted successfully");
				} catch (PortalException e) {
					resourceRequest.setAttribute("statusMessage", "Can't delete Used Buyer.");
					logger.error("Error in delete buyer",e);
				} catch (SystemException e) {
					resourceRequest.setAttribute("statusMessage", "Can't delete Used Buyer.");
					logger.error("Error in delete buyer",e);
				}
			}
		    String jspPage = "/html/buyeradmin/buyertable.jsp";
		    Integer pageNumber = ParamUtil.getInteger(resourceRequest, "pageNumber", 1);
			try {
				resourceRequest.setAttribute("numberOfPages", getTotalNumberOfPages(BuyerLocalServiceUtil.getBuyersCount(),OmpHelper.PAGE_SIZE));
			} catch (SystemException e) {
				logger.error("Error in get total count of buyers",e);
			}
			resourceRequest.setAttribute("pageNumber", pageNumber);
			try {
				resourceRequest.setAttribute("buyerList", BuyerLocalServiceUtil.getBuyersList(pageNumber, OmpHelper.PAGE_SIZE));
			} catch (SystemException e) {
				logger.error("Error in get buyers list",e);
			}
			getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
		}
	}
	private Integer getTotalNumberOfPages(Integer totalNubmerOfResult, Integer pageSize) {
        return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
	}
	
	public void searchFilter(ActionRequest request, ActionResponse response) throws IOException, PortletException{
		response.setRenderParameter("searchFilter", "%" + ParamUtil.getString(request, "buyerName") + "%");  
	}

	private void exportList(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException{
		
		logger.info("inside exportList");
		
		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String userType = OmpHelper.getUserRoles(themeDisplay.getUserId());
		int gruppoUser = GruppoUsers.GNED.getCode();
		if(userType.equals(OmpRoles.OMP_CONTROLLER_VBS.getLabel())
				|| userType.equals(OmpRoles.OMP_APPROVER_VBS.getLabel())
				|| userType.equals(OmpRoles.OMP_ENG_VBS.getLabel())){
			gruppoUser = GruppoUsers.VBS.getCode();
			
		}
		
		String searchFilter = ParamUtil.getString(resourceRequest, "searchFilter","").toUpperCase();
		
		logger.info("searchFilter = " + searchFilter);
		
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		
		String fileName = "OMP_Buyer_"+formatter.format(date)+".xls";
		resourceResponse.setContentType("application/vnd.ms-excel");
		
		resourceResponse.addProperty(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=\""+fileName+"\"");			
		resourceResponse.addProperty(HttpHeaders.CACHE_CONTROL,"max-age=3600, must-revalidate");

		logger.debug("exporting.." + fileName);
		HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(resourceResponse);
		try {
			int totalCount = 0;
			
			if (searchFilter == null || searchFilter.trim().length() == 0)
				totalCount = BuyerLocalServiceUtil.getBuyersCount();
			else
				totalCount = BuyerLocalServiceUtil.getBuyersCountList(searchFilter, gruppoUser);

			logger.debug("Buyer Total Count = " + totalCount);
			List<com.hp.omp.model.Buyer> BuyersList = BuyerLocalServiceUtil.getBuyersList(1, totalCount, searchFilter, gruppoUser);
			BuyerLocalServiceUtil.exportBuyersAsExcel(BuyersList, getBuyerReportHeader(), httpServletResponse.getOutputStream());

		} catch (SystemException e) {
			logger.error("Error in exportBuyer", e);
			e.printStackTrace();
		}
	}
	
	private List<Object> getBuyerReportHeader() {
		List<Object> header = new ArrayList<Object>();
		header.add("NAME");			//0
		header.add("DISPLAY");		//1
		
		return header;
	}
}