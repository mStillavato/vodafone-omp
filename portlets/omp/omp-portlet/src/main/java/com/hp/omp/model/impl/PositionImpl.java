/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.model.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.model.GoodReceipt;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.custom.IAndCType;
import com.hp.omp.model.custom.OmpFormatterUtil;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.custom.PositionStatus;
import com.hp.omp.model.custom.PurchaseRequestStatus;
import com.hp.omp.model.custom.TipoPr;
import com.hp.omp.service.GoodReceiptLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.PrincipalThreadLocal;
import com.liferay.portal.service.UserLocalServiceUtil;

/**
 * The extended model implementation for the Position service. Represents a row in the &quot;POSITION&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.model.Position} interface.
 * </p>
 *
 * @author Brian Wing Shun Chan
 */
public class PositionImpl extends PositionBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a position model instance should use the {@link com.hp.omp.model.Position} interface instead.
	 */
	private transient final static Logger LOGGER = LoggerFactory.getLogger(OmpHelper.class);
	public static final String DATE_FORMAT = "dd/MM/yyyy";
	boolean positionEditBtton=false;
	private String iCApprovalText = null;
	private String approverName;
	private String approvalStatus;
	private String positionStatusLabel;
	private boolean approveBtn;
	private boolean addGrBtn;
	private boolean editGrBtn;
	private String wbsCodeName;
	private String formattedUnitCost;
	private String commaFormattedUnitCost;


	public PositionImpl() {
	}




	public void setCommaFormattedUnitCost(String commaFormattedUnitCost) {
		this.commaFormattedUnitCost = commaFormattedUnitCost;
	}

	public String getCommaFormattedUnitCost() {
		if(super.getActualUnitCost()!=null && ! "".equals(super.getActualUnitCost())) {
			return super.getActualUnitCost().replace(".", ",");
		}else {
			return super.getActualUnitCost();
		}
	}



	public void setPositionStatusLabel(String positionStatusLabel) {
		this.positionStatusLabel = positionStatusLabel;
	}

	public void setFormattedUnitCost(String formatedUnitCost) {
		this.formattedUnitCost = formatedUnitCost;
	}
	public String getFormattedUnitCost() {
		if(super.getActualUnitCost()!=null && ! "".equals(super.getActualUnitCost())) {
			return OmpFormatterUtil.formatNumberByLocale(Double.valueOf(super.getActualUnitCost()), Locale.ITALY);
		}else {
			return super.getActualUnitCost();
		}
	}

	public void setApproveBtn(boolean approveBtn) {
		this.approveBtn = approveBtn;
	}
	public boolean isApproveBtn() {
		try {
			PurchaseRequest pr=PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.valueOf(super.getPurchaseRequestId()));

			if(pr.getStatus()==PurchaseRequestStatus.PENDING_IC_APPROVAL.getCode() && super.getIcApproval() !="" && super.isApproved() !=true) {
				return true;	
			}else {
				return false;
			}
		}catch(Exception e) {
			LOGGER.error("error in checking is approve button "+e);
		}
		return false;
	}

	public String getPositionStatusLabel() {
		String status="";
		try {
			PositionStatus positionStatus = getPositionStatus();
			status = positionStatus.getLabel();

			if(positionStatus.NEW.getLabel().equals(status)) {
				PurchaseRequest pr=PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.valueOf(super.getPurchaseRequestId()));
				if(pr.getStatus()==PurchaseRequestStatus.PENDING_IC_APPROVAL.getCode() || pr.getStatus()==PurchaseRequestStatus.CREATED.getCode()) {
					if(super.getIcApproval() !=null && ! "".equals(super.getIcApproval())) {
						if(super.isApproved()) {
							status=PositionStatus.IC_APPROVED.getLabel();
						}else {
							status=PositionStatus.PENDING_IC_APPROVAL.getLabel();
						}
					}
				}else if(pr.getStatus()==PurchaseRequestStatus.SUBMITTED.getCode()) {
					status=PositionStatus.SUBMITTED.getLabel();
				}else if(pr.getStatus()==PurchaseRequestStatus.PR_ASSIGNED.getCode()) {
					status=PositionStatus.ASSIGNED.getLabel();
				}
			}
		}
		catch(Exception e) {

		}
		return status;

	}

	public void setApproverName(String approverName) {
		this.approverName = approverName;
	}

	public String getApproverName() {
		User user = null;
		try {
			if(super.getApprover() !=null && ! "".equals(super.getApprover())) {
				user = UserLocalServiceUtil.getUser(Long.valueOf(super.getApprover()));
			}
		} catch (Exception e) {
			LOGGER.error("error in getting aprrover name "+e);
		}
		if (user != null) {
			return user.getScreenName();
		}
		return "N/A";
	}

	public void setApprovalStatus(String approvalStatus) {
		this.approvalStatus = approvalStatus;
	}

	public String getApprovalStatus() {

		if(super.getIcApproval() !=null && !"".equals(super.getIcApproval())) {
			if((IAndCType.ICFIELD.getCode()==Integer.valueOf(super.getIcApproval()) || IAndCType.ICTEST.getCode()==Integer.valueOf(super.getIcApproval())) && ! super.isApproved()) {
				return PositionStatus.PENDING_IC_APPROVAL.getLabel();
			}else if((IAndCType.ICFIELD.getCode()==Integer.valueOf(super.getIcApproval()) || IAndCType.ICTEST.getCode()==Integer.valueOf(super.getIcApproval())) && super.isApproved() ) {
				return PositionStatus.IC_APPROVED.getLabel();
			}
		}
		return "";
	}

	public boolean getPositionEditBtton() {
		return positionEditBtton;
	}

	public void setPositionEditBtton(boolean positionEditBtton) {
		this.positionEditBtton = positionEditBtton;
	}

	public PositionStatus getPositionStatus() {

		try {
			long posGrs=GoodReceiptLocalServiceUtil.getPositionGRCount(String.valueOf(super.getPositionId()));
			PurchaseRequest purchaseRequest = PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.parseLong(super.getPurchaseRequestId()));
			int tipoPr = purchaseRequest.getTipoPr();

			if(posGrs !=0) {
				long posClosedGrs=GoodReceiptLocalServiceUtil.getPositionClosedGRCount(String.valueOf(super.getPositionId()));
				if(posGrs==posClosedGrs) {
					return PositionStatus.GR_CLOSED;	
				}else{
					return PositionStatus.GR_REQUESTED;
				}

			}else if(
						(
							(getAssetNumber() != null && !"".equals(getAssetNumber()) 
							&& (getShoppingCart() == null || "".equals(getShoppingCart())) 
							&& (getPoNumber() == null || "".equals(getPoNumber()))) 
							&& (tipoPr == TipoPr.NORMAL.getCode())
							//&& (tipoPr == TipoPr.NORMAL.getCode() || tipoPr == TipoPr.NOT_NSS.getCode())
						)
						
						|| 
						
						(
							(getAssetNumber() == null || "".equals(getAssetNumber())) 
							&& (getShoppingCart() == null || "".equals(getShoppingCart())) 
							&& (getPoNumber() == null || "".equals(getPoNumber())) 
							&& (tipoPr == TipoPr.VBS.getCode())					
						)
					) {
				return PositionStatus.ASSET_RECEIVED;
				
			} else if(
						(
							(
								getAssetNumber() != null && !"".equals(getAssetNumber()) 
								&& (getShoppingCart() != null && !"".equals(getShoppingCart())) 
								&& (getPoNumber() == null || "".equals(getPoNumber()))
							) 
						&& (tipoPr == TipoPr.NORMAL.getCode())
						)
						
						|| 
						
						(
							((getPoNumber() == null || "".equals(getPoNumber()))) 
							&& (tipoPr == TipoPr.NSS.getCode() || tipoPr == TipoPr.VBS.getCode())
						)
					) {
				return PositionStatus.SC_RECEIVED;
				
			} else if (
						(
							(
								getAssetNumber() != null && !"".equals(getAssetNumber()) 
								&& (getShoppingCart() != null && !"".equals(getShoppingCart())) 
								&& (getPoNumber() != null && !"".equals(getPoNumber()))
							) 
							&& (tipoPr == TipoPr.NORMAL.getCode())
						)
						
						|| 
						
						(
							((getPoNumber() != null && !"".equals(getPoNumber()))) 
							&& (tipoPr == TipoPr.NSS.getCode() || tipoPr == TipoPr.NOT_NSS.getCode() || tipoPr == TipoPr.VBS.getCode())
						)
					) {
				return PositionStatus.PO_RECEIVED;
			}
			
			return PositionStatus.NEW;
		}catch(Exception e) {
			LOGGER.debug("error while get position status "+e);
		}
		return PositionStatus.NEW;
	}

	public String getDeliveryDateText() {
		if(super.getDeliveryDate() != null) {
			DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
			return dateFormat.format(super.getDeliveryDate());
		}
		return "";
	}
	public String getOfferDateText() {
		if(super.getOfferDate() != null) {
			DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
			return dateFormat.format(super.getOfferDate());
		}
		return "";
	}

	public String getFieldStatus()throws PortalException, SystemException{
		long userId =  PrincipalThreadLocal.getUserId();
		OmpRoles userRole = OmpHelper.getUserRole(userId);
		switch (userRole) {
		case OMP_CONTROLLER:
			return "";
		case OMP_ANTEX:
			return "";

		default:
			return "readonly";
		}
	}

	public String getCheckboxStatus()throws PortalException, SystemException{
		long userId =  PrincipalThreadLocal.getUserId();
		OmpRoles userRole = OmpHelper.getUserRole(userId);
		switch (userRole) {
		case OMP_CONTROLLER:
			return "";
		case OMP_ANTEX:
			return "";

		default:
			return "disabled";
		}
	}

	public Double getSumPositionGRPercentage() throws SystemException {
		return GoodReceiptLocalServiceUtil.getSumGRPercentageToPosition(String.valueOf(this.getPositionId()));
	}

	public List<GoodReceipt> getPositionGoodReceipts() throws SystemException{
		long id = super.getPositionId();
		if(id != 0) {
			return GoodReceiptLocalServiceUtil.getPositionGoodReceipts(String.valueOf(id));
		} else {
			return new ArrayList<GoodReceipt>();
		}
	}


	public String getiCApprovalText() {
		if(super.getIcApproval()!=null && ! "".equals(super.getIcApproval())) {
			return IAndCType.getStatus(Integer.valueOf(super.getIcApproval())).getLabel();	
		}
		return "";
	}

	public void setiCApprovalText(String iCApprovalText) {
		this.iCApprovalText = iCApprovalText;
	}


	/*public String getWbsCodeName() throws PortalException , SystemException{

		if(super.getWbsCodeId()!=null && ! "".equals(super.getWbsCodeId())) {
			WbsCode wbsCode = WbsCodeLocalServiceUtil.getWbsCode(Long.valueOf(super.getWbsCodeId()));
			wbsCodeName = wbsCode.getWbsCodeName();
		}

		return wbsCodeName;
	}*/

	public boolean isAddGrBtn() throws PortalException, SystemException {

		boolean isAllowed = false;
		long userId = PrincipalThreadLocal.getUserId();
		OmpRoles userRole = OmpHelper.getUserRole(userId);
		String userType = userRole.getLabel();
		String icAppr = super.getIcApproval();

		if ("CONTROLLER".equals(userType) || "CONTROLLER_VBS".equals(userType)) {
			isAllowed = true;

		} else if ((icAppr != null && !"".equals(icAppr))
				&& ("NDSO".equals(userType) || "TC".equals(userType)  || "APPROVER_VBS".equals(userType))) {
			isAllowed = true;

		} else if ((icAppr == null || "".equals(icAppr))
				&& ("ENG".equals(userType)  || "ENG_VBS".equals(userType))) {
			isAllowed = true;
		}
		return isAllowed;

	}




	public void setAddGrBtn(boolean addGrBtn) {
		this.addGrBtn = addGrBtn;
	}



	public boolean isEditGrBtn() throws PortalException, SystemException {

		boolean isAllowed = false;
		long userId = PrincipalThreadLocal.getUserId();
		OmpRoles userRole = OmpHelper.getUserRole(userId);
		String userType = userRole.getLabel();
		String icAppr = super.getIcApproval();

		if ("CONTROLLER".equals(userType) || "CONTROLLER_VBS".equals(userType) || "ANTEX".equals(userType)) {
			isAllowed = true;

		} else if ((icAppr != null && !"".equals(icAppr))
				&& ("NDSO".equals(userType) || "TC".equals(userType)  || "APPROVER_VBS".equals(userType))) {
			isAllowed = true;

		} else if ((icAppr == null || "".equals(icAppr))
				&& ("ENG".equals(userType) || "ENG_VBS".equals(userType))) {
			isAllowed = true;
		}
		return isAllowed;

	}

	public void setEditGrBtn(boolean editGrBtn) {
		this.editGrBtn = editGrBtn;
	}

}