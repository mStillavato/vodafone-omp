package com.hp.taglib;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;

public class HasRole extends TagSupport {

		private String roleName;
		private long userId;
		
		@Override
		public int doStartTag() throws JspException {
			// handle conditional behavior
			boolean result = false;
			try {
				User user = UserLocalServiceUtil.getUser(userId);
				Role role = RoleLocalServiceUtil.getRole(user.getCompanyId(), roleName);
				result = UserLocalServiceUtil.hasRoleUser(role.getRoleId(), userId);
			} catch (SystemException e) {
				e.printStackTrace();
				throw new JspException("Error in check user has role", e);
			} catch (PortalException e) {
				e.printStackTrace();
				throw new JspException("Error in check user has role", e);
			}
			try {
			 JspWriter out = pageContext.getOut();
	         out.println( result );
			}catch(IOException e) {
				e.printStackTrace();
				throw new JspException("Error in check user has role", e);
			}
			return super.doStartTag();
		}

		public String getRoleName() {
			return roleName;
		}

		public void setRoleName(String roleName) {
			this.roleName = roleName;
		}

		public long getUserId() {
			return userId;
		}

		public void setUserId(long userId) {
			this.userId = userId;
		}

	
}
