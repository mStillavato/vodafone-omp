package com.hp.omp.portlet;

import java.io.IOException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.model.MacroDriver;
import com.hp.omp.model.MacroMicroDriver;
import com.hp.omp.model.PurchaseOrder;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.impl.MacroDriverImpl;
import com.hp.omp.service.MacroDriverLocalServiceUtil;
import com.hp.omp.service.MacroMicroDriverLocalServiceUtil;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class MacroDriverAdminPortlet
 */
public class MacroDriverAdminPortlet extends MVCPortlet {
 
	private transient Logger logger = LoggerFactory.getLogger(MacroDriverAdminPortlet.class);
	private String errorMSG = "";
	private String successMSG = "";
	private String ACTION_PARAM= "action";
	private String ADD_ACTION= "add";
	private String UPDATE_ACTION= "update";
	private String DELETE_ACTION= "delete";
	private String PAGINATION_ACTION= "pagination";

	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		super.doView(renderRequest, renderResponse);
	}
	
	
	/* (non-Javadoc)
	 * @see javax.portlet.GenericPortlet#render(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		viewAddMacroDriver(request, response);
		super.render(request, response);
	}
	
	public void viewAddMacroDriver(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		String action = renderRequest.getParameter(ACTION_PARAM);
		
		if(ADD_ACTION.equals(action)){
			renderRequest.setAttribute(ACTION_PARAM, ADD_ACTION);
			renderRequest.setAttribute("display",true);
		}else if(UPDATE_ACTION.equals(action)){
			String macroDriverId = renderRequest.getParameter("macroDriverId");
			MacroDriver macroDriver = getMacroDriver(macroDriverId);
			
			logger.debug("render update action for : "+macroDriver.getMacroDriverName());
			logger.debug("Display = "+macroDriver.getDisplay());
			
			renderRequest.setAttribute(ACTION_PARAM, UPDATE_ACTION);
			renderRequest.setAttribute("macroDriverId", macroDriver.getMacroDriverId());
			renderRequest.setAttribute("macroDriverName", macroDriver.getMacroDriverName());
			renderRequest.setAttribute("display", macroDriver.getDisplay());
		}
	}

	
	@Override
    public void serveResource(ResourceRequest resourceRequest,
                  ResourceResponse resourceResponse) throws IOException,
                  PortletException {
		String action=resourceRequest.getParameter(ACTION_PARAM);
		if(DELETE_ACTION.equals(action)){
			deleteMacroDriver(resourceRequest, resourceResponse);
		}else if(PAGINATION_ACTION.equals(action)){
			paginationList(resourceRequest, resourceResponse);
		}
		
		clearMessages();
           super.serveResource(resourceRequest, resourceResponse);
    }
	
	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#processAction(javax.portlet.ActionRequest, javax.portlet.ActionResponse)
	 */
	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {

		clearMessages();
		String action = actionRequest.getParameter(ACTION_PARAM);
		String macroDriverName = actionRequest.getParameter("macroDriverName");
		boolean display=ParamUtil.getBoolean(actionRequest,"display");
		String operation = "";
		
		logger.debug("macroDriverName="+macroDriverName);
		logger.debug("display = "+display);
		
		logger.debug("creating MacroDriver object ...");
		MacroDriver newMacroDriver = new MacroDriverImpl();
		newMacroDriver.setMacroDriverName(macroDriverName);
		newMacroDriver.setDisplay(display);
		logger.debug("MacroDriver object created !!");
		try {
			if(ADD_ACTION.equals(action)){
				operation = "added";
				logger.debug("adding new MacroDriver ["+macroDriverName+"] in database ...");
				if(MacroDriverLocalServiceUtil.getMacroDriverByName(macroDriverName) == null){
					
					MacroDriverLocalServiceUtil.addMacroDriver(newMacroDriver);
					
					successMSG = "MacroDriver name ["+macroDriverName+"] successfully Added";
					
				}else{
					errorMSG = "MacroDriver name ["+macroDriverName+"] already existing";
				}
			}else if(UPDATE_ACTION.equals(action)){
				operation = "updated";
				String macroDriverId = actionRequest.getParameter("macroDriverId");
				
				newMacroDriver.setMacroDriverId(Integer.valueOf(macroDriverId));
				
				if(! (isMacroDriverNameExists(newMacroDriver, macroDriverName))){
					
					logger.debug("updating  MacroDriver ["+macroDriverId+"] in database ...");
					
					
					MacroDriverLocalServiceUtil.updateMacroDriver(newMacroDriver);
					successMSG = "MacroDriver Name ["+macroDriverName+"] successfully Updated";
				}else{
					errorMSG = "MacroDriver name ["+macroDriverName+"] already existing";
				}
				
			}
			
			
		} catch (SystemException e) {
			errorMSG = "MacroDriver ["+macroDriverName+"] not "+operation+".";
			logger.error("error happened while "+operation+" MacroDriver ["+macroDriverName+"] in database.");
			logger.error(e.getMessage(), e);
		}catch (NumberFormatException nfe) {
			errorMSG = "MacroDriver ["+macroDriverName+"] not "+operation+".";
			logger.error("error happened while "+operation+" MacroDriver ["+macroDriverName+"] in database.");
			logger.error(nfe.getMessage(), nfe);
		}
		
		actionRequest.setAttribute("errorMSG", errorMSG);
		actionRequest.setAttribute("successMSG", successMSG);
		super.processAction(actionRequest, actionResponse);
	}
	
	private void retrieveMacroDriverListToRequest(PortletRequest renderRequest) throws SystemException{
		try {
			String searchFilter = ParamUtil.getString(renderRequest, "searchFilter","");
			int totalCount = MacroDriverLocalServiceUtil.getMacroDriversCount();

			Integer pageNumber = ParamUtil.getInteger(renderRequest, "pageNumber", 1);
			Integer pageSize = OmpHelper.PAGE_SIZE;
			int start = (pageNumber - 1) * pageSize;
			int end = start + pageSize;

			renderRequest.setAttribute("numberOfPages", getTotalNumberOfPages(totalCount, pageSize));
			renderRequest.setAttribute("pageNumber", pageNumber);


			//List<MacroDriver> macroDriverList = MacroDriverLocalServiceUtil.getMacroDrivers(start, end);
			List<MacroDriver> macroDriverList = MacroDriverLocalServiceUtil.getMacroDrivers(start, end, searchFilter);

			renderRequest.setAttribute("macroDriverList", macroDriverList);
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			errorMSG = "can not retrieve MacroDriver list";
			renderRequest.setAttribute("errorMSG", errorMSG);
		}

	}
	
	public void deleteMacroDriver(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse)
			throws PortletException, IOException {
		clearMessages();
		
		String macroDriverId=resourceRequest.getParameter("macroDriverId");
		String macroDriverName=resourceRequest.getParameter("macroDriverName");
		String action=resourceRequest.getParameter("action");
		String jspPage = "/html/macrodriveradmin/macrodriverlist.jsp";
		
		if(Validator.isBlank(action)){
			errorMSG = "No Action in the request";
		}else if(Validator.isBlank(macroDriverId)){
			errorMSG = "No Budget Category Id in the request";
		}else{
			
			try {
				List<PurchaseOrder> purchaseOrderList = PurchaseOrderLocalServiceUtil.getPurchaseOrderByMacroDriverId(macroDriverId);
				List<PurchaseRequest> purchaseRequestList = PurchaseRequestLocalServiceUtil.getPurchaseRequestByMacroDriverId(macroDriverId);
				List<MacroMicroDriver> macroMicroDrivers = MacroMicroDriverLocalServiceUtil.getMacroMicroDriverByMacroDriverId(Long.valueOf(macroDriverId));
				
				if(purchaseOrderList != null && ! (purchaseOrderList.isEmpty())){
					errorMSG = "Can Not remove MacroDriver assigned to Purchase Order, MacroDriver name ["+macroDriverName+"]";
				}else if(purchaseRequestList != null && ! (purchaseRequestList.isEmpty())){
					errorMSG = "Can Not remove MacroDriver assigned to Purchase Request, MacroDriver name ["+macroDriverName+"]";
				}else if(macroMicroDrivers !=null && !(macroMicroDrivers.isEmpty())){
					errorMSG = "Can Not remove MacroDriver has Micro Drivers";
				}
				else{
					MacroDriverLocalServiceUtil.deleteMacroDriver(Long.valueOf(macroDriverId));
					successMSG = "MacroDriver name ["+macroDriverName+"] successfully deleted";
				}
				
			} catch (NumberFormatException e) {
				logger.error(e.getMessage());
				errorMSG = "MacroDriver Id is invalid";
			} catch (PortalException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove MacroDriver";
			} catch (SystemException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove MacroDriver [most probably MacroDriver is assigned.]";
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove MacroDriver";
			}
			
		}
		
		//retrieve updated list
		try {
			retrieveMacroDriverListToRequest(resourceRequest);
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			errorMSG = "can not retrieve MacroDriver list";
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
		
	}
	
	private MacroDriver getMacroDriver(String macroDriverId){
		MacroDriver macroDriver = null;
		try {
			macroDriver = MacroDriverLocalServiceUtil.getMacroDriver(Integer.valueOf(macroDriverId));
		} catch (NumberFormatException e) {
			logger.error("MacroDriver Id has wrong format", e);
		} catch (PortalException e) {
			logger.error("unexpected error", e);
		} catch (SystemException e) {
			logger.error("unexpected error", e);
		}
		
		return macroDriver;
	}
	
	private boolean isMacroDriverNameExists(MacroDriver newMacroDriver, String macroDriverName) throws SystemException{
		
		MacroDriver oldMacroDriver = MacroDriverLocalServiceUtil.getMacroDriverByName(macroDriverName);
		boolean isExists = false;
		
		if(oldMacroDriver != null && 
				(newMacroDriver.getMacroDriverId() != oldMacroDriver.getMacroDriverId()) ){
			isExists = true;
		}
		return isExists;
	}
	
	private void paginationList(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse) throws PortletException, IOException{
		
		String jspPage = "/html/macrodriveradmin/macrodriverlist.jsp";
		try {
			retrieveMacroDriverListToRequest(resourceRequest);
		} catch (SystemException e) {
		logger.error("error happened in pagination ... ",e);
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
	}
	
	private Integer getTotalNumberOfPages(Integer totalNubmerOfResult, Integer pageSize) {
        return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
	}
	
	private void clearMessages(){
		errorMSG = "";
		successMSG = "";
	}
	
	public void searchFilter(ActionRequest request, ActionResponse response) throws IOException, PortletException{
		response.setRenderParameter("searchFilter", "%" + ParamUtil.getString(request, "macroDriverAdminName") + "%");  
	}
}
