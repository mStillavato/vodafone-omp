/**
 * 
 */
package com.hp.omp.helper;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.CostCenter;
import com.hp.omp.model.CostCenterUser;
import com.hp.omp.model.Project;
import com.hp.omp.model.SubProject;
import com.hp.omp.model.WbsCode;
import com.hp.omp.model.custom.GruppoUsers;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.custom.PurchaseOrderStatus;
import com.hp.omp.model.custom.SearchDTO;
import com.hp.omp.portlet.PurchaseOrderPortlet;
import com.hp.omp.service.CostCenterLocalServiceUtil;
import com.hp.omp.service.CostCenterUserLocalServiceUtil;
import com.hp.omp.service.ProjectLocalServiceUtil;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;
import com.hp.omp.service.SubProjectLocalServiceUtil;
import com.hp.omp.service.WbsCodeLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.ParamUtil;

/**
 * @author gewaly
 *
 */
public class POSearchHelper {
	
	Logger logger = LoggerFactory.getLogger(PurchaseOrderPortlet.class);

	public SearchDTO populatePO(SearchDTO searchDTO, Long userId) throws SystemException{
		
		if(searchDTO == null){
			searchDTO= new SearchDTO();
		}
			//PO number
//			List<Long> allPONumbers = PurchaseOrderLocalServiceUtil.getPONumbers();
//			searchDTO.setAllPONumbers(allPONumbers);
			
			//PO WBS CODE
			List<WbsCode> wbsCodeList = WbsCodeLocalServiceUtil.getWbsCodes(0, WbsCodeLocalServiceUtil.getWbsCodesCount());
			searchDTO.setAllWbsCodes(wbsCodeList);
			
			//PO Projects
			List<Project> projectList = ProjectLocalServiceUtil.getProjects(0, ProjectLocalServiceUtil.getProjectsCount());
			searchDTO.setAllProjects(projectList);
			
			//PO Sub projects
			if(StringUtils.isNotBlank(searchDTO.getProjectName())){
				List<SubProject> subProjectList = getSubProjects(Long.valueOf(searchDTO.getProjectName()));
				if(subProjectList != null && !subProjectList.isEmpty()){
					searchDTO.setAllSubProjects(subProjectList);
				}
			}
	
			//populate rest filters
			populate(searchDTO, userId);
			

		return searchDTO;
	}
	
	public SearchDTO populate(SearchDTO searchDTO, Long userId) throws SystemException{
			if(searchDTO == null){
				searchDTO= new SearchDTO();
			}
			
//			//PO Status
			List<PurchaseOrderStatus> allPOStatus = Arrays.asList(PurchaseOrderStatus.values());
			searchDTO.setAllPOStatus(allPOStatus);
//			
//			//Position Fiscal Years
//			List<String> allFiscalYears = PositionLocalServiceUtil.getAllFiscalYears();
//			searchDTO.setAllFiscalYears(allFiscalYears);
//			
//			//Position WBS Codes
//			List<String> allWbsCodes = PositionLocalServiceUtil.getAllWBSCodes();
//			searchDTO.setAllWbsCodes(allWbsCodes);
			
			//cost center list
			//List<CostCenter> allCostCenters = CostCenterLocalServiceUtil.getCostCenters(0, CostCenterLocalServiceUtil.getCostCentersCount());
			int gruppoUser = GruppoUsers.GNED.getCode();
			if(OmpHelper.isControllerVbs(userId) || 
					OmpHelper.isApproverVbs(userId)	|| 
					OmpHelper.isEngVbs(userId)){
				gruppoUser = GruppoUsers.VBS.getCode();
			}
			List<CostCenter> allCostCenters = CostCenterLocalServiceUtil.getCostCentersList(0,
					CostCenterLocalServiceUtil.getCostCentersCount(), 
					null, 
					gruppoUser);
			searchDTO.setAllCostCenters(allCostCenters);
			
			//disable cost center and add assigned value for eng and observer
			if(OmpHelper.isEng(userId) || OmpHelper.isObserver(userId)){
				searchDTO.setDisableCostCenter(true);
				CostCenterUser costCenterUser = CostCenterUserLocalServiceUtil.getCostCenterUserByUserId(userId);
				searchDTO.setCostCenterId(costCenterUser.getCostCenterId());
			}
			
		
		return searchDTO;
	}
	
	public SearchDTO getSearchDTO(PortletRequest request) {
		SearchDTO searchDTO = new SearchDTO();
		
		String miniSearch = request.getParameter("miniSearch");
		String miniSearchValue = request.getParameter("miniSearchValue");
		if("true".equals(miniSearch)){
			searchDTO.setMiniSeach(true);
			searchDTO.setMiniSearchValue(miniSearchValue);
			
			return searchDTO;
		}
		
		boolean emptyAttributes = true;
		
		String poNumber = ParamUtil.getString(request, "poNumber");
		int poStatus = ParamUtil.getInteger(request, "poStatus",-1);
		String fiscalYear = ParamUtil.getString(request, "fiscalYear");
		String wbsCode = ParamUtil.getString(request, "wbsCode");
		String projectName = ParamUtil.getString(request, "project");
		String subProject = ParamUtil.getString(request, "subProject");
		Long costCenter = ParamUtil.getLong(request, "costCenter",-1);
		

		if (!"".equalsIgnoreCase(poNumber)) {
			searchDTO.setPoNumber(poNumber);
			emptyAttributes = false;
		}
		if (-1 != poStatus) {
			searchDTO.setPoStatus(poStatus);
			emptyAttributes = false;
		}
		if (!"".equalsIgnoreCase(fiscalYear)) {
			searchDTO.setFiscalYear(fiscalYear);
			emptyAttributes = false;
		}
		if (!"".equalsIgnoreCase(wbsCode)) {
			searchDTO.setWbsCode(wbsCode);
			emptyAttributes = false;
		}
		if (!"".equalsIgnoreCase(projectName)) {
			searchDTO.setProjectName(projectName);
			emptyAttributes = false;
		}
		if (!"".equalsIgnoreCase(subProject)) {
			searchDTO.setSubProjectName(subProject);
			emptyAttributes = false;
		}
		if ( costCenter != null) {
			searchDTO.setCostCenterId(costCenter);
			emptyAttributes = false;
		}
		
		if(emptyAttributes){
			return null;
		}
		return searchDTO;
	}
	

	public void getSubProjects(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws SystemException, IOException{
		try {
		JSONObject optionsJSON = JSONFactoryUtil.createJSONObject();
		List<SubProject> subProjectList=null;
		Long projectId = ParamUtil.getLong(resourceRequest, "project");
		Long subProjectId =ParamUtil.getLong(resourceRequest, "subProjectId");
		if(projectId !=null && projectId !=0) {
		Project project=ProjectLocalServiceUtil.getProject(projectId);
		if(project.getDisplay()) {
			subProjectList = getSubProjects(projectId);
		}else {
			if(subProjectId !=null && subProjectId !=0) {
			SubProject subProject=SubProjectLocalServiceUtil.getSubProject(subProjectId);
			subProjectList =new ArrayList<SubProject>();
			subProjectList.add(subProject);
			}
		 }
		}
		if(subProjectList == null){
			subProjectList = new ArrayList<SubProject>();
		}
		JSONArray jsonsubProjectsArray = JSONFactoryUtil.createJSONArray();
		for (SubProject subProject : subProjectList) {
			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("id", subProject.getSubProjectId());
			json.put("name", subProject.getSubProjectName());
			json.put("display", subProject.getDisplay());
			
			jsonsubProjectsArray.put(json);
		}
		optionsJSON.put("subProjects", jsonsubProjectsArray);
		PrintWriter writer = resourceResponse.getWriter();
		writer.write(optionsJSON.toString());
		}catch(Exception e) {
			logger.error("error while getSubProjects Ajax", e);
		}
	}

	private List<SubProject> getSubProjects(Long projectId) throws SystemException, NumberFormatException{
		return SubProjectLocalServiceUtil.getSubProjectByProjectId(Long.valueOf(projectId));
	}
	
}
