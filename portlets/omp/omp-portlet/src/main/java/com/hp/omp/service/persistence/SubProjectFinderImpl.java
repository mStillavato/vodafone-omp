package com.hp.omp.service.persistence;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.hp.omp.model.Project;
import com.hp.omp.model.SubProject;
import com.hp.omp.model.impl.ProjectImpl;
import com.hp.omp.model.impl.SubProjectImpl;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class SubProjectFinderImpl extends SubProjectPersistenceImpl implements SubProjectFinder{
	
	private static Log _log = LogFactoryUtil.getLog(SubProjectFinderImpl.class);
	static final String GET_SUB_PROJECTS = SubProjectFinderImpl.class.getName()+".getSubProjects";
	static final String GET_SUB_PROJECTS_LIST = SubProjectFinderImpl.class.getName()+".getSubProjectsList";

	@SuppressWarnings("unchecked")
	public List<SubProject> getSubProjectByProjectId(Long projectId) throws SystemException {
		Session session = null;
		List<SubProject> list = new ArrayList<SubProject>();

		try {
			session = openSession();
			
			String sql = CustomSQLUtil.get(GET_SUB_PROJECTS);
			if(_log.isDebugEnabled()){
				_log.debug("SQL=["+ sql+"]");
				_log.debug("value=["+ projectId+"]");
			}
			SQLQuery query = session.createSQLQuery(sql);
			
			if(query == null){
				_log.error("get sub projects error, no sql found");
				return null;
			}
			query.setLong(0, projectId);
			query.addEntity("SubProject", SubProjectImpl.class);
			
			list = (List<SubProject>)QueryUtil.list(query, getDialect(),  QueryUtil.ALL_POS, QueryUtil.ALL_POS);
		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
		
		return list;
	}

	public List<SubProject> getSubProjectsList(int start, int end,
			String searchFilter) throws SystemException {
		Session session=null;
		List<SubProject> list = new ArrayList<SubProject>();
		try {
		session  = openSession();
		
		String sql = null;
		
		if(StringUtils.isEmpty(searchFilter))
			searchFilter = "%%";
		
		sql = CustomSQLUtil.get(GET_SUB_PROJECTS_LIST);
		SQLQuery query = session.createSQLQuery(sql);
		
		if(query == null){
			_log.error("get sub project list error, no sql found");
			return null;
		}
		
		query.setString(0, searchFilter);
		
		query.addEntity("SubProjectList", SubProjectImpl.class);
		list = (List<SubProject>)QueryUtil.list(query, getDialect(), start, end);
		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
		return list;
	}

}
