package com.hp.omp.portlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.model.CostCenter;
import com.hp.omp.model.CostCenterUser;
import com.hp.omp.model.impl.CostCenterUserImpl;
import com.hp.omp.service.CostCenterLocalServiceUtil;
import com.hp.omp.service.CostCenterUserLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class CostCenterUsersPortlet
 */
public class CostCenterUsersPortlet extends MVCPortlet {

	private transient Logger logger = LoggerFactory.getLogger(CostCenterUsersPortlet.class);
	private static final String VODAFONE_ORG="Vodafone";
	
	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		
		buildCostCenterUserAddView(renderRequest);
		
		
		super.doView(renderRequest, renderResponse);
	}
	
	
	private void buildCostCenterUserAddView(RenderRequest renderRequest){
		List<User> usersList = null;
		String errorMSG = "";
		boolean showDropdowns = true;
		try {
			List<Organization> OrganizationList = OrganizationLocalServiceUtil.getOrganizations(0, OrganizationLocalServiceUtil.getOrganizationsCount());
			for (Organization organization : OrganizationList) {
				if(VODAFONE_ORG.equalsIgnoreCase(organization.getName())){
					usersList = UserLocalServiceUtil.getOrganizationUsers(organization.getOrganizationId());
					break;
				}
			}
			
		} catch (SystemException e) {
			logger.error("error while retreiving users list", e);
			logger.error(e.getMessage(), e);
		}
		List<CostCenter> costCenterList = null;
		try {
			costCenterList = CostCenterLocalServiceUtil.getCostCenters(0, CostCenterLocalServiceUtil.getCostCentersCount());
		} catch (SystemException e) {
			logger.error("error while retreiving cost center list", e);
		}
		

		if(usersList == null || usersList.isEmpty()){
			errorMSG += "No users found under Vodafone Orgnization <br/>";
			showDropdowns = false;
		}else if(costCenterList == null || costCenterList.isEmpty())	{
			errorMSG += "No cost centers found <br/>";
			showDropdowns = false;
		}else{
			renderRequest.setAttribute("usersList", usersList);
			renderRequest.setAttribute("costCenterList", costCenterList);
		}
		
		renderRequest.setAttribute("errorMSG", errorMSG);
		renderRequest.setAttribute("showDropdowns", showDropdowns);
	}
	
	private void buildCostCenterUserListView(PortletRequest renderRequest){
		Map<CostCenterUser, Map<User, CostCenter>> costcenterMap = new HashMap<CostCenterUser, Map<User, CostCenter>>();
		Map<User, CostCenter> usercostcenterMap = null;
		try {
			
			int totalCount = CostCenterUserLocalServiceUtil.getCostCenterUsersCount();
			
			Integer pageNumber = ParamUtil.getInteger(renderRequest, "pageNumber", 1);
			Integer pageSize = OmpHelper.PAGE_SIZE;
			int start = (pageNumber - 1) * pageSize;
			int end = start + pageSize;
			
			renderRequest.setAttribute("numberOfPages", getTotalNumberOfPages(totalCount, pageSize));
			renderRequest.setAttribute("pageNumber", pageNumber);
			
			List<CostCenterUser> costcenterUserList = CostCenterUserLocalServiceUtil.getCostCenterUsers(start, end);
			for (CostCenterUser costCenterUser : costcenterUserList) {
				usercostcenterMap = new HashMap<User, CostCenter>();
				long  userId = costCenterUser.getUserId();
				long costCenterId = costCenterUser.getCostCenterId();
				User user;
				try {
					user = UserLocalServiceUtil.getUser(userId);
				} catch (PortalException e) {
					logger.error("User Id ["+userId+"] not found");
					continue;
				}
				CostCenter costcenter;
				try {
					costcenter = CostCenterLocalServiceUtil.getCostCenter(costCenterId);
				} catch (PortalException e) {
					logger.error("cost center Id ["+costCenterId+"] not found");
					continue;
				}
				
				usercostcenterMap.put(user, costcenter);
				costcenterMap.put(costCenterUser, usercostcenterMap);
			}
			
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			renderRequest.setAttribute("errorMSG", "somthing went wrong");
		} 
		
		renderRequest.setAttribute("costcenterMap", costcenterMap);
	}
	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#processAction(javax.portlet.ActionRequest, javax.portlet.ActionResponse)
	 */
	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {
		String costCenterId = actionRequest.getParameter("costcenters");
		String userId = actionRequest.getParameter("users");
		String error = "";
		try {
			if(Validator.isBlank(costCenterId)){
				error = "cost center not selected";
			}else if(Validator.isBlank(userId)){
				if(!(Validator.isBlank(error))){
					error+=" and ";
				}
				error+="user not selected";
			}else{
				if(CostCenterUserLocalServiceUtil.getCostCenterUserByUserId(Long.valueOf(userId)) == null){
					CostCenterUser costCenterUser = new CostCenterUserImpl();
					costCenterUser.setCostCenterId(Long.valueOf(costCenterId));
					costCenterUser.setUserId(Long.valueOf(userId));
					CostCenterUserLocalServiceUtil.addCostCenterUser(costCenterUser);
					actionRequest.setAttribute("successMSG_2","cost center assigned to user");
				}else{
					error = "user already assigned to cost center";
				}
				
			}
			
			
			
		} catch (NumberFormatException nfe) {
			logger.error(nfe.getMessage(), nfe);
			actionRequest.setAttribute("errorMSG_2", "error happened ["+nfe.getMessage()+"]");
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			actionRequest.setAttribute("errorMSG_2", "error happened ["+e.getMessage()+"]");
		}
		actionRequest.setAttribute("errorMSG_2",error);
		super.processAction(actionRequest, actionResponse);
	}




	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#serveResource(javax.portlet.ResourceRequest, javax.portlet.ResourceResponse)
	 */
	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		String action=resourceRequest.getParameter("action");
		if("removeuser".equals(action)){
			removerCostCenteFromUser(resourceRequest, resourceResponse);
		}else if("pagination".equals(action)){
			paginationList(resourceRequest, resourceResponse);
		}
		super.serveResource(resourceRequest, resourceResponse);
	}
 
	private void removerCostCenteFromUser(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse)throws PortletException, IOException{
		String errorMSG = "";
		String successMSG = "";
		
		String costCenterUserId=resourceRequest.getParameter("costCenterUserId");
		String action=resourceRequest.getParameter("action");
		String jspPage = "/html/costcenterusers/costcenteruserlist.jsp";
		
		if(Validator.isBlank(action)){
			errorMSG = "No Action in the request";
		}else if(Validator.isBlank(costCenterUserId)){
			errorMSG = "No Cost Center Id in the request";
		}else{
			
			try {
				CostCenterUserLocalServiceUtil.deleteCostCenterUser(Long.valueOf(costCenterUserId));
				successMSG = "cost center removed successfully";
			} catch (NumberFormatException nfe) {
				logger.error(nfe.getMessage(), nfe);
				errorMSG = "error happened ["+nfe.getMessage()+"]";
			} catch (SystemException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove Cost Center form the user";
			} catch (PortalException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove Cost Center form the user";
			}
			
		}
		
		//retrieve updated list
		buildCostCenterUserListView(resourceRequest);
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
	}

	private void paginationList(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse) throws PortletException, IOException{
		
		String jspPage = "/html/costcenterusers/costcenteruserlist.jsp";
		
		buildCostCenterUserListView(resourceRequest);
		
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
	}
	
	private Integer getTotalNumberOfPages(Integer totalNubmerOfResult, Integer pageSize) {
        return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
}
}
