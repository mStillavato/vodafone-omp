package com.hp.omp.model.impl;

import com.hp.omp.model.POAudit;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing POAudit in entity cache.
 *
 * @author HP Egypt team
 * @see POAudit
 * @generated
 */
public class POAuditCacheModel implements CacheModel<POAudit>, Serializable {
    public long auditId;
    public Long purchaseOrderId;
    public int status;
    public Long userId;
    public long changeDate;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{auditId=");
        sb.append(auditId);
        sb.append(", purchaseOrderId=");
        sb.append(purchaseOrderId);
        sb.append(", status=");
        sb.append(status);
        sb.append(", userId=");
        sb.append(userId);
        sb.append(", changeDate=");
        sb.append(changeDate);
        sb.append("}");

        return sb.toString();
    }

    public POAudit toEntityModel() {
        POAuditImpl poAuditImpl = new POAuditImpl();

        poAuditImpl.setAuditId(auditId);
        poAuditImpl.setPurchaseOrderId(purchaseOrderId);
        poAuditImpl.setStatus(status);
        poAuditImpl.setUserId(userId);

        if (changeDate == Long.MIN_VALUE) {
            poAuditImpl.setChangeDate(null);
        } else {
            poAuditImpl.setChangeDate(new Date(changeDate));
        }

        poAuditImpl.resetOriginalValues();

        return poAuditImpl;
    }
}
