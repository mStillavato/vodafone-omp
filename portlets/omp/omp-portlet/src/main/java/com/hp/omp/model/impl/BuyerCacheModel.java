package com.hp.omp.model.impl;

import com.hp.omp.model.Buyer;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

/**
 * The cache model class for representing Buyer in entity cache.
 *
 * @author HP Egypt team
 * @see Buyer
 * @generated
 */
public class BuyerCacheModel implements CacheModel<Buyer>, Serializable {
    public long buyerId;
    public String name;
    public boolean display;
    public int gruppoUsers;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{buyerId=");
        sb.append(buyerId);
        sb.append(", name=");
        sb.append(name);
        sb.append(", display=");
        sb.append(display);
        sb.append(", gruppoUsers=");
        sb.append(gruppoUsers);
        sb.append("}");

        return sb.toString();
    }

    public Buyer toEntityModel() {
        BuyerImpl buyerImpl = new BuyerImpl();

        buyerImpl.setBuyerId(buyerId);

        if (name == null) {
            buyerImpl.setName(StringPool.BLANK);
        } else {
            buyerImpl.setName(name);
        }

        buyerImpl.setDisplay(display);
        
        buyerImpl.setGruppoUsers(gruppoUsers);

        buyerImpl.resetOriginalValues();

        return buyerImpl;
    }
}
