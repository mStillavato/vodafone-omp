package com.hp.omp.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


import com.hp.omp.model.custom.GrExcelRow;

public class GrExcelFileHelper {

	public static List<GrExcelRow> readExcelFile(File inputWorkbook) throws IOException  {

		FileInputStream file = new FileInputStream(inputWorkbook);

		//Create Workbook instance holding reference to .xlsx file
		XSSFWorkbook workbook = new XSSFWorkbook(file);

		//Get first/desired sheet from the workbook
		XSSFSheet sheet = workbook.getSheetAt(0);

		List<GrExcelRow> trackingList = new ArrayList<GrExcelRow>(); 
		for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
			XSSFRow row = sheet.getRow(rowIndex);
			GrExcelRow trackingCode = new GrExcelRow();
			if (row != null) {
				for(int columNum = 0 ; columNum < row.getPhysicalNumberOfCells(); columNum++) {
					switch (columNum) {
						case 0:	// Position
						if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
							trackingCode.setPosition(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
						} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
							trackingCode.setPosition(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
						}
						break;
	
						case 1:	// PoOrderId
							if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
								trackingCode.setPoOrderId(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
								trackingCode.setPoOrderId(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
							}
							break;
	
						case 2:	// Cdc
							trackingCode.setCdc(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							break;
	
						case 3:	// Richiedente
							trackingCode.setRichiedente(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							break;
	
						case 4:	// Valore Totale Pos
							if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
								trackingCode.setValoreTotPos(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
								trackingCode.setValoreTotPos(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
							}
							break;
	
						case 5:	// Quantita Pos
							if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
								trackingCode.setQtaPos(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
								trackingCode.setQtaPos(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
							}
							break;
	
						case 6:	// Valore Richiesto
							if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
								trackingCode.setValoreRichiesto(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
								trackingCode.setValoreRichiesto(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
							}
							break;
	
						case 7:
							if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
								trackingCode.setPercentualeRichiesta(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
								trackingCode.setPercentualeRichiesta(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
							}       				  
							break;
					}
				}
			}
			trackingList.add(trackingCode);
		}
		file.close();
		return trackingList;
	}
}
