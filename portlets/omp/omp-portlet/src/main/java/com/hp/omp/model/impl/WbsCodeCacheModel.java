package com.hp.omp.model.impl;

import com.hp.omp.model.WbsCode;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

/**
 * The cache model class for representing WbsCode in entity cache.
 *
 * @author HP Egypt team
 * @see WbsCode
 * @generated
 */
public class WbsCodeCacheModel implements CacheModel<WbsCode>, Serializable {
    public long wbsCodeId;
    public String wbsCodeName;
    public String description;
    public boolean display;
    public String wbsOwnerName;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(9);

        sb.append("{wbsCodeId=");
        sb.append(wbsCodeId);
        sb.append(", wbsCodeName=");
        sb.append(wbsCodeName);
        sb.append(", description=");
        sb.append(description);
        sb.append(", display=");
        sb.append(display);
        sb.append(", wbsOwnerName=");
        sb.append(wbsOwnerName);
        sb.append("}");

        return sb.toString();
    }

    public WbsCode toEntityModel() {
        WbsCodeImpl wbsCodeImpl = new WbsCodeImpl();

        wbsCodeImpl.setWbsCodeId(wbsCodeId);

        if (wbsCodeName == null) {
            wbsCodeImpl.setWbsCodeName(StringPool.BLANK);
        } else {
            wbsCodeImpl.setWbsCodeName(wbsCodeName);
        }

        if (description == null) {
            wbsCodeImpl.setDescription(StringPool.BLANK);
        } else {
            wbsCodeImpl.setDescription(description);
        }

        wbsCodeImpl.setDisplay(display);
        
        if (wbsOwnerName == null) {
            wbsCodeImpl.setWbsOwnerName(StringPool.BLANK);
        } else {
            wbsCodeImpl.setWbsOwnerName(wbsOwnerName);
        }

        wbsCodeImpl.resetOriginalValues();

        return wbsCodeImpl;
    }
}
