package com.hp.omp.portlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.POSearchHelper;
import com.hp.omp.helper.UploadDownloadHelper;
import com.hp.omp.model.Buyer;
import com.hp.omp.model.CostCenter;
import com.hp.omp.model.GoodReceipt;
import com.hp.omp.model.MacroDriver;
import com.hp.omp.model.MicroDriver;
import com.hp.omp.model.Position;
import com.hp.omp.model.Project;
import com.hp.omp.model.PurchaseOrder;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.TrackingCodes;
import com.hp.omp.model.Vendor;
import com.hp.omp.model.WbsCode;
import com.hp.omp.model.custom.Currency;
import com.hp.omp.model.custom.FileUpload;
import com.hp.omp.model.custom.GruppoUsers;
import com.hp.omp.model.custom.OmpFormatterUtil;
import com.hp.omp.model.custom.PositionStatus;
import com.hp.omp.model.custom.PurchaseOrderStatus;
import com.hp.omp.model.impl.GoodReceiptImpl;
import com.hp.omp.model.impl.PurchaseOrderImpl;
import com.hp.omp.service.BuyerLocalServiceUtil;
import com.hp.omp.service.CostCenterLocalServiceUtil;
import com.hp.omp.service.GoodReceiptLocalServiceUtil;
import com.hp.omp.service.MacroDriverLocalServiceUtil;
import com.hp.omp.service.MicroDriverLocalServiceUtil;
import com.hp.omp.service.PositionLocalServiceUtil;
import com.hp.omp.service.ProjectLocalServiceUtil;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.hp.omp.service.TrackingCodesLocalServiceUtil;
import com.hp.omp.service.VendorLocalServiceUtil;
import com.hp.omp.service.WbsCodeLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class PurchaseOrderPortlet
 */
public class PurchaseOrderPortlet extends MVCPortlet {

	Logger logger = LoggerFactory.getLogger(PurchaseOrderPortlet.class);
	public static final String DATE_FORMAT = "dd/MM/yyyy"; 

	private Object lock  = new Object();

	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		
		logger.info("public void render() - Start");
		
		HttpServletRequest httpServletRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(request));
		String jspPage = httpServletRequest.getParameter("jspPage");
		if(jspPage == null) {
			jspPage = ParamUtil.getString(request, "jspPage","/html/purchaseorder/addpo.jsp");
		}
		
		handleGRForm(request,response);
		handleAddPOForm(request,response);
		handleEditPOForm(request, response);
		handlePositionGRList(request, response);

		try {
			List<WbsCode> wbsCodeList = WbsCodeLocalServiceUtil.getWbsCodes(0, WbsCodeLocalServiceUtil.getWbsCodesCount());
			request.setAttribute("allWbsCodes", wbsCodeList);

		} catch (SystemException e) {
			logger.error("error happened during retrieving WBS code/Projects list",e);
		}

		getPortletContext().getRequestDispatcher(jspPage).include(request, response);
		
		logger.info("public void render() - End");
		
	}


	private void handlePositionGRList(RenderRequest request, RenderResponse response) {
		
		logger.info("private void handlePositionGRList() - Start");
		
		String grForm = ParamUtil.getString(request, "GET_CMD", "");
		if ("grList".equals(grForm)) {
			try {
				String positionId = ParamUtil.getString(request, "positionId");
				List<GoodReceipt> grList = GoodReceiptLocalServiceUtil.getPositionGoodReceipts(positionId);
				request.setAttribute("grList", grList);
				
			} catch (SystemException e) {
				logger.error("Error in get Good Receipt list", e);
			}
		}
		
		logger.info("private void handlePositionGRList() - End");
	}



	private void handleGRForm(RenderRequest request, RenderResponse response) {
		
		logger.info("private void handleGRForm() - Start");
		
		String grForm = ParamUtil.getString(request,"GET_CMD", "");
		if("grForm".equals(grForm)) {
			Long grId = ParamUtil.getLong(request, "grId");
			if(grId != 0) {
				try {
					GoodReceipt goodReceipt = GoodReceiptLocalServiceUtil.getGoodReceipt(grId);
					request.setAttribute("gr", goodReceipt);
				} catch (PortalException e) {
					logger.error("Error in get good Receipt for edit",e);
				} catch (SystemException e) {
					logger.error("Error in get good Receipt for edit",e);
				} 
			} else {
				GoodReceipt goodReceipt = new GoodReceiptImpl();
				long positionId = ParamUtil.getLong(request, "positionId", 0);
				String catCode = ParamUtil.getString(request, "catCode","");
				goodReceipt.setPositionId(String.valueOf(positionId));
				request.setAttribute("gr", goodReceipt);
				
				/* 
				 * MS 21.10.2020
				 * Verifico che per il category code indicato
				 * devo chiedere o meno di allegare la documentazione
				 */
				Properties propsCatCode = readProperties();
				request.setAttribute("attachDoc4Gr", false);
				if(!propsCatCode.getProperty("categoryCodeList").contains(catCode.toUpperCase())){
					request.setAttribute("attachDoc4Gr", true);
				}
			}
		}
		
		logger.info("private void handleGRForm() - End");
		
	}


	private void handleAddPOForm(RenderRequest request, RenderResponse response) {

		logger.info("private void handleAddPOForm() - Start");
		
		HttpServletRequest httpServletRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(request));
		String addPoForm = httpServletRequest.getParameter("GET_ADD_PO");
		String finalStep = httpServletRequest.getParameter("finalStep");
		if("true".equals(finalStep)) {
			request.setAttribute("finalStep", true);
		}
		if("addPoForm".equals(addPoForm)) {
			String purchaseRequestId = httpServletRequest.getParameter("prId");
			String purchaseRequestIds = httpServletRequest.getParameter("prIds");
			List<Long> purchaseRequestsIDList = new ArrayList<Long>();

			if(purchaseRequestIds == null || purchaseRequestIds.isEmpty()){
				purchaseRequestIds = purchaseRequestId;
				purchaseRequestsIDList = getPoIdsList(purchaseRequestIds);
			} else {
				purchaseRequestsIDList = getPoIdsList(purchaseRequestIds);
			}


			if(purchaseRequestId == null || purchaseRequestId.isEmpty()){
				purchaseRequestId = String.valueOf(purchaseRequestsIDList.get(0));
			}

			String redirect=httpServletRequest.getParameter("redirect");

			if(redirect==null || redirect.isEmpty()){
				redirect ="/group/vodafone/home"; 
			}

			try {
				PurchaseOrder purchaseOrder= retrievePurchaseOrderInfo(
						PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.parseLong(purchaseRequestId)));

				if(Currency.EUR.getCode() == Integer.valueOf(purchaseOrder.getCurrency())){
					request.setAttribute("currencyName", Currency.EUR.getLabel());
				} else if(Currency.USD.getCode() == Integer.valueOf(purchaseOrder.getCurrency())){
					request.setAttribute("currencyName", Currency.USD.getLabel());
				}

				request.setAttribute("purchaseOrder", purchaseOrder);
				//Retrieve positions of selected purchase requests.
				List<Position> availablePositions = new ArrayList<Position>();
				List<Position> positionsPerPR = null;
				int positionStatus = PositionStatus.SC_RECEIVED.getCode();

				if(purchaseOrder.getAutomatic())
					positionStatus = PositionStatus.NEW.getCode();

				for (Long prID: purchaseRequestsIDList){

					positionsPerPR = new ArrayList<Position>();
					positionsPerPR = PositionLocalServiceUtil.getPositionsByStatus(prID,positionStatus);	

					for(Position pos : positionsPerPR){
						availablePositions.add(pos);
					}
				}

				request.setAttribute("availablePositions",availablePositions);

				List<Vendor> allVendorsList= VendorLocalServiceUtil.getVendors(0, VendorLocalServiceUtil.getVendorsCount());
				List<Vendor> vendorListVbs = new ArrayList<Vendor>();
				for(Vendor vendor : allVendorsList){
					if (vendor.getGruppoUsers() == GruppoUsers.GNED.getCode()){
						vendorListVbs.add(vendor);
					}
				}
				request.setAttribute("allVendors", vendorListVbs);

				List<Project> projectsList = ProjectLocalServiceUtil.getProjects(0, ProjectLocalServiceUtil.getProjectsCount());
				request.setAttribute("allProjects", projectsList);

				List<CostCenter> allCostCenters = CostCenterLocalServiceUtil.getCostCentersList(0,
						CostCenterLocalServiceUtil.getCostCentersCount(), 
						null, 
						GruppoUsers.GNED.getCode());
				request.setAttribute("allCostCenters", allCostCenters);

				//tracking code list
				List<TrackingCodes> allTrackingCodes = TrackingCodesLocalServiceUtil.getTrackingCodeses(0, TrackingCodesLocalServiceUtil.getTrackingCodesesCount());
				request.setAttribute("allTrackingCodes", allTrackingCodes);

				//wbs code list
				List<WbsCode> allWbsCodes = WbsCodeLocalServiceUtil.getWbsCodes(0, WbsCodeLocalServiceUtil.getWbsCodesCount());
				request.setAttribute("allWbsCodes", allWbsCodes);

				//List<Buyer> buyersList = BuyerLocalServiceUtil.getBuyers(0, BuyerLocalServiceUtil.getBuyersCount());
				List<Buyer> buyersList = BuyerLocalServiceUtil.getBuyersList(0, 
						BuyerLocalServiceUtil.getBuyersCountList(null, GruppoUsers.GNED.getCode()),
						null, GruppoUsers.GNED.getCode());
				request.setAttribute("allBuyers",buyersList);

				request.setAttribute("redirect",redirect);

			}catch(PortalException e) {
				logger.error("Erro in handle Add PO Form",e);
			} catch(SystemException e) {
				logger.error("Erro in handle Add PO Form",e);
			} catch (Exception e) {
				logger.error("Erro in handle Add PO Form",e);
			}
		}
		
		logger.info("private void handleAddPOForm() - End");
	}

	private void handleEditPOForm(RenderRequest request, RenderResponse response) {

		logger.info("private void handleEditPOForm() - Start");
		
		HttpServletRequest httpServletRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(request));
		String editPoForm = httpServletRequest.getParameter("GET_EDIT_PO");
		String finalStep = httpServletRequest.getParameter("finalStep");
		
		if("true".equals(finalStep)) {
			request.setAttribute("finalStep", true);
		}
		
		if("editPoForm".equals(editPoForm)) {
			String porderId = httpServletRequest.getParameter("porderId");

			String redirect=httpServletRequest.getParameter("redirect");
			if(redirect==null || redirect.isEmpty()){
				redirect ="/group/vodafone/home"; 
			}

			try {
				PurchaseOrder purchaseOrder = PurchaseOrderLocalServiceUtil.getPurchaseOrder(Long.valueOf(porderId));

				ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
				Long userId = themeDisplay.getUserId();

				List<Position> selectedPositions = PositionLocalServiceUtil.getPurchaseOrderPositions(String.valueOf(purchaseOrder.getPurchaseOrderId()),userId);

				if(purchaseOrder.getTotalValue() == null || purchaseOrder.getTotalValue().equals("")) {
					double totalValue = 0;
					for(Position position : selectedPositions){
						totalValue =+ (Double.valueOf(position.getQuatity()) * Double.valueOf(position.getActualUnitCost()));						
					}

					purchaseOrder.setTotalValue(String.valueOf(totalValue));
					PurchaseOrderLocalServiceUtil.updatePurchaseOrder(purchaseOrder, true);
				}

				purchaseOrder = PurchaseOrderLocalServiceUtil.getPurchaseOrder(Long.valueOf(porderId));
				purchaseOrder.setTotalValue(OmpFormatterUtil.formatNumberByLocale(Double.valueOf(purchaseOrder.getTotalValue()), Locale.ITALY));

				if(Currency.EUR.getCode() == Integer.valueOf(purchaseOrder.getCurrency())){
					request.setAttribute("currencyName", Currency.EUR.getLabel());
				} else if(Currency.USD.getCode() == Integer.valueOf(purchaseOrder.getCurrency())){
					request.setAttribute("currencyName", Currency.USD.getLabel());
				}

				List<Project> projectsList = ProjectLocalServiceUtil.getProjects(0, ProjectLocalServiceUtil.getProjectsCount());
				request.setAttribute("allProjects", projectsList);

				request.setAttribute("purchaseOrder", purchaseOrder);
				request.setAttribute("selectedPositions", selectedPositions);

				int positionStatus = PositionStatus.SC_RECEIVED.getCode();
				if(purchaseOrder.getAutomatic())
					positionStatus = PositionStatus.NEW.getCode();

				List<Vendor> allVendorsList= VendorLocalServiceUtil.getVendors(0, VendorLocalServiceUtil.getVendorsCount());
				List<Vendor> vendorListVbs = new ArrayList<Vendor>();
				for(Vendor vendor : allVendorsList){
					if (vendor.getGruppoUsers() == GruppoUsers.GNED.getCode()){
						vendorListVbs.add(vendor);
					}
				}
				request.setAttribute("allVendors", vendorListVbs);

				List<CostCenter> allCostCenters = CostCenterLocalServiceUtil.getCostCentersList(0,
						CostCenterLocalServiceUtil.getCostCentersCount(), 
						null, 
						GruppoUsers.GNED.getCode());
				request.setAttribute("allCostCenters", allCostCenters);

				//tracking code list
				List<TrackingCodes> allTrackingCodes = TrackingCodesLocalServiceUtil.getTrackingCodeses(0, TrackingCodesLocalServiceUtil.getTrackingCodesesCount());
				request.setAttribute("allTrackingCodes", allTrackingCodes);

				//wbs code list
				List<WbsCode> allWbsCodes = WbsCodeLocalServiceUtil.getWbsCodes(0, WbsCodeLocalServiceUtil.getWbsCodesCount());
				request.setAttribute("allWbsCodes", allWbsCodes);

				//List<Buyer> buyersList = BuyerLocalServiceUtil.getBuyers(0, BuyerLocalServiceUtil.getBuyersCount());
				List<Buyer> buyersList = BuyerLocalServiceUtil.getBuyersList(0, 
						BuyerLocalServiceUtil.getBuyersCountList(null, GruppoUsers.GNED.getCode()),
						null, GruppoUsers.GNED.getCode());
				request.setAttribute("allBuyers",buyersList);

				//List<String> uploadedFiles = new ArrayList<String>();
				List<String> uploadedFiles = UploadDownloadHelper.getGRUploadedFilesNames(purchaseOrder.getPurchaseOrderId());
				request.setAttribute("uploadedFiles", uploadedFiles);

				request.setAttribute("redirect",redirect);

			}catch(PortalException e) {
				logger.error("Erro in handle Edit PO Form",e);
			} catch(SystemException e) {
				logger.error("Erro in handle Edit PO Form",e);
			} catch (Exception e) {
				logger.error("Erro in handle Edit PO Form",e);
			}
		}
		
		logger.info("private void handleEditPOForm() - End");
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) 
			throws IOException,	PortletException {
		
		logger.info("public void serveResource() - Start");
		
		try {
			if("getSubProjects".equals(resourceRequest.getResourceID())){
				POSearchHelper poSearchHelper = new POSearchHelper();
				try {
					poSearchHelper.getSubProjects(resourceRequest, resourceResponse);
				} catch (SystemException e) {
					logger.error("error while happend to retrieve sub projects", e);
				}
			} else if("getMicroDrivers".equals(resourceRequest.getResourceID())){
				try {
					getMicroDrivers(resourceRequest,resourceResponse);
				}catch (SystemException e) {
					logger.error("error in retrieve Micro Drivers", e);
				}

			} else if ("getLovByTrackingCode".equals(resourceRequest.getResourceID())) {
				getTrackingCodeLov(resourceRequest, resourceResponse);

			} else if("uploadFileGR".equals(resourceRequest.getResourceID())){
				uploadFileGR(resourceRequest, resourceResponse);
				
			} else if("listUploadedFileGR".equals(resourceRequest.getResourceID())){
				listUploadedFilesGR(resourceRequest, resourceResponse);
				
			} else if("downloadallFiles".equals(resourceRequest.getResourceID())){
				downloadAllFiles(resourceRequest, resourceResponse);
				
			} else if("downloadFile".equals(resourceRequest.getResourceID())){
				downloadFile(resourceRequest, resourceResponse);
				
			} else if("isUploadFileGR".equals(resourceRequest.getResourceID())){
				isUploadFileGR(resourceRequest, resourceResponse); 
				
			} else if("deleteFile".equals(resourceRequest.getResourceID())){
				deleteFile(resourceRequest, resourceResponse); 
				
			} else{
				postGRForm(resourceRequest,resourceResponse);
				deleteGRForm(resourceRequest,resourceResponse);
			}
		} catch (NumberFormatException e) {
			logger.error("error happen", e);
		} catch (PortalException e) {
			logger.error("error happen", e);
		}
		
		logger.info("public void serveResource() - End");
	}




	private void postGRForm(ResourceRequest resourceRequest, ResourceResponse resourceResponse) 
			throws NumberFormatException, PortalException {
		
		logger.info("private void postGRForm() - Start");
		
		String grForm = ParamUtil.getString(resourceRequest, "POST_CMD","");
		String jspPage = "/html/purchaseorder/grlist.jsp";
		String positionId = ParamUtil.getString(resourceRequest, "positionId");
		Position position = null;
		ThemeDisplay themeDisplay = (ThemeDisplay)resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
		
		if("grForm".equals(grForm)) {
			try {
				GoodReceipt goodReceipt = bindGoodReceipt(resourceRequest);
				position = PositionLocalServiceUtil.getPosition(Long.valueOf(positionId));

				synchronized (lock) {
					if(goodReceipt.getGoodReceiptId() != 0) {
						GoodReceipt oldGoodReceipt = GoodReceiptLocalServiceUtil.getGoodReceipt(goodReceipt.getGoodReceiptId());
						if(((position.getSumPositionGRPercentage() - oldGoodReceipt.getPercentage()) + goodReceipt.getPercentage()) <= 100) {
							goodReceipt.setRequestDate(oldGoodReceipt.getRequestDate());
							GoodReceiptLocalServiceUtil.updateGoodReceipt(goodReceipt);
						}
					} else {
						if((position.getSumPositionGRPercentage() + goodReceipt.getPercentage()) <= 100 ) {
							goodReceipt.setRequestDate(new Date());
							goodReceipt.setCreatedUserId(String.valueOf(themeDisplay.getUserId()));
							GoodReceiptLocalServiceUtil.addGoodReceipt(goodReceipt);
						}
					}
				}
			} catch(SystemException e) {
				logger.error("Error in save or update Good Receipt", e);
			}
			
			try {
				List<GoodReceipt> grList = GoodReceiptLocalServiceUtil.getPositionGoodReceipts(positionId);
				resourceRequest.setAttribute("grList", grList);
				resourceRequest.setAttribute("position", position);
				getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
				
			} catch (PortletException e) {
				logger.error("Error in get gr list",e);
			} catch(SystemException e) {
				logger.error("Error in get gr list",e);
			}catch (IOException e) {
				logger.error("Error in get gr list",e);
			}
		}
		
		logger.info("private void postGRForm() - End");
	}

	private void deleteGRForm(ResourceRequest resourceRequest, ResourceResponse resourceResponse) 
			throws NumberFormatException, PortalException {
		
		logger.info("private void deleteGRForm() - Start");
		
		String grDelete = ParamUtil.getString(resourceRequest, "POST_CMD","");
		String jspPage = "/html/purchaseorder/grlist.jsp";
		
		if("grDelete".equals(grDelete)) {
			Long goodReceiptId = ParamUtil.getLong(resourceRequest, "grId");
			try {
				GoodReceiptLocalServiceUtil.deleteGoodReceipt(goodReceiptId);
			} catch (PortalException e) {
				logger.error("Error in delete gr",e);
			} catch (SystemException e) {
				logger.error("Error in delete gr",e);
			}
			
			try {
				String positionId = ParamUtil.getString(resourceRequest, "positionId");
				List<GoodReceipt> grList = GoodReceiptLocalServiceUtil.getPositionGoodReceipts(positionId);
				resourceRequest.setAttribute("grList", grList);
				Position position = PositionLocalServiceUtil.getPosition(Long.valueOf(positionId));
				resourceRequest.setAttribute("position", position);
				getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
				
			} catch (PortletException e) {
				logger.error("Error in get gr list",e);
			} catch(SystemException e) {
				logger.error("Error in get gr list",e);
			}catch (IOException e) {
				logger.error("Error in get gr list",e);
			}
		}
		
		logger.info("private void deleteGRForm() - End");
	}

	private GoodReceipt bindGoodReceipt(ResourceRequest request) throws NumberFormatException, PortalException, SystemException {
		Long grId = ParamUtil.getLong(request, "grId");
		GoodReceipt goodReceipt;
		if(grId != 0) {
			goodReceipt = GoodReceiptLocalServiceUtil.getGoodReceipt(grId);
		} else {
			goodReceipt = new GoodReceiptImpl();
			goodReceipt.setPositionId(ParamUtil.getString(request, "positionId"));
		}
		double percentage = ParamUtil.getDouble(request, "percentage", 0D);
		goodReceipt.setPercentage(percentage);
		double total = Double.valueOf(goodReceipt.getPosition().getActualUnitCost()) * Double.valueOf(goodReceipt.getPosition().getQuatity());
		
		double t = arrotonda(total, 2);
		double value = (double)(t * percentage)/100;
		
		goodReceipt.setGrValue(String.valueOf(value));
		goodReceipt.setGoodReceiptNumber(ParamUtil.getString(request, "grNumber",""));
		DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		goodReceipt.setRegisterationDate(ParamUtil.getDate(request, "registerationDate",dateFormat,null));
		
		String grFile = ParamUtil.getString(request, "fileUploaded","");
		if(grFile.trim().length() > 0)
			grFile = grFile.substring(grFile.lastIndexOf("\\") + 1);
		
		goodReceipt.setGrFile(grFile);
		
		return goodReceipt;
	}


	private static double arrotonda(double value, int numCifreDecimali) {
		double temp = Math.pow(10, numCifreDecimali);
		return Math.round(value * temp) / temp; 
	}

	private List<Long> getPoIdsList(String poIds){
		List<Long> poIdList = new ArrayList<Long>();
		if(poIds != null && !poIds.isEmpty()){
			for(String id : Arrays.asList(poIds.split(","))){
				poIdList.add(Long.parseLong(id));
			}
		}

		return poIdList;
	}
	

	public void postPOForm(ActionRequest request, ActionResponse response) throws IOException, PortletException {

		logger.info("public void postPOForm() - Start");
		
		long poid = Long.valueOf(ParamUtil.getString(request, "poid"));

		try {
			if(poid != 0) {
				doUpdate(request, response, poid);
			} else {
				doAdd(request, response);
			}
		} catch(Exception e) {
			logger.error("Error in save or update Good Receipt", e);
		}
		try {
			sendRedirect(request, response);
		}catch (IOException e) {
			logger.error("Error in get po list",e);
		}

		logger.info("public void postPOForm() - End");
	}

	private void doAdd(ActionRequest request,
			ActionResponse response) throws IOException, PortletException {

		logger.info("private void doAdd() - Start");
		
		try {
			String[] positions = ParamUtil.getParameterValues(request, "positions");

			for (int i = 0 ; i < positions.length ; i++) {
				String positionId = positions[i];
				Long poNumber = ParamUtil.getLong(request , "ponum"+positionId);
				try {
					PurchaseOrder purchaseOrder = PurchaseOrderLocalServiceUtil.getPurchaseOrder(poNumber);				
					assignPositionToPurchaseOrder(request, purchaseOrder, positionId, poNumber);
				} catch (PortalException e) {
					createNewPurchaseOrder(request, positionId, poNumber);
				}
			}

			sendRedirect(request, response);

		} catch (SystemException e) {
			logger.error("Erro in Add PO Form",e);
			SessionErrors.add(request, "error-submitting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaseorder/addpo.jsp");
		} catch (Exception e) {
			logger.error("Erro in Add PO Form",e);
			SessionErrors.add(request, "error-submitting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaseorder/addpo.jsp");
		}
		
		logger.info("private void doAdd() - End");
	}


	private void createNewPurchaseOrder(ActionRequest request, String positionId, Long poNumber) throws PortalException, SystemException {
		
		logger.info("private void createNewPurchaseOrder() - Start");
		
		PurchaseOrder purchaseOrder = new PurchaseOrderImpl();

		purchaseOrder.setNew(true);
		purchaseOrder.setPurchaseOrderId(poNumber);
		purchaseOrder.setCostCenterId(ParamUtil.getString(request, "costcenter"));
		purchaseOrder.setBuyerId(ParamUtil.getString(request, "buyer"));
		purchaseOrder.setVendorId(ParamUtil.getString(request, "vendor"));
		purchaseOrder.setOla(ParamUtil.getString(request, "ola"));
		purchaseOrder.setProjectId(ParamUtil.getString(request, "projectname"));
		purchaseOrder.setSubProjectId(ParamUtil.getString(request, "subproject"));
		purchaseOrder.setMacroDriverId(ParamUtil.getString(request, "macrodriver"));
		purchaseOrder.setMicroDriverId(ParamUtil.getString(request, "microdriver"));
		purchaseOrder.setOdnpCodeId(ParamUtil.getString(request, "odnpcode"));
		purchaseOrder.setOdnpNameId(ParamUtil.getString(request, "odnpname"));
		//purchaseOrder.setTotalValue(ParamUtil.getString(request, "totalvalue"));
		purchaseOrder.setCurrency(ParamUtil.getString(request, "currencyid"));
		purchaseOrder.setStatus(PurchaseOrderStatus.PO_RECEIVED.getCode());
		purchaseOrder.setAutomatic(ParamUtil.getBoolean(request, "postatus"));
		purchaseOrder.setBudgetCategoryId(ParamUtil.getString(request, "budgetCategoryId"));
		purchaseOrder.setBudgetSubCategoryId(ParamUtil.getString(request, "budgetSubCategoryId"));
		purchaseOrder.setActivityDescription(ParamUtil.getString(request, "activityDescription"));
		User currentUser = PortalUtil.getUser(request);
		purchaseOrder.setCreatedUserId(currentUser.getUserId());
		purchaseOrder.setScreenNameRequester(ParamUtil.getString(request, "screenNameReq"));
		purchaseOrder.setScreenNameReciever(ParamUtil.getString(request, "screenNameRec"));
		purchaseOrder.setWbsCodeId(ParamUtil.getString(request, "wbscode"));
		purchaseOrder.setTrackingCode(ParamUtil.getString(request, "trackingcode"));
		purchaseOrder.setCreatedDate(new Date());
		
		PurchaseOrder addPurchaseOrder = PurchaseOrderLocalServiceUtil.addPurchaseOrder(purchaseOrder);
		logger.debug("The new PO Id is {}",addPurchaseOrder.getPurchaseOrderId());
		
		assignPositionToPurchaseOrder(request, purchaseOrder, positionId, poNumber);
		
		logger.info("private void createNewPurchaseOrder() - End");
	}


	private void assignPositionToPurchaseOrder(ActionRequest request, PurchaseOrder purchaseOrder,
			String positionId,Long poNumber) throws NumberFormatException, PortalException, SystemException {
		
		logger.info("private void assignPositionToPurchaseOrder() - Start");
		
		Position position = PositionLocalServiceUtil.getPosition(Long.valueOf(positionId));
		position.setPurchaseOrderId(String.valueOf(purchaseOrder.getPurchaseOrderId()));
		position.setPoNumber(String.valueOf(poNumber));
		position.setPoNumber(ParamUtil.getString(request , "ponum"+positionId));
		position.setAssetNumber(ParamUtil.getString(request , "assetnum"+positionId));
		position.setShoppingCart(ParamUtil.getString(request , "shoppingcardnum"+positionId));
		position.setPoLabelNumber(ParamUtil.getLong(request , "polabelnumber"+positionId));

		PositionLocalServiceUtil.updatePosition(position);
		
		logger.info("private void assignPositionToPurchaseOrder() - End");
	}


	private void doUpdate(ActionRequest request,
			ActionResponse response ,long pOrderId) throws IOException, PortletException {

		logger.info("private void doUpdate() - Start");
		
		try {
			PurchaseOrder existingPo = PurchaseOrderLocalServiceUtil.getPurchaseOrder(pOrderId);

			existingPo.setCostCenterId(ParamUtil.getString(request, "costcenter"));
			existingPo.setBuyerId(ParamUtil.getString(request, "buyer"));
			existingPo.setVendorId(ParamUtil.getString(request, "vendor"));
			existingPo.setOla(ParamUtil.getString(request, "ola"));
			existingPo.setProjectId(ParamUtil.getString(request, "projectname"));
			existingPo.setSubProjectId(ParamUtil.getString(request, "subproject"));
			existingPo.setMacroDriverId(ParamUtil.getString(request, "macrodriver"));
			existingPo.setMicroDriverId(ParamUtil.getString(request, "microdriver"));
			existingPo.setOdnpCodeId(ParamUtil.getString(request, "odnpcode"));
			existingPo.setOdnpNameId(ParamUtil.getString(request, "odnpname"));
			existingPo.setCurrency(ParamUtil.getString(request, "currencyid"));
			existingPo.setAutomatic(ParamUtil.getBoolean(request, "postatus"));
			existingPo.setBudgetCategoryId(ParamUtil.getString(request, "budgetCategoryId"));
			existingPo.setBudgetSubCategoryId(ParamUtil.getString(request, "budgetSubCategoryId"));
			existingPo.setActivityDescription(ParamUtil.getString(request, "activityDescription"));
			existingPo.setWbsCodeId(ParamUtil.getString(request, "wbscode"));
			existingPo.setTrackingCode(ParamUtil.getString(request, "trackingcode"));
			PurchaseOrderLocalServiceUtil.updatePurchaseOrder(existingPo, true);

			List<Position> oldSelectedPositions = PositionLocalServiceUtil.getPositionsByPurchaseOrderId(pOrderId);
			String[] newSelectedPositions = ParamUtil.getParameterValues(request, "positions");

			for (int i=0 ; i<newSelectedPositions.length ; i++) {
				String positionId = newSelectedPositions[i];
				Long poNumber = ParamUtil.getLong(request , "ponum"+positionId);
				try {
					PurchaseOrder purchaseOrder = PurchaseOrderLocalServiceUtil.getPurchaseOrder(poNumber);
					assignPositionToPurchaseOrder(request, purchaseOrder, positionId, poNumber);
				} catch (PortalException e) {
					createNewPurchaseOrder(request, positionId, poNumber);
				}
			}

			List<Long> deSelectedPostions = getDetSelectedPostionsIds(newSelectedPositions,oldSelectedPositions);
			Position deSelectPosition = null;
			for(Long posId : deSelectedPostions){
				deSelectPosition = PositionLocalServiceUtil.getPosition(posId);
				PurchaseOrder savedPO = PurchaseOrderLocalServiceUtil.getPurchaseOrder(Long.valueOf(deSelectPosition.getPoNumber()));

				deSelectPosition.setPurchaseOrderId(null);
				deSelectPosition.setPoNumber(null);
				deSelectPosition.setAssetNumber(ParamUtil.getString(request , "assetnum"+posId));
				deSelectPosition.setShoppingCart(ParamUtil.getString(request , "shoppingcardnum"+posId));
				
				if(savedPO.getAutomatic()){
					deSelectPosition.setAssetNumber(null);
					deSelectPosition.setShoppingCart(null);
				}

				PositionLocalServiceUtil.updatePosition(deSelectPosition);
			}
			sendRedirect(request, response);

		} catch (SystemException e) {
			logger.error("Erro in Update PO Form",e);
			SessionErrors.add(request, "error-submitting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaseorder/addpo.jsp");
		} catch (Exception e) {
			logger.error("Erro in Add PO Form",e);
			SessionErrors.add(request, "error-submitting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaseorder/addpo.jsp");
		}
		
		logger.info("private void doUpdate() - End");
	}
	

	public void doDelete(ActionRequest request, ActionResponse response) 
			throws IOException, PortletException {
		
		logger.info("public void doDelete() - Start");

		long pOrderId = Long.valueOf(ParamUtil.getString(request, "poid"));
		String pOrderStatus = ParamUtil.getString(request, "postatus");

		try {
			HttpServletRequest httpServletRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(request));
			String redirect=httpServletRequest.getParameter("redirect");

			if(redirect==null || redirect.isEmpty()){
				redirect ="/group/vodafone/home"; 
			}

			List<Position> selectedPositions= PositionLocalServiceUtil.getPositionsByPurchaseOrderId(pOrderId);
			Position deSelectPosition = null;
			for(Position pos : selectedPositions){
				////////////////////delete GRs///////////////////////////////////
				List<GoodReceipt> positionGRS=GoodReceiptLocalServiceUtil.getPositionGoodReceipts(String.valueOf(pos.getPositionId()));

				for(GoodReceipt gr :positionGRS) {
					GoodReceiptLocalServiceUtil.deleteGoodReceipt(gr);
				}

				///////////////////////////////////////////////////////////
				deSelectPosition = PositionLocalServiceUtil.getPosition(pos.getPositionId());
				deSelectPosition.setPurchaseRequestId(pos.getPurchaseRequestId());
				deSelectPosition.setPurchaseOrderId(null);
				deSelectPosition.setPoNumber(null);
				if(Boolean.parseBoolean(pOrderStatus)){
					deSelectPosition.setAssetNumber(null);
					deSelectPosition.setShoppingCart(null);
				} else {
					deSelectPosition.setAssetNumber(pos.getAssetNumber());
					deSelectPosition.setShoppingCart(pos.getShoppingCart());

				}

				PositionLocalServiceUtil.updatePosition(deSelectPosition);
			}

			PurchaseOrderLocalServiceUtil.deletePurchaseOrder(pOrderId);

			request.setAttribute("redirect", redirect);
			sendRedirect(request, response);

		} catch (PortalException e) {
			logger.error("Erro in Delete PO Form",e);
			SessionErrors.add(request, "error-deleting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaseorder/addpo.jsp");
		} catch (SystemException e) {
			logger.error("Erro in Delete PO Form",e);
			SessionErrors.add(request, "error-deleting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaseorder/addpo.jsp");
		} catch (Exception e) {
			logger.error("Erro in Delete PO Form",e);
			SessionErrors.add(request, "error-deleting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaseorder/addpo.jsp");
		}

		logger.info("public void doDelete() - End");
	}

	private List<Long> getDetSelectedPostionsIds(String[] newSelectedPositions,
			List<Position> oldSelectedPositions) {
		
		logger.info("private List<Long> getDetSelectedPostionsIds() - Start");
		
		List<String> newSelectedPositionsList =  Arrays.asList(newSelectedPositions);
		List<Long> detSelectedPostions = new ArrayList<Long>();

		for(Position pos : oldSelectedPositions){
			if(!newSelectedPositionsList.contains(String.valueOf(pos.getPositionId()))){
				detSelectedPostions.add(pos.getPositionId());
			}
		}

		logger.info("private List<Long> getDetSelectedPostionsIds() - End");
		
		return detSelectedPostions;

	}

	private PurchaseOrder retrievePurchaseOrderInfo(PurchaseRequest pr) throws PortalException, SystemException{

		logger.info("private PurchaseOrder retrievePurchaseOrderInfo() - Start");
		
		PurchaseOrder purchaseOrder =new PurchaseOrderImpl();

		purchaseOrder.setCostCenterId(pr.getCostCenterId());
		purchaseOrder.setBuyerId(pr.getBuyerId());
		purchaseOrder.setVendorId(pr.getVendorId());
		purchaseOrder.setBudgetCategoryId(pr.getBudgetCategoryId());
		purchaseOrder.setBudgetSubCategoryId(pr.getBudgetSubCategoryId());
		purchaseOrder.setOla(pr.getOla());
		purchaseOrder.setProjectId(pr.getProjectId());
		purchaseOrder.setSubProjectId(pr.getSubProjectId());
		purchaseOrder.setMacroDriverId(pr.getMacroDriverId());
		purchaseOrder.setMicroDriverId(pr.getMicroDriverId());
		purchaseOrder.setOdnpCodeId(pr.getOdnpCodeId());
		purchaseOrder.setOdnpNameId(pr.getOdnpNameId());
		purchaseOrder.setCurrency(pr.getCurrency());
		purchaseOrder.setTotalValue(OmpFormatterUtil.formatNumberByLocale(Double.valueOf(pr.getTotalValue()), Locale.ITALY));
		purchaseOrder.setAutomatic(pr.getAutomatic());
		purchaseOrder.setScreenNameRequester(pr.getScreenNameRequester());
		purchaseOrder.setScreenNameReciever(pr.getScreenNameReciever());
		purchaseOrder.setActivityDescription(pr.getActivityDescription());
		purchaseOrder.setTrackingCode(pr.getTrackingCode());
		purchaseOrder.setWbsCodeId(pr.getWbsCodeId());

		logger.info("private PurchaseOrder retrievePurchaseOrderInfo() - End");
		
		return purchaseOrder;

	}


	private void getMicroDrivers(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws SystemException, IOException {

		try {
			JSONObject optionsJSON = JSONFactoryUtil.createJSONObject();
			Long macroId = ParamUtil.getLong(resourceRequest, "macroId");
			Long microDriverId = ParamUtil.getLong(resourceRequest, "microDriverId");

			List<MicroDriver> microDriversList= null;
			if(macroId !=null && macroId !=0) {
				MacroDriver macroDriver=MacroDriverLocalServiceUtil.getMacroDriver(macroId);
				if(macroDriver.getDisplay()) {
					microDriversList = MicroDriverLocalServiceUtil.getMicroDriverByMacroDriverId(macroId);
				}else {
					if(microDriverId !=null && microDriverId !=0) {
						MicroDriver microDriver=MicroDriverLocalServiceUtil.getMicroDriver(microDriverId);
						microDriversList=new ArrayList<MicroDriver>();
						microDriversList.add(microDriver);
					}
				}
			}
			if(microDriversList == null){
				microDriversList = new ArrayList<MicroDriver>();
			}
			JSONArray jsonmicroDriverArray = JSONFactoryUtil.createJSONArray();
			for (MicroDriver microDriver : microDriversList) {
				JSONObject json = JSONFactoryUtil.createJSONObject();
				json.put("id", microDriver.getMicroDriverId());
				json.put("name", microDriver.getMicroDriverName());
				json.put("display", microDriver.getDisplay());

				jsonmicroDriverArray.put(json);
			}
			optionsJSON.put("microDrivers", jsonmicroDriverArray);
			PrintWriter writer = resourceResponse.getWriter();
			writer.write(optionsJSON.toString());
		}catch(Exception e) {
			logger.error("error while getMicroDrivers Ajax", e);	
		}
	}



	private void getTrackingCodeLov(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) {

		logger.info("private void getTrackingCodeLov() - Start");
		
		JSONObject optionsJSON = JSONFactoryUtil.createJSONObject();
		String trackingCode = ParamUtil.getString(resourceRequest, "selectedTrackingCode");
		if(trackingCode.contains("||")) {
			trackingCode = trackingCode.trim().split("\\|\\|")[0].trim();
		}
		TrackingCodes trCode;
		try {
			trCode = TrackingCodesLocalServiceUtil.getTrackingCodeByName(trackingCode);
			if(trCode != null) {
				optionsJSON.put("budgetCategoryId",trCode.getBudgetCategoryId());
				optionsJSON.put("budgetSubCategoryId",trCode.getBudgetSubCategoryId());
				optionsJSON.put("macroDriverId",trCode.getMacroDriverId());
				optionsJSON.put("microDriverId",trCode.getMicroDriverId());
				optionsJSON.put("odnpNameId",trCode.getOdnpNameId());
				optionsJSON.put("projectId",trCode.getProjectId());
				optionsJSON.put("subProjectId",trCode.getSubProjectId());
				optionsJSON.put("wbsCodeId",trCode.getWbsCodeId());
			} else {
				optionsJSON.put("empty","true");
			}

			PrintWriter writer = resourceResponse.getWriter();
			writer.write(optionsJSON.toString());
		}catch (SystemException e) {
			logger.error("error in retrieve Tracking Code list of values", e);
			optionsJSON.put("status","failure");
		} catch (IOException e) {
			logger.error("error in retrieve Tracking Code list of values", e);
			optionsJSON.put("status","failure");
		}

		logger.info("private void getTrackingCodeLov() - End");
	}

	
	private void uploadFileGR(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		
		logger.info("private void uploadFileGR() - Start");
		
		long purchaseOrderId = ParamUtil.getLong(request, "purchaseOrderId");

		try {
			UploadDownloadHelper.uploadFile(purchaseOrderId, true,  request);
			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("name","success");
			json.put("id",purchaseOrderId);
			
			response.getWriter().print(json);
		} catch (UploadException ue) {
			logger.error(ue.getMessage());
			response.getWriter().write("failed");

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
		
		logger.info("private void uploadFileGR() - End");
	}
	
	private void listUploadedFilesGR(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		
		logger.info("private void listUploadedFilesGR() - Start");
		
		long purchaseOrderId = ParamUtil.getLong(request, "purchaseOrderId");
		String jspPage = "/html/purchaseorder/filelist.jsp";
		try {
			//List<String> uploadedFiles = new ArrayList<String>();
			List<String> uploadedFiles = UploadDownloadHelper.getGRUploadedFilesNames(purchaseOrderId);
			request.setAttribute("uploadedFiles", uploadedFiles);
			getPortletContext().getRequestDispatcher(jspPage).include(request, response);
		}catch (Exception e) {
			logger.error("error while retrieving the files list",e);
		}
		
		logger.info("private void listUploadedFilesGR() - End");
	}
	
	
	public void downloadFile(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {

		logger.info("public void downloadFile() - Start");
		
		long purchaseOrderId = ParamUtil.getLong(request, "purchaseOrderId");
		String filename=ParamUtil.getString(request, "fileName");
		try {
			UploadDownloadHelper.downloadFileByName(purchaseOrderId,filename,response);
		} catch (UploadException ue) {
			logger.error(ue.getMessage());
			response.getWriter().write("failed");

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
		
		logger.info("public void downloadFile() - End");
	}
	
	public void downloadAllFiles(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {

		logger.info("public void downloadAllFiles() - Start");
		
		long purchaseOrderId = ParamUtil.getLong(request, "purchaseOrderId");
		
		try {
			UploadDownloadHelper.downloadZipFile(purchaseOrderId,response);
		} catch (UploadException ue) {
			logger.error(ue.getMessage());
			response.getWriter().write("failed");

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
		
		logger.info("public void downloadAllFiles() - End");
	}
	
	private void isUploadFileGR(ResourceRequest request, ResourceResponse response) throws PortletException, IOException {

		logger.info("private void isUploadFileGR() - Start");
		
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();

		long pOrderId = Long.valueOf(ParamUtil.getString(request, "pOrderId"));

		try {
			Properties propsCatCode = readProperties();

			List<Position> selectedPositions = PositionLocalServiceUtil.getPositionsByPurchaseOrderId(pOrderId);
			boolean viewPopup = false;
			for(Position pos : selectedPositions){
				if(!propsCatCode.getProperty("categoryCodeList").contains(pos.getCategoryCode())){
					viewPopup = true;
					break;
				}
			}
			if(viewPopup){
				jsonObject.put("isUploadFileGR","true");
			} else{
				jsonObject.put("isUploadFileGR","false");
			}
			response.getWriter().print(jsonObject);
			
		} catch(Exception e) {
			logger.error("Error in save or update Good Receipt", e);
		}

		logger.info("private void isUploadFileGR() - End");
	}
	
	private void deleteFile(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {

		logger.info("private void deleteFile() - Start");
		
		long purchaseOrderId = ParamUtil.getLong(request, "purchaseOrderId");
		
		String filename=ParamUtil.getString(request, "fileName");
		try {
			UploadDownloadHelper.deleteFileByName(purchaseOrderId,filename);
			listUploadedFilesGR(request, response);
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
		
		logger.info("private void deleteFile() - End");
	}
	
	private static synchronized Properties readProperties(){
		String resourceName = "categoryCode.properties";
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		
		Properties props = new Properties();
		try  {
			InputStream resourceStream = loader.getResourceAsStream(resourceName);
			props.load(resourceStream);
		    
		} catch (IOException e){
			System.out.println(e);
		}
		return props;
	}
	
}