package com.hp.omp.service.impl;

import java.util.List;

import com.hp.omp.model.BudgetSubCategory;
import com.hp.omp.service.base.BudgetSubCategoryLocalServiceBaseImpl;
import com.hp.omp.service.persistence.BudgetSubCategoryFinderUtil;
import com.hp.omp.service.persistence.BudgetSubCategoryUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the budget sub category local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.BudgetSubCategoryLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Ahmed Kamel
 * @see com.hp.omp.service.base.BudgetSubCategoryLocalServiceBaseImpl
 * @see com.hp.omp.service.BudgetSubCategoryLocalServiceUtil
 */
public class BudgetSubCategoryLocalServiceImpl
    extends BudgetSubCategoryLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.hp.omp.service.BudgetSubCategoryLocalServiceUtil} to access the budget sub category local service.
     */
	
	public BudgetSubCategory getBudgetSubCategoryByName(String name) throws SystemException{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(BudgetSubCategory.class);
		dynamicQuery.add(RestrictionsFactoryUtil.ilike("budgetSubCategoryName", name));
		 
		BudgetSubCategory budgetSubCategory = null;
		List<BudgetSubCategory>  budgetSubCategoryList = BudgetSubCategoryUtil.findWithDynamicQuery(dynamicQuery);
		if(budgetSubCategoryList != null && ! (budgetSubCategoryList.isEmpty()) ){
			budgetSubCategory = budgetSubCategoryList.get(0);
		}
		
		return budgetSubCategory;
	}
	
	public List<BudgetSubCategory> getBudgetSubCategoryList(Integer pageNumber, Integer pageSize, String searchFilter)
			throws SystemException {
		List<BudgetSubCategory> resultQuery = null;
		resultQuery= BudgetSubCategoryFinderUtil.getBudgetSubCategoryList(pageNumber, pageSize, searchFilter);
		return resultQuery; 
	}
}
