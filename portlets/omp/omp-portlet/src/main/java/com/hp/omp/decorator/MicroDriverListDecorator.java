package com.hp.omp.decorator;

import java.io.Serializable;

import org.displaytag.decorator.TableDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.MicroDriver;

public class MicroDriverListDecorator extends TableDecorator implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2685221218697856230L;

	Logger logger = LoggerFactory.getLogger(MicroDriverListDecorator.class);
	
	public String getMicroDriverId() {
		
        return getRemoveAction();
    }
	
	public String getMicroDriverName() {
		
        return getUpdateAction();
    }
	
	public String getRemoveAction() {
		
        StringBuilder sb = new StringBuilder();
        MicroDriver item = (MicroDriver) this.getCurrentRowObject();
      
        long microDriverId = item.getMicroDriverId();
        String microDriverName = item.getMicroDriverName();
        sb.append("<a href=\"javascript:removeMicroDriver("+microDriverId+",'"+microDriverName+"')\"");
        sb.append("\">");
        sb.append(" Remove ");
        sb.append("</a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
    }	
	
	public String getUpdateAction() {
		
		MicroDriver item = (MicroDriver) this.getCurrentRowObject();
		StringBuilder sb = new StringBuilder();
       
      
		long microDriverId = item.getMicroDriverId();
        String microDriverName = item.getMicroDriverName();
        sb.append("<a onclick=\"javascript:updateMicroDriver(this, "+microDriverId+")\" href='"+microDriverName+"'");
        sb.append("\"> ");
        sb.append(microDriverName);
        sb.append(" </a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
        
    }

}
