package com.hp.omp.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.model.CostCenter;
import com.hp.omp.model.CostCenterUser;
import com.hp.omp.model.Project;
import com.hp.omp.model.SubProject;
import com.hp.omp.model.WbsCode;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.custom.PrListDTO;
import com.hp.omp.model.custom.PurchaseRequestStatus;
import com.hp.omp.model.custom.SearchDTO;
import com.hp.omp.service.CostCenterLocalServiceUtil;
import com.hp.omp.service.CostCenterUserLocalServiceUtil;
import com.hp.omp.service.ProjectLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.hp.omp.service.SubProjectLocalServiceUtil;
import com.hp.omp.service.WbsCodeLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class PrSearchPortlet extends MVCPortlet {

	public static final Logger logger = LoggerFactory.getLogger(PrSearchPortlet.class);
	boolean created = false;
	
	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		try {
			List<WbsCode> wbsCodeList = WbsCodeLocalServiceUtil.getWbsCodes(0, WbsCodeLocalServiceUtil.getWbsCodesCount());
			renderRequest.setAttribute("allWbsCodes", wbsCodeList);
			
			List<Project> projectsList = ProjectLocalServiceUtil.getProjects(0, ProjectLocalServiceUtil.getProjectsCount());
			renderRequest.setAttribute("allProjects", projectsList);
			
		} catch (SystemException e) {
			logger.error("error happened during retrieving WBS code/Projects list",e);
		}
		super.doView(renderRequest, renderResponse);
	}
	
	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		
		try {
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
		
		HttpServletRequest httpServletRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(request));
		String purchaseRequestID =httpServletRequest.getParameter("purchaseRequestID");
		
	     SearchDTO searchDTO = new SearchDTO();
	     searchDTO.setPrId(Long.valueOf(purchaseRequestID));
	     OmpRoles userRole = OmpHelper.getUserRole(themeDisplay.getUserId());
	     List<PrListDTO> prList = PurchaseRequestLocalServiceUtil.prSearch(searchDTO,userRole.getCode(),themeDisplay.getUserId());
		}catch(Exception e) {
			 logger.error("Error in search PR",e );
		}
		super.render(request, response);
	}
	

	private Integer getTotalNumberOfPages(Long totalNubmerOfResult, Integer pageSize) {
        return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
}

}

