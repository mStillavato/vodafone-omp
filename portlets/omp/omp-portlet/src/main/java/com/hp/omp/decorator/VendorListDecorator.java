package com.hp.omp.decorator;

import java.io.Serializable;

import org.displaytag.decorator.TableDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.Vendor;

public class VendorListDecorator extends TableDecorator implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2685221218697856230L;

	Logger logger = LoggerFactory.getLogger(VendorListDecorator.class);
	
	public String getVendorId() {
		
        return getRemoveAction();
    }
	
	public String getVendorName() {
		
        return getUpdateAction();
    }
	
	public String getRemoveAction() {
		
        StringBuilder sb = new StringBuilder();
        Vendor item = (Vendor) this.getCurrentRowObject();
      
        long vendorId = item.getVendorId();
        String vendorName = item.getVendorName();
        sb.append("<a href=\"javascript:removeVendor("+vendorId+",'"+vendorName+"')\"");
        sb.append("\">");
        sb.append(" Remove ");
        sb.append("</a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
    }	
	
	public String getUpdateAction() {
		
		Vendor item = (Vendor) this.getCurrentRowObject();
		StringBuilder sb = new StringBuilder();
       
      
		long vendorId = item.getVendorId();
        String vendorName = item.getVendorName();
        sb.append("<a onclick=\"javascript:updateVendor(this, "+vendorId+")\" href='"+vendorName+"'");
        sb.append("\"> ");
        sb.append(vendorName);
        sb.append(" </a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
        
    }

}
