package com.hp.omp.portlet;

import java.io.IOException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.model.BudgetCategory;
import com.hp.omp.model.PurchaseOrder;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.impl.BudgetCategoryImpl;
import com.hp.omp.service.BudgetCategoryLocalServiceUtil;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class BudgetCategoryAdminPortlet
 */
public class BudgetCategoryAdminPortlet extends MVCPortlet {
 
	private transient Logger logger = LoggerFactory.getLogger(BudgetCategoryAdminPortlet.class);
	private String errorMSG = "";
	private String successMSG = "";
	private String ACTION_PARAM= "action";
	private String ADD_ACTION= "add";
	private String UPDATE_ACTION= "update";
	private String DELETE_ACTION= "delete";
	private String PAGINATION_ACTION= "pagination";
	
	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		super.doView(renderRequest, renderResponse);
	}
	
	
	/* (non-Javadoc)
	 * @see javax.portlet.GenericPortlet#render(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		viewAddBudgetCategory(request, response);
		super.render(request, response);
	}
	
	public void viewAddBudgetCategory(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		String action = renderRequest.getParameter(ACTION_PARAM);
		
		if(ADD_ACTION.equals(action)){
			renderRequest.setAttribute(ACTION_PARAM, ADD_ACTION);
			renderRequest.setAttribute("display",true);
		}else if(UPDATE_ACTION.equals(action)){
			String budgetCategoryId = renderRequest.getParameter("budgetCategoryId");
			BudgetCategory budgetCategory = getBudgetCategory(budgetCategoryId);
			logger.debug("render update action for : "+budgetCategory.getBudgetCategoryName());
			logger.debug("Display = "+budgetCategory.getDisplay());
			
			renderRequest.setAttribute(ACTION_PARAM, UPDATE_ACTION);
			renderRequest.setAttribute("budgetCategoryId", budgetCategory.getBudgetCategoryId());
			renderRequest.setAttribute("budgetCategoryName", budgetCategory.getBudgetCategoryName());
			renderRequest.setAttribute("display", budgetCategory.getDisplay());
		}
	}

	
	@Override
    public void serveResource(ResourceRequest resourceRequest,
                  ResourceResponse resourceResponse) throws IOException,
                  PortletException {
		String action=resourceRequest.getParameter(ACTION_PARAM);
		if(DELETE_ACTION.equals(action)){
			deleteBudgetCategory(resourceRequest, resourceResponse);
		}else if(PAGINATION_ACTION.equals(action)){
			paginationList(resourceRequest, resourceResponse);
		}
		
		clearMessages();
           super.serveResource(resourceRequest, resourceResponse);
    }
	
	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#processAction(javax.portlet.ActionRequest, javax.portlet.ActionResponse)
	 */
	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {

		clearMessages();
		String action = actionRequest.getParameter(ACTION_PARAM);
		String budgetCategoryName = actionRequest.getParameter("budgetCategoryName");
		boolean display=ParamUtil.getBoolean(actionRequest,"display");
		String operation = "";
		
		logger.debug("budgetCategoryName="+budgetCategoryName);
		logger.debug("display = "+display);
		logger.debug("creating BudgetCategory object ...");
		BudgetCategory newBudgetCategory = new BudgetCategoryImpl();
		newBudgetCategory.setBudgetCategoryName(budgetCategoryName);
		newBudgetCategory.setDisplay(display);
		logger.debug("BudgetCategory object created !!");
		try {
			if(ADD_ACTION.equals(action)){
				operation = "added";
				logger.debug("adding new BudgetCategory ["+budgetCategoryName+"] in database ...");
				if(BudgetCategoryLocalServiceUtil.getBudgetCategoryByName(budgetCategoryName) == null){
					
					BudgetCategoryLocalServiceUtil.addBudgetCategory(newBudgetCategory);
					
					successMSG = "BudgetCategory name ["+budgetCategoryName+"] successfully Added";
					
				}else{
					errorMSG = "BudgetCategory name ["+budgetCategoryName+"] already existing";
				}
			}else if(UPDATE_ACTION.equals(action)){
				operation = "updated";
				String budgetCategoryId = actionRequest.getParameter("budgetCategoryId");
				
				newBudgetCategory.setBudgetCategoryId(Integer.valueOf(budgetCategoryId));
				
				if(! (isBudgetCategoryNameExists(newBudgetCategory, budgetCategoryName))){
					
					logger.debug("updating  BudgetCategory ["+budgetCategoryId+"] in database ...");
					
					
					BudgetCategoryLocalServiceUtil.updateBudgetCategory(newBudgetCategory);
					successMSG = "BudgetCategory Name ["+budgetCategoryName+"] successfully Updated";
				}else{
					errorMSG = "BudgetCategory name ["+budgetCategoryName+"] already existing";
				}
				
			}
			
			
		} catch (SystemException e) {
			errorMSG = "BudgetCategory ["+budgetCategoryName+"] not "+operation+".";
			logger.error("error happened while "+operation+" BudgetCategory ["+budgetCategoryName+"] in database.");
			logger.error(e.getMessage(), e);
		}catch (NumberFormatException nfe) {
			errorMSG = "BudgetCategory ["+budgetCategoryName+"] not "+operation+".";
			logger.error("error happened while "+operation+" BudgetCategory ["+budgetCategoryName+"] in database.");
			logger.error(nfe.getMessage(), nfe);
		}
		
		actionRequest.setAttribute("errorMSG", errorMSG);
		actionRequest.setAttribute("successMSG", successMSG);
		super.processAction(actionRequest, actionResponse);
	}
	
	private void retrieveBudgetCategoryListToRequest(PortletRequest renderRequest) throws SystemException{
		try {
		String searchFilter = ParamUtil.getString(renderRequest, "searchFilter","");
			
		int totalCount = BudgetCategoryLocalServiceUtil.getBudgetCategoriesCount();
		
		Integer pageNumber = ParamUtil.getInteger(renderRequest, "pageNumber", 1);
		Integer pageSize = OmpHelper.PAGE_SIZE;
		int start = (pageNumber - 1) * pageSize;
		int end = start + pageSize;
		
		renderRequest.setAttribute("numberOfPages", getTotalNumberOfPages(totalCount, pageSize));
		renderRequest.setAttribute("pageNumber", pageNumber);
		
		//List<BudgetCategory> budgetCategoryList = BudgetCategoryLocalServiceUtil.getBudgetCategories(start, end);
		List<BudgetCategory> budgetCategoryList = BudgetCategoryLocalServiceUtil.getBudgetCategoryList(start, end, searchFilter);
		
		
		renderRequest.setAttribute("budgetCategoryList", budgetCategoryList);
	} catch (SystemException e) {
		logger.error(e.getMessage(), e);
		errorMSG = "can not retrieve BudgetCategory list";
		renderRequest.setAttribute("errorMSG", errorMSG);
	}
		
	}
	
	public void deleteBudgetCategory(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse)
			throws PortletException, IOException {
		clearMessages();
		
		String budgetCategoryId=resourceRequest.getParameter("budgetCategoryId");
		String budgetCategoryName=resourceRequest.getParameter("budgetCategoryName");
		String action=resourceRequest.getParameter("action");
		String jspPage = "/html/budgetcategoryadmin/budgetcategorylist.jsp";
		
		if(Validator.isBlank(action)){
			errorMSG = "No Action in the request";
		}else if(Validator.isBlank(budgetCategoryId)){
			errorMSG = "No Budget Category Id in the request";
		}else{
			
			try {
				List<PurchaseOrder> purchaseOrderList = PurchaseOrderLocalServiceUtil.getPurchaseOrderByBudgetCategoryId(budgetCategoryId);
				List<PurchaseRequest> purchaseRequestList = PurchaseRequestLocalServiceUtil.getPurchaseRequestByBudgetCategoryId(budgetCategoryId);
				
				if(purchaseOrderList != null && ! (purchaseOrderList.isEmpty())){
					errorMSG = "Can Not remove BudgetCategory assigned to Purchase Order, BudgetCategory name ["+budgetCategoryName+"]";
				}else if(purchaseRequestList != null && ! (purchaseRequestList.isEmpty())){
					errorMSG = "Can Not remove BudgetCategory assigned to Purchase Request, BudgetCategory name ["+budgetCategoryName+"]";
				}else{
					BudgetCategoryLocalServiceUtil.deleteBudgetCategory(Long.valueOf(budgetCategoryId));
					successMSG = "BudgetCategory name ["+budgetCategoryName+"] successfully deleted";
				}
				
			} catch (NumberFormatException e) {
				logger.error(e.getMessage());
				errorMSG = "BudgetCategory Id is invalid";
			} catch (PortalException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove BudgetCategory";
			} catch (SystemException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove BudgetCategory [most probably BudgetCategory is assigned.]";
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove BudgetCategory";
			}
			
		}
		
		//retrieve updated list
		try {
			retrieveBudgetCategoryListToRequest(resourceRequest);
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			errorMSG = "can not retrieve BudgetCategory list";
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
		
	}
	
	private BudgetCategory getBudgetCategory(String budgetCategoryId){
		BudgetCategory budgetCategory = null;
		try {
			budgetCategory = BudgetCategoryLocalServiceUtil.getBudgetCategory(Integer.valueOf(budgetCategoryId));
		} catch (NumberFormatException e) {
			logger.error("BudgetCategory Id has wrong format", e);
		} catch (PortalException e) {
			logger.error("unexpected error", e);
		} catch (SystemException e) {
			logger.error("unexpected error", e);
		}
		
		return budgetCategory;
	}
	
	private boolean isBudgetCategoryNameExists(BudgetCategory newBudgetCategory, String budgetCategoryName) throws SystemException{
		
		BudgetCategory oldBudgetCategory = BudgetCategoryLocalServiceUtil.getBudgetCategoryByName(budgetCategoryName);
		boolean isExists = false;
		
		if(oldBudgetCategory != null && 
				(newBudgetCategory.getBudgetCategoryId() != oldBudgetCategory.getBudgetCategoryId()) ){
			isExists = true;
		}
		return isExists;
	}
	
	private void paginationList(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse) throws PortletException, IOException{
		
		String jspPage = "/html/budgetcategoryadmin/budgetcategorylist.jsp";
		try {
			retrieveBudgetCategoryListToRequest(resourceRequest);
		} catch (SystemException e) {
		logger.error("error happened in pagination ... ",e);
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
	}
	
	private Integer getTotalNumberOfPages(Integer totalNubmerOfResult, Integer pageSize) {
        return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
	}
	
	private void clearMessages(){
		errorMSG = "";
		successMSG = "";
	}
	
	public void searchFilter(ActionRequest request, ActionResponse response) throws IOException, PortletException{
		response.setRenderParameter("searchFilter", "%" + ParamUtil.getString(request, "budgetCategoryName") + "%");  
	}
}
