package com.hp.omp.portlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.exceptions.PRHasNoCostCenterException;
import com.hp.omp.exceptions.UploadNssPrExcelException;
import com.hp.omp.exceptions.UserHasNoCostCenterException;
import com.hp.omp.helper.OmpHelper;
import com.hp.omp.helper.PurchaseRequestExcelFileHelper;
import com.hp.omp.helper.PurchaseRequestPortletHelper;
import com.hp.omp.helper.UploadDownloadHelper;
import com.hp.omp.model.Buyer;
import com.hp.omp.model.CostCenter;
import com.hp.omp.model.CostCenterUser;
import com.hp.omp.model.GoodReceipt;
import com.hp.omp.model.MacroDriver;
import com.hp.omp.model.MicroDriver;
import com.hp.omp.model.Position;
import com.hp.omp.model.Project;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.TrackingCodes;
import com.hp.omp.model.Vendor;
import com.hp.omp.model.WbsCode;
import com.hp.omp.model.custom.Currency;
import com.hp.omp.model.custom.GruppoUsers;
import com.hp.omp.model.custom.IAndCType;
import com.hp.omp.model.custom.LocationDTO;
import com.hp.omp.model.custom.OmpFormatterUtil;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.custom.PurchaseRequestExcelRow;
import com.hp.omp.model.custom.PurchaseRequestStatus;
import com.hp.omp.model.custom.TipoPr;
import com.hp.omp.model.impl.PositionImpl;
import com.hp.omp.model.impl.PurchaseRequestImpl;
import com.hp.omp.service.BuyerLocalServiceUtil;
import com.hp.omp.service.CostCenterLocalServiceUtil;
import com.hp.omp.service.CostCenterUserLocalServiceUtil;
import com.hp.omp.service.GoodReceiptLocalServiceUtil;
import com.hp.omp.service.LocationLocalServiceUtil;
import com.hp.omp.service.MacroDriverLocalServiceUtil;
import com.hp.omp.service.MicroDriverLocalServiceUtil;
import com.hp.omp.service.PositionLocalServiceUtil;
import com.hp.omp.service.ProjectLocalServiceUtil;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.hp.omp.service.TrackingCodesLocalServiceUtil;
import com.hp.omp.service.VendorLocalServiceUtil;
import com.hp.omp.service.WbsCodeLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadException;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * @author sami.basyoni
 * 
 */
public class PurchaseRequestNotNssPortlet extends MVCPortlet{

	Logger logger = LoggerFactory.getLogger(PurchaseRequestNotNssPortlet.class);
	public static final String DATE_FORMAT = "MM/dd/yyyy"; 

	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		try {
			List<TrackingCodes> trackingCodeList=TrackingCodesLocalServiceUtil.getTrackingCodeses(0, TrackingCodesLocalServiceUtil.getTrackingCodesesCount());
			renderRequest.setAttribute("allTrackingCodes", trackingCodeList);

			List<WbsCode> wbsCodeList = WbsCodeLocalServiceUtil.getWbsCodes(0, WbsCodeLocalServiceUtil.getWbsCodesCount());
			renderRequest.setAttribute("allWbsCodes", wbsCodeList);

			List<Project> projectsList = ProjectLocalServiceUtil.getProjects(0, ProjectLocalServiceUtil.getProjectsCount());
			renderRequest.setAttribute("allProjects", projectsList);

			List<Vendor> allVendorsList= VendorLocalServiceUtil.getVendors(0, VendorLocalServiceUtil.getVendorsCount());
			List<Vendor> vendorListVbs = new ArrayList<Vendor>();
			for(Vendor vendor : allVendorsList){
				if (vendor.getGruppoUsers() == GruppoUsers.GNED.getCode()){
					vendorListVbs.add(vendor);
				}
			}
			renderRequest.setAttribute("allVendors", vendorListVbs);

			List<IAndCType> allICTypes = new ArrayList<IAndCType>();
			allICTypes.add(IAndCType.ICFIELD);
			allICTypes.add(IAndCType.ICTEST);
			renderRequest.setAttribute("allICTypes", allICTypes);

			List<CostCenter> costCentersList = CostCenterLocalServiceUtil.getCostCentersList(0,
					CostCenterLocalServiceUtil.getCostCentersCount(), 
					null, 
					GruppoUsers.GNED.getCode());
			renderRequest.setAttribute("allCostCenters",costCentersList);

			List<Buyer> buyersList = BuyerLocalServiceUtil.getBuyersList(0, 
					BuyerLocalServiceUtil.getBuyersCountList(null, GruppoUsers.GNED.getCode()),
					null, GruppoUsers.GNED.getCode());
			renderRequest.setAttribute("allBuyers",buyersList);

			/*
			 * Per salvaguardare le prestazioni, la lista delle targhe tecniche e location viene caricata
			 * solo se il ruolo è OMP_ENG o OMP_CONTROLLER e solo se la PR è in stato CREATA, unico stato 
			 * in cui è possibile modificare la targa tecnica.
			 */
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String userType = OmpHelper.getUserRoles(themeDisplay.getUserId());
			if(OmpRoles.OMP_ENG.getLabel().equals(userType) || OmpRoles.OMP_ENG_VBS.getLabel().equals(userType)
					|| OmpRoles.OMP_CONTROLLER.getLabel().equals(userType) || OmpRoles.OMP_CONTROLLER_VBS.getLabel().equals(userType)){

				String actionStatus = (String)renderRequest.getAttribute("actionStatus");
				if(actionStatus != null && PurchaseRequestStatus.CREATED.getLabel().equals(actionStatus)){
					List<LocationDTO> locationsList = LocationLocalServiceUtil.getLocationsList();
					renderRequest.setAttribute("allLocations", locationsList);					
				}
			}
			
		} catch (SystemException e) {
			logger.error("error happened during retrieving WBS code/Projects list",e);
		}
		super.doView(renderRequest, renderResponse);
	}

	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {

		handlePRForm(request,response);

		super.render(request, response);
	}

	private void handlePRForm(RenderRequest request, RenderResponse response) {
		PurchaseRequest purchaseRequest = null;
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		HttpServletRequest httpServletRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(request));

		String purchaseRequestID =httpServletRequest.getParameter("purchaseRequestID");//ParamUtil.getLong(request, "purchaseRequestID");
		String actionStatus =httpServletRequest.getParameter("actionStatus");//ParamUtil.getString(request, "actionStatus");
		String redirect=httpServletRequest.getParameter("redirect");

		if(redirect==null || "".equals(redirect)){
			redirect ="/group/vodafone/home"; 
		}

		String dollarSelected="";
		String euroSelected="";
		boolean hasUploadedFile=false;

		if (actionStatus == null || "".equals(actionStatus)) {
			actionStatus = PurchaseRequestStatus.CREATED.getLabel();
		}

		try {
			String userType = OmpHelper.getUserRoles(themeDisplay.getUserId());//OmpHelper.getUserGroup(request);

			List<Position> positions = new ArrayList<Position>();
			List<String> uploadedFiles = new ArrayList<String>();
			int numberOfPositions = 0;
			int positionStatus = PurchaseRequestPortletHelper.getPositionStatusfromActionStatus(actionStatus);
			if (purchaseRequestID != null && ! "".equals(purchaseRequestID)) {
				purchaseRequest = PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.valueOf(purchaseRequestID));
			} else {
				purchaseRequest = purchaseRequestFromRequest(request);

			}
			if(purchaseRequest.getPurchaseRequestId() >0){
				if(userType.equals(OmpRoles.OMP_NDSO.getLabel())) {
					positions = PositionLocalServiceUtil.getPositionsByIAndCType(purchaseRequest.getPurchaseRequestId(), IAndCType.ICFIELD.getLabel());
				}else if(userType.equals(OmpRoles.OMP_TC.getLabel())){
					positions = PositionLocalServiceUtil.getPositionsByIAndCType(purchaseRequest.getPurchaseRequestId(), IAndCType.ICTEST.getLabel());
				}else {	
					positions = PositionLocalServiceUtil.getPositionsByPrId(purchaseRequest.getPurchaseRequestId());
				}
				if (positions != null) {
					PurchaseRequestPortletHelper.AssignPositionsEditBtton(actionStatus,userType,positions);
					numberOfPositions = positions.size();
					//purchaseRequest.setTotalValue(String.valueOf(calculatePrTotalValue(positions)));
				}
				uploadedFiles=UploadDownloadHelper.getPRUploadedFilesNames(purchaseRequest.getPurchaseRequestId());
			} else{
				// 29.08.2019
				// purchaseRequest.setAutomatic(true);
				purchaseRequest.setAutomatic(false);
				purchaseRequest.setTotalValue("0");
			}

			boolean nssFromFileExcel = false;
			if(purchaseRequest.getTipoPr() == TipoPr.NOT_NSS.getCode() && purchaseRequest.isHasFile())
				nssFromFileExcel = true;

			String[] disableEnable = PurchaseRequestPortletHelper.getDiableEnableView(userType, actionStatus, purchaseRequest.getAutomatic(), nssFromFileExcel);

			String prDisabled = disableEnable[0];
			String assetNumberDisabled = disableEnable[1];
			String prcsNumberDisabled = disableEnable[2];
			String poNumberDisabled = disableEnable[3];
			String evoDisabled=disableEnable[4];
			String editPositionDisabled=disableEnable[5];
			userType = disableEnable[6];

			String poTargaTecnicaDisabled= disableEnable[7];
			String prDeleteBtn=disableEnable[8];
			String rejCauseDisabled= disableEnable[9];
			
			String prVendorDisabled = disableEnable[14];
			
			/*
			 * 04.11.2020
			 * Targa tecnica su posizioni
			 */
			//String prNotNssLocationEvoDisabled = disableEnable[16];
			//String prTargaTecnicaDisabled = disableEnable[18];

			// Not NSS
			assetNumberDisabled = "disabled";
			prcsNumberDisabled = "disabled";
			poNumberDisabled = "disabled";
			if(actionStatus.equalsIgnoreCase(PurchaseRequestStatus.PR_ASSIGNED.getLabel())){
				assetNumberDisabled="";
			}else if(actionStatus.equalsIgnoreCase(PurchaseRequestStatus.ASSET_RECEIVED.getLabel())){
				prcsNumberDisabled="";
			}else if(actionStatus.equalsIgnoreCase(PurchaseRequestStatus.SC_RECEIVED.getLabel())){
				poNumberDisabled="";
			}
			/////////

			if(purchaseRequest.getCurrency().equals(Currency.USD.getCode()+"")){
				dollarSelected="selected";
			}else if(purchaseRequest.getCurrency().equals(Currency.EUR.getCode()+"")){
				euroSelected="selected";
			}

			if ((purchaseRequest.getCostCenterId() ==null || "".equals(purchaseRequest.getCostCenterId()))	&& ! OmpRoles.OMP_CONTROLLER.getLabel().equals(userType)) {

				CostCenter costCenter = null;
				try {
					CostCenterUser costCenterUser = CostCenterUserLocalServiceUtil.getCostCenterUserByUserId(themeDisplay.getUserId());
					if (costCenterUser != null) {
						costCenter = CostCenterLocalServiceUtil.getCostCenter(costCenterUser.getCostCenterId());
					} else {
						throw new UserHasNoCostCenterException("the logged in user has't cost center");
					}
				} catch (Exception e) {
					logger.error("Erro in handle PR Form", e);
					SessionErrors.add(request, "error-costcenter");
				}

				if (costCenter != null) {
					purchaseRequest.setCostCenterId(String.valueOf(costCenter.getCostCenterId()));
				}
			}

			hasUploadedFile= UploadDownloadHelper.isPRHasUploadedFiles(purchaseRequest.getPurchaseRequestId());

			request.setAttribute("pr", purchaseRequest);
			request.setAttribute("positions", positions);
			request.setAttribute("uploadedFiles", uploadedFiles);
			request.setAttribute("actionStatus", actionStatus);
			request.setAttribute("numberOfPositions", numberOfPositions);
			request.setAttribute("prDisabled", prDisabled);
			request.setAttribute("evoDisabled", evoDisabled);
			request.setAttribute("editPositionDisabled", editPositionDisabled);
			request.setAttribute("assetNumberDisabled", assetNumberDisabled);
			request.setAttribute("prcsNumberDisabled", prcsNumberDisabled);
			request.setAttribute("poNumberDisabled", poNumberDisabled);
			request.setAttribute("userType", userType);
			request.setAttribute("dollarSelected", dollarSelected);
			request.setAttribute("euroSelected", euroSelected);
			request.setAttribute("hasUploadedFile", hasUploadedFile);
			request.setAttribute("redirect",redirect);
			request.setAttribute("userType",userType);
			request.setAttribute("poTargaTecnicaDisabled",poTargaTecnicaDisabled);
			//request.setAttribute("prNotNssLocationEvoDisabled", prNotNssLocationEvoDisabled);
			//request.setAttribute("prTargaTecnicaDisabled", prTargaTecnicaDisabled);
			request.setAttribute("prVendorDisabled", prVendorDisabled);
			
			if(purchaseRequest.getTotalValue() !=null && ! "".equals(purchaseRequest.getTotalValue())) {
				request.setAttribute("prTotalValue", OmpFormatterUtil.formatNumberByLocale(Double.valueOf(purchaseRequest.getTotalValue()),Locale.ITALY));
			}else {
				request.setAttribute("prTotalValue","0");
			}
			request.setAttribute("prDeleteBtn", prDeleteBtn);
			request.setAttribute("rejCauseDisabled", rejCauseDisabled);

			request.setAttribute("prNotNssDisabled", "");
			if(purchaseRequest != null && purchaseRequest.getTipoPr() == TipoPr.NOT_NSS.getCode() && purchaseRequest.isHasFile() && !OmpRoles.OMP_ANTEX.getLabel().equals(userType)){
				request.setAttribute("prNotNssDisabled", "disabled");
			}
			
			if(purchaseRequest != null && OmpRoles.OMP_ANTEX.getLabel().equals(userType) && 
					(actionStatus.equalsIgnoreCase(PurchaseRequestStatus.PR_ASSIGNED.getLabel()) || 
					 actionStatus.equalsIgnoreCase(PurchaseRequestStatus.ASSET_RECEIVED.getLabel()) || 
					 actionStatus.equalsIgnoreCase(PurchaseRequestStatus.SC_RECEIVED.getLabel()))){
				request.setAttribute("prNotNssDisabled", "disabled");
			}

		} catch (PortalException e) {
			logger.error("Erro in handle PR Form", e);
			SessionErrors.add(request, "error-handling");
		} catch (SystemException e) {
			logger.error("Erro in handle PR Form", e);
			SessionErrors.add(request, "error-handling");
		}catch (PRHasNoCostCenterException e) {
			logger.error("Erro in handle PR Form", e);
			SessionErrors.add(request, "error-costcenter");
		}catch (Exception e) {
			logger.error("Erro in handle PR Form", e);
			SessionErrors.add(request, "error-handling");
		}
	}

	public void serveResource(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		
		String action=request.getParameter("action");
		
		if("addPurchaseRequest".equals(action)){
			addPurchaseRequest(request, response);

		} else if("updatePurchaseRequest".equals(action)){
			updatePurchaseRequest(request, response);

		} else if("addPosition".equals(action)){
			addPosition(request, response);

		} else if("refreshPosition".equals(action)){
			refreshPosition(request, response);

		} else if("updatePosition".equals(action)){
			updatePosition(request, response);

		} else if("deletePosition".equals(action)){
			deletePosition(request, response);

		} else if("downloadFile".equals(action)){
			downloadFile(request, response);
		} else if("downloadAllFiles".equals(action)){
			downloadAllFiles(request, response);

		} else if("listFiles".equals(action)){
			listUploadedFiles(request, response);

		} else if("uploadFile".equals(action)){
			uploadFile(request, response);
			
		} else if("uploadFileOfferta".equals(action)){
			uploadFileOfferta(request, response);
			
		} else if("deleteFile".equals(action)){
			deleteFile(request, response);

		} else if("approvePosition".equals(action)){
			approvePosition(request, response);
			
		}else if("approvePositionSel".equals(action)){
			approvePositionSel(request, response);
			
		} else if("getLabelNumber".equals(action)){
			getLabelNumber(request, response);

		} else if("poulateTrackingValues".equals(request.getResourceID())){
			try {
				getTrackingCodeValues(request,response);
			} catch (Exception e) {
				logger.error("error in retrieve tracking code values", e);
			}
		}
	}

	/**
	 * Adds a new purchase order to the database.
	 * 
	 */
	private void addPurchaseRequest(ResourceRequest request, ResourceResponse response) throws PortletException, IOException{ 

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		PrintWriter writer = response.getWriter();

		try{
			PurchaseRequest purchaseRequest=purchaseRequestFromRequest(request);
			
			/*
			 * aggiungere controllo su tracking code
			 */
			String selectedTrackingCode = "";
			if(purchaseRequest.getTrackingCode().contains("||"))
				selectedTrackingCode = purchaseRequest.getTrackingCode().trim().split("\\|\\|")[0].trim();

			TrackingCodes tc = TrackingCodesLocalServiceUtil.getTrackingCodeByName(selectedTrackingCode);
			if(tc == null){
				JSONObject json = JSONFactoryUtil.createJSONObject();
				json.put("status","trackingCodeKO");
				writer.print(json);				
				return;
			}
			
			User user=themeDisplay.getUser();
			purchaseRequest.setCreatedUserId(user.getUserId());
			purchaseRequest.setScreenNameRequester(user.getScreenName());
			purchaseRequest.setStatus(PurchaseRequestStatus.CREATED.getCode());
			purchaseRequest.setCreatedDate(new Date(System.currentTimeMillis()));
			purchaseRequest.setTipoPr(TipoPr.NOT_NSS.getCode());
			purchaseRequest.setAutomatic(false);
			purchaseRequest=PurchaseRequestLocalServiceUtil.addPurchaseRequest(purchaseRequest);

			copyWbsFromPRToPositions(purchaseRequest);

			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("status","success");
			json.put("purchaseRequestID",purchaseRequest.getPurchaseRequestId());//print purchaseRequest ID

			writer.print(json);
		}
		catch(Exception e)
		{
			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("status","failure");
			logger.error("Error in add purchase Request ", e);
			writer.print(json);
		}
	}

	private void updatePurchaseRequest(ResourceRequest request, ResourceResponse response) throws PortletException, IOException{ 

		PurchaseRequest purchaseRequest = purchaseRequestFromRequest(request);

		PrintWriter writer = response.getWriter();
		try{
			/*
			 * aggiungere controllo su tracking code
			 */
			String selectedTrackingCode = "";
			if(purchaseRequest.getTrackingCode().contains("||"))
				selectedTrackingCode = purchaseRequest.getTrackingCode().trim().split("\\|\\|")[0].trim();

			TrackingCodes tc = TrackingCodesLocalServiceUtil.getTrackingCodeByName(selectedTrackingCode);
			if(tc == null){
				JSONObject json = JSONFactoryUtil.createJSONObject();
				json.put("status","trackingCodeKO");
				writer.print(json);				
				return;
			}
			
			PurchaseRequest existingPR = PurchaseRequestLocalServiceUtil.getPurchaseRequest(purchaseRequest.getPurchaseRequestId());
			purchaseRequest.setStatus(existingPR.getStatus());
			purchaseRequest.setCreatedDate(existingPR.getCreatedDate());
			purchaseRequest.setCreatedUserId(existingPR.getCreatedUserId());
			purchaseRequest.setScreenNameRequester(existingPR.getScreenNameRequester());
			// 29.08.2019
			// purchaseRequest.setAutomatic(true);
			purchaseRequest.setAutomatic(existingPR.getAutomatic());
			purchaseRequest.setFiscalYear(existingPR.getFiscalYear());
			purchaseRequest.setReceiverUserId(existingPR.getReceiverUserId());
			purchaseRequest.setScreenNameReciever(existingPR.getScreenNameReciever());
			purchaseRequest.setRejectionCause(existingPR.getRejectionCause());
			purchaseRequest.setTipoPr(TipoPr.NOT_NSS.getCode());

			purchaseRequest=PurchaseRequestLocalServiceUtil.updatePurchaseRequest(purchaseRequest);

			copyWbsFromPRToPositions(purchaseRequest);

			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("status","success");
			json.put("purchaseRequestID",purchaseRequest.getPurchaseRequestId());
			writer.print(json);
		}catch(Exception e){
			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("status","failure");
			logger.error("Error in update purchase Request ", e);
			writer.print(json);
		}
	}

	public void deletePurchaseRequest(ActionRequest request, ActionResponse response)
			throws Exception {

		PurchaseRequest purchaseRequest=purchaseRequestFromRequest(request);

		try{
			Set<String> purchasOrderSet=new HashSet<String>();
			List<Position> positions=new ArrayList<Position>();
			positions=PositionLocalServiceUtil.getPurchaseRequestPositions(String.valueOf(purchaseRequest.getPurchaseRequestId()));

			for(Position position :positions){
				purchasOrderSet.add(position.getPurchaseOrderId());
			}
			if(purchasOrderSet.size()>1){ // can't delete cause the positions in different POs
				//you can't delete
				logger.debug("you can't delete cause your positions is distributed between"+purchasOrderSet.size()+"  POs");
			}
			else{ //start delete
				logger.debug("you can delete");
				List<GoodReceipt> goodReceiptList=new ArrayList<GoodReceipt>();
				for(Position position :positions){
					goodReceiptList=GoodReceiptLocalServiceUtil.getPositionGoodReceipts(String.valueOf(position.getPositionId()));
					for(GoodReceipt goodReceipt : goodReceiptList ){
						GoodReceiptLocalServiceUtil.deleteGoodReceipt(goodReceipt); //delete GR
						logger.debug("GR number "+goodReceipt.getGoodReceiptId()+" deleted");
					}
					PositionLocalServiceUtil.deletePosition(position); //delete position
					logger.debug("position number "+position.getPositionId()+" deleted");
				}
				for(String po : purchasOrderSet){
					if(!"".equals(po)&&po!=null){
						PurchaseOrderLocalServiceUtil.deletePurchaseOrder(Long.parseLong(po));//delete PO
						logger.debug("PO number "+po+" deleted");
					}
				}
				PurchaseRequestLocalServiceUtil.deletePurchaseRequest(purchaseRequest.getPurchaseRequestId());  //delete PR
				logger.debug("PR number "+purchaseRequest.getPurchaseRequestId()+" deleted");
				UploadDownloadHelper.deletePRFileDirectory(purchaseRequest.getPurchaseRequestId());
			}
			sendRedirect(request, response);
		}catch(Exception e){
			logger.error("Error in delete purchase Request ", e);
			SessionErrors.add(request, "error-deleting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaserequestnotnss/view.jsp");
		}
	}


	public void submitPurchaseRequest(ActionRequest request, ActionResponse response)
			throws Exception {

		try{
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			PurchaseRequest purchaseRequest = purchaseRequestFromRequest(request);
			long pendingApproval = PositionLocalServiceUtil.getIAndCPositionsCount(String.valueOf(purchaseRequest.getPurchaseRequestId()));

			if(purchaseRequest.getPurchaseRequestId() > 0){
				PurchaseRequest existingPR = PurchaseRequestLocalServiceUtil.getPurchaseRequest(purchaseRequest.getPurchaseRequestId());
				purchaseRequest.setCreatedDate(existingPR.getCreatedDate());
				purchaseRequest.setCreatedUserId(existingPR.getCreatedUserId());
				purchaseRequest.setScreenNameRequester(existingPR.getScreenNameRequester());
				purchaseRequest.setAutomatic(existingPR.getAutomatic());
				purchaseRequest.setFiscalYear(existingPR.getFiscalYear());
				purchaseRequest.setRejectionCause(existingPR.getRejectionCause());
				purchaseRequest.setTipoPr(existingPR.getTipoPr());

				if(pendingApproval==0) {
					purchaseRequest.setStatus(PurchaseRequestStatus.SUBMITTED.getCode());
				}else {
					purchaseRequest.setStatus(PurchaseRequestStatus.PENDING_IC_APPROVAL.getCode());
				}

				PurchaseRequestLocalServiceUtil.updatePurchaseRequest(purchaseRequest,true);
			}else{
				purchaseRequest.setCreatedUserId(themeDisplay.getUserId());
				purchaseRequest.setCreatedDate(new Date(System.currentTimeMillis()));

				if(pendingApproval==0) {
					purchaseRequest.setStatus(PurchaseRequestStatus.SUBMITTED.getCode());
				}else {
					purchaseRequest.setStatus(PurchaseRequestStatus.PENDING_IC_APPROVAL.getCode());
				}
				purchaseRequest=PurchaseRequestLocalServiceUtil.addPurchaseRequest(purchaseRequest);
			}
			copyWbsFromPRToPositions(purchaseRequest);

			sendRedirect(request, response);

		}catch(Exception e){
			logger.error("Error in submit purchase Request ", e);
			SessionErrors.add(request, "error-submitting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaserequestnotnss/view.jsp");
		}
	}

	public void startPurchaseRequest(ActionRequest request, ActionResponse response)
			throws Exception {
		try{
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			PurchaseRequest purchaseRequest=purchaseRequestFromRequest(request);
			if(purchaseRequest.getPurchaseRequestId() > 0){
				purchaseRequest=PurchaseRequestLocalServiceUtil.getPurchaseRequest(purchaseRequest.getPurchaseRequestId());
				User user=themeDisplay.getUser();
				purchaseRequest.setReceiverUserId(String.valueOf(user.getUserId()));
				purchaseRequest.setScreenNameReciever(user.getScreenName());
				//purchaseRequest.setStatus(PurchaseRequestStatus.PR_ASSIGNED.getCode());
				purchaseRequest.setStatus(PurchaseRequestStatus.SC_RECEIVED.getCode());

				purchaseRequest = PurchaseRequestLocalServiceUtil.updatePurchaseRequest(purchaseRequest);
				logger.debug("PR no "+purchaseRequest.getPurchaseRequestId()+"is assigned to antex user "+purchaseRequest.getPurchaseRequestId());
			}
			sendRedirect(request, response);

		}catch(Exception e){
			logger.error("Error in assign purchase Request ", e);
			SessionErrors.add(request, "error-submitting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaserequestnotnss/view.jsp");
		}


	}

	public void approvePositionAll(ActionRequest request, ActionResponse response) throws Exception {
		
		try{
			String actionStatus = ParamUtil.getString(request, "actionStatus");
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			PurchaseRequest purchaseRequest = purchaseRequestFromRequest(request);
			
			List<Position> positionList = PositionLocalServiceUtil.getPositionsByPrId(purchaseRequest.getPurchaseRequestId());
			
			for(Position position : positionList){
				approvePosition(position, actionStatus, themeDisplay);
			}
			
			sendRedirect(request, response);
			
		} catch(Exception e){
			logger.error("Error in assign purchase Request ", e);
			SessionErrors.add(request, "error-submitting");
            PortalUtil.copyRequestParameters(request, response);
            response.setRenderParameter("jspPage", "/html/purchaserequestnotnss/view.jsp");
		}
	}

	private void addPosition(ResourceRequest request, ResourceResponse response) throws PortletException, IOException{

		String jspPage = "/html/purchaserequestnotnss/positionlist.jsp";
		String actionStatus=ParamUtil.getString(request, "actionStatus");
		
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		
		PrintWriter writer = response.getWriter();
		
		Position position = positionFromRequest(request);
		try{
			/*
			 * aggiungere controllo su targa tecnica e location
			 */
			if(position.getTargaTecnica() != null && position.getTargaTecnica().trim().length() > 0){
				LocationDTO locationTarga = LocationLocalServiceUtil.getLocationByTargaTecnica(position.getTargaTecnica());
				if(locationTarga == null){
					JSONObject json = JSONFactoryUtil.createJSONObject();
					json.put("status","targaTecnicaKO");
					writer.print(json);				
					return;
				}
			}
			
			position.setCreatedUserId(themeDisplay.getUserId());
			position.setCreatedDate(new Date(System.currentTimeMillis()));
			position=PositionLocalServiceUtil.addPosition(position);
		}
		catch(Exception e){
			logger.error("Error in add new position ", e);
		}
		try{
			String userType =OmpHelper.getUserRoles(themeDisplay.getUserId());
			List<Position> positions=new ArrayList<Position>();
			int positionStatus=PurchaseRequestPortletHelper.getPositionStatusfromActionStatus(actionStatus);
			double prTotalValue=0;
			PurchaseRequest pr=PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.parseLong(position.getPurchaseRequestId().trim()));
			if(pr.getTotalValue()!=null && ! "".equals(pr.getTotalValue())) {
				prTotalValue=Double.valueOf(pr.getTotalValue());
			}

			positions=PositionLocalServiceUtil.getPositionsByPrId(Long.parseLong(position.getPurchaseRequestId().trim()));
			PurchaseRequestPortletHelper.AssignPositionsEditBtton(actionStatus,userType,positions);
			request.setAttribute("positions", positions);
			request.setAttribute("numberOfPositions", positions.size());
			request.setAttribute("prTotalValue", OmpFormatterUtil.formatNumberByLocale(prTotalValue,Locale.ITALY));
			getPortletContext().getRequestDispatcher(jspPage).include(request, response);
			//writer.write(createPositionsJson(positions,prTotalValue));
		}
		catch(Exception e){
			logger.error("Error in listing the positions in add new position ", e);
		}
	}

	private void refreshPosition(ResourceRequest request, ResourceResponse response) throws PortletException, IOException{

		String jspPage = "/html/purchaserequestnotnss/positionlist.jsp";
		String actionStatus = ParamUtil.getString(request, "actionStatus");
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		try{
			String userType = OmpHelper.getUserRoles(themeDisplay.getUserId());
			List<Position> positions = new ArrayList<Position>();
			positions = PositionLocalServiceUtil.getPositionsByPrId(Long.parseLong(ParamUtil.getString(request, "purchaseRequestId").trim()));
			PurchaseRequestPortletHelper.AssignPositionsEditBtton(actionStatus,userType,positions);
			request.setAttribute("positions", positions);
			request.setAttribute("numberOfPositions", positions.size());
			getPortletContext().getRequestDispatcher(jspPage).include(request, response);
		}
		catch(Exception e){
			logger.error("Error in listing the positions in add new position ", e);
		}
	}

	private void updatePosition(ResourceRequest request, ResourceResponse response) throws PortletException, IOException{
		String jspPage = "/html/purchaserequestnotnss/positionlist.jsp";
		String actionStatus=ParamUtil.getString(request, "actionStatus");
		PrintWriter writer = response.getWriter();
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		try{
			String userType =OmpHelper.getUserRoles(themeDisplay.getUserId());
			Position position=positionFromRequest(request);
			Position oldPosition=PositionLocalServiceUtil.getPosition(position.getPositionId());
			position.setCreatedDate(oldPosition.getCreatedDate());
			position.setCreatedUserId(oldPosition.getCreatedUserId());
			position.setApprover(oldPosition.getApprover());
			position.setApproved(oldPosition.getApproved());
			position.setPurchaseOrderId(oldPosition.getPurchaseOrderId());
			position.setPoLabelNumber(oldPosition.getPoLabelNumber());
			PositionLocalServiceUtil.updatePosition(position);
			List<Position> positions=new ArrayList<Position>();
			int positionStatus=PurchaseRequestPortletHelper.getPositionStatusfromActionStatus(actionStatus);
			double prTotalValue=0;
			PurchaseRequest pr=PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.parseLong(position.getPurchaseRequestId().trim()));
			if(pr.getTotalValue()!=null && ! "".equals(pr.getTotalValue())) {
				prTotalValue=Double.valueOf(pr.getTotalValue());
			}
			positions=PositionLocalServiceUtil.getPositionsByPrId(Long.parseLong(position.getPurchaseRequestId()));
			PurchaseRequestPortletHelper.AssignPositionsEditBtton(actionStatus,userType,positions);
			request.setAttribute("positions", positions);
			request.setAttribute("numberOfPositions", positions.size());
			request.setAttribute("prTotalValue", OmpFormatterUtil.formatNumberByLocale(prTotalValue,Locale.ITALY));
			getPortletContext().getRequestDispatcher(jspPage).include(request, response);
			//.writer.write(createPositionsJson(positions,prTotalValue));
		}
		catch(Exception e){
			logger.error("Error in update position ", e);
		}

	}
	
	private void approvePositionSel(ResourceRequest request, ResourceResponse response) throws PortletException, IOException{
		
		try{
			String jspPage = "/html/purchaserequestnotnss/positionlist.jsp";
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			String userType = OmpHelper.getUserRoles(themeDisplay.getUserId());
			
			String actionStatus = ParamUtil.getString(request, "actionStatus");
			
			Long prId = 0L;
			
			String positionSel = ParamUtil.getString(request, "positionSel");
			String[] positionSelList = positionSel.split(";");
			
			for(String pos : positionSelList){
				Position position = PositionLocalServiceUtil.getPosition(Long.parseLong(pos));
				position.setApproved(true);
				position.setApprover(String.valueOf(themeDisplay.getUserId()));
				PositionLocalServiceUtil.updatePosition(position,true);
				prId = Long.parseLong(position.getPurchaseRequestId().trim());
			}
			
			PurchaseRequest pr = PurchaseRequestLocalServiceUtil.getPurchaseRequest(prId);
			List<Position> positions = new ArrayList<Position>();
			List<Position> remainingPositions = new ArrayList<Position>();
			double prTotalValue = 0;
			if(pr.getTotalValue() != null && ! "".equals(pr.getTotalValue())) {
				prTotalValue = Double.valueOf(pr.getTotalValue());
			}

			if(userType.equals(OmpRoles.OMP_NDSO.getLabel())) {
				positions = PositionLocalServiceUtil.getPositionsByIAndCType(pr.getPurchaseRequestId(), IAndCType.ICFIELD.getLabel());
				remainingPositions = PositionLocalServiceUtil.getPositionsByIAndCTypeAndApprovalStatus
						(pr.getPurchaseRequestId(),  IAndCType.ICFIELD.getLabel(), false);
				if(remainingPositions.size() == 0 || remainingPositions.get(0) == null) {
					request.setAttribute("hideReject", "true");
				}
			}else if(userType.equals(OmpRoles.OMP_TC.getLabel())){
				positions = PositionLocalServiceUtil.getPositionsByIAndCType(pr.getPurchaseRequestId(), IAndCType.ICTEST.getLabel());
				remainingPositions = PositionLocalServiceUtil.getPositionsByIAndCTypeAndApprovalStatus
						(pr.getPurchaseRequestId(),  IAndCType.ICTEST.getLabel(), false);
				if(remainingPositions.size() ==0 || remainingPositions.get(0) == null) {
					request.setAttribute("hideReject", "true");
				}
			}else if(userType.equals(OmpRoles.OMP_CONTROLLER.getLabel())){
				positions = PositionLocalServiceUtil.getPositionsByPrId(pr.getPurchaseRequestId());
			}
			PurchaseRequestPortletHelper.AssignPositionsEditBtton(actionStatus,userType,positions);

			request.setAttribute("positions", positions);
			request.setAttribute("numberOfPositions", positions.size());
			request.setAttribute("prTotalValue", OmpFormatterUtil.formatNumberByLocale(prTotalValue,Locale.ITALY));

			getPortletContext().getRequestDispatcher(jspPage).include(request, response);
		
		}
		catch(Exception e){
			logger.error("Error in update position ", e);
		}	
	}

	private void approvePosition(ResourceRequest request, ResourceResponse response) throws PortletException, IOException{
		String jspPage = "/html/purchaserequestnotnss/positionlist.jsp";
		String actionStatus=ParamUtil.getString(request, "actionStatus");
		PrintWriter writer = response.getWriter();
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		try{
			String userType =OmpHelper.getUserRoles(themeDisplay.getUserId());
			Position position=PositionLocalServiceUtil.getPosition(positionFromRequest(request).getPositionId());
			position.setApproved(true);
			position.setApprover(String.valueOf(themeDisplay.getUserId()));
			PositionLocalServiceUtil.updatePosition(position,true);

			PurchaseRequest pr=PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.parseLong(position.getPurchaseRequestId().trim()));
			List<Position> positions=new ArrayList<Position>();
			List<Position> remainingPositions=new ArrayList<Position>();
			double prTotalValue=0;
			if(pr.getTotalValue()!=null && ! "".equals(pr.getTotalValue())) {
				prTotalValue=Double.valueOf(pr.getTotalValue());
			}

			if(userType.equals(OmpRoles.OMP_NDSO.getLabel())) {
				positions = PositionLocalServiceUtil.getPositionsByIAndCType(pr.getPurchaseRequestId(), IAndCType.ICFIELD.getLabel());
				remainingPositions=PositionLocalServiceUtil.getPositionsByIAndCTypeAndApprovalStatus
						(pr.getPurchaseRequestId(),  IAndCType.ICFIELD.getLabel(), false);
				if(remainingPositions.size() ==0 || remainingPositions.get(0)==null) {
					request.setAttribute("hideReject", "true");
				}
			}else if(userType.equals(OmpRoles.OMP_TC.getLabel())){
				positions = PositionLocalServiceUtil.getPositionsByIAndCType(pr.getPurchaseRequestId(), IAndCType.ICTEST.getLabel());
				remainingPositions=PositionLocalServiceUtil.getPositionsByIAndCTypeAndApprovalStatus
						(pr.getPurchaseRequestId(),  IAndCType.ICTEST.getLabel(), false);
				if(remainingPositions.size() ==0 || remainingPositions.get(0)==null) {
					request.setAttribute("hideReject", "true");
				}
			}else if(userType.equals(OmpRoles.OMP_CONTROLLER.getLabel())){
				positions = PositionLocalServiceUtil.getPositionsByPrId(pr.getPurchaseRequestId());
			}
			PurchaseRequestPortletHelper.AssignPositionsEditBtton(actionStatus,userType,positions);
			request.setAttribute("positions", positions);
			request.setAttribute("numberOfPositions", positions.size());
			request.setAttribute("prTotalValue", OmpFormatterUtil.formatNumberByLocale(prTotalValue,Locale.ITALY));
			getPortletContext().getRequestDispatcher(jspPage).include(request, response);
			//writer.write(createPositionsJson(positions,prTotalValue));
		}
		catch(Exception e){
			logger.error("Error in update position ", e);
		}

	}

	private void approvePosition(Position position, String actionStatus, ThemeDisplay themeDisplay) throws PortletException, IOException{
		
		try{
			String userType = OmpHelper.getUserRoles(themeDisplay.getUserId());
			position.setApproved(true);
			position.setApprover(String.valueOf(themeDisplay.getUserId()));
			PositionLocalServiceUtil.updatePosition(position,true);

			PurchaseRequest pr=PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.parseLong(position.getPurchaseRequestId().trim()));
			List<Position> positions = new ArrayList<Position>();

			if(userType.equals(OmpRoles.OMP_NDSO.getLabel())) {
				positions = PositionLocalServiceUtil.getPositionsByIAndCType(pr.getPurchaseRequestId(), IAndCType.ICFIELD.getLabel());
			}else if(userType.equals(OmpRoles.OMP_TC.getLabel())){
				positions = PositionLocalServiceUtil.getPositionsByIAndCType(pr.getPurchaseRequestId(), IAndCType.ICTEST.getLabel());

			}else if(userType.equals(OmpRoles.OMP_CONTROLLER.getLabel())){
				positions = PositionLocalServiceUtil.getPositionsByPrId(pr.getPurchaseRequestId());
			}
			PurchaseRequestPortletHelper.AssignPositionsEditBtton(actionStatus,userType, positions);
		}
		catch(Exception e){
			logger.error("Error in update position ", e);
		}
	}

	private void deletePosition(ResourceRequest request, ResourceResponse response) throws PortletException, IOException{
		String jspPage = "/html/purchaserequestnotnss/positionlist.jsp";
		String actionStatus=ParamUtil.getString(request, "actionStatus");
		PrintWriter writer = response.getWriter();
		List<GoodReceipt> goodReceiptList=new ArrayList<GoodReceipt>();
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		try{
			String userType =OmpHelper.getUserRoles(themeDisplay.getUserId());
			Position position=positionFromRequest(request);
			goodReceiptList=GoodReceiptLocalServiceUtil.getPositionGoodReceipts(String.valueOf(position.getPositionId()));
			for(GoodReceipt goodReceipt : goodReceiptList ){
				GoodReceiptLocalServiceUtil.deleteGoodReceipt(goodReceipt); //delete GR
			}
			PositionLocalServiceUtil.deletePosition(position);

			double prTotalValue=0;
			PurchaseRequest pr=PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.parseLong(position.getPurchaseRequestId().trim()));
			if(pr.getTotalValue()!=null && ! "".equals(pr.getTotalValue())) {
				prTotalValue=Double.valueOf(pr.getTotalValue());
			}
			List<Position> positions=new ArrayList<Position>();
			int positionStatus=PurchaseRequestPortletHelper.getPositionStatusfromActionStatus(actionStatus); 
			positions=PositionLocalServiceUtil.getPositionsByPrId(Long.parseLong(position.getPurchaseRequestId()));
			PurchaseRequestPortletHelper.AssignPositionsEditBtton(actionStatus,userType,positions);
			request.setAttribute("positions", positions);
			request.setAttribute("numberOfPositions", positions.size());
			request.setAttribute("prTotalValue", OmpFormatterUtil.formatNumberByLocale(prTotalValue,Locale.ITALY));
			getPortletContext().getRequestDispatcher(jspPage).include(request, response);
			//writer.write(createPositionsJson(positions,prTotalValue));
		}
		catch(Exception e){
			logger.error("Error in deleting position ", e);
		}
	}


	public void uploadFile(ResourceRequest request, ResourceResponse response) throws IOException {
		
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		long purchaseRequestId = ParamUtil.getLong(request, "purchaseRequestId");

		UploadPortletRequest uploadRequest = PortalUtil.getUploadPortletRequest(request);

		logger.info("Start uploading excel file");
		Map<Integer, String> errors = new LinkedHashMap<Integer, String>();
		if (uploadRequest.getSize("fileName")==0) {
			errors.put(1,"Empty file");
			SessionErrors.add(request, "Empty File, Please upload the right excel file");
		}

		List<PurchaseRequestExcelRow> readExcelFile = new ArrayList<PurchaseRequestExcelRow>();

		try {
			String inputFilename = uploadRequest.getFile("fileName").getName();
			
	        if ("xls".equals(inputFilename.substring(inputFilename.lastIndexOf(".") + 1, inputFilename.length()))) {
	        	readExcelFile = PurchaseRequestExcelFileHelper.readXlsNotNssFile(uploadRequest.getFile("fileName"));
	        	
	        } else if ("xlsx".equals(inputFilename.substring(inputFilename.lastIndexOf(".") + 1, inputFilename.length()))) {
	        	readExcelFile = PurchaseRequestExcelFileHelper.readXlsxNotNssFile(uploadRequest.getFile("fileName"));
	        }

		} catch (Exception e) {
			errors.put(1, "Wrong file, please upload the right excel file");
			logger.error("Error in reading excel file",e);
		}

		try {
			PurchaseRequest purchaseRequest = elaboraPurchaseRequest(themeDisplay, readExcelFile, errors, purchaseRequestId);

			if (readExcelFile.size()==0) {
				errors.put(1,"Empty file");
				SessionErrors.add(request, "Empty File, Please upload the right excel file");
			} else {

				logger.info("End uploading excel file");

				purchaseRequestId = purchaseRequest.getPurchaseRequestId();

				try {
					UploadDownloadHelper.uploadFilePR(uploadRequest, purchaseRequestId, request);

					JSONObject json = JSONFactoryUtil.createJSONObject();
					json.put("name","success");
					json.put("purchaseRequestID",purchaseRequestId);
					
					if(purchaseRequest.getCurrency().equals(Currency.USD.getCode()+"")){
						json.put("currencyCode", "0");
						json.put("currencyDesc", "Dollar");

					}else if(purchaseRequest.getCurrency().equals(Currency.EUR.getCode()+"")){
						json.put("currencyCode", "1");
						json.put("currencyDesc", "Euro");
					}
					
					Vendor vendor = VendorLocalServiceUtil.getVendor(Long.parseLong(purchaseRequest.getVendorId()));
					json.put("vendorId", vendor.getVendorId());
					json.put("vendorName", vendor.getVendorName());

					if(purchaseRequest.getTotalValue() != null && ! "".equals(purchaseRequest.getTotalValue())) {
						json.put("prTotalValue", OmpFormatterUtil.formatNumberByLocale(Double.valueOf(purchaseRequest.getTotalValue()),Locale.ITALY));
					}else {
						json.put("prTotalValue","0");
					}

					response.getWriter().print(json);

				} catch (UploadException ue) {
					logger.error(ue.getMessage());
					response.getWriter().write("failed");

				} catch (Exception e) {
					logger.error(e.getMessage());
					response.getWriter().write("failed");
				}
			}

		} catch (UploadNssPrExcelException e) {
			logger.error("Error in upload File! ", e);

			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("error","Error in upload File: " + e.getMessage());
			response.getWriter().print(json);
		}
	}

	public void downloadFile(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {

		long purchaseRequestId = ParamUtil.getLong(request, "purchaseRequestId");
		String filename=ParamUtil.getString(request, "fileName");
		try {
			UploadDownloadHelper.downloadFileByName(purchaseRequestId,filename,response);
		} catch (UploadException ue) {
			logger.error(ue.getMessage());
			response.getWriter().write("failed");

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
	}

	public void downloadAllFiles(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		long purchaseRequestId = ParamUtil.getLong(request, "purchaseRequestId");
		try {
			UploadDownloadHelper.downloadZipFile(purchaseRequestId,response);
		} catch (UploadException ue) {
			logger.error(ue.getMessage());
			response.getWriter().write("failed");

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
	}
	public void deleteFile(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {

		long purchaseRequestId = ParamUtil.getLong(request, "purchaseRequestId");
		String filename=ParamUtil.getString(request, "fileName");
		try {
			UploadDownloadHelper.deleteFileByName(purchaseRequestId,filename);
			listUploadedFiles(request, response);
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
	}

	public void getLabelNumber(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {

		long purchaseRequestId = ParamUtil.getLong(request, "purchaseRequestId");
		try {
			long newMaxLabel=0;
			long maxLabel=0;
			List<Long> maxLabelList=PositionLocalServiceUtil.getPositionsMaxLabelNumber(purchaseRequestId);
			if(maxLabelList.size() >0 && maxLabelList.get(0)!=null) {
				maxLabel=maxLabelList.get(0);
			}
			newMaxLabel=maxLabel+10;
			response.getWriter().write(String.valueOf(newMaxLabel));
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
	}

	public void listUploadedFiles(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {

		long purchaseRequestId = ParamUtil.getLong(request, "purchaseRequestId");
		String jspPage = "/html/purchaserequestnotnss/filelist.jsp";
		try {
			List<String> uploadedFiles = new ArrayList<String>();
			uploadedFiles=UploadDownloadHelper.getPRUploadedFilesNames(purchaseRequestId);
			request.setAttribute("uploadedFiles", uploadedFiles);
			getPortletContext().getRequestDispatcher(jspPage).include(request, response);
		}catch (Exception e) {
			logger.error("error while retrieving the files list",e);
		}
	}


	private PurchaseRequest purchaseRequestFromRequest(PortletRequest request) {
		
		PurchaseRequest purchaseRequest = new PurchaseRequestImpl();
		purchaseRequest.setPurchaseRequestId(ParamUtil.getLong(request,"purchaseRequestId"));

		try {
			PurchaseRequest oldPurchaseRequest = PurchaseRequestLocalServiceUtil.getPurchaseRequest(purchaseRequest.getPurchaseRequestId());
			if(oldPurchaseRequest !=null) {
				purchaseRequest.setTotalValue(oldPurchaseRequest.getTotalValue());
			}
		} catch (Exception e) {

		}
		
		purchaseRequest.setCostCenterId(ParamUtil.getString(request,"costcenter"));
		purchaseRequest.setOla(ParamUtil.getString(request,"ola"));
		purchaseRequest.setBuyerId(ParamUtil.getString(request,"buyer"));
		purchaseRequest.setVendorId(ParamUtil.getString(request,"vendor"));
		String projectId = ParamUtil.getString(request,"projectname");
		purchaseRequest.setProjectId(projectId);
		purchaseRequest.setCurrency(ParamUtil.getString(request,"prCurrency"));
		purchaseRequest.setActivityDescription(ParamUtil.getString(request,"activitydescription"));
		purchaseRequest.setAutomatic(false);
		purchaseRequest.setTrackingCode(ParamUtil.getString(request,"trackingcode"));
		purchaseRequest.setWbsCodeId(ParamUtil.getString(request,"prwbscode"));

		return purchaseRequest;

	}

	private Position positionFromRequest(PortletRequest request) {

		Position position=new PositionImpl();
		position.setPositionId(ParamUtil.getLong(request, "positionId"));
		position.setLabelNumber(ParamUtil.getLong(request, "labelNumber"));
		position.setPurchaseRequestId(ParamUtil.getString(request, "purchaseRequestId"));
		position.setFiscalYear(ParamUtil.getString(request, "fiscalyear"));
		position.setCategoryCode(ParamUtil.getString(request, "categorycode"));
		position.setOfferNumber(ParamUtil.getString(request, "offernumber"));
		position.setOfferDate(stringToDate(ParamUtil.getString(request,"offerdate")));

		String quatity=ParamUtil.getString(request, "quantity");
		if(quatity !=null) {
			quatity=quatity.replace(",", ".");
		}
		position.setQuatity(quatity);
		position.setDeliveryDate(stringToDate(ParamUtil.getString(request, "deliverydate")));
		position.setDeliveryAddress(ParamUtil.getString(request, "deliveryadress"));
		position.setEvoLocation(ParamUtil.getString(request, "locationEvo"));
		position.setAssetNumber(ParamUtil.getString(request, "asset-number"));
		position.setShoppingCart(ParamUtil.getString(request, "prcs-number"));
		position.setPoNumber(ParamUtil.getString(request, "po-number"));

		String actualUnitCost=ParamUtil.getString(request, "actual-unit-cost");
		if(actualUnitCost !=null) {
			actualUnitCost=actualUnitCost.replace(",", ".");
		}
		position.setActualUnitCost(actualUnitCost);
		position.setDescription(ParamUtil.getString(request, "position-description"));

		position.setIcApproval(ParamUtil.getString(request, "iandc"));
		position.setEvoCodMateriale(ParamUtil.getString(request, "evocodmateriale"));
		position.setEvoDesMateriale(ParamUtil.getString(request, "evodesmateriale"));
		
		position.setTargaTecnica(ParamUtil.getString(request, "targaTecnica"));
		position.setEvoLocation(ParamUtil.getString(request, "locationEvo"));

		return position;

	}

	public long calculatePrTotalValue(List<Position> positions) {
		long totalValue=0;
		try {
			for(Position position:positions)
			{
				if(position.getActualUnitCost()!=null && ! "".equals(position.getActualUnitCost())
						&&position.getQuatity()!=null && !"".equals(position.getQuatity()))
					totalValue=totalValue+(Integer.parseInt(position.getActualUnitCost())*Integer.parseInt(position.getQuatity()));
			}
		}catch(Exception e) {
			logger.error("you have unitcost or quantity is not number");
			return 0;
		}

		return totalValue;
	}

	private Date stringToDate(String date){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy"); 
		try {
			return df.parse(date);
		} catch (ParseException e) {
			return null;
		}
	}

	private String DateToString(Date date){
		try{
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy"); 
			return df.format(date);
		}catch(Exception e){
			return "";
		}
	}


	private void getMicroDrivers(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws SystemException, IOException {

		try {
			JSONObject optionsJSON = JSONFactoryUtil.createJSONObject();
			Long macroId = ParamUtil.getLong(resourceRequest, "macroId");
			Long microDriverId = ParamUtil.getLong(resourceRequest, "microDriverId");

			List<MicroDriver> microDriversList= null;
			if(macroId !=null && macroId !=0) {
				MacroDriver macroDriver=MacroDriverLocalServiceUtil.getMacroDriver(macroId);
				if(macroDriver.getDisplay()) {
					microDriversList = MicroDriverLocalServiceUtil.getMicroDriverByMacroDriverId(macroId);
				}else {
					if(microDriverId !=null && microDriverId !=0) {
						MicroDriver microDriver=MicroDriverLocalServiceUtil.getMicroDriver(microDriverId);
						microDriversList=new ArrayList<MicroDriver>();
						microDriversList.add(microDriver);
					}
				}
			}

			if(microDriversList == null){
				microDriversList = new ArrayList<MicroDriver>();
			}
			JSONArray jsonmicroDriverArray = JSONFactoryUtil.createJSONArray();
			for (MicroDriver microDriver : microDriversList) {
				JSONObject json = JSONFactoryUtil.createJSONObject();
				json.put("id", microDriver.getMicroDriverId());
				json.put("name", microDriver.getMicroDriverName());
				json.put("display", microDriver.getDisplay());

				jsonmicroDriverArray.put(json);
			}
			optionsJSON.put("microDrivers", jsonmicroDriverArray);
			PrintWriter writer = resourceResponse.getWriter();
			writer.write(optionsJSON.toString());
		}catch(Exception e) {
			logger.error("error while getMicroDrivers Ajax", e);
		}
	}

	private void getTrackingCodeValues(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws SystemException, IOException,PortalException {
		JSONObject optionsJSON = JSONFactoryUtil.createJSONObject();
		String selectedTrackingCode = ParamUtil.getString(resourceRequest, "selectedTrackingCode");
		logger.info("selected tracking code is {}",selectedTrackingCode);
		if(selectedTrackingCode.contains("||")) {

			selectedTrackingCode = selectedTrackingCode.trim().split("\\|\\|")[0].trim();
			logger.info("||||splitted tracking code {}",selectedTrackingCode);
		}
		TrackingCodes trackingCodesValues= TrackingCodesLocalServiceUtil.getTrackingCodeByName(selectedTrackingCode);
		//optionsJSON.put("trackingCodesValues", JSONFactoryUtil.serialize(trackingCodesValues));
		if(trackingCodesValues != null) {
			logger.info("tracking code not equal null");
			optionsJSON.put("budgetCategoryId",trackingCodesValues.getBudgetCategoryId());
			optionsJSON.put("budgetSubCategoryId",trackingCodesValues.getBudgetSubCategoryId());
			optionsJSON.put("macroDriverId",trackingCodesValues.getMacroDriverId());
			optionsJSON.put("microDriverId",trackingCodesValues.getMicroDriverId());
			optionsJSON.put("odnpNameId",trackingCodesValues.getOdnpNameId());
			optionsJSON.put("projectId",trackingCodesValues.getProjectId());
			optionsJSON.put("subProjectId",trackingCodesValues.getSubProjectId());
			optionsJSON.put("wbsCodeId",trackingCodesValues.getWbsCodeId());
		} else {
			logger.info("tracking code is null");
			optionsJSON.put("empty","true");
		}


		PrintWriter writer = resourceResponse.getWriter();
		writer.write(optionsJSON.toString());
	}



	public void rejectPurchaseRequest(ActionRequest request, ActionResponse response)
			throws Exception {
		try{
			long prId = ParamUtil.getLong(request,"purchaseRequestId");
			String rejCause = ParamUtil.getString(request,"rejectionCause");

			PurchaseRequest purchaseRequest=PurchaseRequestLocalServiceUtil.getPurchaseRequest(prId);
			if(purchaseRequest.getPurchaseRequestId() > 0){
				purchaseRequest.setStatus(PurchaseRequestStatus.REJECTED.getCode());
				purchaseRequest.setRejectionCause(rejCause);

				purchaseRequest=PurchaseRequestLocalServiceUtil.updatePurchaseRequest(purchaseRequest);

				List<Position> iAndCPositions=PositionLocalServiceUtil.getIAndCPositions(purchaseRequest.getPurchaseRequestId());
				if(iAndCPositions.size() >0 &&iAndCPositions.get(0) !=null) {
					for(Position pos :iAndCPositions) {
						pos.setApproved(false);
						PositionLocalServiceUtil.updatePositionWithoutHocks(pos, true);
					}
				}
			}
			sendRedirect(request, response);

		}catch(Exception e){
			logger.error("Error in assign purchase Request ", e);
			SessionErrors.add(request, "error-submitting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaserequestnotnss/view.jsp");
		}
	}

	public void copyWbsFromPRToPositions(PurchaseRequest pr) throws SystemException{
		if(pr.getWbsCodeId()!=null || ! "".equals(pr.getWbsCodeId())) {
			List<Position> positions= PositionLocalServiceUtil.getPositionsByPrId(pr.getPurchaseRequestId());
			if(positions !=null) {
				for(Position position :positions) {
					position.setWbsCodeId(pr.getWbsCodeId());
					PositionLocalServiceUtil.updatePosition(position,true);
				}
			}
		}
	}

	private PurchaseRequest elaboraPurchaseRequest(ThemeDisplay themeDisplay, 
			List<PurchaseRequestExcelRow> purchaseRequestRows, 
			Map<Integer, String> errors, 
			long purchaseRequestId) throws UploadNssPrExcelException { 
		
		PurchaseRequest	purchaseRequest = creaPurchaseRequest(purchaseRequestRows.get(0), themeDisplay, errors);
		if (purchaseRequestId > 0)
			purchaseRequest.setPurchaseRequestId(purchaseRequestId);
		
		List<Position> postionList = new ArrayList<Position>();

		int pLabelNumber = 1;
		double totalValue = 0;
		for(PurchaseRequestExcelRow prRow :purchaseRequestRows){

			// 21.06.2019 
			// si puo’ fare nella stessa richiesta: posizioni diverse per location diverse
			
//			if(purchaseRequest.getEvoLocationId() != null && prRow.getEvoLocationId() != null){
//				if(!purchaseRequest.getEvoLocationId().trim().equals(prRow.getEvoLocationId().trim())){
//					throw new UploadNssPrExcelException("There are different values ​​of Header Text");
//				}
//			}
			
			if(prRow.getEvoLocationId() != null && prRow.getEvoLocationId().trim().length() > 0){
				if(prRow.getTargaTecnica() == null || "".equals("" + prRow.getTargaTecnica())){
					throw new UploadNssPrExcelException("Wrong EVO Location");
				} else{
					LocationDTO locationTarga;
					try {
						locationTarga = LocationLocalServiceUtil.getLocationByTargaTecnica(prRow.getTargaTecnica());
						if(locationTarga == null){
							throw new UploadNssPrExcelException("Targa tecnica non presente in archivio");
						}
					} catch (SystemException e) {
						logger.error("Error in purchaseRequestFromRow ", e);
					}
				}
			}
			
			if(prRow.getTargaTecnica() != null && prRow.getTargaTecnica().trim().length() > 0){
				if(prRow.getEvoLocationId() == null || "".equals("" + prRow.getEvoLocationId())){
					throw new UploadNssPrExcelException("Wrong Targa Tecnica");
				} else{
					LocationDTO locationTarga;
					try {
						locationTarga = LocationLocalServiceUtil.getLocationByTargaTecnica(prRow.getTargaTecnica());
						if(locationTarga == null){
							throw new UploadNssPrExcelException("Targa tecnica non presente in archivio");
						}
					} catch (SystemException e) {
						logger.error("Error in purchaseRequestFromRow ", e);
					}
				}
			}

			Position position = positionFromRow(prRow);
			String quantity = position.getQuatity();
			if (quantity != null && quantity.trim().length() > 0 && !"0".equals(quantity.trim())) {
				position.setCreatedUserId(themeDisplay.getUserId());
				position.setCreatedDate(new Date(System.currentTimeMillis()));
				position.setLabelNumber(pLabelNumber * 10);
				pLabelNumber = pLabelNumber + 1;
				position.setPositionEditBtton(true);
	
				if(position.getActualUnitCost()!= null && !"".equals(position.getActualUnitCost())
						&&position.getQuatity()!= null && !"".equals(position.getQuatity())){
					totalValue = totalValue + Double.parseDouble(position.getActualUnitCost()) * Double.parseDouble(position.getQuatity());
				}
				postionList.add(position);
			}
		}

		try {
			purchaseRequest.setTotalValue("" + totalValue);
			if (purchaseRequestId > 0){
				purchaseRequest = PurchaseRequestLocalServiceUtil.updatePurchaseRequest(purchaseRequest);
			} else {
				purchaseRequest = PurchaseRequestLocalServiceUtil.addPurchaseRequest(purchaseRequest);
			}
			
		} catch (SystemException e) {
			logger.error("Error in elaboraPurchaseRequst to add purchase request", e);
		}

		try{
			for(Position position : postionList){
				position.setPurchaseRequestId("" + purchaseRequest.getPurchaseRequestId());
				position = PositionLocalServiceUtil.addPosition(position);
			}
			copyWbsFromPRToPositions(purchaseRequest);
		}
		catch(Exception e){
			logger.error("Error in add new position ", e);
		}

		return purchaseRequest;
	}

	private PurchaseRequest creaPurchaseRequest(PurchaseRequestExcelRow prRow, ThemeDisplay themeDisplay, Map<Integer, String> errors) 
			throws UploadNssPrExcelException {
		PurchaseRequest purchaseRequest = null;
		
		String userType = OmpHelper.getUserRoles(themeDisplay.getUserId());
		int gruppoUser = GruppoUsers.GNED.getCode();
		if(userType.equals(OmpRoles.OMP_CONTROLLER_VBS.getLabel())
				|| userType.equals(OmpRoles.OMP_APPROVER_VBS.getLabel())
				|| userType.equals(OmpRoles.OMP_ENG_VBS.getLabel())){
			gruppoUser = GruppoUsers.VBS.getCode();
			
		}
		
		purchaseRequest = purchaseRequestFromRow(prRow, errors, gruppoUser);

		if (purchaseRequest != null){
			User user = themeDisplay.getUser();
			purchaseRequest.setCreatedUserId(user.getUserId());
			purchaseRequest.setScreenNameRequester(user.getScreenName());
			purchaseRequest.setStatus(PurchaseRequestStatus.CREATED.getCode());
			purchaseRequest.setCreatedDate(new Date(System.currentTimeMillis()));
			purchaseRequest.setTipoPr(TipoPr.NOT_NSS.getCode());

			if ((purchaseRequest.getCostCenterId() == null || "".equals(purchaseRequest.getCostCenterId()))	&& ! OmpRoles.OMP_CONTROLLER.getLabel().equals(userType)) {
				CostCenter costCenter = null;
				try {
					CostCenterUser costCenterUser = CostCenterUserLocalServiceUtil.getCostCenterUserByUserId(themeDisplay.getUserId());
					if (costCenterUser != null) {
						costCenter = CostCenterLocalServiceUtil.getCostCenter(costCenterUser.getCostCenterId());
					} else {
						throw new UserHasNoCostCenterException("the logged in user has't cost center");
					}
				} catch (Exception e) {
					logger.error("Error in elaboraPurchaseRequst retrieve the cos center", e);
				}

				if (costCenter != null) {
					purchaseRequest.setCostCenterId(String.valueOf(costCenter.getCostCenterId()));
				}
			}
		}

		return purchaseRequest;

	}
	private PurchaseRequest purchaseRequestFromRow(PurchaseRequestExcelRow prRow, 
			Map<Integer, String> errors,
			int gruppoUser) throws UploadNssPrExcelException {

		PurchaseRequest purchaseRequest = new PurchaseRequestImpl();
		purchaseRequest.setPurchaseRequestId(0);
		purchaseRequest.setCurrency(prRow.getCurrency());
		
		if (prRow.getVendorCode() != null){
			Vendor vendor = null;
			if("VLU07".equals(prRow.getVendorCode().trim())){
				try {
					vendor = VendorLocalServiceUtil.getVendorByName("VPC - VLU07", gruppoUser);
					purchaseRequest.setVendorId("" + vendor.getVendorId());
				} catch (Exception e) {
					throw new UploadNssPrExcelException("Wrong Vendor Code");
				}
			} else{
				String supplierCode = "0000000000" + prRow.getVendorCode().trim();
				supplierCode = supplierCode.substring(supplierCode.length() - 10);
				try {
					vendor = VendorLocalServiceUtil.getVendorBySupplierCode(supplierCode, GruppoUsers.GNED.getCode());
					purchaseRequest.setVendorId("" + vendor.getVendorId());
				} catch (Exception e) {
					throw new UploadNssPrExcelException("Wrong Vendor Code");
				}
			}
		}
		
		if(prRow.getEvoLocationId() != null && prRow.getEvoLocationId().trim().length() > 0){
			if(prRow.getTargaTecnica() == null || "".equals("" + prRow.getTargaTecnica())){
				throw new UploadNssPrExcelException("Wrong EVO Location");
			} else{
				LocationDTO locationTarga;
				try {
					locationTarga = LocationLocalServiceUtil.getLocationByTargaTecnica(prRow.getTargaTecnica());
					if(locationTarga == null){
						throw new UploadNssPrExcelException("Targa tecnica non presente in archivio");
					}
				} catch (SystemException e) {
					logger.error("Error in purchaseRequestFromRow ", e);
				}
			}
		}
		
		if(prRow.getTargaTecnica() != null && prRow.getTargaTecnica().trim().length() > 0){
			if(prRow.getEvoLocationId() == null || "".equals("" + prRow.getEvoLocationId())){
				throw new UploadNssPrExcelException("Wrong Targa Tecnica");
			} else{
				LocationDTO locationTarga;
				try {
					locationTarga = LocationLocalServiceUtil.getLocationByTargaTecnica(prRow.getTargaTecnica());
					if(locationTarga == null){
						throw new UploadNssPrExcelException("Targa tecnica non presente in archivio");
					}
				} catch (SystemException e) {
					logger.error("Error in purchaseRequestFromRow ", e);
				}
			}
		}

		return purchaseRequest;
	}

	private Position positionFromRow(PurchaseRequestExcelRow prRow) {

		Position position = new PositionImpl();
		position.setPositionId(0);

		if (prRow.getDeliveryDate() != null && prRow.getDeliveryDate().trim().length() > 0) {
			position.setDeliveryDate(stringToDate(prRow.getDeliveryDate().replace('.', '/')));
			position.setFiscalYear(OmpHelper.calculateFiscalYear(position.getDeliveryDate()));
		}
		
		position.setDescription(prRow.getItemText());
		position.setDeliveryAddress(prRow.getAddressNumber());
		position.setEvoCodMateriale(prRow.getEvoCodMateriale());
		position.setEvoDesMateriale(prRow.getEvoDesMateriale());
		position.setIcApproval(prRow.getIcApproval());
		position.setCategoryCode(prRow.getCategoryCode());
		position.setOfferNumber(prRow.getOfferNumber());
		
		position.setEvoLocation(prRow.getEvoLocationId());
		position.setTargaTecnica(prRow.getTargaTecnica());

		String quantity = prRow.getPoQuantity();
		if(quantity != null) {
			quantity = quantity.replace(",", ".");
		}
		position.setQuatity(quantity);

		String actualUnitCost = prRow.getNetPrice();
		if(actualUnitCost != null) {
			actualUnitCost = actualUnitCost.replace(",", ".");
		}
		position.setActualUnitCost(actualUnitCost);
		return position;
	}	

	public void uploadFileOfferta(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {

		long purchaseRequestId = ParamUtil.getLong(request, "purchaseRequestId");
		boolean automatic = ParamUtil.getBoolean(request, "automatic");
		boolean multipleUpload;
		try {
			if(automatic) {
				multipleUpload = false;
			}else {
				multipleUpload = true;
			}
			UploadDownloadHelper.uploadFile(purchaseRequestId, multipleUpload,  request);
			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("name","success");
			json.put("id",purchaseRequestId);
			
			response.getWriter().print(json);
		} catch (UploadException ue) {
			logger.error(ue.getMessage());
			response.getWriter().write("failed");

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
	}


}
