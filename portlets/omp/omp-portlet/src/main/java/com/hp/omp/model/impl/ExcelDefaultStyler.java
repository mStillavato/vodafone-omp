package com.hp.omp.model.impl;

import java.awt.Color;
import java.util.Locale;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFPalette;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Workbook;

import com.hp.omp.model.ExcelStyler;

public class ExcelDefaultStyler implements ExcelStyler {
	
	private short EXCEL_HEADER_ROW_INDEX = HSSFColor.BLUE_GREY.index;
	private short EXCEL_ODD_ROW_INDEX = HSSFColor.LIGHT_BLUE.index;
	private static final short EXCEL_EVEN_ROW_INDEX = HSSFColor.WHITE.index;
	private static final short EXCEL_BORDER_INDEX = HSSFColor.GREY_25_PERCENT.index;
	
	private Color headerColor = new Color(0xEB, 0xF2, 0xF6);
	private Color oddRowColor = new Color(0xf9, 0xf9, 0xf9);
	private Color evenRowColor = new Color(0xff, 0xff, 0xff);
	private Color borderColor = new Color(0xdd, 0xdd, 0xdd);
	
	private Workbook workBook = null;
	
	public ExcelDefaultStyler(Workbook workbook){
		this.workBook = workbook;
		configureCustomPalette(workbook);
	}

	public Workbook getWorkBook() {
		return workBook;
	}

	public void setWorkBook(Workbook workbook) {
		this.workBook = workbook;
	}

	public CellStyle getHeaderCellStyle() {
		return getCellStyle(workBook, EXCEL_HEADER_ROW_INDEX, EXCEL_BORDER_INDEX, Font.BOLDWEIGHT_BOLD, (short)12,false);
	}

	public CellStyle getEvenCellStyle() {
		return getCellStyle(workBook, EXCEL_EVEN_ROW_INDEX, EXCEL_BORDER_INDEX, Font.BOLDWEIGHT_NORMAL, (short)12,false);
	}

	public CellStyle getOddCellStyle() {
		return getCellStyle(workBook, EXCEL_ODD_ROW_INDEX, EXCEL_BORDER_INDEX, Font.BOLDWEIGHT_NORMAL, (short)12, false);
	}
	
	public CellStyle getEvenNumericCellStyle() {
		return getCellStyle(workBook, EXCEL_EVEN_ROW_INDEX, EXCEL_BORDER_INDEX, Font.BOLDWEIGHT_NORMAL, (short)12,true);
	}

	public CellStyle getOddNumericCellStyle() {
		return getCellStyle(workBook, EXCEL_ODD_ROW_INDEX, EXCEL_BORDER_INDEX, Font.BOLDWEIGHT_NORMAL, (short)12, true);
	}
	
	private void configureCustomPalette(Workbook workbook) {
		final HSSFPalette customPalette = ((HSSFWorkbook) workbook)
				.getCustomPalette();
		customPalette.setColorAtIndex(EXCEL_HEADER_ROW_INDEX,
				(byte) headerColor.getRed(), (byte) headerColor.getGreen(),
				(byte) headerColor.getBlue());
		customPalette.setColorAtIndex(EXCEL_ODD_ROW_INDEX,
				(byte) oddRowColor.getRed(), (byte) oddRowColor.getGreen(),
				(byte) oddRowColor.getBlue());
		customPalette.setColorAtIndex(EXCEL_EVEN_ROW_INDEX,
				(byte) evenRowColor.getRed(), (byte) evenRowColor.getGreen(),
				(byte) evenRowColor.getBlue());
		customPalette.setColorAtIndex(EXCEL_BORDER_INDEX,
				(byte) borderColor.getRed(), (byte) borderColor.getGreen(),
				(byte) borderColor.getBlue());
		
	}
	
	private CellStyle getCellStyle(final Workbook workbook, short foregroundIndex, short borderIndex, short fontWeight, short fontHeight, boolean number) {
		final CellStyle style = workbook.createCellStyle();
		final Font headerFont = workbook.createFont();
		CreationHelper creationHelper = workbook.getCreationHelper();
		if(number){
			style.setDataFormat(creationHelper.createDataFormat().getFormat("#,##0.00"));
		}
		headerFont.setBoldweight(fontWeight);
		headerFont.setFontHeightInPoints(fontHeight);
		style.setFillForegroundColor(foregroundIndex);
		style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		setBordersColor(style, borderIndex);
		style.setFont(headerFont);
		return style;
	}
	
	private void setBordersColor(final CellStyle style, final short colorIndex) {
		style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		style.setBorderTop(HSSFCellStyle.BORDER_THIN);
		style.setBorderRight(HSSFCellStyle.BORDER_THIN);
		style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		style.setTopBorderColor(colorIndex);
		style.setBottomBorderColor(colorIndex);
		style.setLeftBorderColor(colorIndex);
		style.setRightBorderColor(colorIndex);
	}
	
	

}
