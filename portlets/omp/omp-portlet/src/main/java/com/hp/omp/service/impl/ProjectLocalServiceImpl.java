/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service.impl;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;

import com.hp.omp.model.ExcelReport;
import com.hp.omp.model.ExcelStyler;
import com.hp.omp.model.ExcelTable;
import com.hp.omp.model.Project;
import com.hp.omp.model.TrackingCodes;
import com.hp.omp.model.custom.ExcelTableRow;
import com.hp.omp.model.custom.OMPAggregateReportExcelRow;
import com.hp.omp.model.impl.ExcelDefaultStyler;
import com.hp.omp.model.impl.ExcelReportImpl;
import com.hp.omp.model.impl.ExcelTableImpl;
import com.hp.omp.service.ProjectLocalServiceUtil;
import com.hp.omp.service.WbsCodeLocalServiceUtil;
import com.hp.omp.service.base.ProjectLocalServiceBaseImpl;
import com.hp.omp.service.persistence.ProjectFinderUtil;
import com.hp.omp.service.persistence.ProjectUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

/**
 * The implementation of the project local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.ProjectLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Ahmed Kamel
 * @see com.hp.omp.service.base.ProjectLocalServiceBaseImpl
 * @see com.hp.omp.service.ProjectLocalServiceUtil
 */
public class ProjectLocalServiceImpl extends ProjectLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.hp.omp.service.ProjectLocalServiceUtil} to access the project local service.
	 */
	
	public Project getProjectByName(String name) throws SystemException{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Project.class);
		dynamicQuery.add(RestrictionsFactoryUtil.ilike("projectName",name));
		Project project = null;
		List<Project>  projectList = ProjectUtil.findWithDynamicQuery(dynamicQuery);
		if(projectList != null && ! (projectList.isEmpty()) ){
			project = projectList.get(0);
		}
		
		return project;
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Project> getProjectsList(int start, int end, String searchFilter)
			throws SystemException {
		List<Project> resultQuery = null;
		resultQuery = ProjectFinderUtil.getProjectsList(start, end, searchFilter);
		return resultQuery; 
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getProjectsCountList(String searchFilter) throws SystemException {
		int countList = 0;
		countList = ProjectFinderUtil.getProjectsCountList(searchFilter);
		return countList;
	}
	
	public void exportProjectsAsExcel(
			List<Project> listProjects, List<Object> header,
			OutputStream out) throws SystemException {
		ExcelReport excelReport = new ExcelReportImpl();
		ExcelStyler styler = new ExcelDefaultStyler(excelReport.getWorkBook());
		excelReport.setStyler(styler);

		List<ExcelTableRow> projectsList = new ArrayList<ExcelTableRow>();
		for(Project project : listProjects){
			OMPAggregateReportExcelRow tableRow = new OMPAggregateReportExcelRow();
			tableRow.setFieldAtIndex(project.getProjectName(), 0);
			tableRow.setFieldAtIndex(project.getDescription(), 1);
			tableRow.setFieldAtIndex(project.getDisplay(), 2);

			projectsList.add(tableRow);
		}

		ExcelTable euroTable = new ExcelTableImpl(header, projectsList, "Projects");
		excelReport.addExcelTable(euroTable, 0);

		Workbook exelWorkBook = excelReport.createExelWorkBook();
		try {
			exelWorkBook.write(out);
		} catch (IOException e) {
		} finally{
			try {
				out.flush();
				out.close();
			} catch (IOException e) {
			}
		}
	}
}