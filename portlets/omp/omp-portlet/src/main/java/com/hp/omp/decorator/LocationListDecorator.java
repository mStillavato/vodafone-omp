package com.hp.omp.decorator;

import java.io.Serializable;

import org.displaytag.decorator.TableDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.Location;

public class LocationListDecorator extends TableDecorator implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2685221218697856230L;

	Logger logger = LoggerFactory.getLogger(LocationListDecorator.class);
	
	public String getLocationTargaId() {
		
        return getRemoveAction();
    }
	
	public String getLocationName() {
		
        return getUpdateAction();
    }
	
	public String getRemoveAction() {
		
        StringBuilder sb = new StringBuilder();
        Location item = (Location) this.getCurrentRowObject();
      
        long locationTargaId = item.getLocationTargaId();
        String codiceOffice = item.getCodiceOffice();
        sb.append("<a href=\"javascript:removeLocation("+locationTargaId+",'"+codiceOffice+"')\"");
        sb.append("\">");
        sb.append(" Remove ");
        sb.append("</a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
    }	
	
	public String getUpdateAction() {
		
		Location item = (Location) this.getCurrentRowObject();
		StringBuilder sb = new StringBuilder();
       
      
        long locationTargaId = item.getLocationTargaId();
        String codiceOffice = item.getCodiceOffice();
        sb.append("<a onclick=\"javascript:updateLocation(this, "+locationTargaId+")\" href='"+codiceOffice+"'");
        sb.append("\"> ");
        sb.append(codiceOffice);
        sb.append(" </a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
        
    }

}
