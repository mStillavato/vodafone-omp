package com.hp.omp.service.base;

import com.hp.omp.service.GoodReceiptLocalServiceUtil;

import java.util.Arrays;


public class GoodReceiptLocalServiceClpInvoker {
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName2;
    private String[] _methodParameterTypes2;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;
    private String _methodName9;
    private String[] _methodParameterTypes9;
    private String _methodName10;
    private String[] _methodParameterTypes10;
    private String _methodName11;
    private String[] _methodParameterTypes11;
    private String _methodName12;
    private String[] _methodParameterTypes12;
    private String _methodName13;
    private String[] _methodParameterTypes13;
    private String _methodName14;
    private String[] _methodParameterTypes14;
    private String _methodName15;
    private String[] _methodParameterTypes15;
    private String _methodName146;
    private String[] _methodParameterTypes146;
    private String _methodName147;
    private String[] _methodParameterTypes147;
    private String _methodName152;
    private String[] _methodParameterTypes152;
    private String _methodName153;
    private String[] _methodParameterTypes153;
    private String _methodName154;
    private String[] _methodParameterTypes154;
    private String _methodName155;
    private String[] _methodParameterTypes155;
    private String _methodName156;
    private String[] _methodParameterTypes156;
    private String _methodName157;
    private String[] _methodParameterTypes157;
    private String _methodName158;
    private String[] _methodParameterTypes158;
    private String _methodName164;
    private String[] _methodParameterTypes164;
    private String _methodName166;
    private String[] _methodParameterTypes166;
    private String _methodName167;
    private String[] _methodParameterTypes167;
    private String _methodName168;
    private String[] _methodParameterTypes168;
    private String _methodName169;
    private String[] _methodParameterTypes169;
    private String _methodName170;
    private String[] _methodParameterTypes170;
    private String _methodName171;
    private String[] _methodParameterTypes171;

    public GoodReceiptLocalServiceClpInvoker() {
        _methodName0 = "addGoodReceipt";

        _methodParameterTypes0 = new String[] { "com.hp.omp.model.GoodReceipt" };

        _methodName1 = "createGoodReceipt";

        _methodParameterTypes1 = new String[] { "long" };

        _methodName2 = "deleteGoodReceipt";

        _methodParameterTypes2 = new String[] { "long" };

        _methodName3 = "deleteGoodReceipt";

        _methodParameterTypes3 = new String[] { "com.hp.omp.model.GoodReceipt" };

        _methodName4 = "dynamicQuery";

        _methodParameterTypes4 = new String[] {  };

        _methodName5 = "dynamicQuery";

        _methodParameterTypes5 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName6 = "dynamicQuery";

        _methodParameterTypes6 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
            };

        _methodName7 = "dynamicQuery";

        _methodParameterTypes7 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName8 = "dynamicQueryCount";

        _methodParameterTypes8 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName9 = "fetchGoodReceipt";

        _methodParameterTypes9 = new String[] { "long" };

        _methodName10 = "getGoodReceipt";

        _methodParameterTypes10 = new String[] { "long" };

        _methodName11 = "getPersistedModel";

        _methodParameterTypes11 = new String[] { "java.io.Serializable" };

        _methodName12 = "getGoodReceipts";

        _methodParameterTypes12 = new String[] { "int", "int" };

        _methodName13 = "getGoodReceiptsCount";

        _methodParameterTypes13 = new String[] {  };

        _methodName14 = "updateGoodReceipt";

        _methodParameterTypes14 = new String[] { "com.hp.omp.model.GoodReceipt" };

        _methodName15 = "updateGoodReceipt";

        _methodParameterTypes15 = new String[] {
                "com.hp.omp.model.GoodReceipt", "boolean"
            };

        _methodName146 = "getBeanIdentifier";

        _methodParameterTypes146 = new String[] {  };

        _methodName147 = "setBeanIdentifier";

        _methodParameterTypes147 = new String[] { "java.lang.String" };

        _methodName152 = "getPositionGoodReceipts";

        _methodParameterTypes152 = new String[] { "java.lang.String" };

        _methodName153 = "getPositionMaxRegisterDateGoodReceipts";

        _methodParameterTypes153 = new String[] { "java.lang.String" };

        _methodName154 = "addGoodReceipt";

        _methodParameterTypes154 = new String[] { "com.hp.omp.model.GoodReceipt" };

        _methodName155 = "deleteGoodReceipt";

        _methodParameterTypes155 = new String[] { "com.hp.omp.model.GoodReceipt" };

        _methodName156 = "deleteGoodReceipt";

        _methodParameterTypes156 = new String[] { "long" };

        _methodName157 = "updateGoodReceipt";

        _methodParameterTypes157 = new String[] { "com.hp.omp.model.GoodReceipt" };

        _methodName158 = "updateGoodReceipt";

        _methodParameterTypes158 = new String[] {
                "com.hp.omp.model.GoodReceipt", "boolean"
            };

        _methodName164 = "getSumGRPercentageToPosition";

        _methodParameterTypes164 = new String[] { "java.lang.String" };

        _methodName166 = "getOpenedGoodReceiptByCostCenterCount";

        _methodParameterTypes166 = new String[] { "long" };

        _methodName167 = "getOpenedGoodReceiptCount";

        _methodParameterTypes167 = new String[] {  };

        _methodName168 = "getOpenedGoodReceiptByCostCenter";

        _methodParameterTypes168 = new String[] { "long", "int", "int" };

        _methodName169 = "getOpenedGoodReceipt";

        _methodParameterTypes169 = new String[] { "int", "int" };

        _methodName170 = "getPositionGRCount";

        _methodParameterTypes170 = new String[] { "java.lang.String" };

        _methodName171 = "getPositionClosedGRCount";

        _methodParameterTypes171 = new String[] { "java.lang.String" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName0.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.addGoodReceipt((com.hp.omp.model.GoodReceipt) arguments[0]);
        }

        if (_methodName1.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.createGoodReceipt(((Long) arguments[0]).longValue());
        }

        if (_methodName2.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.deleteGoodReceipt(((Long) arguments[0]).longValue());
        }

        if (_methodName3.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.deleteGoodReceipt((com.hp.omp.model.GoodReceipt) arguments[0]);
        }

        if (_methodName4.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.dynamicQuery();
        }

        if (_methodName5.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName6.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName7.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[3]);
        }

        if (_methodName8.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName9.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.fetchGoodReceipt(((Long) arguments[0]).longValue());
        }

        if (_methodName10.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.getGoodReceipt(((Long) arguments[0]).longValue());
        }

        if (_methodName11.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.getPersistedModel((java.io.Serializable) arguments[0]);
        }

        if (_methodName12.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.getGoodReceipts(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName13.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.getGoodReceiptsCount();
        }

        if (_methodName14.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.updateGoodReceipt((com.hp.omp.model.GoodReceipt) arguments[0]);
        }

        if (_methodName15.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.updateGoodReceipt((com.hp.omp.model.GoodReceipt) arguments[0],
                ((Boolean) arguments[1]).booleanValue());
        }

        if (_methodName146.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes146, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.getBeanIdentifier();
        }

        if (_methodName147.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes147, parameterTypes)) {
            GoodReceiptLocalServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName152.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes152, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.getPositionGoodReceipts((java.lang.String) arguments[0]);
        }

        if (_methodName153.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes153, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.getPositionMaxRegisterDateGoodReceipts((java.lang.String) arguments[0]);
        }

        if (_methodName154.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes154, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.addGoodReceipt((com.hp.omp.model.GoodReceipt) arguments[0]);
        }

        if (_methodName155.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes155, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.deleteGoodReceipt((com.hp.omp.model.GoodReceipt) arguments[0]);
        }

        if (_methodName156.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes156, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.deleteGoodReceipt(((Long) arguments[0]).longValue());
        }

        if (_methodName157.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes157, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.updateGoodReceipt((com.hp.omp.model.GoodReceipt) arguments[0]);
        }

        if (_methodName158.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes158, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.updateGoodReceipt((com.hp.omp.model.GoodReceipt) arguments[0],
                ((Boolean) arguments[1]).booleanValue());
        }

        if (_methodName164.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes164, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.getSumGRPercentageToPosition((java.lang.String) arguments[0]);
        }

        if (_methodName166.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes166, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.getOpenedGoodReceiptByCostCenterCount(((Long) arguments[0]).longValue());
        }

        if (_methodName167.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes167, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.getOpenedGoodReceiptCount();
        }

        if (_methodName168.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes168, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.getOpenedGoodReceiptByCostCenter(((Long) arguments[0]).longValue(),
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName169.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes169, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.getOpenedGoodReceipt(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName170.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes170, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.getPositionGRCount((java.lang.String) arguments[0]);
        }

        if (_methodName171.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes171, parameterTypes)) {
            return GoodReceiptLocalServiceUtil.getPositionClosedGRCount((java.lang.String) arguments[0]);
        }

        throw new UnsupportedOperationException();
    }
}
