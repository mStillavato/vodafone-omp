package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchProjectException;
import com.hp.omp.model.Project;
import com.hp.omp.model.impl.ProjectImpl;
import com.hp.omp.model.impl.ProjectModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the project service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see ProjectPersistence
 * @see ProjectUtil
 * @generated
 */
public class ProjectPersistenceImpl extends BasePersistenceImpl<Project>
    implements ProjectPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link ProjectUtil} to access the project persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = ProjectImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectModelImpl.FINDER_CACHE_ENABLED, ProjectImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectModelImpl.FINDER_CACHE_ENABLED, ProjectImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_PROJECT = "SELECT project FROM Project project";
    private static final String _SQL_COUNT_PROJECT = "SELECT COUNT(project) FROM Project project";
    private static final String _ORDER_BY_ENTITY_ALIAS = "project.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Project exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(ProjectPersistenceImpl.class);
    private static Project _nullProject = new ProjectImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Project> toCacheModel() {
                return _nullProjectCacheModel;
            }
        };

    private static CacheModel<Project> _nullProjectCacheModel = new CacheModel<Project>() {
            public Project toEntityModel() {
                return _nullProject;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the project in the entity cache if it is enabled.
     *
     * @param project the project
     */
    public void cacheResult(Project project) {
        EntityCacheUtil.putResult(ProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectImpl.class, project.getPrimaryKey(), project);

        project.resetOriginalValues();
    }

    /**
     * Caches the projects in the entity cache if it is enabled.
     *
     * @param projects the projects
     */
    public void cacheResult(List<Project> projects) {
        for (Project project : projects) {
            if (EntityCacheUtil.getResult(
                        ProjectModelImpl.ENTITY_CACHE_ENABLED,
                        ProjectImpl.class, project.getPrimaryKey()) == null) {
                cacheResult(project);
            } else {
                project.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all projects.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(ProjectImpl.class.getName());
        }

        EntityCacheUtil.clearCache(ProjectImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the project.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Project project) {
        EntityCacheUtil.removeResult(ProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectImpl.class, project.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Project> projects) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Project project : projects) {
            EntityCacheUtil.removeResult(ProjectModelImpl.ENTITY_CACHE_ENABLED,
                ProjectImpl.class, project.getPrimaryKey());
        }
    }

    /**
     * Creates a new project with the primary key. Does not add the project to the database.
     *
     * @param projectId the primary key for the new project
     * @return the new project
     */
    public Project create(long projectId) {
        Project project = new ProjectImpl();

        project.setNew(true);
        project.setPrimaryKey(projectId);

        return project;
    }

    /**
     * Removes the project with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param projectId the primary key of the project
     * @return the project that was removed
     * @throws com.hp.omp.NoSuchProjectException if a project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public Project remove(long projectId)
        throws NoSuchProjectException, SystemException {
        return remove(Long.valueOf(projectId));
    }

    /**
     * Removes the project with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the project
     * @return the project that was removed
     * @throws com.hp.omp.NoSuchProjectException if a project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Project remove(Serializable primaryKey)
        throws NoSuchProjectException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Project project = (Project) session.get(ProjectImpl.class,
                    primaryKey);

            if (project == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchProjectException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(project);
        } catch (NoSuchProjectException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Project removeImpl(Project project) throws SystemException {
        project = toUnwrappedModel(project);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, project);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(project);

        return project;
    }

    @Override
    public Project updateImpl(com.hp.omp.model.Project project, boolean merge)
        throws SystemException {
        project = toUnwrappedModel(project);

        boolean isNew = project.isNew();

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, project, merge);

            project.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(ProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectImpl.class, project.getPrimaryKey(), project);

        return project;
    }

    protected Project toUnwrappedModel(Project project) {
        if (project instanceof ProjectImpl) {
            return project;
        }

        ProjectImpl projectImpl = new ProjectImpl();

        projectImpl.setNew(project.isNew());
        projectImpl.setPrimaryKey(project.getPrimaryKey());

        projectImpl.setProjectId(project.getProjectId());
        projectImpl.setProjectName(project.getProjectName());
        projectImpl.setDescription(project.getDescription());
        projectImpl.setDisplay(project.isDisplay());

        return projectImpl;
    }

    /**
     * Returns the project with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the project
     * @return the project
     * @throws com.liferay.portal.NoSuchModelException if a project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Project findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the project with the primary key or throws a {@link com.hp.omp.NoSuchProjectException} if it could not be found.
     *
     * @param projectId the primary key of the project
     * @return the project
     * @throws com.hp.omp.NoSuchProjectException if a project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public Project findByPrimaryKey(long projectId)
        throws NoSuchProjectException, SystemException {
        Project project = fetchByPrimaryKey(projectId);

        if (project == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + projectId);
            }

            throw new NoSuchProjectException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                projectId);
        }

        return project;
    }

    /**
     * Returns the project with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the project
     * @return the project, or <code>null</code> if a project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Project fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the project with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param projectId the primary key of the project
     * @return the project, or <code>null</code> if a project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public Project fetchByPrimaryKey(long projectId) throws SystemException {
        Project project = (Project) EntityCacheUtil.getResult(ProjectModelImpl.ENTITY_CACHE_ENABLED,
                ProjectImpl.class, projectId);

        if (project == _nullProject) {
            return null;
        }

        if (project == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                project = (Project) session.get(ProjectImpl.class,
                        Long.valueOf(projectId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (project != null) {
                    cacheResult(project);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(ProjectModelImpl.ENTITY_CACHE_ENABLED,
                        ProjectImpl.class, projectId, _nullProject);
                }

                closeSession(session);
            }
        }

        return project;
    }

    /**
     * Returns all the projects.
     *
     * @return the projects
     * @throws SystemException if a system exception occurred
     */
    public List<Project> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the projects.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of projects
     * @param end the upper bound of the range of projects (not inclusive)
     * @return the range of projects
     * @throws SystemException if a system exception occurred
     */
    public List<Project> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the projects.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of projects
     * @param end the upper bound of the range of projects (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of projects
     * @throws SystemException if a system exception occurred
     */
    public List<Project> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Project> list = (List<Project>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_PROJECT);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_PROJECT;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<Project>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<Project>) QueryUtil.list(q, getDialect(),
                            start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the projects from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (Project project : findAll()) {
            remove(project);
        }
    }

    /**
     * Returns the number of projects.
     *
     * @return the number of projects
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_PROJECT);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the project persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.Project")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Project>> listenersList = new ArrayList<ModelListener<Project>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<Project>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(ProjectImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
