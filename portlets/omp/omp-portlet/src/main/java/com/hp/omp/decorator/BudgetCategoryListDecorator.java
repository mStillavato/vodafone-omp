package com.hp.omp.decorator;

import java.io.Serializable;

import org.displaytag.decorator.TableDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.BudgetCategory;
import com.hp.omp.model.Vendor;

public class BudgetCategoryListDecorator extends TableDecorator implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2685221218697856230L;

	Logger logger = LoggerFactory.getLogger(BudgetCategoryListDecorator.class);
	
	public String getBudgetCategoryId() {
		
        return getRemoveAction();
    }
	
	public String getBudgetCategoryName() {
		
        return getUpdateAction();
    }
	
	public String getRemoveAction() {
		
        StringBuilder sb = new StringBuilder();
        BudgetCategory item = (BudgetCategory) this.getCurrentRowObject();
      
        long budgetCategoryId = item.getBudgetCategoryId();
        String budgetCategoryName = item.getBudgetCategoryName();
        sb.append("<a href=\"javascript:removeBudgetCategory("+budgetCategoryId+",'"+budgetCategoryName+"')\"");
        sb.append("\">");
        sb.append(" Remove ");
        sb.append("</a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
    }	
	
	public String getUpdateAction() {
		
		BudgetCategory item = (BudgetCategory) this.getCurrentRowObject();
		StringBuilder sb = new StringBuilder();
       
      
		long budgetCategoryId = item.getBudgetCategoryId();
        String budgetCategoryName = item.getBudgetCategoryName();
        sb.append("<a onclick=\"javascript:updateBudgetCategory(this, "+budgetCategoryId+")\" href='"+budgetCategoryName+"'");
        sb.append("\"> ");
        sb.append(budgetCategoryName);
        sb.append(" </a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
        
    }

}
