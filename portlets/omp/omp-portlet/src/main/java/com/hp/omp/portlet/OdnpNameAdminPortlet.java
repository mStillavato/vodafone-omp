package com.hp.omp.portlet;

import java.io.IOException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.model.ODNPName;
import com.hp.omp.model.PurchaseOrder;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.impl.ODNPNameImpl;
import com.hp.omp.service.ODNPNameLocalServiceUtil;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class OdnpNameAdminPortlet
 */
public class OdnpNameAdminPortlet extends MVCPortlet {
	private transient Logger logger = LoggerFactory.getLogger(OdnpNameAdminPortlet.class);
	private String errorMSG = "";
	private String successMSG = "";
	private String ACTION_PARAM= "action";
	private String ADD_ACTION= "add";
	private String UPDATE_ACTION= "update";
	private String DELETE_ACTION= "delete";
	private String PAGINATION_ACTION= "pagination";

	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		super.doView(renderRequest, renderResponse);
	}
	
	
	/* (non-Javadoc)
	 * @see javax.portlet.GenericPortlet#render(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		viewAddOdnpName(request, response);
		super.render(request, response);
	}
	
	public void viewAddOdnpName(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		String action = renderRequest.getParameter(ACTION_PARAM);
		
		if(ADD_ACTION.equals(action)){
			renderRequest.setAttribute(ACTION_PARAM, ADD_ACTION);
			renderRequest.setAttribute("display",true);
		}else if(UPDATE_ACTION.equals(action)){
			String odnpNameId = renderRequest.getParameter("odnpNameId");
			ODNPName odnpName = getOdnpName(odnpNameId);
			
			logger.debug("render update action for : "+odnpName.getOdnpNameName());
			logger.debug("Display = "+odnpName.getDisplay());
			
			renderRequest.setAttribute(ACTION_PARAM, UPDATE_ACTION);
			renderRequest.setAttribute("odnpNameId", odnpName.getOdnpNameId());
			renderRequest.setAttribute("odnpNameName", odnpName.getOdnpNameName());
			renderRequest.setAttribute("display", odnpName.getDisplay());
		}
	}

	
	@Override
    public void serveResource(ResourceRequest resourceRequest,
                  ResourceResponse resourceResponse) throws IOException,
                  PortletException {
		String action=resourceRequest.getParameter(ACTION_PARAM);
		if(DELETE_ACTION.equals(action)){
			deleteOdnpName(resourceRequest, resourceResponse);
		}else if(PAGINATION_ACTION.equals(action)){
			paginationList(resourceRequest, resourceResponse);
		}
		
		clearMessages();
           super.serveResource(resourceRequest, resourceResponse);
    }
	
	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#processAction(javax.portlet.ActionRequest, javax.portlet.ActionResponse)
	 */
	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {

		clearMessages();
		String action = actionRequest.getParameter(ACTION_PARAM);
		String odnpNameName = actionRequest.getParameter("odnpNameName");
		boolean display=ParamUtil.getBoolean(actionRequest,"display");
		String operation = "";
		
		logger.debug("odnpNameName="+odnpNameName);
		logger.debug("display = "+display);
		
		logger.debug("creating OdnpName object ...");
		ODNPName newOdnpName = new ODNPNameImpl();
		newOdnpName.setOdnpNameName(odnpNameName);
		newOdnpName.setDisplay(display);
		logger.debug("OdnpName object created !!");
		try {
			if(ADD_ACTION.equals(action)){
				operation = "added";
				logger.debug("adding new OdnpName ["+odnpNameName+"] in database ...");
				if(ODNPNameLocalServiceUtil.getOdnpNameByName(odnpNameName) == null){
					
					ODNPNameLocalServiceUtil.addODNPName(newOdnpName);
					
					successMSG = "OdnpName name ["+odnpNameName+"] successfully Added";
					
				}else{
					errorMSG = "OdnpName name ["+odnpNameName+"] already existing";
				}
			}else if(UPDATE_ACTION.equals(action)){
				operation = "updated";
				String odnpNameId = actionRequest.getParameter("odnpNameId");
				
				newOdnpName.setOdnpNameId(Integer.valueOf(odnpNameId));
				
				if(! (isOdnpNameNameExists(newOdnpName, odnpNameName))){
					
					logger.debug("updating  OdnpName ["+odnpNameId+"] in database ...");
					
					
					ODNPNameLocalServiceUtil.updateODNPName(newOdnpName);
					successMSG = "OdnpName Name ["+odnpNameName+"] successfully Updated";
				}else{
					errorMSG = "OdnpName name ["+odnpNameName+"] already existing";
				}
				
			}
			
			
		} catch (SystemException e) {
			errorMSG = "OdnpName ["+odnpNameName+"] not "+operation+".";
			logger.error("error happened while "+operation+" OdnpName ["+odnpNameName+"] in database.");
			logger.error(e.getMessage(), e);
		}catch (NumberFormatException nfe) {
			errorMSG = "OdnpName ["+odnpNameName+"] not "+operation+".";
			logger.error("error happened while "+operation+" OdnpName ["+odnpNameName+"] in database.");
			logger.error(nfe.getMessage(), nfe);
		}
		
		actionRequest.setAttribute("errorMSG", errorMSG);
		actionRequest.setAttribute("successMSG", successMSG);
		super.processAction(actionRequest, actionResponse);
	}
	
	private void retrieveOdnpNameListToRequest(PortletRequest renderRequest) throws SystemException{
		try {
			String searchFilter = ParamUtil.getString(renderRequest, "searchFilter","");
			int totalCount = ODNPNameLocalServiceUtil.getODNPNamesCount();

			Integer pageNumber = ParamUtil.getInteger(renderRequest, "pageNumber", 1);
			Integer pageSize = OmpHelper.PAGE_SIZE;
			int start = (pageNumber - 1) * pageSize;
			int end = start + pageSize;

			renderRequest.setAttribute("numberOfPages", getTotalNumberOfPages(totalCount, pageSize));
			renderRequest.setAttribute("pageNumber", pageNumber);


			//List<ODNPName> odnpNameList = ODNPNameLocalServiceUtil.getODNPNames(start, end);
			List<ODNPName> odnpNameList = ODNPNameLocalServiceUtil.getODNPNamesList(start, end, searchFilter);

			renderRequest.setAttribute("odnpNameList", odnpNameList);
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			errorMSG = "can not retrieve OdnpName list";
			renderRequest.setAttribute("errorMSG", errorMSG);
		}

	}
	
	public void deleteOdnpName(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse)
			throws PortletException, IOException {
		clearMessages();
		
		String odnpNameId=resourceRequest.getParameter("odnpNameId");
		String odnpNameName=resourceRequest.getParameter("odnpNameName");
		String action=resourceRequest.getParameter("action");
		String jspPage = "/html/odnpnameadmin/odnpnamelist.jsp";
		
		if(Validator.isBlank(action)){
			errorMSG = "No Action in the request";
		}else if(Validator.isBlank(odnpNameId)){
			errorMSG = "No Budget Category Id in the request";
		}else{
			
			try {
				List<PurchaseOrder> purchaseOrderList = PurchaseOrderLocalServiceUtil.getPurchaseOrderByOdnpNameId(odnpNameId);
				List<PurchaseRequest> purchaseRequestList = PurchaseRequestLocalServiceUtil.getPurchaseRequestByOdnpNameId(odnpNameId);
				
				if(purchaseOrderList != null && ! (purchaseOrderList.isEmpty())){
					errorMSG = "Can Not remove OdnpName assigned to Purchase Order, OdnpName name ["+odnpNameName+"]";
				}else if(purchaseRequestList != null && ! (purchaseRequestList.isEmpty())){
					errorMSG = "Can Not remove OdnpName assigned to Purchase Request, OdnpName name ["+odnpNameName+"]";
				}else{
					ODNPNameLocalServiceUtil.deleteODNPName(Long.valueOf(odnpNameId));
					successMSG = "OdnpName name ["+odnpNameName+"] successfully deleted";
				}
				
			} catch (NumberFormatException e) {
				logger.error(e.getMessage());
				errorMSG = "OdnpName Id is invalid";
			} catch (PortalException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove OdnpName";
			} catch (SystemException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove OdnpName [most probably OdnpName is assigned.]";
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove OdnpName";
			}
			
		}
		
		//retrieve updated list
		try {
			retrieveOdnpNameListToRequest(resourceRequest);
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			errorMSG = "can not retrieve OdnpName list";
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
		
	}
	
	private ODNPName getOdnpName(String odnpNameId){
		ODNPName odnpName = null;
		try {
			odnpName = ODNPNameLocalServiceUtil.getODNPName(Integer.valueOf(odnpNameId));
		} catch (NumberFormatException e) {
			logger.error("OdnpName Id has wrong format", e);
		} catch (PortalException e) {
			logger.error("unexpected error", e);
		} catch (SystemException e) {
			logger.error("unexpected error", e);
		}
		
		return odnpName;
	}
	
	private boolean isOdnpNameNameExists(ODNPName newOdnpName, String odnpNameName) throws SystemException{
		
		ODNPName oldOdnpName = ODNPNameLocalServiceUtil.getOdnpNameByName(odnpNameName);
		boolean isExists = false;
		
		if(oldOdnpName != null && 
				(newOdnpName.getOdnpNameId() != oldOdnpName.getOdnpNameId()) ){
			isExists = true;
		}
		return isExists;
	}
	
	private void paginationList(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse) throws PortletException, IOException{
		
		String jspPage = "/html/odnpnameadmin/odnpnamelist.jsp";
		try {
			retrieveOdnpNameListToRequest(resourceRequest);
		} catch (SystemException e) {
		logger.error("error happened in pagination ... ",e);
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
	}
	
	private Integer getTotalNumberOfPages(Integer totalNubmerOfResult, Integer pageSize) {
        return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
	}
	
	private void clearMessages(){
		errorMSG = "";
		successMSG = "";
	}
	
	public void searchFilter(ActionRequest request, ActionResponse response) throws IOException, PortletException{
		response.setRenderParameter("searchFilter", "%" + ParamUtil.getString(request, "odnpNameAdminName") + "%");  
	}
}
