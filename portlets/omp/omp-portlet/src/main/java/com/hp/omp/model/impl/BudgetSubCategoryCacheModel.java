package com.hp.omp.model.impl;

import com.hp.omp.model.BudgetSubCategory;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

/**
 * The cache model class for representing BudgetSubCategory in entity cache.
 *
 * @author HP Egypt team
 * @see BudgetSubCategory
 * @generated
 */
public class BudgetSubCategoryCacheModel implements CacheModel<BudgetSubCategory>,
    Serializable {
    public long budgetSubCategoryId;
    public String budgetSubCategoryName;
    public boolean display;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{budgetSubCategoryId=");
        sb.append(budgetSubCategoryId);
        sb.append(", budgetSubCategoryName=");
        sb.append(budgetSubCategoryName);
        sb.append(", display=");
        sb.append(display);
        sb.append("}");

        return sb.toString();
    }

    public BudgetSubCategory toEntityModel() {
        BudgetSubCategoryImpl budgetSubCategoryImpl = new BudgetSubCategoryImpl();

        budgetSubCategoryImpl.setBudgetSubCategoryId(budgetSubCategoryId);

        if (budgetSubCategoryName == null) {
            budgetSubCategoryImpl.setBudgetSubCategoryName(StringPool.BLANK);
        } else {
            budgetSubCategoryImpl.setBudgetSubCategoryName(budgetSubCategoryName);
        }

        budgetSubCategoryImpl.setDisplay(display);

        budgetSubCategoryImpl.resetOriginalValues();

        return budgetSubCategoryImpl;
    }
}
