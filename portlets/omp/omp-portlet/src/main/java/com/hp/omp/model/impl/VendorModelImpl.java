package com.hp.omp.model.impl;

import com.hp.omp.model.Vendor;
import com.hp.omp.model.VendorModel;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.util.ExpandoBridgeFactoryUtil;

import java.io.Serializable;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

/**
 * The base model implementation for the Vendor service. Represents a row in the &quot;VENDOR&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link com.hp.omp.model.VendorModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link VendorImpl}.
 * </p>
 *
 * @author HP Egypt team
 * @see VendorImpl
 * @see com.hp.omp.model.Vendor
 * @see com.hp.omp.model.VendorModel
 * @generated
 */
public class VendorModelImpl extends BaseModelImpl<Vendor>
    implements VendorModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. All methods that expect a vendor model instance should use the {@link com.hp.omp.model.Vendor} interface instead.
     */
    public static final String TABLE_NAME = "VENDOR";
    public static final Object[][] TABLE_COLUMNS = {
            { "vendorId", Types.BIGINT },
            { "supplier", Types.VARCHAR },
            { "supplierCode", Types.VARCHAR },
            { "vendorName", Types.VARCHAR },
            { "display", Types.BOOLEAN },
            { "gruppoUsers", Types.INTEGER }
        };
    public static final String TABLE_SQL_CREATE = "create table VENDOR (vendorId LONG not null primary key,supplier VARCHAR(75) null,supplierCode VARCHAR(75) null,vendorName VARCHAR(75) null,display BOOLEAN,gruppoUsers INTEGER)";
    public static final String TABLE_SQL_DROP = "drop table VENDOR";
    public static final String DATA_SOURCE = "ompPortletDataSource";
    public static final String SESSION_FACTORY = "ompPortletSessionFactory";
    public static final String TX_MANAGER = "ompPortletTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.entity.cache.enabled.com.hp.omp.model.Vendor"),
            true);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.finder.cache.enabled.com.hp.omp.model.Vendor"),
            true);
    public static final boolean COLUMN_BITMASK_ENABLED = false;
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
                "lock.expiration.time.com.hp.omp.model.Vendor"));
    private static ClassLoader _classLoader = Vendor.class.getClassLoader();
    private static Class<?>[] _escapedModelInterfaces = new Class[] { Vendor.class };
    private long _vendorId;
    private String _supplier;
    private String _supplierCode;
    private String _vendorName;
    private boolean _display;
    private int _gruppoUsers;
    private Vendor _escapedModel;

    public VendorModelImpl() {
    }

    public long getPrimaryKey() {
        return _vendorId;
    }

    public void setPrimaryKey(long primaryKey) {
        setVendorId(primaryKey);
    }

    public Serializable getPrimaryKeyObj() {
        return new Long(_vendorId);
    }

    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    public Class<?> getModelClass() {
        return Vendor.class;
    }

    public String getModelClassName() {
        return Vendor.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("vendorId", getVendorId());
        attributes.put("supplier", getSupplier());
        attributes.put("supplierCode", getSupplierCode());
        attributes.put("vendorName", getVendorName());
        attributes.put("display", getDisplay());
        attributes.put("gruppoUsers", getGruppoUsers());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long vendorId = (Long) attributes.get("vendorId");

        if (vendorId != null) {
            setVendorId(vendorId);
        }

        String supplier = (String) attributes.get("supplier");

        if (supplier != null) {
            setSupplier(supplier);
        }

        String supplierCode = (String) attributes.get("supplierCode");

        if (supplierCode != null) {
            setSupplierCode(supplierCode);
        }

        String vendorName = (String) attributes.get("vendorName");

        if (vendorName != null) {
            setVendorName(vendorName);
        }

        Boolean display = (Boolean) attributes.get("display");

        if (display != null) {
            setDisplay(display);
        }
        
        Integer gruppoUsers = (Integer) attributes.get("gruppoUsers");

        if (gruppoUsers != null) {
            setGruppoUsers(gruppoUsers);
        }
    }

    public long getVendorId() {
        return _vendorId;
    }

    public void setVendorId(long vendorId) {
        _vendorId = vendorId;
    }

    public String getSupplier() {
        if (_supplier == null) {
            return StringPool.BLANK;
        } else {
            return _supplier;
        }
    }

    public void setSupplier(String supplier) {
        _supplier = supplier;
    }

    public String getSupplierCode() {
        if (_supplierCode == null) {
            return StringPool.BLANK;
        } else {
            return _supplierCode;
        }
    }

    public void setSupplierCode(String supplierCode) {
        _supplierCode = supplierCode;
    }

    public String getVendorName() {
        if (_vendorName == null) {
            return StringPool.BLANK;
        } else {
            return _vendorName;
        }
    }

    public void setVendorName(String vendorName) {
        _vendorName = vendorName;
    }

    public boolean getDisplay() {
        return _display;
    }

    public boolean isDisplay() {
        return _display;
    }

    public void setDisplay(boolean display) {
        _display = display;
    }

    @Override
    public ExpandoBridge getExpandoBridge() {
        return ExpandoBridgeFactoryUtil.getExpandoBridge(0,
            Vendor.class.getName(), getPrimaryKey());
    }

    @Override
    public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
        ExpandoBridge expandoBridge = getExpandoBridge();

        expandoBridge.setAttributes(serviceContext);
    }

    @Override
    public Vendor toEscapedModel() {
        if (_escapedModel == null) {
            _escapedModel = (Vendor) ProxyUtil.newProxyInstance(_classLoader,
                    _escapedModelInterfaces, new AutoEscapeBeanHandler(this));
        }

        return _escapedModel;
    }

    public Vendor toUnescapedModel() {
        return (Vendor) this;
    }

    @Override
    public Object clone() {
        VendorImpl vendorImpl = new VendorImpl();

        vendorImpl.setVendorId(getVendorId());
        vendorImpl.setSupplier(getSupplier());
        vendorImpl.setSupplierCode(getSupplierCode());
        vendorImpl.setVendorName(getVendorName());
        vendorImpl.setDisplay(getDisplay());
        vendorImpl.setGruppoUsers(getGruppoUsers());

        vendorImpl.resetOriginalValues();

        return vendorImpl;
    }

    public int compareTo(Vendor vendor) {
        long primaryKey = vendor.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof Vendor)) {
            return false;
        }

        Vendor vendor = (Vendor) obj;

        long primaryKey = vendor.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public void resetOriginalValues() {
    }

    @Override
    public CacheModel<Vendor> toCacheModel() {
        VendorCacheModel vendorCacheModel = new VendorCacheModel();

        vendorCacheModel.vendorId = getVendorId();

        vendorCacheModel.supplier = getSupplier();

        String supplier = vendorCacheModel.supplier;

        if ((supplier != null) && (supplier.length() == 0)) {
            vendorCacheModel.supplier = null;
        }

        vendorCacheModel.supplierCode = getSupplierCode();

        String supplierCode = vendorCacheModel.supplierCode;

        if ((supplierCode != null) && (supplierCode.length() == 0)) {
            vendorCacheModel.supplierCode = null;
        }

        vendorCacheModel.vendorName = getVendorName();

        String vendorName = vendorCacheModel.vendorName;

        if ((vendorName != null) && (vendorName.length() == 0)) {
            vendorCacheModel.vendorName = null;
        }

        vendorCacheModel.display = getDisplay();
        
        vendorCacheModel.gruppoUsers = getGruppoUsers();

        return vendorCacheModel;
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{vendorId=");
        sb.append(getVendorId());
        sb.append(", supplier=");
        sb.append(getSupplier());
        sb.append(", supplierCode=");
        sb.append(getSupplierCode());
        sb.append(", vendorName=");
        sb.append(getVendorName());
        sb.append(", display=");
        sb.append(getDisplay());
        sb.append(", gruppoUsers=");
        sb.append(getGruppoUsers());
        sb.append("}");

        return sb.toString();
    }

    public String toXmlString() {
        StringBundler sb = new StringBundler(19);

        sb.append("<model><model-name>");
        sb.append("com.hp.omp.model.Vendor");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>vendorId</column-name><column-value><![CDATA[");
        sb.append(getVendorId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>supplier</column-name><column-value><![CDATA[");
        sb.append(getSupplier());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>supplierCode</column-name><column-value><![CDATA[");
        sb.append(getSupplierCode());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>vendorName</column-name><column-value><![CDATA[");
        sb.append(getVendorName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>display</column-name><column-value><![CDATA[");
        sb.append(getDisplay());
        sb.append("]]></column-value></column>");
        sb.append(
                "<column><column-name>gruppoUsers</column-name><column-value><![CDATA[");
        sb.append(getGruppoUsers());
        sb.append("]]></column-value></column>");
        sb.append("</model>");

        return sb.toString();
    }

	public int getGruppoUsers() {
		return _gruppoUsers;
	}

	public void setGruppoUsers(int gruppoUsers) {
		_gruppoUsers = gruppoUsers;
	}
}
