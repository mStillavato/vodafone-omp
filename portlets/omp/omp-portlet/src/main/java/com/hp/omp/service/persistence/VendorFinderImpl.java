package com.hp.omp.service.persistence;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.hp.omp.model.Vendor;
import com.hp.omp.model.impl.VendorImpl;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class VendorFinderImpl extends VendorPersistenceImpl implements VendorFinder{

	private static Log _log = LogFactoryUtil.getLog(VendorFinderImpl.class);
	static final String GET_VENDORS_LIST = VendorFinderImpl.class.getName()+".getVendorsList";
	static final String GET_VENDOR_BY_SUPPLIER_CODE = VendorFinderImpl.class.getName()+".getVendorBySupplierCode";
	static final String GET_VENDORS_COUNT_LIST = VendorFinderImpl.class.getName()+".getVendorsCountList";
	
	@SuppressWarnings("unchecked")
	public List<Vendor> getVendorsList(int start, 
			int end, 
			String searchFilter,
			int gruppoUsers) throws SystemException {
		
		Session session = null;
		List<Vendor> list = new ArrayList<Vendor>();
		try {
		session  = openSession();
		
		String sql = null;
		
		if(StringUtils.isEmpty(searchFilter))
			searchFilter = "%%";
		
		sql = CustomSQLUtil.get(GET_VENDORS_LIST);
		SQLQuery query = session.createSQLQuery(sql);
		
		if(query == null){
			_log.error("get vendor list error, no sql found");
			return null;
		}
		
		query.setString(0, searchFilter);
		query.setInteger(1, gruppoUsers);
		
		query.addEntity("VendorList", VendorImpl.class);
		list = (List<Vendor>)QueryUtil.list(query, getDialect(), start, end);
		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Vendor> getVendorBySupplierCode(String supplierCode, int gruppoUsers)
			throws SystemException {
		Session session=null;
		List<Vendor> list = new ArrayList<Vendor>();
		try {
		session  = openSession();
		
		String sql = null;
		
		sql = CustomSQLUtil.get(GET_VENDOR_BY_SUPPLIER_CODE);
		SQLQuery query = session.createSQLQuery(sql);
		
		if(query == null){
			_log.error("get vendor by supplier code error, no sql found");
			return null;
		}
		
		query.setString(0, supplierCode);
		query.setInteger(1, gruppoUsers);
		
		query.addEntity("VendorList", VendorImpl.class);
		list = (List<Vendor>)QueryUtil.list(query, getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
		return list;
	}

	public int getVendorsCountList(String searchFilter, int gruppoUsers) throws SystemException {
		
		Session session = null; 
		try {
			session = openSession();
			
			String sql = null;
			
			if(StringUtils.isEmpty(searchFilter))
				searchFilter = "%%";
			
			sql = CustomSQLUtil.get(GET_VENDORS_COUNT_LIST);
			SQLQuery query = session.createSQLQuery(sql);
			
			if(query == null){
				_log.error("get vendor count list error, no sql found");
				return 0;
			}
			
			query.setString(0, searchFilter);
			query.setInteger(1, gruppoUsers);
			
			query.addScalar("COUNT_VALUE", Type.INTEGER);

			@SuppressWarnings("unchecked")
			List<Integer> list = query.list();
			if(list != null && list.size() > 0) {
				return list.get(0);
			}
			return 0;
		} finally {
			if(session != null) {
				closeSession(session);
			}
		}
	}
}
