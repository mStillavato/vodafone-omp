package com.hp.omp.model.impl;

import com.hp.omp.model.Project;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

/**
 * The cache model class for representing Project in entity cache.
 *
 * @author HP Egypt team
 * @see Project
 * @generated
 */
public class ProjectCacheModel implements CacheModel<Project>, Serializable {
    public long projectId;
    public String projectName;
    public String description;
    public boolean display;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(9);

        sb.append("{projectId=");
        sb.append(projectId);
        sb.append(", projectName=");
        sb.append(projectName);
        sb.append(", description=");
        sb.append(description);
        sb.append(", display=");
        sb.append(display);
        sb.append("}");

        return sb.toString();
    }

    public Project toEntityModel() {
        ProjectImpl projectImpl = new ProjectImpl();

        projectImpl.setProjectId(projectId);

        if (projectName == null) {
            projectImpl.setProjectName(StringPool.BLANK);
        } else {
            projectImpl.setProjectName(projectName);
        }

        if (description == null) {
            projectImpl.setDescription(StringPool.BLANK);
        } else {
            projectImpl.setDescription(description);
        }

        projectImpl.setDisplay(display);

        projectImpl.resetOriginalValues();

        return projectImpl;
    }
}
