package com.hp.omp.model.impl;

import com.hp.omp.model.MacroMicroDriver;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

/**
 * The cache model class for representing MacroMicroDriver in entity cache.
 *
 * @author HP Egypt team
 * @see MacroMicroDriver
 * @generated
 */
public class MacroMicroDriverCacheModel implements CacheModel<MacroMicroDriver>,
    Serializable {
    public long macroMicroDriverId;
    public long macroDriverId;
    public long microDriverId;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{macroMicroDriverId=");
        sb.append(macroMicroDriverId);
        sb.append(", macroDriverId=");
        sb.append(macroDriverId);
        sb.append(", microDriverId=");
        sb.append(microDriverId);
        sb.append("}");

        return sb.toString();
    }

    public MacroMicroDriver toEntityModel() {
        MacroMicroDriverImpl macroMicroDriverImpl = new MacroMicroDriverImpl();

        macroMicroDriverImpl.setMacroMicroDriverId(macroMicroDriverId);
        macroMicroDriverImpl.setMacroDriverId(macroDriverId);
        macroMicroDriverImpl.setMicroDriverId(microDriverId);

        macroMicroDriverImpl.resetOriginalValues();

        return macroMicroDriverImpl;
    }
}
