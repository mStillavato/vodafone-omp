package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchMicroDriverException;
import com.hp.omp.model.MicroDriver;
import com.hp.omp.model.impl.MicroDriverImpl;
import com.hp.omp.model.impl.MicroDriverModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the micro driver service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see MicroDriverPersistence
 * @see MicroDriverUtil
 * @generated
 */
public class MicroDriverPersistenceImpl extends BasePersistenceImpl<MicroDriver>
    implements MicroDriverPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link MicroDriverUtil} to access the micro driver persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = MicroDriverImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(MicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MicroDriverModelImpl.FINDER_CACHE_ENABLED, MicroDriverImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(MicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MicroDriverModelImpl.FINDER_CACHE_ENABLED, MicroDriverImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(MicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MicroDriverModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_MICRODRIVER = "SELECT microDriver FROM MicroDriver microDriver";
    private static final String _SQL_COUNT_MICRODRIVER = "SELECT COUNT(microDriver) FROM MicroDriver microDriver";
    private static final String _ORDER_BY_ENTITY_ALIAS = "microDriver.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No MicroDriver exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(MicroDriverPersistenceImpl.class);
    private static MicroDriver _nullMicroDriver = new MicroDriverImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<MicroDriver> toCacheModel() {
                return _nullMicroDriverCacheModel;
            }
        };

    private static CacheModel<MicroDriver> _nullMicroDriverCacheModel = new CacheModel<MicroDriver>() {
            public MicroDriver toEntityModel() {
                return _nullMicroDriver;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the micro driver in the entity cache if it is enabled.
     *
     * @param microDriver the micro driver
     */
    public void cacheResult(MicroDriver microDriver) {
        EntityCacheUtil.putResult(MicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MicroDriverImpl.class, microDriver.getPrimaryKey(), microDriver);

        microDriver.resetOriginalValues();
    }

    /**
     * Caches the micro drivers in the entity cache if it is enabled.
     *
     * @param microDrivers the micro drivers
     */
    public void cacheResult(List<MicroDriver> microDrivers) {
        for (MicroDriver microDriver : microDrivers) {
            if (EntityCacheUtil.getResult(
                        MicroDriverModelImpl.ENTITY_CACHE_ENABLED,
                        MicroDriverImpl.class, microDriver.getPrimaryKey()) == null) {
                cacheResult(microDriver);
            } else {
                microDriver.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all micro drivers.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(MicroDriverImpl.class.getName());
        }

        EntityCacheUtil.clearCache(MicroDriverImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the micro driver.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(MicroDriver microDriver) {
        EntityCacheUtil.removeResult(MicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MicroDriverImpl.class, microDriver.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<MicroDriver> microDrivers) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (MicroDriver microDriver : microDrivers) {
            EntityCacheUtil.removeResult(MicroDriverModelImpl.ENTITY_CACHE_ENABLED,
                MicroDriverImpl.class, microDriver.getPrimaryKey());
        }
    }

    /**
     * Creates a new micro driver with the primary key. Does not add the micro driver to the database.
     *
     * @param microDriverId the primary key for the new micro driver
     * @return the new micro driver
     */
    public MicroDriver create(long microDriverId) {
        MicroDriver microDriver = new MicroDriverImpl();

        microDriver.setNew(true);
        microDriver.setPrimaryKey(microDriverId);

        return microDriver;
    }

    /**
     * Removes the micro driver with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param microDriverId the primary key of the micro driver
     * @return the micro driver that was removed
     * @throws com.hp.omp.NoSuchMicroDriverException if a micro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public MicroDriver remove(long microDriverId)
        throws NoSuchMicroDriverException, SystemException {
        return remove(Long.valueOf(microDriverId));
    }

    /**
     * Removes the micro driver with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the micro driver
     * @return the micro driver that was removed
     * @throws com.hp.omp.NoSuchMicroDriverException if a micro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MicroDriver remove(Serializable primaryKey)
        throws NoSuchMicroDriverException, SystemException {
        Session session = null;

        try {
            session = openSession();

            MicroDriver microDriver = (MicroDriver) session.get(MicroDriverImpl.class,
                    primaryKey);

            if (microDriver == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchMicroDriverException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(microDriver);
        } catch (NoSuchMicroDriverException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected MicroDriver removeImpl(MicroDriver microDriver)
        throws SystemException {
        microDriver = toUnwrappedModel(microDriver);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, microDriver);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(microDriver);

        return microDriver;
    }

    @Override
    public MicroDriver updateImpl(com.hp.omp.model.MicroDriver microDriver,
        boolean merge) throws SystemException {
        microDriver = toUnwrappedModel(microDriver);

        boolean isNew = microDriver.isNew();

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, microDriver, merge);

            microDriver.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(MicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MicroDriverImpl.class, microDriver.getPrimaryKey(), microDriver);

        return microDriver;
    }

    protected MicroDriver toUnwrappedModel(MicroDriver microDriver) {
        if (microDriver instanceof MicroDriverImpl) {
            return microDriver;
        }

        MicroDriverImpl microDriverImpl = new MicroDriverImpl();

        microDriverImpl.setNew(microDriver.isNew());
        microDriverImpl.setPrimaryKey(microDriver.getPrimaryKey());

        microDriverImpl.setMicroDriverId(microDriver.getMicroDriverId());
        microDriverImpl.setMicroDriverName(microDriver.getMicroDriverName());
        microDriverImpl.setDisplay(microDriver.isDisplay());

        return microDriverImpl;
    }

    /**
     * Returns the micro driver with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the micro driver
     * @return the micro driver
     * @throws com.liferay.portal.NoSuchModelException if a micro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MicroDriver findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the micro driver with the primary key or throws a {@link com.hp.omp.NoSuchMicroDriverException} if it could not be found.
     *
     * @param microDriverId the primary key of the micro driver
     * @return the micro driver
     * @throws com.hp.omp.NoSuchMicroDriverException if a micro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public MicroDriver findByPrimaryKey(long microDriverId)
        throws NoSuchMicroDriverException, SystemException {
        MicroDriver microDriver = fetchByPrimaryKey(microDriverId);

        if (microDriver == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + microDriverId);
            }

            throw new NoSuchMicroDriverException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                microDriverId);
        }

        return microDriver;
    }

    /**
     * Returns the micro driver with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the micro driver
     * @return the micro driver, or <code>null</code> if a micro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MicroDriver fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the micro driver with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param microDriverId the primary key of the micro driver
     * @return the micro driver, or <code>null</code> if a micro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public MicroDriver fetchByPrimaryKey(long microDriverId)
        throws SystemException {
        MicroDriver microDriver = (MicroDriver) EntityCacheUtil.getResult(MicroDriverModelImpl.ENTITY_CACHE_ENABLED,
                MicroDriverImpl.class, microDriverId);

        if (microDriver == _nullMicroDriver) {
            return null;
        }

        if (microDriver == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                microDriver = (MicroDriver) session.get(MicroDriverImpl.class,
                        Long.valueOf(microDriverId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (microDriver != null) {
                    cacheResult(microDriver);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(MicroDriverModelImpl.ENTITY_CACHE_ENABLED,
                        MicroDriverImpl.class, microDriverId, _nullMicroDriver);
                }

                closeSession(session);
            }
        }

        return microDriver;
    }

    /**
     * Returns all the micro drivers.
     *
     * @return the micro drivers
     * @throws SystemException if a system exception occurred
     */
    public List<MicroDriver> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the micro drivers.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of micro drivers
     * @param end the upper bound of the range of micro drivers (not inclusive)
     * @return the range of micro drivers
     * @throws SystemException if a system exception occurred
     */
    public List<MicroDriver> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the micro drivers.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of micro drivers
     * @param end the upper bound of the range of micro drivers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of micro drivers
     * @throws SystemException if a system exception occurred
     */
    public List<MicroDriver> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<MicroDriver> list = (List<MicroDriver>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_MICRODRIVER);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_MICRODRIVER;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<MicroDriver>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<MicroDriver>) QueryUtil.list(q, getDialect(),
                            start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the micro drivers from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (MicroDriver microDriver : findAll()) {
            remove(microDriver);
        }
    }

    /**
     * Returns the number of micro drivers.
     *
     * @return the number of micro drivers
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_MICRODRIVER);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the micro driver persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.MicroDriver")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<MicroDriver>> listenersList = new ArrayList<ModelListener<MicroDriver>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<MicroDriver>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(MicroDriverImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
