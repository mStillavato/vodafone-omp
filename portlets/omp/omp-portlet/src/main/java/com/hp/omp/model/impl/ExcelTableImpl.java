package com.hp.omp.model.impl;

import java.util.List;

import com.hp.omp.model.ExcelTable;
import com.hp.omp.model.custom.ExcelTableRow;

public class ExcelTableImpl implements ExcelTable{
	
	private List<Object> header;
	private List<ExcelTableRow> data;
	private String title;
	private int totalProcessed;
	
	public ExcelTableImpl (List<Object> header, List<ExcelTableRow> data, String title){
		this.header = header;
		this.data = data;
		this.title = title;
		totalProcessed = 0;
	}
	
	public List<Object> getHeader() {
		return header;
	}
	
	public void setHeader(List<Object> header) {
		this.header = header;
	}
	
	public List<ExcelTableRow> getData() {
		return data;
	}
	
	public void setData(List<ExcelTableRow> data) {
		this.data = data;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	public int getProcessedRows() {
		return totalProcessed;
	}

	public void setProcessedRows(int rowCount) {
		this.totalProcessed = rowCount;
	}

}
