package com.hp.omp.decorator;

import java.io.Serializable;

import org.displaytag.decorator.TableDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.ODNPName;

public class OdnpNameListDecorator extends TableDecorator implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2685221218697856230L;

	Logger logger = LoggerFactory.getLogger(OdnpNameListDecorator.class);
	
	public String getOdnpNameId() {
		
        return getRemoveAction();
    }
	
	public String getOdnpNameName() {
		
        return getUpdateAction();
    }
	
	public String getRemoveAction() {
		
        StringBuilder sb = new StringBuilder();
        ODNPName item = (ODNPName) this.getCurrentRowObject();
      
        long odnpNameId = item.getOdnpNameId();
        String odnpNameName = item.getOdnpNameName();
        sb.append("<a href=\"javascript:removeOdnpName("+odnpNameId+",'"+odnpNameName+"')\"");
        sb.append("\">");
        sb.append(" Remove ");
        sb.append("</a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
    }	
	
	public String getUpdateAction() {
		
		ODNPName item = (ODNPName) this.getCurrentRowObject();
		StringBuilder sb = new StringBuilder();
       
      
		long odnpNameId = item.getOdnpNameId();
        String odnpNameName = item.getOdnpNameName();
        sb.append("<a onclick=\"javascript:updateOdnpName(this, "+odnpNameId+")\" href='"+odnpNameName+"'");
        sb.append("\"> ");
        sb.append(odnpNameName);
        sb.append(" </a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
        
    }

}
