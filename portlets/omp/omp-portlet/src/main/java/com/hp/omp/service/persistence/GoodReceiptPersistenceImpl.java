package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchGoodReceiptException;
import com.hp.omp.model.GoodReceipt;
import com.hp.omp.model.impl.GoodReceiptImpl;
import com.hp.omp.model.impl.GoodReceiptModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the good receipt service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see GoodReceiptPersistence
 * @see GoodReceiptUtil
 * @generated
 */
public class GoodReceiptPersistenceImpl extends BasePersistenceImpl<GoodReceipt>
    implements GoodReceiptPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link GoodReceiptUtil} to access the good receipt persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = GoodReceiptImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_POSITIONGOODRECEIPTS =
        new FinderPath(GoodReceiptModelImpl.ENTITY_CACHE_ENABLED,
            GoodReceiptModelImpl.FINDER_CACHE_ENABLED, GoodReceiptImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByPositionGoodReceipts",
            new String[] {
                String.class.getName(),
                
            "java.lang.Integer", "java.lang.Integer",
                "com.liferay.portal.kernel.util.OrderByComparator"
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_POSITIONGOODRECEIPTS =
        new FinderPath(GoodReceiptModelImpl.ENTITY_CACHE_ENABLED,
            GoodReceiptModelImpl.FINDER_CACHE_ENABLED, GoodReceiptImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByPositionGoodReceipts",
            new String[] { String.class.getName() },
            GoodReceiptModelImpl.POSITIONID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_POSITIONGOODRECEIPTS = new FinderPath(GoodReceiptModelImpl.ENTITY_CACHE_ENABLED,
            GoodReceiptModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByPositionGoodReceipts",
            new String[] { String.class.getName() });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(GoodReceiptModelImpl.ENTITY_CACHE_ENABLED,
            GoodReceiptModelImpl.FINDER_CACHE_ENABLED, GoodReceiptImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(GoodReceiptModelImpl.ENTITY_CACHE_ENABLED,
            GoodReceiptModelImpl.FINDER_CACHE_ENABLED, GoodReceiptImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(GoodReceiptModelImpl.ENTITY_CACHE_ENABLED,
            GoodReceiptModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_GOODRECEIPT = "SELECT goodReceipt FROM GoodReceipt goodReceipt";
    private static final String _SQL_SELECT_GOODRECEIPT_WHERE = "SELECT goodReceipt FROM GoodReceipt goodReceipt WHERE ";
    private static final String _SQL_COUNT_GOODRECEIPT = "SELECT COUNT(goodReceipt) FROM GoodReceipt goodReceipt";
    private static final String _SQL_COUNT_GOODRECEIPT_WHERE = "SELECT COUNT(goodReceipt) FROM GoodReceipt goodReceipt WHERE ";
    private static final String _FINDER_COLUMN_POSITIONGOODRECEIPTS_POSITIONID_1 =
        "goodReceipt.positionId IS NULL";
    private static final String _FINDER_COLUMN_POSITIONGOODRECEIPTS_POSITIONID_2 =
        "goodReceipt.positionId = ?";
    private static final String _FINDER_COLUMN_POSITIONGOODRECEIPTS_POSITIONID_3 =
        "(goodReceipt.positionId IS NULL OR goodReceipt.positionId = ?)";
    private static final String _ORDER_BY_ENTITY_ALIAS = "goodReceipt.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No GoodReceipt exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No GoodReceipt exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(GoodReceiptPersistenceImpl.class);
    private static GoodReceipt _nullGoodReceipt = new GoodReceiptImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<GoodReceipt> toCacheModel() {
                return _nullGoodReceiptCacheModel;
            }
        };

    private static CacheModel<GoodReceipt> _nullGoodReceiptCacheModel = new CacheModel<GoodReceipt>() {
            public GoodReceipt toEntityModel() {
                return _nullGoodReceipt;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the good receipt in the entity cache if it is enabled.
     *
     * @param goodReceipt the good receipt
     */
    public void cacheResult(GoodReceipt goodReceipt) {
        EntityCacheUtil.putResult(GoodReceiptModelImpl.ENTITY_CACHE_ENABLED,
            GoodReceiptImpl.class, goodReceipt.getPrimaryKey(), goodReceipt);

        goodReceipt.resetOriginalValues();
    }

    /**
     * Caches the good receipts in the entity cache if it is enabled.
     *
     * @param goodReceipts the good receipts
     */
    public void cacheResult(List<GoodReceipt> goodReceipts) {
        for (GoodReceipt goodReceipt : goodReceipts) {
            if (EntityCacheUtil.getResult(
                        GoodReceiptModelImpl.ENTITY_CACHE_ENABLED,
                        GoodReceiptImpl.class, goodReceipt.getPrimaryKey()) == null) {
                cacheResult(goodReceipt);
            } else {
                goodReceipt.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all good receipts.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(GoodReceiptImpl.class.getName());
        }

        EntityCacheUtil.clearCache(GoodReceiptImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the good receipt.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(GoodReceipt goodReceipt) {
        EntityCacheUtil.removeResult(GoodReceiptModelImpl.ENTITY_CACHE_ENABLED,
            GoodReceiptImpl.class, goodReceipt.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<GoodReceipt> goodReceipts) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (GoodReceipt goodReceipt : goodReceipts) {
            EntityCacheUtil.removeResult(GoodReceiptModelImpl.ENTITY_CACHE_ENABLED,
                GoodReceiptImpl.class, goodReceipt.getPrimaryKey());
        }
    }

    /**
     * Creates a new good receipt with the primary key. Does not add the good receipt to the database.
     *
     * @param goodReceiptId the primary key for the new good receipt
     * @return the new good receipt
     */
    public GoodReceipt create(long goodReceiptId) {
        GoodReceipt goodReceipt = new GoodReceiptImpl();

        goodReceipt.setNew(true);
        goodReceipt.setPrimaryKey(goodReceiptId);

        return goodReceipt;
    }

    /**
     * Removes the good receipt with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param goodReceiptId the primary key of the good receipt
     * @return the good receipt that was removed
     * @throws com.hp.omp.NoSuchGoodReceiptException if a good receipt with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public GoodReceipt remove(long goodReceiptId)
        throws NoSuchGoodReceiptException, SystemException {
        return remove(Long.valueOf(goodReceiptId));
    }

    /**
     * Removes the good receipt with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the good receipt
     * @return the good receipt that was removed
     * @throws com.hp.omp.NoSuchGoodReceiptException if a good receipt with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public GoodReceipt remove(Serializable primaryKey)
        throws NoSuchGoodReceiptException, SystemException {
        Session session = null;

        try {
            session = openSession();

            GoodReceipt goodReceipt = (GoodReceipt) session.get(GoodReceiptImpl.class,
                    primaryKey);

            if (goodReceipt == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchGoodReceiptException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(goodReceipt);
        } catch (NoSuchGoodReceiptException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected GoodReceipt removeImpl(GoodReceipt goodReceipt)
        throws SystemException {
        goodReceipt = toUnwrappedModel(goodReceipt);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, goodReceipt);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(goodReceipt);

        return goodReceipt;
    }

    @Override
    public GoodReceipt updateImpl(com.hp.omp.model.GoodReceipt goodReceipt,
        boolean merge) throws SystemException {
        goodReceipt = toUnwrappedModel(goodReceipt);

        boolean isNew = goodReceipt.isNew();

        GoodReceiptModelImpl goodReceiptModelImpl = (GoodReceiptModelImpl) goodReceipt;

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, goodReceipt, merge);

            goodReceipt.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !GoodReceiptModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((goodReceiptModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_POSITIONGOODRECEIPTS.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        goodReceiptModelImpl.getOriginalPositionId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_POSITIONGOODRECEIPTS,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_POSITIONGOODRECEIPTS,
                    args);

                args = new Object[] { goodReceiptModelImpl.getPositionId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_POSITIONGOODRECEIPTS,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_POSITIONGOODRECEIPTS,
                    args);
            }
        }

        EntityCacheUtil.putResult(GoodReceiptModelImpl.ENTITY_CACHE_ENABLED,
            GoodReceiptImpl.class, goodReceipt.getPrimaryKey(), goodReceipt);

        return goodReceipt;
    }

    protected GoodReceipt toUnwrappedModel(GoodReceipt goodReceipt) {
        if (goodReceipt instanceof GoodReceiptImpl) {
            return goodReceipt;
        }

        GoodReceiptImpl goodReceiptImpl = new GoodReceiptImpl();

        goodReceiptImpl.setNew(goodReceipt.isNew());
        goodReceiptImpl.setPrimaryKey(goodReceipt.getPrimaryKey());

        goodReceiptImpl.setGoodReceiptId(goodReceipt.getGoodReceiptId());
        goodReceiptImpl.setGoodReceiptNumber(goodReceipt.getGoodReceiptNumber());
        goodReceiptImpl.setRequestDate(goodReceipt.getRequestDate());
        goodReceiptImpl.setPercentage(goodReceipt.getPercentage());
        goodReceiptImpl.setGrValue(goodReceipt.getGrValue());
        goodReceiptImpl.setRegisterationDate(goodReceipt.getRegisterationDate());
        goodReceiptImpl.setCreatedUserId(goodReceipt.getCreatedUserId());
        goodReceiptImpl.setPositionId(goodReceipt.getPositionId());

        return goodReceiptImpl;
    }

    /**
     * Returns the good receipt with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the good receipt
     * @return the good receipt
     * @throws com.liferay.portal.NoSuchModelException if a good receipt with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public GoodReceipt findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the good receipt with the primary key or throws a {@link com.hp.omp.NoSuchGoodReceiptException} if it could not be found.
     *
     * @param goodReceiptId the primary key of the good receipt
     * @return the good receipt
     * @throws com.hp.omp.NoSuchGoodReceiptException if a good receipt with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public GoodReceipt findByPrimaryKey(long goodReceiptId)
        throws NoSuchGoodReceiptException, SystemException {
        GoodReceipt goodReceipt = fetchByPrimaryKey(goodReceiptId);

        if (goodReceipt == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + goodReceiptId);
            }

            throw new NoSuchGoodReceiptException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                goodReceiptId);
        }

        return goodReceipt;
    }

    /**
     * Returns the good receipt with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the good receipt
     * @return the good receipt, or <code>null</code> if a good receipt with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public GoodReceipt fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the good receipt with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param goodReceiptId the primary key of the good receipt
     * @return the good receipt, or <code>null</code> if a good receipt with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public GoodReceipt fetchByPrimaryKey(long goodReceiptId)
        throws SystemException {
        GoodReceipt goodReceipt = (GoodReceipt) EntityCacheUtil.getResult(GoodReceiptModelImpl.ENTITY_CACHE_ENABLED,
                GoodReceiptImpl.class, goodReceiptId);

        if (goodReceipt == _nullGoodReceipt) {
            return null;
        }

        if (goodReceipt == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                goodReceipt = (GoodReceipt) session.get(GoodReceiptImpl.class,
                        Long.valueOf(goodReceiptId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (goodReceipt != null) {
                    cacheResult(goodReceipt);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(GoodReceiptModelImpl.ENTITY_CACHE_ENABLED,
                        GoodReceiptImpl.class, goodReceiptId, _nullGoodReceipt);
                }

                closeSession(session);
            }
        }

        return goodReceipt;
    }

    /**
     * Returns all the good receipts where positionId = &#63;.
     *
     * @param positionId the position ID
     * @return the matching good receipts
     * @throws SystemException if a system exception occurred
     */
    public List<GoodReceipt> findByPositionGoodReceipts(String positionId)
        throws SystemException {
        return findByPositionGoodReceipts(positionId, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the good receipts where positionId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param positionId the position ID
     * @param start the lower bound of the range of good receipts
     * @param end the upper bound of the range of good receipts (not inclusive)
     * @return the range of matching good receipts
     * @throws SystemException if a system exception occurred
     */
    public List<GoodReceipt> findByPositionGoodReceipts(String positionId,
        int start, int end) throws SystemException {
        return findByPositionGoodReceipts(positionId, start, end, null);
    }

    /**
     * Returns an ordered range of all the good receipts where positionId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param positionId the position ID
     * @param start the lower bound of the range of good receipts
     * @param end the upper bound of the range of good receipts (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching good receipts
     * @throws SystemException if a system exception occurred
     */
    public List<GoodReceipt> findByPositionGoodReceipts(String positionId,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_POSITIONGOODRECEIPTS;
            finderArgs = new Object[] { positionId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_POSITIONGOODRECEIPTS;
            finderArgs = new Object[] { positionId, start, end, orderByComparator };
        }

        List<GoodReceipt> list = (List<GoodReceipt>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (GoodReceipt goodReceipt : list) {
                if (!Validator.equals(positionId, goodReceipt.getPositionId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(2);
            }

            query.append(_SQL_SELECT_GOODRECEIPT_WHERE);

            if (positionId == null) {
                query.append(_FINDER_COLUMN_POSITIONGOODRECEIPTS_POSITIONID_1);
            } else {
                if (positionId.equals(StringPool.BLANK)) {
                    query.append(_FINDER_COLUMN_POSITIONGOODRECEIPTS_POSITIONID_3);
                } else {
                    query.append(_FINDER_COLUMN_POSITIONGOODRECEIPTS_POSITIONID_2);
                }
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (positionId != null) {
                    qPos.add(positionId);
                }

                list = (List<GoodReceipt>) QueryUtil.list(q, getDialect(),
                        start, end);
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first good receipt in the ordered set where positionId = &#63;.
     *
     * @param positionId the position ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching good receipt
     * @throws com.hp.omp.NoSuchGoodReceiptException if a matching good receipt could not be found
     * @throws SystemException if a system exception occurred
     */
    public GoodReceipt findByPositionGoodReceipts_First(String positionId,
        OrderByComparator orderByComparator)
        throws NoSuchGoodReceiptException, SystemException {
        GoodReceipt goodReceipt = fetchByPositionGoodReceipts_First(positionId,
                orderByComparator);

        if (goodReceipt != null) {
            return goodReceipt;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("positionId=");
        msg.append(positionId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchGoodReceiptException(msg.toString());
    }

    /**
     * Returns the first good receipt in the ordered set where positionId = &#63;.
     *
     * @param positionId the position ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching good receipt, or <code>null</code> if a matching good receipt could not be found
     * @throws SystemException if a system exception occurred
     */
    public GoodReceipt fetchByPositionGoodReceipts_First(String positionId,
        OrderByComparator orderByComparator) throws SystemException {
        List<GoodReceipt> list = findByPositionGoodReceipts(positionId, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last good receipt in the ordered set where positionId = &#63;.
     *
     * @param positionId the position ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching good receipt
     * @throws com.hp.omp.NoSuchGoodReceiptException if a matching good receipt could not be found
     * @throws SystemException if a system exception occurred
     */
    public GoodReceipt findByPositionGoodReceipts_Last(String positionId,
        OrderByComparator orderByComparator)
        throws NoSuchGoodReceiptException, SystemException {
        GoodReceipt goodReceipt = fetchByPositionGoodReceipts_Last(positionId,
                orderByComparator);

        if (goodReceipt != null) {
            return goodReceipt;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("positionId=");
        msg.append(positionId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchGoodReceiptException(msg.toString());
    }

    /**
     * Returns the last good receipt in the ordered set where positionId = &#63;.
     *
     * @param positionId the position ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching good receipt, or <code>null</code> if a matching good receipt could not be found
     * @throws SystemException if a system exception occurred
     */
    public GoodReceipt fetchByPositionGoodReceipts_Last(String positionId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByPositionGoodReceipts(positionId);

        List<GoodReceipt> list = findByPositionGoodReceipts(positionId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the good receipts before and after the current good receipt in the ordered set where positionId = &#63;.
     *
     * @param goodReceiptId the primary key of the current good receipt
     * @param positionId the position ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next good receipt
     * @throws com.hp.omp.NoSuchGoodReceiptException if a good receipt with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public GoodReceipt[] findByPositionGoodReceipts_PrevAndNext(
        long goodReceiptId, String positionId,
        OrderByComparator orderByComparator)
        throws NoSuchGoodReceiptException, SystemException {
        GoodReceipt goodReceipt = findByPrimaryKey(goodReceiptId);

        Session session = null;

        try {
            session = openSession();

            GoodReceipt[] array = new GoodReceiptImpl[3];

            array[0] = getByPositionGoodReceipts_PrevAndNext(session,
                    goodReceipt, positionId, orderByComparator, true);

            array[1] = goodReceipt;

            array[2] = getByPositionGoodReceipts_PrevAndNext(session,
                    goodReceipt, positionId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected GoodReceipt getByPositionGoodReceipts_PrevAndNext(
        Session session, GoodReceipt goodReceipt, String positionId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_GOODRECEIPT_WHERE);

        if (positionId == null) {
            query.append(_FINDER_COLUMN_POSITIONGOODRECEIPTS_POSITIONID_1);
        } else {
            if (positionId.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_POSITIONGOODRECEIPTS_POSITIONID_3);
            } else {
                query.append(_FINDER_COLUMN_POSITIONGOODRECEIPTS_POSITIONID_2);
            }
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (positionId != null) {
            qPos.add(positionId);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(goodReceipt);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<GoodReceipt> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Returns all the good receipts.
     *
     * @return the good receipts
     * @throws SystemException if a system exception occurred
     */
    public List<GoodReceipt> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the good receipts.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of good receipts
     * @param end the upper bound of the range of good receipts (not inclusive)
     * @return the range of good receipts
     * @throws SystemException if a system exception occurred
     */
    public List<GoodReceipt> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the good receipts.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of good receipts
     * @param end the upper bound of the range of good receipts (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of good receipts
     * @throws SystemException if a system exception occurred
     */
    public List<GoodReceipt> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<GoodReceipt> list = (List<GoodReceipt>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_GOODRECEIPT);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_GOODRECEIPT;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<GoodReceipt>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<GoodReceipt>) QueryUtil.list(q, getDialect(),
                            start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the good receipts where positionId = &#63; from the database.
     *
     * @param positionId the position ID
     * @throws SystemException if a system exception occurred
     */
    public void removeByPositionGoodReceipts(String positionId)
        throws SystemException {
        for (GoodReceipt goodReceipt : findByPositionGoodReceipts(positionId)) {
            remove(goodReceipt);
        }
    }

    /**
     * Removes all the good receipts from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (GoodReceipt goodReceipt : findAll()) {
            remove(goodReceipt);
        }
    }

    /**
     * Returns the number of good receipts where positionId = &#63;.
     *
     * @param positionId the position ID
     * @return the number of matching good receipts
     * @throws SystemException if a system exception occurred
     */
    public int countByPositionGoodReceipts(String positionId)
        throws SystemException {
        Object[] finderArgs = new Object[] { positionId };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_POSITIONGOODRECEIPTS,
                finderArgs, this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_GOODRECEIPT_WHERE);

            if (positionId == null) {
                query.append(_FINDER_COLUMN_POSITIONGOODRECEIPTS_POSITIONID_1);
            } else {
                if (positionId.equals(StringPool.BLANK)) {
                    query.append(_FINDER_COLUMN_POSITIONGOODRECEIPTS_POSITIONID_3);
                } else {
                    query.append(_FINDER_COLUMN_POSITIONGOODRECEIPTS_POSITIONID_2);
                }
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (positionId != null) {
                    qPos.add(positionId);
                }

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_POSITIONGOODRECEIPTS,
                    finderArgs, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the number of good receipts.
     *
     * @return the number of good receipts
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_GOODRECEIPT);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the good receipt persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.GoodReceipt")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<GoodReceipt>> listenersList = new ArrayList<ModelListener<GoodReceipt>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<GoodReceipt>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(GoodReceiptImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
