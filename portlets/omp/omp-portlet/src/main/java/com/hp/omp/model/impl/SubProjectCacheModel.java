package com.hp.omp.model.impl;

import com.hp.omp.model.SubProject;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

/**
 * The cache model class for representing SubProject in entity cache.
 *
 * @author HP Egypt team
 * @see SubProject
 * @generated
 */
public class SubProjectCacheModel implements CacheModel<SubProject>,
    Serializable {
    public long subProjectId;
    public String subProjectName;
    public String description;
    public boolean display;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(9);

        sb.append("{subProjectId=");
        sb.append(subProjectId);
        sb.append(", subProjectName=");
        sb.append(subProjectName);
        sb.append(", description=");
        sb.append(description);
        sb.append(", display=");
        sb.append(display);
        sb.append("}");

        return sb.toString();
    }

    public SubProject toEntityModel() {
        SubProjectImpl subProjectImpl = new SubProjectImpl();

        subProjectImpl.setSubProjectId(subProjectId);

        if (subProjectName == null) {
            subProjectImpl.setSubProjectName(StringPool.BLANK);
        } else {
            subProjectImpl.setSubProjectName(subProjectName);
        }

        if (description == null) {
            subProjectImpl.setDescription(StringPool.BLANK);
        } else {
            subProjectImpl.setDescription(description);
        }

        subProjectImpl.setDisplay(display);

        subProjectImpl.resetOriginalValues();

        return subProjectImpl;
    }
}
