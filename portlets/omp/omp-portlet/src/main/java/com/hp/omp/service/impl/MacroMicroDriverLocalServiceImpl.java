package com.hp.omp.service.impl;

import java.util.List;

import com.hp.omp.model.MacroDriver;
import com.hp.omp.model.MacroMicroDriver;
import com.hp.omp.service.base.MacroMicroDriverLocalServiceBaseImpl;
import com.hp.omp.service.persistence.MacroMicroDriverFinderUtil;
import com.hp.omp.service.persistence.MacroMicroDriverUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the macro micro driver local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.MacroMicroDriverLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Ahmed Kamel
 * @see com.hp.omp.service.base.MacroMicroDriverLocalServiceBaseImpl
 * @see com.hp.omp.service.MacroMicroDriverLocalServiceUtil
 */
public class MacroMicroDriverLocalServiceImpl
    extends MacroMicroDriverLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.hp.omp.service.MacroMicroDriverLocalServiceUtil} to access the macro micro driver local service.
     */
	
	public List<MacroMicroDriver> getMacroMicroDriverByMacroDriverId(Long macroDriverId) throws SystemException{
		return MacroMicroDriverUtil.findByMacroDriverId(macroDriverId);
	}
	
	public List<MacroMicroDriver> getMacroMicroDriverByMicroDriverId(Long microDriverId) throws SystemException{
		return MacroMicroDriverUtil.findByMicroDriverId(microDriverId);
	}
	
	public List<MacroMicroDriver> getMacroMicroDriverByMacroDriverIdAndMicroDriverId(Long macroDriverId, Long microDriverId) throws SystemException{
		return MacroMicroDriverUtil.findByMacroDriverIdAndMicroDriverId(macroDriverId, microDriverId);
	}
	
	public List<MacroDriver> getMacroDriversByMicroDriverId(Long microDriverId) throws SystemException{
		return MacroMicroDriverFinderUtil.getMacroDriversByMicroDriverId(microDriverId);
	}
}
