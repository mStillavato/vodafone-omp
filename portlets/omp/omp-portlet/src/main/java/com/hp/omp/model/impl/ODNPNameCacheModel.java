package com.hp.omp.model.impl;

import com.hp.omp.model.ODNPName;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

/**
 * The cache model class for representing ODNPName in entity cache.
 *
 * @author HP Egypt team
 * @see ODNPName
 * @generated
 */
public class ODNPNameCacheModel implements CacheModel<ODNPName>, Serializable {
    public long odnpNameId;
    public String odnpNameName;
    public boolean display;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{odnpNameId=");
        sb.append(odnpNameId);
        sb.append(", odnpNameName=");
        sb.append(odnpNameName);
        sb.append(", display=");
        sb.append(display);
        sb.append("}");

        return sb.toString();
    }

    public ODNPName toEntityModel() {
        ODNPNameImpl odnpNameImpl = new ODNPNameImpl();

        odnpNameImpl.setOdnpNameId(odnpNameId);

        if (odnpNameName == null) {
            odnpNameImpl.setOdnpNameName(StringPool.BLANK);
        } else {
            odnpNameImpl.setOdnpNameName(odnpNameName);
        }

        odnpNameImpl.setDisplay(display);

        odnpNameImpl.resetOriginalValues();

        return odnpNameImpl;
    }
}
