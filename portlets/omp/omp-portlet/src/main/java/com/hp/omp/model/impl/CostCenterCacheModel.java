package com.hp.omp.model.impl;

import com.hp.omp.model.CostCenter;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

/**
 * The cache model class for representing CostCenter in entity cache.
 *
 * @author HP Egypt team
 * @see CostCenter
 * @generated
 */
public class CostCenterCacheModel implements CacheModel<CostCenter>,
    Serializable {
    public long costCenterId;
    public String costCenterName;
    public String description;
    public boolean display;
    public int gruppoUsers;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(9);

        sb.append("{costCenterId=");
        sb.append(costCenterId);
        sb.append(", costCenterName=");
        sb.append(costCenterName);
        sb.append(", description=");
        sb.append(description);
        sb.append(", display=");
        sb.append(display);
        sb.append(", gruppoUsers=");
        sb.append(gruppoUsers);
        sb.append("}");

        return sb.toString();
    }

    public CostCenter toEntityModel() {
        CostCenterImpl costCenterImpl = new CostCenterImpl();

        costCenterImpl.setCostCenterId(costCenterId);

        if (costCenterName == null) {
            costCenterImpl.setCostCenterName(StringPool.BLANK);
        } else {
            costCenterImpl.setCostCenterName(costCenterName);
        }

        if (description == null) {
            costCenterImpl.setDescription(StringPool.BLANK);
        } else {
            costCenterImpl.setDescription(description);
        }

        costCenterImpl.setDisplay(display);
        
        costCenterImpl.setGruppoUsers(gruppoUsers);

        costCenterImpl.resetOriginalValues();

        return costCenterImpl;
    }
}
