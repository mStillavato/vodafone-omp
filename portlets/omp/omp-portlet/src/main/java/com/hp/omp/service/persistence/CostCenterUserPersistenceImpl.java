package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchCostCenterUserException;
import com.hp.omp.model.CostCenterUser;
import com.hp.omp.model.impl.CostCenterUserImpl;
import com.hp.omp.model.impl.CostCenterUserModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the cost center user service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see CostCenterUserPersistence
 * @see CostCenterUserUtil
 * @generated
 */
public class CostCenterUserPersistenceImpl extends BasePersistenceImpl<CostCenterUser>
    implements CostCenterUserPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link CostCenterUserUtil} to access the cost center user persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = CostCenterUserImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CostCenterUserModelImpl.ENTITY_CACHE_ENABLED,
            CostCenterUserModelImpl.FINDER_CACHE_ENABLED,
            CostCenterUserImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CostCenterUserModelImpl.ENTITY_CACHE_ENABLED,
            CostCenterUserModelImpl.FINDER_CACHE_ENABLED,
            CostCenterUserImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CostCenterUserModelImpl.ENTITY_CACHE_ENABLED,
            CostCenterUserModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_COSTCENTERUSER = "SELECT costCenterUser FROM CostCenterUser costCenterUser";
    private static final String _SQL_COUNT_COSTCENTERUSER = "SELECT COUNT(costCenterUser) FROM CostCenterUser costCenterUser";
    private static final String _ORDER_BY_ENTITY_ALIAS = "costCenterUser.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CostCenterUser exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(CostCenterUserPersistenceImpl.class);
    private static CostCenterUser _nullCostCenterUser = new CostCenterUserImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<CostCenterUser> toCacheModel() {
                return _nullCostCenterUserCacheModel;
            }
        };

    private static CacheModel<CostCenterUser> _nullCostCenterUserCacheModel = new CacheModel<CostCenterUser>() {
            public CostCenterUser toEntityModel() {
                return _nullCostCenterUser;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the cost center user in the entity cache if it is enabled.
     *
     * @param costCenterUser the cost center user
     */
    public void cacheResult(CostCenterUser costCenterUser) {
        EntityCacheUtil.putResult(CostCenterUserModelImpl.ENTITY_CACHE_ENABLED,
            CostCenterUserImpl.class, costCenterUser.getPrimaryKey(),
            costCenterUser);

        costCenterUser.resetOriginalValues();
    }

    /**
     * Caches the cost center users in the entity cache if it is enabled.
     *
     * @param costCenterUsers the cost center users
     */
    public void cacheResult(List<CostCenterUser> costCenterUsers) {
        for (CostCenterUser costCenterUser : costCenterUsers) {
            if (EntityCacheUtil.getResult(
                        CostCenterUserModelImpl.ENTITY_CACHE_ENABLED,
                        CostCenterUserImpl.class, costCenterUser.getPrimaryKey()) == null) {
                cacheResult(costCenterUser);
            } else {
                costCenterUser.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all cost center users.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(CostCenterUserImpl.class.getName());
        }

        EntityCacheUtil.clearCache(CostCenterUserImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the cost center user.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(CostCenterUser costCenterUser) {
        EntityCacheUtil.removeResult(CostCenterUserModelImpl.ENTITY_CACHE_ENABLED,
            CostCenterUserImpl.class, costCenterUser.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<CostCenterUser> costCenterUsers) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (CostCenterUser costCenterUser : costCenterUsers) {
            EntityCacheUtil.removeResult(CostCenterUserModelImpl.ENTITY_CACHE_ENABLED,
                CostCenterUserImpl.class, costCenterUser.getPrimaryKey());
        }
    }

    /**
     * Creates a new cost center user with the primary key. Does not add the cost center user to the database.
     *
     * @param costCenterUserId the primary key for the new cost center user
     * @return the new cost center user
     */
    public CostCenterUser create(long costCenterUserId) {
        CostCenterUser costCenterUser = new CostCenterUserImpl();

        costCenterUser.setNew(true);
        costCenterUser.setPrimaryKey(costCenterUserId);

        return costCenterUser;
    }

    /**
     * Removes the cost center user with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param costCenterUserId the primary key of the cost center user
     * @return the cost center user that was removed
     * @throws com.hp.omp.NoSuchCostCenterUserException if a cost center user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public CostCenterUser remove(long costCenterUserId)
        throws NoSuchCostCenterUserException, SystemException {
        return remove(Long.valueOf(costCenterUserId));
    }

    /**
     * Removes the cost center user with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the cost center user
     * @return the cost center user that was removed
     * @throws com.hp.omp.NoSuchCostCenterUserException if a cost center user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CostCenterUser remove(Serializable primaryKey)
        throws NoSuchCostCenterUserException, SystemException {
        Session session = null;

        try {
            session = openSession();

            CostCenterUser costCenterUser = (CostCenterUser) session.get(CostCenterUserImpl.class,
                    primaryKey);

            if (costCenterUser == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchCostCenterUserException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(costCenterUser);
        } catch (NoSuchCostCenterUserException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected CostCenterUser removeImpl(CostCenterUser costCenterUser)
        throws SystemException {
        costCenterUser = toUnwrappedModel(costCenterUser);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, costCenterUser);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(costCenterUser);

        return costCenterUser;
    }

    @Override
    public CostCenterUser updateImpl(
        com.hp.omp.model.CostCenterUser costCenterUser, boolean merge)
        throws SystemException {
        costCenterUser = toUnwrappedModel(costCenterUser);

        boolean isNew = costCenterUser.isNew();

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, costCenterUser, merge);

            costCenterUser.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(CostCenterUserModelImpl.ENTITY_CACHE_ENABLED,
            CostCenterUserImpl.class, costCenterUser.getPrimaryKey(),
            costCenterUser);

        return costCenterUser;
    }

    protected CostCenterUser toUnwrappedModel(CostCenterUser costCenterUser) {
        if (costCenterUser instanceof CostCenterUserImpl) {
            return costCenterUser;
        }

        CostCenterUserImpl costCenterUserImpl = new CostCenterUserImpl();

        costCenterUserImpl.setNew(costCenterUser.isNew());
        costCenterUserImpl.setPrimaryKey(costCenterUser.getPrimaryKey());

        costCenterUserImpl.setCostCenterUserId(costCenterUser.getCostCenterUserId());
        costCenterUserImpl.setCostCenterId(costCenterUser.getCostCenterId());
        costCenterUserImpl.setUserId(costCenterUser.getUserId());

        return costCenterUserImpl;
    }

    /**
     * Returns the cost center user with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the cost center user
     * @return the cost center user
     * @throws com.liferay.portal.NoSuchModelException if a cost center user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CostCenterUser findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the cost center user with the primary key or throws a {@link com.hp.omp.NoSuchCostCenterUserException} if it could not be found.
     *
     * @param costCenterUserId the primary key of the cost center user
     * @return the cost center user
     * @throws com.hp.omp.NoSuchCostCenterUserException if a cost center user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public CostCenterUser findByPrimaryKey(long costCenterUserId)
        throws NoSuchCostCenterUserException, SystemException {
        CostCenterUser costCenterUser = fetchByPrimaryKey(costCenterUserId);

        if (costCenterUser == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + costCenterUserId);
            }

            throw new NoSuchCostCenterUserException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                costCenterUserId);
        }

        return costCenterUser;
    }

    /**
     * Returns the cost center user with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the cost center user
     * @return the cost center user, or <code>null</code> if a cost center user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CostCenterUser fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the cost center user with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param costCenterUserId the primary key of the cost center user
     * @return the cost center user, or <code>null</code> if a cost center user with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public CostCenterUser fetchByPrimaryKey(long costCenterUserId)
        throws SystemException {
        CostCenterUser costCenterUser = (CostCenterUser) EntityCacheUtil.getResult(CostCenterUserModelImpl.ENTITY_CACHE_ENABLED,
                CostCenterUserImpl.class, costCenterUserId);

        if (costCenterUser == _nullCostCenterUser) {
            return null;
        }

        if (costCenterUser == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                costCenterUser = (CostCenterUser) session.get(CostCenterUserImpl.class,
                        Long.valueOf(costCenterUserId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (costCenterUser != null) {
                    cacheResult(costCenterUser);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(CostCenterUserModelImpl.ENTITY_CACHE_ENABLED,
                        CostCenterUserImpl.class, costCenterUserId,
                        _nullCostCenterUser);
                }

                closeSession(session);
            }
        }

        return costCenterUser;
    }

    /**
     * Returns all the cost center users.
     *
     * @return the cost center users
     * @throws SystemException if a system exception occurred
     */
    public List<CostCenterUser> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the cost center users.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of cost center users
     * @param end the upper bound of the range of cost center users (not inclusive)
     * @return the range of cost center users
     * @throws SystemException if a system exception occurred
     */
    public List<CostCenterUser> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the cost center users.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of cost center users
     * @param end the upper bound of the range of cost center users (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of cost center users
     * @throws SystemException if a system exception occurred
     */
    public List<CostCenterUser> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<CostCenterUser> list = (List<CostCenterUser>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_COSTCENTERUSER);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_COSTCENTERUSER;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<CostCenterUser>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<CostCenterUser>) QueryUtil.list(q,
                            getDialect(), start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the cost center users from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (CostCenterUser costCenterUser : findAll()) {
            remove(costCenterUser);
        }
    }

    /**
     * Returns the number of cost center users.
     *
     * @return the number of cost center users
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_COSTCENTERUSER);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the cost center user persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.CostCenterUser")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<CostCenterUser>> listenersList = new ArrayList<ModelListener<CostCenterUser>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<CostCenterUser>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(CostCenterUserImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
