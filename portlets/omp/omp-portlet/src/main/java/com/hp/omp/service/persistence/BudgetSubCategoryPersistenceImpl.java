package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchBudgetSubCategoryException;
import com.hp.omp.model.BudgetSubCategory;
import com.hp.omp.model.impl.BudgetSubCategoryImpl;
import com.hp.omp.model.impl.BudgetSubCategoryModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the budget sub category service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see BudgetSubCategoryPersistence
 * @see BudgetSubCategoryUtil
 * @generated
 */
public class BudgetSubCategoryPersistenceImpl extends BasePersistenceImpl<BudgetSubCategory>
    implements BudgetSubCategoryPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link BudgetSubCategoryUtil} to access the budget sub category persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = BudgetSubCategoryImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(BudgetSubCategoryModelImpl.ENTITY_CACHE_ENABLED,
            BudgetSubCategoryModelImpl.FINDER_CACHE_ENABLED,
            BudgetSubCategoryImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(BudgetSubCategoryModelImpl.ENTITY_CACHE_ENABLED,
            BudgetSubCategoryModelImpl.FINDER_CACHE_ENABLED,
            BudgetSubCategoryImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(BudgetSubCategoryModelImpl.ENTITY_CACHE_ENABLED,
            BudgetSubCategoryModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_BUDGETSUBCATEGORY = "SELECT budgetSubCategory FROM BudgetSubCategory budgetSubCategory";
    private static final String _SQL_COUNT_BUDGETSUBCATEGORY = "SELECT COUNT(budgetSubCategory) FROM BudgetSubCategory budgetSubCategory";
    private static final String _ORDER_BY_ENTITY_ALIAS = "budgetSubCategory.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No BudgetSubCategory exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(BudgetSubCategoryPersistenceImpl.class);
    private static BudgetSubCategory _nullBudgetSubCategory = new BudgetSubCategoryImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<BudgetSubCategory> toCacheModel() {
                return _nullBudgetSubCategoryCacheModel;
            }
        };

    private static CacheModel<BudgetSubCategory> _nullBudgetSubCategoryCacheModel =
        new CacheModel<BudgetSubCategory>() {
            public BudgetSubCategory toEntityModel() {
                return _nullBudgetSubCategory;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the budget sub category in the entity cache if it is enabled.
     *
     * @param budgetSubCategory the budget sub category
     */
    public void cacheResult(BudgetSubCategory budgetSubCategory) {
        EntityCacheUtil.putResult(BudgetSubCategoryModelImpl.ENTITY_CACHE_ENABLED,
            BudgetSubCategoryImpl.class, budgetSubCategory.getPrimaryKey(),
            budgetSubCategory);

        budgetSubCategory.resetOriginalValues();
    }

    /**
     * Caches the budget sub categories in the entity cache if it is enabled.
     *
     * @param budgetSubCategories the budget sub categories
     */
    public void cacheResult(List<BudgetSubCategory> budgetSubCategories) {
        for (BudgetSubCategory budgetSubCategory : budgetSubCategories) {
            if (EntityCacheUtil.getResult(
                        BudgetSubCategoryModelImpl.ENTITY_CACHE_ENABLED,
                        BudgetSubCategoryImpl.class,
                        budgetSubCategory.getPrimaryKey()) == null) {
                cacheResult(budgetSubCategory);
            } else {
                budgetSubCategory.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all budget sub categories.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(BudgetSubCategoryImpl.class.getName());
        }

        EntityCacheUtil.clearCache(BudgetSubCategoryImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the budget sub category.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(BudgetSubCategory budgetSubCategory) {
        EntityCacheUtil.removeResult(BudgetSubCategoryModelImpl.ENTITY_CACHE_ENABLED,
            BudgetSubCategoryImpl.class, budgetSubCategory.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<BudgetSubCategory> budgetSubCategories) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (BudgetSubCategory budgetSubCategory : budgetSubCategories) {
            EntityCacheUtil.removeResult(BudgetSubCategoryModelImpl.ENTITY_CACHE_ENABLED,
                BudgetSubCategoryImpl.class, budgetSubCategory.getPrimaryKey());
        }
    }

    /**
     * Creates a new budget sub category with the primary key. Does not add the budget sub category to the database.
     *
     * @param budgetSubCategoryId the primary key for the new budget sub category
     * @return the new budget sub category
     */
    public BudgetSubCategory create(long budgetSubCategoryId) {
        BudgetSubCategory budgetSubCategory = new BudgetSubCategoryImpl();

        budgetSubCategory.setNew(true);
        budgetSubCategory.setPrimaryKey(budgetSubCategoryId);

        return budgetSubCategory;
    }

    /**
     * Removes the budget sub category with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param budgetSubCategoryId the primary key of the budget sub category
     * @return the budget sub category that was removed
     * @throws com.hp.omp.NoSuchBudgetSubCategoryException if a budget sub category with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public BudgetSubCategory remove(long budgetSubCategoryId)
        throws NoSuchBudgetSubCategoryException, SystemException {
        return remove(Long.valueOf(budgetSubCategoryId));
    }

    /**
     * Removes the budget sub category with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the budget sub category
     * @return the budget sub category that was removed
     * @throws com.hp.omp.NoSuchBudgetSubCategoryException if a budget sub category with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BudgetSubCategory remove(Serializable primaryKey)
        throws NoSuchBudgetSubCategoryException, SystemException {
        Session session = null;

        try {
            session = openSession();

            BudgetSubCategory budgetSubCategory = (BudgetSubCategory) session.get(BudgetSubCategoryImpl.class,
                    primaryKey);

            if (budgetSubCategory == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchBudgetSubCategoryException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(budgetSubCategory);
        } catch (NoSuchBudgetSubCategoryException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected BudgetSubCategory removeImpl(BudgetSubCategory budgetSubCategory)
        throws SystemException {
        budgetSubCategory = toUnwrappedModel(budgetSubCategory);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, budgetSubCategory);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(budgetSubCategory);

        return budgetSubCategory;
    }

    @Override
    public BudgetSubCategory updateImpl(
        com.hp.omp.model.BudgetSubCategory budgetSubCategory, boolean merge)
        throws SystemException {
        budgetSubCategory = toUnwrappedModel(budgetSubCategory);

        boolean isNew = budgetSubCategory.isNew();

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, budgetSubCategory, merge);

            budgetSubCategory.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(BudgetSubCategoryModelImpl.ENTITY_CACHE_ENABLED,
            BudgetSubCategoryImpl.class, budgetSubCategory.getPrimaryKey(),
            budgetSubCategory);

        return budgetSubCategory;
    }

    protected BudgetSubCategory toUnwrappedModel(
        BudgetSubCategory budgetSubCategory) {
        if (budgetSubCategory instanceof BudgetSubCategoryImpl) {
            return budgetSubCategory;
        }

        BudgetSubCategoryImpl budgetSubCategoryImpl = new BudgetSubCategoryImpl();

        budgetSubCategoryImpl.setNew(budgetSubCategory.isNew());
        budgetSubCategoryImpl.setPrimaryKey(budgetSubCategory.getPrimaryKey());

        budgetSubCategoryImpl.setBudgetSubCategoryId(budgetSubCategory.getBudgetSubCategoryId());
        budgetSubCategoryImpl.setBudgetSubCategoryName(budgetSubCategory.getBudgetSubCategoryName());
        budgetSubCategoryImpl.setDisplay(budgetSubCategory.isDisplay());

        return budgetSubCategoryImpl;
    }

    /**
     * Returns the budget sub category with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the budget sub category
     * @return the budget sub category
     * @throws com.liferay.portal.NoSuchModelException if a budget sub category with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BudgetSubCategory findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the budget sub category with the primary key or throws a {@link com.hp.omp.NoSuchBudgetSubCategoryException} if it could not be found.
     *
     * @param budgetSubCategoryId the primary key of the budget sub category
     * @return the budget sub category
     * @throws com.hp.omp.NoSuchBudgetSubCategoryException if a budget sub category with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public BudgetSubCategory findByPrimaryKey(long budgetSubCategoryId)
        throws NoSuchBudgetSubCategoryException, SystemException {
        BudgetSubCategory budgetSubCategory = fetchByPrimaryKey(budgetSubCategoryId);

        if (budgetSubCategory == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    budgetSubCategoryId);
            }

            throw new NoSuchBudgetSubCategoryException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                budgetSubCategoryId);
        }

        return budgetSubCategory;
    }

    /**
     * Returns the budget sub category with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the budget sub category
     * @return the budget sub category, or <code>null</code> if a budget sub category with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BudgetSubCategory fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the budget sub category with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param budgetSubCategoryId the primary key of the budget sub category
     * @return the budget sub category, or <code>null</code> if a budget sub category with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public BudgetSubCategory fetchByPrimaryKey(long budgetSubCategoryId)
        throws SystemException {
        BudgetSubCategory budgetSubCategory = (BudgetSubCategory) EntityCacheUtil.getResult(BudgetSubCategoryModelImpl.ENTITY_CACHE_ENABLED,
                BudgetSubCategoryImpl.class, budgetSubCategoryId);

        if (budgetSubCategory == _nullBudgetSubCategory) {
            return null;
        }

        if (budgetSubCategory == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                budgetSubCategory = (BudgetSubCategory) session.get(BudgetSubCategoryImpl.class,
                        Long.valueOf(budgetSubCategoryId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (budgetSubCategory != null) {
                    cacheResult(budgetSubCategory);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(BudgetSubCategoryModelImpl.ENTITY_CACHE_ENABLED,
                        BudgetSubCategoryImpl.class, budgetSubCategoryId,
                        _nullBudgetSubCategory);
                }

                closeSession(session);
            }
        }

        return budgetSubCategory;
    }

    /**
     * Returns all the budget sub categories.
     *
     * @return the budget sub categories
     * @throws SystemException if a system exception occurred
     */
    public List<BudgetSubCategory> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the budget sub categories.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of budget sub categories
     * @param end the upper bound of the range of budget sub categories (not inclusive)
     * @return the range of budget sub categories
     * @throws SystemException if a system exception occurred
     */
    public List<BudgetSubCategory> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the budget sub categories.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of budget sub categories
     * @param end the upper bound of the range of budget sub categories (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of budget sub categories
     * @throws SystemException if a system exception occurred
     */
    public List<BudgetSubCategory> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<BudgetSubCategory> list = (List<BudgetSubCategory>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_BUDGETSUBCATEGORY);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_BUDGETSUBCATEGORY;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<BudgetSubCategory>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<BudgetSubCategory>) QueryUtil.list(q,
                            getDialect(), start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the budget sub categories from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (BudgetSubCategory budgetSubCategory : findAll()) {
            remove(budgetSubCategory);
        }
    }

    /**
     * Returns the number of budget sub categories.
     *
     * @return the number of budget sub categories
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_BUDGETSUBCATEGORY);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the budget sub category persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.BudgetSubCategory")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<BudgetSubCategory>> listenersList = new ArrayList<ModelListener<BudgetSubCategory>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<BudgetSubCategory>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(BudgetSubCategoryImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
