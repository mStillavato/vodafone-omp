/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.GRAudit;
import com.hp.omp.model.GoodReceipt;
import com.hp.omp.model.Position;
import com.hp.omp.model.PurchaseOrder;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.custom.GoodReceiptStatus;
import com.hp.omp.model.custom.ListDTO;
import com.hp.omp.model.custom.PurchaseOrderStatus;
import com.hp.omp.model.custom.PurchaseRequestStatus;
import com.hp.omp.model.impl.GRAuditImpl;
import com.hp.omp.service.GRAuditLocalServiceUtil;
import com.hp.omp.service.GoodReceiptLocalServiceUtil;
import com.hp.omp.service.PositionLocalServiceUtil;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.hp.omp.service.base.GoodReceiptLocalServiceBaseImpl;
import com.hp.omp.service.persistence.GoodReceiptFinderUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionList;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.security.auth.PrincipalThreadLocal;

/**
 * The implementation of the good receipt local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.GoodReceiptLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.hp.omp.service.base.GoodReceiptLocalServiceBaseImpl
 * @see com.hp.omp.service.GoodReceiptLocalServiceUtil
 */

public class GoodReceiptLocalServiceImpl extends GoodReceiptLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.hp.omp.service.GoodReceiptLocalServiceUtil} to access the good receipt local service.
	 */
	public static final Logger logger = LoggerFactory.getLogger(GoodReceiptLocalServiceImpl.class);
	
	public List<GoodReceipt> getPositionGoodReceipts(String positionId) throws SystemException{
		
		logger.info("public List<GoodReceipt> getPositionGoodReceipts() - Start");
		
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(GoodReceipt.class);
		dynamicQuery.add(RestrictionsFactoryUtil.eq("positionId", positionId));
		dynamicQuery.addOrder(OrderFactoryUtil.asc("goodReceiptId"));
		@SuppressWarnings("unchecked")
		List<GoodReceipt> resultList = GoodReceiptLocalServiceUtil.dynamicQuery(dynamicQuery);
		if(resultList !=  null)  {
			logger.info("public List<GoodReceipt> getPositionGoodReceipts() - End with resultList not null");
			return resultList;
		}
		
		logger.info("public List<GoodReceipt> getPositionGoodReceipts() - End with resultList null");
		
		return new ArrayList<GoodReceipt>();
	}
	
	
	public List<Date> getPositionMaxRegisterDateGoodReceipts(String positionId) throws SystemException{
		
		logger.info("public List<Date> getPositionMaxRegisterDateGoodReceipts() - Start");
		
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(GoodReceipt.class);
		dynamicQuery.add(RestrictionsFactoryUtil.eq("positionId", positionId));
		dynamicQuery.add(RestrictionsFactoryUtil.isNotNull("goodReceiptNumber"));
		dynamicQuery.add(RestrictionsFactoryUtil.isNotNull("registerationDate"));
		dynamicQuery.setProjection(ProjectionFactoryUtil.max("registerationDate"));
		@SuppressWarnings("unchecked")
		List<Date> resultList = GoodReceiptLocalServiceUtil.dynamicQuery(dynamicQuery);
		if(resultList != null)  {
			logger.info("public List<Date> getPositionMaxRegisterDateGoodReceipts() - End with resultList not null");
			return resultList;
		}
		
		logger.info("public List<Date> getPositionMaxRegisterDateGoodReceipts() - End with resultList null");
		
		return new ArrayList<Date>();
	}
	
	
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public GoodReceipt addGoodReceipt(GoodReceipt goodReceipt) throws SystemException {
		
		logger.info("public GoodReceipt addGoodReceipt() - Start");
		
		GoodReceipt newGoodReceipt = super.addGoodReceipt(goodReceipt);
		addAuditGR(newGoodReceipt, newGoodReceipt.getGoodReceiptStatus());
		updatePurchaseOrderStatus(newGoodReceipt);
		updatePurchaseRequestStatus(newGoodReceipt);
		updatePositionFiscalYear(newGoodReceipt);
		
		logger.info("public GoodReceipt addGoodReceipt() - End");
		
		return newGoodReceipt;
	}
	
	
	@Override
	@Indexable(type = IndexableType.DELETE)
	public GoodReceipt deleteGoodReceipt(GoodReceipt goodReceipt) throws SystemException {
		
		logger.info("public GoodReceipt deleteGoodReceipt() - Start");
		
		addAuditGR(goodReceipt, GoodReceiptStatus.GR_DELETED);
		GoodReceipt newGoodReceipt=  super.deleteGoodReceipt(goodReceipt);
		updatePurchaseOrderStatus(newGoodReceipt);
		updatePurchaseRequestStatus(newGoodReceipt);
		updatePositionFiscalYear(newGoodReceipt);
		
		logger.info("public GoodReceipt deleteGoodReceipt() - End");
		
		return newGoodReceipt;
	}
	
	
	@Override
	@Indexable(type = IndexableType.DELETE)
	public GoodReceipt deleteGoodReceipt(long goodReceiptId) throws PortalException, SystemException {
		
		logger.info("public GoodReceipt deleteGoodReceipt() - Start");
		
		GoodReceipt goodReceipt = GoodReceiptLocalServiceUtil.getGoodReceipt(goodReceiptId);
		addAuditGR(goodReceipt, GoodReceiptStatus.GR_DELETED);
		GoodReceipt newGoodReceipt= super.deleteGoodReceipt(goodReceiptId);
		updatePurchaseOrderStatus(newGoodReceipt);
		updatePurchaseRequestStatus(newGoodReceipt);
		updatePositionFiscalYear(newGoodReceipt);
		
		logger.info("public GoodReceipt deleteGoodReceipt() - End");
		
		return newGoodReceipt;
	}
	
	
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public GoodReceipt updateGoodReceipt(GoodReceipt goodReceipt) throws SystemException {
		
		logger.info("public GoodReceipt updateGoodReceipt() - Start");
		
		GoodReceipt newGoodReceipt = super.updateGoodReceipt(goodReceipt);
		addAuditGR(newGoodReceipt, newGoodReceipt.getGoodReceiptStatus());
		updatePurchaseOrderStatus(newGoodReceipt);
		updatePurchaseRequestStatus(newGoodReceipt);
		updatePositionFiscalYear(newGoodReceipt);
		
		logger.info("public GoodReceipt updateGoodReceipt() - End");
		
		return newGoodReceipt;
	}
	
	
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public GoodReceipt updateGoodReceipt(GoodReceipt goodReceipt, boolean merge) throws SystemException {
		
		logger.info("public GoodReceipt updateGoodReceipt() - Start");
		
		GoodReceipt newGoodReceipt = super.updateGoodReceipt(goodReceipt, merge);
		addAuditGR(newGoodReceipt, newGoodReceipt.getGoodReceiptStatus());
		updatePurchaseOrderStatus(newGoodReceipt);
		updatePurchaseRequestStatus(newGoodReceipt);
		updatePositionFiscalYear(newGoodReceipt);
		
		logger.info("public GoodReceipt updateGoodReceipt() - End");
		
		return newGoodReceipt;
	}
	
	
	private void updatePurchaseRequestStatus(GoodReceipt goodReceipt) {
		
		logger.info("private void updatePurchaseRequestStatus() - Start");
		
		try {
			String purchaseRequestId = goodReceipt.getPosition().getPurchaseRequestId();
			PurchaseRequest purchaseRequest = PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.valueOf(purchaseRequestId));
			
			Double sumOfClosedValue = GoodReceiptFinderUtil.getSumCompeleteGRForPR(Long.valueOf(purchaseRequestId));
			Double totalValue = GoodReceiptFinderUtil.getSumValuePositionForPR(Long.valueOf(purchaseRequestId));
			if(sumOfClosedValue.equals(totalValue)) {
				purchaseRequest.setStatus(PurchaseRequestStatus.CLOSED.getCode());
				PurchaseRequestLocalServiceUtil.updatePurchaseRequest(purchaseRequest);
			} 
		
		} catch (PortalException e) {
			logger.error("Error in update Purchase Request closed status",e);
		} catch (SystemException e) {
			logger.error("Error in update Purchase Request closed status",e);
		}
		
		logger.info("private void updatePurchaseRequestStatus() - End");
	}

	
	/*
	 * Aggiorna lo stato dell'order da 5 (PO_RECEIVED) a 6 (GR_RECEIVED)
	 */
	private void updatePurchaseOrderStatus(GoodReceipt goodReceipt) throws SystemException {
		
		logger.info("private void updatePurchaseOrderStatus() - Start");
		
		try {
			String purchaseOrderId = goodReceipt.getPosition().getPurchaseOrderId();
			PurchaseOrder purchaseOrder = PurchaseOrderLocalServiceUtil.getPurchaseOrder(Long.valueOf(purchaseOrderId));

			Double sumOfClosedValue = GoodReceiptFinderUtil.getSumCompeleteGRForPO(Long.valueOf(purchaseOrderId), true);
			Double totalValue = GoodReceiptFinderUtil.getSumValuePositionForPO(Long.valueOf(purchaseOrderId));
			if(sumOfClosedValue.equals(totalValue)) {
				purchaseOrder.setStatus(PurchaseOrderStatus.PO_CLOSED.getCode());
			} else {
				Double sumOfReceivedValue  = GoodReceiptFinderUtil.getSumCompeleteGRForPO(Long.valueOf(purchaseOrderId), false);
				if(sumOfReceivedValue.equals(totalValue)) {
					purchaseOrder.setStatus(PurchaseOrderStatus.GR_RECEIVED.getCode());
				} else {
					//In case of there is no GRs for the PO 
					purchaseOrder.setStatus(PurchaseOrderStatus.PO_RECEIVED.getCode());
				}
			}
			PurchaseOrderLocalServiceUtil.updatePurchaseOrder(purchaseOrder);
		} catch(PortalException e) {
			logger.error("Error in  updating the purchase order status while updating good receipt",e);
		}

		logger.info("private void updatePurchaseOrderStatus() - End");
	}
	

	private void updatePositionFiscalYear(GoodReceipt goodReceipt) throws SystemException {
		
		logger.info("private void updatePositionFiscalYear() - Start");

		try {
			Date originalDate;
			long positionID = goodReceipt.getPosition().getPositionId();
			Position position = PositionLocalServiceUtil.getPosition(positionID);
			List<Date> grList = getPositionMaxRegisterDateGoodReceipts(String.valueOf(positionID));
			if (grList.size()>0 && grList.get(0) !=null) {
				originalDate = grList.get(0);
			} else {
				originalDate = position.getDeliveryDate();
			}
			Calendar myDate = new GregorianCalendar();
			myDate.setTime(originalDate);
			Calendar compareDate = new GregorianCalendar(
					myDate.get(Calendar.YEAR), 3, 1);
			String fiscalYear;
			if (myDate.before(compareDate)) {
				fiscalYear = (myDate.get(Calendar.YEAR) - 1) + "/" + myDate.get(Calendar.YEAR);
			} else {
				fiscalYear = myDate.get(Calendar.YEAR) + "/" + (myDate.get(Calendar.YEAR) + 1);
			}
			
			position.setFiscalYear(fiscalYear);
			PositionLocalServiceUtil.updatePosition(position);

		} catch (Exception e) {
			logger.error("Error in  updating the position fiscal year while updating good receipt",e);
		}
		
		logger.info("private void updatePositionFiscalYear() - End");
	}


	public Double getSumGRPercentageToPosition(String positionId) throws SystemException{
		
		logger.info("public Double getSumGRPercentageToPosition() - Start");
		
		DynamicQuery grQuery = DynamicQueryFactoryUtil.forClass(GoodReceipt.class);
		grQuery.add(RestrictionsFactoryUtil.eq("positionId", positionId));
		grQuery.setProjection(ProjectionFactoryUtil.sum("percentage"));
		@SuppressWarnings("unchecked")
		List<Double> resultQuery = GoodReceiptLocalServiceUtil.dynamicQuery(grQuery);
		if(resultQuery == null || resultQuery.size() ==0 || resultQuery.get(0) == null) {
			logger.info("public Double getSumGRPercentageToPosition() - End with 0");
			return 0D;
		} else {
			logger.info("public Double getSumGRPercentageToPosition() - End");
			return resultQuery.get(0);
		}
	}
	
	private void addAuditGR(GoodReceipt goodReceipt, GoodReceiptStatus status) throws SystemException {
		
		logger.info("private void addAuditGR() - Start");
		
		GRAudit grAudit = new GRAuditImpl();
		grAudit.setNew(true);
		grAudit.setStatus(status.getCode());
		long userId =  PrincipalThreadLocal.getUserId();
		grAudit.setUserId(userId);
		grAudit.setGoodReceiptId(goodReceipt.getGoodReceiptId());
		grAudit.setChangeDate(new Date());
		GRAuditLocalServiceUtil.addGRAudit(grAudit);
		
		logger.info("private void addAuditGR() - End");
	}

	public Long getOpenedGoodReceiptByCostCenterCount(long costCenterId) {
		
		logger.info("public Long getOpenedGoodReceiptByCostCenterCount() - Start");
		
		try {
			Long totalCount = GoodReceiptFinderUtil.getOpenedGoodReceiptByCostCenterCount(costCenterId);
			logger.info("public Long getOpenedGoodReceiptByCostCenterCount() - End");
			return totalCount;
		} catch (Exception e) {
			logger.error(e.getMessage());
			return Long.valueOf(0L);
		}
	}
	
	public Long getOpenedGoodReceiptCount() {
		
		logger.info("public Long getOpenedGoodReceiptCount() - Start");
		
		try {
			Long totalCount = GoodReceiptFinderUtil.getOpenedGoodReceiptCount();
			logger.info("public Long getOpenedGoodReceiptCount() - End");
			return totalCount;
		} catch (Exception e) {
			logger.error(e.getMessage());
			return Long.valueOf(0L);
		}
	}
	
	public List<ListDTO> getOpenedGoodReceiptByCostCenter(long costCenterId, int start, int end){
		
		logger.info("public List<ListDTO> getOpenedGoodReceiptByCostCenter() - Start");
		
		List<ListDTO> grDTOs = new ArrayList<ListDTO>();
		try {
			grDTOs = GoodReceiptFinderUtil.getOpenedGoodReceiptByCostCenter(costCenterId, start, end);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		logger.info("public List<ListDTO> getOpenedGoodReceiptByCostCenter() - End");
		
		return grDTOs;
	}
	
	public List<ListDTO> getOpenedGoodReceipt(int start, int end){
		
		logger.info("public List<ListDTO> getOpenedGoodReceipt() - Start");
		
		List<ListDTO> grDTOs = new ArrayList<ListDTO>();
		try {
			grDTOs = GoodReceiptFinderUtil.getOpenedGoodReceipt(start, end);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		logger.info("public List<ListDTO> getOpenedGoodReceipt() - End");
		
		return grDTOs;
	}
	
	
	public Long getPositionGRCount(String positionId) throws SystemException {
		DynamicQuery countQuery = DynamicQueryFactoryUtil.forClass(GoodReceipt.class);
		countQuery.add(PropertyFactoryUtil.forName("positionId").eq(positionId));
		countQuery.setProjection(ProjectionFactoryUtil.rowCount());
		@SuppressWarnings("unchecked")
		List<Long> resultQuery =  PurchaseRequestLocalServiceUtil.dynamicQuery(countQuery);
		
		return (resultQuery == null || resultQuery.size() == 0) ?  0L  : resultQuery.get(0);
	}
	
	
	public Long getPositionClosedGRCount(String positionId) throws SystemException {
		
		logger.info("public Long getPositionClosedGRCount() - Start");
		
		DynamicQuery countQuery = DynamicQueryFactoryUtil.forClass(GoodReceipt.class);
		countQuery.add(PropertyFactoryUtil.forName("positionId").eq(positionId));
		countQuery.add(RestrictionsFactoryUtil.isNotNull("goodReceiptNumber"));
		countQuery.add(RestrictionsFactoryUtil.isNotNull("registerationDate"));
		countQuery.setProjection(ProjectionFactoryUtil.rowCount());
		@SuppressWarnings("unchecked")
		List<Long> resultQuery =  PurchaseRequestLocalServiceUtil.dynamicQuery(countQuery);
		
		logger.info("public Long getPositionClosedGRCount() - End");
		
		return (resultQuery == null || resultQuery.size() == 0) ?  0L  : resultQuery.get(0);
	}

	
	public GoodReceipt updatePoStatus(GoodReceipt goodReceipt) throws SystemException {
		
		logger.info("public GoodReceipt updatePoStatus() - Start");
		
		updatePurchaseOrderStatus(goodReceipt);
		updatePurchaseRequestStatus(goodReceipt);
		
		logger.info("public GoodReceipt updatePoStatus() - End");
		
		return goodReceipt;
	}

}