package com.hp.omp.portlet;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.Catalogo;
import com.hp.omp.model.LayoutNotNssCatalogo;
import com.hp.omp.model.custom.LayoutNotNssCatalogoListDTO;
import com.hp.omp.model.custom.OmpFormatterUtil;
import com.hp.omp.model.impl.LayoutNotNssCatalogoImpl;
import com.hp.omp.service.CatalogoLocalServiceUtil;
import com.hp.omp.service.LayoutNotNssCatalogoLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class PurchaseOrderPortlet
 */
public class CatalogoNewNotNssPortlet extends MVCPortlet {

	Logger logger = LoggerFactory.getLogger(CatalogoNewNotNssPortlet.class);
	public static final String DATE_FORMAT = "dd/MM/yyyy"; 

	private Object lock  = new Object();

	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		HttpServletRequest httpServletRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(request));
		String jspPage = httpServletRequest.getParameter("jspPage");
		if(jspPage == null) {
			jspPage = ParamUtil.getString(request, "jspPage","/html/catalogonewnotnss/addpo.jsp");
		}
		handleAddPOForm(request,response);

		getPortletContext().getRequestDispatcher(jspPage).include(request, response);
	}


	private void handleAddPOForm(RenderRequest request, RenderResponse response) {
		logger.info("handleAddPOForm");

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		HttpServletRequest httpServletRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(request));

		String redirect = httpServletRequest.getParameter("redirect");
		if(redirect==null || redirect.isEmpty()){
			redirect ="/group/vodafone/catalogo"; 
		}
		
		String addPoForm = httpServletRequest.getParameter("GET_ADD_PO");

		try {
			if("getRowExcel".equals(addPoForm)) {
				String layoutNotNssCatalogoId = httpServletRequest.getParameter("layoutNotNssCatalogoId");
				logger.info("getRowExcel - layoutNotNssCatalogoId = " + layoutNotNssCatalogoId);

				LayoutNotNssCatalogo layoutNotNssCatalogo = LayoutNotNssCatalogoLocalServiceUtil.getLayoutNotNssCatalogo(Long.valueOf(layoutNotNssCatalogoId));

				if (layoutNotNssCatalogo != null) {
					request.setAttribute("layoutNotNssCatalogoId", layoutNotNssCatalogo.getLayoutNotNssCatalogoId());
					request.setAttribute("lineNumber", layoutNotNssCatalogo.getLineNumber());
					request.setAttribute("quantity", layoutNotNssCatalogo.getQuantity());
					request.setAttribute("deliverydate", dateToString(layoutNotNssCatalogo.getDeliveryDate()));
					request.setAttribute("deliveryadress", layoutNotNssCatalogo.getDeliveryAddress());
					request.setAttribute("locationEVO", layoutNotNssCatalogo.getLocationEvo());
					request.setAttribute("targaTecnica", layoutNotNssCatalogo.getTargaTecnica());
					request.setAttribute("itemText", layoutNotNssCatalogo.getItemText());
					request.setAttribute("primoVendorCode", layoutNotNssCatalogo.getVendorCode());
					request.setAttribute("icApproval", layoutNotNssCatalogo.getIcApproval());
					request.setAttribute("categoryCode", layoutNotNssCatalogo.getCategoryCode());
					request.setAttribute("offerNumber", layoutNotNssCatalogo.getOfferNumber());

					Catalogo catalogo = CatalogoLocalServiceUtil.getCatalogo(layoutNotNssCatalogo.getCatalogoId());
					request.setAttribute("catalogo", catalogo);

					if(catalogo.getPrezzo() != null && ! "".equals(catalogo.getPrezzo())) {
						request.setAttribute("prezzo", OmpFormatterUtil.formatNumberByLocale(Double.valueOf(catalogo.getPrezzo()),Locale.ITALY));
					}else {
						request.setAttribute("prezzo","0");
					}
				}
			}

			if("addPoForm".equals(addPoForm)) {
				String catalogoId = httpServletRequest.getParameter("catalogoId");
				logger.info("addPoForm - catalogoId = " + catalogoId);

				Catalogo catalogo = CatalogoLocalServiceUtil.getCatalogo(Long.parseLong(catalogoId));
				request.setAttribute("catalogo", catalogo);

				if(catalogo.getPrezzo() != null && ! "".equals(catalogo.getPrezzo())) {
					request.setAttribute("prezzo", OmpFormatterUtil.formatNumberByLocale(Double.valueOf(catalogo.getPrezzo()),Locale.ITALY));
				}else {
					request.setAttribute("prezzo","0");
				}

				request.setAttribute("numberOfPositions", 0);
				request.setAttribute("layoutNotNssCatalogoId", 0);
				
				List<LayoutNotNssCatalogoListDTO> layoutNotNssCatalogoList = LayoutNotNssCatalogoLocalServiceUtil.getLayoutNotNssCatalogoList(themeDisplay.getUser().getScreenName());
				if(layoutNotNssCatalogoList != null && layoutNotNssCatalogoList.size() > 0){
					request.setAttribute("layoutNotNssCatalogoList", layoutNotNssCatalogoList);
					request.setAttribute("numberOfPositions", layoutNotNssCatalogoList.size());

					int lastItem = layoutNotNssCatalogoList.size()-1;
					request.setAttribute("quantity", layoutNotNssCatalogoList.get(lastItem).getQuantity());
					request.setAttribute("deliverydate", layoutNotNssCatalogoList.get(lastItem).getDeliveryDate());
					request.setAttribute("deliveryadress", layoutNotNssCatalogoList.get(lastItem).getDeliveryAddress());
					request.setAttribute("locationEVO", layoutNotNssCatalogoList.get(lastItem).getLocationEvo());
					request.setAttribute("targaTecnica", layoutNotNssCatalogoList.get(lastItem).getTargaTecnica());
					request.setAttribute("itemText", layoutNotNssCatalogoList.get(lastItem).getItemText());
					request.setAttribute("primoVendorCode", layoutNotNssCatalogoList.get(lastItem).getVendorCode());
					request.setAttribute("icApproval", layoutNotNssCatalogoList.get(lastItem).getIcApproval());
					request.setAttribute("categoryCode", layoutNotNssCatalogoList.get(lastItem).getCategoryCode());
					request.setAttribute("offerNumber", layoutNotNssCatalogoList.get(lastItem).getOfferNumber());
				}
			}

			request.setAttribute("redirect", redirect);

		}catch(PortalException e) {
			logger.error("Error in handle Add PO Form",e);
		} catch(SystemException e) {
			logger.error("Error in handle Add PO Form",e);
		} catch (Exception e) {
			logger.error("Error in handle Add PO Form",e);
		}
	}


	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		logger.debug("serverResource");
	}

	public void addPosition(ActionRequest request, ActionResponse response) throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		LayoutNotNssCatalogo layoutNotNssCatalogo = new LayoutNotNssCatalogoImpl();
		layoutNotNssCatalogo.setLineNumber(1);
		layoutNotNssCatalogo.setMaterialId(ParamUtil.getString(request, "materialId"));
		layoutNotNssCatalogo.setMaterialDesc(ParamUtil.getString(request, "materialDesc"));
		layoutNotNssCatalogo.setQuantity(ParamUtil.getString(request, "quantity"));
		layoutNotNssCatalogo.setNetPrice(ParamUtil.getString(request, "netPrice"));
		layoutNotNssCatalogo.setCurrency(ParamUtil.getString(request, "currency"));
		layoutNotNssCatalogo.setDeliveryDate(stringToDate(ParamUtil.getString(request, "deliverydate")));
		layoutNotNssCatalogo.setDeliveryAddress(ParamUtil.getString(request, "deliveryadress"));
		layoutNotNssCatalogo.setItemText(ParamUtil.getString(request, "itemText"));
		layoutNotNssCatalogo.setLocationEvo(ParamUtil.getString(request, "locationEVO"));
		layoutNotNssCatalogo.setTargaTecnica(ParamUtil.getString(request, "targaTecnica"));
		layoutNotNssCatalogo.setUser_(themeDisplay.getUser().getScreenName());
		layoutNotNssCatalogo.setVendorCode(ParamUtil.getString(request, "vendorCode"));
		layoutNotNssCatalogo.setCatalogoId(ParamUtil.getLong(request, "catalogoId"));
		layoutNotNssCatalogo.setIcApproval(ParamUtil.getString(request, "iandc"));
		layoutNotNssCatalogo.setCategoryCode(ParamUtil.getString(request, "categorycode"));
		layoutNotNssCatalogo.setOfferNumber(ParamUtil.getString(request, "offernumber"));

		try {
			List<LayoutNotNssCatalogoListDTO> layoutNotNssCatalogoList = LayoutNotNssCatalogoLocalServiceUtil.getLayoutNotNssCatalogoList(layoutNotNssCatalogo.getUser_());
			if(layoutNotNssCatalogoList != null && layoutNotNssCatalogoList.size() > 0)
				layoutNotNssCatalogo.setLineNumber(layoutNotNssCatalogoList.size()+1);

			LayoutNotNssCatalogoLocalServiceUtil.addLayoutNotNssCatalogo(layoutNotNssCatalogo);

			layoutNotNssCatalogoList = LayoutNotNssCatalogoLocalServiceUtil.getLayoutNotNssCatalogoList(layoutNotNssCatalogo.getUser_());
			request.setAttribute("layoutNotNssCatalogoList", layoutNotNssCatalogoList);
			request.setAttribute("numberOfPositions", layoutNotNssCatalogoList.size());

			sendRedirect(request, response);
			//getPortletContext().getRequestDispatcher(jspPage).include(request, response);
		} catch (SystemException e) {
			logger.error("Error in save or update Position Not Nss Catalog", e);
		}

	}

	public void deletePosition(ActionRequest request, ActionResponse response) throws IOException, PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		try {
			long layoutNotNssCatalogoId = ParamUtil.getLong(request, "layoutNotNssCatalogoId");
			logger.info("deletePosition - layoutNotNssCatalogoId = " + layoutNotNssCatalogoId);
			
			LayoutNotNssCatalogoLocalServiceUtil.deleteLayoutNotNssCatalogo(layoutNotNssCatalogoId);

			List<LayoutNotNssCatalogoListDTO> layoutNotNssCatalogoList = LayoutNotNssCatalogoLocalServiceUtil.getLayoutNotNssCatalogoList(themeDisplay.getUser().getScreenName());
			request.setAttribute("layoutNotNssCatalogoList", layoutNotNssCatalogoList);
			request.setAttribute("numberOfPositions", layoutNotNssCatalogoList.size());

			sendRedirect(request, response);
		} catch (PortalException e){
			logger.error("Error in delete Position Not Nss Catalog", e);
		} catch (SystemException e) {
			logger.error("Error in delete Position Not Nss Catalog", e);
		}
	}
	
	public void updatePosition(ActionRequest request, ActionResponse response) throws IOException, PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);

		try {
			long layoutNotNssCatalogoId = ParamUtil.getLong(request, "layoutNotNssCatalogoId");
			logger.info("deletePosition - layoutNotNssCatalogoId = " + layoutNotNssCatalogoId);
			
			LayoutNotNssCatalogo layoutNotNssCatalogo = new LayoutNotNssCatalogoImpl();
			layoutNotNssCatalogo.setLineNumber(ParamUtil.getLong(request, "lineNumber"));
			layoutNotNssCatalogo.setMaterialId(ParamUtil.getString(request, "materialId"));
			layoutNotNssCatalogo.setMaterialDesc(ParamUtil.getString(request, "materialDesc"));
			layoutNotNssCatalogo.setQuantity(ParamUtil.getString(request, "quantity"));
			layoutNotNssCatalogo.setNetPrice(ParamUtil.getString(request, "netPrice"));
			layoutNotNssCatalogo.setCurrency(ParamUtil.getString(request, "currency"));
			layoutNotNssCatalogo.setDeliveryDate(stringToDate(ParamUtil.getString(request, "deliverydate")));
			layoutNotNssCatalogo.setDeliveryAddress(ParamUtil.getString(request, "deliveryadress"));
			layoutNotNssCatalogo.setItemText(ParamUtil.getString(request, "itemText"));
			layoutNotNssCatalogo.setLocationEvo(ParamUtil.getString(request, "locationEVO"));
			layoutNotNssCatalogo.setTargaTecnica(ParamUtil.getString(request, "targaTecnica"));
			layoutNotNssCatalogo.setUser_(themeDisplay.getUser().getScreenName());
			layoutNotNssCatalogo.setVendorCode(ParamUtil.getString(request, "vendorCode"));
			layoutNotNssCatalogo.setCatalogoId(ParamUtil.getLong(request, "catalogoId"));
			layoutNotNssCatalogo.setIcApproval(ParamUtil.getString(request, "iandc"));
			layoutNotNssCatalogo.setCategoryCode(ParamUtil.getString(request, "categorycode"));
			layoutNotNssCatalogo.setOfferNumber(ParamUtil.getString(request, "offernumber"));
			
			LayoutNotNssCatalogoLocalServiceUtil.deleteLayoutNotNssCatalogo(layoutNotNssCatalogoId);
			LayoutNotNssCatalogoLocalServiceUtil.addLayoutNotNssCatalogo(layoutNotNssCatalogo);

			List<LayoutNotNssCatalogoListDTO> layoutNotNssCatalogoList = LayoutNotNssCatalogoLocalServiceUtil.getLayoutNotNssCatalogoList(themeDisplay.getUser().getScreenName());
			request.setAttribute("layoutNotNssCatalogoList", layoutNotNssCatalogoList);
			request.setAttribute("numberOfPositions", layoutNotNssCatalogoList.size());

			sendRedirect(request, response);
		} catch (SystemException e) {
			logger.error("Error in delete Position Not Nss Catalog", e);
		} catch (PortalException e) {
			logger.error("Error in delete Position Not Nss Catalog", e);
		}
	}

	private Date stringToDate(String date){
		try {
			return new SimpleDateFormat("dd/MM/yyyy").parse(date);
		} catch (ParseException e) {
			return null;
		}
	}

	private String dateToString(Date date){
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		return formatter.format(date);
	}
}