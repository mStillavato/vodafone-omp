package com.hp.omp.model.impl;

import com.hp.omp.model.MacroDriver;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

/**
 * The cache model class for representing MacroDriver in entity cache.
 *
 * @author HP Egypt team
 * @see MacroDriver
 * @generated
 */
public class MacroDriverCacheModel implements CacheModel<MacroDriver>,
    Serializable {
    public long macroDriverId;
    public String macroDriverName;
    public boolean display;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{macroDriverId=");
        sb.append(macroDriverId);
        sb.append(", macroDriverName=");
        sb.append(macroDriverName);
        sb.append(", display=");
        sb.append(display);
        sb.append("}");

        return sb.toString();
    }

    public MacroDriver toEntityModel() {
        MacroDriverImpl macroDriverImpl = new MacroDriverImpl();

        macroDriverImpl.setMacroDriverId(macroDriverId);

        if (macroDriverName == null) {
            macroDriverImpl.setMacroDriverName(StringPool.BLANK);
        } else {
            macroDriverImpl.setMacroDriverName(macroDriverName);
        }

        macroDriverImpl.setDisplay(display);

        macroDriverImpl.resetOriginalValues();

        return macroDriverImpl;
    }
}
