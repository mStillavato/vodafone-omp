/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.model.impl;

import com.hp.omp.model.Catalogo;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Catalogo in entity cache.
 *
 * @author HP Egypt team
 * @see Catalogo
 * @generated
 */
public class CatalogoCacheModel implements CacheModel<Catalogo>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(47);

		sb.append("{catalogoId=");
		sb.append(catalogoId);
		sb.append(", chiaveInforecord=");
		sb.append(chiaveInforecord);
		sb.append(", dataCreazione=");
		sb.append(dataCreazione);
		sb.append(", dataFineValidita=");
		sb.append(dataFineValidita);
		sb.append(", divisione=");
		sb.append(divisione);
		sb.append(", flgCanc=");
		sb.append(flgCanc);
		sb.append(", fornFinCode=");
		sb.append(fornFinCode);
		sb.append(", fornitoreCode=");
		sb.append(fornitoreCode);
		sb.append(", fornitoreDesc=");
		sb.append(fornitoreDesc);
		sb.append(", matCatLvl2=");
		sb.append(matCatLvl2);
		sb.append(", matCatLvl2Desc=");
		sb.append(matCatLvl2Desc);
		sb.append(", matCatLvl4=");
		sb.append(matCatLvl4);
		sb.append(", matCatLvl4Desc=");
		sb.append(matCatLvl4Desc);
		sb.append(", matCod=");
		sb.append(matCod);
		sb.append(", matCodFornFinale=");
		sb.append(matCodFornFinale);
		sb.append(", matDesc=");
		sb.append(matDesc);
		sb.append(", prezzo=");
		sb.append(prezzo);
		sb.append(", purchOrganiz=");
		sb.append(purchOrganiz);
		sb.append(", um=");
		sb.append(um);
		sb.append(", umPrezzo=");
		sb.append(umPrezzo);
		sb.append(", umPrezzoPo=");
		sb.append(umPrezzoPo);
		sb.append(", valuta=");
		sb.append(valuta);
		sb.append(", upperMatDesc=");
		sb.append(upperMatDesc);
		sb.append("}");

		return sb.toString();
	}

	public Catalogo toEntityModel() {
		CatalogoImpl catalogoImpl = new CatalogoImpl();

		catalogoImpl.setCatalogoId(catalogoId);

		if (chiaveInforecord == null) {
			catalogoImpl.setChiaveInforecord(StringPool.BLANK);
		}
		else {
			catalogoImpl.setChiaveInforecord(chiaveInforecord);
		}

		if (dataCreazione == Long.MIN_VALUE) {
			catalogoImpl.setDataCreazione(null);
		}
		else {
			catalogoImpl.setDataCreazione(new Date(dataCreazione));
		}

		if (dataFineValidita == Long.MIN_VALUE) {
			catalogoImpl.setDataFineValidita(null);
		}
		else {
			catalogoImpl.setDataFineValidita(new Date(dataFineValidita));
		}

		if (divisione == null) {
			catalogoImpl.setDivisione(StringPool.BLANK);
		}
		else {
			catalogoImpl.setDivisione(divisione);
		}

		catalogoImpl.setFlgCanc(flgCanc);

		if (fornFinCode == null) {
			catalogoImpl.setFornFinCode(StringPool.BLANK);
		}
		else {
			catalogoImpl.setFornFinCode(fornFinCode);
		}

		if (fornitoreCode == null) {
			catalogoImpl.setFornitoreCode(StringPool.BLANK);
		}
		else {
			catalogoImpl.setFornitoreCode(fornitoreCode);
		}

		if (fornitoreDesc == null) {
			catalogoImpl.setFornitoreDesc(StringPool.BLANK);
		}
		else {
			catalogoImpl.setFornitoreDesc(fornitoreDesc);
		}

		if (matCatLvl2 == null) {
			catalogoImpl.setMatCatLvl2(StringPool.BLANK);
		}
		else {
			catalogoImpl.setMatCatLvl2(matCatLvl2);
		}

		if (matCatLvl2Desc == null) {
			catalogoImpl.setMatCatLvl2Desc(StringPool.BLANK);
		}
		else {
			catalogoImpl.setMatCatLvl2Desc(matCatLvl2Desc);
		}

		if (matCatLvl4 == null) {
			catalogoImpl.setMatCatLvl4(StringPool.BLANK);
		}
		else {
			catalogoImpl.setMatCatLvl4(matCatLvl4);
		}

		if (matCatLvl4Desc == null) {
			catalogoImpl.setMatCatLvl4Desc(StringPool.BLANK);
		}
		else {
			catalogoImpl.setMatCatLvl4Desc(matCatLvl4Desc);
		}

		if (matCod == null) {
			catalogoImpl.setMatCod(StringPool.BLANK);
		}
		else {
			catalogoImpl.setMatCod(matCod);
		}

		if (matCodFornFinale == null) {
			catalogoImpl.setMatCodFornFinale(StringPool.BLANK);
		}
		else {
			catalogoImpl.setMatCodFornFinale(matCodFornFinale);
		}

		if (matDesc == null) {
			catalogoImpl.setMatDesc(StringPool.BLANK);
		}
		else {
			catalogoImpl.setMatDesc(matDesc);
		}

		if (prezzo == null) {
			catalogoImpl.setPrezzo(StringPool.BLANK);
		}
		else {
			catalogoImpl.setPrezzo(prezzo);
		}

		if (purchOrganiz == null) {
			catalogoImpl.setPurchOrganiz(StringPool.BLANK);
		}
		else {
			catalogoImpl.setPurchOrganiz(purchOrganiz);
		}

		if (um == null) {
			catalogoImpl.setUm(StringPool.BLANK);
		}
		else {
			catalogoImpl.setUm(um);
		}

		if (umPrezzo == null) {
			catalogoImpl.setUmPrezzo(StringPool.BLANK);
		}
		else {
			catalogoImpl.setUmPrezzo(umPrezzo);
		}

		if (umPrezzoPo == null) {
			catalogoImpl.setUmPrezzoPo(StringPool.BLANK);
		}
		else {
			catalogoImpl.setUmPrezzoPo(umPrezzoPo);
		}

		if (valuta == null) {
			catalogoImpl.setValuta(StringPool.BLANK);
		}
		else {
			catalogoImpl.setValuta(valuta);
		}

		if (upperMatDesc == null) {
			catalogoImpl.setUpperMatDesc(StringPool.BLANK);
		}
		else {
			catalogoImpl.setUpperMatDesc(upperMatDesc);
		}

		catalogoImpl.resetOriginalValues();

		return catalogoImpl;
	}

	public long catalogoId;
	public String chiaveInforecord;
	public long dataCreazione;
	public long dataFineValidita;
	public String divisione;
	public boolean flgCanc;
	public String fornFinCode;
	public String fornitoreCode;
	public String fornitoreDesc;
	public String matCatLvl2;
	public String matCatLvl2Desc;
	public String matCatLvl4;
	public String matCatLvl4Desc;
	public String matCod;
	public String matCodFornFinale;
	public String matDesc;
	public String prezzo;
	public String purchOrganiz;
	public String um;
	public String umPrezzo;
	public String umPrezzoPo;
	public String valuta;
	public String upperMatDesc;
}