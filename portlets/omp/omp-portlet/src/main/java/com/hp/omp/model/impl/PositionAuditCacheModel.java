package com.hp.omp.model.impl;

import com.hp.omp.model.PositionAudit;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing PositionAudit in entity cache.
 *
 * @author HP Egypt team
 * @see PositionAudit
 * @generated
 */
public class PositionAuditCacheModel implements CacheModel<PositionAudit>,
    Serializable {
    public long auditId;
    public Long positionId;
    public int status;
    public Long userId;
    public long changeDate;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{auditId=");
        sb.append(auditId);
        sb.append(", positionId=");
        sb.append(positionId);
        sb.append(", status=");
        sb.append(status);
        sb.append(", userId=");
        sb.append(userId);
        sb.append(", changeDate=");
        sb.append(changeDate);
        sb.append("}");

        return sb.toString();
    }

    public PositionAudit toEntityModel() {
        PositionAuditImpl positionAuditImpl = new PositionAuditImpl();

        positionAuditImpl.setAuditId(auditId);
        positionAuditImpl.setPositionId(positionId);
        positionAuditImpl.setStatus(status);
        positionAuditImpl.setUserId(userId);

        if (changeDate == Long.MIN_VALUE) {
            positionAuditImpl.setChangeDate(null);
        } else {
            positionAuditImpl.setChangeDate(new Date(changeDate));
        }

        positionAuditImpl.resetOriginalValues();

        return positionAuditImpl;
    }
}
