package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchPositionException;
import com.hp.omp.model.Position;
import com.hp.omp.model.impl.PositionImpl;
import com.hp.omp.model.impl.PositionModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the position service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see PositionPersistence
 * @see PositionUtil
 * @generated
 */
public class PositionPersistenceImpl extends BasePersistenceImpl<Position>
    implements PositionPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link PositionUtil} to access the position persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = PositionImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PURCHASEORDERPOSITIONS =
        new FinderPath(PositionModelImpl.ENTITY_CACHE_ENABLED,
            PositionModelImpl.FINDER_CACHE_ENABLED, PositionImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByPurchaseOrderPositions",
            new String[] {
                String.class.getName(),
                
            "java.lang.Integer", "java.lang.Integer",
                "com.liferay.portal.kernel.util.OrderByComparator"
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PURCHASEORDERPOSITIONS =
        new FinderPath(PositionModelImpl.ENTITY_CACHE_ENABLED,
            PositionModelImpl.FINDER_CACHE_ENABLED, PositionImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByPurchaseOrderPositions",
            new String[] { String.class.getName() },
            PositionModelImpl.PURCHASEORDERID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_PURCHASEORDERPOSITIONS = new FinderPath(PositionModelImpl.ENTITY_CACHE_ENABLED,
            PositionModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByPurchaseOrderPositions",
            new String[] { String.class.getName() });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PURCHASEREQUESTPOSITIONS =
        new FinderPath(PositionModelImpl.ENTITY_CACHE_ENABLED,
            PositionModelImpl.FINDER_CACHE_ENABLED, PositionImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByPurchaseRequestPositions",
            new String[] {
                String.class.getName(),
                
            "java.lang.Integer", "java.lang.Integer",
                "com.liferay.portal.kernel.util.OrderByComparator"
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PURCHASEREQUESTPOSITIONS =
        new FinderPath(PositionModelImpl.ENTITY_CACHE_ENABLED,
            PositionModelImpl.FINDER_CACHE_ENABLED, PositionImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByPurchaseRequestPositions",
            new String[] { String.class.getName() },
            PositionModelImpl.PURCHASEREQUESTID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_PURCHASEREQUESTPOSITIONS =
        new FinderPath(PositionModelImpl.ENTITY_CACHE_ENABLED,
            PositionModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByPurchaseRequestPositions",
            new String[] { String.class.getName() });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PositionModelImpl.ENTITY_CACHE_ENABLED,
            PositionModelImpl.FINDER_CACHE_ENABLED, PositionImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PositionModelImpl.ENTITY_CACHE_ENABLED,
            PositionModelImpl.FINDER_CACHE_ENABLED, PositionImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PositionModelImpl.ENTITY_CACHE_ENABLED,
            PositionModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_POSITION = "SELECT position FROM Position position";
    private static final String _SQL_SELECT_POSITION_WHERE = "SELECT position FROM Position position WHERE ";
    private static final String _SQL_COUNT_POSITION = "SELECT COUNT(position) FROM Position position";
    private static final String _SQL_COUNT_POSITION_WHERE = "SELECT COUNT(position) FROM Position position WHERE ";
    private static final String _FINDER_COLUMN_PURCHASEORDERPOSITIONS_PURCHASEORDERID_1 =
        "position.purchaseOrderId IS NULL";
    private static final String _FINDER_COLUMN_PURCHASEORDERPOSITIONS_PURCHASEORDERID_2 =
        "position.purchaseOrderId = ?";
    private static final String _FINDER_COLUMN_PURCHASEORDERPOSITIONS_PURCHASEORDERID_3 =
        "(position.purchaseOrderId IS NULL OR position.purchaseOrderId = ?)";
    private static final String _FINDER_COLUMN_PURCHASEREQUESTPOSITIONS_PURCHASEREQUESTID_1 =
        "position.purchaseRequestId IS NULL";
    private static final String _FINDER_COLUMN_PURCHASEREQUESTPOSITIONS_PURCHASEREQUESTID_2 =
        "position.purchaseRequestId = ?";
    private static final String _FINDER_COLUMN_PURCHASEREQUESTPOSITIONS_PURCHASEREQUESTID_3 =
        "(position.purchaseRequestId IS NULL OR position.purchaseRequestId = ?)";
    private static final String _ORDER_BY_ENTITY_ALIAS = "position.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Position exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Position exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(PositionPersistenceImpl.class);
    private static Position _nullPosition = new PositionImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Position> toCacheModel() {
                return _nullPositionCacheModel;
            }
        };

    private static CacheModel<Position> _nullPositionCacheModel = new CacheModel<Position>() {
            public Position toEntityModel() {
                return _nullPosition;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the position in the entity cache if it is enabled.
     *
     * @param position the position
     */
    public void cacheResult(Position position) {
        EntityCacheUtil.putResult(PositionModelImpl.ENTITY_CACHE_ENABLED,
            PositionImpl.class, position.getPrimaryKey(), position);

        position.resetOriginalValues();
    }

    /**
     * Caches the positions in the entity cache if it is enabled.
     *
     * @param positions the positions
     */
    public void cacheResult(List<Position> positions) {
        for (Position position : positions) {
            if (EntityCacheUtil.getResult(
                        PositionModelImpl.ENTITY_CACHE_ENABLED,
                        PositionImpl.class, position.getPrimaryKey()) == null) {
                cacheResult(position);
            } else {
                position.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all positions.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(PositionImpl.class.getName());
        }

        EntityCacheUtil.clearCache(PositionImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the position.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Position position) {
        EntityCacheUtil.removeResult(PositionModelImpl.ENTITY_CACHE_ENABLED,
            PositionImpl.class, position.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Position> positions) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Position position : positions) {
            EntityCacheUtil.removeResult(PositionModelImpl.ENTITY_CACHE_ENABLED,
                PositionImpl.class, position.getPrimaryKey());
        }
    }

    /**
     * Creates a new position with the primary key. Does not add the position to the database.
     *
     * @param positionId the primary key for the new position
     * @return the new position
     */
    public Position create(long positionId) {
        Position position = new PositionImpl();

        position.setNew(true);
        position.setPrimaryKey(positionId);

        return position;
    }

    /**
     * Removes the position with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param positionId the primary key of the position
     * @return the position that was removed
     * @throws com.hp.omp.NoSuchPositionException if a position with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public Position remove(long positionId)
        throws NoSuchPositionException, SystemException {
        return remove(Long.valueOf(positionId));
    }

    /**
     * Removes the position with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the position
     * @return the position that was removed
     * @throws com.hp.omp.NoSuchPositionException if a position with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Position remove(Serializable primaryKey)
        throws NoSuchPositionException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Position position = (Position) session.get(PositionImpl.class,
                    primaryKey);

            if (position == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchPositionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(position);
        } catch (NoSuchPositionException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Position removeImpl(Position position) throws SystemException {
        position = toUnwrappedModel(position);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, position);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(position);

        return position;
    }

    @Override
    public Position updateImpl(com.hp.omp.model.Position position, boolean merge)
        throws SystemException {
        position = toUnwrappedModel(position);

        boolean isNew = position.isNew();

        PositionModelImpl positionModelImpl = (PositionModelImpl) position;

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, position, merge);

            position.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !PositionModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((positionModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PURCHASEORDERPOSITIONS.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        positionModelImpl.getOriginalPurchaseOrderId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PURCHASEORDERPOSITIONS,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PURCHASEORDERPOSITIONS,
                    args);

                args = new Object[] { positionModelImpl.getPurchaseOrderId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PURCHASEORDERPOSITIONS,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PURCHASEORDERPOSITIONS,
                    args);
            }

            if ((positionModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PURCHASEREQUESTPOSITIONS.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        positionModelImpl.getOriginalPurchaseRequestId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PURCHASEREQUESTPOSITIONS,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PURCHASEREQUESTPOSITIONS,
                    args);

                args = new Object[] { positionModelImpl.getPurchaseRequestId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PURCHASEREQUESTPOSITIONS,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PURCHASEREQUESTPOSITIONS,
                    args);
            }
        }

        EntityCacheUtil.putResult(PositionModelImpl.ENTITY_CACHE_ENABLED,
            PositionImpl.class, position.getPrimaryKey(), position);

        return position;
    }

    protected Position toUnwrappedModel(Position position) {
        if (position instanceof PositionImpl) {
            return position;
        }

        PositionImpl positionImpl = new PositionImpl();

        positionImpl.setNew(position.isNew());
        positionImpl.setPrimaryKey(position.getPrimaryKey());

        positionImpl.setPositionId(position.getPositionId());
        positionImpl.setFiscalYear(position.getFiscalYear());
        positionImpl.setDescription(position.getDescription());
        positionImpl.setCategoryCode(position.getCategoryCode());
        positionImpl.setOfferNumber(position.getOfferNumber());
        positionImpl.setOfferDate(position.getOfferDate());
        positionImpl.setQuatity(position.getQuatity());
        positionImpl.setDeliveryDate(position.getDeliveryDate());
        positionImpl.setDeliveryAddress(position.getDeliveryAddress());
        positionImpl.setActualUnitCost(position.getActualUnitCost());
        positionImpl.setNumberOfUnit(position.getNumberOfUnit());
        positionImpl.setLabelNumber(position.getLabelNumber());
        positionImpl.setPoLabelNumber(position.getPoLabelNumber());
        positionImpl.setTargaTecnica(position.getTargaTecnica());
        positionImpl.setApproved(position.isApproved());
        positionImpl.setIcApproval(position.getIcApproval());
        positionImpl.setApprover(position.getApprover());
        positionImpl.setAssetNumber(position.getAssetNumber());
        positionImpl.setShoppingCart(position.getShoppingCart());
        positionImpl.setPoNumber(position.getPoNumber());
        positionImpl.setEvoLocation(position.getEvoLocation());
        positionImpl.setWbsCodeId(position.getWbsCodeId());
        positionImpl.setPurchaseRequestId(position.getPurchaseRequestId());
        positionImpl.setPurchaseOrderId(position.getPurchaseOrderId());
        positionImpl.setCreatedDate(position.getCreatedDate());
        positionImpl.setCreatedUserId(position.getCreatedUserId());

        return positionImpl;
    }

    /**
     * Returns the position with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the position
     * @return the position
     * @throws com.liferay.portal.NoSuchModelException if a position with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Position findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the position with the primary key or throws a {@link com.hp.omp.NoSuchPositionException} if it could not be found.
     *
     * @param positionId the primary key of the position
     * @return the position
     * @throws com.hp.omp.NoSuchPositionException if a position with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public Position findByPrimaryKey(long positionId)
        throws NoSuchPositionException, SystemException {
        Position position = fetchByPrimaryKey(positionId);

        if (position == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + positionId);
            }

            throw new NoSuchPositionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                positionId);
        }

        return position;
    }

    /**
     * Returns the position with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the position
     * @return the position, or <code>null</code> if a position with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Position fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the position with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param positionId the primary key of the position
     * @return the position, or <code>null</code> if a position with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public Position fetchByPrimaryKey(long positionId)
        throws SystemException {
        Position position = (Position) EntityCacheUtil.getResult(PositionModelImpl.ENTITY_CACHE_ENABLED,
                PositionImpl.class, positionId);

        if (position == _nullPosition) {
            return null;
        }

        if (position == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                position = (Position) session.get(PositionImpl.class,
                        Long.valueOf(positionId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (position != null) {
                    cacheResult(position);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(PositionModelImpl.ENTITY_CACHE_ENABLED,
                        PositionImpl.class, positionId, _nullPosition);
                }

                closeSession(session);
            }
        }

        return position;
    }

    /**
     * Returns all the positions where purchaseOrderId = &#63;.
     *
     * @param purchaseOrderId the purchase order ID
     * @return the matching positions
     * @throws SystemException if a system exception occurred
     */
    public List<Position> findByPurchaseOrderPositions(String purchaseOrderId)
        throws SystemException {
        return findByPurchaseOrderPositions(purchaseOrderId, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the positions where purchaseOrderId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param purchaseOrderId the purchase order ID
     * @param start the lower bound of the range of positions
     * @param end the upper bound of the range of positions (not inclusive)
     * @return the range of matching positions
     * @throws SystemException if a system exception occurred
     */
    public List<Position> findByPurchaseOrderPositions(String purchaseOrderId,
        int start, int end) throws SystemException {
        return findByPurchaseOrderPositions(purchaseOrderId, start, end, null);
    }

    /**
     * Returns an ordered range of all the positions where purchaseOrderId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param purchaseOrderId the purchase order ID
     * @param start the lower bound of the range of positions
     * @param end the upper bound of the range of positions (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching positions
     * @throws SystemException if a system exception occurred
     */
    public List<Position> findByPurchaseOrderPositions(String purchaseOrderId,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PURCHASEORDERPOSITIONS;
            finderArgs = new Object[] { purchaseOrderId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PURCHASEORDERPOSITIONS;
            finderArgs = new Object[] {
                    purchaseOrderId,
                    
                    start, end, orderByComparator
                };
        }

        List<Position> list = (List<Position>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Position position : list) {
                if (!Validator.equals(purchaseOrderId,
                            position.getPurchaseOrderId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(2);
            }

            query.append(_SQL_SELECT_POSITION_WHERE);

            if (purchaseOrderId == null) {
                query.append(_FINDER_COLUMN_PURCHASEORDERPOSITIONS_PURCHASEORDERID_1);
            } else {
                if (purchaseOrderId.equals(StringPool.BLANK)) {
                    query.append(_FINDER_COLUMN_PURCHASEORDERPOSITIONS_PURCHASEORDERID_3);
                } else {
                    query.append(_FINDER_COLUMN_PURCHASEORDERPOSITIONS_PURCHASEORDERID_2);
                }
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (purchaseOrderId != null) {
                    qPos.add(purchaseOrderId);
                }

                list = (List<Position>) QueryUtil.list(q, getDialect(), start,
                        end);
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first position in the ordered set where purchaseOrderId = &#63;.
     *
     * @param purchaseOrderId the purchase order ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching position
     * @throws com.hp.omp.NoSuchPositionException if a matching position could not be found
     * @throws SystemException if a system exception occurred
     */
    public Position findByPurchaseOrderPositions_First(String purchaseOrderId,
        OrderByComparator orderByComparator)
        throws NoSuchPositionException, SystemException {
        Position position = fetchByPurchaseOrderPositions_First(purchaseOrderId,
                orderByComparator);

        if (position != null) {
            return position;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("purchaseOrderId=");
        msg.append(purchaseOrderId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPositionException(msg.toString());
    }

    /**
     * Returns the first position in the ordered set where purchaseOrderId = &#63;.
     *
     * @param purchaseOrderId the purchase order ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching position, or <code>null</code> if a matching position could not be found
     * @throws SystemException if a system exception occurred
     */
    public Position fetchByPurchaseOrderPositions_First(
        String purchaseOrderId, OrderByComparator orderByComparator)
        throws SystemException {
        List<Position> list = findByPurchaseOrderPositions(purchaseOrderId, 0,
                1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last position in the ordered set where purchaseOrderId = &#63;.
     *
     * @param purchaseOrderId the purchase order ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching position
     * @throws com.hp.omp.NoSuchPositionException if a matching position could not be found
     * @throws SystemException if a system exception occurred
     */
    public Position findByPurchaseOrderPositions_Last(String purchaseOrderId,
        OrderByComparator orderByComparator)
        throws NoSuchPositionException, SystemException {
        Position position = fetchByPurchaseOrderPositions_Last(purchaseOrderId,
                orderByComparator);

        if (position != null) {
            return position;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("purchaseOrderId=");
        msg.append(purchaseOrderId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPositionException(msg.toString());
    }

    /**
     * Returns the last position in the ordered set where purchaseOrderId = &#63;.
     *
     * @param purchaseOrderId the purchase order ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching position, or <code>null</code> if a matching position could not be found
     * @throws SystemException if a system exception occurred
     */
    public Position fetchByPurchaseOrderPositions_Last(String purchaseOrderId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByPurchaseOrderPositions(purchaseOrderId);

        List<Position> list = findByPurchaseOrderPositions(purchaseOrderId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the positions before and after the current position in the ordered set where purchaseOrderId = &#63;.
     *
     * @param positionId the primary key of the current position
     * @param purchaseOrderId the purchase order ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next position
     * @throws com.hp.omp.NoSuchPositionException if a position with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public Position[] findByPurchaseOrderPositions_PrevAndNext(
        long positionId, String purchaseOrderId,
        OrderByComparator orderByComparator)
        throws NoSuchPositionException, SystemException {
        Position position = findByPrimaryKey(positionId);

        Session session = null;

        try {
            session = openSession();

            Position[] array = new PositionImpl[3];

            array[0] = getByPurchaseOrderPositions_PrevAndNext(session,
                    position, purchaseOrderId, orderByComparator, true);

            array[1] = position;

            array[2] = getByPurchaseOrderPositions_PrevAndNext(session,
                    position, purchaseOrderId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Position getByPurchaseOrderPositions_PrevAndNext(
        Session session, Position position, String purchaseOrderId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_POSITION_WHERE);

        if (purchaseOrderId == null) {
            query.append(_FINDER_COLUMN_PURCHASEORDERPOSITIONS_PURCHASEORDERID_1);
        } else {
            if (purchaseOrderId.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_PURCHASEORDERPOSITIONS_PURCHASEORDERID_3);
            } else {
                query.append(_FINDER_COLUMN_PURCHASEORDERPOSITIONS_PURCHASEORDERID_2);
            }
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (purchaseOrderId != null) {
            qPos.add(purchaseOrderId);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(position);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Position> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Returns all the positions where purchaseRequestId = &#63;.
     *
     * @param purchaseRequestId the purchase request ID
     * @return the matching positions
     * @throws SystemException if a system exception occurred
     */
    public List<Position> findByPurchaseRequestPositions(
        String purchaseRequestId) throws SystemException {
        return findByPurchaseRequestPositions(purchaseRequestId,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the positions where purchaseRequestId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param purchaseRequestId the purchase request ID
     * @param start the lower bound of the range of positions
     * @param end the upper bound of the range of positions (not inclusive)
     * @return the range of matching positions
     * @throws SystemException if a system exception occurred
     */
    public List<Position> findByPurchaseRequestPositions(
        String purchaseRequestId, int start, int end) throws SystemException {
        return findByPurchaseRequestPositions(purchaseRequestId, start, end,
            null);
    }

    /**
     * Returns an ordered range of all the positions where purchaseRequestId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param purchaseRequestId the purchase request ID
     * @param start the lower bound of the range of positions
     * @param end the upper bound of the range of positions (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching positions
     * @throws SystemException if a system exception occurred
     */
    public List<Position> findByPurchaseRequestPositions(
        String purchaseRequestId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PURCHASEREQUESTPOSITIONS;
            finderArgs = new Object[] { purchaseRequestId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PURCHASEREQUESTPOSITIONS;
            finderArgs = new Object[] {
                    purchaseRequestId,
                    
                    start, end, orderByComparator
                };
        }

        List<Position> list = (List<Position>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Position position : list) {
                if (!Validator.equals(purchaseRequestId,
                            position.getPurchaseRequestId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(2);
            }

            query.append(_SQL_SELECT_POSITION_WHERE);

            if (purchaseRequestId == null) {
                query.append(_FINDER_COLUMN_PURCHASEREQUESTPOSITIONS_PURCHASEREQUESTID_1);
            } else {
                if (purchaseRequestId.equals(StringPool.BLANK)) {
                    query.append(_FINDER_COLUMN_PURCHASEREQUESTPOSITIONS_PURCHASEREQUESTID_3);
                } else {
                    query.append(_FINDER_COLUMN_PURCHASEREQUESTPOSITIONS_PURCHASEREQUESTID_2);
                }
            }

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (purchaseRequestId != null) {
                    qPos.add(purchaseRequestId);
                }

                list = (List<Position>) QueryUtil.list(q, getDialect(), start,
                        end);
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first position in the ordered set where purchaseRequestId = &#63;.
     *
     * @param purchaseRequestId the purchase request ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching position
     * @throws com.hp.omp.NoSuchPositionException if a matching position could not be found
     * @throws SystemException if a system exception occurred
     */
    public Position findByPurchaseRequestPositions_First(
        String purchaseRequestId, OrderByComparator orderByComparator)
        throws NoSuchPositionException, SystemException {
        Position position = fetchByPurchaseRequestPositions_First(purchaseRequestId,
                orderByComparator);

        if (position != null) {
            return position;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("purchaseRequestId=");
        msg.append(purchaseRequestId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPositionException(msg.toString());
    }

    /**
     * Returns the first position in the ordered set where purchaseRequestId = &#63;.
     *
     * @param purchaseRequestId the purchase request ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching position, or <code>null</code> if a matching position could not be found
     * @throws SystemException if a system exception occurred
     */
    public Position fetchByPurchaseRequestPositions_First(
        String purchaseRequestId, OrderByComparator orderByComparator)
        throws SystemException {
        List<Position> list = findByPurchaseRequestPositions(purchaseRequestId,
                0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last position in the ordered set where purchaseRequestId = &#63;.
     *
     * @param purchaseRequestId the purchase request ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching position
     * @throws com.hp.omp.NoSuchPositionException if a matching position could not be found
     * @throws SystemException if a system exception occurred
     */
    public Position findByPurchaseRequestPositions_Last(
        String purchaseRequestId, OrderByComparator orderByComparator)
        throws NoSuchPositionException, SystemException {
        Position position = fetchByPurchaseRequestPositions_Last(purchaseRequestId,
                orderByComparator);

        if (position != null) {
            return position;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("purchaseRequestId=");
        msg.append(purchaseRequestId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPositionException(msg.toString());
    }

    /**
     * Returns the last position in the ordered set where purchaseRequestId = &#63;.
     *
     * @param purchaseRequestId the purchase request ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching position, or <code>null</code> if a matching position could not be found
     * @throws SystemException if a system exception occurred
     */
    public Position fetchByPurchaseRequestPositions_Last(
        String purchaseRequestId, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByPurchaseRequestPositions(purchaseRequestId);

        List<Position> list = findByPurchaseRequestPositions(purchaseRequestId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the positions before and after the current position in the ordered set where purchaseRequestId = &#63;.
     *
     * @param positionId the primary key of the current position
     * @param purchaseRequestId the purchase request ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next position
     * @throws com.hp.omp.NoSuchPositionException if a position with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public Position[] findByPurchaseRequestPositions_PrevAndNext(
        long positionId, String purchaseRequestId,
        OrderByComparator orderByComparator)
        throws NoSuchPositionException, SystemException {
        Position position = findByPrimaryKey(positionId);

        Session session = null;

        try {
            session = openSession();

            Position[] array = new PositionImpl[3];

            array[0] = getByPurchaseRequestPositions_PrevAndNext(session,
                    position, purchaseRequestId, orderByComparator, true);

            array[1] = position;

            array[2] = getByPurchaseRequestPositions_PrevAndNext(session,
                    position, purchaseRequestId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Position getByPurchaseRequestPositions_PrevAndNext(
        Session session, Position position, String purchaseRequestId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_POSITION_WHERE);

        if (purchaseRequestId == null) {
            query.append(_FINDER_COLUMN_PURCHASEREQUESTPOSITIONS_PURCHASEREQUESTID_1);
        } else {
            if (purchaseRequestId.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_PURCHASEREQUESTPOSITIONS_PURCHASEREQUESTID_3);
            } else {
                query.append(_FINDER_COLUMN_PURCHASEREQUESTPOSITIONS_PURCHASEREQUESTID_2);
            }
        }

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        if (purchaseRequestId != null) {
            qPos.add(purchaseRequestId);
        }

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(position);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Position> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Returns all the positions.
     *
     * @return the positions
     * @throws SystemException if a system exception occurred
     */
    public List<Position> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the positions.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of positions
     * @param end the upper bound of the range of positions (not inclusive)
     * @return the range of positions
     * @throws SystemException if a system exception occurred
     */
    public List<Position> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the positions.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of positions
     * @param end the upper bound of the range of positions (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of positions
     * @throws SystemException if a system exception occurred
     */
    public List<Position> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Position> list = (List<Position>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_POSITION);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_POSITION;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<Position>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<Position>) QueryUtil.list(q, getDialect(),
                            start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the positions where purchaseOrderId = &#63; from the database.
     *
     * @param purchaseOrderId the purchase order ID
     * @throws SystemException if a system exception occurred
     */
    public void removeByPurchaseOrderPositions(String purchaseOrderId)
        throws SystemException {
        for (Position position : findByPurchaseOrderPositions(purchaseOrderId)) {
            remove(position);
        }
    }

    /**
     * Removes all the positions where purchaseRequestId = &#63; from the database.
     *
     * @param purchaseRequestId the purchase request ID
     * @throws SystemException if a system exception occurred
     */
    public void removeByPurchaseRequestPositions(String purchaseRequestId)
        throws SystemException {
        for (Position position : findByPurchaseRequestPositions(
                purchaseRequestId)) {
            remove(position);
        }
    }

    /**
     * Removes all the positions from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (Position position : findAll()) {
            remove(position);
        }
    }

    /**
     * Returns the number of positions where purchaseOrderId = &#63;.
     *
     * @param purchaseOrderId the purchase order ID
     * @return the number of matching positions
     * @throws SystemException if a system exception occurred
     */
    public int countByPurchaseOrderPositions(String purchaseOrderId)
        throws SystemException {
        Object[] finderArgs = new Object[] { purchaseOrderId };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_PURCHASEORDERPOSITIONS,
                finderArgs, this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_POSITION_WHERE);

            if (purchaseOrderId == null) {
                query.append(_FINDER_COLUMN_PURCHASEORDERPOSITIONS_PURCHASEORDERID_1);
            } else {
                if (purchaseOrderId.equals(StringPool.BLANK)) {
                    query.append(_FINDER_COLUMN_PURCHASEORDERPOSITIONS_PURCHASEORDERID_3);
                } else {
                    query.append(_FINDER_COLUMN_PURCHASEORDERPOSITIONS_PURCHASEORDERID_2);
                }
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (purchaseOrderId != null) {
                    qPos.add(purchaseOrderId);
                }

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PURCHASEORDERPOSITIONS,
                    finderArgs, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the number of positions where purchaseRequestId = &#63;.
     *
     * @param purchaseRequestId the purchase request ID
     * @return the number of matching positions
     * @throws SystemException if a system exception occurred
     */
    public int countByPurchaseRequestPositions(String purchaseRequestId)
        throws SystemException {
        Object[] finderArgs = new Object[] { purchaseRequestId };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_PURCHASEREQUESTPOSITIONS,
                finderArgs, this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_POSITION_WHERE);

            if (purchaseRequestId == null) {
                query.append(_FINDER_COLUMN_PURCHASEREQUESTPOSITIONS_PURCHASEREQUESTID_1);
            } else {
                if (purchaseRequestId.equals(StringPool.BLANK)) {
                    query.append(_FINDER_COLUMN_PURCHASEREQUESTPOSITIONS_PURCHASEREQUESTID_3);
                } else {
                    query.append(_FINDER_COLUMN_PURCHASEREQUESTPOSITIONS_PURCHASEREQUESTID_2);
                }
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (purchaseRequestId != null) {
                    qPos.add(purchaseRequestId);
                }

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PURCHASEREQUESTPOSITIONS,
                    finderArgs, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the number of positions.
     *
     * @return the number of positions
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_POSITION);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the position persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.Position")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Position>> listenersList = new ArrayList<ModelListener<Position>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<Position>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(PositionImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
