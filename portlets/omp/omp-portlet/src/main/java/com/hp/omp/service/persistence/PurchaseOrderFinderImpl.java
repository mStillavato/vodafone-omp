package com.hp.omp.service.persistence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.custom.PoListDTO;
import com.hp.omp.model.custom.PurchaseOrderStatus;
import com.hp.omp.model.custom.SearchDTO;
import com.hp.omp.model.custom.TipoPr;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class PurchaseOrderFinderImpl extends PurchaseOrderPersistenceImpl implements PurchaseOrderFinder{

	private static final Logger _log = LoggerFactory.getLogger(PurchaseOrderFinderImpl.class);

	public long getPurchaseOrdersGRRequestedCount(SearchDTO searchDto){
		
		_log.info("public long getPurchaseOrdersGRRequestedCount() - Start");
		
		Session session = null;

		Long count = 0L;
		try {
			session = openSession();
			Query query;

			if(searchDto.getPoStatus() != null){
				String hql;
				if (PurchaseOrderStatus.GR_RECEIVED.getCode() == searchDto.getPoStatus()) {
					hql = CustomSQLUtil.get(GET_PO_GR_REQUESTED_COUNT);
				} else {
					hql = CustomSQLUtil.get(GET_PO_COUNT_BYSTATUS);
				}

				if(!OmpRoles.OMP_CONTROLLER.getLabel().equalsIgnoreCase(searchDto.getUserRole())
						&& !OmpRoles.OMP_CONTROLLER_VBS.getLabel().equalsIgnoreCase(searchDto.getUserRole())){
					if(searchDto.getPositionICApproval() != null && !"".equals(searchDto.getPositionICApproval())){
						hql+= AND + CustomSQLUtil.get(GENERATE_IC_APPROVAL_CLAUSE);
					} else {
						hql+= AND + CustomSQLUtil.get(GENERATE_IC_APPROVAL_EMPTY_CLAUSE);
					}
				}

				if(searchDto.getTipoPr() != null && searchDto.getTipoPr() == TipoPr.VBS.getCode()) {
					hql+= " and po.tipoPr = 3 ";
				} else{
					hql+= " and po.tipoPr != 3 ";
				}

				if(searchDto.getCostCenterId() != null){
					hql+= AND + CustomSQLUtil.get(GENERATE_SEARCH_CLAUSE_COST_CENTER_ID);

					query = session.createQuery(hql);	
					QueryPos queryPos = QueryPos.getInstance(query);

					if(PurchaseOrderStatus.GR_RECEIVED.getCode() == searchDto.getPoStatus()){
						queryPos.add(PurchaseOrderStatus.PO_CLOSED.getCode());
					}

					if(PurchaseOrderStatus.PO_RECEIVED.getCode() == searchDto.getPoStatus()){
						queryPos.add(searchDto.getPoStatus());
					}

					if(searchDto.getPositionICApproval() != null && !"".equals(searchDto.getPositionICApproval())){
						queryPos.add(searchDto.getPositionICApproval());
					}
					queryPos.add(searchDto.getCostCenterId());

				} else{
					query = session.createQuery(hql);
					QueryPos queryPos = QueryPos.getInstance(query);

					if(PurchaseOrderStatus.GR_RECEIVED.getCode() == searchDto.getPoStatus()){
						queryPos.add(PurchaseOrderStatus.PO_CLOSED.getCode());
					}

					if(PurchaseOrderStatus.PO_RECEIVED.getCode() == searchDto.getPoStatus()){
						queryPos.add(searchDto.getPoStatus());
					}
					if(searchDto.getPositionICApproval() != null){
						queryPos.add(searchDto.getPositionICApproval());
					}
				}
				//Receiver
			} else {
				String hql = "select count(distinct po)"
						+" from PurchaseOrder po, "
						+" Position position, "
						+" GoodReceipt gr, "
						+" Vendor v "
						+" where po.purchaseOrderId = position.purchaseOrderId "
						+" and gr.positionId = position.positionId "
						+" and po.vendorId = v.vendorId ";

				if(OmpRoles.OMP_ANTEX.getLabel().equalsIgnoreCase(searchDto.getUserRole())){
					// hql +=" and po.status in  ("+PurchaseOrderStatus.PO_RECEIVED.getCode() +","+PurchaseOrderStatus.GR_RECEIVED.getCode() +")";

					hql += " and (gr.registerationDate is null or gr.goodReceiptNumber is null) ";
					//hql += " and po.status != '5' ";
				}

				if(searchDto.getTipoPr() != null && searchDto.getTipoPr() == TipoPr.VBS.getCode()){
					hql+= " and po.tipoPr = 3 ";
				} else{
					hql+= " and po.tipoPr != 3 ";
				}

				query = session.createQuery(hql);
			}


			@SuppressWarnings("unchecked")
			List<Object> list= (List<Object>) QueryUtil.list(query,
					getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			count = (Long) list.get(0);

		} catch (Exception e) {
			_log.error(e.getMessage());
		} finally {
			closeSession(session);
		}
		
		_log.info("public long getPurchaseOrdersGRRequestedCount() - End");
		
		return count;
	}


	public long getPurchaseOrdersGRClosedCount(SearchDTO searchDto){
		
		_log.info("public long getPurchaseOrdersGRClosedCount() - Start");
		
		Session session = null;

		Long count = 0L;
		try {
			session = openSession();
			Query query;

			String hql = CustomSQLUtil.get(GET_PO_COUNT_BYSTATUS);

			if(searchDto.getTipoPr() != null && searchDto.getTipoPr() == TipoPr.VBS.getCode()){
				hql+= " and po.tipoPr = 3 ";
			} else{
				hql+= " and po.tipoPr != 3 ";
			}

			query = session.createQuery(hql);
			QueryPos queryPos = QueryPos.getInstance(query);
			queryPos.add(PurchaseOrderStatus.PO_CLOSED.getCode());

			@SuppressWarnings("unchecked")
			List<Object> list= (List<Object>) QueryUtil.list(query,
					getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			count = (Long) list.get(0);

		} catch (Exception e) {
			_log.error(e.getMessage());
		} finally {
			closeSession(session);
			_log.info("public long getPurchaseOrdersGRClosedCount() - End");
		}
		
		return count;
	}


	@SuppressWarnings("unchecked")
	public List<PoListDTO> getPurchaseOrdersGRRequested(SearchDTO searchDto){
		
		_log.info("public List<PoListDTO> getPurchaseOrdersGRRequested() - Start");
		
		Session session = null;
		List<PoListDTO> polist=new ArrayList<PoListDTO>();
		session = openSession();

		//List<PurchaseOrder> list = new ArrayList<PurchaseOrder>();
		try {
			Query query;

			if(searchDto.getPoStatus() != null){
				String hql;
				if (PurchaseOrderStatus.GR_RECEIVED.getCode() == searchDto.getPoStatus()) {
					hql = CustomSQLUtil.get(GET_PO_GR_REQUESTED);
				} else {
					hql = CustomSQLUtil.get(GET_PO_BY_STATUS);
				}

				if(!OmpRoles.OMP_CONTROLLER.getLabel().equalsIgnoreCase(searchDto.getUserRole())
						&& !OmpRoles.OMP_CONTROLLER_VBS.getLabel().equalsIgnoreCase(searchDto.getUserRole())){

					if(searchDto.getPositionICApproval() != null && !"".equals(searchDto.getPositionICApproval())){
						hql+= AND + CustomSQLUtil.get(GENERATE_IC_APPROVAL_CLAUSE);

					} else {
						hql+= AND + CustomSQLUtil.get(GENERATE_IC_APPROVAL_EMPTY_CLAUSE);
					}
				}

				if(searchDto.getCostCenterId() != null){
					hql += AND + CustomSQLUtil.get(GENERATE_SEARCH_CLAUSE_COST_CENTER_ID);
					
					if(searchDto.getTipoPr() != null && searchDto.getTipoPr() == TipoPr.VBS.getCode()){
						hql += " and po.tipoPr = 3 ";
					} else{
						hql += " and po.tipoPr != 3 ";
					}

					hql += " "+ getOrderByClause(searchDto.getOrderby());
					hql += " "+getOrderClasue(searchDto.getOrder());

					query = session.createQuery(hql);	
					QueryPos queryPos = QueryPos.getInstance(query);

					if (PurchaseOrderStatus.GR_RECEIVED.getCode() == searchDto.getPoStatus()) {
						queryPos.add(PurchaseOrderStatus.PO_CLOSED.getCode());
					}


					if(PurchaseOrderStatus.PO_RECEIVED.getCode() == searchDto.getPoStatus()){
						queryPos.add(searchDto.getPoStatus());
					}

					if(searchDto.getPositionICApproval() != null && !"".equals(searchDto.getPositionICApproval())){
						queryPos.add(searchDto.getPositionICApproval());
					}
					queryPos.add(searchDto.getCostCenterId());

				} else{
					
					if(searchDto.getTipoPr() != null && searchDto.getTipoPr() == TipoPr.VBS.getCode()){
						hql += " and po.tipoPr = 3 ";
					} else{
						hql += " and po.tipoPr != 3 ";
					}

					hql += " "+ getOrderByClause(searchDto.getOrderby());
					hql += " "+getOrderClasue(searchDto.getOrder());

					query = session.createQuery(hql);
					QueryPos queryPos = QueryPos.getInstance(query);

					if (PurchaseOrderStatus.GR_RECEIVED.getCode() == searchDto.getPoStatus()) {
						queryPos.add(PurchaseOrderStatus.PO_CLOSED.getCode());
					}

					if(PurchaseOrderStatus.PO_RECEIVED.getCode() == searchDto.getPoStatus()){
						queryPos.add(searchDto.getPoStatus());
					}
					if(searchDto.getPositionICApproval() != null){
						queryPos.add(searchDto.getPositionICApproval());
					}
				}

			} else {
				String hql = "select distinct po.purchaseOrderId,po.screenNameRequester,po.screenNameReciever,v.vendorName,po.totalValue,po.fiscalYear,po.activityDescription,po.automatic,po.status,po.currency, po.tipoPr "
						+" from PurchaseOrder po, "
						+" Position position, "
						+" GoodReceipt gr, "
						+" Vendor v "
						+" where po.purchaseOrderId = position.purchaseOrderId "
						+" and gr.positionId = position.positionId "
						+" and po.vendorId = v.vendorId ";

				if(OmpRoles.OMP_ANTEX.getLabel().equalsIgnoreCase(searchDto.getUserRole())){
					//hql +=" and po.status in  ("+PurchaseOrderStatus.PO_RECEIVED.getCode() +","+PurchaseOrderStatus.GR_RECEIVED.getCode() +")";
					hql += " and (gr.registerationDate is null or gr.goodReceiptNumber is null) ";
					//	hql += " and po.status != '5' ";
				}

				if(!OmpRoles.OMP_ANTEX.getLabel().equalsIgnoreCase(searchDto.getUserRole())){
					if(searchDto.getTipoPr() != null && searchDto.getTipoPr() == TipoPr.VBS.getCode()){
						hql += " and po.tipoPr = 3 ";
					} else{
						hql += " and po.tipoPr != 3 ";
					}
				}
				
				hql += " "+ getOrderByClause(searchDto.getOrderby());
				hql += " "+getOrderClasue(searchDto.getOrder());

				query = session.createQuery(hql);
			}

			List<Object[]> list = (List<Object[]>)QueryUtil.list(query,getDialect(), searchDto.getStart(), searchDto.getEnd());

			for(Object[] row:list) {
				PoListDTO poListDTO=new PoListDTO();

				poListDTO.setPurchaseOrderId(Long.valueOf(row[0].toString()));
				poListDTO.setScreenNameRequester(""+row[1]);
				poListDTO.setScreenNameReciever(""+row[2]);
				poListDTO.setVendorName(row[3]+"");
				poListDTO.setTotalValue(row[4]+"");
				poListDTO.setFiscalYear(row[5]+"");
				poListDTO.setActivityDescription(row[6]+"");
				poListDTO.setAutomatic(Boolean.valueOf(row[7].toString()));
				poListDTO.setStatus(Integer.valueOf(row[8].toString()));
				poListDTO.setCurrency(row[9]+"");
				poListDTO.setTipoPr(Integer.valueOf(row[10] + ""));

				polist.add(poListDTO);

			}
		} catch (Exception e) {
			_log.error(e.getMessage());
		} finally {
			if(session != null) {
				closeSession(session);
				_log.info("public List<PoListDTO> getPurchaseOrdersGRRequested() - End");
			}
		}
		return polist;
	}


	@SuppressWarnings("unchecked")
	public List<PoListDTO> getPurchaseOrdersGRClosed(SearchDTO searchDto){
		
		_log.info("public List<PoListDTO> getPurchaseOrdersGRClosed() - Start");
		
		Session session = null;
		List<PoListDTO> polist=new ArrayList<PoListDTO>();
		session = openSession();

		try {
			Query query;

			String hql = CustomSQLUtil.get(GET_PO_BY_STATUS);

			if(searchDto.getTipoPr() != null && searchDto.getTipoPr() == TipoPr.VBS.getCode()){
				hql+= " and po.tipoPr = 3 ";
			} else{
				hql+= " and po.tipoPr != 3 ";
			}

			hql += " "+ getOrderByClause(searchDto.getOrderby());
			hql += " "+getOrderClasue(searchDto.getOrder());
			query = session.createQuery(hql);

			QueryPos queryPos = QueryPos.getInstance(query);
			queryPos.add(PurchaseOrderStatus.PO_CLOSED.getCode());

			List<Object[]> list = (List<Object[]>)QueryUtil.list(query,getDialect(), searchDto.getStart(), searchDto.getEnd());

			for(Object[] row:list) {
				PoListDTO poListDTO=new PoListDTO();

				poListDTO.setPurchaseOrderId(Long.valueOf(row[0].toString()));
				poListDTO.setScreenNameRequester(""+row[1]);
				poListDTO.setScreenNameReciever(""+row[2]);
				poListDTO.setVendorName(row[3]+"");
				poListDTO.setTotalValue(row[4]+"");
				poListDTO.setFiscalYear(row[5]+"");
				poListDTO.setActivityDescription(row[6]+"");
				poListDTO.setAutomatic(Boolean.valueOf(row[7].toString()));
				poListDTO.setStatus(Integer.valueOf(row[8].toString()));
				poListDTO.setCurrency(row[9]+"");
				poListDTO.setTipoPr(Integer.valueOf(row[10] + ""));

				polist.add(poListDTO);

			}
		} catch (Exception e) {
			_log.error(e.getMessage());
		} finally {
			if(session != null) {
				closeSession(session);
				_log.info("public List<PoListDTO> getPurchaseOrdersGRClosed() - End");
			}
		}
		return polist;
	}

	
	private String getOrderByClause(String orderby) {
		
		_log.info("private String getOrderByClause() - Start");
		
		if(orderby == null || "".equals(orderby)){
			return CustomSQLUtil.get(ORDER_BY_PURCHASE_ORDER_ID);
		}

		if("purchaseOrderId".equals(orderby)){
			_log.info("private String getOrderByClause() - End ORDER_BY_PURCHASE_ORDER_ID");
			return CustomSQLUtil.get(ORDER_BY_PURCHASE_ORDER_ID);
			
		}else if("screenNameReciever".equals(orderby)){
			_log.info("private String getOrderByClause() - End ORDER_BY_RECEIVER_ID");
			return CustomSQLUtil.get(ORDER_BY_RECEIVER_ID);
			
		}else if("screenNameRequester".equals(orderby)){
			_log.info("private String getOrderByClause() - End ORDER_BY_CREATOR_ID");
			return CustomSQLUtil.get(ORDER_BY_CREATOR_ID);
			
		}else if("vendorName".equals(orderby)){
			_log.info("private String getOrderByClause() - End ORDER_BY_VENDOR");
			return CustomSQLUtil.get(ORDER_BY_VENDOR);
			
		}else if("totalValue".equals(orderby)){
			_log.info("private String getOrderByClause() - End ORDER_BY_TOTAL_VALUE");
			return CustomSQLUtil.get(ORDER_BY_TOTAL_VALUE);
			
		}else if("fiscalYear".equals(orderby)){
			_log.info("private String getOrderByClause() - End ORDER_BY_FISCAL_YEAR");
			return CustomSQLUtil.get(ORDER_BY_FISCAL_YEAR);
			
		}else if("activityDescription".equals(orderby)){
			_log.info("private String getOrderByClause() - End ORDER_BY_DESCRIPTION");
			return CustomSQLUtil.get(ORDER_BY_DESCRIPTION);
		}else{
			_log.info("private String getOrderByClause() - End ORDER_BY_PURCHASE_ORDER_ID");
			return " "+CustomSQLUtil.get(ORDER_BY_PURCHASE_ORDER_ID);
		}
	}


	private String getOrderClasue(String order) {
		if(order == null || "".equals(order)){
			return CustomSQLUtil.get(ORDER_DESC);
		}

		if("asc".equals(order)){
			return CustomSQLUtil.get(ORDER_ASC);
		}else{
			return CustomSQLUtil.get(ORDER_DESC);
		}
	}

	public List<Long> getPONumbers() throws SystemException{
		return getAllNumberValues(GET_PO_NUMBERS);
	}


	public List<String> getAllPOProjects() throws SystemException{
		return getAllStringValues(GET_ALL_PROJECTNAMES);
	}

	private List<String> getAllStringValues(String sqlID) throws SystemException{
		
		_log.info("private List<String> getAllStringValues() - Start");
		
		List<String> allValues = new ArrayList<String>();
		String value = null;
		Session session = null;
		try {
			session = openSession();
			String hql = CustomSQLUtil.get(sqlID);
			Query query = session.createQuery(hql);
			@SuppressWarnings("unchecked")
			Iterator<String> iterate = (Iterator<String>)QueryUtil.iterate(query, getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			while(iterate.hasNext()){
				value = iterate.next();
				allValues.add(value);
			}
			return allValues;
		} catch (ORMException e) {
			_log.error(e.getMessage(), e);
			throw new SystemException(e);
		} finally{
			closeSession(session);
			_log.info("private List<String> getAllStringValues() - End");
		}
	}

	private List<Long> getAllNumberValues(String sqlID) throws SystemException{
		
		_log.info("private List<Long> getAllNumberValues() - Start");
		
		List<Long> allValues = new ArrayList<Long>();
		Long value = null;
		Session session = null;
		try {
			session = openSession();
			String hql = CustomSQLUtil.get(sqlID);
			Query query = session.createQuery(hql);
			@SuppressWarnings("unchecked")
			Iterator<Long> iterate = (Iterator<Long>)QueryUtil.iterate(query, getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			while(iterate.hasNext()){
				value = iterate.next();
				allValues.add(value);
			}
			return allValues;
		} catch (ORMException e) {
			_log.error(e.getMessage(), e);
			throw new SystemException(e);
		} finally{
			closeSession(session);
			_log.info("private List<Long> getAllNumberValues() - End");
		}
	}

	public List<String> getSubProjects(String projectName) throws SystemException{
		List<String> subProjectNames = new ArrayList<String>();
		String subProjectName = null;
		Session session = null;
		try {
			session = openSession();
			String hql = CustomSQLUtil.get(GET_SUBPROJECTS);
			Query query = session.createQuery(hql);			
			QueryPos queryPos = QueryPos.getInstance(query);
			queryPos.add(projectName);
			Iterator<String> iterate = (Iterator<String>)QueryUtil.iterate(query, getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			while(iterate.hasNext()){
				subProjectName = iterate.next();
				subProjectNames.add(subProjectName);
			}
			if(subProjectNames.isEmpty()){
				return null;
			}
			return subProjectNames;
		} catch (ORMException e) {
			_log.error(e.getMessage(), e);
			throw new SystemException(e);
		} finally{
			closeSession(session);
		}
	}

	@SuppressWarnings("unchecked")
	public List<PoListDTO> getAdvancedSearchResults(SearchDTO searchDTO, int start, int end, String orderby, String order)	throws SystemException {
		Session session = null;
		List<PoListDTO> polist = new ArrayList<PoListDTO>();

		try {
			session = openSession();

			SQLQuery query = null;

			if(searchDTO.isMiniSeach()){
				query = getAdvancedSearchOrClauseSql(searchDTO, CustomSQLUtil.get(GENERATE_SEARCH_BASE), session, orderby, order, true);
			}else{
				query = getAdvancedSearchAndClauseSql(searchDTO, CustomSQLUtil.get(GENERATE_SEARCH_BASE), session, orderby, order, true);
			}

			if(query == null){
				return null;
			}



			List<Object[]> list = (List<Object[]>)QueryUtil.list(query,getDialect(), start, end);

			for(Object[] row:list) {
				PoListDTO poListDTO=new PoListDTO();

				poListDTO.setPurchaseOrderId(Long.valueOf(row[0].toString()));
				poListDTO.setScreenNameRequester(""+row[1]);
				poListDTO.setScreenNameReciever(""+row[2]);
				poListDTO.setVendorName(row[3]+"");
				poListDTO.setTotalValue(row[4]+"");
				poListDTO.setFiscalYear(row[5]+"");
				poListDTO.setActivityDescription(row[6]+"");
				poListDTO.setAutomatic(Boolean.valueOf(row[7].toString()));
				poListDTO.setStatus(Integer.valueOf(row[8].toString()));
				poListDTO.setCurrency(row[9]+"");
				poListDTO.setTipoPr(Integer.valueOf(row[10] + ""));

				polist.add(poListDTO);

			}
		} catch (ORMException e) {
			throw processException(e);
		} finally{
			closeSession(session);
		}

		return polist;
	}

	@SuppressWarnings("unchecked")
	public Long getAdvancedSearchResultsCount(SearchDTO searchDTO)	throws SystemException {
		Session session = null;
		Long count = 0L;
		try {
			session = openSession();

			SQLQuery query = null;

			if(searchDTO.isMiniSeach()){
				query = getAdvancedSearchOrClauseSql(searchDTO, CustomSQLUtil.get(GENERATE_SEARCH_COUNT_BASE), session , null, null, true);
			}else{
				query = getAdvancedSearchAndClauseSql(searchDTO, CustomSQLUtil.get(GENERATE_SEARCH_COUNT_BASE), session, null, null, true);
			}

			if(query == null){
				return null;
			}

			query.addScalar("COUNT_VALUE", Type.LONG);

			List<Long> list = (List<Long>) QueryUtil.list(query, getDialect(),  QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			if(list!=null){
				count = list.get(0);
			}

		} catch (ORMException e) {
			throw processException(e);
		} finally{
			closeSession(session);
		}

		return count;
	}

	@SuppressWarnings("unchecked")
	public List<PoListDTO> getAdvancedSearchGrRequestedResults(SearchDTO searchDTO, int start, int end, String orderby, String order)	throws SystemException {
		Session session = null;
		List<PoListDTO> polist = new ArrayList<PoListDTO>();

		try {
			session = openSession();
			SQLQuery query = null;
			if(searchDTO.isMiniSeach()){
				query = getAdvancedSearchOrClauseSql(searchDTO, CustomSQLUtil.get(GENERATE_SEARCH_GR_REQUESTED_BASE), session, orderby, order, false);
			}else{
				query = getAdvancedSearchAndClauseSql(searchDTO, CustomSQLUtil.get(GENERATE_SEARCH_GR_REQUESTED_BASE), session, orderby, order, false);
			}

			if(query == null){
				return null;
			}


			List<Object[]> list = (List<Object[]>)QueryUtil.list(query,getDialect(), start, end);

			for(Object[] row:list) {
				PoListDTO poListDTO=new PoListDTO();

				poListDTO.setPurchaseOrderId(Long.valueOf(row[0].toString()));
				poListDTO.setScreenNameRequester(""+row[1]);
				poListDTO.setScreenNameReciever(""+row[2]);
				poListDTO.setVendorName(row[3]+"");
				poListDTO.setTotalValue(row[4]+"");
				poListDTO.setFiscalYear(row[5]+"");
				poListDTO.setActivityDescription(row[6]+"");
				poListDTO.setAutomatic(Boolean.valueOf(row[7].toString()));
				poListDTO.setStatus(Integer.valueOf(row[8].toString()));
				poListDTO.setCurrency(row[9]+"");
				poListDTO.setTipoPr(Integer.valueOf(row[10] + ""));

				polist.add(poListDTO);
			}

		} catch (ORMException e) {
			throw processException(e);
		} finally{
			closeSession(session);
		}

		return polist;
	}

	@SuppressWarnings("unchecked")
	public Long getAdvancedSearchGrRequestedResultsCount(SearchDTO searchDTO)	throws SystemException {
		Session session = null;
		Long count = 0L;
		try {
			session = openSession();
			SQLQuery query = null;
			if(searchDTO.isMiniSeach()){
				query = getAdvancedSearchOrClauseSql(searchDTO, CustomSQLUtil.get(GENERATE_SEARCH_GR_REQUESTED_COUNT_BASE), session, null, null, false);
			}else{
				query = getAdvancedSearchAndClauseSql(searchDTO, CustomSQLUtil.get(GENERATE_SEARCH_GR_REQUESTED_COUNT_BASE), session, null, null, false);
			}

			if(query == null){
				return null;
			}

			query.addScalar("COUNT_VALUE", Type.LONG);

			List<Object> list = (List<Object>) QueryUtil.list(query, getDialect(),  QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			if(list!=null){
				count = (Long) list.get(0);
			}

		} catch (ORMException e) {
			throw processException(e);
		} finally{
			closeSession(session);
		}

		return count;
	}


	private SQLQuery getAdvancedSearchAndClauseSql(SearchDTO searchDTO, String baseSql, Session session, String orderby, String order, boolean addWhere) {

		StringBuilder sql = new StringBuilder(baseSql);

		List<String> parameterList = new ArrayList<String>();

		if(!addWhere)
			parameterList.add(Integer.toString(PurchaseOrderStatus.PO_CLOSED.getCode()));

		Boolean [] firstInput = new Boolean [] {new Boolean(addWhere)};

		if(!OmpRoles.OMP_CONTROLLER.getLabel().equalsIgnoreCase(searchDTO.getUserRole())
				&& !OmpRoles.OMP_CONTROLLER_VBS.getLabel().equalsIgnoreCase(searchDTO.getUserRole())
				&& !OmpRoles.OMP_ANTEX.getLabel().equalsIgnoreCase(searchDTO.getUserRole())){
			if(searchDTO.getPositionICApproval() != null){
				sql.append(getCondition(firstInput));
				sql.append(CustomSQLUtil.get(GENERATE_IC_APPROVAL_CLAUSE));
				parameterList.add(searchDTO.getPositionICApproval());
			} else {
				sql.append(getCondition(firstInput));
				sql.append(CustomSQLUtil.get(GENERATE_IC_APPROVAL_EMPTY_CLAUSE));
			}
		}

		if(searchDTO.getPoNumber() != null){
			sql.append(getCondition(firstInput));
			sql.append(CustomSQLUtil.get(GENERATE_SEARCH_PO_NUMBER));
			parameterList.add(getFormatedValue(searchDTO.getPoNumber()));
		}
		if(searchDTO.getProjectName() != null){
			sql.append(getCondition(firstInput));
			sql.append(CustomSQLUtil.get(GENERATE_SEARCH_CLAUSE_PROJECT_NAME));
			parameterList.add(searchDTO.getProjectName());
		}
		if(searchDTO.getSubProjectName() != null){
			sql.append(getCondition(firstInput));
			sql.append(CustomSQLUtil.get(GENERATE_SEARCH_CLAUSE_SUB_PROJECT_NAME));
			parameterList.add(getFormatedValue(searchDTO.getSubProjectName()));
		}
		if(searchDTO.getCostCenterId() != -1){
			sql.append(getCondition(firstInput));
			sql.append(CustomSQLUtil.get(GENERATE_SEARCH_CLAUSE_COST_CENTER_ID));
			parameterList.add(Long.toString(searchDTO.getCostCenterId()));
		}

		if(searchDTO.getPoStatus() != null && (PurchaseOrderStatus.PO_RECEIVED.getCode() == searchDTO.getPoStatus()
				|| PurchaseOrderStatus.GR_RECEIVED.getCode() == searchDTO.getPoStatus()
				|| PurchaseOrderStatus.PO_CLOSED.getCode() == searchDTO.getPoStatus() )){
			sql.append(getCondition(firstInput));
			sql.append(CustomSQLUtil.get(GENERATE_SEARCH_PO_STATUS));
			parameterList.add(Long.toString(searchDTO.getPoStatus()));
		}
		if(searchDTO.getFiscalYear() != null){
			sql.append(getCondition(firstInput));
			sql.append(CustomSQLUtil.get(GENERATE_SEARCH_CLAUSE_FISCAL_YEAR));
			parameterList.add(getFormatedValue(searchDTO.getFiscalYear()));
		}
		if(searchDTO.getWbsCode() != null){
			sql.append(getCondition(firstInput) );
			sql.append( CustomSQLUtil.get(GENERATE_SEARCH_CLAUSE_WBS));
			parameterList.add(searchDTO.getWbsCode());
		}
		if(OmpRoles.OMP_ANTEX.getLabel().equalsIgnoreCase(searchDTO.getUserRole())) {
			sql.append(getCondition(firstInput));
			sql.append(" (gr.registerationDate is null or gr.goodReceiptNumber is null) ");
		}

		if(searchDTO.getTipoPr() != null && searchDTO.getTipoPr() == TipoPr.VBS.getCode()){
			sql.append(" and po.tipoPr = 3 ");
		} else{
			sql.append(" and po.tipoPr != 3 ");
		}

		if(orderby != null){
			sql.append( " ");
			sql.append(getOrderByClause(orderby));
			sql.append(" ");
			sql.append(getOrderClasue(order));
		}

		SQLQuery query = session.createSQLQuery(sql.toString());
		for (int i = 0; i < parameterList.size(); i++) {
			query.setString(i, String.valueOf(parameterList.get(i)));
		}

		return query;
	}


	private SQLQuery getAdvancedSearchOrClauseSql(SearchDTO searchDTO, String baseSql, Session session, String orderby, String order, boolean addWhere) {

		StringBuilder sql = new StringBuilder(baseSql);
		List<String> parameterList = new ArrayList<String>();


		if(addWhere){
			sql.append(" where ");
		} else {
			parameterList.add(Integer.toString(PurchaseOrderStatus.PO_CLOSED.getCode()));

		}

		if(OmpRoles.OMP_ANTEX.getLabel().equalsIgnoreCase(searchDTO.getUserRole())
				){
			sql.append(AND);
			sql.append(" (gr.registerationDate is null or gr.goodReceiptNumber is null) ");
			//sql.append(" po.status in  ("+PurchaseOrderStatus.PO_RECEIVED.getCode() +","+PurchaseOrderStatus.GR_RECEIVED.getCode() +")");
			//parameterList.add(Long.toString(searchDTO.getPoStatus()));
		}

		if(searchDTO.getPoStatus() != null &&
				(PurchaseOrderStatus.PO_RECEIVED.getCode() == searchDTO.getPoStatus() || 
				PurchaseOrderStatus.PO_CLOSED.getCode() == searchDTO.getPoStatus())){

			sql.append(CustomSQLUtil.get(GENERATE_SEARCH_PO_STATUS));
			parameterList.add(Long.toString(searchDTO.getPoStatus()));
		}


		if(!OmpRoles.OMP_CONTROLLER.getLabel().equalsIgnoreCase(searchDTO.getUserRole())
				&& !OmpRoles.OMP_CONTROLLER_VBS.getLabel().equalsIgnoreCase(searchDTO.getUserRole())
				&& !OmpRoles.OMP_ANTEX.getLabel().equalsIgnoreCase(searchDTO.getUserRole())){
			sql.append(AND);

			if(searchDTO.getPositionICApproval() != null){
				sql.append(CustomSQLUtil.get(GENERATE_IC_APPROVAL_CLAUSE));
				parameterList.add(searchDTO.getPositionICApproval());
			} else {
				sql.append(CustomSQLUtil.get(GENERATE_IC_APPROVAL_EMPTY_CLAUSE));
			}

		}

		if(searchDTO.getMiniSearchValue() != null && !"".equals(searchDTO.getMiniSearchValue().trim())) {
			sql.append(AND);

			sql.append("(");

			sql.append( CustomSQLUtil.get(GENERATE_SEARCH_PO_NUMBER));
			parameterList.add(getFormatedValue(searchDTO.getMiniSearchValue()));
			sql.append( OR );

			sql.append(CustomSQLUtil.get(GENERATE_SEARCH_CLAUSE_VENDORE));
			parameterList.add(getFormatedValue(searchDTO.getMiniSearchValue()));

			sql.append( OR);
			sql.append(CustomSQLUtil.get(GENERATE_SEARCH_CLAUSE_FISCAL_YEAR));
			parameterList.add(getFormatedValue(searchDTO.getMiniSearchValue()));

			sql.append(OR );
			sql.append(CustomSQLUtil.get(GENERATE_SEARCH_CLAUSE_TOTAL_VALUE));
			parameterList.add(getFormatedValue(searchDTO.getMiniSearchValue()));

			sql.append(OR );
			sql.append(CustomSQLUtil.get(GENERATE_SEARCH_CLAUSE_ACTIVITY_DESCRIPTION));
			parameterList.add(getFormatedValue(searchDTO.getMiniSearchValue()));

			sql.append(OR );
			sql.append(CustomSQLUtil.get(GENERATE_SEARCH_CLAUSE_SCREEN_NAME_REQUESTER));
			parameterList.add(getFormatedValue(searchDTO.getMiniSearchValue()));

			sql.append(OR );
			sql.append(CustomSQLUtil.get(GENERATE_SEARCH_CLAUSE_SCREEN_NAME_RECIEVER));
			parameterList.add(getFormatedValue(searchDTO.getMiniSearchValue()));


			sql.append( ")");
		}

		if(searchDTO.getTipoPr() != null && searchDTO.getTipoPr() == TipoPr.VBS.getCode()){
			sql.append(" and po.tipoPr = 3 ");
		} else{
			sql.append(" and po.tipoPr != 3 ");
		}

		if(orderby != null){
			sql.append(" ");
			sql.append( getOrderByClause(orderby));
			sql.append(" ");
			sql.append(getOrderClasue(order));
		}

		SQLQuery query = session.createSQLQuery(sql.toString());
		for (int i = 0; i < parameterList.size(); i++) {
			query.setString(i, String.valueOf(parameterList.get(i)));
		}

		return query;
	}


	private String getCondition(Boolean [] firstInput){
		if(firstInput[0]){
			firstInput[0] = false;
			return WHERE;
		}else{
			return AND;
		}
	}

	/**
	 * replace = to like
	 * @param value
	 * @param criteria
	 * @return
	 */
	private String getFormatedCriteriaX(String value, String criteria){
		if(isContainsAsterisk(value)){
			return replaceEqual(criteria);
		}

		return criteria;
	}

	/**
	 * replace * to %
	 * @param value
	 * @return
	 */
	private String getFormatedValue(String value){
		if(isContainsAsterisk(value)){
			value = replaceAsterisk(value);
		}else{
			value = "%"+value+"%";
		}

		return value.toLowerCase();
	}
	private boolean isContainsAsterisk(String str){
		if(str.contains("*")){
			return true;
		}

		return false;
	}

	private String replaceAsterisk(String str){
		return str.replaceAll("\\*", "%");
	}

	private String replaceEqual(String str){
		return str.replaceAll("=", "like");
	}

	private static String GET_PO_GR_REQUESTED = PurchaseOrderFinderImpl.class.getName()+".getGRRequested";
	private static String GET_PO_GR_REQUESTED_COUNT = PurchaseOrderFinderImpl.class.getName()+".getGRRequestedCount";
	private static String ORDER_ASC = PurchaseOrderFinderImpl.class.getName()+".ascOrder";
	private static String ORDER_DESC = PurchaseOrderFinderImpl.class.getName()+".descOrder";
	private static String ORDER_BY_PURCHASE_ORDER_ID = PurchaseOrderFinderImpl.class.getName()+".orderByPurhaseOrderId";
	private static String ORDER_BY_RECEIVER_ID = PurchaseOrderFinderImpl.class.getName()+".orderByReceiverUserId";
	private static String ORDER_BY_CREATOR_ID = PurchaseOrderFinderImpl.class.getName()+".orderByCreatedUserId";
	private static String ORDER_BY_VENDOR = PurchaseOrderFinderImpl.class.getName()+".orderByVendor";
	private static String ORDER_BY_BUDGET_SUB_CAT = PurchaseOrderFinderImpl.class.getName()+".orderByBudgetSubCategory";
	private static String ORDER_BY_TOTAL_VALUE = PurchaseOrderFinderImpl.class.getName()+".orderByTotalValue";
	private static String ORDER_BY_FISCAL_YEAR = PurchaseOrderFinderImpl.class.getName()+".orderByFiscalYear";
	private static String ORDER_BY_DESCRIPTION = PurchaseOrderFinderImpl.class.getName()+".orderByDescription";
	private static final String GET_ALL_PROJECTNAMES = PurchaseOrderFinderImpl.class.getName()+".getAllProjectNames";
	private static final String GET_SUBPROJECTS = PurchaseOrderFinderImpl.class.getName()+".getSubProjects";
	private static final String GET_PO_NUMBERS = PurchaseOrderFinderImpl.class.getName()+".getPONumbers";

	private static String GENERATE_SEARCH_GR_REQUESTED_COUNT_BASE = PurchaseOrderFinderImpl.class.getName()+".advancedSearcGRRequestedCountBase";
	private static String GENERATE_SEARCH_GR_REQUESTED_BASE = PurchaseOrderFinderImpl.class.getName()+".advancedSearcGRRequestedBase";
	private static String GENERATE_SEARCH_COUNT_BASE = PurchaseOrderFinderImpl.class.getName()+".advancedSearchCountBase";
	private static String GENERATE_SEARCH_BASE = PurchaseOrderFinderImpl.class.getName()+".advancedSearchBase";
	private static String GENERATE_SEARCH_PO_NUMBER = PurchaseOrderFinderImpl.class.getName()+".purchaseOrderIdClause";
	private static String GENERATE_SEARCH_CLAUSE_PROJECT_NAME = PurchaseOrderFinderImpl.class.getName()+".projectNameClause";
	private static String GENERATE_SEARCH_CLAUSE_SUB_PROJECT_NAME = PurchaseOrderFinderImpl.class.getName()+".subProjectNameClause";
	private static String GENERATE_SEARCH_CLAUSE_COST_CENTER_ID = PurchaseOrderFinderImpl.class.getName()+".costCenterIdClause";
	private static String GENERATE_SEARCH_PO_STATUS = PurchaseOrderFinderImpl.class.getName()+".statusClause";
	private static String GENERATE_SEARCH_CLAUSE_FISCAL_YEAR = PurchaseOrderFinderImpl.class.getName()+".fiscalYearClause";
	private static String GENERATE_SEARCH_CLAUSE_WBS = PurchaseOrderFinderImpl.class.getName()+".wbsClause";
	private static String GENERATE_SEARCH_CLAUSE_VENDORE = PurchaseOrderFinderImpl.class.getName()+".vendorClause";
	private static String GENERATE_SEARCH_CLAUSE_TOTAL_VALUE = PurchaseOrderFinderImpl.class.getName()+".totalValueClause";
	private static String GENERATE_SEARCH_CLAUSE_ACTIVITY_DESCRIPTION = PurchaseOrderFinderImpl.class.getName()+".activityDescriptionClause";
	private static String GENERATE_SEARCH_CLAUSE_BUDGET_SUB_CATEGORY = PurchaseOrderFinderImpl.class.getName()+".budgetSubCategoryClause";
	private static String GENERATE_SEARCH_CLAUSE_SCREEN_NAME_REQUESTER = PurchaseOrderFinderImpl.class.getName()+".screenNameRequesterClause";
	private static String GENERATE_SEARCH_CLAUSE_SCREEN_NAME_RECIEVER = PurchaseOrderFinderImpl.class.getName()+".screenNameRecieverClause";

	private static String GET_PO_COUNT_BYSTATUS = PurchaseOrderFinderImpl.class.getName()+".getPurchaseOrdersCountByStatus";
	private static String GET_PO_BY_STATUS = PurchaseOrderFinderImpl.class.getName()+".getPurchaseOrdersByStatus";

	private static String GENERATE_IC_APPROVAL_CLAUSE = PurchaseOrderFinderImpl.class.getName()+".icApprovalClause";
	private static String GENERATE_IC_APPROVAL_EMPTY_CLAUSE = PurchaseOrderFinderImpl.class.getName()+".icApprovalEmptyClause";


	private static String AND = " and ";
	private static String OR = " or ";
	private static String WHERE = " where ";



}
