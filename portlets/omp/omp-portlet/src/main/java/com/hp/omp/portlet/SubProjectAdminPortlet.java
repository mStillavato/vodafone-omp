package com.hp.omp.portlet;

import java.io.IOException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.model.Project;
import com.hp.omp.model.ProjectSubProject;
import com.hp.omp.model.PurchaseOrder;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.SubProject;
import com.hp.omp.model.impl.ProjectSubProjectImpl;
import com.hp.omp.model.impl.SubProjectImpl;
import com.hp.omp.service.ProjectLocalServiceUtil;
import com.hp.omp.service.ProjectSubProjectLocalServiceUtil;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.hp.omp.service.SubProjectLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class SubProjectAdminPortlet
 */
public class SubProjectAdminPortlet extends MVCPortlet {
 
	private transient Logger logger = LoggerFactory.getLogger(SubProjectAdminPortlet.class);
	private String errorMSG = "";
	private String successMSG = "";
	private String ACTION_PARAM= "action";
	private String ADD_ACTION= "add";
	private String UPDATE_ACTION= "update";
	private String DELETE_ACTION= "delete";
	private String PAGINATION_ACTION= "pagination";
	
	

	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		super.doView(renderRequest, renderResponse);
	}
	
	
	/* (non-Javadoc)
	 * @see javax.portlet.GenericPortlet#render(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		viewAddSubPeojects(request, response);
		super.render(request, response);
	}
	
	public void viewAddSubPeojects(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		String action = renderRequest.getParameter(ACTION_PARAM);
		
		if(ADD_ACTION.equals(action)){
			renderRequest.setAttribute(ACTION_PARAM, ADD_ACTION);
			renderRequest.setAttribute("display",true);
		}else if(UPDATE_ACTION.equals(action)){
			String subProjectId = renderRequest.getParameter("subProjectId");
			SubProject subProject = getSubProject(subProjectId);
			Long projectId = getProjectId(subProjectId);
			
			logger.debug("render update action for : "+subProject.getSubProjectName());
			logger.debug("Display = "+subProject.getDisplay());
			
			renderRequest.setAttribute(ACTION_PARAM, UPDATE_ACTION);
			renderRequest.setAttribute("projectId", projectId);
			renderRequest.setAttribute("subProjectId", subProject.getSubProjectId());
			renderRequest.setAttribute("subProjectName", subProject.getSubProjectName());
			renderRequest.setAttribute("subProjectDesc", subProject.getDescription());
			renderRequest.setAttribute("display", subProject.getDisplay());
			
		}
		
		renderRequest.setAttribute("projectList", getProjectList());
	}
	
	@Override
    public void serveResource(ResourceRequest resourceRequest,
                  ResourceResponse resourceResponse) throws IOException,
                  PortletException {
		String action=resourceRequest.getParameter(ACTION_PARAM);
		if(DELETE_ACTION.equals(action)){
			deleteSubProject(resourceRequest, resourceResponse);
		}else if(PAGINATION_ACTION.equals(action)){
			paginationList(resourceRequest, resourceResponse);
		}
		
		clearMessages();
           super.serveResource(resourceRequest, resourceResponse);
    }

	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#processAction(javax.portlet.ActionRequest, javax.portlet.ActionResponse)
	 */
	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {

		clearMessages();
		String action = actionRequest.getParameter(ACTION_PARAM);
		String projectId = actionRequest.getParameter("project");
		String subProjectName = actionRequest.getParameter("subProjectName");
		String subProjectDesc = actionRequest.getParameter("subProjectDesc");
		boolean display=ParamUtil.getBoolean(actionRequest,"display");
		String operation = "";
		
		logger.debug("subProjectName="+subProjectName);
		logger.debug("subProjectDesc="+subProjectDesc);
		logger.debug("display = "+display);
		
		logger.debug("creating sub project object ...");
		SubProject newSubProject = new SubProjectImpl();
		newSubProject.setSubProjectName(subProjectName);
		newSubProject.setDescription(subProjectDesc);
		newSubProject.setDisplay(display);
		logger.debug("sub project object created !!");
		try {
			if(StringUtils.isNotBlank(projectId)){
				if(ADD_ACTION.equals(action)){
					operation = "added";
					addSubProject(newSubProject, projectId);
					
				}else if(UPDATE_ACTION.equals(action)){
					operation = "updated";
					String subProjectId = actionRequest.getParameter("subProjectId");
					String oldProjectId = actionRequest.getParameter("oldProjectId");
					newSubProject.setSubProjectId(Long.valueOf(subProjectId));
					updateSubProject(newSubProject, projectId, oldProjectId);
					
				}
			}else{
				errorMSG = "Project not selected";
			}
			
		} catch (SystemException e) {
			errorMSG = "Sub Project ["+subProjectName+"] not "+operation+".";
			logger.error("error happened while "+operation+" sub project ["+subProjectName+"] in database.");
			logger.error(e.getMessage(), e);
		}catch (NumberFormatException nfe) {
			errorMSG = "Sub Project ["+subProjectName+"] not "+operation+".";
			logger.error("error happened while "+operation+" sub project ["+subProjectName+"] in database.");
			logger.error(nfe.getMessage(), nfe);
		}
		
		actionRequest.setAttribute("errorMSG", errorMSG);
		actionRequest.setAttribute("successMSG", successMSG);
		super.processAction(actionRequest, actionResponse);
	}
	
	private void addSubProject(SubProject newSubProject, String projectId) throws SystemException{
		logger.debug("adding new sub project ["+newSubProject.getSubProjectName()+"] in database ...");
		if(SubProjectLocalServiceUtil.getSubProjectByName(newSubProject.getSubProjectName()) == null){
			
			SubProjectLocalServiceUtil.addSubProject(newSubProject);
			
			addProjectSubProject(projectId, newSubProject.getSubProjectName());
			
			successMSG = "new sub project name ["+newSubProject.getSubProjectName()+"] successfully Added";
			
		}else{
			errorMSG = "sub project name ["+newSubProject.getSubProjectName()+"] already existing";
		}
		
	}
	
	private void updateSubProject(SubProject newSubProject, String projectId, String oldProjectId) throws SystemException, NumberFormatException{
		
		
		
		if(! (isSubProjectNameExists(newSubProject, newSubProject.getSubProjectName()))){
			
			logger.debug("updating sub project ["+newSubProject.getSubProjectId()+"] in database ...");
			
			SubProjectLocalServiceUtil.updateSubProject(newSubProject);

			updateProjectSubProject(newSubProject.getSubProjectId(), Long.valueOf(projectId), Long.valueOf(oldProjectId));
			
			successMSG = "sup project Name ["+newSubProject.getSubProjectName()+"] successfully Updated";
		}else{
			errorMSG = "sup project name ["+newSubProject.getSubProjectName()+"] already existing";
		}
		
	}
	
	private void addProjectSubProject(String projectId, String subProjectName) throws SystemException{
		Long subProjectId = 0L;
		try {
			SubProject subProject = SubProjectLocalServiceUtil.getSubProjectByName(subProjectName);
			if (subProject == null){
				throw new SystemException("sub project is null");
			}
			
			subProjectId = subProject.getSubProjectId();
			
			ProjectSubProject projectSubProject = new ProjectSubProjectImpl();
			projectSubProject.setProjectId(Long.valueOf(projectId));
			projectSubProject.setSubProjectId(subProjectId);
			
			ProjectSubProjectLocalServiceUtil.addProjectSubProject(projectSubProject);
			
		} catch (SystemException e) {
			
			logger.error("error happened while adding new projectSubProject projectId=["+projectId+"] subProjectName=["+subProjectName+"]", e);
			logger.info("rolling back added sub project ["+subProjectName+"]");
			try {
				SubProjectLocalServiceUtil.deleteSubProject(subProjectId);
			} catch (PortalException e1) {
				logger.info("rolling back sub project ["+subProjectName+"], failed");
				throw new SystemException(e1);
			} catch (SystemException e1) {
				logger.info("rolling back sub project ["+subProjectName+"], failed");
				throw new SystemException(e1);
			}
			
			throw new SystemException(e);
		}
	}
	
	private void updateProjectSubProject(Long subProjectId, Long projectId, Long oldProjectId){
		
		try {
			if(projectId != oldProjectId){
				List<ProjectSubProject> projectSubProjectList = ProjectSubProjectLocalServiceUtil.getProjectSubProjectByProjectIdAndSubProjectId(oldProjectId, subProjectId);
				if(projectSubProjectList != null && !(projectSubProjectList.isEmpty())){
					ProjectSubProject projectSubProject = projectSubProjectList.get(0);
					if(projectSubProject != null){
						projectSubProject.setProjectId(Long.valueOf(projectId));
						projectSubProject.setSubProjectId(Long.valueOf(subProjectId));
						
						ProjectSubProjectLocalServiceUtil.updateProjectSubProject(projectSubProject);
					}
				}
			}
		} catch (SystemException e) {
			logger.error("error happened while updating project sub project for subProjectId  ["+subProjectId+"], projectId ["+projectId+"] in database.");
			logger.error(e.getMessage(), e);
		}catch (NumberFormatException nfe) {
			logger.error("error happened while updating project sub project for subProjectId  ["+subProjectId+"], projectId ["+projectId+"] in database.");
			logger.error(nfe.getMessage(), nfe);
		}
		
	}
	
	private void retrieveSubProjectListToRequest(PortletRequest renderRequest) throws SystemException{
		try {
			String searchFilter = ParamUtil.getString(renderRequest, "searchFilter","");

			int totalCount = SubProjectLocalServiceUtil.getSubProjectsCount();

			Integer pageNumber = ParamUtil.getInteger(renderRequest, "pageNumber", 1);
			Integer pageSize = OmpHelper.PAGE_SIZE;
			int start = (pageNumber - 1) * pageSize;
			int end = start + pageSize;

			renderRequest.setAttribute("numberOfPages", getTotalNumberOfPages(totalCount, pageSize));
			renderRequest.setAttribute("pageNumber", pageNumber);


			//List<SubProject> subProjectList = SubProjectLocalServiceUtil.getSubProjects(start, end);
			List<SubProject> subProjectList = SubProjectLocalServiceUtil.getSubProjectsList(start, end, searchFilter);

			renderRequest.setAttribute("subProjectList", subProjectList);
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			errorMSG = "can not retrieve sub project list";
			renderRequest.setAttribute("errorMSG", errorMSG);
		}

	}
	
	public void deleteSubProject(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse)
			throws PortletException, IOException {
		clearMessages();
		
		String subProjectId=resourceRequest.getParameter("subProjectId");
		String subProjectName=resourceRequest.getParameter("subProjectName");
		String action=resourceRequest.getParameter("action");
		String jspPage = "/html/subprojectadmin/subprojectlist.jsp";
		
		if(Validator.isBlank(action)){
			errorMSG = "No Action in the request";
		}else if(Validator.isBlank(subProjectId)){
			errorMSG = "No sub project Id in the request";
		}else{
			
			try {
				List<PurchaseOrder> purchaseOrderList = PurchaseOrderLocalServiceUtil.getPurchaseOrderBySubProjectId(subProjectId);
				List<PurchaseRequest> purchaseRequestList = PurchaseRequestLocalServiceUtil.getPurchaseRequestBySubProjectId(subProjectId);
				
				if(purchaseOrderList != null && ! (purchaseOrderList.isEmpty())){
					errorMSG = "Can Not remove Sub Project assigned to Purchase Order, Sub Project name ["+subProjectName+"]";
				}else if(purchaseRequestList != null && ! (purchaseRequestList.isEmpty())){
					errorMSG = "Can Not remove Sub Project assigned to Purchase Request, Sub Project name ["+subProjectName+"]";
				}else{
					
					deleteProjectSubProject(subProjectId);
					
					SubProjectLocalServiceUtil.deleteSubProject(Long.valueOf(subProjectId));
					
					successMSG = "Sub Project name ["+subProjectName+"] successfully deleted";
				}
				
			} catch (NumberFormatException e) {
				logger.error(e.getMessage());
				errorMSG = "sub project Id is invalid";
			} catch (PortalException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove sub project";
			} catch (SystemException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove sub project [most probably project is assigned.]";
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove sub project";
			}
			
		}
		
		//retrieve updated list
		try {
			retrieveSubProjectListToRequest(resourceRequest);
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			errorMSG = "can not retrieve sub project list";
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
		
	}
	
	private SubProject getSubProject(String subProjectId){
		SubProject subProject = null;
		try {
			subProject = SubProjectLocalServiceUtil.getSubProject(Long.valueOf(subProjectId));
		} catch (NumberFormatException e) {
			logger.error("sub project Id has wrong format", e);
		} catch (PortalException e) {
			logger.error("unexpected error", e);
		} catch (SystemException e) {
			logger.error("unexpected error", e);
		}
		
		return subProject;
	}
	
	private Long getProjectId(String subProjectId){
		Long projectId = 0L;
		try{
			List<ProjectSubProject> projectSubProjectList = ProjectSubProjectLocalServiceUtil.getProjectSubProjectBySubProjectId(Long.valueOf(subProjectId));
			if(projectSubProjectList != null && !(projectSubProjectList.isEmpty())){
				ProjectSubProject projectSubProject = projectSubProjectList.get(0);
				if(projectSubProject != null){
					projectId = projectSubProject.getProjectId();
				}
			}
			
		} catch (NumberFormatException e) {
			logger.error("sub project Id has wrong format", e);
		} catch (SystemException e) {
			logger.error("unexpected error", e);
		} catch (Exception e) {
			logger.error("unexpected error", e);
		}
		
		return projectId;
	}
	private List<Project> getProjectList() {
		List<Project> projectsList = null;
		try {
			 projectsList = ProjectLocalServiceUtil.getProjects(0, ProjectLocalServiceUtil.getProjectsCount());
		} catch (SystemException e) {
			logger.error("error happened while retrieving project list ... ",e);
		}
		
		return projectsList;
	}
	
	
	private boolean isSubProjectNameExists(SubProject newSubProject, String subProjectName) throws SystemException{
		
		SubProject oldSubProject = SubProjectLocalServiceUtil.getSubProjectByName(subProjectName);
		boolean isExists = false;
		
		if(oldSubProject != null && 
				(newSubProject.getSubProjectId() != oldSubProject.getSubProjectId()) ){
			isExists = true;
		}
		return isExists;
	}
	
	private void deleteProjectSubProject(String subProjectId) throws SystemException {
		Long projectSubProjectId = 0L;
		try {
			List<ProjectSubProject> projectSubProjectList = ProjectSubProjectLocalServiceUtil.getProjectSubProjectBySubProjectId(Long.valueOf(subProjectId));
			
			if(projectSubProjectList != null && ! (projectSubProjectList.isEmpty()) ){
				for (ProjectSubProject projectSubProject : projectSubProjectList) {
					projectSubProjectId = projectSubProject.getProjectSubProjectId();
					ProjectSubProjectLocalServiceUtil.deleteProjectSubProject(projectSubProject);
					
					logger.debug("ProjectSubProjectId ["+projectSubProjectId+"] deleted successfully .");
				}
				
				
			}
		} catch (NumberFormatException e) {
			if(projectSubProjectId == 0){
				logger.error("error happend while deleting projectSubProject for subProjectId["+subProjectId+"]", e);
			}else{
				logger.error("error happend while deleting projectSubProjectId["+projectSubProjectId+"]", e);
			}
			
			throw new SystemException(e);
		} catch (SystemException e) {
			if(projectSubProjectId == 0){
				logger.error("error happend while deleting projectSubProject for subProjectId["+subProjectId+"]", e);
			}else{
				logger.error("error happend while deleting projectSubProjectId="+projectSubProjectId+"]", e);
			}
			throw new SystemException(e);
		}
		
	}
	
	private void paginationList(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse) throws PortletException, IOException{
		
		String jspPage = "/html/subprojectadmin/subprojectlist.jsp";
		try {
			retrieveSubProjectListToRequest(resourceRequest);
		} catch (SystemException e) {
		logger.error("error happened in pagination ... ",e);
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
	}
	
	private Integer getTotalNumberOfPages(Integer totalNubmerOfResult, Integer pageSize) {
        return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
	}
	
	
	private void clearMessages(){
		errorMSG = "";
		successMSG = "";
	}
	
	public void searchFilter(ActionRequest request, ActionResponse response) throws IOException, PortletException{
		response.setRenderParameter("searchFilter", "%" + ParamUtil.getString(request, "subProjectName") + "%");  
	}

}
