package com.hp.omp.service.persistence;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.custom.Currency;
import com.hp.omp.model.custom.ExcelTableRow;
import com.hp.omp.model.custom.IAndCType;
import com.hp.omp.model.custom.OMPAggregateReportExcelRow;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.custom.PurchaseRequestStatus;
import com.hp.omp.model.custom.ReportCheckIcDTO;
import com.hp.omp.model.custom.ReportDTO;
import com.hp.omp.model.custom.SearchReportCheckIcDTO;
import com.hp.omp.model.custom.TipoPr;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.SessionFactory;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class ReportPersistenceImpl implements ReportPersistence {

	private SessionFactory _sessionFactory = null;

	public void setSessionFactory(SessionFactory sessionFactory) {
		_sessionFactory = sessionFactory;
	}

	public Double generateAggregateReport(ReportDTO reportDTO, boolean closed)
			throws SystemException {
		Session session = null;
		SQLQuery query = null;
		try {
			session = _sessionFactory.openSession();

			query = getReportSql(reportDTO, CustomSQLUtil.get(GENERATE_AGGREGATE_REPORT_BASE),
					CustomSQLUtil.get(GENERATE_DETAIL_ORDERBY_CLAUSE), closed, session);

			if(query == null){
				return null;
			}

			query.addScalar("TOTAL_VALUE", Type.DOUBLE);

			List<Double> list = (List<Double>)QueryUtil.list(query,_sessionFactory.getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);

			if(list == null || list.isEmpty() || list.get(0) == null){
				return 0.0;
			}
			return list.get(0);
		} catch (ORMException e) {
			throw processException(e);
		} finally{
			_sessionFactory.closeSession(session);
		}
	}

	public Double generateAggregateReportTotalPoistionValues(ReportDTO reportDTO)
			throws SystemException {
		Session session = null;
		SQLQuery query = null;
		try {
			session = _sessionFactory.openSession();

			query = getReportSql(reportDTO, CustomSQLUtil.get(GENERATE_AGGREGATE_REPORT_POSTIONS_TOTAL_VALUES_BASE),
					CustomSQLUtil.get(GENERATE_DETAIL_ORDERBY_CLAUSE), null, session);

			if(query == null){
				return null;
			}

			query.addScalar("TOTAL_VALUE", Type.DOUBLE);

			List<Double> list = (List<Double>)QueryUtil.list(query,_sessionFactory.getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);

			if(list == null || list.isEmpty() || list.get(0) == null){
				return 0.0;
			}
			return list.get(0);
		} catch (ORMException e) {
			throw processException(e);
		} finally{
			_sessionFactory.closeSession(session);
		}
	}

	public Double generateAggregatePrPositionsWithoutPoNumberValue(ReportDTO reportDTO)
			throws SystemException {
		Session session = null;
		SQLQuery query = null;
		try {
			session = _sessionFactory.openSession();
			reportDTO.setPrReport(true);
			query = getReportSql(reportDTO, CustomSQLUtil.get(GENERATE_AGGREGATE_PR_POSTIONS_TOTAL_VALUES),
					"", null, session);

			if(query == null){
				return null;
			}

			query.addScalar("TOTAL_VALUE", Type.DOUBLE);

			List<Double> list = (List<Double>)QueryUtil.list(query,_sessionFactory.getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);

			if(list == null || list.isEmpty() || list.get(0) == null){
				return 0.0;
			}
			return list.get(0);
		} catch (ORMException e) {
			throw processException(e);
		} finally{
			_sessionFactory.closeSession(session);
		}
	}

	public List<ExcelTableRow> generateDetailReport(ReportDTO reportDTO) throws SystemException{
		Session session = null;
		SQLQuery query = null;
		List<ExcelTableRow> result = null;
		List<Object> positions = new ArrayList<Object>();
		List<Object> comittedPositions = new ArrayList<Object>();
		try {
			session = _sessionFactory.openSession();
			boolean isPrReport = reportDTO.isPrReport();
			List<String> parameterList = new ArrayList<String>();


			//Create PO Part
			reportDTO.setPrReport(false);
			String poSQLPart = getReportSqlString(reportDTO, CustomSQLUtil.get(GENERATE_DETAIL_REPORT_BASE), "", null, session);
			parameterList = getParameterList(reportDTO, parameterList);

			//Create PR Part
			reportDTO.setPrReport(true);
			String prSQLPart = getReportSqlString(reportDTO, CustomSQLUtil.get(GENERATE_DETAIL_REPORT_PR_BASE), "", null, session);
			parameterList = getParameterList(reportDTO, parameterList);

			//UNION PO AND PR
			String sql = " ("+poSQLPart+") UNION ALL ("+prSQLPart+") ";
			sql += CustomSQLUtil.get(GENERATE_DETAIL_REPORT_ORDERBY_CLAUSE);

			//GET SQL QUERY
			query = getSQLQuery(sql, parameterList, session);


			reportDTO.setPrReport(isPrReport);
			//			String sql = "("+CustomSQLUtil.get(GENERATE_DETAIL_REPORT_BASE)+") UNION ALL ("+CustomSQLUtil.get(GENERATE_DETAIL_REPORT_PR_BASE)+")";
			//			query = getReportSql(reportDTO,  sql,
			//					CustomSQLUtil.get(GENERATE_DETAIL_ORDERBY_CLAUSE), null, session);
			//			
			if(query == null){
				return null;
			}
			result = new ArrayList<ExcelTableRow>();
			List<Object[]> list = (List<Object[]>)QueryUtil.list(query,_sessionFactory.getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			for (Object[] objects : list) {
				ExcelTableRow tableRow = getTableRow(objects, positions,comittedPositions);
				result.add(tableRow);
			}

			return result;
		} catch (ORMException e) {
			throw processException(e);
		} finally{
			_sessionFactory.closeSession(session);
		}
	}

	public List<ExcelTableRow> generateControllerDetailReport(ReportDTO reportDTO) throws SystemException{
		Session session = null;
		SQLQuery query = null;
		List<ExcelTableRow> result = null;
		try {
			session = _sessionFactory.openSession();
			List<String> parameterList = new ArrayList<String>();

			//Create PO has tracking code Part
			reportDTO.setPrReport(false);
			String poSQLPart = getReportSqlString(reportDTO, CustomSQLUtil.get(GENERATE_CONTROLLER_DETAIL_REPORT_PO_BASE), "", null, session);
			parameterList = getParameterList(reportDTO, parameterList);


			////Create PO no tracking code Part
			reportDTO.setPrReport(true);
			String prSQLPart = getReportSqlString(reportDTO, CustomSQLUtil.get(GENERATE_CONTROLLER_DETAIL_REPORT_PR_BASE), "", null, session);
			parameterList = getParameterList(reportDTO, parameterList);

			//UNION PO AND PR
			String sql = " ("+poSQLPart+") UNION ALL ("+prSQLPart+") ";
			sql += CustomSQLUtil.get(GENERATE_CONTROLLER_DETAIL_ORDERBY_CLAUSE);


			//GET SQL QUERY
			query = getSQLQuery(sql, parameterList, session);

			/*query = getReportSql(reportDTO,  CustomSQLUtil.get(GENERATE_CONTROLLER_DETAIL_REPORT_PR_BASE), 
					CustomSQLUtil.get(GENERATE_CONTROLLER_DETAIL_ORDERBY_CLAUSE), null, session);*/

			if(query == null){
				return null;
			}
			result = new ArrayList<ExcelTableRow>();
			List<Object[]> list = (List<Object[]>)QueryUtil.list(query,_sessionFactory.getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			if (list != null) {
				_log.debug("list size: " + list.size());
			}
			for (Object[] objects : list) {
				ExcelTableRow tableRow = getDetailsTableRow(objects);
				result.add(tableRow);
			}
			_log.debug("result size: " + result.size());
			return result;
		} catch (ORMException e) {
			throw processException(e);
		} finally{
			_sessionFactory.closeSession(session);
		}
	}

	private ExcelTableRow getDetailsTableRow(Object[] objects) {
		OMPAggregateReportExcelRow tableRow = new OMPAggregateReportExcelRow();

		int prStatus 					= 1;
		int currencyIndex 				= 7;
		int prTotalValueIndex 			= 8;
		int positionQuantityIndex 		= 11;
		int positionActualUnitCostIndex = 12;
		int poTotalValueIndex			= 13;
		int tcIndex 					= 16;
		int positionCostIndex 			= 25;
		int grPercentage 				= 26;
		int grValueIndex 				= 27;
		int grRegDateIndex 				= 28;
		int offerDateIndex 				= 31;
		int deliveryDateIndex 			= 32;
		int icFlagIndex 				= 38;
		int approverIndex 				= 40;
		int prCreatedDateIndex			= 44;

		for (int i = 0; i < objects.length; i++) {
			if (i == prStatus) {
				if(objects[i] !=null) {
					BigDecimal statusPr =(BigDecimal)objects[i];
					int status = statusPr.intValue();
					if (PurchaseRequestStatus.CREATED.getCode() == status) {
						tableRow.setFieldAtIndex(PurchaseRequestStatus.CREATED.getLabel(),i);
					} else if (PurchaseRequestStatus.UPDATED.getCode() == status){
						tableRow.setFieldAtIndex(PurchaseRequestStatus.UPDATED.getLabel(),i);
					} else if (PurchaseRequestStatus.SUBMITTED.getCode() == status){
						tableRow.setFieldAtIndex(PurchaseRequestStatus.SUBMITTED.getLabel(),i);
					} else if (PurchaseRequestStatus.CLOSED.getCode() == status){
						tableRow.setFieldAtIndex(PurchaseRequestStatus.CLOSED.getLabel(),i);
					} else if (PurchaseRequestStatus.DELETED.getCode() == status){
						tableRow.setFieldAtIndex(PurchaseRequestStatus.DELETED.getLabel(),i);
					} else if (PurchaseRequestStatus.ASSET_RECEIVED.getCode() == status){
						tableRow.setFieldAtIndex(PurchaseRequestStatus.ASSET_RECEIVED.getLabel(),i);
					} else if (PurchaseRequestStatus.SC_RECEIVED.getCode() == status){
						tableRow.setFieldAtIndex(PurchaseRequestStatus.SC_RECEIVED.getLabel(),i);
					} else if (PurchaseRequestStatus.PO_RECEIVED.getCode() == status){
						tableRow.setFieldAtIndex(PurchaseRequestStatus.PO_RECEIVED.getLabel(),i);
					} else if (PurchaseRequestStatus.PR_ASSIGNED.getCode() == status){
						tableRow.setFieldAtIndex(PurchaseRequestStatus.PR_ASSIGNED.getLabel(),i);
					} else if (PurchaseRequestStatus.PENDING_IC_APPROVAL.getCode() == status){
						tableRow.setFieldAtIndex(PurchaseRequestStatus.PENDING_IC_APPROVAL.getLabel(),i);
					} else if (PurchaseRequestStatus.REJECTED.getCode() == status){
						tableRow.setFieldAtIndex(PurchaseRequestStatus.REJECTED.getLabel(),i);
					}else{
						tableRow.setFieldAtIndex("", i);
					}
					_log.trace(objects[i].toString());					
				} else {
					tableRow.setFieldAtIndex("", i);
				}

			}else if(i == currencyIndex){ //Currency
				if(objects[i] !=null) {
					BigDecimal currencyDecimal =(BigDecimal)objects[i];
					int cur = currencyDecimal.intValue();
					if(Currency.EUR.getCode() == cur){
						tableRow.setFieldAtIndex(Currency.EUR.getLabel(),i);
					}else if(Currency.USD.getCode() == cur){
						tableRow.setFieldAtIndex(Currency.USD.getLabel(),i);
					}else{
						tableRow.setFieldAtIndex("", i);
					}
					_log.trace(objects[i].toString());
				}else {
					tableRow.setFieldAtIndex("", i);
				}
			}else if (i == prTotalValueIndex || i == positionQuantityIndex || i == positionActualUnitCostIndex 
					|| i == poTotalValueIndex || i == positionCostIndex || i == grValueIndex || i == grPercentage){
				if(objects[i] !=null) {
					Double doubleValue=Double.valueOf(objects[i].toString());
					tableRow.setFieldAtIndex(doubleValue, i);
				}else {
					tableRow.setFieldAtIndex("", i);
				}
			} else if(i == icFlagIndex){ //IC Flag
				if(objects[i] !=null) {
					String icFlagString = objects[i].toString();
					int icFlag = Integer.parseInt(icFlagString);
					if(IAndCType.ICFIELD.getCode() == icFlag){
						tableRow.setFieldAtIndex(IAndCType.ICFIELD.getLabel(), i);
					}else if(IAndCType.ICTEST.getCode() == icFlag){
						tableRow.setFieldAtIndex(IAndCType.ICTEST.getLabel(), i);
					}else{
						tableRow.setFieldAtIndex("", i);
					}
					_log.trace(objects[i].toString());
				}else{
					tableRow.setFieldAtIndex("", i);
				}
			}else if(i == approverIndex){ //Approver
				try {
					if (objects[i] != null) {
						String userIdString = objects[i].toString();
						Long userId = Long.parseLong(userIdString);
						User user = UserLocalServiceUtil.getUserById(userId);
						tableRow.setFieldAtIndex(user.getScreenName(), i);
						_log.trace(objects[i].toString());
					} else {
						tableRow.setFieldAtIndex("", i);
					}
				} catch (Exception e) {
					debug("Cannor get User: "+objects[i]);
					debug(e.getMessage());
					tableRow.setFieldAtIndex("", i);
				}
			}else if(i == tcIndex){ //Tracking Code
				if (objects[i] != null) {
					String trackingString = objects[i].toString();
					if(trackingString.contains("||")){
						String trackingCode = trackingString.substring(0, trackingString.indexOf("||"));
						trackingCode = trackingCode.trim();
						tableRow.setFieldAtIndex(trackingCode, i);
					}else{
						tableRow.setFieldAtIndex(trackingString, i);
					}
				}else{
					tableRow.setFieldAtIndex("", i);
				}

			}else if (i == grRegDateIndex || i == offerDateIndex || i == deliveryDateIndex || i == prCreatedDateIndex){
				if (objects[i] != null) {
					try{
						tableRow.setFieldAtIndex(dateFormatter((Date)objects[i]), i);
					}catch(ParseException e){
						e.printStackTrace();
						tableRow.setFieldAtIndex("", i);
					}

				}else{
					tableRow.setFieldAtIndex("", i);
				}

			}else if(objects[i] == null){
				tableRow.setFieldAtIndex("", i);
				_log.trace("field is empty");
			}else{
				_log.trace(objects[i].toString());
				tableRow.setFieldAtIndex(objects[i], i);
			}
		}
		return tableRow;
	}
	
	private ExcelTableRow getDetailsCheckIcTableRow(Object[] objects) {
		OMPAggregateReportExcelRow tableRow = new OMPAggregateReportExcelRow();

		int approverIndex 				= 05;
		int grRequestDateIndex 			= 07;
		int positionQuantityIndex 		= 11;
		int positionCostIndex 			= 12;

		for (int i = 0; i < objects.length; i++) {
			if (i == positionQuantityIndex || i == positionCostIndex){
				if(objects[i] !=null) {
					Double doubleValue=Double.valueOf(objects[i].toString());
					tableRow.setFieldAtIndex(doubleValue, i);
				}else {
					tableRow.setFieldAtIndex("", i);
				}
			} else if (i == grRequestDateIndex) {
				if (objects[i] != null) {
					try{
						tableRow.setFieldAtIndex(dateFormatter((Date)objects[i]), i);
					}catch(ParseException e){
						tableRow.setFieldAtIndex("", i);
					}

				}else{
					tableRow.setFieldAtIndex("", i);
				}
			} else if(i == approverIndex){ //Approver
				try {
					if (objects[i] != null) {
						String userIdString = objects[i].toString();
						Long userId = Long.parseLong(userIdString);
						User user = UserLocalServiceUtil.getUserById(userId);
						tableRow.setFieldAtIndex(user.getScreenName(), i);
						_log.trace(objects[i].toString());
					} else {
						tableRow.setFieldAtIndex("", i);
					}
				} catch (Exception e) {
					debug("Cannor get User: "+objects[i]);
					debug(e.getMessage());
					tableRow.setFieldAtIndex("", i);
				}
			} else if(objects[i] == null){
				tableRow.setFieldAtIndex("", i);
				_log.trace("field is empty");
			} else{
				_log.trace(objects[i].toString());
				tableRow.setFieldAtIndex(objects[i], i);
			}
		}
		return tableRow;
	}

	private String dateFormatter(Date date) throws ParseException {
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(date);
	}

	private ExcelTableRow getTableRow(Object[] objects, List<Object> positions, List<Object> comittedPositions) {
		OMPAggregateReportExcelRow tableRow = new OMPAggregateReportExcelRow();
		int maxColumnIndex = 21;
		int poIdIndex = 3;
		int grNumberIndex = 5;
		int grRegDateIndex = 6;
		int tcIndex = 10;
		int grValueIndex = 13;
		int grPercentageIndex = 14; 
		int positionValueIndex = 15;
		int committedValueIndex = 16;
		int deliveryDateIndex = 17;
		int statusFieldIndex = 20;
		int grIdIndex = 21;
		int positionIdIndex = 22;


		for (int i = 0; i < objects.length; i++) {

			if(i > maxColumnIndex){ //No need for the extra columns [CR 46]
				continue;
			}


			if(i == grValueIndex) { //GR value
				try {
					if(objects[i] !=null) {
						Double grValue=Double.valueOf(objects[i].toString());
						//					String formattedGrValue=OmpFormatterUtil.formatNumberByLocale(grValue, Locale.ITALY);
						//					tableRow.setFieldAtIndex(formattedGrValue, i);
						tableRow.setFieldAtIndex(grValue, i);
					}else {
						tableRow.setFieldAtIndex("", i);
					}
				}catch(Exception e) {
					debug("Cannot get GR value: "+objects[i]);
					debug(e.getMessage());
					tableRow.setFieldAtIndex("", i);
				}

			}else if(i == positionValueIndex) { //position_value
				//_log.trace("postion ID: "+objects[positionIdIndex].toString());
				if(positions.contains(objects[positionIdIndex].toString())){
					tableRow.setFieldAtIndex("", i);
					//_log.trace("skip position value for same position id, "+objects[i]);
				}else{
					try {
						if(objects[i] !=null) {
							BigDecimal positionValueRs=(BigDecimal)objects[i];
							Double positionValue=positionValueRs.doubleValue();
							//							String formattedpositionValue=OmpFormatterUtil.formatNumberByLocale(positionValue, Locale.ITALY);
							//							tableRow.setFieldAtIndex(formattedpositionValue, i);
							tableRow.setFieldAtIndex(positionValue, i);
							//_log.trace(objects[i]);
							positions.add(objects[positionIdIndex].toString());// add position id to skip the position value in next same rows 
						}else {
							tableRow.setFieldAtIndex("", i);
						}
					}catch(Exception e) {
						debug("Cannot get position value: "+objects[i]);
						debug(e.getMessage());
						tableRow.setFieldAtIndex("", i);
					}
				}
			}else if(i == committedValueIndex) {
				//_log.debug("postion ID [comitted]: "+objects[positionIdIndex].toString());
				if(comittedPositions.contains(objects[positionIdIndex].toString())){
					tableRow.setFieldAtIndex("", i);
					//_log.trace("skip position value for same position id, "+objects[i]);
				}else{
					try {
						if(objects[i] !=null) {
							BigDecimal positionValueRs=(BigDecimal)objects[i];
							Double positionValue=positionValueRs.doubleValue();
							//							String formattedpositionValue=OmpFormatterUtil.formatNumberByLocale(positionValue, Locale.ITALY);
							//							tableRow.setFieldAtIndex(formattedpositionValue, i);
							tableRow.setFieldAtIndex(positionValue, i);
							//_log.trace(objects[i]);
							comittedPositions.add(objects[positionIdIndex].toString());// add position id to skip the position value in next same rows 
						}else {
							tableRow.setFieldAtIndex("", i);
						}
					}catch(Exception e) {
						debug("Cannot get position value: "+objects[i]);
						debug(e.getMessage());
						tableRow.setFieldAtIndex("", i);
					}
				}
			}else if( i == grPercentageIndex){
				if(objects[i] !=null) {
					Double doubleValue=Double.valueOf(objects[i].toString());
					tableRow.setFieldAtIndex(doubleValue, i);
				}else {
					tableRow.setFieldAtIndex("", i);
				}
			}else if(i == tcIndex){ //Tracking Code
				if (objects[i] != null) {
					String trackingString = objects[i].toString();
					if(trackingString.contains("||")){
						String trackingCode = trackingString.substring(0, trackingString.indexOf("||"));
						trackingCode = trackingCode.trim();
						tableRow.setFieldAtIndex(trackingCode, i);
					}else{
						tableRow.setFieldAtIndex(trackingString, i);
					}
				}else{
					tableRow.setFieldAtIndex("", i);
				}

			}else if (i == grRegDateIndex || i == deliveryDateIndex) {
				if (objects[i] != null) {
					try{
						tableRow.setFieldAtIndex(dateFormatter((Date)objects[i]), i);
					}catch(ParseException e){
						tableRow.setFieldAtIndex("", i);
					}

				}else{
					tableRow.setFieldAtIndex("", i);
				}


			}else if(i == statusFieldIndex){

				if(objects[grIdIndex] != null && objects[grNumberIndex] != null && objects[grRegDateIndex] != null){
					tableRow.setFieldAtIndex("Closed", i);
				}else if(objects[grIdIndex] != null){
					tableRow.setFieldAtIndex("Closing", i);
				}else if(objects[poIdIndex] != null){
					tableRow.setFieldAtIndex("Comitted", i);
				}else if(objects[poIdIndex] == null){
					String prstatus = getPRStatus(objects[statusFieldIndex]);
					tableRow.setFieldAtIndex(prstatus, i);
					//_log.debug("PR Status: "+prstatus);
				}else if(objects[statusFieldIndex] == null){
					tableRow.setFieldAtIndex("", i);
				}

			}else if(objects[i] == null){
				tableRow.setFieldAtIndex("", i);
				//_log.trace("field is empty");
			}else{
				//_log.trace(objects[i]);
				tableRow.setFieldAtIndex(objects[i], i);
			}

		}
		return tableRow;
	}

	public String getPRStatus(Object statusObj){
		return "PR Inserted";
	}

	public List<String> getAllFiscalYears() throws SystemException {
		return getAllValues(GET_ALL_FISCALYEARS);
	}

	public List<String> getAllCostCenters() throws SystemException{	
		return getAllValues(GET_ALL_COSTCENTERS);
	}

	public List<String> getAllProjectNames() throws SystemException{
		return getAllValues(GET_ALL_PROJECTNAMES);
	}

	private List<String> getAllValues(String sqlID) throws SystemException{
		List<String> allValues = new ArrayList<String>();
		String value = null;
		Session session = null;
		try {
			session = _sessionFactory.openSession();
			String hql = CustomSQLUtil.get(sqlID);
			Query query = session.createQuery(hql);
			Iterator<String> iterate = (Iterator<String>)QueryUtil.iterate(query,_sessionFactory.getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			while(iterate.hasNext()){
				value = iterate.next();
				allValues.add(value);
			}
			return allValues;
		} catch (ORMException e) {
			throw processException(e);
		} finally{
			_sessionFactory.closeSession(session);
		}
	}

	public List<String> getSubProjects(Long projectId) throws SystemException{
		List<String> subProjectNames = new ArrayList<String>();
		String subProjectName = null;
		Session session = null;
		try {
			session = _sessionFactory.openSession();
			String hql = CustomSQLUtil.get(GET_SUBPROJECTS);
			Query query = session.createQuery(hql);			
			QueryPos queryPos = QueryPos.getInstance(query);
			queryPos.add(projectId);
			Iterator<String> iterate = (Iterator<String>)QueryUtil.iterate(query,_sessionFactory.getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			while(iterate.hasNext()){
				subProjectName = iterate.next();
				subProjectNames.add(subProjectName);
			}
			if(subProjectNames.isEmpty()){
				return null;
			}
			return subProjectNames;
		} catch (ORMException e) {
			throw processException(e);
		} finally{
			_sessionFactory.closeSession(session);
		}
	}

	private SQLQuery getReportSql(ReportDTO reportDTO, String baseSql, String orderByClause, Boolean closedPosition, Session session) {
		List<String> parameterList = new ArrayList<String>();
		String sql = getReportSqlString(reportDTO, baseSql, orderByClause, closedPosition, session);
		parameterList = getParameterList(reportDTO, parameterList);
		return getSQLQuery(sql, parameterList, session);
	}

	private SQLQuery getSQLQuery(String sql, List<String> parameterList, Session session){
		SQLQuery query = session.createSQLQuery(sql);
		for (int i = 0; i < parameterList.size(); i++) {
			query.setString(i, parameterList.get(i));
		}
		return query;
	}

	private List<String> getParameterList(ReportDTO reportDTO, List<String> parameterList){

		if(reportDTO.getFiscalYear() != null){
			parameterList.add(reportDTO.getFiscalYear());
		}
		if(reportDTO.getWbsCode() != null){
			parameterList.add(reportDTO.getWbsCode());
		}
		if(reportDTO.getProjectId() != null){
			parameterList.add(String.valueOf(reportDTO.getProjectId()));
		}
		if(reportDTO.getTargaTecnica() != null){
			parameterList.add(String.valueOf(reportDTO.getTargaTecnica()));
		}
		if(reportDTO.getCostCenterId() != null){
			parameterList.add(String.valueOf(reportDTO.getCostCenterId()));
		}
		if(reportDTO.getCurrency() != null){
			if(reportDTO.getCurrency() != 2){
				parameterList.add(String.valueOf(reportDTO.getCurrency()));
			}
		}
		if(reportDTO.getPrStatus() != null){
			parameterList.add(String.valueOf(reportDTO.getPrStatus()));
		}
		if(reportDTO.getVendorId() != null){
			parameterList.add(String.valueOf(reportDTO.getVendorId()));
		}

		if (reportDTO.getUserType() != null) {
			if (OmpRoles.OMP_NDSO.getLabel().equalsIgnoreCase(reportDTO.getUserType())) {
				parameterList.add(String.valueOf(IAndCType.ICFIELD.getCode()));
			} else if (OmpRoles.OMP_TC.getLabel().equalsIgnoreCase(reportDTO.getUserType())) {
				parameterList.add(String.valueOf(IAndCType.ICTEST.getCode()));
			}
		}
		
		//if(reportDTO.getTipoPr() != null){
			parameterList.add(String.valueOf(TipoPr.VBS.getCode()));
		//}

		if(_log.isDebugEnabled()){
			_log.debug("Report search values= ["+parameterList+" ]");
		}

		return parameterList;
	}

	private String getReportSqlString(ReportDTO reportDTO, String baseSql, String orderByClause, Boolean closedPosition, Session session) {

		StringBuilder sql = new StringBuilder(baseSql);

		if(reportDTO.getFiscalYear() != null){
			sql.append(CustomSQLUtil.get(GENERATE_REPORT_CLAUSE_FISCAL_YEAR));
		}

		if(reportDTO.getWbsCode() != null){
			if (reportDTO.isPrReport()) {
				sql.append(CustomSQLUtil.get(GENERATE_REPORT_CLAUSE_PR_WBS));
			}else{
				sql.append(CustomSQLUtil.get(GENERATE_REPORT_CLAUSE_WBS));
			}
		}

		if(reportDTO.getProjectId() != null){
			if (reportDTO.isPrReport()) {
				sql.append(CustomSQLUtil.get(GENERATE_REPORT_CLAUSE_PR_PROJECT_ID));
			}else{
				sql.append(CustomSQLUtil.get(GENERATE_REPORT_CLAUSE_PROJECT_ID));
			}
		}

		if(reportDTO.getTargaTecnica() != null){
			sql.append(CustomSQLUtil.get(GENERATE_REPORT_CLAUSE_TARGA_TECNICA));
		}

		if(reportDTO.getCostCenterId() != null){
			sql.append(CustomSQLUtil.get(GENERATE_REPORT_CLAUSE_COST_CENTER));
		}

		if(reportDTO.getCurrency() != null){
			if(reportDTO.getCurrency() != 2){
				if (reportDTO.isPrReport()) {
					sql.append(CustomSQLUtil.get(GENERATE_REPORT_CLAUSE_PR_CURRENCY));
				}else{
					sql.append(CustomSQLUtil.get(GENERATE_REPORT_CLAUSE_CURRENCY));
				}
			}
		}

		if(reportDTO.getPrStatus() != null){
			if (reportDTO.isPrReport()) {
				sql.append(CustomSQLUtil.get(GENERATE_REPORT_CLAUSE_PR_STATUS));
			}else{
				sql.append(CustomSQLUtil.get(GENERATE_REPORT_CLAUSE_PO_STATUS));
			}
		}

		if(reportDTO.getVendorId() != null){
			if (reportDTO.isPrReport()) {
				sql.append(CustomSQLUtil.get(GENERATE_REPORT_CLAUSE_PR_VENDOR));
			}else{
				sql.append(CustomSQLUtil.get(GENERATE_REPORT_CLAUSE_PO_VENDOR));
			}				
		}

		if(reportDTO.getUserType() != null){
			if (OmpRoles.OMP_NDSO.getLabel().equalsIgnoreCase(reportDTO.getUserType())
					|| OmpRoles.OMP_TC.getLabel().equalsIgnoreCase(reportDTO.getUserType())) {
				sql.append(CustomSQLUtil.get(GENERATE_REPORT_CLAUSE_POSITION_IC_APPROVAL));
			}
		}
		
		if(reportDTO.getTipoPr() != null){
			if (reportDTO.isPrReport()) {
				sql.append(CustomSQLUtil.get(GENERATE_REPORT_CLAUSE_PR_VBS));
			}else{
				sql.append(CustomSQLUtil.get(GENERATE_REPORT_CLAUSE_PO_VBS));
			}				
		} else{
			if (reportDTO.isPrReport()) {
				sql.append(CustomSQLUtil.get(GENERATE_REPORT_CLAUSE_PR_NO_VBS));
			}else{
				sql.append(CustomSQLUtil.get(GENERATE_REPORT_CLAUSE_PO_NO_VBS));
			}				
		}

		if(closedPosition != null){
			if(closedPosition){
				sql.append(CustomSQLUtil.get(GENERATE_REPORT_IS_NOT_NULL_CLAUSE));
			}else{
				sql.append(CustomSQLUtil.get(GENERATE_REPORT_IS_NULL_CLAUSE));
			}
		}
		//		sql.append(" order by  po.purchaseOrderId ,  position.positionId");
		sql.append(orderByClause);

		if(_log.isDebugEnabled()){
			_log.debug("Report search sql= ["+sql.toString()+" ]");
		}

		return sql.toString();
	}


	private SystemException processException(Exception e) {
		_log.error("Caught unexpected exception " + e.getMessage());
		e.printStackTrace();

		if (_log.isDebugEnabled()) {
			_log.debug(e.getMessage(), e);
		}

		return new SystemException(e);
	}


	static final String GET_ALL_COSTCENTERS = ReportPersistence.class.getName()+".getAllCostCenters";
	static final String GET_ALL_FISCALYEARS = ReportPersistence.class.getName()+".getAllFiscalYears";
	static final String GET_ALL_PROJECTNAMES = ReportPersistence.class.getName()+".getAllProjectNames";
	static final String GET_SUBPROJECTS = ReportPersistence.class.getName()+".getSubProjects";

	static final String GENERATE_AGGREGATE_REPORT_BASE = ReportPersistence.class.getName()+".generateAggregateReportBase";
	static final String GENERATE_DETAIL_REPORT_BASE = ReportPersistence.class.getName()+".generateDetailReportBase";
	static final String GENERATE_DETAIL_REPORT_PR_BASE = ReportPersistence.class.getName()+".generateDetailReportPRBase";
	static final String GENERATE_DETAIL_ORDERBY_CLAUSE = ReportPersistence.class.getName()+".detailsOrderBy";
	static final String GENERATE_DETAIL_REPORT_ORDERBY_CLAUSE = ReportPersistence.class.getName()+".detailsReportOrderBy";
	static final String GENERATE_CONTROLLER_DETAIL_REPORT_PR_BASE = ReportPersistence.class.getName()+".generateControllerDetailReportPrBase";
	static final String GENERATE_CONTROLLER_DETAIL_REPORT_PO_BASE = ReportPersistence.class.getName()+".generateControllerDetailReportPoBase";
	static final String GENERATE_CONTROLLER_DETAIL_ORDERBY_CLAUSE = ReportPersistence.class.getName()+".controllerDetailsOrderBy";
	static final String GENERATE_REPORT_CLAUSE_FISCAL_YEAR = ReportPersistence.class.getName()+".fiscalYearClause";
	static final String GENERATE_REPORT_CLAUSE_PROJECT_ID = ReportPersistence.class.getName()+".projectIdClause";
	static final String GENERATE_REPORT_CLAUSE_PR_PROJECT_ID = ReportPersistence.class.getName()+".prProjectIdClause";
	static final String GENERATE_REPORT_CLAUSE_SUB_PROJECT_ID = ReportPersistence.class.getName()+".subProjectIdClause";
	static final String GENERATE_REPORT_CLAUSE_PR_SUB_PROJECT_ID = ReportPersistence.class.getName()+".prSubProjectIdClause";
	static final String GENERATE_REPORT_CLAUSE_TARGA_TECNICA = ReportPersistence.class.getName()+".targaTecnicaClause";
	static final String GENERATE_REPORT_CLAUSE_WBS = ReportPersistence.class.getName()+".wBSClause";
	static final String GENERATE_REPORT_CLAUSE_PR_WBS = ReportPersistence.class.getName()+".prWBSClause";
	static final String GENERATE_REPORT_CLAUSE_COST_CENTER = ReportPersistence.class.getName()+".costCenterClause";
	static final String GENERATE_REPORT_CLAUSE_CURRENCY = ReportPersistence.class.getName()+".currencyClause";
	static final String GENERATE_REPORT_CLAUSE_PR_CURRENCY = ReportPersistence.class.getName()+".prCurrencyClause";
	static final String GENERATE_REPORT_CLAUSE_PR_STATUS = ReportPersistence.class.getName()+".prStatusClause";
	static final String GENERATE_REPORT_CLAUSE_PR_VENDOR = ReportPersistence.class.getName()+".prVendorClause";
	static final String GENERATE_REPORT_CLAUSE_PO_STATUS = ReportPersistence.class.getName()+".poStatusClause";
	static final String GENERATE_REPORT_CLAUSE_PO_VENDOR = ReportPersistence.class.getName()+".poVendorClause";
	static final String GENERATE_REPORT_IS_NULL_CLAUSE = ReportPersistence.class.getName()+".registerationIsNullClause";
	static final String GENERATE_REPORT_IS_NOT_NULL_CLAUSE = ReportPersistence.class.getName()+".registerationIsNotNullClause";
	static final String GENERATE_AGGREGATE_REPORT_POSTIONS_TOTAL_VALUES_BASE = ReportPersistence.class.getName()+".generateAggregateReportOpenPositionsTotalValueBase";
	static final String GENERATE_AGGREGATE_PR_POSTIONS_TOTAL_VALUES = ReportPersistence.class.getName()+".generateAggregatePrPositionsTotalValue";
	static final String GENERATE_REPORT_CLAUSE_POSITION_IC_APPROVAL = ReportPersistence.class.getName()+".positionIAndCClause";
	
	static final String GENERATE_REPORT_CLAUSE_PR_VBS = ReportPersistence.class.getName()+".prVBSClause";
	static final String GENERATE_REPORT_CLAUSE_PR_NO_VBS = ReportPersistence.class.getName()+".prNoVBSClause";
	static final String GENERATE_REPORT_CLAUSE_PO_VBS = ReportPersistence.class.getName()+".poVBSClause";
	static final String GENERATE_REPORT_CLAUSE_PO_NO_VBS = ReportPersistence.class.getName()+".poNoVBSClause";
	
	static final String GENERATE_REPORT_CHECK_IC_PR_BASE = ReportPersistence.class.getName()+".generateReportCheckIcPrBase";
	static final String GENERATE_REPORT_CHECK_IC_PO_BASE = ReportPersistence.class.getName()+".generateReportCheckIcPoBase";
	
	private static void debug(Object msg){
		if(_log.isDebugEnabled()){
			if(msg !=null) {
				_log.debug(msg.toString());
			}
		}
	}

	private static Logger  _log = LoggerFactory.getLogger(ReportPersistenceImpl.class);

	
	public List<ReportCheckIcDTO> getReportCheckIcList(SearchReportCheckIcDTO searchDTO) throws SystemException {
		_log.debug("inside public List<ReportCheckIcDTO> getReportCheckIcList()");
		
		Session session = null;
		SQLQuery query  = null;
		
		try {
			session = _sessionFactory.openSession();

			//Create PO has tracking code Part
			String poSQLPart = CustomSQLUtil.get(GENERATE_REPORT_CHECK_IC_PO_BASE);

			////Create PO no tracking code Part
			String prSQLPart = CustomSQLUtil.get(GENERATE_REPORT_CHECK_IC_PR_BASE);

			//UNION PO AND PR
			String sql = " ("+poSQLPart+") UNION ALL ("+prSQLPart+") ";
			sql += CustomSQLUtil.get(GENERATE_CONTROLLER_DETAIL_ORDERBY_CLAUSE);

			//GET SQL QUERY
			query = session.createSQLQuery(sql);
			int start = (searchDTO.getPageNumber() - 1) * searchDTO.getPageSize();
			query.setFirstResult(start);
			query.setMaxResults(searchDTO.getPageSize());

			if(query == null){
				return null;
			}
			
			//List<Object[]> list = (List<Object[]>)QueryUtil.list(query,_sessionFactory.getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			@SuppressWarnings("unused")
			List<Object[]> list = query.list();
			
			if (list != null) {
				_log.debug("list size: " + list.size());
			}
			
			List<ReportCheckIcDTO> reportCheckIcList = new ArrayList<ReportCheckIcDTO>();
			for (Object[] row : list) {
				ReportCheckIcDTO reportCheckIcDTO = new ReportCheckIcDTO();

				reportCheckIcDTO.setPurchaseRequestId("" + row[0]);
				reportCheckIcDTO.setFiscalYear("" + row[1]);
				reportCheckIcDTO.setPurchaseOrderId("" + row[2]);
				reportCheckIcDTO.setEvoLocation("" + row[3]);
				reportCheckIcDTO.setTargaTecnica("" + row[4]);
				reportCheckIcDTO.setIcApprover("" + row[5]);
				reportCheckIcDTO.setGrRequestor("" + row[6]);
				reportCheckIcDTO.setGrRequestDate(formatDate("" + row[7]));
				//reportCheckIcDTO.setPrDescription("" + row[8]);
				
				reportCheckIcList.add(reportCheckIcDTO);
			}
			return reportCheckIcList;
			
		} catch (ORMException e) {
			throw processException(e);
		} finally{
			_sessionFactory.closeSession(session);
		}
	}

	public Long getReportCheckIcListCount(SearchReportCheckIcDTO searchDTO) throws SystemException {
		Session session = null; 
		
		try {
			session = _sessionFactory.openSession();
			
			//Create PO has tracking code Part
			String poSQLPart = CustomSQLUtil.get(GENERATE_REPORT_CHECK_IC_PO_BASE);

			////Create PO no tracking code Part
			String prSQLPart = CustomSQLUtil.get(GENERATE_REPORT_CHECK_IC_PR_BASE);

			//UNION PO AND PR
			String sql = " ("+poSQLPart+") UNION ALL ("+prSQLPart+") ";

			//GET SQL QUERY
			SQLQuery query = session.createSQLQuery(sql);
			
			if(query == null){
				return null;
			}
			
			List<Object[]> list = (List<Object[]>)QueryUtil.list(query,_sessionFactory.getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			if (list != null) {
				_log.debug("list size: " + list.size());
				return Long.valueOf(list.size());
			} else{
				_log.debug("list size null");
				return 0L;
			}

		} finally {
			if(session != null) {
				_sessionFactory.closeSession(session);
			}
		}
	}

	public List<ExcelTableRow> generateCheckIcReport() throws SystemException {
		Session session = null;
		SQLQuery query = null;
		List<ExcelTableRow> result = null;
		try {
			session = _sessionFactory.openSession();

			//Create PO has tracking code Part
			String poSQLPart = CustomSQLUtil.get(GENERATE_REPORT_CHECK_IC_PO_BASE);

			////Create PO no tracking code Part
			String prSQLPart = CustomSQLUtil.get(GENERATE_REPORT_CHECK_IC_PR_BASE);

			//UNION PO AND PR
			String sql = " ("+poSQLPart+") UNION ALL ("+prSQLPart+") ";
			sql += CustomSQLUtil.get(GENERATE_CONTROLLER_DETAIL_ORDERBY_CLAUSE);


			//GET SQL QUERY
			query = session.createSQLQuery(sql);

			if(query == null){
				return null;
			}
			result = new ArrayList<ExcelTableRow>();
			List<Object[]> list = (List<Object[]>)QueryUtil.list(query,_sessionFactory.getDialect(), QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			if (list != null) {
				_log.debug("list size: " + list.size());
			}
			for (Object[] objects : list) {
				ExcelTableRow tableRow = getDetailsCheckIcTableRow(objects);
				result.add(tableRow);
			}
			_log.debug("result size: " + result.size());
			return result;
		} catch (ORMException e) {
			throw processException(e);
		} finally{
			_sessionFactory.closeSession(session);
		}
	}
	
	private String formatDate(String date){
		try{
			String anno = date.substring(0, 4);
			String mese = date.substring(5, 7);
			String giorno = date.substring(8, 10);
			 
			return (giorno + '/' + mese + '/' + anno);
		}catch(Exception e){
			return "";
		}
	}
}
