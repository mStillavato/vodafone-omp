/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.hp.omp.model.custom.LocationDTO;
import com.hp.omp.service.base.LocationLocalServiceBaseImpl;
import com.hp.omp.service.persistence.LocationFinderUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

/**
 * The implementation of the location local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.LocationLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see com.hp.omp.service.base.LocationLocalServiceBaseImpl
 * @see com.hp.omp.service.LocationLocalServiceUtil
 */
public class LocationLocalServiceImpl extends LocationLocalServiceBaseImpl {

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<LocationDTO> getLocationsList()
			throws SystemException {
		return LocationFinderUtil.getLocationsList();
	}

	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long getLocationsCountList(String searchFilter)
			throws SystemException {
		return LocationFinderUtil.getLocationsCountList(searchFilter);
	}
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public LocationDTO getLocationByTargaTecnica(String targaTecnica)
			throws SystemException {
		return LocationFinderUtil.getLocationByTargaTecnica(targaTecnica);
	}
    
	private Date stringToDate(String date){
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy"); 
		try {
			return df.parse(date);
		} catch (ParseException e) {
			return null;
		}
	}
	
	private String formatDate(String date){
		try{
			String anno = date.substring(0, 4);
			String mese = date.substring(5, 7);
			String giorno = date.substring(8, 10);
			 
			return (giorno + '.' + mese + '.' + anno);
		}catch(Exception e){
			return "";
		}
	}


	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public LocationDTO getLocationByLocationEvo(String locationEvo)
			throws SystemException {
		return LocationFinderUtil.getLocationByLocationEvo(locationEvo);
	}

}