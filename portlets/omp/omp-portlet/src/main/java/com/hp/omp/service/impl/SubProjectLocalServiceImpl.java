/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service.impl;

import java.util.List;

import com.hp.omp.model.Project;
import com.hp.omp.model.SubProject;
import com.hp.omp.service.base.SubProjectLocalServiceBaseImpl;
import com.hp.omp.service.persistence.ProjectFinderUtil;
import com.hp.omp.service.persistence.SubProjectFinderUtil;
import com.hp.omp.service.persistence.SubProjectUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

/**
 * The implementation of the sub project local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.SubProjectLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Ahmed Kamel
 * @see com.hp.omp.service.base.SubProjectLocalServiceBaseImpl
 * @see com.hp.omp.service.SubProjectLocalServiceUtil
 */
public class SubProjectLocalServiceImpl extends SubProjectLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.hp.omp.service.SubProjectLocalServiceUtil} to access the sub project local service.
	 */
	
	public SubProject getSubProjectByName(String name) throws SystemException{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(SubProject.class);
		dynamicQuery.add(RestrictionsFactoryUtil.ilike("subProjectName",name));
		 
		SubProject subProject = null;
		List<SubProject>  subProjectList = SubProjectUtil.findWithDynamicQuery(dynamicQuery);
		if(subProjectList != null && ! (subProjectList.isEmpty()) ){
			subProject = subProjectList.get(0);
		}
		
		return subProject;
	}
	
	public List<SubProject> getSubProjectByProjectId(Long projectId) throws SystemException{
		return SubProjectFinderUtil.getSubProjectByProjectId(projectId);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<SubProject> getSubProjectsList(int start, int end,
			String searchFilter) throws SystemException {
		List<SubProject> resultQuery = null;
		resultQuery= SubProjectFinderUtil.getSubProjectsList(start, end, searchFilter);
		return resultQuery; 
	}
}