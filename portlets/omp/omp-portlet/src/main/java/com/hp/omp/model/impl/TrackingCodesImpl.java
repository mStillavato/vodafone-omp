package com.hp.omp.model.impl;

/**
 * The extended model implementation for the TrackingCodes service. Represents a row in the &quot;TRACKING_CODES&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.model.TrackingCodes} interface.
 * </p>
 *
 * @author HP Egypt team
 */
public class TrackingCodesImpl extends TrackingCodesBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a tracking codes model instance should use the {@link com.hp.omp.model.TrackingCodes} interface instead.
     */
    public TrackingCodesImpl() {
    }
}
