package com.hp.omp.service.impl;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;

import com.hp.omp.model.Buyer;
import com.hp.omp.model.ExcelReport;
import com.hp.omp.model.ExcelStyler;
import com.hp.omp.model.ExcelTable;
import com.hp.omp.model.custom.ExcelTableRow;
import com.hp.omp.model.custom.OMPAggregateReportExcelRow;
import com.hp.omp.model.impl.ExcelDefaultStyler;
import com.hp.omp.model.impl.ExcelReportImpl;
import com.hp.omp.model.impl.ExcelTableImpl;
import com.hp.omp.service.BuyerLocalServiceUtil;
import com.hp.omp.service.base.BuyerLocalServiceBaseImpl;
import com.hp.omp.service.persistence.BuyerFinderUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

/**
 * The implementation of the buyer local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.BuyerLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see com.hp.omp.service.base.BuyerLocalServiceBaseImpl
 * @see com.hp.omp.service.BuyerLocalServiceUtil
 */
public class BuyerLocalServiceImpl extends BuyerLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.hp.omp.service.BuyerLocalServiceUtil} to access the buyer local service.
     */
	
	public List<Buyer> getBuyersList(Integer pageNumber, Integer pageSize) throws SystemException {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Buyer.class);
		Integer start = (pageNumber - 1) * pageSize;
		Integer end = start + pageSize;
		dynamicQuery.setLimit(start, end);
		dynamicQuery.addOrder(OrderFactoryUtil.asc("buyerId"));
		@SuppressWarnings("unchecked")
		List<Buyer> resultQuery =  BuyerLocalServiceUtil.dynamicQuery(dynamicQuery);
		return resultQuery; 
	}
	
	public Buyer getBuyerByName(String name) throws SystemException {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Buyer.class);
		dynamicQuery.add(RestrictionsFactoryUtil.ilike("name", name));
		@SuppressWarnings("unchecked")
		List<Buyer> resultQuery =  BuyerLocalServiceUtil.dynamicQuery(dynamicQuery);
		return (resultQuery == null || resultQuery.size() == 0) ?  null  : resultQuery.get(0);
	}
	
	
	public List<Buyer> getDisplayedBuyers() throws SystemException{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Buyer.class);
		dynamicQuery.add(RestrictionsFactoryUtil.eq("display", Boolean.TRUE));
		@SuppressWarnings("unchecked")
		List<Buyer> resultQuery =  BuyerLocalServiceUtil.dynamicQuery(dynamicQuery);
		return resultQuery; 
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Buyer> getBuyersList(Integer start, Integer end,
			String searchFilter, int gruppoUsers) throws SystemException {
//		Integer start = (pageNumber - 1) * pageSize;
//		Integer end = start + pageSize;		
		List<Buyer> resultQuery = null;
		resultQuery = BuyerFinderUtil.getBuyersList(start, end, searchFilter, gruppoUsers);
		return resultQuery; 
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getBuyersCountList(String searchFilter, int gruppoUsers) throws SystemException {
		int countList = 0;
		countList = BuyerFinderUtil.getBuyersCountList(searchFilter, gruppoUsers);
		return countList;
	}

	public void exportBuyersAsExcel(List<Buyer> listBuyers,
			List<Object> header, OutputStream out) throws SystemException {
		ExcelReport excelReport = new ExcelReportImpl();
		ExcelStyler styler = new ExcelDefaultStyler(excelReport.getWorkBook());
		excelReport.setStyler(styler);

		List<ExcelTableRow> buyersList = new ArrayList<ExcelTableRow>();
		for(Buyer buyer : listBuyers){
			OMPAggregateReportExcelRow tableRow = new OMPAggregateReportExcelRow();
			tableRow.setFieldAtIndex(buyer.getName(), 0);
			tableRow.setFieldAtIndex(buyer.getDisplay(), 1);

			buyersList.add(tableRow);
		}

		ExcelTable euroTable = new ExcelTableImpl(header, buyersList, "Buyers");
		excelReport.addExcelTable(euroTable, 0);

		Workbook exelWorkBook = excelReport.createExelWorkBook();
		try {
			exelWorkBook.write(out);
		} catch (IOException e) {
		} finally{
			try {
				out.flush();
				out.close();
			} catch (IOException e) {
			}
		}
	}
}
