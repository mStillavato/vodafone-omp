package com.hp.omp.decorator;

import java.io.Serializable;

import org.displaytag.decorator.TableDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.Project;

public class ProjectListDecorator extends TableDecorator implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2685221218697856230L;

	Logger logger = LoggerFactory.getLogger(ProjectListDecorator.class);
	
	public String getProjectId() {
		
        return getRemoveAction();
    }
	
	public String getProjectName() {
		
        return getUpdateAction();
    }
	
	public String getRemoveAction() {
		
        StringBuilder sb = new StringBuilder();
        Project item = (Project) this.getCurrentRowObject();
      
        long projectId = item.getProjectId();
        String projectName = item.getProjectName();
        sb.append("<a href=\"javascript:removeProject("+projectId+",'"+projectName+"')\"");
        sb.append("\">");
        sb.append(" Remove ");
        sb.append("</a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
    }	
	
	public String getUpdateAction() {
		
		Project item = (Project) this.getCurrentRowObject();
		StringBuilder sb = new StringBuilder();
       
      
        long projectId = item.getProjectId();
        String projectName = item.getProjectName();
        sb.append("<a onclick=\"javascript:updateProject(this, "+projectId+")\" href='"+projectName+"'");
        sb.append("\"> ");
        sb.append(projectName);
        sb.append(" </a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
        
    }

}
