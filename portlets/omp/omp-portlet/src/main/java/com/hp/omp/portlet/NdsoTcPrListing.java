package com.hp.omp.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.helper.UploadDownloadHelper;
import com.hp.omp.model.CostCenter;
import com.hp.omp.model.CostCenterUser;
import com.hp.omp.model.Project;
import com.hp.omp.model.SubProject;
import com.hp.omp.model.WbsCode;
import com.hp.omp.model.custom.IAndCType;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.custom.PrListDTO;
import com.hp.omp.model.custom.PurchaseRequestStatus;
import com.hp.omp.model.custom.SearchDTO;
import com.hp.omp.service.CostCenterLocalServiceUtil;
import com.hp.omp.service.CostCenterUserLocalServiceUtil;
import com.hp.omp.service.ProjectLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.hp.omp.service.SubProjectLocalServiceUtil;
import com.hp.omp.service.WbsCodeLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class NdsoTcPrListing extends MVCPortlet {

	public static final Logger logger = LoggerFactory.getLogger(PurchaseRequestListPortlet.class);
	boolean created = false;
	
	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		try {
			List<WbsCode> wbsCodeList = WbsCodeLocalServiceUtil.getWbsCodes(0, WbsCodeLocalServiceUtil.getWbsCodesCount());
			renderRequest.setAttribute("allWbsCodes", wbsCodeList);
			
			List<Project> projectsList = ProjectLocalServiceUtil.getProjects(0, ProjectLocalServiceUtil.getProjectsCount());
			renderRequest.setAttribute("allProjects", projectsList);
			
		} catch (SystemException e) {
			logger.error("error happened during retrieving WBS code/Projects list",e);
		}
		super.doView(renderRequest, renderResponse);
	}
	
	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
		List<PurchaseRequestStatus> status = getPRListStatus(); 
		request.setAttribute("statusList", status);
		request.setAttribute("pendingStatus", PurchaseRequestStatus.PENDING_IC_APPROVAL);
		request.setAttribute("costCentersList", getCostCentersList(themeDisplay.getUserId()));
		String userType =OmpHelper.getUserRoles(themeDisplay.getUserId());
		request.setAttribute("userType",userType);
		request.setAttribute("userCostCenterId",getUserCostCenterId(themeDisplay.getUserId()));
		
		super.render(request, response);
	}
	
	private List<PurchaseRequestStatus> getPRListStatus(){
		List<PurchaseRequestStatus> statusList = new ArrayList<PurchaseRequestStatus>();
		statusList.add(PurchaseRequestStatus.PENDING_IC_APPROVAL);
		statusList.add(PurchaseRequestStatus.SUBMITTED);
		statusList.add(PurchaseRequestStatus.PR_ASSIGNED);
		statusList.add(PurchaseRequestStatus.ASSET_RECEIVED);
		statusList.add(PurchaseRequestStatus.SC_RECEIVED);
		statusList.add(PurchaseRequestStatus.PO_RECEIVED);
		statusList.add(PurchaseRequestStatus.REJECTED);
		return statusList;
	}
	
	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		
		 if("searchPr".equals(resourceRequest.getResourceID())) {
			 searchPR(resourceRequest, resourceResponse);
		 }else {
	     String jspPage = "/html/ndsotcprlisting/prlist.jsp";
	     
	     try {
	    	 if("getSubProjects".equals(resourceRequest.getResourceID())){
					getSubProjects(resourceRequest,resourceResponse);
	    	 }else if("downloadFile".equals(resourceRequest.getResourceID())){
	    		 downloadFile(resourceRequest, resourceResponse);
	 		}else if("downloadZipFile".equals(resourceRequest.getResourceID())){
	 			downloadAllFiles(resourceRequest, resourceResponse);
	 		}else{
	    		 ThemeDisplay themeDisplay = (ThemeDisplay)resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
	    	     Integer statusCode = ParamUtil.getInteger(resourceRequest, "status", PurchaseRequestStatus.PENDING_IC_APPROVAL.getCode());	
	    	     Integer pageNumber = ParamUtil.getInteger(resourceRequest, "pageNumber", 1);
	    	     String orderby = ParamUtil.getString(resourceRequest, "orderby", "purchaseRequestId");
	    	     String order = ParamUtil.getString(resourceRequest, "order", "desc");
	    	     
	    	     //get search parameters from the request
	    	     long prId = ParamUtil.getLong(resourceRequest, "prId");
	    	     long costCentId = ParamUtil.getLong(resourceRequest, "costCenter");
	    	     String fYear = ParamUtil.getString(resourceRequest, "fYear");
	    	     String project = ParamUtil.getString(resourceRequest, "project");
	    	     String subProject = ParamUtil.getString(resourceRequest, "subProject");
	    	     String wbsCode = ParamUtil.getString(resourceRequest, "wbsCode");
	    	     String searchKeyWord = ParamUtil.getString(resourceRequest, "searchKeyWord");
	    	     boolean miniSeach = ParamUtil.getBoolean(resourceRequest, "isMini",true);
	    	     //fill in SearchDTO with  search parameters
	    	     SearchDTO searchDTO = new SearchDTO();
	    	     searchDTO.setPrId(prId);
	    	     searchDTO.setCostCenterId(costCentId);
	    	     searchDTO.setPrStatus(statusCode);
	    	     searchDTO.setFiscalYear(fYear);
	    	     searchDTO.setProjectName(project);
	    	     searchDTO.setSubProjectName(subProject);
	    	     searchDTO.setWbsCode(wbsCode);
	    	     searchDTO.setGlobalSearchKeyWord(searchKeyWord);
	    	     searchDTO.setPageNumber(pageNumber);
	    	     searchDTO.setPageSize(OmpHelper.PAGE_SIZE);
	    	     searchDTO.setOrder(order);
	    	     searchDTO.setOrderby(orderby);
	    	     searchDTO.setMiniSeach(miniSeach);
	    	     searchDTO.setMiniSearchValue(searchKeyWord);
	    	     if(statusCode.equals(PurchaseRequestStatus.PENDING_IC_APPROVAL.getCode())) {
	    	    	 searchDTO.setPositionApproved(0);
	    	     }
	    				long userId = themeDisplay.getUserId();
	    				if(OmpHelper.isNdso(userId)){
	    					searchDTO.setPositionICApproval(Integer.toString(IAndCType.ICFIELD.getCode()));
	    				}else if(OmpHelper.isTc(userId)){
	    					searchDTO.setPositionICApproval(Integer.toString(IAndCType.ICTEST.getCode()));
	    				}
	    	    	/* long costCenterId = 0;
	    	    	 if(OmpRoles.OMP_ENG.equals(userRole)) {
	    	    		 CostCenterUser costCenterUser = CostCenterUserLocalServiceUtil.getCostCenterUserByUserId(themeDisplay.getUserId());
	    	    		 costCenterId = costCenterUser.getCostCenterId();
	    	    	 }*/
	    			List<PrListDTO> prList = PurchaseRequestLocalServiceUtil.getAntexPurchaseRequestList(searchDTO);
	    			setHasFile(prList);
	    			Long totalCount = PurchaseRequestLocalServiceUtil.getAntexPurchaseRequestCount(searchDTO);
	    			logger.info("Result List count {}",totalCount);
	    			resourceRequest.setAttribute("numberOfPages", getTotalNumberOfPages(totalCount, OmpHelper.PAGE_SIZE));
	    			resourceRequest.setAttribute("pageNumber", pageNumber);
	    			resourceRequest.setAttribute("prList", prList);
	    			
	    			 getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
	    	 }
	     
	     } catch(Exception e) {
	    	 logger.error("Error in get PRL for Enginner",e );
	    	 SessionErrors.add(resourceRequest, "Unassgined-CostCenter");
	     }
		 }
		
	}
	private Integer getTotalNumberOfPages(Long totalNubmerOfResult, Integer pageSize) {
        return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
}
	
	public void searchPR(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws PortletException, IOException {
		logger.debug("inside searchPR");
		String jspPage = "/html/ndsotcprlisting/prlist.jsp";
	     
	     try {
	    	
	    		 ThemeDisplay themeDisplay = (ThemeDisplay)resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
    	     
	    	     String prSearchValue = ParamUtil.getString(resourceRequest, "searchPrValue");
	    	     SearchDTO searchDTO = new SearchDTO();
	    	     searchDTO.setPrSearchValue(prSearchValue);
	    	    	 OmpRoles userRole = OmpHelper.getUserRole(themeDisplay.getUserId());
	    			List<PrListDTO> prList = PurchaseRequestLocalServiceUtil.prSearch(searchDTO,userRole.getCode(),themeDisplay.getUserId());
	    			resourceRequest.setAttribute("prList", prList);
	    			
	    			 getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
	    	 
	     
	     } catch(Exception e) {
	    	 logger.error("Error in get search result  for I&C committers",e );
	     }
	}

	private List<CostCenter> getCostCentersList(long userId)
			throws PortletException {

		List<CostCenter> allCostCenters = new ArrayList<CostCenter>();
		try {
			allCostCenters = CostCenterLocalServiceUtil.getCostCenters(0,CostCenterLocalServiceUtil.getCostCentersCount());
		} catch (SystemException e) {
			throw new PortletException(e);
		}

		return allCostCenters;
	}
	
	
	private long getUserCostCenterId(long userId)
			throws PortletException {
			
			long ccId =0;
		
		try {
			OmpRoles userRole = OmpHelper.getUserRole(userId);

			if ("OMP_ENG".equalsIgnoreCase(userRole.name()) || "OMP_OBSERVER".equalsIgnoreCase(userRole.name())) {
				CostCenterUser costCenterUser = CostCenterUserLocalServiceUtil.getCostCenterUserByUserId(userId);
				if(costCenterUser != null){
					ccId = costCenterUser.getCostCenterId();
				}
				
			}
			
		} catch (SystemException e) {
			throw new PortletException(e);
		} catch (PortalException e) {
			throw new PortletException(e);
		}

		return ccId;
	}
	 
	public void getSubProjects(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws SystemException, IOException {
		JSONObject optionsJSON = JSONFactoryUtil.createJSONObject();
		Long project = ParamUtil.getLong(resourceRequest, "project");
		List<SubProject> subProjectList = getSubProjects(project);
		if(subProjectList == null){
			subProjectList = new ArrayList<SubProject>();
		}
		JSONArray jsonsubProjectsArray = JSONFactoryUtil.createJSONArray();
		for (SubProject subProject : subProjectList) {
			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("id", subProject.getSubProjectId());
			json.put("name", subProject.getSubProjectName());
			
			jsonsubProjectsArray.put(json);
		}
		optionsJSON.put("subProjects", jsonsubProjectsArray);
		PrintWriter writer = resourceResponse.getWriter();
		writer.write(optionsJSON.toString());
	}
	
	private List<SubProject> getSubProjects(Long projectId) throws SystemException, NumberFormatException{
		return SubProjectLocalServiceUtil.getSubProjectByProjectId(Long.valueOf(projectId));
	}
	
	public void downloadFile(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		logger.debug("inside download File");
		long purchaseRequestId = ParamUtil.getLong(request, "purchaseRequestId");
		try {
			logger.debug("file to view is " + purchaseRequestId);
			// Download action
			UploadDownloadHelper.downloadFile(purchaseRequestId, response);
		} catch (UploadException ue) {
			logger.error(ue.getMessage());
			response.getWriter().write("failed");

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
	}
	
	public void downloadAllFiles(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		logger.debug("inside download All Files");
		long purchaseRequestId = ParamUtil.getLong(request, "purchaseRequestId");
		try {
			logger.debug("Zip file to view is " + purchaseRequestId);
			// Download action
			UploadDownloadHelper.downloadZipFile(purchaseRequestId,response);
		} catch (UploadException ue) {
			logger.error(ue.getMessage());
			response.getWriter().write("failed");

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
	}
	private String getRedirect(String actionStatus){
		logger.debug("actionStatus inside get redirect is "+actionStatus);
		String redirect="/group/vodafone/home";
		if(PurchaseRequestStatus.PR_ASSIGNED.getLabel().equals(actionStatus)) {
			redirect="/group/vodafone/asset-insert";
		} else if (PurchaseRequestStatus.ASSET_RECEIVED.getLabel().equals(actionStatus)) {
			redirect="/group/vodafone/sc-insert";
		} else if (PurchaseRequestStatus.SC_RECEIVED.getLabel().equals(actionStatus)) {
			redirect="/group/vodafone/po-insert";
		}
		logger.debug("redirect inside get redirect is "+redirect);
		return redirect;
		
	}
	private void setHasFile(List<PrListDTO> prList) {
		
		for(PrListDTO prListDTO : prList) {
			try {
				prListDTO.setHasFile(UploadDownloadHelper.isPRHasUploadedFiles(prListDTO.getPurchaseRequestId()));
			} catch (IOException e) {
				prListDTO.setHasFile(false);
			}
		}
	}
	
}

