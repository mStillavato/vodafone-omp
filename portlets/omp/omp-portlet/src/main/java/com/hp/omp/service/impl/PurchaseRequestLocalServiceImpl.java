/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.hp.omp.model.PRAudit;
import com.hp.omp.model.Position;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.PurchaseRequestModel;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.custom.PositionStatus;
import com.hp.omp.model.custom.PrListDTO;
import com.hp.omp.model.custom.PurchaseRequestStatus;
import com.hp.omp.model.custom.SearchDTO;
import com.hp.omp.model.impl.PRAuditImpl;
import com.hp.omp.model.impl.PurchaseRequestImpl;
import com.hp.omp.service.PRAuditLocalServiceUtil;
import com.hp.omp.service.PositionLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.hp.omp.service.base.PurchaseRequestLocalServiceBaseImpl;
import com.hp.omp.service.persistence.PurchaseRequestFinderUtil;
import com.hp.omp.service.persistence.PurchaseRequestUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.security.auth.PrincipalThreadLocal;

/**
 * The implementation of the purchase request local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.PurchaseRequestLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.hp.omp.service.base.PurchaseRequestLocalServiceBaseImpl
 * @see com.hp.omp.service.PurchaseRequestLocalServiceUtil
 */
public class PurchaseRequestLocalServiceImpl
	extends PurchaseRequestLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.hp.omp.service.PurchaseRequestLocalServiceUtil} to access the purchase request local service.
	 */
	
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public PurchaseRequest addPurchaseRequest(PurchaseRequest purchaseRequest)
			throws SystemException {
		PurchaseRequest newPurchseRequest = super.addPurchaseRequest(purchaseRequest);
		addAuditPR(newPurchseRequest, newPurchseRequest.getPurchaseRequestStatus());
		return newPurchseRequest;
	}
	
	
	@Override
	@Indexable(type = IndexableType.DELETE)
	public PurchaseRequest deletePurchaseRequest(long purchaseRequestId)
			throws PortalException, SystemException {
		PurchaseRequest purchaseRequest = PurchaseRequestLocalServiceUtil.getPurchaseRequest(purchaseRequestId);
		addAuditPR(purchaseRequest, PurchaseRequestStatus.DELETED);
		return super.deletePurchaseRequest(purchaseRequestId);
	}
	
	@Override
	@Indexable(type = IndexableType.DELETE)
	public PurchaseRequest deletePurchaseRequest(PurchaseRequest purchaseRequest)
			throws SystemException {
		addAuditPR(purchaseRequest, PurchaseRequestStatus.DELETED);
		return super.deletePurchaseRequest(purchaseRequest);
	}
	
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public PurchaseRequest updatePurchaseRequest(PurchaseRequest purchaseRequest)
			throws SystemException {
		PurchaseRequest newPurchaseRequest = super.updatePurchaseRequest(purchaseRequest);
		addAuditPR(newPurchaseRequest, newPurchaseRequest.getPurchaseRequestStatus());
		return newPurchaseRequest;
	}
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public PurchaseRequest updatePurchaseRequest(
			PurchaseRequest purchaseRequest, boolean merge)
			throws SystemException {
		PurchaseRequest newPurchaseRequest = super.updatePurchaseRequest(purchaseRequest, merge);
		addAuditPR(newPurchaseRequest, newPurchaseRequest.getPurchaseRequestStatus());
		return newPurchaseRequest;
	}
	
	private void addAuditPR(PurchaseRequest purchaseRequest, PurchaseRequestStatus status) throws SystemException {
		PRAudit prAudit = new PRAuditImpl();
		prAudit.setNew(true);
		prAudit.setStatus(status.getCode());
		long userId =  PrincipalThreadLocal.getUserId();
		prAudit.setUserId(userId);
		prAudit.setPurchaseRequestId(purchaseRequest.getPurchaseRequestId());
		prAudit.setChangeDate(new Date());
		PRAuditLocalServiceUtil.addPRAudit(prAudit);
	}
	
	public PurchaseRequest getPurchaseRequestByStatus(long prId, int status) throws SystemException{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseRequest.class);
		dynamicQuery.add(PropertyFactoryUtil.forName("purchaseRequestId").eq(prId));
		dynamicQuery.add(PropertyFactoryUtil.forName("status").eq(status));
		 
		PurchaseRequest purchaseRequest = new PurchaseRequestImpl();
		List<PurchaseRequest>  prList = PurchaseRequestUtil.findWithDynamicQuery(dynamicQuery);
		if(prList != null && ! (prList.isEmpty()) ){
			purchaseRequest = prList.get(0);
		}
		return purchaseRequest;
	}
	
	public List<PurchaseRequest> getPurchaseRequests(
			List<Long> purchaseRequestsIDList) throws SystemException {

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseRequest.class);
		dynamicQuery.add(PropertyFactoryUtil.forName("purchaseRequestId").in(purchaseRequestsIDList));

		return PurchaseRequestUtil.findWithDynamicQuery(dynamicQuery);
	}
	
	public List<PurchaseRequest> getPurchaseRequesListByStatus(int status) throws SystemException{
		
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseRequestModel.class);
		dynamicQuery.add(PropertyFactoryUtil.forName("status").eq(status));
		List<PurchaseRequest>  prList = PurchaseRequestUtil.findWithDynamicQuery(dynamicQuery);
		return prList;
	}
	public List<PurchaseRequest> getPurchaseRequesListByStatus(int status, int start, int end, String orderby, String order) throws SystemException{
		
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseRequest.class);
		dynamicQuery.add(PropertyFactoryUtil.forName("status").eq(status));
		if("asc".equals(order)) {
			dynamicQuery.addOrder(OrderFactoryUtil.asc(orderby));
		} else {
			dynamicQuery.addOrder(OrderFactoryUtil.desc(orderby));
		}
		dynamicQuery.setLimit(start, end);
		@SuppressWarnings("unchecked")
		List<PurchaseRequest>  prList = PurchaseRequestLocalServiceUtil.dynamicQuery(dynamicQuery);
		return prList;
	}
	
	public Long getPurchaseRequesListByStatusCount(int status,boolean automatic) throws SystemException {
		DynamicQuery countQuery = DynamicQueryFactoryUtil.forClass(PurchaseRequestModel.class);
		countQuery.add(PropertyFactoryUtil.forName("status").eq(status));
		countQuery.add(PropertyFactoryUtil.forName("automatic").eq(automatic));
		countQuery.setProjection(ProjectionFactoryUtil.rowCount());
		@SuppressWarnings("unchecked")
		List<Long> resultQuery =  PurchaseRequestLocalServiceUtil.dynamicQuery(countQuery);
		return (resultQuery == null || resultQuery.size() == 0) ?  0L  : resultQuery.get(0);
	}
	
	
	
	public List<PurchaseRequest> getPurchaseRequesListByStatusAndType(int status,boolean automatic,int start, int end, String orderby, String order) throws SystemException{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseRequestModel.class);
		dynamicQuery.add(PropertyFactoryUtil.forName("automatic").eq(automatic));
		dynamicQuery.add(PropertyFactoryUtil.forName("status").eq(status));
		if("asc".equals(order)) {
			dynamicQuery.addOrder(OrderFactoryUtil.asc(orderby));
		} else {
			dynamicQuery.addOrder(OrderFactoryUtil.desc(orderby));
		}
		dynamicQuery.setLimit(start, end);
		@SuppressWarnings("unchecked")
		List<PurchaseRequest>  prList = PurchaseRequestLocalServiceUtil.dynamicQuery(dynamicQuery);
		return prList;
	}
	
	
	public List<PurchaseRequest> getPurchaseRequesListByPositionStatusAndType(Set<Long> purchaseRequestIdList,boolean automatic, int start, int end, String orderby, String order) throws Exception{
		
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseRequest.class);
		
		dynamicQuery.add(PropertyFactoryUtil.forName("automatic").eq(automatic));
		dynamicQuery.add(RestrictionsFactoryUtil.in("purchaseRequestId", purchaseRequestIdList));
		dynamicQuery.setLimit(start, end);
		if("asc".equals(order)) {
			dynamicQuery.addOrder(OrderFactoryUtil.asc(orderby));
		} else {
			dynamicQuery.addOrder(OrderFactoryUtil.desc(orderby));
		}
		@SuppressWarnings("unchecked")
		List<PurchaseRequest>  purchaseRequestList=PurchaseRequestLocalServiceUtil.dynamicQuery(dynamicQuery);
		return purchaseRequestList;
	}
	
	public List<PurchaseRequest> getPurchaseRequestListByPositionStatusAndType(int positionStatus,boolean automatic, int start, int end, String orderby, String order) throws Exception{
		
		DynamicQuery positionQuery = getPositionByStatusQuery(positionStatus);
		
		DynamicQuery prQuery = DynamicQueryFactoryUtil.forClass(PurchaseRequest.class);
		prQuery.add(PropertyFactoryUtil.forName("automatic").eq(automatic));
		prQuery.add(PropertyFactoryUtil.forName("purchaseRequestId").in(positionQuery));
		prQuery.setLimit(start, end);
		if("asc".equals(order)) {
			prQuery.addOrder(OrderFactoryUtil.asc(orderby));
		} else {
			prQuery.addOrder(OrderFactoryUtil.desc(orderby));
		}
		@SuppressWarnings("unchecked")
		List<PurchaseRequest>  purchaseRequestList=PurchaseRequestLocalServiceUtil.dynamicQuery(prQuery);
		return purchaseRequestList;
	}
	
public Long getPurchaseRequestListByPositionStatusAndTypeCount(int positionStatus,boolean automatic) throws Exception{
		
		DynamicQuery positionQuery = getPositionByStatusQuery(positionStatus);
		
		DynamicQuery countQuery = DynamicQueryFactoryUtil.forClass(PurchaseRequest.class);
		countQuery.add(PropertyFactoryUtil.forName("automatic").eq(automatic));
		countQuery.add(PropertyFactoryUtil.forName("purchaseRequestId").in(positionQuery));
		countQuery.setProjection(ProjectionFactoryUtil.rowCount());
		@SuppressWarnings("unchecked")
		List<Long> resultQuery =  PurchaseRequestLocalServiceUtil.dynamicQuery(countQuery);
		return (resultQuery == null || resultQuery.size() == 0) ?  0L  : resultQuery.get(0);
	}
	
	public List<PurchaseRequest> getPRListByStatusAndCostCenter(int status, long costCenterId,int roleCode, int start, int end, String orderby, String order) throws SystemException{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseRequest.class);
		dynamicQuery.add(PropertyFactoryUtil.forName("status").eq(status));
		if(OmpRoles.OMP_ENG.equals(OmpRoles.getStatus(roleCode)) || OmpRoles.OMP_OBSERVER.equals(OmpRoles.getStatus(roleCode))) {
			dynamicQuery.add(PropertyFactoryUtil.forName("costCenterId").eq(String.valueOf(costCenterId)));
		}
		if("asc".equals(order)) {
			dynamicQuery.addOrder(OrderFactoryUtil.asc(orderby));
		} else {
			dynamicQuery.addOrder(OrderFactoryUtil.desc(orderby));
		}
		dynamicQuery.setLimit(start, end);
		@SuppressWarnings("unchecked")
		List<PurchaseRequest>  prList = PurchaseRequestLocalServiceUtil.dynamicQuery(dynamicQuery);
		return prList;
	}
	public Long getPRListByStatusAndCostCenterCount(int status, long costCenterId, int roleCode) throws SystemException{
		DynamicQuery countQuery = DynamicQueryFactoryUtil.forClass(PurchaseRequest.class);
		countQuery.add(PropertyFactoryUtil.forName("status").eq(status));
		if(!OmpRoles.OMP_CONTROLLER.equals(OmpRoles.getStatus(roleCode))) {
			countQuery.add(PropertyFactoryUtil.forName("costCenterId").eq(String.valueOf(costCenterId)));
		}
		countQuery.setProjection(ProjectionFactoryUtil.rowCount());
		@SuppressWarnings("unchecked")
		List<Long> resultQuery =  PurchaseRequestLocalServiceUtil.dynamicQuery(countQuery);
		return (resultQuery == null || resultQuery.size() == 0) ?  0L  : resultQuery.get(0);
	}
	public List<PrListDTO> getPurchaseRequestList(SearchDTO searchDTO,int roleCode, long userId){
		return PurchaseRequestFinderUtil.getPurchaseRequestList(searchDTO, roleCode, userId);
	}
	
	public List<PrListDTO> prSearch(SearchDTO searchDTO,int roleCode, long userId){
		return PurchaseRequestFinderUtil.prSearch(searchDTO, roleCode, userId);
	}
	
	public Long getPurchaseRequestCount(SearchDTO searchDTO,int roleCode, long userId) {
		return PurchaseRequestFinderUtil.getPurchaseRequestCount(searchDTO, roleCode, userId);
	}
	public List<PrListDTO> getAntexPurchaseRequestList(SearchDTO searchDTO){
		return PurchaseRequestFinderUtil.getAntexPurchaseRequestList(searchDTO);
	}
	
	public Long getAntexPurchaseRequestCount(SearchDTO searchDTO) {
		return PurchaseRequestFinderUtil.getAntexPurchaseRequestCount(searchDTO);
	}
	private DynamicQuery getPositionByStatusQuery(int positionStatus)  throws Exception{
		DynamicQuery positionQuery = DynamicQueryFactoryUtil.forClass(Position.class);
		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.isNotNull("purchaseRequestId");
		if(positionStatus == PositionStatus.NEW.getCode()){
			criterion = getNEWStatusQuery(criterion);
		}else if(positionStatus == PositionStatus.ASSET_RECEIVED.getCode()){
			criterion = getAssetReceivedStatusQuery(criterion);
		}else if(positionStatus == PositionStatus.SC_RECEIVED.getCode()){
			criterion = getSCReceivedStatusQuery(criterion);
		}else if(positionStatus == PositionStatus.PO_RECEIVED.getCode()){
			criterion = getPOReceivedStatusQuery(criterion);
		}else{
			throw new Exception("position status not defined"); 
		}
		positionQuery.add(criterion); 
		positionQuery.setProjection(ProjectionFactoryUtil.distinct(ProjectionFactoryUtil.property("purchaseRequestId"))); 
		
		return positionQuery;
	}

	public List<PurchaseRequest> getPurchaseRequestByProjectId(String projectId) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseRequest.class);
		
		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("projectId", projectId);
		
		dynamicQuery.add(criterion); 
		@SuppressWarnings("unchecked")
		List<PurchaseRequest>  purchaseRequestList = PurchaseRequestLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		return purchaseRequestList;
	}
	
	public List<PurchaseRequest> getPurchaseRequestBySubProjectId(String subProjectId) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseRequest.class);
		
		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("subProjectId", subProjectId);
		
		dynamicQuery.add(criterion); 
		@SuppressWarnings("unchecked")
		List<PurchaseRequest>  purchaseRequestList = PurchaseRequestLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		return purchaseRequestList;
	}
	
	public List<PurchaseRequest> getPurchaseRequestByVendorId(String vendorId) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseRequest.class);
		
		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("vendorId", vendorId);
		
		dynamicQuery.add(criterion); 
		@SuppressWarnings("unchecked")
		List<PurchaseRequest>  purchaseRequestList = PurchaseRequestLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		return purchaseRequestList;
	}
	
	public List<PurchaseRequest> getPurchaseRequestByBudgetCategoryId(String budgetCategoryId) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseRequest.class);
		
		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("budgetCategoryId", budgetCategoryId);
		
		dynamicQuery.add(criterion); 
		@SuppressWarnings("unchecked")
		List<PurchaseRequest>  purchaseRequestList = PurchaseRequestLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		return purchaseRequestList;
	}
	
	public List<PurchaseRequest> getPurchaseRequestByBudgetSubCategoryId(String budgetSubCategoryId) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseRequest.class);
		
		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("budgetSubCategoryId", budgetSubCategoryId);
		
		dynamicQuery.add(criterion); 
		@SuppressWarnings("unchecked")
		List<PurchaseRequest>  purchaseRequestList = PurchaseRequestLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		return purchaseRequestList;
	}
	

	public List<PurchaseRequest> getPurchaseRequestByOdnpNameId(String odnpNameId) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseRequest.class);
		
		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("odnpNameId", odnpNameId);
		
		dynamicQuery.add(criterion); 
		@SuppressWarnings("unchecked")
		List<PurchaseRequest>  purchaseRequestList = PurchaseRequestLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		return purchaseRequestList;
	}
	

	public List<PurchaseRequest> getPurchaseRequestByMacroDriverId(String macroDriver) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseRequest.class);
		
		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("macroDriverId", macroDriver);
		
		dynamicQuery.add(criterion); 
		@SuppressWarnings("unchecked")
		List<PurchaseRequest>  purchaseRequestList = PurchaseRequestLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		return purchaseRequestList;
	}
	
	public List<PurchaseRequest> getPurchaseRequestByMicroDriverId(String microDriver) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseRequest.class);
		
		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("microDriverId", microDriver);
		
		dynamicQuery.add(criterion); 
		@SuppressWarnings("unchecked")
		List<PurchaseRequest>  purchaseRequestList = PurchaseRequestLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		return purchaseRequestList;
	}
	

	private Criterion getNEWStatusQuery(Criterion criterion){
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("assetNumber"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("shoppingCart"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("poNumber"));
		
		return criterion;
	}
	
	private Criterion getAssetReceivedStatusQuery(Criterion criterion){
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("assetNumber"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("shoppingCart"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("poNumber"));
		
		return criterion;
	}
	
	private Criterion getSCReceivedStatusQuery(Criterion criterion){
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("assetNumber"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("shoppingCart"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("poNumber"));
		
		return criterion;
	}
	private Criterion getPOReceivedStatusQuery(Criterion criterion){
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("assetNumber"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("shoppingCart"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("poNumber"));
		
		return criterion;
	}
}