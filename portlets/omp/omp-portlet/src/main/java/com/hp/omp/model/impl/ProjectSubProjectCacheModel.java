package com.hp.omp.model.impl;

import com.hp.omp.model.ProjectSubProject;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

/**
 * The cache model class for representing ProjectSubProject in entity cache.
 *
 * @author HP Egypt team
 * @see ProjectSubProject
 * @generated
 */
public class ProjectSubProjectCacheModel implements CacheModel<ProjectSubProject>,
    Serializable {
    public long projectSubProjectId;
    public long projectId;
    public long subProjectId;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{projectSubProjectId=");
        sb.append(projectSubProjectId);
        sb.append(", projectId=");
        sb.append(projectId);
        sb.append(", subProjectId=");
        sb.append(subProjectId);
        sb.append("}");

        return sb.toString();
    }

    public ProjectSubProject toEntityModel() {
        ProjectSubProjectImpl projectSubProjectImpl = new ProjectSubProjectImpl();

        projectSubProjectImpl.setProjectSubProjectId(projectSubProjectId);
        projectSubProjectImpl.setProjectId(projectId);
        projectSubProjectImpl.setSubProjectId(subProjectId);

        projectSubProjectImpl.resetOriginalValues();

        return projectSubProjectImpl;
    }
}
