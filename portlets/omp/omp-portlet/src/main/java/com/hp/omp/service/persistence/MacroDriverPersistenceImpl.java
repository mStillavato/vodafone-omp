package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchMacroDriverException;
import com.hp.omp.model.MacroDriver;
import com.hp.omp.model.impl.MacroDriverImpl;
import com.hp.omp.model.impl.MacroDriverModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the macro driver service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see MacroDriverPersistence
 * @see MacroDriverUtil
 * @generated
 */
public class MacroDriverPersistenceImpl extends BasePersistenceImpl<MacroDriver>
    implements MacroDriverPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link MacroDriverUtil} to access the macro driver persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = MacroDriverImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(MacroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroDriverModelImpl.FINDER_CACHE_ENABLED, MacroDriverImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(MacroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroDriverModelImpl.FINDER_CACHE_ENABLED, MacroDriverImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(MacroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroDriverModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_MACRODRIVER = "SELECT macroDriver FROM MacroDriver macroDriver";
    private static final String _SQL_COUNT_MACRODRIVER = "SELECT COUNT(macroDriver) FROM MacroDriver macroDriver";
    private static final String _ORDER_BY_ENTITY_ALIAS = "macroDriver.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No MacroDriver exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(MacroDriverPersistenceImpl.class);
    private static MacroDriver _nullMacroDriver = new MacroDriverImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<MacroDriver> toCacheModel() {
                return _nullMacroDriverCacheModel;
            }
        };

    private static CacheModel<MacroDriver> _nullMacroDriverCacheModel = new CacheModel<MacroDriver>() {
            public MacroDriver toEntityModel() {
                return _nullMacroDriver;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the macro driver in the entity cache if it is enabled.
     *
     * @param macroDriver the macro driver
     */
    public void cacheResult(MacroDriver macroDriver) {
        EntityCacheUtil.putResult(MacroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroDriverImpl.class, macroDriver.getPrimaryKey(), macroDriver);

        macroDriver.resetOriginalValues();
    }

    /**
     * Caches the macro drivers in the entity cache if it is enabled.
     *
     * @param macroDrivers the macro drivers
     */
    public void cacheResult(List<MacroDriver> macroDrivers) {
        for (MacroDriver macroDriver : macroDrivers) {
            if (EntityCacheUtil.getResult(
                        MacroDriverModelImpl.ENTITY_CACHE_ENABLED,
                        MacroDriverImpl.class, macroDriver.getPrimaryKey()) == null) {
                cacheResult(macroDriver);
            } else {
                macroDriver.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all macro drivers.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(MacroDriverImpl.class.getName());
        }

        EntityCacheUtil.clearCache(MacroDriverImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the macro driver.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(MacroDriver macroDriver) {
        EntityCacheUtil.removeResult(MacroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroDriverImpl.class, macroDriver.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<MacroDriver> macroDrivers) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (MacroDriver macroDriver : macroDrivers) {
            EntityCacheUtil.removeResult(MacroDriverModelImpl.ENTITY_CACHE_ENABLED,
                MacroDriverImpl.class, macroDriver.getPrimaryKey());
        }
    }

    /**
     * Creates a new macro driver with the primary key. Does not add the macro driver to the database.
     *
     * @param macroDriverId the primary key for the new macro driver
     * @return the new macro driver
     */
    public MacroDriver create(long macroDriverId) {
        MacroDriver macroDriver = new MacroDriverImpl();

        macroDriver.setNew(true);
        macroDriver.setPrimaryKey(macroDriverId);

        return macroDriver;
    }

    /**
     * Removes the macro driver with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param macroDriverId the primary key of the macro driver
     * @return the macro driver that was removed
     * @throws com.hp.omp.NoSuchMacroDriverException if a macro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroDriver remove(long macroDriverId)
        throws NoSuchMacroDriverException, SystemException {
        return remove(Long.valueOf(macroDriverId));
    }

    /**
     * Removes the macro driver with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the macro driver
     * @return the macro driver that was removed
     * @throws com.hp.omp.NoSuchMacroDriverException if a macro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MacroDriver remove(Serializable primaryKey)
        throws NoSuchMacroDriverException, SystemException {
        Session session = null;

        try {
            session = openSession();

            MacroDriver macroDriver = (MacroDriver) session.get(MacroDriverImpl.class,
                    primaryKey);

            if (macroDriver == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchMacroDriverException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(macroDriver);
        } catch (NoSuchMacroDriverException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected MacroDriver removeImpl(MacroDriver macroDriver)
        throws SystemException {
        macroDriver = toUnwrappedModel(macroDriver);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, macroDriver);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(macroDriver);

        return macroDriver;
    }

    @Override
    public MacroDriver updateImpl(com.hp.omp.model.MacroDriver macroDriver,
        boolean merge) throws SystemException {
        macroDriver = toUnwrappedModel(macroDriver);

        boolean isNew = macroDriver.isNew();

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, macroDriver, merge);

            macroDriver.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(MacroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroDriverImpl.class, macroDriver.getPrimaryKey(), macroDriver);

        return macroDriver;
    }

    protected MacroDriver toUnwrappedModel(MacroDriver macroDriver) {
        if (macroDriver instanceof MacroDriverImpl) {
            return macroDriver;
        }

        MacroDriverImpl macroDriverImpl = new MacroDriverImpl();

        macroDriverImpl.setNew(macroDriver.isNew());
        macroDriverImpl.setPrimaryKey(macroDriver.getPrimaryKey());

        macroDriverImpl.setMacroDriverId(macroDriver.getMacroDriverId());
        macroDriverImpl.setMacroDriverName(macroDriver.getMacroDriverName());
        macroDriverImpl.setDisplay(macroDriver.isDisplay());

        return macroDriverImpl;
    }

    /**
     * Returns the macro driver with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the macro driver
     * @return the macro driver
     * @throws com.liferay.portal.NoSuchModelException if a macro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MacroDriver findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the macro driver with the primary key or throws a {@link com.hp.omp.NoSuchMacroDriverException} if it could not be found.
     *
     * @param macroDriverId the primary key of the macro driver
     * @return the macro driver
     * @throws com.hp.omp.NoSuchMacroDriverException if a macro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroDriver findByPrimaryKey(long macroDriverId)
        throws NoSuchMacroDriverException, SystemException {
        MacroDriver macroDriver = fetchByPrimaryKey(macroDriverId);

        if (macroDriver == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + macroDriverId);
            }

            throw new NoSuchMacroDriverException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                macroDriverId);
        }

        return macroDriver;
    }

    /**
     * Returns the macro driver with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the macro driver
     * @return the macro driver, or <code>null</code> if a macro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MacroDriver fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the macro driver with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param macroDriverId the primary key of the macro driver
     * @return the macro driver, or <code>null</code> if a macro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroDriver fetchByPrimaryKey(long macroDriverId)
        throws SystemException {
        MacroDriver macroDriver = (MacroDriver) EntityCacheUtil.getResult(MacroDriverModelImpl.ENTITY_CACHE_ENABLED,
                MacroDriverImpl.class, macroDriverId);

        if (macroDriver == _nullMacroDriver) {
            return null;
        }

        if (macroDriver == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                macroDriver = (MacroDriver) session.get(MacroDriverImpl.class,
                        Long.valueOf(macroDriverId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (macroDriver != null) {
                    cacheResult(macroDriver);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(MacroDriverModelImpl.ENTITY_CACHE_ENABLED,
                        MacroDriverImpl.class, macroDriverId, _nullMacroDriver);
                }

                closeSession(session);
            }
        }

        return macroDriver;
    }

    /**
     * Returns all the macro drivers.
     *
     * @return the macro drivers
     * @throws SystemException if a system exception occurred
     */
    public List<MacroDriver> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the macro drivers.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of macro drivers
     * @param end the upper bound of the range of macro drivers (not inclusive)
     * @return the range of macro drivers
     * @throws SystemException if a system exception occurred
     */
    public List<MacroDriver> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the macro drivers.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of macro drivers
     * @param end the upper bound of the range of macro drivers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of macro drivers
     * @throws SystemException if a system exception occurred
     */
    public List<MacroDriver> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<MacroDriver> list = (List<MacroDriver>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_MACRODRIVER);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_MACRODRIVER;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<MacroDriver>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<MacroDriver>) QueryUtil.list(q, getDialect(),
                            start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the macro drivers from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (MacroDriver macroDriver : findAll()) {
            remove(macroDriver);
        }
    }

    /**
     * Returns the number of macro drivers.
     *
     * @return the number of macro drivers
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_MACRODRIVER);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the macro driver persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.MacroDriver")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<MacroDriver>> listenersList = new ArrayList<ModelListener<MacroDriver>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<MacroDriver>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(MacroDriverImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
