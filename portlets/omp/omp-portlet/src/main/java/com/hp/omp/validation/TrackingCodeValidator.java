package com.hp.omp.validation;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.TrackingCodes;
import com.hp.omp.service.TrackingCodesLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;

public class TrackingCodeValidator {

	private static final Logger logger = LoggerFactory.getLogger(TrackingCodeValidator.class);

	public static boolean validateTrackingCode(TrackingCodes trackingCode, List<String> errors) throws SystemException {
		
		if(emptyMandatoryField(trackingCode, errors)) {
			logger.debug("Validation error empty mandatory field");
			return false;
		}
		
		if(codeNameIsNotUnique(trackingCode)) {
			logger.debug("Validation error tracking code name is not unique");
			errors.add("duplicate tracking code");
			return false;
		} 

		return true;
	}

	public static boolean validateUploadedTrackingCode(TrackingCodes trackingCode, List<String> errors) throws SystemException {
		return validateTrackingCode(trackingCode, errors);
	}
	
	private static boolean codeNameIsNotUnique(TrackingCodes newTrackingCode) throws SystemException {
		TrackingCodes trackingCode = TrackingCodesLocalServiceUtil.getTrackingCodeByName(newTrackingCode.getTrackingCodeName());
		if(trackingCode != null) {
			if(trackingCode.getTrackingCodeId() == newTrackingCode.getTrackingCodeId()) {
				return false;
			} else {
				return true;
			}
		}
		return false;
	}

	private static boolean emptyMandatoryField(TrackingCodes trackingCode,
			List<String> errors) {
		
		boolean invalid = false;
		
		if(trackingCode.getTrackingCodeName() == null || "".equals(trackingCode.getTrackingCodeName().trim())) {
			invalid = true;
			errors.add("name-required");
		}
		
		if(trackingCode.getTrackingCodeDescription() == null || "".equals(trackingCode.getTrackingCodeDescription().trim())) {
			invalid = true;
			errors.add("description-required");
		}
		
		if(trackingCode.getProjectId() == null || "".equals(trackingCode.getProjectId())) {
			invalid = true;
			errors.add("project-required");
		}
		
		if(trackingCode.getWbsCodeId() == null || "".equals(trackingCode.getWbsCodeId())) {
			invalid = true;
			errors.add("wbscode-required");
		}
		
		return invalid;
	}
}
