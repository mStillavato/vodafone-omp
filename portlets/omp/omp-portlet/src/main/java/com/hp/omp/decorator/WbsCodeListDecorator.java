package com.hp.omp.decorator;

import java.io.Serializable;

import org.displaytag.decorator.TableDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.WbsCode;

public class WbsCodeListDecorator extends TableDecorator implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2685221218697856230L;

	Logger logger = LoggerFactory.getLogger(WbsCodeListDecorator.class);
	
	public String getWbsCodeId() {
		
        return getRemoveAction();
    }
	
	public String getWbsCodeName() {
		
        return getUpdateAction();
    }
	
	public String getRemoveAction() {
		
        StringBuilder sb = new StringBuilder();
        WbsCode item = (WbsCode) this.getCurrentRowObject();
      
        long wbsCodeId = item.getWbsCodeId();
        String wbsCodeName = item.getWbsCodeName();
        sb.append("<a href=\"javascript:removeWbsCode("+wbsCodeId+",'"+wbsCodeName+"')\"");
        sb.append("\">");
        sb.append(" Remove ");
        sb.append("</a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
    }	
	
	public String getUpdateAction() {
		
		WbsCode item = (WbsCode) this.getCurrentRowObject();
		StringBuilder sb = new StringBuilder();
       
      
        long wbsCodeId = item.getWbsCodeId();
        String wbsCodeName = item.getWbsCodeName();
        sb.append("<a onclick=\"javascript:updateWbsCode(this, "+wbsCodeId+")\" href='"+wbsCodeName+"'");
        sb.append("\"> ");
        sb.append(wbsCodeName);
        sb.append(" </a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
        
    }

}
