package com.hp.omp.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletMode;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletSession;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.helper.PurchaseRequestPortletHelper;
import com.hp.omp.helper.UploadDownloadHelper;
import com.hp.omp.model.Project;
import com.hp.omp.model.SubProject;
import com.hp.omp.model.WbsCode;
import com.hp.omp.model.custom.IAndCType;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.custom.PrListDTO;
import com.hp.omp.model.custom.PurchaseRequestStatus;
import com.hp.omp.model.custom.SearchDTO;
import com.hp.omp.service.CostCenterLocalServiceUtil;
import com.hp.omp.service.ProjectLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.hp.omp.service.SubProjectLocalServiceUtil;
import com.hp.omp.service.WbsCodeLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class antexPRList
 */
public class AntexPRListPortlet extends MVCPortlet {

	Logger logger = LoggerFactory.getLogger(AntexPRListPortlet.class);
	
	public static final String DATE_FORMAT = "MM/dd/yyyy"; 
	private static String SUBMITTED_STATE = "0";
	private static String PR_Assigned_STATE = "1";
	private static String ASSET_INSERT_STATE = "2"; 
	private static String SC_INSERT_STATE = "3"; 
	private static String PENDING_APPROVAL_STATE = "4";

	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		
		logger.info("INFO: public void doView() - Start");
		
		PortletPreferences preferences = renderRequest.getPreferences();
		String status = preferences.getValue("status", SUBMITTED_STATE);
		String actionStatus = "";
		
		if(PR_Assigned_STATE.equals(status)) {
			actionStatus =PurchaseRequestStatus.PR_ASSIGNED.getLabel();
		} else if (ASSET_INSERT_STATE.equals(status)) {
			actionStatus =PurchaseRequestStatus.ASSET_RECEIVED.getLabel();
		} else if (SC_INSERT_STATE.equals(status)) {
			actionStatus = PurchaseRequestStatus.SC_RECEIVED.getLabel();
		} else if (PENDING_APPROVAL_STATE.equals(status)) {
			actionStatus = PurchaseRequestStatus.PENDING_IC_APPROVAL.getLabel();
		}

		renderRequest.setAttribute("actionStatus", actionStatus);
		try {
			renderRequest.setAttribute("costCentersList", CostCenterLocalServiceUtil.getCostCenters(0,CostCenterLocalServiceUtil.getCostCentersCount()));

			List<WbsCode> wbsCodeList = WbsCodeLocalServiceUtil.getWbsCodes(0, WbsCodeLocalServiceUtil.getWbsCodesCount());
			renderRequest.setAttribute("allWbsCodes", wbsCodeList);

			List<Project> projectsList = ProjectLocalServiceUtil.getProjects(0, ProjectLocalServiceUtil.getProjectsCount());
			renderRequest.setAttribute("allProjects", projectsList);

		} catch (SystemException e) {
			logger.error("Error in cost center list",e);
		}
		
		super.doView(renderRequest, renderResponse);
		
		logger.info("INFO: public void doView() - End");
	}
	
	
	@ProcessAction(name="setPortletStatus")
	public void setPortletStatus(ActionRequest request, ActionResponse response){
		
		logger.info("INFO: public void setPortletStatus() - Start");
		
		PortletPreferences preferences = null;
		
		try {
			String statusString = request.getParameter("status");
			preferences = request.getPreferences();
			preferences.setValue("status", statusString);
			preferences.store();
			response.setPortletMode(PortletMode.VIEW);
		} catch (Exception e) {
			logger.error("Unable to Set Prefernces", e);
		}
		
		logger.info("INFO: public void setPortletStatus() - End");
	}

	
	@Override
	public void doEdit(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		
		logger.info("INFO: public void doEdit() - Start");
		
		PortletPreferences preferences = renderRequest.getPreferences();
		String mode = preferences.getValue("status", SUBMITTED_STATE);
		renderRequest.setAttribute("status", mode);
		super.doEdit(renderRequest, renderResponse);
		
		logger.info("INFO: public void doEdit() - End");
	}

	private void handleAntexPrList(ResourceRequest request, ResourceResponse response) {
		
		logger.info("INFO: private void handleAntexPrList() - Start");

		String jspPage = "/html/antexprlist/prlist.jsp";
		boolean automatic = ParamUtil.getBoolean(request, "automatic");
		String actionStatus = ParamUtil.getString(request,"actionStatus");
		String redirect = getRedirect(actionStatus);
		Integer pageNumber = ParamUtil.getInteger(request, "pageNumber", 1);
		String orderby = ParamUtil.getString(request, "orderby", "purchaseRequestId");
		String order = ParamUtil.getString(request, "order", "desc");
		int positionStatus= 0;
		boolean miniSeach = ParamUtil.getBoolean(request, "isMini",true);

		Integer pageSize = OmpHelper.PAGE_SIZE;
		long prId = ParamUtil.getLong(request, "prId");
		long costCentId = ParamUtil.getLong(request, "costCenter");
		String fYear = ParamUtil.getString(request, "fYear");
		String project = ParamUtil.getString(request, "project");
		String subProject = ParamUtil.getString(request, "subProject");
		String wbsCode = ParamUtil.getString(request, "wbsCode");
		String searchKeyWord = ParamUtil.getString(request, "searchKeyWord");
		
		SearchDTO searchDTO = new SearchDTO();
		searchDTO.setPrId(prId);
		searchDTO.setCostCenterId(costCentId);
		searchDTO.setFiscalYear(fYear);
		searchDTO.setProjectName(project);
		searchDTO.setSubProjectName(subProject);
		searchDTO.setWbsCode(wbsCode);
		searchDTO.setMiniSearchValue(searchKeyWord);
		searchDTO.setMiniSeach(miniSeach);
		searchDTO.setAutomatic(automatic?1:0);
		searchDTO.setPageSize(pageSize);
		searchDTO.setPageNumber(pageNumber);
		searchDTO.setOrder(order);
		searchDTO.setOrderby(orderby);
		
		if("".equals(actionStatus)||actionStatus==null) {
			positionStatus = PurchaseRequestStatus.SUBMITTED.getCode();
			searchDTO.setPrStatus(positionStatus);
		} else if((PurchaseRequestStatus.SC_RECEIVED.getLabel().equalsIgnoreCase(actionStatus)&& automatic) || PurchaseRequestStatus.PR_ASSIGNED.getLabel().equals(actionStatus)){
			positionStatus = PurchaseRequestStatus.PR_ASSIGNED.getCode();
			searchDTO.setPrStatus(positionStatus);
		} else if(PurchaseRequestStatus.PENDING_IC_APPROVAL.getLabel().equalsIgnoreCase(actionStatus)){
			searchDTO.setPrStatus(PurchaseRequestStatus.PENDING_IC_APPROVAL.getCode());
			searchDTO.setPositionApproved(0);
			searchDTO.setAutomatic(null);
			ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
			long userId = themeDisplay.getUserId();
			if(OmpHelper.isNdso(userId)){
				searchDTO.setPositionICApproval(Integer.toString(IAndCType.ICFIELD.getCode()));
			}else if(OmpHelper.isTc(userId)){
				searchDTO.setPositionICApproval(Integer.toString(IAndCType.ICTEST.getCode()));
			}

		} else {
			positionStatus = PurchaseRequestPortletHelper.getPositionStatusfromActionStatus(actionStatus);
			searchDTO.setPositionStatus(positionStatus);
		} 
		
		try {
			List<PrListDTO> prList = PurchaseRequestLocalServiceUtil.getAntexPurchaseRequestList(searchDTO);
			setHasFile(prList);
			Long totalCount = PurchaseRequestLocalServiceUtil.getAntexPurchaseRequestCount(searchDTO);

			String toSendStatus=PurchaseRequestStatus.SUBMITTED.getLabel();
			if(! "".equals(actionStatus)&& actionStatus!=null)			{
				toSendStatus=actionStatus;
			}

			request.setAttribute("prList", prList);
			request.setAttribute("numberOfPages", getTotalNumberOfPages(totalCount, pageSize));
			request.setAttribute("pageNumber", pageNumber);
			request.setAttribute("actionStatus", actionStatus);
			request.setAttribute("toSendStatus", toSendStatus);
			request.setAttribute("redirect", redirect);
			request.setAttribute("automatic",automatic);

			getPortletContext().getRequestDispatcher(jspPage).include(request, response);

		}catch (Exception e) {
			logger.error("Erro in handle Antex PR list Form", e);
			SessionErrors.add(request, "error-handling");
		}

		logger.info("INFO: private void handleAntexPrList() - End");
	}

	public void searchPR(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws PortletException, IOException {

		logger.info("INFO: public void searchPR() - Start");
		
		String jspPage = "/html/antexprlist/prlist.jsp";

		try {
			ThemeDisplay themeDisplay = (ThemeDisplay)resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);

			String prSearchValue = ParamUtil.getString(resourceRequest, "searchPrValue");
			SearchDTO searchDTO = new SearchDTO();
			searchDTO.setPrSearchValue(prSearchValue);
			OmpRoles userRole = OmpHelper.getUserRole(themeDisplay.getUserId());
			List<PrListDTO> prList = PurchaseRequestLocalServiceUtil.prSearch(searchDTO,userRole.getCode(),themeDisplay.getUserId());
			resourceRequest.setAttribute("prList", prList);
			setHasFile(prList);

			if(prList.size()>0 &&prList.get(0) !=null) {
				resourceRequest.setAttribute("actionStatus", prList.get(0).getStatus());
				resourceRequest.setAttribute("toSendStatus", prList.get(0).getStatus());
				resourceRequest.setAttribute("redirect", "/group/vodafone/home");
				resourceRequest.setAttribute("automatic",prList.get(0).isAutomatic());
			}
			getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);


		} catch(Exception e) {
			logger.error("Error in get search result  for Antex",e );
		}
		
		logger.info("INFO: public void searchPR() - End");
	}

	private Integer getTotalNumberOfPages(Long totalNubmerOfResult, Integer pageSize) {
		return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
	}

	public void serveResource(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		
		logger.info("INFO: public void serveResource() - Start");
		
		String action=ParamUtil.getString(request,"action");

		if("downloadFile".equals(action)){
			downloadFile(request, response);
		}else if("listPR".equals(action)){
			handleAntexPrList(request, response);
		}else if("downloadZipFile".equals(action)){
			downloadAllFiles(request, response);
		}else if("getSubProjects".equals(request.getResourceID())){
			try {
				getSubProjects(request,response);
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}else if("searchPr".equals(request.getResourceID())) {
			searchPR(request, response);
		}

		logger.info("INFO: public void serveResource() - End");
	}

	public void downloadFile(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {

		logger.info("INFO: public void downloadFile() - Start");
		
		long purchaseRequestId = ParamUtil.getLong(request, "purchaseRequestId");
		
		try {
			UploadDownloadHelper.downloadFile(purchaseRequestId, response);
		} catch (UploadException ue) {
			logger.error(ue.getMessage());
			response.getWriter().write("failed");

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
		
		logger.info("INFO: public void downloadFile() - End");
	}

	public void downloadAllFiles(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {

		logger.info("INFO: public void downloadAllFiles() - Start");
		
		long purchaseRequestId = ParamUtil.getLong(request, "purchaseRequestId");
		
		try {
			UploadDownloadHelper.downloadZipFile(purchaseRequestId,response);
		} catch (UploadException ue) {
			logger.error(ue.getMessage());
			response.getWriter().write("failed");

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
		
		logger.info("INFO: public void downloadAllFiles() - End");
	}
	
	
	private String getRedirect(String actionStatus){
		
		logger.info("INFO: private String getRedirect() - Start");
		
		String redirect = "/group/vodafone/home";
		
		if(PurchaseRequestStatus.PR_ASSIGNED.getLabel().equals(actionStatus)) {
			redirect="/group/vodafone/asset-insert";
		} else if (PurchaseRequestStatus.ASSET_RECEIVED.getLabel().equals(actionStatus)) {
			redirect="/group/vodafone/sc-insert";
		} else if (PurchaseRequestStatus.SC_RECEIVED.getLabel().equals(actionStatus)) {
			redirect="/group/vodafone/po-insert";
		}
		
		logger.info("INFO: private String getRedirect() - End");
		
		return redirect;

	}
	private void setHasFile(List<PrListDTO> prList) {
		
		logger.info("INFO: private void setHasFile() - Start");

		for(PrListDTO prListDTO : prList) {
			try {
				prListDTO.setHasFile(UploadDownloadHelper.isPRHasUploadedFiles(prListDTO.getPurchaseRequestId()));
			} catch (IOException e) {
				prListDTO.setHasFile(false);
			}
		}
		
		logger.info("INFO: private void setHasFile() - End");
	}


	public void getSubProjects(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws SystemException, IOException {
		JSONObject optionsJSON = JSONFactoryUtil.createJSONObject();
		Long project = ParamUtil.getLong(resourceRequest, "project");
		List<SubProject> subProjectList = getSubProjects(project);
		if(subProjectList == null){
			subProjectList = new ArrayList<SubProject>();
		}
		JSONArray jsonsubProjectsArray = JSONFactoryUtil.createJSONArray();
		for (SubProject subProject : subProjectList) {
			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("id", subProject.getSubProjectId());
			json.put("name", subProject.getSubProjectName());

			jsonsubProjectsArray.put(json);
		}
		optionsJSON.put("subProjects", jsonsubProjectsArray);
		PrintWriter writer = resourceResponse.getWriter();
		writer.write(optionsJSON.toString());
	}

	private List<SubProject> getSubProjects(Long projectId) throws SystemException, NumberFormatException{
		return SubProjectLocalServiceUtil.getSubProjectByProjectId(Long.valueOf(projectId));
	}
}
