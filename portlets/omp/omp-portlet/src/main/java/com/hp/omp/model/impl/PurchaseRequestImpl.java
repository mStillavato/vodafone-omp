/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.model.impl;

import java.io.IOException;

import com.hp.omp.helper.UploadDownloadHelper;
import com.hp.omp.model.Project;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.custom.PurchaseRequestStatus;
import com.hp.omp.service.ProjectLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.hp.omp.service.persistence.PurchaseRequestUtil;

/**
 * The extended model implementation for the PurchaseRequest service. Represents a row in the &quot;PURCHASE_REQUEST&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.model.PurchaseRequest} interface.
 * </p>
 *
 * @author Brian Wing Shun Chan
 */
public class PurchaseRequestImpl extends PurchaseRequestBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a purchase request model instance should use the {@link com.hp.omp.model.PurchaseRequest} interface instead.
	 */
	String costCenterName;
	
	public String getCostCenterName() {
		return costCenterName;
	}

	public void setCostCenterName(String costCenterName) {
		this.costCenterName = costCenterName;
	}

	public PurchaseRequestImpl() {
	}
	
	public PurchaseRequestStatus getPurchaseRequestStatus(){
		return PurchaseRequestStatus.getStatus(super.getStatus());
	}
	
	public boolean isHasFile() {
		
		try {
			return UploadDownloadHelper.isPRHasUploadedFiles(super.getPurchaseRequestId());
		} catch (IOException e) {
			return false;
		}
	}
	
	public boolean isDisabledProject() {
		
		try {
			PurchaseRequest pr=PurchaseRequestLocalServiceUtil.getPurchaseRequest(super.getPurchaseRequestId());
			Project project=ProjectLocalServiceUtil.getProject(Long.valueOf(pr.getProjectId()));
			return project.getDisplay();
		} catch (Exception e) {
			return false;
		}
	}
}