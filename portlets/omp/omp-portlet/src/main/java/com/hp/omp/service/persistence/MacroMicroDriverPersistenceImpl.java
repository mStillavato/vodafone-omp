package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchMacroMicroDriverException;
import com.hp.omp.model.MacroMicroDriver;
import com.hp.omp.model.impl.MacroMicroDriverImpl;
import com.hp.omp.model.impl.MacroMicroDriverModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the macro micro driver service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see MacroMicroDriverPersistence
 * @see MacroMicroDriverUtil
 * @generated
 */
public class MacroMicroDriverPersistenceImpl extends BasePersistenceImpl<MacroMicroDriver>
    implements MacroMicroDriverPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link MacroMicroDriverUtil} to access the macro micro driver persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = MacroMicroDriverImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_MACRODRIVERID =
        new FinderPath(MacroMicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroMicroDriverModelImpl.FINDER_CACHE_ENABLED,
            MacroMicroDriverImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByMacroDriverId",
            new String[] {
                Long.class.getName(),
                
            "java.lang.Integer", "java.lang.Integer",
                "com.liferay.portal.kernel.util.OrderByComparator"
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MACRODRIVERID =
        new FinderPath(MacroMicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroMicroDriverModelImpl.FINDER_CACHE_ENABLED,
            MacroMicroDriverImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByMacroDriverId",
            new String[] { Long.class.getName() },
            MacroMicroDriverModelImpl.MACRODRIVERID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_MACRODRIVERID = new FinderPath(MacroMicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroMicroDriverModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByMacroDriverId",
            new String[] { Long.class.getName() });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_MICRODRIVERID =
        new FinderPath(MacroMicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroMicroDriverModelImpl.FINDER_CACHE_ENABLED,
            MacroMicroDriverImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByMicroDriverId",
            new String[] {
                Long.class.getName(),
                
            "java.lang.Integer", "java.lang.Integer",
                "com.liferay.portal.kernel.util.OrderByComparator"
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MICRODRIVERID =
        new FinderPath(MacroMicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroMicroDriverModelImpl.FINDER_CACHE_ENABLED,
            MacroMicroDriverImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByMicroDriverId",
            new String[] { Long.class.getName() },
            MacroMicroDriverModelImpl.MICRODRIVERID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_MICRODRIVERID = new FinderPath(MacroMicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroMicroDriverModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByMicroDriverId",
            new String[] { Long.class.getName() });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_MACRODRIVERIDANDMICRODRIVERID =
        new FinderPath(MacroMicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroMicroDriverModelImpl.FINDER_CACHE_ENABLED,
            MacroMicroDriverImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByMacroDriverIdAndMicroDriverId",
            new String[] {
                Long.class.getName(), Long.class.getName(),
                
            "java.lang.Integer", "java.lang.Integer",
                "com.liferay.portal.kernel.util.OrderByComparator"
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MACRODRIVERIDANDMICRODRIVERID =
        new FinderPath(MacroMicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroMicroDriverModelImpl.FINDER_CACHE_ENABLED,
            MacroMicroDriverImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByMacroDriverIdAndMicroDriverId",
            new String[] { Long.class.getName(), Long.class.getName() },
            MacroMicroDriverModelImpl.MACRODRIVERID_COLUMN_BITMASK |
            MacroMicroDriverModelImpl.MICRODRIVERID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_MACRODRIVERIDANDMICRODRIVERID =
        new FinderPath(MacroMicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroMicroDriverModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByMacroDriverIdAndMicroDriverId",
            new String[] { Long.class.getName(), Long.class.getName() });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(MacroMicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroMicroDriverModelImpl.FINDER_CACHE_ENABLED,
            MacroMicroDriverImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(MacroMicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroMicroDriverModelImpl.FINDER_CACHE_ENABLED,
            MacroMicroDriverImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(MacroMicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroMicroDriverModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_MACROMICRODRIVER = "SELECT macroMicroDriver FROM MacroMicroDriver macroMicroDriver";
    private static final String _SQL_SELECT_MACROMICRODRIVER_WHERE = "SELECT macroMicroDriver FROM MacroMicroDriver macroMicroDriver WHERE ";
    private static final String _SQL_COUNT_MACROMICRODRIVER = "SELECT COUNT(macroMicroDriver) FROM MacroMicroDriver macroMicroDriver";
    private static final String _SQL_COUNT_MACROMICRODRIVER_WHERE = "SELECT COUNT(macroMicroDriver) FROM MacroMicroDriver macroMicroDriver WHERE ";
    private static final String _FINDER_COLUMN_MACRODRIVERID_MACRODRIVERID_2 = "macroMicroDriver.macroDriverId = ?";
    private static final String _FINDER_COLUMN_MICRODRIVERID_MICRODRIVERID_2 = "macroMicroDriver.microDriverId = ?";
    private static final String _FINDER_COLUMN_MACRODRIVERIDANDMICRODRIVERID_MACRODRIVERID_2 =
        "macroMicroDriver.macroDriverId = ? AND ";
    private static final String _FINDER_COLUMN_MACRODRIVERIDANDMICRODRIVERID_MICRODRIVERID_2 =
        "macroMicroDriver.microDriverId = ?";
    private static final String _ORDER_BY_ENTITY_ALIAS = "macroMicroDriver.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No MacroMicroDriver exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No MacroMicroDriver exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(MacroMicroDriverPersistenceImpl.class);
    private static MacroMicroDriver _nullMacroMicroDriver = new MacroMicroDriverImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<MacroMicroDriver> toCacheModel() {
                return _nullMacroMicroDriverCacheModel;
            }
        };

    private static CacheModel<MacroMicroDriver> _nullMacroMicroDriverCacheModel = new CacheModel<MacroMicroDriver>() {
            public MacroMicroDriver toEntityModel() {
                return _nullMacroMicroDriver;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the macro micro driver in the entity cache if it is enabled.
     *
     * @param macroMicroDriver the macro micro driver
     */
    public void cacheResult(MacroMicroDriver macroMicroDriver) {
        EntityCacheUtil.putResult(MacroMicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroMicroDriverImpl.class, macroMicroDriver.getPrimaryKey(),
            macroMicroDriver);

        macroMicroDriver.resetOriginalValues();
    }

    /**
     * Caches the macro micro drivers in the entity cache if it is enabled.
     *
     * @param macroMicroDrivers the macro micro drivers
     */
    public void cacheResult(List<MacroMicroDriver> macroMicroDrivers) {
        for (MacroMicroDriver macroMicroDriver : macroMicroDrivers) {
            if (EntityCacheUtil.getResult(
                        MacroMicroDriverModelImpl.ENTITY_CACHE_ENABLED,
                        MacroMicroDriverImpl.class,
                        macroMicroDriver.getPrimaryKey()) == null) {
                cacheResult(macroMicroDriver);
            } else {
                macroMicroDriver.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all macro micro drivers.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(MacroMicroDriverImpl.class.getName());
        }

        EntityCacheUtil.clearCache(MacroMicroDriverImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the macro micro driver.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(MacroMicroDriver macroMicroDriver) {
        EntityCacheUtil.removeResult(MacroMicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroMicroDriverImpl.class, macroMicroDriver.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<MacroMicroDriver> macroMicroDrivers) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (MacroMicroDriver macroMicroDriver : macroMicroDrivers) {
            EntityCacheUtil.removeResult(MacroMicroDriverModelImpl.ENTITY_CACHE_ENABLED,
                MacroMicroDriverImpl.class, macroMicroDriver.getPrimaryKey());
        }
    }

    /**
     * Creates a new macro micro driver with the primary key. Does not add the macro micro driver to the database.
     *
     * @param macroMicroDriverId the primary key for the new macro micro driver
     * @return the new macro micro driver
     */
    public MacroMicroDriver create(long macroMicroDriverId) {
        MacroMicroDriver macroMicroDriver = new MacroMicroDriverImpl();

        macroMicroDriver.setNew(true);
        macroMicroDriver.setPrimaryKey(macroMicroDriverId);

        return macroMicroDriver;
    }

    /**
     * Removes the macro micro driver with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param macroMicroDriverId the primary key of the macro micro driver
     * @return the macro micro driver that was removed
     * @throws com.hp.omp.NoSuchMacroMicroDriverException if a macro micro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroMicroDriver remove(long macroMicroDriverId)
        throws NoSuchMacroMicroDriverException, SystemException {
        return remove(Long.valueOf(macroMicroDriverId));
    }

    /**
     * Removes the macro micro driver with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the macro micro driver
     * @return the macro micro driver that was removed
     * @throws com.hp.omp.NoSuchMacroMicroDriverException if a macro micro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MacroMicroDriver remove(Serializable primaryKey)
        throws NoSuchMacroMicroDriverException, SystemException {
        Session session = null;

        try {
            session = openSession();

            MacroMicroDriver macroMicroDriver = (MacroMicroDriver) session.get(MacroMicroDriverImpl.class,
                    primaryKey);

            if (macroMicroDriver == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchMacroMicroDriverException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(macroMicroDriver);
        } catch (NoSuchMacroMicroDriverException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected MacroMicroDriver removeImpl(MacroMicroDriver macroMicroDriver)
        throws SystemException {
        macroMicroDriver = toUnwrappedModel(macroMicroDriver);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, macroMicroDriver);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(macroMicroDriver);

        return macroMicroDriver;
    }

    @Override
    public MacroMicroDriver updateImpl(
        com.hp.omp.model.MacroMicroDriver macroMicroDriver, boolean merge)
        throws SystemException {
        macroMicroDriver = toUnwrappedModel(macroMicroDriver);

        boolean isNew = macroMicroDriver.isNew();

        MacroMicroDriverModelImpl macroMicroDriverModelImpl = (MacroMicroDriverModelImpl) macroMicroDriver;

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, macroMicroDriver, merge);

            macroMicroDriver.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !MacroMicroDriverModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((macroMicroDriverModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MACRODRIVERID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        Long.valueOf(macroMicroDriverModelImpl.getOriginalMacroDriverId())
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MACRODRIVERID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MACRODRIVERID,
                    args);

                args = new Object[] {
                        Long.valueOf(macroMicroDriverModelImpl.getMacroDriverId())
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MACRODRIVERID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MACRODRIVERID,
                    args);
            }

            if ((macroMicroDriverModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MICRODRIVERID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        Long.valueOf(macroMicroDriverModelImpl.getOriginalMicroDriverId())
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MICRODRIVERID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MICRODRIVERID,
                    args);

                args = new Object[] {
                        Long.valueOf(macroMicroDriverModelImpl.getMicroDriverId())
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MICRODRIVERID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MICRODRIVERID,
                    args);
            }

            if ((macroMicroDriverModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MACRODRIVERIDANDMICRODRIVERID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        Long.valueOf(macroMicroDriverModelImpl.getOriginalMacroDriverId()),
                        Long.valueOf(macroMicroDriverModelImpl.getOriginalMicroDriverId())
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MACRODRIVERIDANDMICRODRIVERID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MACRODRIVERIDANDMICRODRIVERID,
                    args);

                args = new Object[] {
                        Long.valueOf(macroMicroDriverModelImpl.getMacroDriverId()),
                        Long.valueOf(macroMicroDriverModelImpl.getMicroDriverId())
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_MACRODRIVERIDANDMICRODRIVERID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MACRODRIVERIDANDMICRODRIVERID,
                    args);
            }
        }

        EntityCacheUtil.putResult(MacroMicroDriverModelImpl.ENTITY_CACHE_ENABLED,
            MacroMicroDriverImpl.class, macroMicroDriver.getPrimaryKey(),
            macroMicroDriver);

        return macroMicroDriver;
    }

    protected MacroMicroDriver toUnwrappedModel(
        MacroMicroDriver macroMicroDriver) {
        if (macroMicroDriver instanceof MacroMicroDriverImpl) {
            return macroMicroDriver;
        }

        MacroMicroDriverImpl macroMicroDriverImpl = new MacroMicroDriverImpl();

        macroMicroDriverImpl.setNew(macroMicroDriver.isNew());
        macroMicroDriverImpl.setPrimaryKey(macroMicroDriver.getPrimaryKey());

        macroMicroDriverImpl.setMacroMicroDriverId(macroMicroDriver.getMacroMicroDriverId());
        macroMicroDriverImpl.setMacroDriverId(macroMicroDriver.getMacroDriverId());
        macroMicroDriverImpl.setMicroDriverId(macroMicroDriver.getMicroDriverId());

        return macroMicroDriverImpl;
    }

    /**
     * Returns the macro micro driver with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the macro micro driver
     * @return the macro micro driver
     * @throws com.liferay.portal.NoSuchModelException if a macro micro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MacroMicroDriver findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the macro micro driver with the primary key or throws a {@link com.hp.omp.NoSuchMacroMicroDriverException} if it could not be found.
     *
     * @param macroMicroDriverId the primary key of the macro micro driver
     * @return the macro micro driver
     * @throws com.hp.omp.NoSuchMacroMicroDriverException if a macro micro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroMicroDriver findByPrimaryKey(long macroMicroDriverId)
        throws NoSuchMacroMicroDriverException, SystemException {
        MacroMicroDriver macroMicroDriver = fetchByPrimaryKey(macroMicroDriverId);

        if (macroMicroDriver == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    macroMicroDriverId);
            }

            throw new NoSuchMacroMicroDriverException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                macroMicroDriverId);
        }

        return macroMicroDriver;
    }

    /**
     * Returns the macro micro driver with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the macro micro driver
     * @return the macro micro driver, or <code>null</code> if a macro micro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public MacroMicroDriver fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the macro micro driver with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param macroMicroDriverId the primary key of the macro micro driver
     * @return the macro micro driver, or <code>null</code> if a macro micro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroMicroDriver fetchByPrimaryKey(long macroMicroDriverId)
        throws SystemException {
        MacroMicroDriver macroMicroDriver = (MacroMicroDriver) EntityCacheUtil.getResult(MacroMicroDriverModelImpl.ENTITY_CACHE_ENABLED,
                MacroMicroDriverImpl.class, macroMicroDriverId);

        if (macroMicroDriver == _nullMacroMicroDriver) {
            return null;
        }

        if (macroMicroDriver == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                macroMicroDriver = (MacroMicroDriver) session.get(MacroMicroDriverImpl.class,
                        Long.valueOf(macroMicroDriverId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (macroMicroDriver != null) {
                    cacheResult(macroMicroDriver);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(MacroMicroDriverModelImpl.ENTITY_CACHE_ENABLED,
                        MacroMicroDriverImpl.class, macroMicroDriverId,
                        _nullMacroMicroDriver);
                }

                closeSession(session);
            }
        }

        return macroMicroDriver;
    }

    /**
     * Returns all the macro micro drivers where macroDriverId = &#63;.
     *
     * @param macroDriverId the macro driver ID
     * @return the matching macro micro drivers
     * @throws SystemException if a system exception occurred
     */
    public List<MacroMicroDriver> findByMacroDriverId(long macroDriverId)
        throws SystemException {
        return findByMacroDriverId(macroDriverId, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the macro micro drivers where macroDriverId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param macroDriverId the macro driver ID
     * @param start the lower bound of the range of macro micro drivers
     * @param end the upper bound of the range of macro micro drivers (not inclusive)
     * @return the range of matching macro micro drivers
     * @throws SystemException if a system exception occurred
     */
    public List<MacroMicroDriver> findByMacroDriverId(long macroDriverId,
        int start, int end) throws SystemException {
        return findByMacroDriverId(macroDriverId, start, end, null);
    }

    /**
     * Returns an ordered range of all the macro micro drivers where macroDriverId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param macroDriverId the macro driver ID
     * @param start the lower bound of the range of macro micro drivers
     * @param end the upper bound of the range of macro micro drivers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching macro micro drivers
     * @throws SystemException if a system exception occurred
     */
    public List<MacroMicroDriver> findByMacroDriverId(long macroDriverId,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MACRODRIVERID;
            finderArgs = new Object[] { macroDriverId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_MACRODRIVERID;
            finderArgs = new Object[] {
                    macroDriverId,
                    
                    start, end, orderByComparator
                };
        }

        List<MacroMicroDriver> list = (List<MacroMicroDriver>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (MacroMicroDriver macroMicroDriver : list) {
                if ((macroDriverId != macroMicroDriver.getMacroDriverId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(2);
            }

            query.append(_SQL_SELECT_MACROMICRODRIVER_WHERE);

            query.append(_FINDER_COLUMN_MACRODRIVERID_MACRODRIVERID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(macroDriverId);

                list = (List<MacroMicroDriver>) QueryUtil.list(q, getDialect(),
                        start, end);
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first macro micro driver in the ordered set where macroDriverId = &#63;.
     *
     * @param macroDriverId the macro driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching macro micro driver
     * @throws com.hp.omp.NoSuchMacroMicroDriverException if a matching macro micro driver could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroMicroDriver findByMacroDriverId_First(long macroDriverId,
        OrderByComparator orderByComparator)
        throws NoSuchMacroMicroDriverException, SystemException {
        MacroMicroDriver macroMicroDriver = fetchByMacroDriverId_First(macroDriverId,
                orderByComparator);

        if (macroMicroDriver != null) {
            return macroMicroDriver;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("macroDriverId=");
        msg.append(macroDriverId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMacroMicroDriverException(msg.toString());
    }

    /**
     * Returns the first macro micro driver in the ordered set where macroDriverId = &#63;.
     *
     * @param macroDriverId the macro driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching macro micro driver, or <code>null</code> if a matching macro micro driver could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroMicroDriver fetchByMacroDriverId_First(long macroDriverId,
        OrderByComparator orderByComparator) throws SystemException {
        List<MacroMicroDriver> list = findByMacroDriverId(macroDriverId, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last macro micro driver in the ordered set where macroDriverId = &#63;.
     *
     * @param macroDriverId the macro driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching macro micro driver
     * @throws com.hp.omp.NoSuchMacroMicroDriverException if a matching macro micro driver could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroMicroDriver findByMacroDriverId_Last(long macroDriverId,
        OrderByComparator orderByComparator)
        throws NoSuchMacroMicroDriverException, SystemException {
        MacroMicroDriver macroMicroDriver = fetchByMacroDriverId_Last(macroDriverId,
                orderByComparator);

        if (macroMicroDriver != null) {
            return macroMicroDriver;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("macroDriverId=");
        msg.append(macroDriverId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMacroMicroDriverException(msg.toString());
    }

    /**
     * Returns the last macro micro driver in the ordered set where macroDriverId = &#63;.
     *
     * @param macroDriverId the macro driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching macro micro driver, or <code>null</code> if a matching macro micro driver could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroMicroDriver fetchByMacroDriverId_Last(long macroDriverId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByMacroDriverId(macroDriverId);

        List<MacroMicroDriver> list = findByMacroDriverId(macroDriverId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the macro micro drivers before and after the current macro micro driver in the ordered set where macroDriverId = &#63;.
     *
     * @param macroMicroDriverId the primary key of the current macro micro driver
     * @param macroDriverId the macro driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next macro micro driver
     * @throws com.hp.omp.NoSuchMacroMicroDriverException if a macro micro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroMicroDriver[] findByMacroDriverId_PrevAndNext(
        long macroMicroDriverId, long macroDriverId,
        OrderByComparator orderByComparator)
        throws NoSuchMacroMicroDriverException, SystemException {
        MacroMicroDriver macroMicroDriver = findByPrimaryKey(macroMicroDriverId);

        Session session = null;

        try {
            session = openSession();

            MacroMicroDriver[] array = new MacroMicroDriverImpl[3];

            array[0] = getByMacroDriverId_PrevAndNext(session,
                    macroMicroDriver, macroDriverId, orderByComparator, true);

            array[1] = macroMicroDriver;

            array[2] = getByMacroDriverId_PrevAndNext(session,
                    macroMicroDriver, macroDriverId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected MacroMicroDriver getByMacroDriverId_PrevAndNext(Session session,
        MacroMicroDriver macroMicroDriver, long macroDriverId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_MACROMICRODRIVER_WHERE);

        query.append(_FINDER_COLUMN_MACRODRIVERID_MACRODRIVERID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(macroDriverId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(macroMicroDriver);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<MacroMicroDriver> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Returns all the macro micro drivers where microDriverId = &#63;.
     *
     * @param microDriverId the micro driver ID
     * @return the matching macro micro drivers
     * @throws SystemException if a system exception occurred
     */
    public List<MacroMicroDriver> findByMicroDriverId(long microDriverId)
        throws SystemException {
        return findByMicroDriverId(microDriverId, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the macro micro drivers where microDriverId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param microDriverId the micro driver ID
     * @param start the lower bound of the range of macro micro drivers
     * @param end the upper bound of the range of macro micro drivers (not inclusive)
     * @return the range of matching macro micro drivers
     * @throws SystemException if a system exception occurred
     */
    public List<MacroMicroDriver> findByMicroDriverId(long microDriverId,
        int start, int end) throws SystemException {
        return findByMicroDriverId(microDriverId, start, end, null);
    }

    /**
     * Returns an ordered range of all the macro micro drivers where microDriverId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param microDriverId the micro driver ID
     * @param start the lower bound of the range of macro micro drivers
     * @param end the upper bound of the range of macro micro drivers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching macro micro drivers
     * @throws SystemException if a system exception occurred
     */
    public List<MacroMicroDriver> findByMicroDriverId(long microDriverId,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MICRODRIVERID;
            finderArgs = new Object[] { microDriverId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_MICRODRIVERID;
            finderArgs = new Object[] {
                    microDriverId,
                    
                    start, end, orderByComparator
                };
        }

        List<MacroMicroDriver> list = (List<MacroMicroDriver>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (MacroMicroDriver macroMicroDriver : list) {
                if ((microDriverId != macroMicroDriver.getMicroDriverId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(2);
            }

            query.append(_SQL_SELECT_MACROMICRODRIVER_WHERE);

            query.append(_FINDER_COLUMN_MICRODRIVERID_MICRODRIVERID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(microDriverId);

                list = (List<MacroMicroDriver>) QueryUtil.list(q, getDialect(),
                        start, end);
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first macro micro driver in the ordered set where microDriverId = &#63;.
     *
     * @param microDriverId the micro driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching macro micro driver
     * @throws com.hp.omp.NoSuchMacroMicroDriverException if a matching macro micro driver could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroMicroDriver findByMicroDriverId_First(long microDriverId,
        OrderByComparator orderByComparator)
        throws NoSuchMacroMicroDriverException, SystemException {
        MacroMicroDriver macroMicroDriver = fetchByMicroDriverId_First(microDriverId,
                orderByComparator);

        if (macroMicroDriver != null) {
            return macroMicroDriver;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("microDriverId=");
        msg.append(microDriverId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMacroMicroDriverException(msg.toString());
    }

    /**
     * Returns the first macro micro driver in the ordered set where microDriverId = &#63;.
     *
     * @param microDriverId the micro driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching macro micro driver, or <code>null</code> if a matching macro micro driver could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroMicroDriver fetchByMicroDriverId_First(long microDriverId,
        OrderByComparator orderByComparator) throws SystemException {
        List<MacroMicroDriver> list = findByMicroDriverId(microDriverId, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last macro micro driver in the ordered set where microDriverId = &#63;.
     *
     * @param microDriverId the micro driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching macro micro driver
     * @throws com.hp.omp.NoSuchMacroMicroDriverException if a matching macro micro driver could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroMicroDriver findByMicroDriverId_Last(long microDriverId,
        OrderByComparator orderByComparator)
        throws NoSuchMacroMicroDriverException, SystemException {
        MacroMicroDriver macroMicroDriver = fetchByMicroDriverId_Last(microDriverId,
                orderByComparator);

        if (macroMicroDriver != null) {
            return macroMicroDriver;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("microDriverId=");
        msg.append(microDriverId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMacroMicroDriverException(msg.toString());
    }

    /**
     * Returns the last macro micro driver in the ordered set where microDriverId = &#63;.
     *
     * @param microDriverId the micro driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching macro micro driver, or <code>null</code> if a matching macro micro driver could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroMicroDriver fetchByMicroDriverId_Last(long microDriverId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByMicroDriverId(microDriverId);

        List<MacroMicroDriver> list = findByMicroDriverId(microDriverId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the macro micro drivers before and after the current macro micro driver in the ordered set where microDriverId = &#63;.
     *
     * @param macroMicroDriverId the primary key of the current macro micro driver
     * @param microDriverId the micro driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next macro micro driver
     * @throws com.hp.omp.NoSuchMacroMicroDriverException if a macro micro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroMicroDriver[] findByMicroDriverId_PrevAndNext(
        long macroMicroDriverId, long microDriverId,
        OrderByComparator orderByComparator)
        throws NoSuchMacroMicroDriverException, SystemException {
        MacroMicroDriver macroMicroDriver = findByPrimaryKey(macroMicroDriverId);

        Session session = null;

        try {
            session = openSession();

            MacroMicroDriver[] array = new MacroMicroDriverImpl[3];

            array[0] = getByMicroDriverId_PrevAndNext(session,
                    macroMicroDriver, microDriverId, orderByComparator, true);

            array[1] = macroMicroDriver;

            array[2] = getByMicroDriverId_PrevAndNext(session,
                    macroMicroDriver, microDriverId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected MacroMicroDriver getByMicroDriverId_PrevAndNext(Session session,
        MacroMicroDriver macroMicroDriver, long microDriverId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_MACROMICRODRIVER_WHERE);

        query.append(_FINDER_COLUMN_MICRODRIVERID_MICRODRIVERID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(microDriverId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(macroMicroDriver);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<MacroMicroDriver> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Returns all the macro micro drivers where macroDriverId = &#63; and microDriverId = &#63;.
     *
     * @param macroDriverId the macro driver ID
     * @param microDriverId the micro driver ID
     * @return the matching macro micro drivers
     * @throws SystemException if a system exception occurred
     */
    public List<MacroMicroDriver> findByMacroDriverIdAndMicroDriverId(
        long macroDriverId, long microDriverId) throws SystemException {
        return findByMacroDriverIdAndMicroDriverId(macroDriverId,
            microDriverId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the macro micro drivers where macroDriverId = &#63; and microDriverId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param macroDriverId the macro driver ID
     * @param microDriverId the micro driver ID
     * @param start the lower bound of the range of macro micro drivers
     * @param end the upper bound of the range of macro micro drivers (not inclusive)
     * @return the range of matching macro micro drivers
     * @throws SystemException if a system exception occurred
     */
    public List<MacroMicroDriver> findByMacroDriverIdAndMicroDriverId(
        long macroDriverId, long microDriverId, int start, int end)
        throws SystemException {
        return findByMacroDriverIdAndMicroDriverId(macroDriverId,
            microDriverId, start, end, null);
    }

    /**
     * Returns an ordered range of all the macro micro drivers where macroDriverId = &#63; and microDriverId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param macroDriverId the macro driver ID
     * @param microDriverId the micro driver ID
     * @param start the lower bound of the range of macro micro drivers
     * @param end the upper bound of the range of macro micro drivers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching macro micro drivers
     * @throws SystemException if a system exception occurred
     */
    public List<MacroMicroDriver> findByMacroDriverIdAndMicroDriverId(
        long macroDriverId, long microDriverId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MACRODRIVERIDANDMICRODRIVERID;
            finderArgs = new Object[] { macroDriverId, microDriverId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_MACRODRIVERIDANDMICRODRIVERID;
            finderArgs = new Object[] {
                    macroDriverId, microDriverId,
                    
                    start, end, orderByComparator
                };
        }

        List<MacroMicroDriver> list = (List<MacroMicroDriver>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (MacroMicroDriver macroMicroDriver : list) {
                if ((macroDriverId != macroMicroDriver.getMacroDriverId()) ||
                        (microDriverId != macroMicroDriver.getMicroDriverId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_MACROMICRODRIVER_WHERE);

            query.append(_FINDER_COLUMN_MACRODRIVERIDANDMICRODRIVERID_MACRODRIVERID_2);

            query.append(_FINDER_COLUMN_MACRODRIVERIDANDMICRODRIVERID_MICRODRIVERID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(macroDriverId);

                qPos.add(microDriverId);

                list = (List<MacroMicroDriver>) QueryUtil.list(q, getDialect(),
                        start, end);
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first macro micro driver in the ordered set where macroDriverId = &#63; and microDriverId = &#63;.
     *
     * @param macroDriverId the macro driver ID
     * @param microDriverId the micro driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching macro micro driver
     * @throws com.hp.omp.NoSuchMacroMicroDriverException if a matching macro micro driver could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroMicroDriver findByMacroDriverIdAndMicroDriverId_First(
        long macroDriverId, long microDriverId,
        OrderByComparator orderByComparator)
        throws NoSuchMacroMicroDriverException, SystemException {
        MacroMicroDriver macroMicroDriver = fetchByMacroDriverIdAndMicroDriverId_First(macroDriverId,
                microDriverId, orderByComparator);

        if (macroMicroDriver != null) {
            return macroMicroDriver;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("macroDriverId=");
        msg.append(macroDriverId);

        msg.append(", microDriverId=");
        msg.append(microDriverId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMacroMicroDriverException(msg.toString());
    }

    /**
     * Returns the first macro micro driver in the ordered set where macroDriverId = &#63; and microDriverId = &#63;.
     *
     * @param macroDriverId the macro driver ID
     * @param microDriverId the micro driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching macro micro driver, or <code>null</code> if a matching macro micro driver could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroMicroDriver fetchByMacroDriverIdAndMicroDriverId_First(
        long macroDriverId, long microDriverId,
        OrderByComparator orderByComparator) throws SystemException {
        List<MacroMicroDriver> list = findByMacroDriverIdAndMicroDriverId(macroDriverId,
                microDriverId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last macro micro driver in the ordered set where macroDriverId = &#63; and microDriverId = &#63;.
     *
     * @param macroDriverId the macro driver ID
     * @param microDriverId the micro driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching macro micro driver
     * @throws com.hp.omp.NoSuchMacroMicroDriverException if a matching macro micro driver could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroMicroDriver findByMacroDriverIdAndMicroDriverId_Last(
        long macroDriverId, long microDriverId,
        OrderByComparator orderByComparator)
        throws NoSuchMacroMicroDriverException, SystemException {
        MacroMicroDriver macroMicroDriver = fetchByMacroDriverIdAndMicroDriverId_Last(macroDriverId,
                microDriverId, orderByComparator);

        if (macroMicroDriver != null) {
            return macroMicroDriver;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("macroDriverId=");
        msg.append(macroDriverId);

        msg.append(", microDriverId=");
        msg.append(microDriverId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMacroMicroDriverException(msg.toString());
    }

    /**
     * Returns the last macro micro driver in the ordered set where macroDriverId = &#63; and microDriverId = &#63;.
     *
     * @param macroDriverId the macro driver ID
     * @param microDriverId the micro driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching macro micro driver, or <code>null</code> if a matching macro micro driver could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroMicroDriver fetchByMacroDriverIdAndMicroDriverId_Last(
        long macroDriverId, long microDriverId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByMacroDriverIdAndMicroDriverId(macroDriverId,
                microDriverId);

        List<MacroMicroDriver> list = findByMacroDriverIdAndMicroDriverId(macroDriverId,
                microDriverId, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the macro micro drivers before and after the current macro micro driver in the ordered set where macroDriverId = &#63; and microDriverId = &#63;.
     *
     * @param macroMicroDriverId the primary key of the current macro micro driver
     * @param macroDriverId the macro driver ID
     * @param microDriverId the micro driver ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next macro micro driver
     * @throws com.hp.omp.NoSuchMacroMicroDriverException if a macro micro driver with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public MacroMicroDriver[] findByMacroDriverIdAndMicroDriverId_PrevAndNext(
        long macroMicroDriverId, long macroDriverId, long microDriverId,
        OrderByComparator orderByComparator)
        throws NoSuchMacroMicroDriverException, SystemException {
        MacroMicroDriver macroMicroDriver = findByPrimaryKey(macroMicroDriverId);

        Session session = null;

        try {
            session = openSession();

            MacroMicroDriver[] array = new MacroMicroDriverImpl[3];

            array[0] = getByMacroDriverIdAndMicroDriverId_PrevAndNext(session,
                    macroMicroDriver, macroDriverId, microDriverId,
                    orderByComparator, true);

            array[1] = macroMicroDriver;

            array[2] = getByMacroDriverIdAndMicroDriverId_PrevAndNext(session,
                    macroMicroDriver, macroDriverId, microDriverId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected MacroMicroDriver getByMacroDriverIdAndMicroDriverId_PrevAndNext(
        Session session, MacroMicroDriver macroMicroDriver, long macroDriverId,
        long microDriverId, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_MACROMICRODRIVER_WHERE);

        query.append(_FINDER_COLUMN_MACRODRIVERIDANDMICRODRIVERID_MACRODRIVERID_2);

        query.append(_FINDER_COLUMN_MACRODRIVERIDANDMICRODRIVERID_MICRODRIVERID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(macroDriverId);

        qPos.add(microDriverId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(macroMicroDriver);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<MacroMicroDriver> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Returns all the macro micro drivers.
     *
     * @return the macro micro drivers
     * @throws SystemException if a system exception occurred
     */
    public List<MacroMicroDriver> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the macro micro drivers.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of macro micro drivers
     * @param end the upper bound of the range of macro micro drivers (not inclusive)
     * @return the range of macro micro drivers
     * @throws SystemException if a system exception occurred
     */
    public List<MacroMicroDriver> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the macro micro drivers.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of macro micro drivers
     * @param end the upper bound of the range of macro micro drivers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of macro micro drivers
     * @throws SystemException if a system exception occurred
     */
    public List<MacroMicroDriver> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<MacroMicroDriver> list = (List<MacroMicroDriver>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_MACROMICRODRIVER);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_MACROMICRODRIVER;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<MacroMicroDriver>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<MacroMicroDriver>) QueryUtil.list(q,
                            getDialect(), start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the macro micro drivers where macroDriverId = &#63; from the database.
     *
     * @param macroDriverId the macro driver ID
     * @throws SystemException if a system exception occurred
     */
    public void removeByMacroDriverId(long macroDriverId)
        throws SystemException {
        for (MacroMicroDriver macroMicroDriver : findByMacroDriverId(
                macroDriverId)) {
            remove(macroMicroDriver);
        }
    }

    /**
     * Removes all the macro micro drivers where microDriverId = &#63; from the database.
     *
     * @param microDriverId the micro driver ID
     * @throws SystemException if a system exception occurred
     */
    public void removeByMicroDriverId(long microDriverId)
        throws SystemException {
        for (MacroMicroDriver macroMicroDriver : findByMicroDriverId(
                microDriverId)) {
            remove(macroMicroDriver);
        }
    }

    /**
     * Removes all the macro micro drivers where macroDriverId = &#63; and microDriverId = &#63; from the database.
     *
     * @param macroDriverId the macro driver ID
     * @param microDriverId the micro driver ID
     * @throws SystemException if a system exception occurred
     */
    public void removeByMacroDriverIdAndMicroDriverId(long macroDriverId,
        long microDriverId) throws SystemException {
        for (MacroMicroDriver macroMicroDriver : findByMacroDriverIdAndMicroDriverId(
                macroDriverId, microDriverId)) {
            remove(macroMicroDriver);
        }
    }

    /**
     * Removes all the macro micro drivers from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (MacroMicroDriver macroMicroDriver : findAll()) {
            remove(macroMicroDriver);
        }
    }

    /**
     * Returns the number of macro micro drivers where macroDriverId = &#63;.
     *
     * @param macroDriverId the macro driver ID
     * @return the number of matching macro micro drivers
     * @throws SystemException if a system exception occurred
     */
    public int countByMacroDriverId(long macroDriverId)
        throws SystemException {
        Object[] finderArgs = new Object[] { macroDriverId };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_MACRODRIVERID,
                finderArgs, this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_MACROMICRODRIVER_WHERE);

            query.append(_FINDER_COLUMN_MACRODRIVERID_MACRODRIVERID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(macroDriverId);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_MACRODRIVERID,
                    finderArgs, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the number of macro micro drivers where microDriverId = &#63;.
     *
     * @param microDriverId the micro driver ID
     * @return the number of matching macro micro drivers
     * @throws SystemException if a system exception occurred
     */
    public int countByMicroDriverId(long microDriverId)
        throws SystemException {
        Object[] finderArgs = new Object[] { microDriverId };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_MICRODRIVERID,
                finderArgs, this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_MACROMICRODRIVER_WHERE);

            query.append(_FINDER_COLUMN_MICRODRIVERID_MICRODRIVERID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(microDriverId);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_MICRODRIVERID,
                    finderArgs, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the number of macro micro drivers where macroDriverId = &#63; and microDriverId = &#63;.
     *
     * @param macroDriverId the macro driver ID
     * @param microDriverId the micro driver ID
     * @return the number of matching macro micro drivers
     * @throws SystemException if a system exception occurred
     */
    public int countByMacroDriverIdAndMicroDriverId(long macroDriverId,
        long microDriverId) throws SystemException {
        Object[] finderArgs = new Object[] { macroDriverId, microDriverId };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_MACRODRIVERIDANDMICRODRIVERID,
                finderArgs, this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_MACROMICRODRIVER_WHERE);

            query.append(_FINDER_COLUMN_MACRODRIVERIDANDMICRODRIVERID_MACRODRIVERID_2);

            query.append(_FINDER_COLUMN_MACRODRIVERIDANDMICRODRIVERID_MICRODRIVERID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(macroDriverId);

                qPos.add(microDriverId);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_MACRODRIVERIDANDMICRODRIVERID,
                    finderArgs, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the number of macro micro drivers.
     *
     * @return the number of macro micro drivers
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_MACROMICRODRIVER);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the macro micro driver persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.MacroMicroDriver")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<MacroMicroDriver>> listenersList = new ArrayList<ModelListener<MacroMicroDriver>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<MacroMicroDriver>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(MacroMicroDriverImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
