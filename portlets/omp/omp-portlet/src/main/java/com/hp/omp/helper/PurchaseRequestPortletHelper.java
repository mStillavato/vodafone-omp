package com.hp.omp.helper;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.Position;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.custom.PositionStatus;
import com.hp.omp.model.custom.PurchaseRequestStatus;

/**
 * @author sami.basyoni
 * 
 */
public class PurchaseRequestPortletHelper {
	private transient final static Logger LOGGER = LoggerFactory.getLogger(OmpHelper.class);

	public static int getPositionStatusfromActionStatus(String actionStatus){
		int positionStatus=0 ;

		if(actionStatus.equals(PurchaseRequestStatus.CREATED.getLabel()) || actionStatus.equals(PurchaseRequestStatus.SUBMITTED.getLabel()))
		{
			positionStatus=PositionStatus.NEW.getCode();
		}
		else if (actionStatus.equals(PurchaseRequestStatus.ASSET_RECEIVED.getLabel())){
			positionStatus=PositionStatus.ASSET_RECEIVED.getCode();
		}
		else if (actionStatus.equals(PurchaseRequestStatus.SC_RECEIVED.getLabel())){
			positionStatus=PositionStatus.SC_RECEIVED.getCode();
		}
		else if (actionStatus.equals(PurchaseRequestStatus.PO_RECEIVED.getLabel())){
			positionStatus=PositionStatus.PO_RECEIVED.getCode();
		} 
		return positionStatus;
	}

	public static String[] getDiableEnableView(String userType, String actionStatus, boolean automatic, boolean nssFromFileExcel){
		String disableEnable[]=new String[19];

		String prDisabled="";
		String assetNumberDisabled="disabled";
		String prcsNumberDisabled="disabled";
		String poNumberDisabled="disabled";
		String evoDisabled="";
		String editPositionDisabled="";
		String poTargaTecnicaDisabled="disabled";
		String prDeleteBtn="No";
		String rejCauseDisabled="disabled";
		String poOfferNumberDisabled="disabled";

		String prNssTrackingCodeDisabled = "";
		String prNssWbsCodeDisabled = "";
		String prNssProjectNameDisabled = "";
		String prNssVendorDisabled = "";
		String prNssPlantDisabled = "";
		String prNssLocationEvoDisabled = "";
		String prNssCurrencyDisabled = "";

		String prTargaTecnicaDisabled = "";

		//CR5.1
		if(isController(userType) || isEng(userType)){
			poTargaTecnicaDisabled = "";
		}

		//CR5.4
		if(isApprover(userType)){
			evoDisabled="disabled";
			prDisabled="disabled";
			editPositionDisabled="disabled";
		}

		if(isEng(userType)){
			if(! actionStatus.equalsIgnoreCase(PurchaseRequestStatus.CREATED.getLabel()) && ! actionStatus.equalsIgnoreCase(PurchaseRequestStatus.REJECTED.getLabel())) {
				userType=OmpRoles.OMP_OBSERVER.getLabel();
			}
			if(actionStatus.equalsIgnoreCase(PurchaseRequestStatus.CREATED.getLabel()) || actionStatus.equalsIgnoreCase(PurchaseRequestStatus.PENDING_IC_APPROVAL.getLabel())){
				prDeleteBtn="yes";
				poOfferNumberDisabled = "";
			}
		}
		if(userType.equalsIgnoreCase(OmpRoles.OMP_ANTEX.getLabel())){
			prDisabled="disabled";
			poTargaTecnicaDisabled = "";
			if(automatic || nssFromFileExcel){
				assetNumberDisabled="";
				prcsNumberDisabled="";
				poNumberDisabled="";
			}else{
				if(actionStatus.equalsIgnoreCase(PurchaseRequestStatus.PR_ASSIGNED.getLabel())){
					assetNumberDisabled="";
				}else if(actionStatus.equalsIgnoreCase(PurchaseRequestStatus.ASSET_RECEIVED.getLabel())){
					prcsNumberDisabled="";
					evoDisabled="disabled";
				}else if(actionStatus.equalsIgnoreCase(PurchaseRequestStatus.SC_RECEIVED.getLabel())){
					poNumberDisabled="";
					evoDisabled="disabled";
				}
			}
		}
		if(userType.equalsIgnoreCase(OmpRoles.OMP_OBSERVER.getLabel())){
			evoDisabled="disabled";
			prDisabled="disabled";
			editPositionDisabled="disabled";
		}
		if(isController(userType)){
			assetNumberDisabled="";
			prcsNumberDisabled="";
			poNumberDisabled="";
			poOfferNumberDisabled="";
			if(! PurchaseRequestStatus.REJECTED.getLabel().equals(actionStatus)) {
				prDeleteBtn="yes";
			}
		}


		//CR5.3
		if((isController(userType) || userType.equalsIgnoreCase(OmpRoles.OMP_ANTEX.getLabel())) 
				&& actionStatus.equalsIgnoreCase(PurchaseRequestStatus.SUBMITTED.getLabel())){
			LOGGER.debug("iside Antex rej cause = " + actionStatus);
			rejCauseDisabled = "";
		}

		//CR 68
		if(actionStatus.equalsIgnoreCase(PurchaseRequestStatus.PENDING_IC_APPROVAL.getLabel()) 
				&& (isController(userType) || isApprover(userType))){
			LOGGER.debug("iside NDSO and TC rej cause = " + actionStatus);
			rejCauseDisabled = "";
		}

		// Da eliminare xchè non funziona
		if((isController(userType) || isEng(userType)) && nssFromFileExcel){				
			prNssTrackingCodeDisabled = "disabled";
			prNssWbsCodeDisabled = "disabled";
			prNssProjectNameDisabled = "disabled";
			prNssVendorDisabled = "disabled";
			prNssPlantDisabled = "disabled";
			prNssLocationEvoDisabled = "disabled";
			prNssCurrencyDisabled = "disabled";
			prTargaTecnicaDisabled = "disabled";
		}
		//

		disableEnable[0]=prDisabled;
		disableEnable[1]=assetNumberDisabled;
		disableEnable[2]=prcsNumberDisabled;
		disableEnable[3]=poNumberDisabled;
		disableEnable[4]=evoDisabled;
		disableEnable[5]=editPositionDisabled;
		disableEnable[6]=userType;
		disableEnable[7]=poTargaTecnicaDisabled;
		disableEnable[8]=prDeleteBtn;
		disableEnable[9]=rejCauseDisabled;
		disableEnable[10]=poOfferNumberDisabled;

		disableEnable[11] = prNssTrackingCodeDisabled;
		disableEnable[12] = prNssWbsCodeDisabled;
		disableEnable[13] = prNssProjectNameDisabled;
		disableEnable[14] = prNssVendorDisabled;
		disableEnable[15] = prNssPlantDisabled;
		disableEnable[16] = prNssLocationEvoDisabled;
		disableEnable[17] = prNssCurrencyDisabled;
		disableEnable[18] = prTargaTecnicaDisabled;

		return disableEnable;
	}



	public static void AssignPositionsEditBtton(String actionStatus,String userType,List<Position> positions){

		String realRole = userType;
		for(Position position : positions) {

			String positionStatus=position.getPositionStatus().getLabel();

			if(isEng(realRole)){
				if(actionStatus.equalsIgnoreCase(PurchaseRequestStatus.CREATED.getLabel())) {
					position.setPositionEditBtton(true);
					continue;
				}else if(!actionStatus.equalsIgnoreCase(PurchaseRequestStatus.CLOSED.getLabel())){
					//CR5.1
					position.setPositionEditBtton(true);
					userType=OmpRoles.OMP_OBSERVER.getLabel();
					continue;
				}
			}

			if(isApprover(realRole)){
				if(actionStatus.equalsIgnoreCase(PurchaseRequestStatus.PENDING_IC_APPROVAL.getLabel()) && position.getApproved() !=true){
					position.setPositionEditBtton(true);
					continue;
				}else {
					position.setPositionEditBtton(false);
					continue;
				}
			}
			if(realRole.equals(OmpRoles.OMP_OBSERVER.getLabel())){
				position.setPositionEditBtton(false);
				continue;
			}

			if(isController(realRole)){
				position.setPositionEditBtton(true);
				continue;
			}


			if(actionStatus.equalsIgnoreCase(PurchaseRequestStatus.SUBMITTED.getLabel())) {
				position.setPositionEditBtton(false);
				continue;
			}
			if(actionStatus.equalsIgnoreCase(PurchaseRequestStatus.PR_ASSIGNED.getLabel()) 
					&& positionStatus.equalsIgnoreCase(PositionStatus.NEW.getLabel())) {
				position.setPositionEditBtton(true);
				continue;
			}
			if(actionStatus.equalsIgnoreCase(PurchaseRequestStatus.ASSET_RECEIVED.getLabel()) 
					&& positionStatus.equalsIgnoreCase(PositionStatus.ASSET_RECEIVED.getLabel())) {
				position.setPositionEditBtton(true);
				continue;
			}
			if(actionStatus.equalsIgnoreCase(PurchaseRequestStatus.SC_RECEIVED.getLabel()) 
					&& positionStatus.equalsIgnoreCase(PositionStatus.SC_RECEIVED.getLabel())) {
				position.setPositionEditBtton(true);
				continue;
			}
		}


	}

	private static boolean isController(String userType){

		if (userType.equalsIgnoreCase(OmpRoles.OMP_CONTROLLER.getLabel())
				|| userType.equalsIgnoreCase(OmpRoles.OMP_CONTROLLER_VBS.getLabel())){
			return true;
		}
		else{
			return false;
		}
	}

	private static boolean isEng(String userType){

		if (userType.equalsIgnoreCase(OmpRoles.OMP_ENG.getLabel())
				|| userType.equalsIgnoreCase(OmpRoles.OMP_ENG_VBS.getLabel())){
			return true;
		}
		else{
			return false;
		}
	}

	private static boolean isApprover(String userType){
		if(userType.equalsIgnoreCase(OmpRoles.OMP_NDSO.getLabel()) 
				|| userType.equalsIgnoreCase(OmpRoles.OMP_TC.getLabel())
				|| userType.equalsIgnoreCase(OmpRoles.OMP_APPROVER_VBS.getLabel())){
			return true;
		}
		else{
			return false;
		}
	}

}
