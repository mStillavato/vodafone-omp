package com.hp.omp.model.impl;

import com.hp.omp.model.PRAudit;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing PRAudit in entity cache.
 *
 * @author HP Egypt team
 * @see PRAudit
 * @generated
 */
public class PRAuditCacheModel implements CacheModel<PRAudit>, Serializable {
    public long auditId;
    public Long purchaseRequestId;
    public int status;
    public Long userId;
    public long changeDate;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{auditId=");
        sb.append(auditId);
        sb.append(", purchaseRequestId=");
        sb.append(purchaseRequestId);
        sb.append(", status=");
        sb.append(status);
        sb.append(", userId=");
        sb.append(userId);
        sb.append(", changeDate=");
        sb.append(changeDate);
        sb.append("}");

        return sb.toString();
    }

    public PRAudit toEntityModel() {
        PRAuditImpl prAuditImpl = new PRAuditImpl();

        prAuditImpl.setAuditId(auditId);
        prAuditImpl.setPurchaseRequestId(purchaseRequestId);
        prAuditImpl.setStatus(status);
        prAuditImpl.setUserId(userId);

        if (changeDate == Long.MIN_VALUE) {
            prAuditImpl.setChangeDate(null);
        } else {
            prAuditImpl.setChangeDate(new Date(changeDate));
        }

        prAuditImpl.resetOriginalValues();

        return prAuditImpl;
    }
}
