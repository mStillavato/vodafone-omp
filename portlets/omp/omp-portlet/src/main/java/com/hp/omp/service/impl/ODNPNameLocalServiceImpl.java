package com.hp.omp.service.impl;

import java.util.List;

import com.hp.omp.model.ODNPName;
import com.hp.omp.service.base.ODNPNameLocalServiceBaseImpl;
import com.hp.omp.service.persistence.ODNPNameFinderUtil;
import com.hp.omp.service.persistence.ODNPNameUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

/**
 * The implementation of the o d n p name local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.ODNPNameLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Ahmed Kamel
 * @see com.hp.omp.service.base.ODNPNameLocalServiceBaseImpl
 * @see com.hp.omp.service.ODNPNameLocalServiceUtil
 */
public class ODNPNameLocalServiceImpl extends ODNPNameLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.hp.omp.service.ODNPNameLocalServiceUtil} to access the o d n p name local service.
     */
	
	public ODNPName getOdnpNameByName(String name) throws SystemException{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(ODNPName.class);
		dynamicQuery.add(RestrictionsFactoryUtil.ilike("odnpNameName", name));
		 
		ODNPName odnpName = null;
		List<ODNPName>  odnpNameList = ODNPNameUtil.findWithDynamicQuery(dynamicQuery);
		if(odnpNameList != null && ! (odnpNameList.isEmpty()) ){
			odnpName = odnpNameList.get(0);
		}
		
		return odnpName;
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<ODNPName> getODNPNamesList(int start, int end,
			String searchFilter) throws SystemException {
		List<ODNPName> resultQuery = null;
		resultQuery= ODNPNameFinderUtil.getODNPNamesList(start, end, searchFilter);
		return resultQuery; 
	}
}
