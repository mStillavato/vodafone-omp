package com.hp.omp.portlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.custom.CatalogoExportListDTO;
import com.hp.omp.model.custom.ReportCheckIcDTO;
import com.hp.omp.model.custom.ReportDTO;
import com.hp.omp.model.custom.SearchReportCheckIcDTO;
import com.hp.omp.service.CatalogoLocalServiceUtil;
import com.hp.omp.service.ReportLocalServiceUtil;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class ReportCheckIcPortlet extends MVCPortlet {

	public static final Logger logger = LoggerFactory.getLogger(ReportCheckIcPortlet.class);
	
	boolean created = false;

	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {

		try {
			Integer pageNumber = ParamUtil.getInteger(renderRequest, "pageNumber", 1);
			
			SearchReportCheckIcDTO searchDTO = new SearchReportCheckIcDTO();
			searchDTO.setPageNumber(pageNumber);
			searchDTO.setPageSize(15);
	
			List<ReportCheckIcDTO> reportCheckIcList = ReportLocalServiceUtil.geReportCheckIcList(searchDTO);
			Long totalCount = ReportLocalServiceUtil.getReportCheckIcListCount(searchDTO);
			//Long totalCount = Long.valueOf(reportCheckIcList.size());
			logger.info("Result List count {}",totalCount);
			
			renderRequest.setAttribute("numberOfPages", getTotalNumberOfPages(totalCount, 15));
			renderRequest.setAttribute("pageNumber", pageNumber);
			renderRequest.setAttribute("reportCheckIcList", reportCheckIcList);
			
			super.doView(renderRequest, renderResponse);
		
		} catch(Exception e) {
			logger.error("Error in get list report check ic for Enginner",e );
			SessionErrors.add(renderRequest, "Unassgined-CostCenter");
		}
	}
	
	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {

		String jspPage = "/html/reportcheckic/reportcheckiclist.jsp";
		
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay)resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String user = themeDisplay.getUser().getScreenName();
			
			
			if("writeExcelReportCheckIc".equals(resourceRequest.getResourceID())){
				exportReport(resourceRequest, resourceResponse, user);
			}
			
			Integer statusCode = ParamUtil.getInteger(resourceRequest, "status");	
			logger.debug("tab status = "+statusCode);
			Integer pageNumber = ParamUtil.getInteger(resourceRequest, "pageNumber", 1);
			
			SearchReportCheckIcDTO searchDTO = new SearchReportCheckIcDTO();
			searchDTO.setPageNumber(pageNumber);
			searchDTO.setPageSize(15);

			List<ReportCheckIcDTO> reportCheckIcList = ReportLocalServiceUtil.geReportCheckIcList(searchDTO);
			Long totalCount = ReportLocalServiceUtil.getReportCheckIcListCount(searchDTO);
			//Long totalCount = Long.valueOf(reportCheckIcList.size());
			logger.info("Result List count {}",totalCount);
			
			resourceRequest.setAttribute("numberOfPages", getTotalNumberOfPages(totalCount, 15));
			resourceRequest.setAttribute("pageNumber", pageNumber);
			resourceRequest.setAttribute("reportCheckIcList", reportCheckIcList);
			
			getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);

		} catch(Exception e) {
			logger.error("Error in get PRL for Enginner",e );
			SessionErrors.add(resourceRequest, "Unassgined-CostCenter");
		}
	}

	private Integer getTotalNumberOfPages(Long totalNubmerOfResult, Integer pageSize) {
		return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
	}

	
	private void exportReport(ResourceRequest resourceRequest, ResourceResponse resourceResponse, String user) {
		try {
			
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_HHmmss");
			String fileName = "OMP_Report_CheckIC" + formatter.format(date)+".xls";
			resourceResponse.setContentType("application/vnd.ms-excel");
			resourceResponse.addProperty(HttpHeaders.CONTENT_DISPOSITION,
					"attachment; filename=\""+fileName+"\"");			
			resourceResponse.addProperty(HttpHeaders.CACHE_CONTROL,"max-age=3600, must-revalidate");

			logger.info("exporting.."+fileName);
			HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(resourceResponse);
			ReportLocalServiceUtil.exportReportCheckIcAsExcel(getReportHeader(), httpServletResponse.getOutputStream());
		} catch (Exception e) {
			logger.error("error in exportReport",e);
		}
	}
	
	private List<Object> getReportHeader() {
		List<Object> header = new ArrayList<Object>();

		header.add("OMP_ID");					// 00
		header.add("FISCALYEAR");				// 01
		header.add("PONUMBER");					// 02
		header.add("EVOLOCATION");				// 03
		header.add("TARGATECNICA");				// 04		
		header.add("IC_APPROVER");				// 05
		header.add("GR REQUESTER");				// 06
		header.add("GR_REQUEST_DATE");			// 07
		header.add("PR_DESCRIPTION");			// 08
		header.add("POS_DESCRIPTION");			// 09		
		header.add("PO_POSITION");				// 10
		header.add("QUANTITY");					// 11
		header.add("POSITIONCOST");				// 12
		header.add("VENDOR");					// 13
		header.add("GR_NUMBER");				// 14
		header.add("ASSETNUMBER");				// 15
		header.add("REQUESTOR");				// 16

		return header;
	}
	

}

