package com.hp.omp.portlet;

import java.io.IOException;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.exceptions.UserHasNoCostCenterException;
import com.hp.omp.helper.OmpHelper;
import com.hp.omp.model.CostCenter;
import com.hp.omp.model.CostCenterUser;
import com.hp.omp.model.custom.GruppoUsers;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.custom.ReportDTO;
import com.hp.omp.service.CostCenterLocalServiceUtil;
import com.hp.omp.service.CostCenterUserLocalServiceUtil;
import com.hp.omp.service.ReportLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class ReportPortlet
 */
public class StoricoPortlet extends MVCPortlet {
	
	Logger logger = LoggerFactory.getLogger(StoricoPortlet.class);
	
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		ReportDTO reportDTO = null;
		Object reportDTOObject = renderRequest.getAttribute("reportDTO");
		if(reportDTOObject != null && reportDTOObject instanceof ReportDTO){
			reportDTO = (ReportDTO)reportDTOObject;
		}else{
			reportDTO = new ReportDTO();
		}
		
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		
		
		reportDTO = populate(reportDTO,themeDisplay);
		renderRequest.setAttribute("reportDTO", reportDTO);
		
		super.doView(renderRequest, renderResponse);
	}

	private ReportDTO populate(ReportDTO reportDTO,ThemeDisplay themeDisplay) {
		try {
			
			String userType = OmpHelper.getUserRoles(themeDisplay.getUserId());
    		int gruppoUser = GruppoUsers.GNED.getCode();
    		if(userType.equals(OmpRoles.OMP_CONTROLLER_VBS.getLabel())
    				|| userType.equals(OmpRoles.OMP_APPROVER_VBS.getLabel())
    				|| userType.equals(OmpRoles.OMP_ENG_VBS.getLabel())){
    			gruppoUser = GruppoUsers.VBS.getCode();
    		}
			List<CostCenter> costCentersList = CostCenterLocalServiceUtil.getCostCentersList(0, 
					CostCenterLocalServiceUtil.getCostCentersCount(), null, gruppoUser);
			
			//List<CostCenter> costCentersList = CostCenterLocalServiceUtil.getCostCenters(0,CostCenterLocalServiceUtil.getCostCentersCount());
			
			reportDTO.setAllCostCenters(costCentersList);
			CostCenter costCenter = null;
			if (! OmpRoles.OMP_CONTROLLER.getLabel().equals(userType) 
					&& ! OmpRoles.OMP_ANTEX.getLabel().equals(userType) 
					&& ! OmpRoles.OMP_NDSO.getLabel().equals(userType) 
					&& ! OmpRoles.OMP_TC.getLabel().equals(userType)) {
				
				try {
					CostCenterUser costCenterUser = CostCenterUserLocalServiceUtil.getCostCenterUserByUserId(themeDisplay.getUserId());
					if (costCenterUser != null) {
						costCenter = CostCenterLocalServiceUtil.getCostCenter(costCenterUser.getCostCenterId());
					} else {
						throw new UserHasNoCostCenterException("the logged in user has't cost center");
					}
				} catch (Exception e) {
					logger.error("Erro in populate ReportDTO", e);
				}
			}
			
			reportDTO.setPrReport(true);
			
			if (costCenter != null) {
				reportDTO.setCostCenterId(costCenter.getCostCenterId());
			}
			
			List<String> fiscalYears = ReportLocalServiceUtil.getAllFiscalYears();
			reportDTO.setAllFiscalYears(fiscalYears);
			
		} catch (SystemException e) {
			processException(e);
		}
		return reportDTO;
	}
	
	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		_log.debug("serverResource");
		if("exportReport".equals(resourceRequest.getResourceID())){
			debug("serverResource: exportReport");
		}else if("exportDetailsReport".equals(resourceRequest.getResourceID())){
			debug("serverResource: exportDetailsReport");
		}
	}

	private void processException(Exception e) {
		_log.error("Caught unexpected exception " + e.getStackTrace());
		if (_log.isDebugEnabled()) {
			_log.debug(e.getMessage(), e);
		}
	}
	
	private static void debug(String msg){
		if(_log.isDebugEnabled()){
			_log.debug(msg);
		}
	}

	private static Logger _log = LoggerFactory.getLogger(StoricoPortlet.class);
}

