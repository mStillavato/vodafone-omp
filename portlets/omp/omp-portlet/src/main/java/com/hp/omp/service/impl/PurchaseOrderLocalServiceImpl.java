/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service.impl;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.POAudit;
import com.hp.omp.model.PurchaseOrder;
import com.hp.omp.model.PurchaseOrderModel;
import com.hp.omp.model.custom.PoListDTO;
import com.hp.omp.model.custom.PurchaseOrderStatus;
import com.hp.omp.model.custom.SearchDTO;
import com.hp.omp.model.impl.POAuditImpl;
import com.hp.omp.service.POAuditLocalServiceUtil;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;
import com.hp.omp.service.base.PurchaseOrderLocalServiceBaseImpl;
import com.hp.omp.service.persistence.PurchaseOrderFinderUtil;
import com.hp.omp.service.persistence.PurchaseOrderUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.security.auth.PrincipalThreadLocal;

/**
 * The implementation of the purchase order local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.PurchaseOrderLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.hp.omp.service.base.PurchaseOrderLocalServiceBaseImpl
 * @see com.hp.omp.service.PurchaseOrderLocalServiceUtil
 */
public class PurchaseOrderLocalServiceImpl
	extends PurchaseOrderLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.hp.omp.service.PurchaseOrderLocalServiceUtil} to access the purchase order local service.
	 */
	
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public PurchaseOrder addPurchaseOrder(PurchaseOrder purchaseOrder)
			throws SystemException {
		PurchaseOrder newPurchaseOrder = super.addPurchaseOrder(purchaseOrder);
		addAuditPO(newPurchaseOrder, newPurchaseOrder.getPurchaseOrderStatus());
		return newPurchaseOrder;
	}
	
	@Override
	@Indexable(type = IndexableType.DELETE)
	public PurchaseOrder deletePurchaseOrder(long purchaseOrderId)
			throws PortalException, SystemException {
		PurchaseOrder purchaseOrder = PurchaseOrderLocalServiceUtil.getPurchaseOrder(purchaseOrderId);
		addAuditPO(purchaseOrder, PurchaseOrderStatus.PO_DELETED);
		return super.deletePurchaseOrder(purchaseOrderId);
	}
	
	@Override
	@Indexable(type = IndexableType.DELETE)
	public PurchaseOrder deletePurchaseOrder(PurchaseOrder purchaseOrder)
			throws SystemException {
		addAuditPO(purchaseOrder, PurchaseOrderStatus.PO_DELETED);
		return super.deletePurchaseOrder(purchaseOrder);
	}
	
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public PurchaseOrder updatePurchaseOrder(PurchaseOrder purchaseOrder)
			throws SystemException {
		PurchaseOrder newPurchaseOrder = super.updatePurchaseOrder(purchaseOrder);
		addAuditPO(newPurchaseOrder, newPurchaseOrder.getPurchaseOrderStatus());
		return newPurchaseOrder; 
	}
	
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public PurchaseOrder updatePurchaseOrder(PurchaseOrder purchaseOrder,
			boolean merge) throws SystemException {
		PurchaseOrder newPurchaseOrder = super.updatePurchaseOrder(purchaseOrder, merge);
		addAuditPO(newPurchaseOrder, newPurchaseOrder.getPurchaseOrderStatus());
		return newPurchaseOrder;
	}
	
	private void addAuditPO(PurchaseOrder purchaseOrder, PurchaseOrderStatus status) throws SystemException {
		POAudit poAudit = new POAuditImpl();
		poAudit.setNew(true);
		poAudit.setStatus(status.getCode());
		long userId =  PrincipalThreadLocal.getUserId();
		poAudit.setUserId(userId);
		poAudit.setPurchaseOrderId(purchaseOrder.getPurchaseOrderId());
		poAudit.setChangeDate(new Date());
		POAuditLocalServiceUtil.addPOAudit(poAudit);
	}
	
	@SuppressWarnings("unchecked")
	public List<PurchaseOrder> getPurchaseOrdersListByStatus(int status ,int start,int end, String orderby, String order) throws Exception{
		
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseOrder.class);
		dynamicQuery.add(PropertyFactoryUtil.forName("status").eq(status));
		if("asc".equals(order)) {
			dynamicQuery.addOrder(OrderFactoryUtil.asc(orderby));
			logger.debug("getPurchaseOrdersListByStatus order asc" + orderby);
		} else {
			dynamicQuery.addOrder(OrderFactoryUtil.desc(orderby));
			logger.debug("getPurchaseOrdersListByStatus order desc " +  " order by " + orderby);
		}
		dynamicQuery.setLimit(start, end);
		return PurchaseOrderLocalServiceUtil.dynamicQuery(dynamicQuery);
	}
	
	public Long getPurchaseOrdersCountByStatus(int status) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseOrder.class);
		dynamicQuery.add(PropertyFactoryUtil.forName("status").eq(status));
		dynamicQuery.setProjection(ProjectionFactoryUtil.rowCount());
		@SuppressWarnings("unchecked")
		List<Long> resultQuery = PurchaseOrderLocalServiceUtil.dynamicQuery(dynamicQuery);
		return (resultQuery == null || resultQuery.size() == 0) ?  0L  : resultQuery.get(0);
	}
	
	public long getPurchaseOrdersGRRequestedCount(SearchDTO searchDto){
		return PurchaseOrderFinderUtil.getPurchaseOrdersGRRequestedCount(searchDto);
	}
	
	public List<PoListDTO> getPurchaseOrdersGRRequested(SearchDTO searchDto){
		return PurchaseOrderFinderUtil.getPurchaseOrdersGRRequested(searchDto);
	}
	
	public long getPurchaseOrdersGRClosedCount(SearchDTO searchDto){
		return PurchaseOrderFinderUtil.getPurchaseOrdersGRClosedCount(searchDto);
	}
	
	public List<PoListDTO> getPurchaseOrdersGRClosed(SearchDTO searchDto){
		return PurchaseOrderFinderUtil.getPurchaseOrdersGRClosed(searchDto);
	}
	public List<PurchaseOrder> getPurchaseOrdersListByStatusAndCostCenter(int status, long costCenterId, int start, int end, 
			String orderby, String order) throws Exception{
	
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseOrder.class);
		dynamicQuery.add(PropertyFactoryUtil.forName("status").eq(status));
		dynamicQuery.add(PropertyFactoryUtil.forName("costCenterId").eq(String.valueOf(costCenterId)));
		if("asc".equals(order)) {
			dynamicQuery.addOrder(OrderFactoryUtil.asc(orderby));
		} else {
			dynamicQuery.addOrder(OrderFactoryUtil.desc(orderby));
		}
//		dynamicQuery.addOrder(OrderFactoryUtil.desc("purchaseOrderId"));
		dynamicQuery.setLimit(start, end);
		return PurchaseOrderLocalServiceUtil.dynamicQuery(dynamicQuery);
	}
	
	public Long getPurchaseOrdersCountByStatusAndCostCenter(int status, long costCenterId) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseOrderModel.class);
		dynamicQuery.add(PropertyFactoryUtil.forName("status").eq(status));
		dynamicQuery.add(PropertyFactoryUtil.forName("costCenterId").eq(String.valueOf(costCenterId)));
		
		return PurchaseOrderUtil.getPersistence().countWithDynamicQuery(dynamicQuery);
		
	}

	public int getTotalNumberOfPurchaseOrdersByStatus(int status) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseOrderModel.class);
		dynamicQuery.add(PropertyFactoryUtil.forName("status").eq(status));
		
		 List<PurchaseOrder> poList = PurchaseOrderUtil.findWithDynamicQuery(dynamicQuery);
	
		 int poCount=0;
		 if(poList!= null){
			 poList.size();
		 }
		 return poCount;
	}
	
	public List<PurchaseOrder> getPurchaseOrderByProjectId(String projectId) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseOrder.class);
		
		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("projectId", projectId);
		
		dynamicQuery.add(criterion); 
		@SuppressWarnings("unchecked")
		List<PurchaseOrder>  purchaseOrderList = PurchaseOrderLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		return purchaseOrderList;
	}
	
	public List<PurchaseOrder> getPurchaseOrderBySubProjectId(String subProjectId) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseOrder.class);
		
		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("subProjectId", subProjectId);
		
		dynamicQuery.add(criterion); 
		@SuppressWarnings("unchecked")
		List<PurchaseOrder>  purchaseOrderList = PurchaseOrderLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		return purchaseOrderList;
	}
	
	public List<PurchaseOrder> getPurchaseOrderByVendorId(String vendorId) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseOrder.class);
		
		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("vendorId", vendorId);
		
		dynamicQuery.add(criterion); 
		@SuppressWarnings("unchecked")
		List<PurchaseOrder>  purchaseOrderList = PurchaseOrderLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		return purchaseOrderList;
	}
	
	public List<PurchaseOrder> getPurchaseOrderByBudgetCategoryId(String budgetCategoryId) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseOrder.class);
		
		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("budgetCategoryId", budgetCategoryId);
		
		dynamicQuery.add(criterion); 
		@SuppressWarnings("unchecked")
		List<PurchaseOrder>  purchaseOrderList = PurchaseOrderLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		return purchaseOrderList;
	}
	
	public List<PurchaseOrder> getPurchaseOrderByBudgetSubCategoryId(String budgetSubCategoryId) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseOrder.class);
		
		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("budgetSubCategoryId", budgetSubCategoryId);
		
		dynamicQuery.add(criterion); 
		@SuppressWarnings("unchecked")
		List<PurchaseOrder>  purchaseOrderList = PurchaseOrderLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		return purchaseOrderList;
	}
	
	public List<PurchaseOrder> getPurchaseOrderByOdnpNameId(String odnpNameId) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseOrder.class);
		
		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("odnpNameId", odnpNameId);
		
		dynamicQuery.add(criterion); 
		@SuppressWarnings("unchecked")
		List<PurchaseOrder>  purchaseOrderList = PurchaseOrderLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		return purchaseOrderList;
	}
	
	public List<PurchaseOrder> getPurchaseOrderByMacroDriverId(String macroDriverId) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseOrder.class);
		
		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("macroDriverId", macroDriverId);
		
		dynamicQuery.add(criterion); 
		@SuppressWarnings("unchecked")
		List<PurchaseOrder>  purchaseOrderList = PurchaseOrderLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		return purchaseOrderList;
	}
	
	public List<PurchaseOrder> getPurchaseOrderByMicroDriverId(String microDriverId) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(PurchaseOrder.class);
		
		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("microDriverId", microDriverId);
		
		dynamicQuery.add(criterion); 
		@SuppressWarnings("unchecked")
		List<PurchaseOrder>  purchaseOrderList = PurchaseOrderLocalServiceUtil.dynamicQuery(dynamicQuery);
		
		return purchaseOrderList;
	}
	
	public List<Long> getPONumbers() throws SystemException{
		return PurchaseOrderFinderUtil.getPONumbers();
	}

	
	public List<String> getAllPOProjects() throws SystemException{
		return PurchaseOrderFinderUtil.getAllPOProjects();
	}

	public List<String> getSubProjects(String projectName) throws SystemException{
		return PurchaseOrderFinderUtil.getSubProjects(projectName);
	}
	
	public List<PoListDTO> getAdvancedSearchResults(SearchDTO searchDTO, int start, int end, String orderby, String order) throws SystemException{
		return PurchaseOrderFinderUtil.getAdvancedSearchResults(searchDTO, start, end, orderby, order);
	}
	
	public List<PoListDTO> getAdvancedSearchGrRequestedResults(SearchDTO searchDTO, int start, int end, String orderby, String order) throws SystemException{
		return PurchaseOrderFinderUtil.getAdvancedSearchGrRequestedResults(searchDTO, start, end, orderby, order);
	}

	public Long getAdvancedSearchResultsCount(SearchDTO searchDTO) throws SystemException{
		return PurchaseOrderFinderUtil.getAdvancedSearchResultsCount(searchDTO);
	}

	public Long getAdvancedSearchGrRequestedResultsCount(SearchDTO searchDTO) throws SystemException{
		return PurchaseOrderFinderUtil.getAdvancedSearchGrRequestedResultsCount(searchDTO);
	}
	
	
	public static final Logger logger = LoggerFactory.getLogger(PurchaseOrderLocalServiceImpl.class);
}