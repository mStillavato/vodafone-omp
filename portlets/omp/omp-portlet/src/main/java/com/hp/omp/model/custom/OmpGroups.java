/**
 * 
 */
package com.hp.omp.model.custom;

import java.util.HashMap;
import java.util.Map;

/**
 * @author gewaly
 *
 */
public enum OmpGroups {
	
	ENG(1,"ENG"),
	ANTEX(2,"ANTEX"),
	CONTROLLER(3,"CONTROLLER"),
	OBSERVER(4,"OBSERVER"),
	NDSO(5,"NDSO"),
	TC(6,"TC"),
	ENG_VBS(7,"ENG_VBS"),
	CONTROLLER_VBS(8,"CONTROLLER_VBS"),
	APPROVER_VBS(9,"APPROVER_VBS");
	
	private int code;
	private String label;
	
	private OmpGroups(int code, String label){
		this.code = code;
		this.label = label;
	}
	
	
	  private static Map<Integer, OmpGroups> codeToStatusMapping;
		 
	 
	    public static OmpGroups getStatus(int i) {
	        if (codeToStatusMapping == null) {
	            initMapping();
	        }
	        return codeToStatusMapping.get(i);
	    }
	 
	    private static void initMapping() {
	        codeToStatusMapping = new HashMap<Integer, OmpGroups>();
	        for (OmpGroups s : values()) {
	            codeToStatusMapping.put(s.code, s);
	        }
	    }
	public int getCode(){
		return code;
	}
	
	public String getLabel(){
		return label;
	}
}

