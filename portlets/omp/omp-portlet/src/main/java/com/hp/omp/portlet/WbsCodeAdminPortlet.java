package com.hp.omp.portlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.model.Position;
import com.hp.omp.model.WbsCode;
import com.hp.omp.model.impl.WbsCodeImpl;
import com.hp.omp.service.PositionLocalServiceUtil;
import com.hp.omp.service.ProjectLocalServiceUtil;
import com.hp.omp.service.WbsCodeLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class WbsCodeAdminPortlet
 */
public class WbsCodeAdminPortlet extends MVCPortlet {
	private transient Logger logger = LoggerFactory.getLogger(WbsCodeAdminPortlet.class);
	private String errorMSG = "";
	private String successMSG = "";
	private String ACTION_PARAM= "action";
	private String ADD_ACTION= "add";
	private String UPDATE_ACTION= "update";
	private String DELETE_ACTION= "delete";
	private String PAGINATION_ACTION= "pagination";
	private String EXPORT_LIST= "exportList";
	
	

	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		super.doView(renderRequest, renderResponse);
	}
	
	
	/* (non-Javadoc)
	 * @see javax.portlet.GenericPortlet#render(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		viewAddWbsCode(request, response);
		super.render(request, response);
	}
	
	public void viewAddWbsCode(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		String action = renderRequest.getParameter(ACTION_PARAM);
		
		if(ADD_ACTION.equals(action)){
			renderRequest.setAttribute(ACTION_PARAM, ADD_ACTION);
			renderRequest.setAttribute("display",true);
		}else if(UPDATE_ACTION.equals(action)){
			String wbsCodeId = renderRequest.getParameter("wbsCodeId");
			WbsCode wbsCode = getWbsCode(wbsCodeId);
			
			logger.debug("render update action for : "+wbsCode.getWbsCodeName());
			logger.debug("Display = "+wbsCode.getDisplay());
			
			renderRequest.setAttribute(ACTION_PARAM, UPDATE_ACTION);
			renderRequest.setAttribute("wbsCodeId", wbsCode.getWbsCodeId());
			renderRequest.setAttribute("wbsCodeName", wbsCode.getWbsCodeName());
			renderRequest.setAttribute("wbsCodeDesc", wbsCode.getDescription());
			renderRequest.setAttribute("display", wbsCode.getDisplay());
			renderRequest.setAttribute("wbsOwnerName", wbsCode.getWbsOwnerName());
		}
	}
	
	@Override
    public void serveResource(ResourceRequest resourceRequest,
                  ResourceResponse resourceResponse) throws IOException,
                  PortletException {
		String action=resourceRequest.getParameter(ACTION_PARAM);
		if(DELETE_ACTION.equals(action)){
			deleteWbsCode(resourceRequest, resourceResponse);
		}else if(PAGINATION_ACTION.equals(action)){
			paginationList(resourceRequest, resourceResponse);
		} else if(EXPORT_LIST.equals(action)){
			exportList(resourceRequest, resourceResponse);
		}
		
		clearMessages();
           super.serveResource(resourceRequest, resourceResponse);
    }

	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#processAction(javax.portlet.ActionRequest, javax.portlet.ActionResponse)
	 */
	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {

		clearMessages();
		String action = actionRequest.getParameter(ACTION_PARAM);
		String wbsCodeName = actionRequest.getParameter("wbsCodeName");
		String wbsCodeDesc = actionRequest.getParameter("wbsCodeDesc");
		boolean display = ParamUtil.getBoolean(actionRequest,"display");
		String wbsOwnerName = actionRequest.getParameter("wbsOwnerName");
		String operation = "";
		
		logger.debug("wbsCodeName="+wbsCodeName);
		logger.debug("wbsCodeDesc="+wbsCodeDesc);
		logger.debug("display = "+display);
		
		logger.debug("creating wbs code object ...");
		WbsCode newWbsCode = new WbsCodeImpl();
		newWbsCode.setWbsCodeName(wbsCodeName);
		newWbsCode.setDescription(wbsCodeDesc);
		newWbsCode.setDisplay(display);
		newWbsCode.setWbsOwnerName(wbsOwnerName);
		logger.debug("wbs code object created !!");
		try {
			if(ADD_ACTION.equals(action)){
				operation = "added";
				logger.debug("adding new wbs code ["+wbsCodeName+"] in database ...");
				if(WbsCodeLocalServiceUtil.getWbsCodeByName(wbsCodeName) == null){
					
					WbsCodeLocalServiceUtil.addWbsCode(newWbsCode);
					
					successMSG = "wbs code name ["+wbsCodeName+"] successfully Added";
					
				}else{
					errorMSG = "wbs code name ["+wbsCodeName+"] already existing";
				}
			}else if(UPDATE_ACTION.equals(action)){
				operation = "updated";
				String wbsCodeId = actionRequest.getParameter("wbsCodeId");
				
				newWbsCode.setWbsCodeId(Integer.valueOf(wbsCodeId));
				
				if(! (isWbsCodeNameExists(newWbsCode, wbsCodeName))){
					
					logger.debug("updating  wbs code ["+wbsCodeId+"] in database ...");
					
					
					WbsCodeLocalServiceUtil.updateWbsCode(newWbsCode);
					successMSG = "wbs code Name ["+wbsCodeName+"] successfully Updated";
				}else{
					errorMSG = "wbs code name ["+wbsCodeName+"] already existing";
				}
				
			}
			
			
		} catch (SystemException e) {
			errorMSG = "Wbs Code ["+wbsCodeName+"] not "+operation+".";
			logger.error("error happened while "+operation+" wbs code ["+wbsCodeName+"] in database.");
			logger.error(e.getMessage(), e);
		}catch (NumberFormatException nfe) {
			errorMSG = "Wbs Code ["+wbsCodeName+"] not "+operation+".";
			logger.error("error happened while "+operation+" wbs code ["+wbsCodeName+"] in database.");
			logger.error(nfe.getMessage(), nfe);
		}
		
		actionRequest.setAttribute("errorMSG", errorMSG);
		actionRequest.setAttribute("successMSG", successMSG);
		super.processAction(actionRequest, actionResponse);
	}
	
	private void retrieveWbsCodeListToRequest(PortletRequest renderRequest) throws SystemException{
		try {
			String searchFilter = ParamUtil.getString(renderRequest, "searchFilter","");
			
			int totalCount = 0;
			if (searchFilter == null || searchFilter.trim().length() == 0)
				totalCount = WbsCodeLocalServiceUtil.getWbsCodesCount();
			else
				totalCount = WbsCodeLocalServiceUtil.getWbsCodesCountList(searchFilter);

			logger.debug("Wbs Code Total Count = " + totalCount);

			Integer pageNumber = ParamUtil.getInteger(renderRequest, "pageNumber", 1);
			Integer pageSize = OmpHelper.PAGE_SIZE;
			int start = (pageNumber - 1) * pageSize;
			int end = start + pageSize;

			renderRequest.setAttribute("numberOfPages", getTotalNumberOfPages(totalCount, pageSize));
			renderRequest.setAttribute("pageNumber", pageNumber);


			//List<WbsCode> wbsCodeList = WbsCodeLocalServiceUtil.getWbsCodes(start, end);
			List<WbsCode> wbsCodeList = WbsCodeLocalServiceUtil.getWbsCodesList(start, end, searchFilter);

			renderRequest.setAttribute("wbsCodeList", wbsCodeList);
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			errorMSG = "can not retrieve Wbs Code list";
			renderRequest.setAttribute("errorMSG", errorMSG);
		}

	}
	
	public void deleteWbsCode(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse)
			throws PortletException, IOException {
		clearMessages();
		
		String wbsCodeId=resourceRequest.getParameter("wbsCodeId");
		String wbsCodeName=resourceRequest.getParameter("wbsCodeName");
		String action=resourceRequest.getParameter("action");
		String jspPage = "/html/wbscodeadmin/wbscodelist.jsp";
		
		if(Validator.isBlank(action)){
			errorMSG = "No Action in the request";
		}else if(Validator.isBlank(wbsCodeId)){
			errorMSG = "No wbs code Id in the request";
		}else{
			
			try {
				List<Position> poistionList = PositionLocalServiceUtil.getPositionsByWbsCodeId(wbsCodeId);
				if(poistionList == null || poistionList.isEmpty()){
					WbsCodeLocalServiceUtil.deleteWbsCode(Long.valueOf(wbsCodeId));
					successMSG = "wbs code name ["+wbsCodeName+"] successfully deleted";
				}else{
					errorMSG = "Can Not remove WBS code assigned to positions, Wbs Code name ["+wbsCodeName+"]";
				}

			} catch (NumberFormatException e) {
				logger.error(e.getMessage());
				errorMSG = "wbs code Id is invalid";
			} catch (PortalException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove wbs code";
			} catch (SystemException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove wbs code [most probably wbs code assigned.]";
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove wbs code";
			}
			
		}
		
		//retrieve updated list
		try {
			retrieveWbsCodeListToRequest(resourceRequest);
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			errorMSG = "can not retrieve wbs code list";
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
		
	}
	
	private WbsCode getWbsCode(String wbsCodeId){
		WbsCode wbsCode = null;
		try {
			wbsCode = WbsCodeLocalServiceUtil.getWbsCode(Integer.valueOf(wbsCodeId));
		} catch (NumberFormatException e) {
			logger.error("wbs code Id has wrong format", e);
		} catch (PortalException e) {
			logger.error("unexpected error", e);
		} catch (SystemException e) {
			logger.error("unexpected error", e);
		}
		
		return wbsCode;
	}
	
	private boolean isWbsCodeNameExists(WbsCode newWbsCode, String wbsCodeName) throws SystemException{
		
		WbsCode oldWbsCode = WbsCodeLocalServiceUtil.getWbsCodeByName(wbsCodeName);
		boolean isExists = false;
		
		if(oldWbsCode != null && 
				(newWbsCode.getWbsCodeId() != oldWbsCode.getWbsCodeId()) ){
			isExists = true;
		}
		return isExists;
	}
	
	private void paginationList(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse) throws PortletException, IOException{
		
		String jspPage = "/html/wbscodeadmin/wbscodelist.jsp";
		try {
			retrieveWbsCodeListToRequest(resourceRequest);
		} catch (SystemException e) {
		logger.error("error happened in pagination ... ",e);
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
	}
	
	private Integer getTotalNumberOfPages(Integer totalNubmerOfResult, Integer pageSize) {
        return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
	}
	
	private void clearMessages(){
		errorMSG = "";
		successMSG = "";
	}

	public void searchFilter(ActionRequest request, ActionResponse response) throws IOException, PortletException{
		response.setRenderParameter("searchFilter", "%" + ParamUtil.getString(request, "wbsName") + "%");  
	}
	
	private void exportList(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException{
		logger.info("inside exportList");  
		
		String searchFilter = ParamUtil.getString(resourceRequest, "searchFilter","").toUpperCase();
		
		logger.info("searchTCN = " + searchFilter);
		
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		
		String fileName = "OMP_WbsCode_"+formatter.format(date)+".xls";
		resourceResponse.setContentType("application/vnd.ms-excel");
		
		resourceResponse.addProperty(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=\""+fileName+"\"");			
		resourceResponse.addProperty(HttpHeaders.CACHE_CONTROL,"max-age=3600, must-revalidate");

		logger.debug("exporting.." + fileName);
		HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(resourceResponse);
		try {
			int totalCount = 0;
			
			if (searchFilter == null || searchFilter.trim().length() == 0)
				totalCount = WbsCodeLocalServiceUtil.getWbsCodesCount();
			else
				totalCount = WbsCodeLocalServiceUtil.getWbsCodesCountList(searchFilter);

			logger.debug("WbsCode Total Count = " + totalCount);
			List<com.hp.omp.model.WbsCode> WbsCodesList = WbsCodeLocalServiceUtil.getWbsCodesList(0, totalCount, searchFilter);
			WbsCodeLocalServiceUtil.exportWbsCodesAsExcel(WbsCodesList, getWbsCodeReportHeader(), httpServletResponse.getOutputStream());

		} catch (SystemException e) {
			logger.error("Error in exportWbsCode", e);
			e.printStackTrace();
		}
	}
	
	private List<Object> getWbsCodeReportHeader() {
		List<Object> header = new ArrayList<Object>();
		header.add("NAME");			//0
		header.add("DESCRIPTION");	//1
		header.add("DISPLAY");		//2
		
		return header;
	}
}
