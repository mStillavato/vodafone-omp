package com.hp.omp.decorator;

import java.io.Serializable;

import org.displaytag.decorator.TableDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.SubProject;

public class SubProjectListDecorator extends TableDecorator implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2685221218697856230L;

	Logger logger = LoggerFactory.getLogger(SubProjectListDecorator.class);
	
	public String getSubProjectId() {
		
        return getRemoveAction();
    }
	
	public String getSubProjectName() {
		
        return getUpdateAction();
    }
	
	public String getRemoveAction() {
		
        StringBuilder sb = new StringBuilder();
        SubProject item = (SubProject) this.getCurrentRowObject();
      
        long subProjectId = item.getSubProjectId();
        String subProjectName = item.getSubProjectName();
        sb.append("<a href=\"javascript:removeSubProject("+subProjectId+",'"+subProjectName+"')\"");
        sb.append("\">");
        sb.append(" Remove ");
        sb.append("</a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
    }	
	
	public String getUpdateAction() {
		
		SubProject item = (SubProject) this.getCurrentRowObject();
		StringBuilder sb = new StringBuilder();
       
      
        long subProjectId = item.getSubProjectId();
        String subProjectName = item.getSubProjectName();
        sb.append("<a onclick=\"javascript:updateSubProject(this, "+subProjectId+")\" href='"+subProjectName+"'");
        sb.append("\"> ");
        sb.append(subProjectName);
        sb.append(" </a>");
        
        logger.debug(sb.toString());
        
        return sb.toString();
        
    }

}
