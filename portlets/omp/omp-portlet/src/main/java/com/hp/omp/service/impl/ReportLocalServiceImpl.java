package com.hp.omp.service.impl;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;

import com.hp.omp.model.ExcelReport;
import com.hp.omp.model.ExcelStyler;
import com.hp.omp.model.ExcelTable;
import com.hp.omp.model.custom.Currency;
import com.hp.omp.model.custom.ExcelTableRow;
import com.hp.omp.model.custom.ReportCheckIcDTO;
import com.hp.omp.model.custom.ReportDTO;
import com.hp.omp.model.custom.SearchReportCheckIcDTO;
import com.hp.omp.model.impl.ExcelDefaultStyler;
import com.hp.omp.model.impl.ExcelReportImpl;
import com.hp.omp.model.impl.ExcelTableImpl;
import com.hp.omp.service.base.ReportLocalServiceBaseImpl;
import com.hp.omp.service.persistence.ReportPersistence;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

/**
 * The implementation of the report local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.ReportLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.hp.omp.service.base.ReportLocalServiceBaseImpl
 * @see com.hp.omp.service.ReportLocalServiceUtil
 */
public class ReportLocalServiceImpl extends ReportLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.hp.omp.service.ReportLocalServiceUtil} to access the report local service.
     */
	private ReportPersistence _reportPersistence = null;
	
	private static Log _log = LogFactoryUtil.getLog(ReportLocalServiceImpl.class);
	
	private static void debug(String msg){
		if(_log.isDebugEnabled()){
			_log.debug(msg);
		}
	}
	
	public void setReportPersistence(ReportPersistence reportPersistence){
		_reportPersistence = reportPersistence;
	}
	
	public List<String> getAllCostCenters() throws SystemException{
		List<String> costCenters = _reportPersistence.getAllCostCenters();
		return costCenters;
	}
	
	public List<String> getAllProjectNames() throws SystemException{
		List<String> projectNames = _reportPersistence.getAllProjectNames();
		return projectNames;
	}
	
	public List<String> getSubProjects(Long projectId) throws SystemException{
		List<String> subProjects = _reportPersistence.getSubProjects(projectId);
		return subProjects;
	}
	
	public List<String> getAllFiscalYears() throws SystemException{
		List<String> fiscalYears = _reportPersistence.getAllFiscalYears();
		return fiscalYears;
	}
	
	public Double generateAggregateReportClosed(ReportDTO reportDTO)
			throws SystemException {
		Double generateAggregateReportBase = _reportPersistence.generateAggregateReport(reportDTO,true);
		return generateAggregateReportBase;
	}
	
	public Double generateAggregateReportClosing(ReportDTO reportDTO)
			throws SystemException {
		Double generateAggregateReportBase = _reportPersistence.generateAggregateReport(reportDTO,false);
		return generateAggregateReportBase;
	}
	
	public Double generateAggregateReportTotalPoistionValues(ReportDTO reportDTO)
			throws SystemException {
		Double generateAggregateReportBase = _reportPersistence.generateAggregateReportTotalPoistionValues(reportDTO);
		return generateAggregateReportBase;
	}
	
	public Double generateAggregatePrPositionsWithoutPoNumberValue(ReportDTO reportDTO)
			throws SystemException {
		Double generateAggregatePRReportBase = _reportPersistence.generateAggregatePrPositionsWithoutPoNumberValue(reportDTO);
		return generateAggregatePRReportBase;
	}
	
	public List<ExcelTableRow> generateDetailReport(ReportDTO reportDTO) throws SystemException {
		List<ExcelTableRow> generateDetailReport = _reportPersistence.generateDetailReport(reportDTO);
		for (ExcelTableRow excelTableRow : generateDetailReport) {
			for (int i = 0; i < excelTableRow.getTotalNumberFields(); i++) {
				System.out.print(excelTableRow.getFieldAtIndex(i)+" ");
			}
			System.out.println();
		}
		return generateDetailReport;
	}
	
	public void exportDetailsReportAsExcel(ReportDTO reportDTO, List<Object> header, OutputStream out) throws SystemException {
		ExcelReport excelReport = new ExcelReportImpl();
		ExcelStyler styler = new ExcelDefaultStyler(excelReport.getWorkBook());
		excelReport.setStyler(styler);
		List<ExcelTableRow> details = _reportPersistence.generateControllerDetailReport(reportDTO);
		_log.debug("Details size: "+details.size());
		
		ExcelTable excelTable = new ExcelTableImpl(header, details, "Details");
		excelReport.addExcelTable(excelTable, 0);
		
		Workbook exelWorkBook = excelReport.createExelWorkBook();
		_log.debug("created workBook: "+exelWorkBook);
		_log.debug("total processed rows: "+excelReport.getTotalRowsProcessed());
		try {
			exelWorkBook.write(out);
		} catch (IOException e) {
			_log.error("IO Exception while exporting excel file ",e);
		} finally{
			try {
				out.flush();
				out.close();
			} catch (IOException e) {
				_log.error("IO Exception while closing output stream: "+e.getMessage());
			}
			
		}
	}
	
	public void exportReportAsExcel(ReportDTO reportDTO, List<Object> header, OutputStream out) throws SystemException {
		
		ExcelReport excelReport = new ExcelReportImpl();
		ExcelStyler styler = new ExcelDefaultStyler(excelReport.getWorkBook());
		excelReport.setStyler(styler);
		
		if(Currency.EUR.getCode() == reportDTO.getCurrency()){
			List<ExcelTableRow> euroDetails = _reportPersistence.generateDetailReport(reportDTO);
			debug("Euro Details size: "+euroDetails.size());
			
			ExcelTable euroTable = new ExcelTableImpl(header, euroDetails, "Euro Details");
			excelReport.addExcelTable(euroTable, 0);
		}else{
			//Default USD
			List<ExcelTableRow> usdDetails = _reportPersistence.generateDetailReport(reportDTO);
			debug("USD Details size: "+usdDetails.size());
			
			ExcelTable usdTable = new ExcelTableImpl(header, usdDetails, "USD Details");
			excelReport.addExcelTable(usdTable, 0);
		}

		
		Workbook exelWorkBook = excelReport.createExelWorkBook();
		debug("created workBook: "+exelWorkBook);
		try {
			exelWorkBook.write(out);
		} catch (IOException e) {
			_log.error("IO Exception while exporting excel file: "+e.getMessage());
		} finally{
			try {
				out.flush();
				out.close();
			} catch (IOException e) {
				_log.error("IO Exception while closing output stream: "+e.getMessage());
			}
			
		}
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<ReportCheckIcDTO> getReportCheckIcList(SearchReportCheckIcDTO searchDTO) throws SystemException {		
		return _reportPersistence.getReportCheckIcList(searchDTO);
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public long getReportCheckIcListCount(SearchReportCheckIcDTO searchDTO) throws SystemException {
		return _reportPersistence.getReportCheckIcListCount(searchDTO);
	}

	public void exportReportCheckIcAsExcel(List<Object> header, OutputStream out)
			throws SystemException {
		
		ExcelReport excelReport = new ExcelReportImpl();
		ExcelStyler styler = new ExcelDefaultStyler(excelReport.getWorkBook());
		excelReport.setStyler(styler);
		
		List<ExcelTableRow> euroDetails = _reportPersistence.generateCheckIcReport();
		debug("Euro Details size: " + euroDetails.size());
		
		ExcelTable euroTable = new ExcelTableImpl(header, euroDetails, "Euro Details");
		excelReport.addExcelTable(euroTable, 0);
		
		Workbook exelWorkBook = excelReport.createExelWorkBook();
		debug("created workBook: " + exelWorkBook);
		try {
			exelWorkBook.write(out);
		} catch (IOException e) {
			_log.error("IO Exception while exporting excel file: "+e.getMessage());
		} finally{
			try {
				out.flush();
				out.close();
			} catch (IOException e) {
				_log.error("IO Exception while closing output stream: "+e.getMessage());
			}
		}
	}
	
}
