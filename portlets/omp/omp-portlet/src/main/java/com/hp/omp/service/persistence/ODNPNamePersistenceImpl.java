package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchODNPNameException;
import com.hp.omp.model.ODNPName;
import com.hp.omp.model.impl.ODNPNameImpl;
import com.hp.omp.model.impl.ODNPNameModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the o d n p name service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see ODNPNamePersistence
 * @see ODNPNameUtil
 * @generated
 */
public class ODNPNamePersistenceImpl extends BasePersistenceImpl<ODNPName>
    implements ODNPNamePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link ODNPNameUtil} to access the o d n p name persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = ODNPNameImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ODNPNameModelImpl.ENTITY_CACHE_ENABLED,
            ODNPNameModelImpl.FINDER_CACHE_ENABLED, ODNPNameImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ODNPNameModelImpl.ENTITY_CACHE_ENABLED,
            ODNPNameModelImpl.FINDER_CACHE_ENABLED, ODNPNameImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ODNPNameModelImpl.ENTITY_CACHE_ENABLED,
            ODNPNameModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_ODNPNAME = "SELECT odnpName FROM ODNPName odnpName";
    private static final String _SQL_COUNT_ODNPNAME = "SELECT COUNT(odnpName) FROM ODNPName odnpName";
    private static final String _ORDER_BY_ENTITY_ALIAS = "odnpName.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ODNPName exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(ODNPNamePersistenceImpl.class);
    private static ODNPName _nullODNPName = new ODNPNameImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<ODNPName> toCacheModel() {
                return _nullODNPNameCacheModel;
            }
        };

    private static CacheModel<ODNPName> _nullODNPNameCacheModel = new CacheModel<ODNPName>() {
            public ODNPName toEntityModel() {
                return _nullODNPName;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the o d n p name in the entity cache if it is enabled.
     *
     * @param odnpName the o d n p name
     */
    public void cacheResult(ODNPName odnpName) {
        EntityCacheUtil.putResult(ODNPNameModelImpl.ENTITY_CACHE_ENABLED,
            ODNPNameImpl.class, odnpName.getPrimaryKey(), odnpName);

        odnpName.resetOriginalValues();
    }

    /**
     * Caches the o d n p names in the entity cache if it is enabled.
     *
     * @param odnpNames the o d n p names
     */
    public void cacheResult(List<ODNPName> odnpNames) {
        for (ODNPName odnpName : odnpNames) {
            if (EntityCacheUtil.getResult(
                        ODNPNameModelImpl.ENTITY_CACHE_ENABLED,
                        ODNPNameImpl.class, odnpName.getPrimaryKey()) == null) {
                cacheResult(odnpName);
            } else {
                odnpName.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all o d n p names.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(ODNPNameImpl.class.getName());
        }

        EntityCacheUtil.clearCache(ODNPNameImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the o d n p name.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(ODNPName odnpName) {
        EntityCacheUtil.removeResult(ODNPNameModelImpl.ENTITY_CACHE_ENABLED,
            ODNPNameImpl.class, odnpName.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<ODNPName> odnpNames) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (ODNPName odnpName : odnpNames) {
            EntityCacheUtil.removeResult(ODNPNameModelImpl.ENTITY_CACHE_ENABLED,
                ODNPNameImpl.class, odnpName.getPrimaryKey());
        }
    }

    /**
     * Creates a new o d n p name with the primary key. Does not add the o d n p name to the database.
     *
     * @param odnpNameId the primary key for the new o d n p name
     * @return the new o d n p name
     */
    public ODNPName create(long odnpNameId) {
        ODNPName odnpName = new ODNPNameImpl();

        odnpName.setNew(true);
        odnpName.setPrimaryKey(odnpNameId);

        return odnpName;
    }

    /**
     * Removes the o d n p name with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param odnpNameId the primary key of the o d n p name
     * @return the o d n p name that was removed
     * @throws com.hp.omp.NoSuchODNPNameException if a o d n p name with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public ODNPName remove(long odnpNameId)
        throws NoSuchODNPNameException, SystemException {
        return remove(Long.valueOf(odnpNameId));
    }

    /**
     * Removes the o d n p name with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the o d n p name
     * @return the o d n p name that was removed
     * @throws com.hp.omp.NoSuchODNPNameException if a o d n p name with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ODNPName remove(Serializable primaryKey)
        throws NoSuchODNPNameException, SystemException {
        Session session = null;

        try {
            session = openSession();

            ODNPName odnpName = (ODNPName) session.get(ODNPNameImpl.class,
                    primaryKey);

            if (odnpName == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchODNPNameException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(odnpName);
        } catch (NoSuchODNPNameException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected ODNPName removeImpl(ODNPName odnpName) throws SystemException {
        odnpName = toUnwrappedModel(odnpName);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, odnpName);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(odnpName);

        return odnpName;
    }

    @Override
    public ODNPName updateImpl(com.hp.omp.model.ODNPName odnpName, boolean merge)
        throws SystemException {
        odnpName = toUnwrappedModel(odnpName);

        boolean isNew = odnpName.isNew();

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, odnpName, merge);

            odnpName.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(ODNPNameModelImpl.ENTITY_CACHE_ENABLED,
            ODNPNameImpl.class, odnpName.getPrimaryKey(), odnpName);

        return odnpName;
    }

    protected ODNPName toUnwrappedModel(ODNPName odnpName) {
        if (odnpName instanceof ODNPNameImpl) {
            return odnpName;
        }

        ODNPNameImpl odnpNameImpl = new ODNPNameImpl();

        odnpNameImpl.setNew(odnpName.isNew());
        odnpNameImpl.setPrimaryKey(odnpName.getPrimaryKey());

        odnpNameImpl.setOdnpNameId(odnpName.getOdnpNameId());
        odnpNameImpl.setOdnpNameName(odnpName.getOdnpNameName());
        odnpNameImpl.setDisplay(odnpName.isDisplay());

        return odnpNameImpl;
    }

    /**
     * Returns the o d n p name with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the o d n p name
     * @return the o d n p name
     * @throws com.liferay.portal.NoSuchModelException if a o d n p name with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ODNPName findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the o d n p name with the primary key or throws a {@link com.hp.omp.NoSuchODNPNameException} if it could not be found.
     *
     * @param odnpNameId the primary key of the o d n p name
     * @return the o d n p name
     * @throws com.hp.omp.NoSuchODNPNameException if a o d n p name with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public ODNPName findByPrimaryKey(long odnpNameId)
        throws NoSuchODNPNameException, SystemException {
        ODNPName odnpName = fetchByPrimaryKey(odnpNameId);

        if (odnpName == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + odnpNameId);
            }

            throw new NoSuchODNPNameException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                odnpNameId);
        }

        return odnpName;
    }

    /**
     * Returns the o d n p name with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the o d n p name
     * @return the o d n p name, or <code>null</code> if a o d n p name with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ODNPName fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the o d n p name with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param odnpNameId the primary key of the o d n p name
     * @return the o d n p name, or <code>null</code> if a o d n p name with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public ODNPName fetchByPrimaryKey(long odnpNameId)
        throws SystemException {
        ODNPName odnpName = (ODNPName) EntityCacheUtil.getResult(ODNPNameModelImpl.ENTITY_CACHE_ENABLED,
                ODNPNameImpl.class, odnpNameId);

        if (odnpName == _nullODNPName) {
            return null;
        }

        if (odnpName == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                odnpName = (ODNPName) session.get(ODNPNameImpl.class,
                        Long.valueOf(odnpNameId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (odnpName != null) {
                    cacheResult(odnpName);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(ODNPNameModelImpl.ENTITY_CACHE_ENABLED,
                        ODNPNameImpl.class, odnpNameId, _nullODNPName);
                }

                closeSession(session);
            }
        }

        return odnpName;
    }

    /**
     * Returns all the o d n p names.
     *
     * @return the o d n p names
     * @throws SystemException if a system exception occurred
     */
    public List<ODNPName> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the o d n p names.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of o d n p names
     * @param end the upper bound of the range of o d n p names (not inclusive)
     * @return the range of o d n p names
     * @throws SystemException if a system exception occurred
     */
    public List<ODNPName> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the o d n p names.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of o d n p names
     * @param end the upper bound of the range of o d n p names (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of o d n p names
     * @throws SystemException if a system exception occurred
     */
    public List<ODNPName> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<ODNPName> list = (List<ODNPName>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_ODNPNAME);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_ODNPNAME;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<ODNPName>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<ODNPName>) QueryUtil.list(q, getDialect(),
                            start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the o d n p names from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (ODNPName odnpName : findAll()) {
            remove(odnpName);
        }
    }

    /**
     * Returns the number of o d n p names.
     *
     * @return the number of o d n p names
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_ODNPNAME);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the o d n p name persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.ODNPName")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<ODNPName>> listenersList = new ArrayList<ModelListener<ODNPName>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<ODNPName>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(ODNPNameImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
