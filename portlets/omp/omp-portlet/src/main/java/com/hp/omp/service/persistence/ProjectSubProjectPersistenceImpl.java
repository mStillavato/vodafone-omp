package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchProjectSubProjectException;
import com.hp.omp.model.ProjectSubProject;
import com.hp.omp.model.impl.ProjectSubProjectImpl;
import com.hp.omp.model.impl.ProjectSubProjectModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the project sub project service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see ProjectSubProjectPersistence
 * @see ProjectSubProjectUtil
 * @generated
 */
public class ProjectSubProjectPersistenceImpl extends BasePersistenceImpl<ProjectSubProject>
    implements ProjectSubProjectPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link ProjectSubProjectUtil} to access the project sub project persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = ProjectSubProjectImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTID =
        new FinderPath(ProjectSubProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectSubProjectModelImpl.FINDER_CACHE_ENABLED,
            ProjectSubProjectImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByProjectId",
            new String[] {
                Long.class.getName(),
                
            "java.lang.Integer", "java.lang.Integer",
                "com.liferay.portal.kernel.util.OrderByComparator"
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID =
        new FinderPath(ProjectSubProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectSubProjectModelImpl.FINDER_CACHE_ENABLED,
            ProjectSubProjectImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByProjectId",
            new String[] { Long.class.getName() },
            ProjectSubProjectModelImpl.PROJECTID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_PROJECTID = new FinderPath(ProjectSubProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectSubProjectModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByProjectId",
            new String[] { Long.class.getName() });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SUBPROJECTID =
        new FinderPath(ProjectSubProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectSubProjectModelImpl.FINDER_CACHE_ENABLED,
            ProjectSubProjectImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBySubProjectId",
            new String[] {
                Long.class.getName(),
                
            "java.lang.Integer", "java.lang.Integer",
                "com.liferay.portal.kernel.util.OrderByComparator"
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBPROJECTID =
        new FinderPath(ProjectSubProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectSubProjectModelImpl.FINDER_CACHE_ENABLED,
            ProjectSubProjectImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBySubProjectId",
            new String[] { Long.class.getName() },
            ProjectSubProjectModelImpl.SUBPROJECTID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_SUBPROJECTID = new FinderPath(ProjectSubProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectSubProjectModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBySubProjectId",
            new String[] { Long.class.getName() });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTIDANDSUBPROJECTID =
        new FinderPath(ProjectSubProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectSubProjectModelImpl.FINDER_CACHE_ENABLED,
            ProjectSubProjectImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByProjectIdAndSubProjectId",
            new String[] {
                Long.class.getName(), Long.class.getName(),
                
            "java.lang.Integer", "java.lang.Integer",
                "com.liferay.portal.kernel.util.OrderByComparator"
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTIDANDSUBPROJECTID =
        new FinderPath(ProjectSubProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectSubProjectModelImpl.FINDER_CACHE_ENABLED,
            ProjectSubProjectImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByProjectIdAndSubProjectId",
            new String[] { Long.class.getName(), Long.class.getName() },
            ProjectSubProjectModelImpl.PROJECTID_COLUMN_BITMASK |
            ProjectSubProjectModelImpl.SUBPROJECTID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_PROJECTIDANDSUBPROJECTID =
        new FinderPath(ProjectSubProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectSubProjectModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByProjectIdAndSubProjectId",
            new String[] { Long.class.getName(), Long.class.getName() });
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ProjectSubProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectSubProjectModelImpl.FINDER_CACHE_ENABLED,
            ProjectSubProjectImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ProjectSubProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectSubProjectModelImpl.FINDER_CACHE_ENABLED,
            ProjectSubProjectImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ProjectSubProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectSubProjectModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_PROJECTSUBPROJECT = "SELECT projectSubProject FROM ProjectSubProject projectSubProject";
    private static final String _SQL_SELECT_PROJECTSUBPROJECT_WHERE = "SELECT projectSubProject FROM ProjectSubProject projectSubProject WHERE ";
    private static final String _SQL_COUNT_PROJECTSUBPROJECT = "SELECT COUNT(projectSubProject) FROM ProjectSubProject projectSubProject";
    private static final String _SQL_COUNT_PROJECTSUBPROJECT_WHERE = "SELECT COUNT(projectSubProject) FROM ProjectSubProject projectSubProject WHERE ";
    private static final String _FINDER_COLUMN_PROJECTID_PROJECTID_2 = "projectSubProject.projectId = ?";
    private static final String _FINDER_COLUMN_SUBPROJECTID_SUBPROJECTID_2 = "projectSubProject.subProjectId = ?";
    private static final String _FINDER_COLUMN_PROJECTIDANDSUBPROJECTID_PROJECTID_2 =
        "projectSubProject.projectId = ? AND ";
    private static final String _FINDER_COLUMN_PROJECTIDANDSUBPROJECTID_SUBPROJECTID_2 =
        "projectSubProject.subProjectId = ?";
    private static final String _ORDER_BY_ENTITY_ALIAS = "projectSubProject.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ProjectSubProject exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No ProjectSubProject exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(ProjectSubProjectPersistenceImpl.class);
    private static ProjectSubProject _nullProjectSubProject = new ProjectSubProjectImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<ProjectSubProject> toCacheModel() {
                return _nullProjectSubProjectCacheModel;
            }
        };

    private static CacheModel<ProjectSubProject> _nullProjectSubProjectCacheModel =
        new CacheModel<ProjectSubProject>() {
            public ProjectSubProject toEntityModel() {
                return _nullProjectSubProject;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the project sub project in the entity cache if it is enabled.
     *
     * @param projectSubProject the project sub project
     */
    public void cacheResult(ProjectSubProject projectSubProject) {
        EntityCacheUtil.putResult(ProjectSubProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectSubProjectImpl.class, projectSubProject.getPrimaryKey(),
            projectSubProject);

        projectSubProject.resetOriginalValues();
    }

    /**
     * Caches the project sub projects in the entity cache if it is enabled.
     *
     * @param projectSubProjects the project sub projects
     */
    public void cacheResult(List<ProjectSubProject> projectSubProjects) {
        for (ProjectSubProject projectSubProject : projectSubProjects) {
            if (EntityCacheUtil.getResult(
                        ProjectSubProjectModelImpl.ENTITY_CACHE_ENABLED,
                        ProjectSubProjectImpl.class,
                        projectSubProject.getPrimaryKey()) == null) {
                cacheResult(projectSubProject);
            } else {
                projectSubProject.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all project sub projects.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(ProjectSubProjectImpl.class.getName());
        }

        EntityCacheUtil.clearCache(ProjectSubProjectImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the project sub project.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(ProjectSubProject projectSubProject) {
        EntityCacheUtil.removeResult(ProjectSubProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectSubProjectImpl.class, projectSubProject.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<ProjectSubProject> projectSubProjects) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (ProjectSubProject projectSubProject : projectSubProjects) {
            EntityCacheUtil.removeResult(ProjectSubProjectModelImpl.ENTITY_CACHE_ENABLED,
                ProjectSubProjectImpl.class, projectSubProject.getPrimaryKey());
        }
    }

    /**
     * Creates a new project sub project with the primary key. Does not add the project sub project to the database.
     *
     * @param projectSubProjectId the primary key for the new project sub project
     * @return the new project sub project
     */
    public ProjectSubProject create(long projectSubProjectId) {
        ProjectSubProject projectSubProject = new ProjectSubProjectImpl();

        projectSubProject.setNew(true);
        projectSubProject.setPrimaryKey(projectSubProjectId);

        return projectSubProject;
    }

    /**
     * Removes the project sub project with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param projectSubProjectId the primary key of the project sub project
     * @return the project sub project that was removed
     * @throws com.hp.omp.NoSuchProjectSubProjectException if a project sub project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public ProjectSubProject remove(long projectSubProjectId)
        throws NoSuchProjectSubProjectException, SystemException {
        return remove(Long.valueOf(projectSubProjectId));
    }

    /**
     * Removes the project sub project with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the project sub project
     * @return the project sub project that was removed
     * @throws com.hp.omp.NoSuchProjectSubProjectException if a project sub project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ProjectSubProject remove(Serializable primaryKey)
        throws NoSuchProjectSubProjectException, SystemException {
        Session session = null;

        try {
            session = openSession();

            ProjectSubProject projectSubProject = (ProjectSubProject) session.get(ProjectSubProjectImpl.class,
                    primaryKey);

            if (projectSubProject == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchProjectSubProjectException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(projectSubProject);
        } catch (NoSuchProjectSubProjectException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected ProjectSubProject removeImpl(ProjectSubProject projectSubProject)
        throws SystemException {
        projectSubProject = toUnwrappedModel(projectSubProject);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, projectSubProject);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(projectSubProject);

        return projectSubProject;
    }

    @Override
    public ProjectSubProject updateImpl(
        com.hp.omp.model.ProjectSubProject projectSubProject, boolean merge)
        throws SystemException {
        projectSubProject = toUnwrappedModel(projectSubProject);

        boolean isNew = projectSubProject.isNew();

        ProjectSubProjectModelImpl projectSubProjectModelImpl = (ProjectSubProjectModelImpl) projectSubProject;

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, projectSubProject, merge);

            projectSubProject.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !ProjectSubProjectModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((projectSubProjectModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        Long.valueOf(projectSubProjectModelImpl.getOriginalProjectId())
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PROJECTID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID,
                    args);

                args = new Object[] {
                        Long.valueOf(projectSubProjectModelImpl.getProjectId())
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PROJECTID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID,
                    args);
            }

            if ((projectSubProjectModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBPROJECTID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        Long.valueOf(projectSubProjectModelImpl.getOriginalSubProjectId())
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SUBPROJECTID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBPROJECTID,
                    args);

                args = new Object[] {
                        Long.valueOf(projectSubProjectModelImpl.getSubProjectId())
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SUBPROJECTID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBPROJECTID,
                    args);
            }

            if ((projectSubProjectModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTIDANDSUBPROJECTID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        Long.valueOf(projectSubProjectModelImpl.getOriginalProjectId()),
                        Long.valueOf(projectSubProjectModelImpl.getOriginalSubProjectId())
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PROJECTIDANDSUBPROJECTID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTIDANDSUBPROJECTID,
                    args);

                args = new Object[] {
                        Long.valueOf(projectSubProjectModelImpl.getProjectId()),
                        Long.valueOf(projectSubProjectModelImpl.getSubProjectId())
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PROJECTIDANDSUBPROJECTID,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTIDANDSUBPROJECTID,
                    args);
            }
        }

        EntityCacheUtil.putResult(ProjectSubProjectModelImpl.ENTITY_CACHE_ENABLED,
            ProjectSubProjectImpl.class, projectSubProject.getPrimaryKey(),
            projectSubProject);

        return projectSubProject;
    }

    protected ProjectSubProject toUnwrappedModel(
        ProjectSubProject projectSubProject) {
        if (projectSubProject instanceof ProjectSubProjectImpl) {
            return projectSubProject;
        }

        ProjectSubProjectImpl projectSubProjectImpl = new ProjectSubProjectImpl();

        projectSubProjectImpl.setNew(projectSubProject.isNew());
        projectSubProjectImpl.setPrimaryKey(projectSubProject.getPrimaryKey());

        projectSubProjectImpl.setProjectSubProjectId(projectSubProject.getProjectSubProjectId());
        projectSubProjectImpl.setProjectId(projectSubProject.getProjectId());
        projectSubProjectImpl.setSubProjectId(projectSubProject.getSubProjectId());

        return projectSubProjectImpl;
    }

    /**
     * Returns the project sub project with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the project sub project
     * @return the project sub project
     * @throws com.liferay.portal.NoSuchModelException if a project sub project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ProjectSubProject findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the project sub project with the primary key or throws a {@link com.hp.omp.NoSuchProjectSubProjectException} if it could not be found.
     *
     * @param projectSubProjectId the primary key of the project sub project
     * @return the project sub project
     * @throws com.hp.omp.NoSuchProjectSubProjectException if a project sub project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public ProjectSubProject findByPrimaryKey(long projectSubProjectId)
        throws NoSuchProjectSubProjectException, SystemException {
        ProjectSubProject projectSubProject = fetchByPrimaryKey(projectSubProjectId);

        if (projectSubProject == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    projectSubProjectId);
            }

            throw new NoSuchProjectSubProjectException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                projectSubProjectId);
        }

        return projectSubProject;
    }

    /**
     * Returns the project sub project with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the project sub project
     * @return the project sub project, or <code>null</code> if a project sub project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public ProjectSubProject fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the project sub project with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param projectSubProjectId the primary key of the project sub project
     * @return the project sub project, or <code>null</code> if a project sub project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public ProjectSubProject fetchByPrimaryKey(long projectSubProjectId)
        throws SystemException {
        ProjectSubProject projectSubProject = (ProjectSubProject) EntityCacheUtil.getResult(ProjectSubProjectModelImpl.ENTITY_CACHE_ENABLED,
                ProjectSubProjectImpl.class, projectSubProjectId);

        if (projectSubProject == _nullProjectSubProject) {
            return null;
        }

        if (projectSubProject == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                projectSubProject = (ProjectSubProject) session.get(ProjectSubProjectImpl.class,
                        Long.valueOf(projectSubProjectId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (projectSubProject != null) {
                    cacheResult(projectSubProject);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(ProjectSubProjectModelImpl.ENTITY_CACHE_ENABLED,
                        ProjectSubProjectImpl.class, projectSubProjectId,
                        _nullProjectSubProject);
                }

                closeSession(session);
            }
        }

        return projectSubProject;
    }

    /**
     * Returns all the project sub projects where projectId = &#63;.
     *
     * @param projectId the project ID
     * @return the matching project sub projects
     * @throws SystemException if a system exception occurred
     */
    public List<ProjectSubProject> findByProjectId(long projectId)
        throws SystemException {
        return findByProjectId(projectId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the project sub projects where projectId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param projectId the project ID
     * @param start the lower bound of the range of project sub projects
     * @param end the upper bound of the range of project sub projects (not inclusive)
     * @return the range of matching project sub projects
     * @throws SystemException if a system exception occurred
     */
    public List<ProjectSubProject> findByProjectId(long projectId, int start,
        int end) throws SystemException {
        return findByProjectId(projectId, start, end, null);
    }

    /**
     * Returns an ordered range of all the project sub projects where projectId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param projectId the project ID
     * @param start the lower bound of the range of project sub projects
     * @param end the upper bound of the range of project sub projects (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching project sub projects
     * @throws SystemException if a system exception occurred
     */
    public List<ProjectSubProject> findByProjectId(long projectId, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTID;
            finderArgs = new Object[] { projectId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTID;
            finderArgs = new Object[] { projectId, start, end, orderByComparator };
        }

        List<ProjectSubProject> list = (List<ProjectSubProject>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (ProjectSubProject projectSubProject : list) {
                if ((projectId != projectSubProject.getProjectId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(2);
            }

            query.append(_SQL_SELECT_PROJECTSUBPROJECT_WHERE);

            query.append(_FINDER_COLUMN_PROJECTID_PROJECTID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(projectId);

                list = (List<ProjectSubProject>) QueryUtil.list(q,
                        getDialect(), start, end);
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first project sub project in the ordered set where projectId = &#63;.
     *
     * @param projectId the project ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching project sub project
     * @throws com.hp.omp.NoSuchProjectSubProjectException if a matching project sub project could not be found
     * @throws SystemException if a system exception occurred
     */
    public ProjectSubProject findByProjectId_First(long projectId,
        OrderByComparator orderByComparator)
        throws NoSuchProjectSubProjectException, SystemException {
        ProjectSubProject projectSubProject = fetchByProjectId_First(projectId,
                orderByComparator);

        if (projectSubProject != null) {
            return projectSubProject;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("projectId=");
        msg.append(projectId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchProjectSubProjectException(msg.toString());
    }

    /**
     * Returns the first project sub project in the ordered set where projectId = &#63;.
     *
     * @param projectId the project ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching project sub project, or <code>null</code> if a matching project sub project could not be found
     * @throws SystemException if a system exception occurred
     */
    public ProjectSubProject fetchByProjectId_First(long projectId,
        OrderByComparator orderByComparator) throws SystemException {
        List<ProjectSubProject> list = findByProjectId(projectId, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last project sub project in the ordered set where projectId = &#63;.
     *
     * @param projectId the project ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching project sub project
     * @throws com.hp.omp.NoSuchProjectSubProjectException if a matching project sub project could not be found
     * @throws SystemException if a system exception occurred
     */
    public ProjectSubProject findByProjectId_Last(long projectId,
        OrderByComparator orderByComparator)
        throws NoSuchProjectSubProjectException, SystemException {
        ProjectSubProject projectSubProject = fetchByProjectId_Last(projectId,
                orderByComparator);

        if (projectSubProject != null) {
            return projectSubProject;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("projectId=");
        msg.append(projectId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchProjectSubProjectException(msg.toString());
    }

    /**
     * Returns the last project sub project in the ordered set where projectId = &#63;.
     *
     * @param projectId the project ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching project sub project, or <code>null</code> if a matching project sub project could not be found
     * @throws SystemException if a system exception occurred
     */
    public ProjectSubProject fetchByProjectId_Last(long projectId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByProjectId(projectId);

        List<ProjectSubProject> list = findByProjectId(projectId, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the project sub projects before and after the current project sub project in the ordered set where projectId = &#63;.
     *
     * @param projectSubProjectId the primary key of the current project sub project
     * @param projectId the project ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next project sub project
     * @throws com.hp.omp.NoSuchProjectSubProjectException if a project sub project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public ProjectSubProject[] findByProjectId_PrevAndNext(
        long projectSubProjectId, long projectId,
        OrderByComparator orderByComparator)
        throws NoSuchProjectSubProjectException, SystemException {
        ProjectSubProject projectSubProject = findByPrimaryKey(projectSubProjectId);

        Session session = null;

        try {
            session = openSession();

            ProjectSubProject[] array = new ProjectSubProjectImpl[3];

            array[0] = getByProjectId_PrevAndNext(session, projectSubProject,
                    projectId, orderByComparator, true);

            array[1] = projectSubProject;

            array[2] = getByProjectId_PrevAndNext(session, projectSubProject,
                    projectId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected ProjectSubProject getByProjectId_PrevAndNext(Session session,
        ProjectSubProject projectSubProject, long projectId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PROJECTSUBPROJECT_WHERE);

        query.append(_FINDER_COLUMN_PROJECTID_PROJECTID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(projectId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(projectSubProject);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<ProjectSubProject> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Returns all the project sub projects where subProjectId = &#63;.
     *
     * @param subProjectId the sub project ID
     * @return the matching project sub projects
     * @throws SystemException if a system exception occurred
     */
    public List<ProjectSubProject> findBySubProjectId(long subProjectId)
        throws SystemException {
        return findBySubProjectId(subProjectId, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the project sub projects where subProjectId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param subProjectId the sub project ID
     * @param start the lower bound of the range of project sub projects
     * @param end the upper bound of the range of project sub projects (not inclusive)
     * @return the range of matching project sub projects
     * @throws SystemException if a system exception occurred
     */
    public List<ProjectSubProject> findBySubProjectId(long subProjectId,
        int start, int end) throws SystemException {
        return findBySubProjectId(subProjectId, start, end, null);
    }

    /**
     * Returns an ordered range of all the project sub projects where subProjectId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param subProjectId the sub project ID
     * @param start the lower bound of the range of project sub projects
     * @param end the upper bound of the range of project sub projects (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching project sub projects
     * @throws SystemException if a system exception occurred
     */
    public List<ProjectSubProject> findBySubProjectId(long subProjectId,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SUBPROJECTID;
            finderArgs = new Object[] { subProjectId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SUBPROJECTID;
            finderArgs = new Object[] {
                    subProjectId,
                    
                    start, end, orderByComparator
                };
        }

        List<ProjectSubProject> list = (List<ProjectSubProject>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (ProjectSubProject projectSubProject : list) {
                if ((subProjectId != projectSubProject.getSubProjectId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(2);
            }

            query.append(_SQL_SELECT_PROJECTSUBPROJECT_WHERE);

            query.append(_FINDER_COLUMN_SUBPROJECTID_SUBPROJECTID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(subProjectId);

                list = (List<ProjectSubProject>) QueryUtil.list(q,
                        getDialect(), start, end);
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first project sub project in the ordered set where subProjectId = &#63;.
     *
     * @param subProjectId the sub project ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching project sub project
     * @throws com.hp.omp.NoSuchProjectSubProjectException if a matching project sub project could not be found
     * @throws SystemException if a system exception occurred
     */
    public ProjectSubProject findBySubProjectId_First(long subProjectId,
        OrderByComparator orderByComparator)
        throws NoSuchProjectSubProjectException, SystemException {
        ProjectSubProject projectSubProject = fetchBySubProjectId_First(subProjectId,
                orderByComparator);

        if (projectSubProject != null) {
            return projectSubProject;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("subProjectId=");
        msg.append(subProjectId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchProjectSubProjectException(msg.toString());
    }

    /**
     * Returns the first project sub project in the ordered set where subProjectId = &#63;.
     *
     * @param subProjectId the sub project ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching project sub project, or <code>null</code> if a matching project sub project could not be found
     * @throws SystemException if a system exception occurred
     */
    public ProjectSubProject fetchBySubProjectId_First(long subProjectId,
        OrderByComparator orderByComparator) throws SystemException {
        List<ProjectSubProject> list = findBySubProjectId(subProjectId, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last project sub project in the ordered set where subProjectId = &#63;.
     *
     * @param subProjectId the sub project ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching project sub project
     * @throws com.hp.omp.NoSuchProjectSubProjectException if a matching project sub project could not be found
     * @throws SystemException if a system exception occurred
     */
    public ProjectSubProject findBySubProjectId_Last(long subProjectId,
        OrderByComparator orderByComparator)
        throws NoSuchProjectSubProjectException, SystemException {
        ProjectSubProject projectSubProject = fetchBySubProjectId_Last(subProjectId,
                orderByComparator);

        if (projectSubProject != null) {
            return projectSubProject;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("subProjectId=");
        msg.append(subProjectId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchProjectSubProjectException(msg.toString());
    }

    /**
     * Returns the last project sub project in the ordered set where subProjectId = &#63;.
     *
     * @param subProjectId the sub project ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching project sub project, or <code>null</code> if a matching project sub project could not be found
     * @throws SystemException if a system exception occurred
     */
    public ProjectSubProject fetchBySubProjectId_Last(long subProjectId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countBySubProjectId(subProjectId);

        List<ProjectSubProject> list = findBySubProjectId(subProjectId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the project sub projects before and after the current project sub project in the ordered set where subProjectId = &#63;.
     *
     * @param projectSubProjectId the primary key of the current project sub project
     * @param subProjectId the sub project ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next project sub project
     * @throws com.hp.omp.NoSuchProjectSubProjectException if a project sub project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public ProjectSubProject[] findBySubProjectId_PrevAndNext(
        long projectSubProjectId, long subProjectId,
        OrderByComparator orderByComparator)
        throws NoSuchProjectSubProjectException, SystemException {
        ProjectSubProject projectSubProject = findByPrimaryKey(projectSubProjectId);

        Session session = null;

        try {
            session = openSession();

            ProjectSubProject[] array = new ProjectSubProjectImpl[3];

            array[0] = getBySubProjectId_PrevAndNext(session,
                    projectSubProject, subProjectId, orderByComparator, true);

            array[1] = projectSubProject;

            array[2] = getBySubProjectId_PrevAndNext(session,
                    projectSubProject, subProjectId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected ProjectSubProject getBySubProjectId_PrevAndNext(Session session,
        ProjectSubProject projectSubProject, long subProjectId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PROJECTSUBPROJECT_WHERE);

        query.append(_FINDER_COLUMN_SUBPROJECTID_SUBPROJECTID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(subProjectId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(projectSubProject);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<ProjectSubProject> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Returns all the project sub projects where projectId = &#63; and subProjectId = &#63;.
     *
     * @param projectId the project ID
     * @param subProjectId the sub project ID
     * @return the matching project sub projects
     * @throws SystemException if a system exception occurred
     */
    public List<ProjectSubProject> findByProjectIdAndSubProjectId(
        long projectId, long subProjectId) throws SystemException {
        return findByProjectIdAndSubProjectId(projectId, subProjectId,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the project sub projects where projectId = &#63; and subProjectId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param projectId the project ID
     * @param subProjectId the sub project ID
     * @param start the lower bound of the range of project sub projects
     * @param end the upper bound of the range of project sub projects (not inclusive)
     * @return the range of matching project sub projects
     * @throws SystemException if a system exception occurred
     */
    public List<ProjectSubProject> findByProjectIdAndSubProjectId(
        long projectId, long subProjectId, int start, int end)
        throws SystemException {
        return findByProjectIdAndSubProjectId(projectId, subProjectId, start,
            end, null);
    }

    /**
     * Returns an ordered range of all the project sub projects where projectId = &#63; and subProjectId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param projectId the project ID
     * @param subProjectId the sub project ID
     * @param start the lower bound of the range of project sub projects
     * @param end the upper bound of the range of project sub projects (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching project sub projects
     * @throws SystemException if a system exception occurred
     */
    public List<ProjectSubProject> findByProjectIdAndSubProjectId(
        long projectId, long subProjectId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PROJECTIDANDSUBPROJECTID;
            finderArgs = new Object[] { projectId, subProjectId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PROJECTIDANDSUBPROJECTID;
            finderArgs = new Object[] {
                    projectId, subProjectId,
                    
                    start, end, orderByComparator
                };
        }

        List<ProjectSubProject> list = (List<ProjectSubProject>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (ProjectSubProject projectSubProject : list) {
                if ((projectId != projectSubProject.getProjectId()) ||
                        (subProjectId != projectSubProject.getSubProjectId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_PROJECTSUBPROJECT_WHERE);

            query.append(_FINDER_COLUMN_PROJECTIDANDSUBPROJECTID_PROJECTID_2);

            query.append(_FINDER_COLUMN_PROJECTIDANDSUBPROJECTID_SUBPROJECTID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(projectId);

                qPos.add(subProjectId);

                list = (List<ProjectSubProject>) QueryUtil.list(q,
                        getDialect(), start, end);
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first project sub project in the ordered set where projectId = &#63; and subProjectId = &#63;.
     *
     * @param projectId the project ID
     * @param subProjectId the sub project ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching project sub project
     * @throws com.hp.omp.NoSuchProjectSubProjectException if a matching project sub project could not be found
     * @throws SystemException if a system exception occurred
     */
    public ProjectSubProject findByProjectIdAndSubProjectId_First(
        long projectId, long subProjectId, OrderByComparator orderByComparator)
        throws NoSuchProjectSubProjectException, SystemException {
        ProjectSubProject projectSubProject = fetchByProjectIdAndSubProjectId_First(projectId,
                subProjectId, orderByComparator);

        if (projectSubProject != null) {
            return projectSubProject;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("projectId=");
        msg.append(projectId);

        msg.append(", subProjectId=");
        msg.append(subProjectId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchProjectSubProjectException(msg.toString());
    }

    /**
     * Returns the first project sub project in the ordered set where projectId = &#63; and subProjectId = &#63;.
     *
     * @param projectId the project ID
     * @param subProjectId the sub project ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching project sub project, or <code>null</code> if a matching project sub project could not be found
     * @throws SystemException if a system exception occurred
     */
    public ProjectSubProject fetchByProjectIdAndSubProjectId_First(
        long projectId, long subProjectId, OrderByComparator orderByComparator)
        throws SystemException {
        List<ProjectSubProject> list = findByProjectIdAndSubProjectId(projectId,
                subProjectId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last project sub project in the ordered set where projectId = &#63; and subProjectId = &#63;.
     *
     * @param projectId the project ID
     * @param subProjectId the sub project ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching project sub project
     * @throws com.hp.omp.NoSuchProjectSubProjectException if a matching project sub project could not be found
     * @throws SystemException if a system exception occurred
     */
    public ProjectSubProject findByProjectIdAndSubProjectId_Last(
        long projectId, long subProjectId, OrderByComparator orderByComparator)
        throws NoSuchProjectSubProjectException, SystemException {
        ProjectSubProject projectSubProject = fetchByProjectIdAndSubProjectId_Last(projectId,
                subProjectId, orderByComparator);

        if (projectSubProject != null) {
            return projectSubProject;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("projectId=");
        msg.append(projectId);

        msg.append(", subProjectId=");
        msg.append(subProjectId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchProjectSubProjectException(msg.toString());
    }

    /**
     * Returns the last project sub project in the ordered set where projectId = &#63; and subProjectId = &#63;.
     *
     * @param projectId the project ID
     * @param subProjectId the sub project ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching project sub project, or <code>null</code> if a matching project sub project could not be found
     * @throws SystemException if a system exception occurred
     */
    public ProjectSubProject fetchByProjectIdAndSubProjectId_Last(
        long projectId, long subProjectId, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByProjectIdAndSubProjectId(projectId, subProjectId);

        List<ProjectSubProject> list = findByProjectIdAndSubProjectId(projectId,
                subProjectId, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the project sub projects before and after the current project sub project in the ordered set where projectId = &#63; and subProjectId = &#63;.
     *
     * @param projectSubProjectId the primary key of the current project sub project
     * @param projectId the project ID
     * @param subProjectId the sub project ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next project sub project
     * @throws com.hp.omp.NoSuchProjectSubProjectException if a project sub project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public ProjectSubProject[] findByProjectIdAndSubProjectId_PrevAndNext(
        long projectSubProjectId, long projectId, long subProjectId,
        OrderByComparator orderByComparator)
        throws NoSuchProjectSubProjectException, SystemException {
        ProjectSubProject projectSubProject = findByPrimaryKey(projectSubProjectId);

        Session session = null;

        try {
            session = openSession();

            ProjectSubProject[] array = new ProjectSubProjectImpl[3];

            array[0] = getByProjectIdAndSubProjectId_PrevAndNext(session,
                    projectSubProject, projectId, subProjectId,
                    orderByComparator, true);

            array[1] = projectSubProject;

            array[2] = getByProjectIdAndSubProjectId_PrevAndNext(session,
                    projectSubProject, projectId, subProjectId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected ProjectSubProject getByProjectIdAndSubProjectId_PrevAndNext(
        Session session, ProjectSubProject projectSubProject, long projectId,
        long subProjectId, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PROJECTSUBPROJECT_WHERE);

        query.append(_FINDER_COLUMN_PROJECTIDANDSUBPROJECTID_PROJECTID_2);

        query.append(_FINDER_COLUMN_PROJECTIDANDSUBPROJECTID_SUBPROJECTID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(projectId);

        qPos.add(subProjectId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(projectSubProject);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<ProjectSubProject> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Returns all the project sub projects.
     *
     * @return the project sub projects
     * @throws SystemException if a system exception occurred
     */
    public List<ProjectSubProject> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the project sub projects.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of project sub projects
     * @param end the upper bound of the range of project sub projects (not inclusive)
     * @return the range of project sub projects
     * @throws SystemException if a system exception occurred
     */
    public List<ProjectSubProject> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the project sub projects.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of project sub projects
     * @param end the upper bound of the range of project sub projects (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of project sub projects
     * @throws SystemException if a system exception occurred
     */
    public List<ProjectSubProject> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<ProjectSubProject> list = (List<ProjectSubProject>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_PROJECTSUBPROJECT);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_PROJECTSUBPROJECT;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<ProjectSubProject>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<ProjectSubProject>) QueryUtil.list(q,
                            getDialect(), start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the project sub projects where projectId = &#63; from the database.
     *
     * @param projectId the project ID
     * @throws SystemException if a system exception occurred
     */
    public void removeByProjectId(long projectId) throws SystemException {
        for (ProjectSubProject projectSubProject : findByProjectId(projectId)) {
            remove(projectSubProject);
        }
    }

    /**
     * Removes all the project sub projects where subProjectId = &#63; from the database.
     *
     * @param subProjectId the sub project ID
     * @throws SystemException if a system exception occurred
     */
    public void removeBySubProjectId(long subProjectId)
        throws SystemException {
        for (ProjectSubProject projectSubProject : findBySubProjectId(
                subProjectId)) {
            remove(projectSubProject);
        }
    }

    /**
     * Removes all the project sub projects where projectId = &#63; and subProjectId = &#63; from the database.
     *
     * @param projectId the project ID
     * @param subProjectId the sub project ID
     * @throws SystemException if a system exception occurred
     */
    public void removeByProjectIdAndSubProjectId(long projectId,
        long subProjectId) throws SystemException {
        for (ProjectSubProject projectSubProject : findByProjectIdAndSubProjectId(
                projectId, subProjectId)) {
            remove(projectSubProject);
        }
    }

    /**
     * Removes all the project sub projects from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (ProjectSubProject projectSubProject : findAll()) {
            remove(projectSubProject);
        }
    }

    /**
     * Returns the number of project sub projects where projectId = &#63;.
     *
     * @param projectId the project ID
     * @return the number of matching project sub projects
     * @throws SystemException if a system exception occurred
     */
    public int countByProjectId(long projectId) throws SystemException {
        Object[] finderArgs = new Object[] { projectId };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_PROJECTID,
                finderArgs, this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_PROJECTSUBPROJECT_WHERE);

            query.append(_FINDER_COLUMN_PROJECTID_PROJECTID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(projectId);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PROJECTID,
                    finderArgs, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the number of project sub projects where subProjectId = &#63;.
     *
     * @param subProjectId the sub project ID
     * @return the number of matching project sub projects
     * @throws SystemException if a system exception occurred
     */
    public int countBySubProjectId(long subProjectId) throws SystemException {
        Object[] finderArgs = new Object[] { subProjectId };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_SUBPROJECTID,
                finderArgs, this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_PROJECTSUBPROJECT_WHERE);

            query.append(_FINDER_COLUMN_SUBPROJECTID_SUBPROJECTID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(subProjectId);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_SUBPROJECTID,
                    finderArgs, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the number of project sub projects where projectId = &#63; and subProjectId = &#63;.
     *
     * @param projectId the project ID
     * @param subProjectId the sub project ID
     * @return the number of matching project sub projects
     * @throws SystemException if a system exception occurred
     */
    public int countByProjectIdAndSubProjectId(long projectId, long subProjectId)
        throws SystemException {
        Object[] finderArgs = new Object[] { projectId, subProjectId };

        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_BY_PROJECTIDANDSUBPROJECTID,
                finderArgs, this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_PROJECTSUBPROJECT_WHERE);

            query.append(_FINDER_COLUMN_PROJECTIDANDSUBPROJECTID_PROJECTID_2);

            query.append(_FINDER_COLUMN_PROJECTIDANDSUBPROJECTID_SUBPROJECTID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(projectId);

                qPos.add(subProjectId);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PROJECTIDANDSUBPROJECTID,
                    finderArgs, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the number of project sub projects.
     *
     * @return the number of project sub projects
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_PROJECTSUBPROJECT);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the project sub project persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.ProjectSubProject")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<ProjectSubProject>> listenersList = new ArrayList<ModelListener<ProjectSubProject>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<ProjectSubProject>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(ProjectSubProjectImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
