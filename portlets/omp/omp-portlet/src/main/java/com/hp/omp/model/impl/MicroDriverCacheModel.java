package com.hp.omp.model.impl;

import com.hp.omp.model.MicroDriver;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

/**
 * The cache model class for representing MicroDriver in entity cache.
 *
 * @author HP Egypt team
 * @see MicroDriver
 * @generated
 */
public class MicroDriverCacheModel implements CacheModel<MicroDriver>,
    Serializable {
    public long microDriverId;
    public String microDriverName;
    public boolean display;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{microDriverId=");
        sb.append(microDriverId);
        sb.append(", microDriverName=");
        sb.append(microDriverName);
        sb.append(", display=");
        sb.append(display);
        sb.append("}");

        return sb.toString();
    }

    public MicroDriver toEntityModel() {
        MicroDriverImpl microDriverImpl = new MicroDriverImpl();

        microDriverImpl.setMicroDriverId(microDriverId);

        if (microDriverName == null) {
            microDriverImpl.setMicroDriverName(StringPool.BLANK);
        } else {
            microDriverImpl.setMicroDriverName(microDriverName);
        }

        microDriverImpl.setDisplay(display);

        microDriverImpl.resetOriginalValues();

        return microDriverImpl;
    }
}
