package com.hp.omp.service.impl;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import com.hp.omp.model.ExcelReport;
import com.hp.omp.model.ExcelStyler;
import com.hp.omp.model.ExcelTable;
import com.hp.omp.model.GoodReceipt;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.TrackingCodes;
import com.hp.omp.model.custom.CatalogoListDTO;
import com.hp.omp.model.custom.ExcelTableRow;
import com.hp.omp.model.custom.OMPAggregateReportExcelRow;
import com.hp.omp.model.impl.ExcelDefaultStyler;
import com.hp.omp.model.impl.ExcelReportImpl;
import com.hp.omp.model.impl.ExcelTableImpl;
import com.hp.omp.service.ProjectLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.hp.omp.service.TrackingCodesLocalServiceUtil;
import com.hp.omp.service.WbsCodeLocalServiceUtil;
import com.hp.omp.service.base.TrackingCodesLocalServiceBaseImpl;
import com.hp.omp.service.persistence.ProjectFinderUtil;
import com.hp.omp.service.persistence.TrackingCodesFinderImpl;
import com.hp.omp.service.persistence.TrackingCodesFinderUtil;
import com.hp.omp.service.persistence.TrackingCodesUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;

import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The implementation of the tracking codes local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.TrackingCodesLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see com.hp.omp.service.base.TrackingCodesLocalServiceBaseImpl
 * @see com.hp.omp.service.TrackingCodesLocalServiceUtil
 */
public class TrackingCodesLocalServiceImpl
    extends TrackingCodesLocalServiceBaseImpl {
	
	Logger logger = LoggerFactory.getLogger(TrackingCodesLocalServiceImpl.class);
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.hp.omp.service.TrackingCodesLocalServiceUtil} to access the tracking codes local service.
     */
	public TrackingCodes getTrackingCodesByFields(TrackingCodes trackingCode) throws SystemException {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(TrackingCodes.class);
		dynamicQuery.add(PropertyFactoryUtil.forName("budgetCategoryId").eq(trackingCode.getBudgetCategoryId()));
		dynamicQuery.add(PropertyFactoryUtil.forName("budgetSubCategoryId").eq(trackingCode.getBudgetSubCategoryId()));
		dynamicQuery.add(PropertyFactoryUtil.forName("macroDriverId").eq(trackingCode.getMacroDriverId()));
		dynamicQuery.add(PropertyFactoryUtil.forName("microDriverId").eq(trackingCode.getMicroDriverId()));
		dynamicQuery.add(PropertyFactoryUtil.forName("odnpNameId").eq(trackingCode.getOdnpNameId()));
		dynamicQuery.add(PropertyFactoryUtil.forName("projectId").eq(trackingCode.getProjectId()));
		dynamicQuery.add(PropertyFactoryUtil.forName("subProjectId").eq(trackingCode.getSubProjectId()));
		dynamicQuery.add(PropertyFactoryUtil.forName("wbsCodeId").eq(trackingCode.getWbsCodeId()));
		@SuppressWarnings("unchecked")
		List<TrackingCodes> resultQuery =  TrackingCodesLocalServiceUtil.dynamicQuery(dynamicQuery);
		return (resultQuery == null || resultQuery.size() == 0) ?  null  : resultQuery.get(0);
	}
	
	public TrackingCodes getTrackingCodesByPRFields(PurchaseRequest pr) throws SystemException {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(TrackingCodes.class);
		dynamicQuery.add(PropertyFactoryUtil.forName("budgetCategoryId").eq(pr.getBudgetCategoryId()));
		dynamicQuery.add(PropertyFactoryUtil.forName("budgetSubCategoryId").eq(pr.getBudgetSubCategoryId()));
		dynamicQuery.add(PropertyFactoryUtil.forName("macroDriverId").eq(pr.getMacroDriverId()));
		dynamicQuery.add(PropertyFactoryUtil.forName("microDriverId").eq(pr.getMicroDriverId()));
		dynamicQuery.add(PropertyFactoryUtil.forName("odnpNameId").eq(pr.getOdnpNameId()));
		dynamicQuery.add(PropertyFactoryUtil.forName("projectId").eq(pr.getProjectId()));
		dynamicQuery.add(PropertyFactoryUtil.forName("subProjectId").eq(pr.getSubProjectId()));
		dynamicQuery.add(PropertyFactoryUtil.forName("wbsCodeId").eq(pr.getWbsCodeId()));
		@SuppressWarnings("unchecked")
		List<TrackingCodes> resultQuery =  TrackingCodesLocalServiceUtil.dynamicQuery(dynamicQuery);
		return (resultQuery == null || resultQuery.size() == 0) ?  null  : resultQuery.get(0);
	}
	
	
	public TrackingCodes getTrackingCodeByName(String name) throws SystemException {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(TrackingCodes.class);
		dynamicQuery.add(RestrictionsFactoryUtil.ilike("trackingCodeName", name));
		@SuppressWarnings("unchecked")
		List<TrackingCodes> resultQuery =  TrackingCodesLocalServiceUtil.dynamicQuery(dynamicQuery);
		return (resultQuery == null || resultQuery.size() == 0) ?  null  : resultQuery.get(0);
	}
	
	public List<TrackingCodes> getTrackingCodesList(Integer pageNumber, Integer pageSize, String searchTCN) throws SystemException {				
		Integer start = (pageNumber - 1) * pageSize;
		Integer end = start + pageSize;		
		List<TrackingCodes> resultQuery = null;
		resultQuery = TrackingCodesFinderUtil.getTrackingCodesOrderedAsc(start, end, searchTCN);
		return resultQuery; 
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getTrackingCodesCountList(String searchTCN) throws SystemException {
		int countList = 0;
		countList = TrackingCodesFinderUtil.getTrackingCodesCountList(searchTCN);
		return countList;
	}

	public void exportTrackingCodesAsExcel(
			List<TrackingCodes> listTrackingCodes, List<Object> header,
			OutputStream out) throws SystemException {
		ExcelReport excelReport = new ExcelReportImpl();
		ExcelStyler styler = new ExcelDefaultStyler(excelReport.getWorkBook());
		excelReport.setStyler(styler);

		List<ExcelTableRow> trackingCodeList = new ArrayList<ExcelTableRow>();
		for(TrackingCodes trackingCode : listTrackingCodes){
			OMPAggregateReportExcelRow tableRow = new OMPAggregateReportExcelRow();
			tableRow.setFieldAtIndex(trackingCode.getTrackingCodeName(), 0);
			tableRow.setFieldAtIndex(trackingCode.getTrackingCodeDescription(), 1);
			try {
				tableRow.setFieldAtIndex(ProjectLocalServiceUtil.getProject(Long.valueOf(trackingCode.getProjectId())).getProjectName(), 2);
				tableRow.setFieldAtIndex(WbsCodeLocalServiceUtil.getWbsCode(Long.valueOf(trackingCode.getWbsCodeId())).getWbsCodeName(), 3);
			} catch (NumberFormatException e) {
				logger.error("Error in exportTrackingCodesAsExcel", e);
			} catch (PortalException e) {
				logger.error("Error in exportTrackingCodesAsExcel", e);
			}
			tableRow.setFieldAtIndex(trackingCode.getDisplay(), 4);
			
			trackingCodeList.add(tableRow);
		}

		ExcelTable euroTable = new ExcelTableImpl(header, trackingCodeList, "Tracking Codes");
		excelReport.addExcelTable(euroTable, 0);

		Workbook exelWorkBook = excelReport.createExelWorkBook();
		try {
			exelWorkBook.write(out);
		} catch (IOException e) {
		} finally{
			try {
				out.flush();
				out.close();
			} catch (IOException e) {
			}
		}
	}
	
}



