package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchPRAuditException;
import com.hp.omp.model.PRAudit;
import com.hp.omp.model.impl.PRAuditImpl;
import com.hp.omp.model.impl.PRAuditModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the p r audit service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see PRAuditPersistence
 * @see PRAuditUtil
 * @generated
 */
public class PRAuditPersistenceImpl extends BasePersistenceImpl<PRAudit>
    implements PRAuditPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link PRAuditUtil} to access the p r audit persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = PRAuditImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PRAuditModelImpl.ENTITY_CACHE_ENABLED,
            PRAuditModelImpl.FINDER_CACHE_ENABLED, PRAuditImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PRAuditModelImpl.ENTITY_CACHE_ENABLED,
            PRAuditModelImpl.FINDER_CACHE_ENABLED, PRAuditImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PRAuditModelImpl.ENTITY_CACHE_ENABLED,
            PRAuditModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_PRAUDIT = "SELECT prAudit FROM PRAudit prAudit";
    private static final String _SQL_COUNT_PRAUDIT = "SELECT COUNT(prAudit) FROM PRAudit prAudit";
    private static final String _ORDER_BY_ENTITY_ALIAS = "prAudit.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No PRAudit exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(PRAuditPersistenceImpl.class);
    private static PRAudit _nullPRAudit = new PRAuditImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<PRAudit> toCacheModel() {
                return _nullPRAuditCacheModel;
            }
        };

    private static CacheModel<PRAudit> _nullPRAuditCacheModel = new CacheModel<PRAudit>() {
            public PRAudit toEntityModel() {
                return _nullPRAudit;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the p r audit in the entity cache if it is enabled.
     *
     * @param prAudit the p r audit
     */
    public void cacheResult(PRAudit prAudit) {
        EntityCacheUtil.putResult(PRAuditModelImpl.ENTITY_CACHE_ENABLED,
            PRAuditImpl.class, prAudit.getPrimaryKey(), prAudit);

        prAudit.resetOriginalValues();
    }

    /**
     * Caches the p r audits in the entity cache if it is enabled.
     *
     * @param prAudits the p r audits
     */
    public void cacheResult(List<PRAudit> prAudits) {
        for (PRAudit prAudit : prAudits) {
            if (EntityCacheUtil.getResult(
                        PRAuditModelImpl.ENTITY_CACHE_ENABLED,
                        PRAuditImpl.class, prAudit.getPrimaryKey()) == null) {
                cacheResult(prAudit);
            } else {
                prAudit.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all p r audits.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(PRAuditImpl.class.getName());
        }

        EntityCacheUtil.clearCache(PRAuditImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the p r audit.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(PRAudit prAudit) {
        EntityCacheUtil.removeResult(PRAuditModelImpl.ENTITY_CACHE_ENABLED,
            PRAuditImpl.class, prAudit.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<PRAudit> prAudits) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (PRAudit prAudit : prAudits) {
            EntityCacheUtil.removeResult(PRAuditModelImpl.ENTITY_CACHE_ENABLED,
                PRAuditImpl.class, prAudit.getPrimaryKey());
        }
    }

    /**
     * Creates a new p r audit with the primary key. Does not add the p r audit to the database.
     *
     * @param auditId the primary key for the new p r audit
     * @return the new p r audit
     */
    public PRAudit create(long auditId) {
        PRAudit prAudit = new PRAuditImpl();

        prAudit.setNew(true);
        prAudit.setPrimaryKey(auditId);

        return prAudit;
    }

    /**
     * Removes the p r audit with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param auditId the primary key of the p r audit
     * @return the p r audit that was removed
     * @throws com.hp.omp.NoSuchPRAuditException if a p r audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public PRAudit remove(long auditId)
        throws NoSuchPRAuditException, SystemException {
        return remove(Long.valueOf(auditId));
    }

    /**
     * Removes the p r audit with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the p r audit
     * @return the p r audit that was removed
     * @throws com.hp.omp.NoSuchPRAuditException if a p r audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PRAudit remove(Serializable primaryKey)
        throws NoSuchPRAuditException, SystemException {
        Session session = null;

        try {
            session = openSession();

            PRAudit prAudit = (PRAudit) session.get(PRAuditImpl.class,
                    primaryKey);

            if (prAudit == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchPRAuditException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(prAudit);
        } catch (NoSuchPRAuditException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected PRAudit removeImpl(PRAudit prAudit) throws SystemException {
        prAudit = toUnwrappedModel(prAudit);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, prAudit);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(prAudit);

        return prAudit;
    }

    @Override
    public PRAudit updateImpl(com.hp.omp.model.PRAudit prAudit, boolean merge)
        throws SystemException {
        prAudit = toUnwrappedModel(prAudit);

        boolean isNew = prAudit.isNew();

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, prAudit, merge);

            prAudit.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(PRAuditModelImpl.ENTITY_CACHE_ENABLED,
            PRAuditImpl.class, prAudit.getPrimaryKey(), prAudit);

        return prAudit;
    }

    protected PRAudit toUnwrappedModel(PRAudit prAudit) {
        if (prAudit instanceof PRAuditImpl) {
            return prAudit;
        }

        PRAuditImpl prAuditImpl = new PRAuditImpl();

        prAuditImpl.setNew(prAudit.isNew());
        prAuditImpl.setPrimaryKey(prAudit.getPrimaryKey());

        prAuditImpl.setAuditId(prAudit.getAuditId());
        prAuditImpl.setPurchaseRequestId(prAudit.getPurchaseRequestId());
        prAuditImpl.setStatus(prAudit.getStatus());
        prAuditImpl.setUserId(prAudit.getUserId());
        prAuditImpl.setChangeDate(prAudit.getChangeDate());

        return prAuditImpl;
    }

    /**
     * Returns the p r audit with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the p r audit
     * @return the p r audit
     * @throws com.liferay.portal.NoSuchModelException if a p r audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PRAudit findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the p r audit with the primary key or throws a {@link com.hp.omp.NoSuchPRAuditException} if it could not be found.
     *
     * @param auditId the primary key of the p r audit
     * @return the p r audit
     * @throws com.hp.omp.NoSuchPRAuditException if a p r audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public PRAudit findByPrimaryKey(long auditId)
        throws NoSuchPRAuditException, SystemException {
        PRAudit prAudit = fetchByPrimaryKey(auditId);

        if (prAudit == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + auditId);
            }

            throw new NoSuchPRAuditException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                auditId);
        }

        return prAudit;
    }

    /**
     * Returns the p r audit with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the p r audit
     * @return the p r audit, or <code>null</code> if a p r audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PRAudit fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the p r audit with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param auditId the primary key of the p r audit
     * @return the p r audit, or <code>null</code> if a p r audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public PRAudit fetchByPrimaryKey(long auditId) throws SystemException {
        PRAudit prAudit = (PRAudit) EntityCacheUtil.getResult(PRAuditModelImpl.ENTITY_CACHE_ENABLED,
                PRAuditImpl.class, auditId);

        if (prAudit == _nullPRAudit) {
            return null;
        }

        if (prAudit == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                prAudit = (PRAudit) session.get(PRAuditImpl.class,
                        Long.valueOf(auditId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (prAudit != null) {
                    cacheResult(prAudit);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(PRAuditModelImpl.ENTITY_CACHE_ENABLED,
                        PRAuditImpl.class, auditId, _nullPRAudit);
                }

                closeSession(session);
            }
        }

        return prAudit;
    }

    /**
     * Returns all the p r audits.
     *
     * @return the p r audits
     * @throws SystemException if a system exception occurred
     */
    public List<PRAudit> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the p r audits.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of p r audits
     * @param end the upper bound of the range of p r audits (not inclusive)
     * @return the range of p r audits
     * @throws SystemException if a system exception occurred
     */
    public List<PRAudit> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the p r audits.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of p r audits
     * @param end the upper bound of the range of p r audits (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of p r audits
     * @throws SystemException if a system exception occurred
     */
    public List<PRAudit> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<PRAudit> list = (List<PRAudit>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_PRAUDIT);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_PRAUDIT;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<PRAudit>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<PRAudit>) QueryUtil.list(q, getDialect(),
                            start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the p r audits from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (PRAudit prAudit : findAll()) {
            remove(prAudit);
        }
    }

    /**
     * Returns the number of p r audits.
     *
     * @return the number of p r audits
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_PRAUDIT);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the p r audit persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.PRAudit")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<PRAudit>> listenersList = new ArrayList<ModelListener<PRAudit>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<PRAudit>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(PRAuditImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
