package com.hp.omp.portlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.model.MacroDriver;
import com.hp.omp.model.MacroMicroDriver;
import com.hp.omp.model.MicroDriver;
import com.hp.omp.model.PurchaseOrder;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.impl.MacroMicroDriverImpl;
import com.hp.omp.model.impl.MicroDriverImpl;
import com.hp.omp.service.MacroDriverLocalServiceUtil;
import com.hp.omp.service.MacroMicroDriverLocalServiceUtil;
import com.hp.omp.service.MicroDriverLocalServiceUtil;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class MicroDriverNewPortlet
 */
public class MicroDriverAdminPortlet extends MVCPortlet {
 

	private transient Logger logger = LoggerFactory.getLogger(MicroDriverAdminPortlet.class);
	private String errorMSG = "";
	private String successMSG = "";
	private String ACTION_PARAM= "action";
	private String ADD_ACTION= "add";
	private String UPDATE_ACTION= "update";
	private String DELETE_ACTION= "delete";
	private String PAGINATION_ACTION= "pagination";
	
	

	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		super.doView(renderRequest, renderResponse);
	}
	
	
	/* (non-Javadoc)
	 * @see javax.portlet.GenericPortlet#render(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		viewAddMicroDrivers(request, response);
		super.render(request, response);
	}
	
	public void viewAddMicroDrivers(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		String action = renderRequest.getParameter(ACTION_PARAM);
		
		if(ADD_ACTION.equals(action)){
			renderRequest.setAttribute(ACTION_PARAM, ADD_ACTION);
			renderRequest.setAttribute("display",true);
		}else if(UPDATE_ACTION.equals(action)){
			String microDriverId = renderRequest.getParameter("microDriverId");
			MicroDriver microDriver = getMicroDriver(microDriverId);
			List<MacroDriver> assignedMacroDrivers = getAssignedMacroDrivers(microDriverId);
			
			logger.debug("render update action for : "+microDriver.getMicroDriverName());
			logger.debug("Display = "+microDriver.getDisplay());
			
			renderRequest.setAttribute(ACTION_PARAM, UPDATE_ACTION);
			renderRequest.setAttribute("assignedMacroDrivers", assignedMacroDrivers);
			renderRequest.setAttribute("microDriverId", microDriver.getMicroDriverId());
			renderRequest.setAttribute("microDriverName", microDriver.getMicroDriverName());
			renderRequest.setAttribute("display", microDriver.getDisplay());
			
		}
		
		renderRequest.setAttribute("macroDriverList", getMacroDriverList());
	}
	
	@Override
    public void serveResource(ResourceRequest resourceRequest,
                  ResourceResponse resourceResponse) throws IOException,
                  PortletException {
		String action=resourceRequest.getParameter(ACTION_PARAM);
		if(DELETE_ACTION.equals(action)){
			deleteMicroDriver(resourceRequest, resourceResponse);
		}else if(PAGINATION_ACTION.equals(action)){
			paginationList(resourceRequest, resourceResponse);
		}
		
		clearMessages();
           super.serveResource(resourceRequest, resourceResponse);
    }

	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#processAction(javax.portlet.ActionRequest, javax.portlet.ActionResponse)
	 */
	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {

		clearMessages();
		String action = actionRequest.getParameter(ACTION_PARAM);
		String[] assignedMacroDrivers = actionRequest.getParameterValues("assignedMacroDrivers");
		String microDriverName = actionRequest.getParameter("microDriverName");
		List<String> assignedMacroDriverIdsList = new ArrayList<String>();
		boolean display=ParamUtil.getBoolean(actionRequest,"display");
		
		String operation = "";
		
		logger.debug("microDriverName="+microDriverName);
		logger.debug("display = "+display);
		
		logger.debug("creating microDriver object ...");
		MicroDriver newMicroDriver = new MicroDriverImpl();
		newMicroDriver.setMicroDriverName(microDriverName);
		newMicroDriver.setDisplay(display);
		logger.debug("macroDriver object created !!");
		try {
			if(assignedMacroDrivers != null){
				assignedMacroDriverIdsList = Arrays.asList(assignedMacroDrivers);
			}
			if(! (assignedMacroDriverIdsList.isEmpty())){
				if(ADD_ACTION.equals(action)){
					operation = "added";
					addMicroDriver(newMicroDriver, assignedMacroDriverIdsList);
					
				}else if(UPDATE_ACTION.equals(action)){
					operation = "updated";
					String microDriverId = actionRequest.getParameter("microDriverId");
					newMicroDriver.setMicroDriverId(Long.valueOf(microDriverId));
					updateMicroDriver(newMicroDriver, assignedMacroDriverIdsList);
					
				}
			}else{
				errorMSG = "MacroDriver not selected";
			}
			
		} catch (SystemException e) {
			errorMSG = "MicroDriver ["+microDriverName+"] not "+operation+".";
			logger.error("error happened while "+operation+" microDriver ["+microDriverName+"] in database.");
			logger.error(e.getMessage(), e);
		}catch (NumberFormatException nfe) {
			errorMSG = "MicroDriver ["+microDriverName+"] not "+operation+".";
			logger.error("error happened while "+operation+" microDriver ["+microDriverName+"] in database.");
			logger.error(nfe.getMessage(), nfe);
		} catch (PortalException e) {
			errorMSG = "MicroDriver ["+microDriverName+"] not "+operation+".";
			logger.error("error happened while "+operation+" microDriver ["+microDriverName+"] in database.");
			logger.error(e.getMessage(), e);
		}
		
		actionRequest.setAttribute("errorMSG", errorMSG);
		actionRequest.setAttribute("successMSG", successMSG);
		super.processAction(actionRequest, actionResponse);
	}
	
	private void addMicroDriver(MicroDriver newMicroDriver, List<String> macroDriverIdsList) throws SystemException{
		logger.debug("adding new microDriver ["+newMicroDriver.getMicroDriverName()+"] in database ...");
		if(MicroDriverLocalServiceUtil.getMicroDriverByName(newMicroDriver.getMicroDriverName()) == null){
			
			MicroDriverLocalServiceUtil.addMicroDriver(newMicroDriver);
			
			MicroDriver microDriver = MicroDriverLocalServiceUtil.getMicroDriverByName(newMicroDriver.getMicroDriverName());
			if (microDriver == null){
				throw new SystemException("microDriver is null");
			}
			
			addMacroMicroDrivers(microDriver,  macroDriverIdsList);
			
			
			successMSG = "new microDriver name ["+newMicroDriver.getMicroDriverName()+"] successfully Added";
			
		}else{
			errorMSG = "microDriver name ["+newMicroDriver.getMicroDriverName()+"] already existing";
		}
		
	}
	
	private void updateMicroDriver(MicroDriver newMicroDriver, List<String> assignedMacroDriverIdsList) throws SystemException, NumberFormatException, PortalException{
		
		if(! (isMicroDriverNameExists(newMicroDriver, newMicroDriver.getMicroDriverName()))){
			
			logger.debug("updating microDriver ["+newMicroDriver.getMicroDriverId()+"] in database ...");
			
			MicroDriverLocalServiceUtil.updateMicroDriver(newMicroDriver);

			deleteMacroMicroDriver(String.valueOf(newMicroDriver.getMicroDriverId()));
			
			addMacroMicroDrivers(newMicroDriver, assignedMacroDriverIdsList);
//			updateMacroMicroDriver(newMicroDriver.getMicroDriverId(), Long.valueOf(macroDriverId), Long.valueOf(oldMacroDriverId));
			
			successMSG = "microDriver Name ["+newMicroDriver.getMicroDriverName()+"] successfully Updated";
		}else{
			errorMSG = "microDriver name ["+newMicroDriver.getMicroDriverName()+"] already existing";
		}
		
	}
	
	private void addMacroMicroDrivers(MicroDriver microDriver, List<String> assignedMacroDriverIdsList) throws SystemException{
		if( microDriver == null ){
			throw new SystemException("MicroDriver is null");
		}
		for (String macroDriverId : assignedMacroDriverIdsList) {
			if(StringUtils.isNotBlank(macroDriverId) ){
				try{
					addMacroMicroDriver(Long.valueOf(macroDriverId), microDriver.getMicroDriverId(), microDriver.getMicroDriverName());
				}catch (NumberFormatException nfe){
					logger.warn("number formate exception while adding MaroMicroDriver for MicroDriverName["+microDriver.getMicroDriverName()+"], the  macroDriverId is not number macroDriverId is ["+macroDriverId+"] ");
				}
				
			}
		}
	}
	
	private void addMacroMicroDriver(Long macroDriverId, Long microDriverId, String microDriverName) throws SystemException{
		try {
			
			MacroMicroDriver macroMicroDriver = new MacroMicroDriverImpl();
			macroMicroDriver.setMacroDriverId(macroDriverId);
			macroMicroDriver.setMicroDriverId(microDriverId);
			
			MacroMicroDriverLocalServiceUtil.addMacroMicroDriver(macroMicroDriver);
			
		} catch (SystemException e) {
			
			logger.error("error happened while adding new macroMicroDriver macroDriverId=["+macroDriverId+"] microDriverName=["+microDriverName+"]", e);
			logger.info("rolling back added microDriver ["+microDriverName+"]");
			try {
				MicroDriverLocalServiceUtil.deleteMicroDriver(microDriverId);
			} catch (PortalException e1) {
				logger.info("rolling back microDriver ["+microDriverName+"], failed");
				throw new SystemException(e1);
			} catch (SystemException e1) {
				logger.info("rolling back microDriver ["+microDriverName+"], failed");
				throw new SystemException(e1);
			}
			
			throw new SystemException(e);
		}
	}
	
	private void updateMacroMicroDriver(Long microDriverId, Long macroDriverId, Long oldMacroDriverId){
		
		try {
			if(macroDriverId != oldMacroDriverId){
				List<MacroMicroDriver> macroMicroDriverList = MacroMicroDriverLocalServiceUtil.getMacroMicroDriverByMacroDriverIdAndMicroDriverId(oldMacroDriverId, microDriverId);
				if(macroMicroDriverList != null && !(macroMicroDriverList.isEmpty())){
					MacroMicroDriver macroMicroDriver = macroMicroDriverList.get(0);
					if(macroMicroDriver != null){
						macroMicroDriver.setMacroDriverId(Long.valueOf(macroDriverId));
						macroMicroDriver.setMicroDriverId(Long.valueOf(microDriverId));
						
						MacroMicroDriverLocalServiceUtil.updateMacroMicroDriver(macroMicroDriver);
					}
				}
			}
		} catch (SystemException e) {
			logger.error("error happened while updating macroDriver microDriver for microDriverId  ["+microDriverId+"], macroDriverId ["+macroDriverId+"] in database.");
			logger.error(e.getMessage(), e);
		}catch (NumberFormatException nfe) {
			logger.error("error happened while updating macroDriver microDriver for microDriverId  ["+microDriverId+"], macroDriverId ["+macroDriverId+"] in database.");
			logger.error(nfe.getMessage(), nfe);
		}
		
	}
	
	private void retrieveMicroDriverListToRequest(PortletRequest renderRequest) throws SystemException{
		try {
			String searchFilter = ParamUtil.getString(renderRequest, "searchFilter","");
			int totalCount = MicroDriverLocalServiceUtil.getMicroDriversCount();

			Integer pageNumber = ParamUtil.getInteger(renderRequest, "pageNumber", 1);
			Integer pageSize = OmpHelper.PAGE_SIZE;
			int start = (pageNumber - 1) * pageSize;
			int end = start + pageSize;

			renderRequest.setAttribute("numberOfPages", getTotalNumberOfPages(totalCount, pageSize));
			renderRequest.setAttribute("pageNumber", pageNumber);


			//List<MicroDriver> microDriverList = MicroDriverLocalServiceUtil.getMicroDrivers(start, end);
			List<MicroDriver> microDriverList = MicroDriverLocalServiceUtil.getMicroDriversList(start, end, searchFilter);

			renderRequest.setAttribute("microDriverList", microDriverList);
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			errorMSG = "can not retrieve microDriver list";
			renderRequest.setAttribute("errorMSG", errorMSG);
		}

	}
	
	public void deleteMicroDriver(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse)
			throws PortletException, IOException {
		clearMessages();
		
		String microDriverId=resourceRequest.getParameter("microDriverId");
		String microDriverName=resourceRequest.getParameter("microDriverName");
		String action=resourceRequest.getParameter("action");
		String jspPage = "/html/microdriveradmin/microdriverlist.jsp";
		
		if(Validator.isBlank(action)){
			errorMSG = "No Action in the request";
		}else if(Validator.isBlank(microDriverId)){
			errorMSG = "No microDriver Id in the request";
		}else{
			
			try {
				List<PurchaseOrder> purchaseOrderList = PurchaseOrderLocalServiceUtil.getPurchaseOrderByMicroDriverId(microDriverId);
				List<PurchaseRequest> purchaseRequestList = PurchaseRequestLocalServiceUtil.getPurchaseRequestByMicroDriverId(microDriverId);
				
				if(purchaseOrderList != null && ! (purchaseOrderList.isEmpty())){
					errorMSG = "Can Not remove MicroDriver assigned to Purchase Order, MicroDriver name ["+microDriverName+"]";
				}else if(purchaseRequestList != null && ! (purchaseRequestList.isEmpty())){
					errorMSG = "Can Not remove MicroDriver assigned to Purchase Request, MicroDriver name ["+microDriverName+"]";
				}else{
					
					deleteMacroMicroDriver(microDriverId);
					
					MicroDriverLocalServiceUtil.deleteMicroDriver(Long.valueOf(microDriverId));
					
					successMSG = "MicroDriver name ["+microDriverName+"] successfully deleted";
				}
				
			} catch (NumberFormatException e) {
				logger.error(e.getMessage());
				errorMSG = "microDriver Id is invalid";
			} catch (PortalException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove microDriver";
			} catch (SystemException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove sub macroDriver [most probably macroDriver is assigned.]";
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove microDriver";
			}
			
		}
		
		//retrieve updated list
		try {
			retrieveMicroDriverListToRequest(resourceRequest);
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			errorMSG = "can not retrieve microDriver list";
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
		
	}
	
	private MicroDriver getMicroDriver(String microDriverId){
		MicroDriver microDriver = null;
		try {
			microDriver = MicroDriverLocalServiceUtil.getMicroDriver(Long.valueOf(microDriverId));
		} catch (NumberFormatException e) {
			logger.error("microDriver Id has wrong format", e);
		} catch (PortalException e) {
			logger.error("unexpected error", e);
		} catch (SystemException e) {
			logger.error("unexpected error", e);
		}
		
		return microDriver;
	}
	
	private List<MacroDriver> getAssignedMacroDrivers(String microDriverId){
		List<MacroDriver> macroDrivers = new ArrayList<MacroDriver>();
		try{
			macroDrivers =  MacroMicroDriverLocalServiceUtil.getMacroDriversByMicroDriverId(Long.valueOf(microDriverId));

		} catch (NumberFormatException e) {
			logger.error("microDriver Id has wrong format", e);
		} catch (SystemException e) {
			logger.error("unexpected error", e);
		} catch (Exception e) {
			logger.error("unexpected error", e);
		}
		
		return macroDrivers;
	}
	private List<MacroDriver> getMacroDriverList() {
		List<MacroDriver> macroDriversList = null;
		try {
			 macroDriversList = MacroDriverLocalServiceUtil.getMacroDrivers(0, MacroDriverLocalServiceUtil.getMacroDriversCount());
		} catch (SystemException e) {
			logger.error("error happened while retrieving macroDriver list ... ",e);
		}
		
		return macroDriversList;
	}
	
	
	private boolean isMicroDriverNameExists(MicroDriver newMicroDriver, String microDriverName) throws SystemException{
		
		MicroDriver oldMicroDriver = MicroDriverLocalServiceUtil.getMicroDriverByName(microDriverName);
		boolean isExists = false;
		
		if(oldMicroDriver != null && 
				(newMicroDriver.getMicroDriverId() != oldMicroDriver.getMicroDriverId()) ){
			isExists = true;
		}
		return isExists;
	}
	
	private void deleteMacroMicroDriver(String microDriverId) throws SystemException, PortalException {
		Long macroMicroDriverId = 0L;
		try {
			List<MacroMicroDriver> macroMicroDriverList = MacroMicroDriverLocalServiceUtil.getMacroMicroDriverByMicroDriverId(Long.valueOf(microDriverId));
			
			if(macroMicroDriverList != null && ! (macroMicroDriverList.isEmpty()) ){
				
				for (MacroMicroDriver macroMicroDriver : macroMicroDriverList) {
					macroMicroDriverId = macroMicroDriver.getMacroMicroDriverId();
					MacroMicroDriverLocalServiceUtil.deleteMacroMicroDriver(macroMicroDriverId);
					
					logger.debug("MacroMicroDriverId ["+macroMicroDriverId+"] deleted successfully .");
				}
				
			}
		} catch (NumberFormatException e) {
			if(macroMicroDriverId == 0){
				logger.error("error happend while deleting macroMicroDriver for microDriverId["+microDriverId+"]", e);
			}else{
				logger.error("error happend while deleting macroMicroDriverId["+macroMicroDriverId+"]", e);
			}
			
			throw new SystemException(e);
		} catch (SystemException e) {
			if(macroMicroDriverId == 0){
				logger.error("error happend while deleting macroMicroDriver for microDriverId["+microDriverId+"]", e);
			}else{
				logger.error("error happend while deleting macroMicroDriverId="+macroMicroDriverId+"]", e);
			}
			throw new SystemException(e);
		} catch (PortalException e) {
			logger.error("error happend while deleting macroMicroDriverId="+macroMicroDriverId+"]", e);
			throw new PortalException(e);
		}
		
	}
	
	private void paginationList(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse) throws PortletException, IOException{
		
		String jspPage = "/html/microdriveradmin/microdriverlist.jsp";
		try {
			retrieveMicroDriverListToRequest(resourceRequest);
		} catch (SystemException e) {
		logger.error("error happened in pagination ... ",e);
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
	}
	
	private Integer getTotalNumberOfPages(Integer totalNubmerOfResult, Integer pageSize) {
        return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
	}
	
	
	private void clearMessages(){
		errorMSG = "";
		successMSG = "";
	}

	public void searchFilter(ActionRequest request, ActionResponse response) throws IOException, PortletException{
		response.setRenderParameter("searchFilter", "%" + ParamUtil.getString(request, "microDriverAdminName") + "%");  
	}
}
