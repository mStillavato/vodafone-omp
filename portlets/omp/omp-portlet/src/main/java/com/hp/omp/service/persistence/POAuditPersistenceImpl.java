package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchPOAuditException;
import com.hp.omp.model.POAudit;
import com.hp.omp.model.impl.POAuditImpl;
import com.hp.omp.model.impl.POAuditModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the p o audit service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see POAuditPersistence
 * @see POAuditUtil
 * @generated
 */
public class POAuditPersistenceImpl extends BasePersistenceImpl<POAudit>
    implements POAuditPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link POAuditUtil} to access the p o audit persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = POAuditImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(POAuditModelImpl.ENTITY_CACHE_ENABLED,
            POAuditModelImpl.FINDER_CACHE_ENABLED, POAuditImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(POAuditModelImpl.ENTITY_CACHE_ENABLED,
            POAuditModelImpl.FINDER_CACHE_ENABLED, POAuditImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(POAuditModelImpl.ENTITY_CACHE_ENABLED,
            POAuditModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_POAUDIT = "SELECT poAudit FROM POAudit poAudit";
    private static final String _SQL_COUNT_POAUDIT = "SELECT COUNT(poAudit) FROM POAudit poAudit";
    private static final String _ORDER_BY_ENTITY_ALIAS = "poAudit.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No POAudit exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(POAuditPersistenceImpl.class);
    private static POAudit _nullPOAudit = new POAuditImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<POAudit> toCacheModel() {
                return _nullPOAuditCacheModel;
            }
        };

    private static CacheModel<POAudit> _nullPOAuditCacheModel = new CacheModel<POAudit>() {
            public POAudit toEntityModel() {
                return _nullPOAudit;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the p o audit in the entity cache if it is enabled.
     *
     * @param poAudit the p o audit
     */
    public void cacheResult(POAudit poAudit) {
        EntityCacheUtil.putResult(POAuditModelImpl.ENTITY_CACHE_ENABLED,
            POAuditImpl.class, poAudit.getPrimaryKey(), poAudit);

        poAudit.resetOriginalValues();
    }

    /**
     * Caches the p o audits in the entity cache if it is enabled.
     *
     * @param poAudits the p o audits
     */
    public void cacheResult(List<POAudit> poAudits) {
        for (POAudit poAudit : poAudits) {
            if (EntityCacheUtil.getResult(
                        POAuditModelImpl.ENTITY_CACHE_ENABLED,
                        POAuditImpl.class, poAudit.getPrimaryKey()) == null) {
                cacheResult(poAudit);
            } else {
                poAudit.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all p o audits.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(POAuditImpl.class.getName());
        }

        EntityCacheUtil.clearCache(POAuditImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the p o audit.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(POAudit poAudit) {
        EntityCacheUtil.removeResult(POAuditModelImpl.ENTITY_CACHE_ENABLED,
            POAuditImpl.class, poAudit.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<POAudit> poAudits) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (POAudit poAudit : poAudits) {
            EntityCacheUtil.removeResult(POAuditModelImpl.ENTITY_CACHE_ENABLED,
                POAuditImpl.class, poAudit.getPrimaryKey());
        }
    }

    /**
     * Creates a new p o audit with the primary key. Does not add the p o audit to the database.
     *
     * @param auditId the primary key for the new p o audit
     * @return the new p o audit
     */
    public POAudit create(long auditId) {
        POAudit poAudit = new POAuditImpl();

        poAudit.setNew(true);
        poAudit.setPrimaryKey(auditId);

        return poAudit;
    }

    /**
     * Removes the p o audit with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param auditId the primary key of the p o audit
     * @return the p o audit that was removed
     * @throws com.hp.omp.NoSuchPOAuditException if a p o audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public POAudit remove(long auditId)
        throws NoSuchPOAuditException, SystemException {
        return remove(Long.valueOf(auditId));
    }

    /**
     * Removes the p o audit with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the p o audit
     * @return the p o audit that was removed
     * @throws com.hp.omp.NoSuchPOAuditException if a p o audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public POAudit remove(Serializable primaryKey)
        throws NoSuchPOAuditException, SystemException {
        Session session = null;

        try {
            session = openSession();

            POAudit poAudit = (POAudit) session.get(POAuditImpl.class,
                    primaryKey);

            if (poAudit == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchPOAuditException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(poAudit);
        } catch (NoSuchPOAuditException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected POAudit removeImpl(POAudit poAudit) throws SystemException {
        poAudit = toUnwrappedModel(poAudit);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, poAudit);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(poAudit);

        return poAudit;
    }

    @Override
    public POAudit updateImpl(com.hp.omp.model.POAudit poAudit, boolean merge)
        throws SystemException {
        poAudit = toUnwrappedModel(poAudit);

        boolean isNew = poAudit.isNew();

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, poAudit, merge);

            poAudit.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(POAuditModelImpl.ENTITY_CACHE_ENABLED,
            POAuditImpl.class, poAudit.getPrimaryKey(), poAudit);

        return poAudit;
    }

    protected POAudit toUnwrappedModel(POAudit poAudit) {
        if (poAudit instanceof POAuditImpl) {
            return poAudit;
        }

        POAuditImpl poAuditImpl = new POAuditImpl();

        poAuditImpl.setNew(poAudit.isNew());
        poAuditImpl.setPrimaryKey(poAudit.getPrimaryKey());

        poAuditImpl.setAuditId(poAudit.getAuditId());
        poAuditImpl.setPurchaseOrderId(poAudit.getPurchaseOrderId());
        poAuditImpl.setStatus(poAudit.getStatus());
        poAuditImpl.setUserId(poAudit.getUserId());
        poAuditImpl.setChangeDate(poAudit.getChangeDate());

        return poAuditImpl;
    }

    /**
     * Returns the p o audit with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the p o audit
     * @return the p o audit
     * @throws com.liferay.portal.NoSuchModelException if a p o audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public POAudit findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the p o audit with the primary key or throws a {@link com.hp.omp.NoSuchPOAuditException} if it could not be found.
     *
     * @param auditId the primary key of the p o audit
     * @return the p o audit
     * @throws com.hp.omp.NoSuchPOAuditException if a p o audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public POAudit findByPrimaryKey(long auditId)
        throws NoSuchPOAuditException, SystemException {
        POAudit poAudit = fetchByPrimaryKey(auditId);

        if (poAudit == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + auditId);
            }

            throw new NoSuchPOAuditException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                auditId);
        }

        return poAudit;
    }

    /**
     * Returns the p o audit with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the p o audit
     * @return the p o audit, or <code>null</code> if a p o audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public POAudit fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the p o audit with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param auditId the primary key of the p o audit
     * @return the p o audit, or <code>null</code> if a p o audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public POAudit fetchByPrimaryKey(long auditId) throws SystemException {
        POAudit poAudit = (POAudit) EntityCacheUtil.getResult(POAuditModelImpl.ENTITY_CACHE_ENABLED,
                POAuditImpl.class, auditId);

        if (poAudit == _nullPOAudit) {
            return null;
        }

        if (poAudit == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                poAudit = (POAudit) session.get(POAuditImpl.class,
                        Long.valueOf(auditId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (poAudit != null) {
                    cacheResult(poAudit);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(POAuditModelImpl.ENTITY_CACHE_ENABLED,
                        POAuditImpl.class, auditId, _nullPOAudit);
                }

                closeSession(session);
            }
        }

        return poAudit;
    }

    /**
     * Returns all the p o audits.
     *
     * @return the p o audits
     * @throws SystemException if a system exception occurred
     */
    public List<POAudit> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the p o audits.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of p o audits
     * @param end the upper bound of the range of p o audits (not inclusive)
     * @return the range of p o audits
     * @throws SystemException if a system exception occurred
     */
    public List<POAudit> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the p o audits.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of p o audits
     * @param end the upper bound of the range of p o audits (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of p o audits
     * @throws SystemException if a system exception occurred
     */
    public List<POAudit> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<POAudit> list = (List<POAudit>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_POAUDIT);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_POAUDIT;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<POAudit>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<POAudit>) QueryUtil.list(q, getDialect(),
                            start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the p o audits from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (POAudit poAudit : findAll()) {
            remove(poAudit);
        }
    }

    /**
     * Returns the number of p o audits.
     *
     * @return the number of p o audits
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_POAUDIT);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the p o audit persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.POAudit")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<POAudit>> listenersList = new ArrayList<ModelListener<POAudit>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<POAudit>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(POAuditImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
