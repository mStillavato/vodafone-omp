package com.hp.omp.service.persistence;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.hp.omp.model.BudgetCategory;
import com.hp.omp.model.impl.BudgetCategoryImpl;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class BudgetCategoryFinderImpl extends BudgetCategoryPersistenceImpl implements BudgetCategoryFinder{

	private static Log _log = LogFactoryUtil.getLog(BudgetCategoryFinderImpl.class);
	static final String GET_BUDGET_CATEGORY_LIST = BudgetCategoryFinderImpl.class.getName()+".getBudgetCategoryList";
	@SuppressWarnings("unchecked")
	public List<BudgetCategory> getBudgetCategoryList(int start , int end, String searchFilter) throws SystemException
	{
		Session session=null;
		List<BudgetCategory> list = new ArrayList<BudgetCategory>();
		try {
		session  = openSession();
		
		String sql = null;
		
		if(StringUtils.isEmpty(searchFilter))
			searchFilter = "%%";
		
		sql = CustomSQLUtil.get(GET_BUDGET_CATEGORY_LIST);
		SQLQuery query = session.createSQLQuery(sql);
		
		if(query == null){
			_log.error("get budget category list error, no sql found");
			return null;
		}
		
		query.setString(0, searchFilter);
		
		query.addEntity("BudgetCategoryList", BudgetCategoryImpl.class);
		list = (List<BudgetCategory>)QueryUtil.list(query, getDialect(), start, end);
		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
		return list;
	}
}
