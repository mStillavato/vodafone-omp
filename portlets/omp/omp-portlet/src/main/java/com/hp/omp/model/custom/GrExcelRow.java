package com.hp.omp.model.custom;

public class GrExcelRow {

	private String position;
	private String poOrderId;
	private String cdc;
	private String richiedente;
	private String valoreTotPos;
	private String qtaPos;
	private String valoreRichiesto;
	private String percentualeRichiesta;
	
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getPoOrderId() {
		return poOrderId;
	}
	public void setPoOrderId(String poOrderId) {
		this.poOrderId = poOrderId;
	}
	public String getCdc() {
		return cdc;
	}
	public void setCdc(String cdc) {
		this.cdc = cdc;
	}
	public String getRichiedente() {
		return richiedente;
	}
	public void setRichiedente(String richiedente) {
		this.richiedente = richiedente;
	}
	public String getValoreTotPos() {
		return valoreTotPos;
	}
	public void setValoreTotPos(String valoreTotPos) {
		this.valoreTotPos = valoreTotPos;
	}
	public String getQtaPos() {
		return qtaPos;
	}
	public void setQtaPos(String qtaPos) {
		this.qtaPos = qtaPos;
	}
	public String getValoreRichiesto() {
		return valoreRichiesto;
	}
	public void setValoreRichiesto(String valoreRichiesto) {
		this.valoreRichiesto = valoreRichiesto;
	}
	public String getPercentualeRichiesta() {
		return percentualeRichiesta;
	}
	public void setPercentualeRichiesta(String percentualeRichiesta) {
		this.percentualeRichiesta = percentualeRichiesta;
	}

}
