package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchPositionAuditException;
import com.hp.omp.model.PositionAudit;
import com.hp.omp.model.impl.PositionAuditImpl;
import com.hp.omp.model.impl.PositionAuditModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the position audit service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see PositionAuditPersistence
 * @see PositionAuditUtil
 * @generated
 */
public class PositionAuditPersistenceImpl extends BasePersistenceImpl<PositionAudit>
    implements PositionAuditPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link PositionAuditUtil} to access the position audit persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = PositionAuditImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PositionAuditModelImpl.ENTITY_CACHE_ENABLED,
            PositionAuditModelImpl.FINDER_CACHE_ENABLED,
            PositionAuditImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PositionAuditModelImpl.ENTITY_CACHE_ENABLED,
            PositionAuditModelImpl.FINDER_CACHE_ENABLED,
            PositionAuditImpl.class, FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PositionAuditModelImpl.ENTITY_CACHE_ENABLED,
            PositionAuditModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_POSITIONAUDIT = "SELECT positionAudit FROM PositionAudit positionAudit";
    private static final String _SQL_COUNT_POSITIONAUDIT = "SELECT COUNT(positionAudit) FROM PositionAudit positionAudit";
    private static final String _ORDER_BY_ENTITY_ALIAS = "positionAudit.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No PositionAudit exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(PositionAuditPersistenceImpl.class);
    private static PositionAudit _nullPositionAudit = new PositionAuditImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<PositionAudit> toCacheModel() {
                return _nullPositionAuditCacheModel;
            }
        };

    private static CacheModel<PositionAudit> _nullPositionAuditCacheModel = new CacheModel<PositionAudit>() {
            public PositionAudit toEntityModel() {
                return _nullPositionAudit;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the position audit in the entity cache if it is enabled.
     *
     * @param positionAudit the position audit
     */
    public void cacheResult(PositionAudit positionAudit) {
        EntityCacheUtil.putResult(PositionAuditModelImpl.ENTITY_CACHE_ENABLED,
            PositionAuditImpl.class, positionAudit.getPrimaryKey(),
            positionAudit);

        positionAudit.resetOriginalValues();
    }

    /**
     * Caches the position audits in the entity cache if it is enabled.
     *
     * @param positionAudits the position audits
     */
    public void cacheResult(List<PositionAudit> positionAudits) {
        for (PositionAudit positionAudit : positionAudits) {
            if (EntityCacheUtil.getResult(
                        PositionAuditModelImpl.ENTITY_CACHE_ENABLED,
                        PositionAuditImpl.class, positionAudit.getPrimaryKey()) == null) {
                cacheResult(positionAudit);
            } else {
                positionAudit.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all position audits.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(PositionAuditImpl.class.getName());
        }

        EntityCacheUtil.clearCache(PositionAuditImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the position audit.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(PositionAudit positionAudit) {
        EntityCacheUtil.removeResult(PositionAuditModelImpl.ENTITY_CACHE_ENABLED,
            PositionAuditImpl.class, positionAudit.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<PositionAudit> positionAudits) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (PositionAudit positionAudit : positionAudits) {
            EntityCacheUtil.removeResult(PositionAuditModelImpl.ENTITY_CACHE_ENABLED,
                PositionAuditImpl.class, positionAudit.getPrimaryKey());
        }
    }

    /**
     * Creates a new position audit with the primary key. Does not add the position audit to the database.
     *
     * @param auditId the primary key for the new position audit
     * @return the new position audit
     */
    public PositionAudit create(long auditId) {
        PositionAudit positionAudit = new PositionAuditImpl();

        positionAudit.setNew(true);
        positionAudit.setPrimaryKey(auditId);

        return positionAudit;
    }

    /**
     * Removes the position audit with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param auditId the primary key of the position audit
     * @return the position audit that was removed
     * @throws com.hp.omp.NoSuchPositionAuditException if a position audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public PositionAudit remove(long auditId)
        throws NoSuchPositionAuditException, SystemException {
        return remove(Long.valueOf(auditId));
    }

    /**
     * Removes the position audit with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the position audit
     * @return the position audit that was removed
     * @throws com.hp.omp.NoSuchPositionAuditException if a position audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PositionAudit remove(Serializable primaryKey)
        throws NoSuchPositionAuditException, SystemException {
        Session session = null;

        try {
            session = openSession();

            PositionAudit positionAudit = (PositionAudit) session.get(PositionAuditImpl.class,
                    primaryKey);

            if (positionAudit == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchPositionAuditException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(positionAudit);
        } catch (NoSuchPositionAuditException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected PositionAudit removeImpl(PositionAudit positionAudit)
        throws SystemException {
        positionAudit = toUnwrappedModel(positionAudit);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, positionAudit);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(positionAudit);

        return positionAudit;
    }

    @Override
    public PositionAudit updateImpl(
        com.hp.omp.model.PositionAudit positionAudit, boolean merge)
        throws SystemException {
        positionAudit = toUnwrappedModel(positionAudit);

        boolean isNew = positionAudit.isNew();

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, positionAudit, merge);

            positionAudit.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(PositionAuditModelImpl.ENTITY_CACHE_ENABLED,
            PositionAuditImpl.class, positionAudit.getPrimaryKey(),
            positionAudit);

        return positionAudit;
    }

    protected PositionAudit toUnwrappedModel(PositionAudit positionAudit) {
        if (positionAudit instanceof PositionAuditImpl) {
            return positionAudit;
        }

        PositionAuditImpl positionAuditImpl = new PositionAuditImpl();

        positionAuditImpl.setNew(positionAudit.isNew());
        positionAuditImpl.setPrimaryKey(positionAudit.getPrimaryKey());

        positionAuditImpl.setAuditId(positionAudit.getAuditId());
        positionAuditImpl.setPositionId(positionAudit.getPositionId());
        positionAuditImpl.setStatus(positionAudit.getStatus());
        positionAuditImpl.setUserId(positionAudit.getUserId());
        positionAuditImpl.setChangeDate(positionAudit.getChangeDate());

        return positionAuditImpl;
    }

    /**
     * Returns the position audit with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the position audit
     * @return the position audit
     * @throws com.liferay.portal.NoSuchModelException if a position audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PositionAudit findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the position audit with the primary key or throws a {@link com.hp.omp.NoSuchPositionAuditException} if it could not be found.
     *
     * @param auditId the primary key of the position audit
     * @return the position audit
     * @throws com.hp.omp.NoSuchPositionAuditException if a position audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public PositionAudit findByPrimaryKey(long auditId)
        throws NoSuchPositionAuditException, SystemException {
        PositionAudit positionAudit = fetchByPrimaryKey(auditId);

        if (positionAudit == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + auditId);
            }

            throw new NoSuchPositionAuditException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                auditId);
        }

        return positionAudit;
    }

    /**
     * Returns the position audit with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the position audit
     * @return the position audit, or <code>null</code> if a position audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public PositionAudit fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the position audit with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param auditId the primary key of the position audit
     * @return the position audit, or <code>null</code> if a position audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public PositionAudit fetchByPrimaryKey(long auditId)
        throws SystemException {
        PositionAudit positionAudit = (PositionAudit) EntityCacheUtil.getResult(PositionAuditModelImpl.ENTITY_CACHE_ENABLED,
                PositionAuditImpl.class, auditId);

        if (positionAudit == _nullPositionAudit) {
            return null;
        }

        if (positionAudit == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                positionAudit = (PositionAudit) session.get(PositionAuditImpl.class,
                        Long.valueOf(auditId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (positionAudit != null) {
                    cacheResult(positionAudit);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(PositionAuditModelImpl.ENTITY_CACHE_ENABLED,
                        PositionAuditImpl.class, auditId, _nullPositionAudit);
                }

                closeSession(session);
            }
        }

        return positionAudit;
    }

    /**
     * Returns all the position audits.
     *
     * @return the position audits
     * @throws SystemException if a system exception occurred
     */
    public List<PositionAudit> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the position audits.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of position audits
     * @param end the upper bound of the range of position audits (not inclusive)
     * @return the range of position audits
     * @throws SystemException if a system exception occurred
     */
    public List<PositionAudit> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the position audits.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of position audits
     * @param end the upper bound of the range of position audits (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of position audits
     * @throws SystemException if a system exception occurred
     */
    public List<PositionAudit> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<PositionAudit> list = (List<PositionAudit>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_POSITIONAUDIT);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_POSITIONAUDIT;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<PositionAudit>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<PositionAudit>) QueryUtil.list(q,
                            getDialect(), start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the position audits from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (PositionAudit positionAudit : findAll()) {
            remove(positionAudit);
        }
    }

    /**
     * Returns the number of position audits.
     *
     * @return the number of position audits
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_POSITIONAUDIT);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the position audit persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.PositionAudit")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<PositionAudit>> listenersList = new ArrayList<ModelListener<PositionAudit>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<PositionAudit>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(PositionAuditImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
