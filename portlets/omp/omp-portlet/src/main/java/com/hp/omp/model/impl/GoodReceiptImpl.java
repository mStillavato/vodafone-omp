/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.model.impl;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.model.Position;
import com.hp.omp.model.custom.GoodReceiptStatus;
import com.hp.omp.model.custom.OmpFormatterUtil;
import com.hp.omp.model.custom.OmpGroups;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.service.GoodReceiptLocalServiceUtil;
import com.hp.omp.service.PositionLocalServiceUtil;
import com.hp.omp.service.impl.GoodReceiptLocalServiceImpl;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.security.auth.PrincipalThreadLocal;

/**
 * The extended model implementation for the GoodReceipt service. Represents a row in the &quot;GOOD_RECEIPT&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.model.GoodReceipt} interface.
 * </p>
 *
 * @author Brian Wing Shun Chan
 */
public class GoodReceiptImpl extends GoodReceiptBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a good receipt model instance should use the {@link com.hp.omp.model.GoodReceipt} interface instead.
	 */
	public GoodReceiptImpl() {
	}
	public static Logger logger = LoggerFactory.getLogger(GoodReceiptImpl.class);
	public static final String DATE_FORMAT = "dd/MM/yyyy";
	
	
	public GoodReceiptStatus getGoodReceiptStatus() {
		if(getRegisterationDate() == null || getGoodReceiptNumber() == null || "".equals(getGoodReceiptNumber())) {
			return GoodReceiptStatus.GR_REQUESTED;
		} 
			return GoodReceiptStatus.GR_CLOSED;
	}
	
	public Date getRequestDate() {
		if(super.getRequestDate() == null) {
			return new Date();
		} 
		return super.getRequestDate();
	}
	
	public String getRequestDateText() {
		DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		return dateFormat.format(getRequestDate());
	}
	
	public String getRegisterationDateText() {
		if(super.getRegisterationDate() != null) {
			DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
			return dateFormat.format(super.getRegisterationDate());
		}
		return "";
	}
	

	
	public String getPercentageText() throws SystemException{
		DecimalFormat df = new DecimalFormat("##");
		
			if(super.getGoodReceiptId() == 0 && super.getPositionId() != null && !super.getPositionId().equals("")) {
				Double sumPercentage = GoodReceiptLocalServiceUtil.getSumGRPercentageToPosition(super.getPositionId());
				if(sumPercentage == null) {
					sumPercentage = 0D;
				}
				Double max = 100 - sumPercentage;
				return df.format(max);
			} 
		return df.format(super.getPercentage());
	}
	
	public String getFormattedPercentageText() throws SystemException{
			if(super.getGoodReceiptId() == 0 && super.getPositionId() != null && !super.getPositionId().equals("")) {
				Double sumPercentage = GoodReceiptLocalServiceUtil.getSumGRPercentageToPosition(super.getPositionId());
				if(sumPercentage == null) {
					sumPercentage = 0D;
				}
				Double max = 100 - sumPercentage;
				return OmpFormatterUtil.formatNumberByLocale(max, Locale.ITALY);
			} 
		return OmpFormatterUtil.formatNumberByLocale(super.getPercentage(),Locale.ITALY);
	}
	
	public String getFormattedPercentage() throws SystemException{
		 return OmpFormatterUtil.formatNumberByLocale(super.getPercentage(),Locale.ITALY);
		}
	
	public Double getMaxPercentage() throws SystemException {
		if (super.getPositionId() != null && !"".equals(super.getPositionId())) {
			logger.debug("Good Receipt percentage {}", super.getPercentage());
			Double max = 100 - GoodReceiptLocalServiceUtil.getSumGRPercentageToPosition(super.getPositionId()) + super.getPercentage();
			logger.debug("Good Receipt max value {}",max);
			return max;
		}
		return 100D;
	}
	public String getGrValueText() throws NumberFormatException, PortalException, SystemException {
		if(super.getGoodReceiptId() == 0 && super.getPositionId() != null && !super.getPositionId().equals("")) {
			double total =Double.valueOf(getPosition().getActualUnitCost()) * Double.valueOf(getPosition().getQuatity());
			logger.debug("The GR total is {}", total);
			double calc = (double)(total*Double.valueOf(getPercentageText()))/100;
			logger.debug("The GR calculated Value is {}", calc);
			return String.valueOf(calc);
			
		} 
	return String.valueOf(super.getGrValue());
	}
	
	public String getFormattedGrValueText() throws NumberFormatException, PortalException, SystemException {
	 String value=	getGrValueText();
	 if(value !=null && ! "".equals(value)) {
		 return OmpFormatterUtil.formatNumberByLocale(Double.valueOf(value), Locale.ITALY);
	 }else {
		 return value;
	 }
		
	}
	
	public String getFormattedGrValue() throws NumberFormatException, PortalException, SystemException {
		 String value=	super.getGrValue();
		 if(value !=null && ! "".equals(value)) {
			 return OmpFormatterUtil.formatNumberByLocale(Double.valueOf(value), Locale.ITALY);
		 }else {
			 return value;
		 }
			
		}
	
	public Position getPosition() throws PortalException, SystemException {
		if(super.getPositionId() != null) {
			return PositionLocalServiceUtil.getPosition(Long.valueOf(super.getPositionId()));
		}
		return null;
	}
	
	public String getRequestDateReadonly() throws PortalException, SystemException {
		long userId =  PrincipalThreadLocal.getUserId();
		OmpRoles userRole = OmpHelper.getUserRole(userId);
		if(OmpRoles.OMP_CONTROLLER.equals(userRole) || OmpRoles.OMP_CONTROLLER_VBS.equals(userRole)) {
			return "";
		} 
		return "readonly";
	}
	public String getPercentageReadonly() throws PortalException, SystemException {
		return sharedFieldsStatus();
	}
	public String getGrValueReadonly() throws PortalException, SystemException {
		long userId =  PrincipalThreadLocal.getUserId();
		OmpRoles userRole = OmpHelper.getUserRole(userId);
		if(OmpRoles.OMP_CONTROLLER.equals(userRole) || OmpRoles.OMP_CONTROLLER_VBS.equals(userRole)) {
			return "";
		} 
			return "readonly";
	
	}
	public String getGrNumberReadonly() throws PortalException, SystemException {
		return antexFieldsStatus();
	}
	
	private String sharedFieldsStatus()throws PortalException, SystemException{
		long userId =  PrincipalThreadLocal.getUserId();
		OmpRoles userRole = OmpHelper.getUserRole(userId);
		switch (userRole) {
		case OMP_CONTROLLER:
			return "";
		case OMP_ENG:
		case OMP_ENG_VBS:
		case OMP_APPROVER_VBS:
		case OMP_NDSO:
		case OMP_TC:
			if(this.getRegisterationDate() != null) {
				return "readonly";
			} else {
				return "";
			}
		case OMP_ANTEX:
			return "readonly";
		case OMP_CONTROLLER_VBS:
			return "";
		default:
			return "readonly";
		}
	}
	public String getRegisterationDateReadonly() throws PortalException, SystemException {
		return antexFieldsStatus();
	}
	
	public String antexFieldsStatus() throws PortalException, SystemException{
		long userId =  PrincipalThreadLocal.getUserId();
		OmpRoles userRole = OmpHelper.getUserRole(userId);
		switch (userRole) {
		case OMP_CONTROLLER:
			return "";
		case OMP_CONTROLLER_VBS:
			return "";
		case OMP_ENG:
			return "readonly";
		case OMP_ENG_VBS:
			return "readonly";
		case OMP_ANTEX:
			if(this.getRegisterationDate() != null && this.getGoodReceiptNumber() != null && !"".equals(this.getGoodReceiptNumber())) {
				return "readonly";
			} else {
				return "";
			}
		default:
			return "readonly";
		}
	}
	
}