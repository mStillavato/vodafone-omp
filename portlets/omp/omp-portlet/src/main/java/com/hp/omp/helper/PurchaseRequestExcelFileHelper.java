package com.hp.omp.helper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.hp.omp.model.custom.GrExcelRow;
import com.hp.omp.model.custom.LocationDTO;
import com.hp.omp.model.custom.PurchaseRequestExcelRow;
import com.hp.omp.service.LocationLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;

public class PurchaseRequestExcelFileHelper {

	public static List<PurchaseRequestExcelRow> readExcelFile(File inputWorkbook) throws IOException, SystemException  {

		FileInputStream file = new FileInputStream(inputWorkbook);

		//Create Workbook instance holding reference to .xlsx file
		XSSFWorkbook workbook = new XSSFWorkbook(file);

		//Get first/desired sheet from the workbook
		XSSFSheet sheet = workbook.getSheetAt(0);

		List<PurchaseRequestExcelRow> prList = new ArrayList<PurchaseRequestExcelRow>(); 
		for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
			XSSFRow row = sheet.getRow(rowIndex);
			PurchaseRequestExcelRow pr = new PurchaseRequestExcelRow();
			if (row != null) {
				
				if(row.getCell(0) == null 
						&& row.getCell(1) == null 
						&& row.getCell(2) == null 
						&& row.getCell(3) == null
						&& row.getCell(4) == null
						&& row.getCell(5) == null){
					break;
				}
				
				//for(int columNum = 0 ; columNum < row.getPhysicalNumberOfCells(); columNum++) {
				for(int columNum = 0 ; columNum < 65; columNum++) {
					switch (columNum) {
					case 5:	//Vendor Code
						if(row.getCell(columNum) != null){
							if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
								pr.setVendorCode(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
								pr.setVendorCode(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
							}
						} else {
							pr.setVendorCode("");
						}
						break;
						
					case 7:	//LocationEvoId
						if(row.getCell(columNum) != null){
							if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
								pr.setEvoLocationId(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
								if(pr.getEvoLocationId() != null && pr.getEvoLocationId().trim() != ""){
									LocationDTO locationDto = LocationLocalServiceUtil.getLocationByLocationEvo(pr.getEvoLocationId());
									if(locationDto != null){
										pr.setTargaTecnica("" + locationDto.getTargaTecnicaSap());
									}
								}
							} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
								pr.setEvoLocationId(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
								if(pr.getEvoLocationId() != null && pr.getEvoLocationId().trim() != ""){
									LocationDTO locationDto = LocationLocalServiceUtil.getLocationByLocationEvo(pr.getEvoLocationId());
									if(locationDto != null){
										pr.setTargaTecnica("" + locationDto.getTargaTecnicaSap());
									}
								}
							}
							
						} else {
							pr.setEvoLocationId("");
							pr.setTargaTecnica("");
						}
						break;
						
					case 11:	//Codice Materiale Evo
						if(row.getCell(columNum) != null){
							if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
								pr.setEvoCodMateriale(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
								pr.setEvoCodMateriale(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
							}
						} else {
							pr.setEvoCodMateriale("");
						}
						break;
						
					case 12:	//Descrizione Materiale Evo
						pr.setEvoDesMateriale(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
						break;
						
					case 13:	//PO Quantity
						if(row.getCell(columNum) != null){
							if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
								pr.setPoQuantity(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
								pr.setPoQuantity(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
							}
						} else {
							pr.setPoQuantity("0");
						}
						break;
						
					case 15: // Delivery Date
						pr.setDeliveryDate(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
						break;
						
					case 16:	//Net Price
						if(row.getCell(columNum) != null){
							if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
								pr.setNetPrice(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
								pr.setNetPrice(row.getCell(columNum) == null ? "" : (double)row.getCell(columNum).getNumericCellValue()+"");
							}
						} else {
							pr.setNetPrice("0");
						}
						break;
						
					case 17:	//Currency
						if(row.getCell(columNum) != null){
							if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
								pr.setCurrency("1");	// EUR
								if(row.getCell(columNum) != null){
									if("USD".equals(row.getCell(columNum).getStringCellValue())){
										pr.setCurrency("0");	//USD
									}
								}
							} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
								pr.setCurrency("1");
								if(row.getCell(columNum) != null){
									if("USD".equals(row.getCell(columNum).getNumericCellValue()+"")){
										pr.setCurrency("0");
									}
								}
							}
						} else {
							pr.setCurrency("0");
						}
						break;
						
					case 19:	//Plant
						pr.setPlant(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
						break;
						
					case 27:	// WBS Code
						if(row.getCell(columNum) != null){
							if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
								pr.setWbsCodeName(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
								pr.setWbsCodeName(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
							}
						} else {
							pr.setWbsCodeName("");
						}
						break;
						
					case 44:	//Address Number
						pr.setAddressNumber(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
						break;
						
					case 53:	// Item text
						if(row.getCell(columNum) != null){
							if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
								pr.setItemText(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
								pr.setItemText(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
							}
						} else {
							pr.setItemText("");
						}
						break;		
						
					case 62:	// Tracking Number
						if(row.getCell(columNum) != null){
							if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
								pr.setTrackingNumber(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
								pr.setTrackingNumber(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
							}
						} else {
							pr.setTrackingNumber("");
						}
						break;
					}
				}
			}
			prList.add(pr);
		}
		file.close();
		return prList;
	}
	
	public static List<PurchaseRequestExcelRow> readXlsNotNssFile(File inputWorkbook) throws IOException, SystemException  {

		FileInputStream file = new FileInputStream(inputWorkbook);
		
    	HSSFWorkbook workbook = new HSSFWorkbook(file);
    	HSSFSheet sheet = workbook.getSheetAt(0);
        	
		List<PurchaseRequestExcelRow> prList = new ArrayList<PurchaseRequestExcelRow>(); 
		for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
			HSSFRow row = sheet.getRow(rowIndex);
			PurchaseRequestExcelRow pr = new PurchaseRequestExcelRow();
			if (row != null) {
				
				if(row.getCell(1) == null){
					break;
				}
				
				//for(int columNum = 0 ; columNum < row.getPhysicalNumberOfCells(); columNum++) {
				for(int columNum = 0 ; columNum < 15; columNum++) {
					switch (columNum) {
						case 1:	//Codice Materiale Evo
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setEvoCodMateriale(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setEvoCodMateriale(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
								}
							} else {
								pr.setEvoCodMateriale("");
							}
							break;
							
						case 2:	//Descrizione Materiale Evo
							pr.setEvoDesMateriale(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							break;
							
						case 3:	//PO Quantity
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setPoQuantity(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setPoQuantity(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
								}
							} else {
								pr.setPoQuantity("0");
							}
							break;	
							
						case 4:	//Net Price
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setNetPrice(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setNetPrice(row.getCell(columNum) == null ? "" : (double)row.getCell(columNum).getNumericCellValue()+"");
								}
							} else {
								pr.setNetPrice("0");
							}
							break;	
							
						case 5:	//Currency
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setCurrency("1");	// EUR
									if(row.getCell(columNum) != null){
										if("USD".equals(row.getCell(columNum).getStringCellValue())){
											pr.setCurrency("0");	//USD
										}
									}
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setCurrency("1");
									if(row.getCell(columNum) != null){
										if("USD".equals(row.getCell(columNum).getNumericCellValue()+"")){
											pr.setCurrency("0");
										}
									}
								}
							} else {
								pr.setCurrency("0");
							}
							break;
						
						case 6:	// Delivery Date
							pr.setDeliveryDate(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							break;
							
						case 7:	//Address Number
							pr.setAddressNumber(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							break;
							
						case 8:	// Item text
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setItemText(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setItemText(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
								}
							} else {
								pr.setItemText("");
							}
							break;
							
							
						case 9:	//LocationEvoId
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setEvoLocationId(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
									if(pr.getEvoLocationId() != null && pr.getEvoLocationId().trim() != ""){
										LocationDTO locationDto = LocationLocalServiceUtil.getLocationByLocationEvo(pr.getEvoLocationId());
										if(locationDto != null){
											pr.setTargaTecnica("" + locationDto.getTargaTecnicaSap());
										}
									}
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setEvoLocationId(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
									if(pr.getEvoLocationId() != null && pr.getEvoLocationId().trim() != ""){
										LocationDTO locationDto = LocationLocalServiceUtil.getLocationByLocationEvo(pr.getEvoLocationId());
										if(locationDto != null){
											pr.setTargaTecnica("" + locationDto.getTargaTecnicaSap());
										}
									}
								}
								
							} else {
								pr.setEvoLocationId("");
							}
							break;
							
						case 10:	//Targa Tecnica
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setTargaTecnica(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
									if(pr.getTargaTecnica() != null && pr.getTargaTecnica().trim() != ""){
										LocationDTO locationDto = LocationLocalServiceUtil.getLocationByTargaTecnica(pr.getTargaTecnica());
										if(locationDto != null){
											pr.setEvoLocationId("" + locationDto.getLocation());
										}
									}
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setTargaTecnica(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
									if(pr.getTargaTecnica() != null && pr.getTargaTecnica().trim() != ""){
										LocationDTO locationDto = LocationLocalServiceUtil.getLocationByTargaTecnica(pr.getTargaTecnica());
										if(locationDto != null){
											pr.setEvoLocationId("" + locationDto.getLocation());
										}
									}
								}
								
							} else {
								if(pr.getTargaTecnica() == null)
									pr.setTargaTecnica("");
							}
							break;
							
						case 11:	//Vendor
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setVendorCode(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setVendorCode(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
								}
							} else {
								pr.setVendorCode("");
							}
							break;
							
						case 12:	// IC
							// 1 = I&C Field
							// 2 = I&C Test
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setIcApproval(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setIcApproval(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
								}
							} else {
								pr.setIcApproval(null);
							}
							break;
							
						case 13:	// Categoy Code
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setCategoryCode(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setCategoryCode(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
								}
							} else {
								pr.setCategoryCode("");
							}
							break;
							
						case 14:	// Offer Number
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setOfferNumber(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setOfferNumber(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
								}
							} else {
								pr.setOfferNumber("");
							}
							break;
					}
				}
			}
			prList.add(pr);
		}
		file.close();
		return prList;
	}
	
	public static List<PurchaseRequestExcelRow> readXlsxNotNssFile(File inputWorkbook) throws IOException, SystemException  {

		FileInputStream file = new FileInputStream(inputWorkbook);

		XSSFWorkbook workbook = new XSSFWorkbook(file);
		XSSFSheet sheet = workbook.getSheetAt(0);
	     
		List<PurchaseRequestExcelRow> prList = new ArrayList<PurchaseRequestExcelRow>(); 
		for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
			XSSFRow row = sheet.getRow(rowIndex);
			PurchaseRequestExcelRow pr = new PurchaseRequestExcelRow();
			if (row != null) {
				
				if(row.getCell(1) == null){
					break;
				}
				
				//for(int columNum = 0 ; columNum < row.getPhysicalNumberOfCells(); columNum++) {
				for(int columNum = 0 ; columNum < 15; columNum++) {
					switch (columNum) {
						case 1:	//Codice Materiale Evo
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setEvoCodMateriale(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setEvoCodMateriale(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
								}
							} else {
								pr.setEvoCodMateriale("");
							}
							break;
							
						case 2:	//Descrizione Materiale Evo
							pr.setEvoDesMateriale(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							break;
							
						case 3:	//PO Quantity
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setPoQuantity(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setPoQuantity(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
								}
							} else {
								pr.setPoQuantity("0");
							}
							break;	
							
						case 4:	//Net Price
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setNetPrice(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setNetPrice(row.getCell(columNum) == null ? "" : (double)row.getCell(columNum).getNumericCellValue()+"");
								}
							} else {
								pr.setNetPrice("0");
							}
							break;	
							
						case 5:	//Currency
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setCurrency("1");	// EUR
									if(row.getCell(columNum) != null){
										if("USD".equals(row.getCell(columNum).getStringCellValue())){
											pr.setCurrency("0");	//USD
										}
									}
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setCurrency("1");
									if(row.getCell(columNum) != null){
										if("USD".equals(row.getCell(columNum).getNumericCellValue()+"")){
											pr.setCurrency("0");
										}
									}
								}
							} else {
								pr.setCurrency("0");
							}
							break;
						
						case 6:	// Delivery Date
							pr.setDeliveryDate(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							break;
							
						case 7:	//Address Number
							pr.setAddressNumber(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							break;
							
						case 8:	// Item text
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setItemText(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setItemText(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
								}
							} else {
								pr.setItemText("");
							}
							break;
							
							
						case 9:	//LocationEvoId
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setEvoLocationId(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
									if(pr.getEvoLocationId() != null && pr.getEvoLocationId().trim() != ""){
										LocationDTO locationDto = LocationLocalServiceUtil.getLocationByLocationEvo(pr.getEvoLocationId());
										if(locationDto != null){
											pr.setTargaTecnica("" + locationDto.getTargaTecnicaSap());
										}
									}
									
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setEvoLocationId(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
									if(pr.getEvoLocationId() != null && pr.getEvoLocationId().trim() != ""){
										LocationDTO locationDto = LocationLocalServiceUtil.getLocationByLocationEvo(pr.getEvoLocationId());
										if(locationDto != null){
											pr.setTargaTecnica("" + locationDto.getTargaTecnicaSap());
										}
									}
								}

							} else {
								pr.setEvoLocationId("");
							}
							break;
							
						case 10:	//Targa Tecnica
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setTargaTecnica(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
									if(pr.getTargaTecnica() != null && pr.getTargaTecnica().trim() != ""){
										LocationDTO locationDto = LocationLocalServiceUtil.getLocationByTargaTecnica(pr.getTargaTecnica());
										if(locationDto != null){
											pr.setEvoLocationId("" + locationDto.getLocation());
										}
									}
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setTargaTecnica(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
									if(pr.getTargaTecnica() != null && pr.getTargaTecnica().trim() != ""){
										LocationDTO locationDto = LocationLocalServiceUtil.getLocationByTargaTecnica(pr.getTargaTecnica());
										if(locationDto != null){
											pr.setEvoLocationId("" + locationDto.getLocation());
										}
									}
								}
								
							} else {
								if(pr.getTargaTecnica() == null)
									pr.setTargaTecnica("");
							}
							break;
							
						case 11:	//Vendor
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setVendorCode(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setVendorCode(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
								}
							} else {
								pr.setVendorCode("");
							}
							break;
							
						case 12:	// IC
							// 1 = I&C Field
							// 2 = I&C Test
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setIcApproval(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setIcApproval(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
								}
							} else {
								pr.setIcApproval(null);
							}
							break;
							
						case 13:	// Categoy Code
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setCategoryCode(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setCategoryCode(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
								}
							} else {
								pr.setCategoryCode("");
							}
							break;
							
						case 14:	// Offer Number
							if(row.getCell(columNum) != null){
								if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
									pr.setOfferNumber(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
								} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
									pr.setOfferNumber(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
								}
							} else {
								pr.setOfferNumber("");
							}
							break;
					}
				}
			}
			prList.add(pr);
		}
		file.close();
		return prList;
	}
	
	public static List<GrExcelRow> readExcelFileGr(File inputWorkbook) throws IOException  {

		FileInputStream file = new FileInputStream(inputWorkbook);

		//Create Workbook instance holding reference to .xlsx file
		XSSFWorkbook workbook = new XSSFWorkbook(file);

		//Get first/desired sheet from the workbook
		XSSFSheet sheet = workbook.getSheetAt(0);

		List<GrExcelRow> trackingList = new ArrayList<GrExcelRow>(); 
		for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
			XSSFRow row = sheet.getRow(rowIndex);
			GrExcelRow trackingCode = new GrExcelRow();
			if (row != null) {
				
				if(row.getCell(0) == null){
					break;
				}
				
				for(int columNum = 0 ; columNum < row.getPhysicalNumberOfCells(); columNum++) {
					switch (columNum) {
						case 0:	// PoOrderId
							if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
								trackingCode.setPoOrderId(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
								trackingCode.setPoOrderId(row.getCell(columNum) == null ? "" : (long)row.getCell(columNum).getNumericCellValue()+"");
							}
							break;
							
						case 1:	// Position
							if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
								trackingCode.setPosition(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
								trackingCode.setPosition(row.getCell(columNum) == null ? "" : (int)row.getCell(columNum).getNumericCellValue()+"");
							}
							break;	
	
						case 2: // Percentuale Richiesta
							if(row.getCell(columNum).getCellType() == Cell.CELL_TYPE_STRING) {
								trackingCode.setPercentualeRichiesta(row.getCell(columNum) == null ? "":row.getCell(columNum).getStringCellValue());
							} else if (row.getCell(columNum).getCellType() == Cell.CELL_TYPE_NUMERIC) {
								trackingCode.setPercentualeRichiesta(row.getCell(columNum) == null ? "" : (double)row.getCell(columNum).getNumericCellValue()+"");
							}       				  
							break;
					}
				}
			}
			trackingList.add(trackingCode);
		}
		file.close();
		return trackingList;
	}
}
