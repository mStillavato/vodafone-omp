package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchVendorException;
import com.hp.omp.model.Vendor;
import com.hp.omp.model.impl.VendorImpl;
import com.hp.omp.model.impl.VendorModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the vendor service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see VendorPersistence
 * @see VendorUtil
 * @generated
 */
public class VendorPersistenceImpl extends BasePersistenceImpl<Vendor>
    implements VendorPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link VendorUtil} to access the vendor persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = VendorImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VendorModelImpl.ENTITY_CACHE_ENABLED,
            VendorModelImpl.FINDER_CACHE_ENABLED, VendorImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VendorModelImpl.ENTITY_CACHE_ENABLED,
            VendorModelImpl.FINDER_CACHE_ENABLED, VendorImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VendorModelImpl.ENTITY_CACHE_ENABLED,
            VendorModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_VENDOR = "SELECT vendor FROM Vendor vendor";
    private static final String _SQL_COUNT_VENDOR = "SELECT COUNT(vendor) FROM Vendor vendor";
    private static final String _ORDER_BY_ENTITY_ALIAS = "vendor.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Vendor exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(VendorPersistenceImpl.class);
    private static Vendor _nullVendor = new VendorImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Vendor> toCacheModel() {
                return _nullVendorCacheModel;
            }
        };

    private static CacheModel<Vendor> _nullVendorCacheModel = new CacheModel<Vendor>() {
            public Vendor toEntityModel() {
                return _nullVendor;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the vendor in the entity cache if it is enabled.
     *
     * @param vendor the vendor
     */
    public void cacheResult(Vendor vendor) {
        EntityCacheUtil.putResult(VendorModelImpl.ENTITY_CACHE_ENABLED,
            VendorImpl.class, vendor.getPrimaryKey(), vendor);

        vendor.resetOriginalValues();
    }

    /**
     * Caches the vendors in the entity cache if it is enabled.
     *
     * @param vendors the vendors
     */
    public void cacheResult(List<Vendor> vendors) {
        for (Vendor vendor : vendors) {
            if (EntityCacheUtil.getResult(
                        VendorModelImpl.ENTITY_CACHE_ENABLED, VendorImpl.class,
                        vendor.getPrimaryKey()) == null) {
                cacheResult(vendor);
            } else {
                vendor.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all vendors.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(VendorImpl.class.getName());
        }

        EntityCacheUtil.clearCache(VendorImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the vendor.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Vendor vendor) {
        EntityCacheUtil.removeResult(VendorModelImpl.ENTITY_CACHE_ENABLED,
            VendorImpl.class, vendor.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Vendor> vendors) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Vendor vendor : vendors) {
            EntityCacheUtil.removeResult(VendorModelImpl.ENTITY_CACHE_ENABLED,
                VendorImpl.class, vendor.getPrimaryKey());
        }
    }

    /**
     * Creates a new vendor with the primary key. Does not add the vendor to the database.
     *
     * @param vendorId the primary key for the new vendor
     * @return the new vendor
     */
    public Vendor create(long vendorId) {
        Vendor vendor = new VendorImpl();

        vendor.setNew(true);
        vendor.setPrimaryKey(vendorId);

        return vendor;
    }

    /**
     * Removes the vendor with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param vendorId the primary key of the vendor
     * @return the vendor that was removed
     * @throws com.hp.omp.NoSuchVendorException if a vendor with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public Vendor remove(long vendorId)
        throws NoSuchVendorException, SystemException {
        return remove(Long.valueOf(vendorId));
    }

    /**
     * Removes the vendor with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the vendor
     * @return the vendor that was removed
     * @throws com.hp.omp.NoSuchVendorException if a vendor with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vendor remove(Serializable primaryKey)
        throws NoSuchVendorException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Vendor vendor = (Vendor) session.get(VendorImpl.class, primaryKey);

            if (vendor == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchVendorException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(vendor);
        } catch (NoSuchVendorException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Vendor removeImpl(Vendor vendor) throws SystemException {
        vendor = toUnwrappedModel(vendor);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, vendor);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(vendor);

        return vendor;
    }

    @Override
    public Vendor updateImpl(com.hp.omp.model.Vendor vendor, boolean merge)
        throws SystemException {
        vendor = toUnwrappedModel(vendor);

        boolean isNew = vendor.isNew();

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, vendor, merge);

            vendor.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(VendorModelImpl.ENTITY_CACHE_ENABLED,
            VendorImpl.class, vendor.getPrimaryKey(), vendor);

        return vendor;
    }

    protected Vendor toUnwrappedModel(Vendor vendor) {
        if (vendor instanceof VendorImpl) {
            return vendor;
        }

        VendorImpl vendorImpl = new VendorImpl();

        vendorImpl.setNew(vendor.isNew());
        vendorImpl.setPrimaryKey(vendor.getPrimaryKey());

        vendorImpl.setVendorId(vendor.getVendorId());
        vendorImpl.setSupplier(vendor.getSupplier());
        vendorImpl.setSupplierCode(vendor.getSupplierCode());
        vendorImpl.setVendorName(vendor.getVendorName());
        vendorImpl.setDisplay(vendor.isDisplay());
        vendorImpl.setGruppoUsers(vendor.getGruppoUsers());

        return vendorImpl;
    }

    /**
     * Returns the vendor with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the vendor
     * @return the vendor
     * @throws com.liferay.portal.NoSuchModelException if a vendor with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vendor findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the vendor with the primary key or throws a {@link com.hp.omp.NoSuchVendorException} if it could not be found.
     *
     * @param vendorId the primary key of the vendor
     * @return the vendor
     * @throws com.hp.omp.NoSuchVendorException if a vendor with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public Vendor findByPrimaryKey(long vendorId)
        throws NoSuchVendorException, SystemException {
        Vendor vendor = fetchByPrimaryKey(vendorId);

        if (vendor == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + vendorId);
            }

            throw new NoSuchVendorException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                vendorId);
        }

        return vendor;
    }

    /**
     * Returns the vendor with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the vendor
     * @return the vendor, or <code>null</code> if a vendor with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vendor fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the vendor with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param vendorId the primary key of the vendor
     * @return the vendor, or <code>null</code> if a vendor with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public Vendor fetchByPrimaryKey(long vendorId) throws SystemException {
        Vendor vendor = (Vendor) EntityCacheUtil.getResult(VendorModelImpl.ENTITY_CACHE_ENABLED,
                VendorImpl.class, vendorId);

        if (vendor == _nullVendor) {
            return null;
        }

        if (vendor == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                vendor = (Vendor) session.get(VendorImpl.class,
                        Long.valueOf(vendorId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (vendor != null) {
                    cacheResult(vendor);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(VendorModelImpl.ENTITY_CACHE_ENABLED,
                        VendorImpl.class, vendorId, _nullVendor);
                }

                closeSession(session);
            }
        }

        return vendor;
    }

    /**
     * Returns all the vendors.
     *
     * @return the vendors
     * @throws SystemException if a system exception occurred
     */
    public List<Vendor> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vendors.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of vendors
     * @param end the upper bound of the range of vendors (not inclusive)
     * @return the range of vendors
     * @throws SystemException if a system exception occurred
     */
    public List<Vendor> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the vendors.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of vendors
     * @param end the upper bound of the range of vendors (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of vendors
     * @throws SystemException if a system exception occurred
     */
    public List<Vendor> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Vendor> list = (List<Vendor>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_VENDOR);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_VENDOR;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<Vendor>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<Vendor>) QueryUtil.list(q, getDialect(),
                            start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the vendors from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (Vendor vendor : findAll()) {
            remove(vendor);
        }
    }

    /**
     * Returns the number of vendors.
     *
     * @return the number of vendors
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_VENDOR);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the vendor persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.Vendor")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Vendor>> listenersList = new ArrayList<ModelListener<Vendor>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<Vendor>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(VendorImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
