package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchCostCenterException;
import com.hp.omp.model.CostCenter;
import com.hp.omp.model.impl.CostCenterImpl;
import com.hp.omp.model.impl.CostCenterModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the cost center service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see CostCenterPersistence
 * @see CostCenterUtil
 * @generated
 */
public class CostCenterPersistenceImpl extends BasePersistenceImpl<CostCenter>
    implements CostCenterPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link CostCenterUtil} to access the cost center persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = CostCenterImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(CostCenterModelImpl.ENTITY_CACHE_ENABLED,
            CostCenterModelImpl.FINDER_CACHE_ENABLED, CostCenterImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(CostCenterModelImpl.ENTITY_CACHE_ENABLED,
            CostCenterModelImpl.FINDER_CACHE_ENABLED, CostCenterImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(CostCenterModelImpl.ENTITY_CACHE_ENABLED,
            CostCenterModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_COSTCENTER = "SELECT costCenter FROM CostCenter costCenter";
    private static final String _SQL_COUNT_COSTCENTER = "SELECT COUNT(costCenter) FROM CostCenter costCenter";
    private static final String _ORDER_BY_ENTITY_ALIAS = "costCenter.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No CostCenter exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(CostCenterPersistenceImpl.class);
    private static CostCenter _nullCostCenter = new CostCenterImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<CostCenter> toCacheModel() {
                return _nullCostCenterCacheModel;
            }
        };

    private static CacheModel<CostCenter> _nullCostCenterCacheModel = new CacheModel<CostCenter>() {
            public CostCenter toEntityModel() {
                return _nullCostCenter;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the cost center in the entity cache if it is enabled.
     *
     * @param costCenter the cost center
     */
    public void cacheResult(CostCenter costCenter) {
        EntityCacheUtil.putResult(CostCenterModelImpl.ENTITY_CACHE_ENABLED,
            CostCenterImpl.class, costCenter.getPrimaryKey(), costCenter);

        costCenter.resetOriginalValues();
    }

    /**
     * Caches the cost centers in the entity cache if it is enabled.
     *
     * @param costCenters the cost centers
     */
    public void cacheResult(List<CostCenter> costCenters) {
        for (CostCenter costCenter : costCenters) {
            if (EntityCacheUtil.getResult(
                        CostCenterModelImpl.ENTITY_CACHE_ENABLED,
                        CostCenterImpl.class, costCenter.getPrimaryKey()) == null) {
                cacheResult(costCenter);
            } else {
                costCenter.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all cost centers.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(CostCenterImpl.class.getName());
        }

        EntityCacheUtil.clearCache(CostCenterImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the cost center.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(CostCenter costCenter) {
        EntityCacheUtil.removeResult(CostCenterModelImpl.ENTITY_CACHE_ENABLED,
            CostCenterImpl.class, costCenter.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<CostCenter> costCenters) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (CostCenter costCenter : costCenters) {
            EntityCacheUtil.removeResult(CostCenterModelImpl.ENTITY_CACHE_ENABLED,
                CostCenterImpl.class, costCenter.getPrimaryKey());
        }
    }

    /**
     * Creates a new cost center with the primary key. Does not add the cost center to the database.
     *
     * @param costCenterId the primary key for the new cost center
     * @return the new cost center
     */
    public CostCenter create(long costCenterId) {
        CostCenter costCenter = new CostCenterImpl();

        costCenter.setNew(true);
        costCenter.setPrimaryKey(costCenterId);

        return costCenter;
    }

    /**
     * Removes the cost center with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param costCenterId the primary key of the cost center
     * @return the cost center that was removed
     * @throws com.hp.omp.NoSuchCostCenterException if a cost center with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public CostCenter remove(long costCenterId)
        throws NoSuchCostCenterException, SystemException {
        return remove(Long.valueOf(costCenterId));
    }

    /**
     * Removes the cost center with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the cost center
     * @return the cost center that was removed
     * @throws com.hp.omp.NoSuchCostCenterException if a cost center with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CostCenter remove(Serializable primaryKey)
        throws NoSuchCostCenterException, SystemException {
        Session session = null;

        try {
            session = openSession();

            CostCenter costCenter = (CostCenter) session.get(CostCenterImpl.class,
                    primaryKey);

            if (costCenter == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchCostCenterException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(costCenter);
        } catch (NoSuchCostCenterException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected CostCenter removeImpl(CostCenter costCenter)
        throws SystemException {
        costCenter = toUnwrappedModel(costCenter);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, costCenter);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(costCenter);

        return costCenter;
    }

    @Override
    public CostCenter updateImpl(com.hp.omp.model.CostCenter costCenter,
        boolean merge) throws SystemException {
        costCenter = toUnwrappedModel(costCenter);

        boolean isNew = costCenter.isNew();

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, costCenter, merge);

            costCenter.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(CostCenterModelImpl.ENTITY_CACHE_ENABLED,
            CostCenterImpl.class, costCenter.getPrimaryKey(), costCenter);

        return costCenter;
    }

    protected CostCenter toUnwrappedModel(CostCenter costCenter) {
        if (costCenter instanceof CostCenterImpl) {
            return costCenter;
        }

        CostCenterImpl costCenterImpl = new CostCenterImpl();

        costCenterImpl.setNew(costCenter.isNew());
        costCenterImpl.setPrimaryKey(costCenter.getPrimaryKey());

        costCenterImpl.setCostCenterId(costCenter.getCostCenterId());
        costCenterImpl.setCostCenterName(costCenter.getCostCenterName());
        costCenterImpl.setDescription(costCenter.getDescription());
        costCenterImpl.setDisplay(costCenter.isDisplay());
        costCenterImpl.setGruppoUsers(costCenter.getGruppoUsers());

        return costCenterImpl;
    }

    /**
     * Returns the cost center with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the cost center
     * @return the cost center
     * @throws com.liferay.portal.NoSuchModelException if a cost center with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CostCenter findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the cost center with the primary key or throws a {@link com.hp.omp.NoSuchCostCenterException} if it could not be found.
     *
     * @param costCenterId the primary key of the cost center
     * @return the cost center
     * @throws com.hp.omp.NoSuchCostCenterException if a cost center with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public CostCenter findByPrimaryKey(long costCenterId)
        throws NoSuchCostCenterException, SystemException {
        CostCenter costCenter = fetchByPrimaryKey(costCenterId);

        if (costCenter == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + costCenterId);
            }

            throw new NoSuchCostCenterException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                costCenterId);
        }

        return costCenter;
    }

    /**
     * Returns the cost center with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the cost center
     * @return the cost center, or <code>null</code> if a cost center with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public CostCenter fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the cost center with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param costCenterId the primary key of the cost center
     * @return the cost center, or <code>null</code> if a cost center with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public CostCenter fetchByPrimaryKey(long costCenterId)
        throws SystemException {
        CostCenter costCenter = (CostCenter) EntityCacheUtil.getResult(CostCenterModelImpl.ENTITY_CACHE_ENABLED,
                CostCenterImpl.class, costCenterId);

        if (costCenter == _nullCostCenter) {
            return null;
        }

        if (costCenter == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                costCenter = (CostCenter) session.get(CostCenterImpl.class,
                        Long.valueOf(costCenterId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (costCenter != null) {
                    cacheResult(costCenter);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(CostCenterModelImpl.ENTITY_CACHE_ENABLED,
                        CostCenterImpl.class, costCenterId, _nullCostCenter);
                }

                closeSession(session);
            }
        }

        return costCenter;
    }

    /**
     * Returns all the cost centers.
     *
     * @return the cost centers
     * @throws SystemException if a system exception occurred
     */
    public List<CostCenter> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the cost centers.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of cost centers
     * @param end the upper bound of the range of cost centers (not inclusive)
     * @return the range of cost centers
     * @throws SystemException if a system exception occurred
     */
    public List<CostCenter> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the cost centers.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of cost centers
     * @param end the upper bound of the range of cost centers (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of cost centers
     * @throws SystemException if a system exception occurred
     */
    public List<CostCenter> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<CostCenter> list = (List<CostCenter>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_COSTCENTER);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_COSTCENTER;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<CostCenter>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<CostCenter>) QueryUtil.list(q, getDialect(),
                            start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the cost centers from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (CostCenter costCenter : findAll()) {
            remove(costCenter);
        }
    }

    /**
     * Returns the number of cost centers.
     *
     * @return the number of cost centers
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_COSTCENTER);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the cost center persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.CostCenter")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<CostCenter>> listenersList = new ArrayList<ModelListener<CostCenter>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<CostCenter>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(CostCenterImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
