package com.hp.omp.service.impl;

import com.hp.omp.service.base.ODNPCodeLocalServiceBaseImpl;

/**
 * The implementation of the o d n p code local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.ODNPCodeLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Ahmed Kamel
 * @see com.hp.omp.service.base.ODNPCodeLocalServiceBaseImpl
 * @see com.hp.omp.service.ODNPCodeLocalServiceUtil
 */
public class ODNPCodeLocalServiceImpl extends ODNPCodeLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.hp.omp.service.ODNPCodeLocalServiceUtil} to access the o d n p code local service.
     */
}
