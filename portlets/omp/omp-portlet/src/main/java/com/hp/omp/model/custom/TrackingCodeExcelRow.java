package com.hp.omp.model.custom;

public class TrackingCodeExcelRow {

	private String trackingCode;
	private String trackingDescription;
	private String project;
	private String subProject;
	private String budgetCategory;
	private String budgetSubCategory;
	private String macroDriver;
	private String microDriver;
	private String odnp;
	private String wbsCode;
	public String getTrackingCode() {
		return trackingCode;
	}
	public void setTrackingCode(String trackingCode) {
		this.trackingCode = trackingCode;
	}
	public String getTrackingDescription() {
		return trackingDescription;
	}
	public void setTrackingDescription(String trackingDescription) {
		this.trackingDescription = trackingDescription;
	}
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	public String getSubProject() {
		return subProject;
	}
	public void setSubProject(String subProject) {
		this.subProject = subProject;
	}
	public String getBudgetCategory() {
		return budgetCategory;
	}
	public void setBudgetCategory(String budgetCategory) {
		this.budgetCategory = budgetCategory;
	}
	public String getBudgetSubCategory() {
		return budgetSubCategory;
	}
	public void setBudgetSubCategory(String budgetSubCategory) {
		this.budgetSubCategory = budgetSubCategory;
	}
	public String getMacroDriver() {
		return macroDriver;
	}
	public void setMacroDriver(String macroDriver) {
		this.macroDriver = macroDriver;
	}
	public String getMicroDriver() {
		return microDriver;
	}
	public void setMicroDriver(String microDriver) {
		this.microDriver = microDriver;
	}
	public String getOdnp() {
		return odnp;
	}
	public void setOdnp(String odnp) {
		this.odnp = odnp;
	}
	public String getWbsCode() {
		return wbsCode;
	}
	public void setWbsCode(String wbsCode) {
		this.wbsCode = wbsCode;
	}
	
	
	
	
}
