package com.hp.omp.model.impl;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.hp.omp.model.ExcelReport;
import com.hp.omp.model.ExcelStyler;
import com.hp.omp.model.ExcelTable;
import com.hp.omp.model.custom.ExcelTableRow;
import com.hp.omp.model.custom.OmpFormatterUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

public class ExcelReportImpl implements ExcelReport {
	
	private static Log _log = LogFactoryUtil.getLog(ExcelReportImpl.class);
	
	private List<ExcelTable> tables;
	private Workbook workbook;
	private ExcelStyler styler;
	
	private DataFormatter doubleFormatter;
	
	public ExcelReportImpl(){
		this.tables = new ArrayList<ExcelTable>();
		this.workbook = new HSSFWorkbook();
	}
	
	public ExcelReportImpl(ExcelStyler styler){
		this();
		this.setStyler(styler);
	}
	
	public void addExcelTable(ExcelTable excelTable, int order){
		this.tables.add(order,excelTable);
	}
	
	public ExcelStyler getStyler() {
		return styler;
	}

	public void setStyler(ExcelStyler styler) {
		this.styler = styler;
	}
	
	public Workbook createExelWorkBook(){
		if(tables == null || tables.isEmpty()){
			return null;
		}
		for (ExcelTable excelTable : tables) {
			Sheet sheet = createSheet(workbook, excelTable.getTitle());
			sheet.createFreezePane(0, 1);
			final int rowIndex = addHeaderRow(sheet, 0, excelTable);
			addTableRows(sheet, rowIndex, excelTable);
		}
		return workbook;
	}
	
	private Sheet createSheet(final Workbook workbook, final String title) {
		final Sheet sheet = workbook.createSheet(title);
		sheet.setFitToPage(true);
		sheet.setHorizontallyCenter(true);
		final PrintSetup printSetup = sheet.getPrintSetup();
		printSetup.setLandscape(true);
		sheet.setAutobreaks(true);
		printSetup.setFitHeight((short) 1);
		printSetup.setFitWidth((short) 1);
		return sheet;
	}
	
	private int addHeaderRow(final Sheet sheet, final int rowIndex, ExcelTable table) {
		final CellStyle headerCellStyle = styler.getHeaderCellStyle();
		int tableRowIndex = rowIndex;
		final Row headerRow = sheet.createRow(tableRowIndex);
		int columnIndex = 0;
		for (Object headerElement : table.getHeader()) {
			createExcelCell(headerRow, columnIndex, headerElement.toString(),
					headerCellStyle);
			sheet.autoSizeColumn(columnIndex);
			columnIndex++;
		}
		tableRowIndex++;
		return tableRowIndex;
	}
	
	private int addTableRows(final Sheet sheet, final int rowIndex, ExcelTable table) {
		final CellStyle oddCellStyle = styler.getOddCellStyle();
		final CellStyle evenCellStyle =styler.getEvenCellStyle();
		final CellStyle evenNumericCellStyle = styler.getEvenNumericCellStyle();
		final CellStyle oddNumericCellStyle = styler.getOddNumericCellStyle();
		CellStyle cellStyle = null;
		CellStyle cellNumericStyle = null;
		int tableRowIndex = rowIndex;
		int headerSize = table.getHeader().size();
		for (ExcelTableRow excelRow : table.getData()) {
			final int rowcellsCount = Math.min(excelRow.getTotalNumberFields(),
					headerSize);
			final Row tableRow = sheet.createRow(tableRowIndex);
			if (tableRowIndex % 2 == 0) {
				cellStyle = evenCellStyle;
				cellNumericStyle = evenNumericCellStyle;
			} else {
				cellStyle = oddCellStyle;
				cellNumericStyle = oddNumericCellStyle;
			}
			for (int rowCellIndex = 0; rowCellIndex < rowcellsCount; rowCellIndex++) {
				final Object cellObject = excelRow.getFieldAtIndex(rowCellIndex);
				if(cellObject instanceof Double){
					Double cellValueDouble = (Double) cellObject;
					createExcelCell(tableRow, rowCellIndex, cellValueDouble ,
							cellNumericStyle);
				}else{
					String cellValueString = cellObject.toString();
					createExcelCell(tableRow, rowCellIndex, cellValueString ,
							cellStyle);
				}
			}
			
			tableRowIndex++;
			table.setProcessedRows(tableRowIndex - rowIndex);
			
		}
//		for (int colIndex = 0; colIndex < headerSize; colIndex++) {
//			sheet.autoSizeColumn(colIndex);
//		}
		return tableRowIndex;
	}


	private Cell createExcelCell(final Row row, final int columnIndex,
			final String text, final CellStyle cellStyle) {
		final Cell cell = row.createCell(columnIndex);
		cell.setCellValue(text);
		cell.setCellStyle(cellStyle);
		debug("cell created with value: "+text);
		return cell;
	}
	
	private Cell createExcelCell(final Row row, final int columnIndex,
			final Double cellValue, final CellStyle cellStyle) {
		final Cell cell = row.createCell(columnIndex);
		cell.setCellValue(cellValue);
		cell.setCellStyle(cellStyle);
		cell.setCellType(Cell.CELL_TYPE_NUMERIC);
		debug("cell created with value: "+cellValue);
		return cell;
	}

	public Workbook getWorkBook() {
		return workbook;
	}
	
	private static void debug(String msg){
		if(_log.isDebugEnabled()){
			_log.debug(msg);
		}
	}

	public int getTotalRowsProcessed() {
		int total = 0;
		for (ExcelTable excelTable : tables) {
			total += excelTable.getProcessedRows();
		}
		return total;
	}
	
}
