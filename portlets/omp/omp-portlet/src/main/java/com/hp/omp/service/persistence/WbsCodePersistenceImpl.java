package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchWbsCodeException;
import com.hp.omp.model.WbsCode;
import com.hp.omp.model.impl.WbsCodeImpl;
import com.hp.omp.model.impl.WbsCodeModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the wbs code service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see WbsCodePersistence
 * @see WbsCodeUtil
 * @generated
 */
public class WbsCodePersistenceImpl extends BasePersistenceImpl<WbsCode>
    implements WbsCodePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link WbsCodeUtil} to access the wbs code persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = WbsCodeImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(WbsCodeModelImpl.ENTITY_CACHE_ENABLED,
            WbsCodeModelImpl.FINDER_CACHE_ENABLED, WbsCodeImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(WbsCodeModelImpl.ENTITY_CACHE_ENABLED,
            WbsCodeModelImpl.FINDER_CACHE_ENABLED, WbsCodeImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(WbsCodeModelImpl.ENTITY_CACHE_ENABLED,
            WbsCodeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_WBSCODE = "SELECT wbsCode FROM WbsCode wbsCode";
    private static final String _SQL_COUNT_WBSCODE = "SELECT COUNT(wbsCode) FROM WbsCode wbsCode";
    private static final String _ORDER_BY_ENTITY_ALIAS = "wbsCode.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No WbsCode exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(WbsCodePersistenceImpl.class);
    private static WbsCode _nullWbsCode = new WbsCodeImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<WbsCode> toCacheModel() {
                return _nullWbsCodeCacheModel;
            }
        };

    private static CacheModel<WbsCode> _nullWbsCodeCacheModel = new CacheModel<WbsCode>() {
            public WbsCode toEntityModel() {
                return _nullWbsCode;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the wbs code in the entity cache if it is enabled.
     *
     * @param wbsCode the wbs code
     */
    public void cacheResult(WbsCode wbsCode) {
        EntityCacheUtil.putResult(WbsCodeModelImpl.ENTITY_CACHE_ENABLED,
            WbsCodeImpl.class, wbsCode.getPrimaryKey(), wbsCode);

        wbsCode.resetOriginalValues();
    }

    /**
     * Caches the wbs codes in the entity cache if it is enabled.
     *
     * @param wbsCodes the wbs codes
     */
    public void cacheResult(List<WbsCode> wbsCodes) {
        for (WbsCode wbsCode : wbsCodes) {
            if (EntityCacheUtil.getResult(
                        WbsCodeModelImpl.ENTITY_CACHE_ENABLED,
                        WbsCodeImpl.class, wbsCode.getPrimaryKey()) == null) {
                cacheResult(wbsCode);
            } else {
                wbsCode.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all wbs codes.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(WbsCodeImpl.class.getName());
        }

        EntityCacheUtil.clearCache(WbsCodeImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the wbs code.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(WbsCode wbsCode) {
        EntityCacheUtil.removeResult(WbsCodeModelImpl.ENTITY_CACHE_ENABLED,
            WbsCodeImpl.class, wbsCode.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<WbsCode> wbsCodes) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (WbsCode wbsCode : wbsCodes) {
            EntityCacheUtil.removeResult(WbsCodeModelImpl.ENTITY_CACHE_ENABLED,
                WbsCodeImpl.class, wbsCode.getPrimaryKey());
        }
    }

    /**
     * Creates a new wbs code with the primary key. Does not add the wbs code to the database.
     *
     * @param wbsCodeId the primary key for the new wbs code
     * @return the new wbs code
     */
    public WbsCode create(long wbsCodeId) {
        WbsCode wbsCode = new WbsCodeImpl();

        wbsCode.setNew(true);
        wbsCode.setPrimaryKey(wbsCodeId);

        return wbsCode;
    }

    /**
     * Removes the wbs code with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param wbsCodeId the primary key of the wbs code
     * @return the wbs code that was removed
     * @throws com.hp.omp.NoSuchWbsCodeException if a wbs code with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public WbsCode remove(long wbsCodeId)
        throws NoSuchWbsCodeException, SystemException {
        return remove(Long.valueOf(wbsCodeId));
    }

    /**
     * Removes the wbs code with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the wbs code
     * @return the wbs code that was removed
     * @throws com.hp.omp.NoSuchWbsCodeException if a wbs code with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public WbsCode remove(Serializable primaryKey)
        throws NoSuchWbsCodeException, SystemException {
        Session session = null;

        try {
            session = openSession();

            WbsCode wbsCode = (WbsCode) session.get(WbsCodeImpl.class,
                    primaryKey);

            if (wbsCode == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchWbsCodeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(wbsCode);
        } catch (NoSuchWbsCodeException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected WbsCode removeImpl(WbsCode wbsCode) throws SystemException {
        wbsCode = toUnwrappedModel(wbsCode);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, wbsCode);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(wbsCode);

        return wbsCode;
    }

    @Override
    public WbsCode updateImpl(com.hp.omp.model.WbsCode wbsCode, boolean merge)
        throws SystemException {
        wbsCode = toUnwrappedModel(wbsCode);

        boolean isNew = wbsCode.isNew();

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, wbsCode, merge);

            wbsCode.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(WbsCodeModelImpl.ENTITY_CACHE_ENABLED,
            WbsCodeImpl.class, wbsCode.getPrimaryKey(), wbsCode);

        return wbsCode;
    }

    protected WbsCode toUnwrappedModel(WbsCode wbsCode) {
        if (wbsCode instanceof WbsCodeImpl) {
            return wbsCode;
        }

        WbsCodeImpl wbsCodeImpl = new WbsCodeImpl();

        wbsCodeImpl.setNew(wbsCode.isNew());
        wbsCodeImpl.setPrimaryKey(wbsCode.getPrimaryKey());

        wbsCodeImpl.setWbsCodeId(wbsCode.getWbsCodeId());
        wbsCodeImpl.setWbsCodeName(wbsCode.getWbsCodeName());
        wbsCodeImpl.setDescription(wbsCode.getDescription());
        wbsCodeImpl.setDisplay(wbsCode.isDisplay());

        return wbsCodeImpl;
    }

    /**
     * Returns the wbs code with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the wbs code
     * @return the wbs code
     * @throws com.liferay.portal.NoSuchModelException if a wbs code with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public WbsCode findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the wbs code with the primary key or throws a {@link com.hp.omp.NoSuchWbsCodeException} if it could not be found.
     *
     * @param wbsCodeId the primary key of the wbs code
     * @return the wbs code
     * @throws com.hp.omp.NoSuchWbsCodeException if a wbs code with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public WbsCode findByPrimaryKey(long wbsCodeId)
        throws NoSuchWbsCodeException, SystemException {
        WbsCode wbsCode = fetchByPrimaryKey(wbsCodeId);

        if (wbsCode == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + wbsCodeId);
            }

            throw new NoSuchWbsCodeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                wbsCodeId);
        }

        return wbsCode;
    }

    /**
     * Returns the wbs code with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the wbs code
     * @return the wbs code, or <code>null</code> if a wbs code with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public WbsCode fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the wbs code with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param wbsCodeId the primary key of the wbs code
     * @return the wbs code, or <code>null</code> if a wbs code with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public WbsCode fetchByPrimaryKey(long wbsCodeId) throws SystemException {
        WbsCode wbsCode = (WbsCode) EntityCacheUtil.getResult(WbsCodeModelImpl.ENTITY_CACHE_ENABLED,
                WbsCodeImpl.class, wbsCodeId);

        if (wbsCode == _nullWbsCode) {
            return null;
        }

        if (wbsCode == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                wbsCode = (WbsCode) session.get(WbsCodeImpl.class,
                        Long.valueOf(wbsCodeId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (wbsCode != null) {
                    cacheResult(wbsCode);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(WbsCodeModelImpl.ENTITY_CACHE_ENABLED,
                        WbsCodeImpl.class, wbsCodeId, _nullWbsCode);
                }

                closeSession(session);
            }
        }

        return wbsCode;
    }

    /**
     * Returns all the wbs codes.
     *
     * @return the wbs codes
     * @throws SystemException if a system exception occurred
     */
    public List<WbsCode> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the wbs codes.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of wbs codes
     * @param end the upper bound of the range of wbs codes (not inclusive)
     * @return the range of wbs codes
     * @throws SystemException if a system exception occurred
     */
    public List<WbsCode> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the wbs codes.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of wbs codes
     * @param end the upper bound of the range of wbs codes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of wbs codes
     * @throws SystemException if a system exception occurred
     */
    public List<WbsCode> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<WbsCode> list = (List<WbsCode>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_WBSCODE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_WBSCODE;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<WbsCode>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<WbsCode>) QueryUtil.list(q, getDialect(),
                            start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the wbs codes from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (WbsCode wbsCode : findAll()) {
            remove(wbsCode);
        }
    }

    /**
     * Returns the number of wbs codes.
     *
     * @return the number of wbs codes
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_WBSCODE);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the wbs code persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.WbsCode")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<WbsCode>> listenersList = new ArrayList<ModelListener<WbsCode>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<WbsCode>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(WbsCodeImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
