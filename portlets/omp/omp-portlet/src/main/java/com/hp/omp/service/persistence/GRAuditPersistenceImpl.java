package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchGRAuditException;
import com.hp.omp.model.GRAudit;
import com.hp.omp.model.impl.GRAuditImpl;
import com.hp.omp.model.impl.GRAuditModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the g r audit service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see GRAuditPersistence
 * @see GRAuditUtil
 * @generated
 */
public class GRAuditPersistenceImpl extends BasePersistenceImpl<GRAudit>
    implements GRAuditPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link GRAuditUtil} to access the g r audit persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = GRAuditImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(GRAuditModelImpl.ENTITY_CACHE_ENABLED,
            GRAuditModelImpl.FINDER_CACHE_ENABLED, GRAuditImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(GRAuditModelImpl.ENTITY_CACHE_ENABLED,
            GRAuditModelImpl.FINDER_CACHE_ENABLED, GRAuditImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(GRAuditModelImpl.ENTITY_CACHE_ENABLED,
            GRAuditModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_GRAUDIT = "SELECT grAudit FROM GRAudit grAudit";
    private static final String _SQL_COUNT_GRAUDIT = "SELECT COUNT(grAudit) FROM GRAudit grAudit";
    private static final String _ORDER_BY_ENTITY_ALIAS = "grAudit.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No GRAudit exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(GRAuditPersistenceImpl.class);
    private static GRAudit _nullGRAudit = new GRAuditImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<GRAudit> toCacheModel() {
                return _nullGRAuditCacheModel;
            }
        };

    private static CacheModel<GRAudit> _nullGRAuditCacheModel = new CacheModel<GRAudit>() {
            public GRAudit toEntityModel() {
                return _nullGRAudit;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the g r audit in the entity cache if it is enabled.
     *
     * @param grAudit the g r audit
     */
    public void cacheResult(GRAudit grAudit) {
        EntityCacheUtil.putResult(GRAuditModelImpl.ENTITY_CACHE_ENABLED,
            GRAuditImpl.class, grAudit.getPrimaryKey(), grAudit);

        grAudit.resetOriginalValues();
    }

    /**
     * Caches the g r audits in the entity cache if it is enabled.
     *
     * @param grAudits the g r audits
     */
    public void cacheResult(List<GRAudit> grAudits) {
        for (GRAudit grAudit : grAudits) {
            if (EntityCacheUtil.getResult(
                        GRAuditModelImpl.ENTITY_CACHE_ENABLED,
                        GRAuditImpl.class, grAudit.getPrimaryKey()) == null) {
                cacheResult(grAudit);
            } else {
                grAudit.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all g r audits.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(GRAuditImpl.class.getName());
        }

        EntityCacheUtil.clearCache(GRAuditImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the g r audit.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(GRAudit grAudit) {
        EntityCacheUtil.removeResult(GRAuditModelImpl.ENTITY_CACHE_ENABLED,
            GRAuditImpl.class, grAudit.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<GRAudit> grAudits) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (GRAudit grAudit : grAudits) {
            EntityCacheUtil.removeResult(GRAuditModelImpl.ENTITY_CACHE_ENABLED,
                GRAuditImpl.class, grAudit.getPrimaryKey());
        }
    }

    /**
     * Creates a new g r audit with the primary key. Does not add the g r audit to the database.
     *
     * @param auditId the primary key for the new g r audit
     * @return the new g r audit
     */
    public GRAudit create(long auditId) {
        GRAudit grAudit = new GRAuditImpl();

        grAudit.setNew(true);
        grAudit.setPrimaryKey(auditId);

        return grAudit;
    }

    /**
     * Removes the g r audit with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param auditId the primary key of the g r audit
     * @return the g r audit that was removed
     * @throws com.hp.omp.NoSuchGRAuditException if a g r audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public GRAudit remove(long auditId)
        throws NoSuchGRAuditException, SystemException {
        return remove(Long.valueOf(auditId));
    }

    /**
     * Removes the g r audit with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the g r audit
     * @return the g r audit that was removed
     * @throws com.hp.omp.NoSuchGRAuditException if a g r audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public GRAudit remove(Serializable primaryKey)
        throws NoSuchGRAuditException, SystemException {
        Session session = null;

        try {
            session = openSession();

            GRAudit grAudit = (GRAudit) session.get(GRAuditImpl.class,
                    primaryKey);

            if (grAudit == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchGRAuditException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(grAudit);
        } catch (NoSuchGRAuditException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected GRAudit removeImpl(GRAudit grAudit) throws SystemException {
        grAudit = toUnwrappedModel(grAudit);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, grAudit);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(grAudit);

        return grAudit;
    }

    @Override
    public GRAudit updateImpl(com.hp.omp.model.GRAudit grAudit, boolean merge)
        throws SystemException {
        grAudit = toUnwrappedModel(grAudit);

        boolean isNew = grAudit.isNew();

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, grAudit, merge);

            grAudit.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(GRAuditModelImpl.ENTITY_CACHE_ENABLED,
            GRAuditImpl.class, grAudit.getPrimaryKey(), grAudit);

        return grAudit;
    }

    protected GRAudit toUnwrappedModel(GRAudit grAudit) {
        if (grAudit instanceof GRAuditImpl) {
            return grAudit;
        }

        GRAuditImpl grAuditImpl = new GRAuditImpl();

        grAuditImpl.setNew(grAudit.isNew());
        grAuditImpl.setPrimaryKey(grAudit.getPrimaryKey());

        grAuditImpl.setAuditId(grAudit.getAuditId());
        grAuditImpl.setGoodReceiptId(grAudit.getGoodReceiptId());
        grAuditImpl.setStatus(grAudit.getStatus());
        grAuditImpl.setUserId(grAudit.getUserId());
        grAuditImpl.setChangeDate(grAudit.getChangeDate());

        return grAuditImpl;
    }

    /**
     * Returns the g r audit with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the g r audit
     * @return the g r audit
     * @throws com.liferay.portal.NoSuchModelException if a g r audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public GRAudit findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the g r audit with the primary key or throws a {@link com.hp.omp.NoSuchGRAuditException} if it could not be found.
     *
     * @param auditId the primary key of the g r audit
     * @return the g r audit
     * @throws com.hp.omp.NoSuchGRAuditException if a g r audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public GRAudit findByPrimaryKey(long auditId)
        throws NoSuchGRAuditException, SystemException {
        GRAudit grAudit = fetchByPrimaryKey(auditId);

        if (grAudit == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + auditId);
            }

            throw new NoSuchGRAuditException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                auditId);
        }

        return grAudit;
    }

    /**
     * Returns the g r audit with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the g r audit
     * @return the g r audit, or <code>null</code> if a g r audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public GRAudit fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the g r audit with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param auditId the primary key of the g r audit
     * @return the g r audit, or <code>null</code> if a g r audit with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public GRAudit fetchByPrimaryKey(long auditId) throws SystemException {
        GRAudit grAudit = (GRAudit) EntityCacheUtil.getResult(GRAuditModelImpl.ENTITY_CACHE_ENABLED,
                GRAuditImpl.class, auditId);

        if (grAudit == _nullGRAudit) {
            return null;
        }

        if (grAudit == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                grAudit = (GRAudit) session.get(GRAuditImpl.class,
                        Long.valueOf(auditId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (grAudit != null) {
                    cacheResult(grAudit);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(GRAuditModelImpl.ENTITY_CACHE_ENABLED,
                        GRAuditImpl.class, auditId, _nullGRAudit);
                }

                closeSession(session);
            }
        }

        return grAudit;
    }

    /**
     * Returns all the g r audits.
     *
     * @return the g r audits
     * @throws SystemException if a system exception occurred
     */
    public List<GRAudit> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the g r audits.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of g r audits
     * @param end the upper bound of the range of g r audits (not inclusive)
     * @return the range of g r audits
     * @throws SystemException if a system exception occurred
     */
    public List<GRAudit> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the g r audits.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of g r audits
     * @param end the upper bound of the range of g r audits (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of g r audits
     * @throws SystemException if a system exception occurred
     */
    public List<GRAudit> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<GRAudit> list = (List<GRAudit>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_GRAUDIT);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_GRAUDIT;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<GRAudit>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<GRAudit>) QueryUtil.list(q, getDialect(),
                            start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the g r audits from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (GRAudit grAudit : findAll()) {
            remove(grAudit);
        }
    }

    /**
     * Returns the number of g r audits.
     *
     * @return the number of g r audits
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_GRAUDIT);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the g r audit persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.GRAudit")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<GRAudit>> listenersList = new ArrayList<ModelListener<GRAudit>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<GRAudit>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(GRAuditImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
