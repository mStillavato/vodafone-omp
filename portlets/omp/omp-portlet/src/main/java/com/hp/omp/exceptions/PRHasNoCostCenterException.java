package com.hp.omp.exceptions;



/**
 * @author Sami.basyoni
 */
public class PRHasNoCostCenterException extends Exception {

	public PRHasNoCostCenterException() {
		super();
	}

	public PRHasNoCostCenterException(String msg) {
		super(msg);
	}

	public PRHasNoCostCenterException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public PRHasNoCostCenterException(Throwable cause) {
		super(cause);
	}

}