package com.hp.omp.portlet;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.custom.CatalogoExportListDTO;
import com.hp.omp.model.custom.CatalogoListDTO;
import com.hp.omp.model.custom.LayoutNotNssCatalogoListDTO;
import com.hp.omp.model.custom.SearchCatalogoDTO;
import com.hp.omp.service.CatalogoLocalServiceUtil;
import com.hp.omp.service.LayoutNotNssCatalogoLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class CatalogoListPortlet extends MVCPortlet {

	public static final Logger logger = LoggerFactory.getLogger(CatalogoListPortlet.class);
	boolean created = false;

	@ProcessAction(name="clearExcelFile")
	public void clearExcelFile(ActionRequest actionRequest, ActionResponse actionResponse){
		logger.info("Inside in deleteExcelFile");
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String user = themeDisplay.getUser().getScreenName();
			LayoutNotNssCatalogoLocalServiceUtil.deleteLayoutNotNssCatalogo4User(user);

		} catch (SystemException e) {
			logger.error("Error in deleteExcelFile",e );
		}
	}
	
	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {

		String jspPage = "/html/catalogolist/catalogolist.jsp";
		
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay)resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
			String user = themeDisplay.getUser().getScreenName();
			
			Properties propsCatalogo = readProperties();
			
			String vendorNotView = null;
			if(propsCatalogo.getProperty("users").contains(themeDisplay.getUser().getEmailAddress())){
				vendorNotView = propsCatalogo.getProperty("vendors");
			} else {
				System.out.println("User Not In Black List");
			}
			
			//System.out.println(vendorNotView);
			
			if("writeExcelFile".equals(resourceRequest.getResourceID())){
				exportReport(resourceRequest, resourceResponse, user);
			}
			
			if("writeExcelCatalogo".equals(resourceRequest.getResourceID())){
				exportCatalogo(resourceRequest, resourceResponse, user);
			}
			
			Integer statusCode = ParamUtil.getInteger(resourceRequest, "status");	
			logger.debug("tab status = "+statusCode);
			Integer pageNumber = ParamUtil.getInteger(resourceRequest, "pageNumber", 1);

			//get search parameters from the request
			String vendor = ParamUtil.getString(resourceRequest, "vendor");
			String matCatLvl4 = ParamUtil.getString(resourceRequest, "matCatLvl4");
			String codMateriale = ParamUtil.getString(resourceRequest, "codMateriale");
			String codFornitore = ParamUtil.getString(resourceRequest, "codFornitore");
			String descMateriale = ParamUtil.getString(resourceRequest, "descMateriale");
			double prezzo = ParamUtil.getDouble(resourceRequest, "prezzo");
			boolean miniSeach = ParamUtil.getBoolean(resourceRequest, "isMini",true);
			String searchKeyWord = ParamUtil.getString(resourceRequest, "searchKeyWord");
			
			logger.debug("searchKeyWord = " + searchKeyWord);
			logger.debug("matCatLvl4 = " + matCatLvl4);
			
			//fill in SearchDTO with  search parameters
			SearchCatalogoDTO searchDTO = new SearchCatalogoDTO();
			searchDTO.setVendor(vendor);
			searchDTO.setMatCatLvl4(matCatLvl4);
			searchDTO.setCodMateriale(codMateriale);
			searchDTO.setCodFornitore(codFornitore);
			searchDTO.setDescMateriale(descMateriale);
			searchDTO.setPrezzo(prezzo);
			searchDTO.setPageNumber(pageNumber);
			searchDTO.setPageSize(10);
			searchDTO.setMiniSeach(miniSeach);
			searchDTO.setMiniSearchValue(searchKeyWord);
			searchDTO.setVendorNotView(vendorNotView);

			List<CatalogoListDTO> catalogoList = CatalogoLocalServiceUtil.getCatalogoList(searchDTO);
			Long totalCount = CatalogoLocalServiceUtil.getCatalogoListCount(searchDTO);

			logger.info("Result List count {}",totalCount);
			resourceRequest.setAttribute("numberOfPages", getTotalNumberOfPages(totalCount, 10));
			resourceRequest.setAttribute("pageNumber", pageNumber);
			resourceRequest.setAttribute("catalogoList", catalogoList);
			resourceRequest.setAttribute("searchKeyWord", searchKeyWord);
			resourceRequest.setAttribute("vendor", vendor);
			resourceRequest.setAttribute("matCatLvl4", matCatLvl4);
			resourceRequest.setAttribute("codMateriale", codMateriale);
			resourceRequest.setAttribute("codFornitore", codFornitore);
			resourceRequest.setAttribute("descMateriale", descMateriale);
			
			resourceRequest.setAttribute("numberOfPositions", 0);
			List<LayoutNotNssCatalogoListDTO> layoutNotNssCatalogoList = LayoutNotNssCatalogoLocalServiceUtil.getLayoutNotNssCatalogoList(user);
			if(layoutNotNssCatalogoList != null && layoutNotNssCatalogoList.size() > 0){
				resourceRequest.setAttribute("layoutNotNssCatalogoList", layoutNotNssCatalogoList);
				resourceRequest.setAttribute("numberOfPositions", layoutNotNssCatalogoList.size());
			}

			getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);

		} catch(Exception e) {
			logger.error("Error in get PRL for Enginner",e );
			SessionErrors.add(resourceRequest, "Unassgined-CostCenter");
		}
	}

	private Integer getTotalNumberOfPages(Long totalNubmerOfResult, Integer pageSize) {
		return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
	}

	private void exportReport(ResourceRequest resourceRequest, ResourceResponse resourceResponse, String user) {
		try {
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
			
			String fileName = "OMP_NotNssCatalog_"+formatter.format(date)+".xls";
			resourceResponse.setContentType("application/vnd.ms-excel");
			
			resourceResponse.addProperty(HttpHeaders.CONTENT_DISPOSITION,
					"attachment; filename=\""+fileName+"\"");			
			resourceResponse.addProperty(HttpHeaders.CACHE_CONTROL,"max-age=3600, must-revalidate");

			logger.debug("exporting.."+fileName);
			HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(resourceResponse);
			List<LayoutNotNssCatalogoListDTO> layoutNotNssCatalogoList = LayoutNotNssCatalogoLocalServiceUtil.getLayoutNotNssCatalogoList(user);

			CatalogoLocalServiceUtil.exportReportAsExcel(layoutNotNssCatalogoList, getReportHeader(), httpServletResponse.getOutputStream());
		} catch (Exception e) {
			logger.error("error in exportReport",e);
		}
	}
	
	private void exportCatalogo(ResourceRequest resourceRequest, ResourceResponse resourceResponse, String user) {
		logger.info("inside private void exportCatalogo(...");
		
		try {
			boolean miniSeach = ParamUtil.getBoolean(resourceRequest, "isMini",true);
			String searchKeyWord = ParamUtil.getString(resourceRequest, "searchKeyWord");
			String vendor = ParamUtil.getString(resourceRequest, "vendor");
			String matCatLvl4 = ParamUtil.getString(resourceRequest, "matCatLvl4");
			String codMateriale = ParamUtil.getString(resourceRequest, "codMateriale");
			String codFornitore = ParamUtil.getString(resourceRequest, "codFornitore");
			String descMateriale = ParamUtil.getString(resourceRequest, "descMateriale");
			double prezzo = ParamUtil.getDouble(resourceRequest, "prezzo");
			
			SearchCatalogoDTO searchDTO = new SearchCatalogoDTO();
			searchDTO.setPageSize(10000);
			searchDTO.setGlobalSearchKeyWord(searchKeyWord);
			searchDTO.setMiniSeach(miniSeach);
			searchDTO.setMiniSearchValue(searchKeyWord);
			searchDTO.setVendor(vendor);
			searchDTO.setMatCatLvl4(matCatLvl4);
			searchDTO.setCodMateriale(codMateriale);
			searchDTO.setCodFornitore(codFornitore);
			searchDTO.setDescMateriale(descMateriale);
			searchDTO.setPrezzo(prezzo);
			
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
			
			String fileName = "OMP_Catalog_"+formatter.format(date)+".xls";
			resourceResponse.setContentType("application/vnd.ms-excel");
			
			resourceResponse.addProperty(HttpHeaders.CONTENT_DISPOSITION,
					"attachment; filename=\""+fileName+"\"");			
			resourceResponse.addProperty(HttpHeaders.CACHE_CONTROL,"max-age=3600, must-revalidate");

			logger.debug("exporting.."+fileName);
			HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(resourceResponse);
			List<CatalogoExportListDTO> catalogoList = CatalogoLocalServiceUtil.getExportCatalogoList(searchDTO);

			CatalogoLocalServiceUtil.exportCatalogoAsExcel(catalogoList, getCatalogoReportHeader(), httpServletResponse.getOutputStream());
		} catch (Exception e) {
			logger.error("error in exportReport",e);
		}
	}
	
	private List<Object> getReportHeader() {
		List<Object> header = new ArrayList<Object>();
		header.add("Line number");//0
		header.add("Material ID");//1
		header.add("Description");//2
		header.add("PO Quantity");//3
		header.add("Net Price");//4
		header.add("Currency");//5
		header.add("Delivery Date");//6
		header.add("Delivery Address");//7
		header.add("Item Text");//8
		header.add("Location EVO");//9
		header.add("Targa Tecnica");//10
		header.add("Vendor");//11
		header.add("IC Approval");//12
		header.add("Category Code");//13
		header.add("Offer Number");//14
		return header;
	}
	
	private List<Object> getCatalogoReportHeader() {
		List<Object> header = new ArrayList<Object>();
		
		header.add("Chiave Inforecord");	// 00
		header.add("Data Creaz");			// 01
		header.add("Data Fine Validita");	// 02
		header.add("Divisione");			// 03
		header.add("Flg Canc");				// 04
		header.add("Forn Fin Code");		// 05
		header.add("Fornitore Cod");		// 06
		header.add("Fornitore Desc");		// 07
		header.add("Mat Cat Lvl 2");		// 08
		header.add("Mat Cat Lvl 2 Desc");	// 09
		header.add("Mat Cat Lvl 4");		// 10
		header.add("Mat Cat Lvl 4 Desc");	// 11
		header.add("Mat Cod");				// 12
		header.add("Mat Code Forn Finale");	// 13
		header.add("Mat Desc");				// 14
		header.add("Prezzo");				// 15
		header.add("Purch Organiz");		// 16
		header.add("UM");					// 17
		header.add("UM Prezzo");			// 18
		header.add("UM Prezzo PO");			// 19
		header.add("Valuta");				// 20
		header.add("Upper Mat Desc");		// 21
		
		return header;
	}
	
	private static synchronized Properties readProperties(){
		String resourceName = "catalogo.properties";
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		
		Properties props = new Properties();
		try  {
			InputStream resourceStream = loader.getResourceAsStream(resourceName);
			props.load(resourceStream);
		    
		} catch (IOException e){
			System.out.println(e);
		}
		return props;
	}
}

