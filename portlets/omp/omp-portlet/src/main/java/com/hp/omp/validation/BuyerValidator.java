package com.hp.omp.validation;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.Buyer;
import com.hp.omp.service.BuyerLocalServiceUtil;
import com.liferay.portal.kernel.exception.SystemException;

public class BuyerValidator {
	private static final Logger logger = LoggerFactory.getLogger(BuyerValidator.class);

	public static boolean validateBuyer(Buyer buyer, List<String> errors) throws SystemException {
		if(emptyMandatoryField(buyer, errors)) {
			logger.debug("Validation error empty mandatory field");
			return false;
		}
		if(buyerNameExist(buyer)) {
			logger.debug("Validation error buyer name is exist");
			errors.add("This buyer name exist");
			return false;
		} 
		return true;
	}

	private static boolean buyerNameExist(Buyer buyer) {
		try {
			Buyer oldBuyer = BuyerLocalServiceUtil.getBuyerByName(buyer.getName());
			if(oldBuyer != null  && oldBuyer.getBuyerId() != buyer.getBuyerId() && oldBuyer.getGruppoUsers() != buyer.getGruppoUsers()) {
				return true;
			}
		} catch (SystemException e) {
			logger.error("Error in get buyer by name",e);
		}
		return false;
	}

	private static boolean emptyMandatoryField(Buyer buyer, List<String> errors) {
		if(buyer.getName() == null || "".equals(buyer.getName().trim())) {
			errors.add("Buyer name is mandatory");
			return true;
		}
		return false;
	}
}
