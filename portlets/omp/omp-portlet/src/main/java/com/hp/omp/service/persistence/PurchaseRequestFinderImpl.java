package com.hp.omp.service.persistence;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.custom.PositionStatus;
import com.hp.omp.model.custom.PrListDTO;
import com.hp.omp.model.custom.SearchDTO;
import com.hp.omp.model.custom.TipoPr;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class PurchaseRequestFinderImpl extends BasePersistenceImpl<PurchaseRequest> implements PurchaseRequestFinder {
	private static final Logger logger = LoggerFactory.getLogger(PurchaseRequestFinderImpl.class);


	public Long getPurchaseRequestCount(SearchDTO searchDTO,int roleCode, long userId) {
		Session session = null; 
		try {

			session = openSession();
			StringBuilder sql = new StringBuilder(CustomSQLUtil.get(GET_COUNT_ALL_PR));
			SQLQuery query = createSearchCriteria(searchDTO, roleCode, userId,
					session, sql);
			query.addScalar("COUNT_VALUE", Type.LONG);

			//query.set
			//query.setFirstResult(searchDTO.getfi)
			@SuppressWarnings("unchecked")
			List<Long> list = query.list();
			if(list != null && list.size() > 0) {
				return list.get(0);
			}
			return 0L;
		} finally {
			if(session != null) {
				closeSession(session);
			}
		}
	}




	public List<PrListDTO> getPurchaseRequestList(SearchDTO searchDTO,int roleCode, long userId){
		Session session = null; 
		try {
			session = openSession();
			StringBuilder sql = new StringBuilder(CustomSQLUtil.get(GET_ALL_PR));
			SQLQuery query = createSearchCriteria(searchDTO, roleCode, userId,
					session, sql);
			int start = (searchDTO.getPageNumber() - 1) * searchDTO.getPageSize();
			query.setFirstResult(start);
			query.setMaxResults(searchDTO.getPageSize());
			
			@SuppressWarnings("unused")
			List<Object[]> list = query.list();
			
			List<PrListDTO> prlist = new ArrayList<PrListDTO>();
			
			for(Object[] row:list) {
				PrListDTO prListDTO=new PrListDTO();
				prListDTO.setPurchaseRequestId(Long.valueOf(row[0].toString()));
				prListDTO.setScreenNameRequester(""+row[1]);
				prListDTO.setScreenNameReciever(""+row[2]);
				prListDTO.setVendorName(row[3]+"");
				prListDTO.setBudgetSubCategoryName(row[4]+""); 
				prListDTO.setTotalValue(row[5]+"");
				prListDTO.setFiscalYear(row[6]+"");
				prListDTO.setActivityDescription(row[7]+"");

				if("1".equals(row[8].toString())) {
					prListDTO.setAutomatic(true);
				}else {
					prListDTO.setAutomatic(false);
				}

				prListDTO.setStatus(Integer.valueOf(row[9]+""));
				prListDTO.setCurrency(row[10]+"");
				prListDTO.setCostCenterId(row[11]+"");

				prListDTO.setTipoPr(Integer.valueOf(row[13]+""));

				prlist.add(prListDTO);
			}
			return prlist;
			
		} finally {
			if(session != null) {
				closeSession(session);
			}
		}
	}

	public List<PrListDTO> prSearch(SearchDTO searchDTO,int roleCode, long userId){
		Session session = null; 
		try {

			session = openSession();
			StringBuilder sql = new StringBuilder(CustomSQLUtil.get(GET_ALL_PR));
			SQLQuery query = createPrSearchCriteria(searchDTO, roleCode, userId,
					session, sql);
			//query.set
			//query.setFirstResult(searchDTO.getfi)
			@SuppressWarnings("unused")
			List<Object[]> list = query.list();
			List<PrListDTO> prlist=new ArrayList<PrListDTO>();
			for(Object[] row:list) {
				PrListDTO prListDTO=new PrListDTO();
				prListDTO.setPurchaseRequestId(Long.valueOf(row[0].toString()));
				prListDTO.setScreenNameRequester(""+row[1]);
				prListDTO.setScreenNameReciever(""+row[2]);
				prListDTO.setVendorName(row[3]+"");
				prListDTO.setBudgetSubCategoryName(row[4]+""); 
				prListDTO.setTotalValue(row[5]+"");
				prListDTO.setFiscalYear(row[6]+"");
				prListDTO.setActivityDescription(row[7]+"");

				if("1".equals(row[8].toString())) {
					prListDTO.setAutomatic(true);
				}else {
					prListDTO.setAutomatic(false);
				}

				prListDTO.setStatus(Integer.valueOf(row[9]+""));
				prListDTO.setCurrency(row[10]+"");
				prListDTO.setCostCenterId(row[11]+"");
				prListDTO.setCostCenterName(row[12]+"");
				prListDTO.setTipoPr(Integer.valueOf(row[13]+""));

				prlist.add(prListDTO);

			}
			return prlist;
		} finally {
			if(session != null) {
				closeSession(session);
			}
		}
	}


	private SQLQuery createSearchCriteria(SearchDTO searchDTO, int roleCode,
			long userId, Session session, StringBuilder sql) {
		List<Object> parameterList = new ArrayList<Object>();

		boolean ccAllowedList = (OmpRoles.OMP_CONTROLLER.equals(OmpRoles.getStatus(roleCode)) 
				|| OmpRoles.OMP_ANTEX.equals(OmpRoles.getStatus(roleCode))
				|| OmpRoles.OMP_NDSO.equals(OmpRoles.getStatus(roleCode))
				|| OmpRoles.OMP_TC.equals(OmpRoles.getStatus(roleCode))
				|| OmpRoles.OMP_CONTROLLER_VBS.equals(OmpRoles.getStatus(roleCode))
				|| OmpRoles.OMP_ENG_VBS.equals(OmpRoles.getStatus(roleCode))
				|| OmpRoles.OMP_APPROVER_VBS.equals(OmpRoles.getStatus(roleCode))
				);

		if(!ccAllowedList) {
			sql.append(" and ccu.userId = ? ");
			parameterList.add(userId);
		}
		
		if(searchDTO.getTipoPr() != null && searchDTO.getTipoPr() == TipoPr.VBS.getCode()){
			sql.append(" and pr.tipoPr = 3 ");
		} else{
			sql.append(" and pr.tipoPr != 3 ");
		}

		return appendSearchCriteria(searchDTO, session, sql, parameterList, ccAllowedList);
	}

	private SQLQuery createPrSearchCriteria(SearchDTO searchDTO, int roleCode,
			long userId, Session session, StringBuilder sql) {
		List<Object> parameterList = new ArrayList<Object>();

		boolean ccAllowedList = (OmpRoles.OMP_CONTROLLER.equals(OmpRoles.getStatus(roleCode)) 
				|| OmpRoles.OMP_ANTEX.equals(OmpRoles.getStatus(roleCode))
				|| OmpRoles.OMP_NDSO.equals(OmpRoles.getStatus(roleCode))
				|| OmpRoles.OMP_TC.equals(OmpRoles.getStatus(roleCode))
				|| OmpRoles.OMP_CONTROLLER_VBS.equals(OmpRoles.getStatus(roleCode))
				|| OmpRoles.OMP_ENG_VBS.equals(OmpRoles.getStatus(roleCode))
				|| OmpRoles.OMP_APPROVER_VBS.equals(OmpRoles.getStatus(roleCode))
				);

		if(!ccAllowedList) {
			sql.append(" and ccu.userId = ? ");
			parameterList.add(userId);
		}

		sql.append(" and pr.purchaseRequestId = ?");
		parameterList.add(searchDTO.getPrSearchValue());

		SQLQuery query = session.createSQLQuery(sql.toString());
		for (int i = 0; i < parameterList.size(); i++) {
			query.setString(i, String.valueOf(parameterList.get(i)));
		}
		return query;
	}

	public List<PrListDTO> getAntexPurchaseRequestList(SearchDTO searchDTO){
		Session session = null; 
		try {

			session = openSession();
			StringBuilder sql = new StringBuilder(CustomSQLUtil.get(GET_ALL_ANTEX_PR));
			List<Object> parameterList = new ArrayList<Object>();

			SQLQuery query = appendSearchCriteria(searchDTO, session, sql, parameterList, true);
			//query.addEntity("PrListDTO", PrListDTO.class);
			int start = (searchDTO.getPageNumber() - 1) * searchDTO.getPageSize();
			query.setFirstResult(start);
			query.setMaxResults(searchDTO.getPageSize());

			@SuppressWarnings("unused")
			List<Object[]> list = query.list();
			List<PrListDTO> prlist=new ArrayList<PrListDTO>();
			for(Object[] row:list) {
				PrListDTO prListDTO=new PrListDTO();

				prListDTO.setPurchaseRequestId(Long.valueOf(row[0].toString()));
				prListDTO.setScreenNameRequester(""+row[1]);
				prListDTO.setScreenNameReciever(""+row[2]);
				prListDTO.setVendorName(row[3]+"");
				prListDTO.setBudgetSubCategoryName(row[4]+"");
				prListDTO.setTotalValue(row[5]+"");
				prListDTO.setFiscalYear(row[6]+"");
				prListDTO.setActivityDescription(row[7]+"");
				if("1".equals(row[8].toString())) {
					prListDTO.setAutomatic(true);
				}else {
					prListDTO.setAutomatic(false);
				}
				prListDTO.setStatus(Integer.valueOf(row[9].toString()));
				prListDTO.setCurrency(row[10]+"");
				prListDTO.setCostCenterId(row[11]+"");
				prListDTO.setCostCenterName(row[12]+"");
				prListDTO.setTipoPr(Integer.valueOf(row[13].toString()));

				prlist.add(prListDTO);

			}
			return prlist;
		} finally {
			if(session != null) {
				closeSession(session);
			}
		}
	}

	public Long getAntexPurchaseRequestCount(SearchDTO searchDTO) {
		Session session = null; 
		try {

			session = openSession();
			StringBuilder sql = new StringBuilder(CustomSQLUtil.get(GET_COUNT_ALL_ANTEX_PR));
			List<Object> parameterList = new ArrayList<Object>();

			SQLQuery query = appendSearchCriteria(searchDTO, session, sql, parameterList,true);
			query.addScalar("COUNT_VALUE", Type.LONG);

			@SuppressWarnings("unchecked")
			List<Long> list = query.list();
			if(list != null && list.size() > 0) {
				return list.get(0);
			}
			return 0L;
		} finally {
			if(session != null) {
				closeSession(session);
			}
		}
	}

	private SQLQuery appendSearchCriteria(SearchDTO searchDTO, Session session,
			StringBuilder sql, List<Object> parameterList,boolean searchCostCenter) {

		if(searchDTO.getPrStatus() != null && 0 != searchDTO.getPrStatus()) {
			sql.append(" and pr.status = ? ");
			parameterList.add(searchDTO.getPrStatus());
		}
		if(searchDTO.getAutomatic() != null) {
			sql.append(" and pr.automatic = ? ");
			parameterList.add(searchDTO.getAutomatic());
		}
		if(searchDTO.getPositionStatus() != null) {
			appendPositionStatusCriteria(sql, searchDTO.getPositionStatus());
		}
		if(searchDTO.getPositionApproved() != null) {
			sql.append(" and position.approved = ? ");
			parameterList.add(searchDTO.getPositionApproved());
		}
		if(searchDTO.getPositionICApproval() != null) {
			sql.append(" and position.icApproval = ? ");
			parameterList.add(searchDTO.getPositionICApproval());
		}
		if(searchDTO.getMiniSearchValue() != null && !"".equals(searchDTO.getMiniSearchValue().trim()) && searchDTO.isMiniSeach()) {

			String searchKey = searchDTO.getMiniSearchValue();
			sql.append(" and ( pr.fiscalYear like ? or upper(v.vendorName) like ? or upper(bsc.budgetSubCategoryName) like ?" +
					" or upper(pr.activityDescription) like ? or upper(pr.totalValue) like ? or upper(pr.screenNameRequester) like ? " +
					" or upper(pr.screenNameReciever) like ? "+
					" ");
			parameterList.add("%"+searchKey+"%");
			parameterList.add("%"+searchKey.toUpperCase()+"%");
			parameterList.add("%"+searchKey.toUpperCase()+"%");
			parameterList.add("%"+searchKey.toUpperCase()+"%");
			parameterList.add("%"+searchKey.toUpperCase()+"%");
			parameterList.add("%"+searchKey.toUpperCase()+"%");
			parameterList.add("%"+searchKey.toUpperCase()+"%");
			if(searchCostCenter) {
				sql.append(" or upper(cc.costCenterName)  like ? ");
				parameterList.add("%"+searchKey.toUpperCase()+"%");
			}

			if(isNumberic(searchKey)) {
				sql.append(" or pr.purchaseRequestId = ?");
				parameterList.add(searchKey);
			}
			sql.append(" )");
		} else {
			if(searchDTO.getPrId() != null && searchDTO.getPrId() != 0) {
				sql.append(" and pr.purchaseRequestId like ?");
				parameterList.add("%"+searchDTO.getPrId()+"%");

			}
			if(searchDTO.getCostCenterId() != null && searchDTO.getCostCenterId() != 0){
				sql.append(" and pr.costCenterId = ?");
				parameterList.add(searchDTO.getCostCenterId());
			}
			if(searchDTO.getFiscalYear() != null && !"".equals(searchDTO.getFiscalYear().trim())) {
				sql.append(" and pr.fiscalYear like ?");
				parameterList.add("%"+searchDTO.getFiscalYear()+"%");
			}
			if(searchDTO.getWbsCode() != null && !"".equals(searchDTO.getWbsCode().trim())) {
				sql.append(" and position.wbsCodeId = ?");
				parameterList.add(searchDTO.getWbsCode());
			}
			if(searchDTO.getProjectName() != null && !"".equals(searchDTO.getProjectName())) {
				sql.append(" and pr.projectId = ?");
				parameterList.add(searchDTO.getProjectName());

			}
			if(searchDTO.getSubProjectName() != null && !"".equals(searchDTO.getSubProjectName())) {
				sql.append(" and upper(pr.subProjectId) = ?");
				parameterList.add(searchDTO.getSubProjectName());
			}
		}
		if(searchDTO.getOrderby() != null && !"".equals(searchDTO.getOrderby().trim())) {
			if(searchDTO.getOrderby().equals("costCenterName")) {
				sql.append(" order by ").append("cc."+searchDTO.getOrderby());	
			}else if(searchDTO.getOrderby().equals("vendorName")) {
				sql.append(" order by ").append("v."+searchDTO.getOrderby());	
			}else if(searchDTO.getOrderby().equals("budgetSubCategoryName")) {
				sql.append(" order by ").append("bsc."+searchDTO.getOrderby());	
			}else {
				sql.append(" order by ").append("pr."+searchDTO.getOrderby());
			}
			if(searchDTO.getOrder() != null && !"".equals(searchDTO.getOrder().trim())) {
				sql.append(" ").append(searchDTO.getOrder());
			}
		}

		SQLQuery query = session.createSQLQuery(sql.toString());
		for (int i = 0; i < parameterList.size(); i++) {
			query.setString(i, String.valueOf(parameterList.get(i)));
		}
		return query;
	}
	private boolean isNumberic(String searchKey) {
		try  
		{  
			long d = Long.parseLong(searchKey);  
		}  
		catch(NumberFormatException nfe)  
		{  
			return false;  
		}  
		return true; 
	}

	private StringBuilder appendPositionStatusCriteria(StringBuilder sql, Integer positionStatus) {
		if(positionStatus == PositionStatus.NEW.getCode()){
			sql.append(" and position.assetNumber is null and position.shoppingCart is null and position.poNumber is null ");

		}else if(positionStatus == PositionStatus.ASSET_RECEIVED.getCode()){
			sql.append(" and ((position.assetNumber is not null and position.shoppingCart is null and position.poNumber is null and pr.tipoPr in(0)) ");
			sql.append(" or (position.shoppingCart is null and position.poNumber is null and pr.tipoPr in (1,2,3) and pr.status = 7)) ");

		}else if(positionStatus == PositionStatus.SC_RECEIVED.getCode()){
			sql.append(" and ((position.assetNumber is not null and position.shoppingCart is not null and position.poNumber is null and pr.tipoPr in(0)) ");
			sql.append(" or (position.poNumber is null and pr.tipoPr in (1,2,3) and pr.status = 8))");

		}else if(positionStatus == PositionStatus.PO_RECEIVED.getCode()){
			sql.append(" and ((position.assetNumber is not null and position.shoppingCart is not null and position.poNumber is not null and pr.tipoPr in(0)) ");
			sql.append(" or (position.poNumber is not null and pr.tipoPr in (1,2,3)))");
		}
		return sql;
	}

	private static String GET_ALL_PR = PurchaseRequestPersistence.class.getName()+".getPurchaseReqeust";
	private static String GET_COUNT_ALL_PR = PurchaseRequestPersistence.class.getName()+".getPurchaseReqeustCount";
	private static String GET_ALL_ANTEX_PR = PurchaseRequestPersistence.class.getName()+".getAntexPurchaseReqeust";
	private static String GET_COUNT_ALL_ANTEX_PR = PurchaseRequestPersistence.class.getName()+".getAntexPurchaseReqeustCount";


}
