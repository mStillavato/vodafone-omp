package com.hp.omp.service.base;

import com.hp.omp.service.TrackingCodesLocalServiceUtil;

import java.util.Arrays;


public class TrackingCodesLocalServiceClpInvoker {
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName2;
    private String[] _methodParameterTypes2;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;
    private String _methodName9;
    private String[] _methodParameterTypes9;
    private String _methodName10;
    private String[] _methodParameterTypes10;
    private String _methodName11;
    private String[] _methodParameterTypes11;
    private String _methodName12;
    private String[] _methodParameterTypes12;
    private String _methodName13;
    private String[] _methodParameterTypes13;
    private String _methodName14;
    private String[] _methodParameterTypes14;
    private String _methodName15;
    private String[] _methodParameterTypes15;
    private String _methodName146;
    private String[] _methodParameterTypes146;
    private String _methodName147;
    private String[] _methodParameterTypes147;
    private String _methodName152;
    private String[] _methodParameterTypes152;
    private String _methodName153;
    private String[] _methodParameterTypes153;
    private String _methodName154;
    private String[] _methodParameterTypes154;
    private String _methodName155;
    private String[] _methodParameterTypes155;

    public TrackingCodesLocalServiceClpInvoker() {
        _methodName0 = "addTrackingCodes";

        _methodParameterTypes0 = new String[] { "com.hp.omp.model.TrackingCodes" };

        _methodName1 = "createTrackingCodes";

        _methodParameterTypes1 = new String[] { "long" };

        _methodName2 = "deleteTrackingCodes";

        _methodParameterTypes2 = new String[] { "long" };

        _methodName3 = "deleteTrackingCodes";

        _methodParameterTypes3 = new String[] { "com.hp.omp.model.TrackingCodes" };

        _methodName4 = "dynamicQuery";

        _methodParameterTypes4 = new String[] {  };

        _methodName5 = "dynamicQuery";

        _methodParameterTypes5 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName6 = "dynamicQuery";

        _methodParameterTypes6 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
            };

        _methodName7 = "dynamicQuery";

        _methodParameterTypes7 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName8 = "dynamicQueryCount";

        _methodParameterTypes8 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName9 = "fetchTrackingCodes";

        _methodParameterTypes9 = new String[] { "long" };

        _methodName10 = "getTrackingCodes";

        _methodParameterTypes10 = new String[] { "long" };

        _methodName11 = "getPersistedModel";

        _methodParameterTypes11 = new String[] { "java.io.Serializable" };

        _methodName12 = "getTrackingCodeses";

        _methodParameterTypes12 = new String[] { "int", "int" };

        _methodName13 = "getTrackingCodesesCount";

        _methodParameterTypes13 = new String[] {  };

        _methodName14 = "updateTrackingCodes";

        _methodParameterTypes14 = new String[] { "com.hp.omp.model.TrackingCodes" };

        _methodName15 = "updateTrackingCodes";

        _methodParameterTypes15 = new String[] {
                "com.hp.omp.model.TrackingCodes", "boolean"
            };

        _methodName146 = "getBeanIdentifier";

        _methodParameterTypes146 = new String[] {  };

        _methodName147 = "setBeanIdentifier";

        _methodParameterTypes147 = new String[] { "java.lang.String" };

        _methodName152 = "getTrackingCodesByFields";

        _methodParameterTypes152 = new String[] { "com.hp.omp.model.TrackingCodes" };

        _methodName153 = "getTrackingCodesByPRFields";

        _methodParameterTypes153 = new String[] {
                "com.hp.omp.model.PurchaseRequest"
            };

        _methodName154 = "getTrackingCodeByName";

        _methodParameterTypes154 = new String[] { "java.lang.String" };

        _methodName155 = "getTrackingCodesList";

        _methodParameterTypes155 = new String[] {
                "java.lang.Integer", "java.lang.Integer"
            };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName0.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.addTrackingCodes((com.hp.omp.model.TrackingCodes) arguments[0]);
        }

        if (_methodName1.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.createTrackingCodes(((Long) arguments[0]).longValue());
        }

        if (_methodName2.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.deleteTrackingCodes(((Long) arguments[0]).longValue());
        }

        if (_methodName3.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.deleteTrackingCodes((com.hp.omp.model.TrackingCodes) arguments[0]);
        }

        if (_methodName4.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.dynamicQuery();
        }

        if (_methodName5.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName6.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName7.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[3]);
        }

        if (_methodName8.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName9.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.fetchTrackingCodes(((Long) arguments[0]).longValue());
        }

        if (_methodName10.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.getTrackingCodes(((Long) arguments[0]).longValue());
        }

        if (_methodName11.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.getPersistedModel((java.io.Serializable) arguments[0]);
        }

        if (_methodName12.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.getTrackingCodeses(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName13.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.getTrackingCodesesCount();
        }

        if (_methodName14.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.updateTrackingCodes((com.hp.omp.model.TrackingCodes) arguments[0]);
        }

        if (_methodName15.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.updateTrackingCodes((com.hp.omp.model.TrackingCodes) arguments[0],
                ((Boolean) arguments[1]).booleanValue());
        }

        if (_methodName146.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes146, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.getBeanIdentifier();
        }

        if (_methodName147.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes147, parameterTypes)) {
            TrackingCodesLocalServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName152.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes152, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.getTrackingCodesByFields((com.hp.omp.model.TrackingCodes) arguments[0]);
        }

        if (_methodName153.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes153, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.getTrackingCodesByPRFields((com.hp.omp.model.PurchaseRequest) arguments[0]);
        }

        if (_methodName154.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes154, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.getTrackingCodeByName((java.lang.String) arguments[0]);
        }

        if (_methodName155.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes155, parameterTypes)) {
            return TrackingCodesLocalServiceUtil.getTrackingCodesList((java.lang.Integer) arguments[0],
                (java.lang.Integer) arguments[1], null);
        }

        throw new UnsupportedOperationException();
    }
}
