package com.hp.omp.model.impl;

import com.hp.omp.model.BudgetCategory;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

/**
 * The cache model class for representing BudgetCategory in entity cache.
 *
 * @author HP Egypt team
 * @see BudgetCategory
 * @generated
 */
public class BudgetCategoryCacheModel implements CacheModel<BudgetCategory>,
    Serializable {
    public long budgetCategoryId;
    public String budgetCategoryName;
    public boolean display;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{budgetCategoryId=");
        sb.append(budgetCategoryId);
        sb.append(", budgetCategoryName=");
        sb.append(budgetCategoryName);
        sb.append(", display=");
        sb.append(display);
        sb.append("}");

        return sb.toString();
    }

    public BudgetCategory toEntityModel() {
        BudgetCategoryImpl budgetCategoryImpl = new BudgetCategoryImpl();

        budgetCategoryImpl.setBudgetCategoryId(budgetCategoryId);

        if (budgetCategoryName == null) {
            budgetCategoryImpl.setBudgetCategoryName(StringPool.BLANK);
        } else {
            budgetCategoryImpl.setBudgetCategoryName(budgetCategoryName);
        }

        budgetCategoryImpl.setDisplay(display);

        budgetCategoryImpl.resetOriginalValues();

        return budgetCategoryImpl;
    }
}
