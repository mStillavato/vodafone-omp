package com.hp.omp.model.custom;

public class FileUpload {
	String fileName;
	String fileNameDisplayed;
	Long fileDate;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Long getFileDate() {
		return fileDate;
	}
	public void setFileDate(Long fileDate) {
		this.fileDate = fileDate;
	}
	public String getFileNameDisplayed() {
		return fileNameDisplayed;
	}
	public void setFileNameDisplayed(String fileNameDisplayed) {
		this.fileNameDisplayed = fileNameDisplayed;
	}
	
}
