package com.hp.omp.service.persistence;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.hp.omp.model.MacroDriver;
import com.hp.omp.model.impl.MacroDriverImpl;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class MacroDriverFinderImpl extends MacroDriverPersistenceImpl implements MacroDriverFinder{

	private static Log _log = LogFactoryUtil.getLog(MacroDriverFinderImpl.class);
	static final String GET_MACRO_DRIVER_LIST = MacroDriverFinderImpl.class.getName()+".getMacroDriversList";
	@SuppressWarnings("unchecked")
	public List<MacroDriver> getMacroDrivers(int start, int end,
			String searchFilter) throws SystemException {
		Session session=null;
		List<MacroDriver> list = new ArrayList<MacroDriver>();
		try {
		session  = openSession();
		
		String sql = null;
		
		if(StringUtils.isEmpty(searchFilter))
			searchFilter = "%%";
		
		sql = CustomSQLUtil.get(GET_MACRO_DRIVER_LIST);
		SQLQuery query = session.createSQLQuery(sql);
		
		if(query == null){
			_log.error("get macro driver list error, no sql found");
			return null;
		}
		
		query.setString(0, searchFilter);
		
		query.addEntity("MacroDriverList", MacroDriverImpl.class);
		list = (List<MacroDriver>)QueryUtil.list(query, getDialect(), start, end);
		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
		return list;
	}
}
