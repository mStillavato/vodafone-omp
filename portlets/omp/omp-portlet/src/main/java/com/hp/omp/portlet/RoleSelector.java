package com.hp.omp.portlet;

import java.io.IOException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.model.custom.OmpRoles;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.Team;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.TeamLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class RoleSelector
 */
public class RoleSelector extends MVCPortlet {
	
	Logger logger = LoggerFactory.getLogger(PurchaseOrderPortlet.class);
	
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		try {
			logger.debug("inside doView()" );
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			User user=themeDisplay.getUser();
			List<Role>userRoles=user.getRoles();
			
			logger.debug("userRoles size = " + userRoles.size());
			renderRequest.setAttribute("userRoles", userRoles);
			if(userRoles.size()>0) {
				
				if (userRoles.size() == 1) {
					 HttpSession httpSession = PortalUtil.getHttpServletRequest(renderRequest).getSession();
					String sendRedirect="";
					Role userRole=userRoles.get(0);
					logger.debug("user current role = " + userRole.getName());
					if(userRole.getName().equals(OmpRoles.OMP_CONTROLLER.name())) {
						 sendRedirect="controller home site";
						 httpSession.setAttribute("userCurrentRole", OmpRoles.OMP_CONTROLLER);
						 
					}else if(userRole.getName().equals(OmpRoles.OMP_ENG.name())) {
						 sendRedirect="/group/eng_org/eng_home";
						 httpSession.setAttribute("userCurrentRole", OmpRoles.OMP_ENG);
						 addRemoveOrganization(user.getUserId(), "ENG_ORG");
					}
					else if(userRole.getName().equals(OmpRoles.OMP_NDSO.name())) {
						sendRedirect="/group/ndso_org/ndso_home";
						 httpSession.setAttribute("userCurrentRole", OmpRoles.OMP_NDSO);
						 addRemoveOrganization(user.getUserId(), "NDSO_ORG");
					}
					
					logger.debug("sendRedirect url = " + sendRedirect);
					renderRequest.setAttribute("sendRedirect", sendRedirect);
				}
			}
		} catch (SystemException e) {
			logger.error("error happened during retrieving user roles",e);
		}
		super.doView(renderRequest, renderResponse);
	}
	
	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		
		super.render(request, response);
	}
	
	public void selectRole(ActionRequest request, ActionResponse response)
			throws Exception {
		logger.debug("inside selectRole ");
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		User user=themeDisplay.getUser();
		String userSelectedRole=ParamUtil.getString(request, "userSelectedRole");
		logger.debug("userSelectedRole = " + userSelectedRole);
		 HttpSession httpSession = PortalUtil.getHttpServletRequest(request).getSession();
			String sendRedirect="";
		if(userSelectedRole.equals(OmpRoles.OMP_CONTROLLER.name())) {
			 sendRedirect="controller home group";
			 httpSession.setAttribute("userCurrentRole", OmpRoles.OMP_CONTROLLER.getLabel());
			 
		}else if(userSelectedRole.equals(OmpRoles.OMP_ENG.name())) {
			 sendRedirect="/group/eng_org/eng_home";
			 httpSession.setAttribute("userCurrentRole", OmpRoles.OMP_ENG.getLabel());
			 addRemoveOrganization(user.getUserId(), "ENG_ORG");
		}
		else if(userSelectedRole.equals(OmpRoles.OMP_NDSO.name())) {
			sendRedirect="/group/ndso_org/ndso_home";
			 httpSession.setAttribute("userCurrentRole", OmpRoles.OMP_NDSO.getLabel());
			 addRemoveOrganization(user.getUserId(), "NDSO_ORG");
		}
		String sessionRole=httpSession.getAttribute("userCurrentRole").toString();
		logger.debug("sessionRole = " + sessionRole);
		logger.debug("sendRedirect URL = " + sendRedirect);
		response.sendRedirect(sendRedirect);
		
	}
	
	private void addRemoveOrganization(long userId,String organizationName) {
		try {
			logger.debug("userId = " + userId);
			logger.debug("organizationName sent= " + organizationName);
			long organizationId = 0;
			
			List<Organization> organizations=OrganizationLocalServiceUtil.getOrganizations(0, OrganizationLocalServiceUtil.getOrganizationsCount());
			logger.debug("all organizations site = " + organizations.size());
			for(Organization organization :organizations) {
				logger.debug("organizationName = " + organization.getName());
				if(organization.getName().equals(organizationName)) {
					
					organizationId=organization.getOrganizationId();
				}
			}
			logger.debug("organizationId = " + organizationId);
			
			List<Organization> userOrganizations= OrganizationLocalServiceUtil.getUserOrganizations(userId);
			OrganizationLocalServiceUtil.deleteUserOrganizations(userId,userOrganizations);
			OrganizationLocalServiceUtil.addUserOrganization(userId, organizationId);
			//UserLocalServiceUtil.addu
			
		} catch (Exception e) {
			logger.error("error happened during add remove organization",e);
		}
	}
 

}
