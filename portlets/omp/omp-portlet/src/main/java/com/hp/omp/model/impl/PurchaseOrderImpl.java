/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.model.impl;

import java.io.IOException;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.helper.UploadDownloadHelper;
import com.hp.omp.model.custom.OmpGroups;
import com.hp.omp.model.custom.OmpRoles;
import com.hp.omp.model.custom.PurchaseOrderStatus;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.security.auth.PrincipalThreadLocal;

/**
 * The extended model implementation for the PurchaseOrder service. Represents a row in the &quot;PURCHASE_ORDER&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.model.PurchaseOrder} interface.
 * </p>
 *
 * @author Brian Wing Shun Chan
 */
public class PurchaseOrderImpl extends PurchaseOrderBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a purchase order model instance should use the {@link com.hp.omp.model.PurchaseOrder} interface instead.
	 */
	public PurchaseOrderImpl() {
	}
	
	public PurchaseOrderStatus getPurchaseOrderStatus() {
		return PurchaseOrderStatus.getStatus(super.getStatus());
	}
	
	private String fieldStatus = null;
	private String txtBoxStatus = null;
	private String trCodeFieldsStatus = null;
	

	public String getTxtBoxStatus() throws PortalException, SystemException {
		
		if (txtBoxStatus == null) {
			long userId = PrincipalThreadLocal.getUserId();
			OmpRoles userRole = OmpHelper.getUserRole(userId);
			if (userRole.getLabel().equalsIgnoreCase("CONTROLLER") || userRole.getLabel().equalsIgnoreCase("CONTROLLER_VBS")) {
				txtBoxStatus = "";
			} else {
				txtBoxStatus = "readonly";
			}
		}
		return txtBoxStatus;
	}

	

	public String getFieldStatus()throws PortalException, SystemException{
		if (fieldStatus == null) {
			long userId = PrincipalThreadLocal.getUserId();
			OmpRoles userRole = OmpHelper.getUserRole(userId);
			if (userRole.getLabel().equalsIgnoreCase("CONTROLLER") || userRole.getLabel().equalsIgnoreCase("CONTROLLER_VBS")) {
				fieldStatus = "";
			} else {
				fieldStatus = "disabled";
			}
		}

		return fieldStatus;
	}
	
	public void setFieldStatus(String fieldStatus)throws PortalException, SystemException{
		this.fieldStatus = fieldStatus;
	}
	
	public boolean getAllowUpdate()throws PortalException, SystemException{
		long userId =  PrincipalThreadLocal.getUserId();
		OmpRoles userRole = OmpHelper.getUserRole(userId);
		switch (userRole) {
		case OMP_CONTROLLER:
			return true;
		default:
			return false;
		}
	}
	
	public boolean getAllowDelete()throws PortalException, SystemException{
		long userId =  PrincipalThreadLocal.getUserId();
		OmpRoles userRole = OmpHelper.getUserRole(userId);
		switch (userRole) {
		case OMP_CONTROLLER:
			return true;
		default:
			return false;
		}
	}

	public boolean getAllowAdd()throws PortalException, SystemException{
		long userId =  PrincipalThreadLocal.getUserId();
		OmpRoles userRole = OmpHelper.getUserRole(userId);
		switch (userRole) {
		case OMP_CONTROLLER:
			return true;
		case OMP_ANTEX:
			return true;
		default:
			return false;
		}
	}
	
	
	public String getTrCodeFieldsStatus() throws PortalException, SystemException {
		if (trCodeFieldsStatus == null) {
			long userId = PrincipalThreadLocal.getUserId();
			OmpRoles userRole = OmpHelper.getUserRole(userId);
			if ((userRole.getLabel().equalsIgnoreCase("CONTROLLER") || userRole.getLabel().equalsIgnoreCase("CONTROLLER_VBS"))
					&& "".equals(super.getTrackingCode())) {
				trCodeFieldsStatus = "";
			} else {
				trCodeFieldsStatus = "disabled";
			}
		}

		return trCodeFieldsStatus;
	}

	public void setTrCodeFieldsStatus(String trCodeFieldsStatus) {
		this.trCodeFieldsStatus = trCodeFieldsStatus;
	}

	public boolean isHasFileGR() {
		try {
			return UploadDownloadHelper.isPRHasUploadedFiles(super.getPurchaseOrderId());
		} catch (IOException e) {
			return false;
		}
	}
}