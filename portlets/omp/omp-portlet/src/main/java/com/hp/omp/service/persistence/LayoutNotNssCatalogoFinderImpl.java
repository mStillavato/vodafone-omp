package com.hp.omp.service.persistence;

import java.util.ArrayList;
import java.util.List;

import com.hp.omp.model.custom.LayoutNotNssCatalogoListDTO;
import com.hp.omp.model.custom.SearchDTO;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class LayoutNotNssCatalogoFinderImpl extends LayoutNotNssCatalogoPersistenceImpl implements LayoutNotNssCatalogoFinder{

	private static Log _log = LogFactoryUtil.getLog(LayoutNotNssCatalogoFinderImpl.class);
	static final String GET_CATALOGO_LIST = LayoutNotNssCatalogoFinderImpl.class.getName()+".getLayoutNotNssCatalogoList";
	static final String GET_COUNT_CATALOGO = LayoutNotNssCatalogoFinderImpl.class.getName()+".getLayoutNotNssCatalogoListCount";
	static final String DEL_CATALOGO_LIST = LayoutNotNssCatalogoFinderImpl.class.getName()+".delLayoutNotNssCatalogoList";
	static final String DEL_CATALOGO_ONE = LayoutNotNssCatalogoFinderImpl.class.getName()+".delLayoutNotNssCatalogoOne";

	@SuppressWarnings("unchecked")
	public List<LayoutNotNssCatalogoListDTO> getLayoutNotNssCatalogoList(String user) {
		Session session=null;

		try {
			session  = openSession();

			StringBuilder sql = new StringBuilder(CustomSQLUtil.get(GET_CATALOGO_LIST));
			SQLQuery query = session.createSQLQuery(sql.toString());
			query.setString(0, user);
			
			@SuppressWarnings("unused")
			List<Object[]> list = query.list();
			_log.debug("LayoutNotNssCatalogo list :- "+list);			

			List<LayoutNotNssCatalogoListDTO> layoutNotNssCatalogoList = new ArrayList<LayoutNotNssCatalogoListDTO>();

			for(Object[] row:list) {
				LayoutNotNssCatalogoListDTO layoutNotNssCatalogoListDTO = new LayoutNotNssCatalogoListDTO();

				layoutNotNssCatalogoListDTO.setLayoutNotNssCatalogoId(Long.valueOf(row[0].toString()));
				layoutNotNssCatalogoListDTO.setLineNumber(Long.valueOf(row[1].toString()));
				layoutNotNssCatalogoListDTO.setMaterialId(remove_zeros("" + row[2]));
				layoutNotNssCatalogoListDTO.setMaterialDesc("" + row[3]);
				layoutNotNssCatalogoListDTO.setQuantity("" + row[4]);
				layoutNotNssCatalogoListDTO.setNetPrice("" + row[5]);
				layoutNotNssCatalogoListDTO.setCurrency("" + row[6]);
				layoutNotNssCatalogoListDTO.setDeliveryDate(formatDate("" + row[7]));
				layoutNotNssCatalogoListDTO.setDeliveryAddress("" + row[8]);
				layoutNotNssCatalogoListDTO.setItemText("" + row[9]);
				layoutNotNssCatalogoListDTO.setLocationEvo("" + row[10]);
				layoutNotNssCatalogoListDTO.setTargaTecnica("" + row[11]);
				layoutNotNssCatalogoListDTO.setUser_("" + row[12]);
				layoutNotNssCatalogoListDTO.setVendorCode("" + row[13]);
				layoutNotNssCatalogoListDTO.setCatalogoId(Long.valueOf(row[14].toString()));
				layoutNotNssCatalogoListDTO.setIcApproval("" + row[15]);
				layoutNotNssCatalogoListDTO.setCategoryCode("" + row[16]);
				layoutNotNssCatalogoListDTO.setOfferNumber("" + row[17]);

				_log.debug("layoutNotNssCatalogoListDTO :- " + layoutNotNssCatalogoListDTO);
				layoutNotNssCatalogoList.add(layoutNotNssCatalogoListDTO);
			}
			return layoutNotNssCatalogoList;

		} finally{
			if(session != null) {
				closeSession(session);
			}
		}

	}

	public Long getLayoutNotNssCatalogoListCount(String user) {
		Session session = null; 
		try {

			session = openSession();
			StringBuilder sql = new StringBuilder(CustomSQLUtil.get(GET_COUNT_CATALOGO));
			SQLQuery query = session.createSQLQuery(sql.toString());
			query.setString(1, user);
			query.addScalar("COUNT_VALUE", Type.LONG);

			//query.set
			//query.setFirstResult(searchDTO.getfi)
			@SuppressWarnings("unchecked")
			List<Long> list = query.list();
			if(list != null && list.size() > 0) {
				return list.get(0);
			}
			return 0L;
		} finally {
			if(session != null) {
				closeSession(session);
			}
		}
	}

	private String remove_zeros(String str) {
		return str.replaceFirst("^0+(?!$)", "");
	}

	public void deleteLayoutNotNssCatalogo4User(String user) {
		Session session=null;

		try {
			session  = openSession();

			StringBuilder sql = new StringBuilder(CustomSQLUtil.get(DEL_CATALOGO_LIST));
			SQLQuery query = session.createSQLQuery(sql.toString());
			query.setString(0, user);
			query.executeUpdate();
		} finally{
			if(session != null) {
				closeSession(session);
			}
		}
	}
	
	private String formatDate(String date){
		try{
			String anno = date.substring(0, 4);
			String mese = date.substring(5, 7);
			String giorno = date.substring(8, 10);
			 
			return (giorno + '/' + mese + '/' + anno);
		}catch(Exception e){
			return "";
		}
	}
}
