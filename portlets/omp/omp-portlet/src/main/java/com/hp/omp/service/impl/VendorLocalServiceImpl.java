package com.hp.omp.service.impl;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Workbook;

import com.hp.omp.model.ExcelReport;
import com.hp.omp.model.ExcelStyler;
import com.hp.omp.model.ExcelTable;
import com.hp.omp.model.Vendor;
import com.hp.omp.model.custom.ExcelTableRow;
import com.hp.omp.model.custom.OMPAggregateReportExcelRow;
import com.hp.omp.model.impl.ExcelDefaultStyler;
import com.hp.omp.model.impl.ExcelReportImpl;
import com.hp.omp.model.impl.ExcelTableImpl;
import com.hp.omp.service.base.VendorLocalServiceBaseImpl;
import com.hp.omp.service.persistence.VendorFinderUtil;
import com.hp.omp.service.persistence.VendorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

/**
 * The implementation of the vendor local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.VendorLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Ahmed Kamel
 * @see com.hp.omp.service.base.VendorLocalServiceBaseImpl
 * @see com.hp.omp.service.VendorLocalServiceUtil
 */
public class VendorLocalServiceImpl extends VendorLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link com.hp.omp.service.VendorLocalServiceUtil} to access the vendor local service.
     */
	
	public Vendor getVendorByName(String name, int gruppoUsers) throws SystemException{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Vendor.class);
		dynamicQuery.add(RestrictionsFactoryUtil.ilike("vendorName", name));
		dynamicQuery.add(RestrictionsFactoryUtil.eq("gruppoUsers", gruppoUsers));
		 
		Vendor vendor = null;
		List<Vendor>  vendorList = VendorUtil.findWithDynamicQuery(dynamicQuery);
		if(vendorList != null && ! (vendorList.isEmpty()) ){
			vendor = vendorList.get(0);
		}
		
		return vendor;
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Vendor> getVendorsList(int start, int end, String searchFilter, int gruppoUsers)
			throws SystemException {
		List<Vendor> resultQuery = null;
		resultQuery= VendorFinderUtil.getVendorsList(start, end, searchFilter, gruppoUsers);
		return resultQuery; 
	}
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Vendor getVendorBySupplierCode(String supplierCode, int gruppoUsers) throws SystemException{
		Vendor vendor = null;
		List<Vendor>  vendorList = VendorFinderUtil.getVendorBySupplierCode(supplierCode, gruppoUsers);
		if(vendorList != null && ! (vendorList.isEmpty()) ){
			vendor = vendorList.get(0);
		}
		return vendor;
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getVendorsCountList(String searchFilter, int gruppoUsers) throws SystemException {
		int countList = 0;
		countList = VendorFinderUtil.getVendorsCountList(searchFilter, gruppoUsers);
		return countList;
	}

	public void exportVendorsAsExcel(List<Vendor> listVendors,
			List<Object> header, OutputStream out) throws SystemException {
		ExcelReport excelReport = new ExcelReportImpl();
		ExcelStyler styler = new ExcelDefaultStyler(excelReport.getWorkBook());
		excelReport.setStyler(styler);

		List<ExcelTableRow> vendorsList = new ArrayList<ExcelTableRow>();
		for(Vendor vendor : listVendors){
			OMPAggregateReportExcelRow tableRow = new OMPAggregateReportExcelRow();
			tableRow.setFieldAtIndex(vendor.getVendorName(), 0);
			tableRow.setFieldAtIndex(vendor.getSupplier(), 1);
			tableRow.setFieldAtIndex(vendor.getSupplierCode(), 2);
			tableRow.setFieldAtIndex(vendor.getDisplay(), 3);

			vendorsList.add(tableRow);
		}

		ExcelTable euroTable = new ExcelTableImpl(header, vendorsList, "Vendors");
		excelReport.addExcelTable(euroTable, 0);

		Workbook exelWorkBook = excelReport.createExelWorkBook();
		try {
			exelWorkBook.write(out);
		} catch (IOException e) {
		} finally{
			try {
				out.flush();
				out.close();
			} catch (IOException e) {
			}
		}
	}
}
