package com.hp.omp.service.persistence;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.hp.omp.model.WbsCode;
import com.hp.omp.model.impl.WbsCodeImpl;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class WbsCodeFinderImpl extends WbsCodePersistenceImpl implements WbsCodeFinder{

	private static Log _log = LogFactoryUtil.getLog(WbsCodeFinderImpl.class);
	static final String GET_WBS_CODES_LIST = WbsCodeFinderImpl.class.getName()+".getWbsCodesList";
	static final String GET_WBS_CODES_COUNT_LIST = WbsCodeFinderImpl.class.getName()+".getWbsCodesCountList";
	
	@SuppressWarnings("unchecked")
	public List<WbsCode> getWbsCodesList(int start, int end, String searchFilter)
			throws SystemException {
		Session session=null;
		List<WbsCode> list = new ArrayList<WbsCode>();
		try {
		session  = openSession();
		
		String sql = null;
		
		if(StringUtils.isEmpty(searchFilter))
			searchFilter = "%%";
		
		sql = CustomSQLUtil.get(GET_WBS_CODES_LIST);
		SQLQuery query = session.createSQLQuery(sql);
		
		if(query == null){
			_log.error("get wbs code list error, no sql found");
			return null;
		}
		
		query.setString(0, searchFilter);
		
		query.addEntity("WbsCodeList", WbsCodeImpl.class);
		list = (List<WbsCode>)QueryUtil.list(query, getDialect(), start, end);
		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
		return list;
	}

	public int getWbsCodesCountList(String searchFilter) throws SystemException {
		Session session = null; 
		try {
			session = openSession();
			
			String sql = null;
			
			if(StringUtils.isEmpty(searchFilter))
				searchFilter = "%%";
			
			sql = CustomSQLUtil.get(GET_WBS_CODES_COUNT_LIST);
			SQLQuery query = session.createSQLQuery(sql);
			
			if(query == null){
				_log.error("get wbs code count list error, no sql found");
				return 0;
			}
			
			query.setString(0, searchFilter);
			
			query.addScalar("COUNT_VALUE", Type.INTEGER);

			@SuppressWarnings("unchecked")
			List<Integer> list = query.list();
			if(list != null && list.size() > 0) {
				return list.get(0);
			}
			return 0;
		} finally {
			if(session != null) {
				closeSession(session);
			}
		}
	}
}
