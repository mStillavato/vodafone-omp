/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service.impl;

import java.util.List;

import com.hp.omp.model.CostCenter;
import com.hp.omp.service.base.CostCenterLocalServiceBaseImpl;
import com.hp.omp.service.persistence.CostCenterFinderUtil;
import com.hp.omp.service.persistence.CostCenterUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;

/**
 * The implementation of the cost center local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.CostCenterLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Ahmed Kamel
 * @see com.hp.omp.service.base.CostCenterLocalServiceBaseImpl
 * @see com.hp.omp.service.CostCenterLocalServiceUtil
 */
public class CostCenterLocalServiceImpl extends CostCenterLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.hp.omp.service.CostCenterLocalServiceUtil} to access the cost center local service.
	 */
	
	public CostCenter getCostCenterByName(String name, int gruppoUsers) throws SystemException{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(CostCenter.class);
		dynamicQuery.add(PropertyFactoryUtil.forName("costCenterName").eq(name));
		dynamicQuery.add(RestrictionsFactoryUtil.eq("gruppoUsers", gruppoUsers));
		 
		CostCenter costCenter = null;
		List<CostCenter>  csList = CostCenterUtil.findWithDynamicQuery(dynamicQuery);
		if(csList != null && ! (csList.isEmpty()) ){
			costCenter = csList.get(0);
		}
		
		return costCenter;
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<CostCenter> getCostCentersList(int start, int end,
			String searchFilter, int gruppoUsers) throws SystemException {
		List<CostCenter> resultQuery = null;
		resultQuery= CostCenterFinderUtil.getCostCentersList(start, end, searchFilter, gruppoUsers);
		return resultQuery; 
	}
	
}