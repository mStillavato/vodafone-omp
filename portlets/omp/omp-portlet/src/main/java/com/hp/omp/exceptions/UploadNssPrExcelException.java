package com.hp.omp.exceptions;



/**
 * @author Sami.basyoni
 */
public class UploadNssPrExcelException extends Exception {

	public UploadNssPrExcelException() {
		super();
	}

	public UploadNssPrExcelException(String msg) {
		super(msg);
	}

	public UploadNssPrExcelException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public UploadNssPrExcelException(Throwable cause) {
		super(cause);
	}

}