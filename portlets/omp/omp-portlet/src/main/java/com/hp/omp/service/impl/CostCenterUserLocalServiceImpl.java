/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service.impl;

import java.util.List;

import com.hp.omp.model.CostCenterUser;
import com.hp.omp.service.base.CostCenterUserLocalServiceBaseImpl;
import com.hp.omp.service.persistence.CostCenterUserUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the cost center user local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.CostCenterUserLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Ahmed Kamel
 * @see com.hp.omp.service.base.CostCenterUserLocalServiceBaseImpl
 * @see com.hp.omp.service.CostCenterUserLocalServiceUtil
 */
public class CostCenterUserLocalServiceImpl
	extends CostCenterUserLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.hp.omp.service.CostCenterUserLocalServiceUtil} to access the cost center user local service.
	 */
	
	public CostCenterUser getCostCenterUserByCostCenterAndUser(long costCenterId, long userId) throws SystemException{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(CostCenterUser.class);
		dynamicQuery.add(PropertyFactoryUtil.forName("costCenterId").eq(costCenterId));
		dynamicQuery.add(PropertyFactoryUtil.forName("userId").eq(userId));
		 
		CostCenterUser costCenterUser = null;
		List<CostCenterUser>  ccsList = CostCenterUserUtil.findWithDynamicQuery(dynamicQuery);
		if(ccsList != null && ! (ccsList.isEmpty()) ){
			costCenterUser = ccsList.get(0);
		}
		
		return costCenterUser;
	}
	
	public CostCenterUser getCostCenterUserByUserId(long userId) throws SystemException{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(CostCenterUser.class);
		dynamicQuery.add(PropertyFactoryUtil.forName("userId").eq(userId));
		 
		CostCenterUser costCenterUser = null;
		List<CostCenterUser>  ccsList = CostCenterUserUtil.findWithDynamicQuery(dynamicQuery);
		if(ccsList != null && ! (ccsList.isEmpty()) ){
			costCenterUser = ccsList.get(0);
		}
		
		return costCenterUser;
	}
	
	public CostCenterUser getCostCenterUserByCostCenterId(long costCenterId) throws SystemException{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(CostCenterUser.class);
		dynamicQuery.add(PropertyFactoryUtil.forName("costCenterId").eq(costCenterId));
		 
		CostCenterUser costCenterUser = null;
		List<CostCenterUser>  ccsList = CostCenterUserUtil.findWithDynamicQuery(dynamicQuery);
		if(ccsList != null && ! (ccsList.isEmpty()) ){
			costCenterUser = ccsList.get(0);
		}
		
		return costCenterUser;
	}
}