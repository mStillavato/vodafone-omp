package com.hp.omp.model.impl;

import com.hp.omp.model.Vendor;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

/**
 * The cache model class for representing Vendor in entity cache.
 *
 * @author HP Egypt team
 * @see Vendor
 * @generated
 */
public class VendorCacheModel implements CacheModel<Vendor>, Serializable {
    public long vendorId;
    public String supplier;
    public String supplierCode;
    public String vendorName;
    public boolean display;
    public int gruppoUsers;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{vendorId=");
        sb.append(vendorId);
        sb.append(", supplier=");
        sb.append(supplier);
        sb.append(", supplierCode=");
        sb.append(supplierCode);
        sb.append(", vendorName=");
        sb.append(vendorName);
        sb.append(", display=");
        sb.append(display);
        sb.append(", gruppoUsers=");
        sb.append(gruppoUsers);
        sb.append("}");

        return sb.toString();
    }

    public Vendor toEntityModel() {
        VendorImpl vendorImpl = new VendorImpl();

        vendorImpl.setVendorId(vendorId);

        if (supplier == null) {
            vendorImpl.setSupplier(StringPool.BLANK);
        } else {
            vendorImpl.setSupplier(supplier);
        }

        if (supplierCode == null) {
            vendorImpl.setSupplierCode(StringPool.BLANK);
        } else {
            vendorImpl.setSupplierCode(supplierCode);
        }

        if (vendorName == null) {
            vendorImpl.setVendorName(StringPool.BLANK);
        } else {
            vendorImpl.setVendorName(vendorName);
        }

        vendorImpl.setDisplay(display);
        
        vendorImpl.setGruppoUsers(gruppoUsers);

        vendorImpl.resetOriginalValues();

        return vendorImpl;
    }
}
