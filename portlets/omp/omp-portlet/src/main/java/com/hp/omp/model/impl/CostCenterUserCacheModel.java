package com.hp.omp.model.impl;

import com.hp.omp.model.CostCenterUser;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

/**
 * The cache model class for representing CostCenterUser in entity cache.
 *
 * @author HP Egypt team
 * @see CostCenterUser
 * @generated
 */
public class CostCenterUserCacheModel implements CacheModel<CostCenterUser>,
    Serializable {
    public long costCenterUserId;
    public long costCenterId;
    public long userId;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(7);

        sb.append("{costCenterUserId=");
        sb.append(costCenterUserId);
        sb.append(", costCenterId=");
        sb.append(costCenterId);
        sb.append(", userId=");
        sb.append(userId);
        sb.append("}");

        return sb.toString();
    }

    public CostCenterUser toEntityModel() {
        CostCenterUserImpl costCenterUserImpl = new CostCenterUserImpl();

        costCenterUserImpl.setCostCenterUserId(costCenterUserId);
        costCenterUserImpl.setCostCenterId(costCenterId);
        costCenterUserImpl.setUserId(userId);

        costCenterUserImpl.resetOriginalValues();

        return costCenterUserImpl;
    }
}
