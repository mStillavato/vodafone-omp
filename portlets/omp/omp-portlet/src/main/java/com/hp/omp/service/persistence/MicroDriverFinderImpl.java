package com.hp.omp.service.persistence;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.hp.omp.model.MicroDriver;
import com.hp.omp.model.impl.MicroDriverImpl;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class MicroDriverFinderImpl extends MicroDriverPersistenceImpl implements MicroDriverFinder{
	
	private static Log _log = LogFactoryUtil.getLog(MicroDriverFinderImpl.class);
	static final String GET_MICRO_DRIVERS = MicroDriverFinderImpl.class.getName()+".getMicroDrivers";
	static final String GET_MICRO_DRIVER_LIST = MicroDriverFinderImpl.class.getName()+".getMicroDriversList";

	@SuppressWarnings("unchecked")
	public List<MicroDriver> getMicroDriverByMacroDriverId(Long macroDriverId) throws SystemException {
		Session session = null;
		List<MicroDriver> list = new ArrayList<MicroDriver>();

		try {
			session = openSession();
			
			String sql = CustomSQLUtil.get(GET_MICRO_DRIVERS);
			if(_log.isDebugEnabled()){
				_log.debug("SQL=["+ sql+"]");
				_log.debug("value=["+ macroDriverId+"]");
			}
			SQLQuery query = session.createSQLQuery(sql);
			
			if(query == null){
				_log.error("get sub projects error, no sql found");
				return null;
			}
			query.setLong(0, macroDriverId);
			query.addEntity("MicroDriver", MicroDriverImpl.class);
			
			list = (List<MicroDriver>)QueryUtil.list(query, getDialect(),  QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			
		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
		
		return list;
	}

	public List<MicroDriver> getMicroDriversList(int start, int end,
			String searchFilter) throws SystemException {
		Session session=null;
		List<MicroDriver> list = new ArrayList<MicroDriver>();
		try {
		session  = openSession();
		
		String sql = null;
		
		if(StringUtils.isEmpty(searchFilter))
			searchFilter = "%%";
		
		sql = CustomSQLUtil.get(GET_MICRO_DRIVER_LIST);
		SQLQuery query = session.createSQLQuery(sql);
		
		if(query == null){
			_log.error("get micro driver list error, no sql found");
			return null;
		}
		
		query.setString(0, searchFilter);
		
		query.addEntity("MicroDriverList", MicroDriverImpl.class);
		list = (List<MicroDriver>)QueryUtil.list(query, getDialect(), start, end);
		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
		return list;
	}

}
