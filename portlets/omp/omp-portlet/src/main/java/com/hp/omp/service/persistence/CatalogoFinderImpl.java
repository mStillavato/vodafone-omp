package com.hp.omp.service.persistence;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.hp.omp.model.Catalogo;
import com.hp.omp.model.custom.CatalogoExportListDTO;
import com.hp.omp.model.custom.CatalogoListDTO;
import com.hp.omp.model.custom.SearchCatalogoDTO;
import com.hp.omp.model.impl.CatalogoImpl;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class CatalogoFinderImpl extends CatalogoPersistenceImpl implements CatalogoFinder{

	private static Log _log = LogFactoryUtil.getLog(CatalogoFinderImpl.class);
	static final String GET_CATALOGO_LIST = CatalogoFinderImpl.class.getName()+".getCatalogoList";
	static final String GET_COUNT_CATALOGO = CatalogoFinderImpl.class.getName()+".getCatalogoListCount";
	static final String GET_CATALOGO_FOR_VENDOR = CatalogoFinderImpl.class.getName()+".getCatalogoForVendor";
	static final String GET_EXPORT_CATALOGO_LIST = CatalogoFinderImpl.class.getName()+".getExportCatalogoList";

	@SuppressWarnings("unchecked")
	public List<CatalogoListDTO> getCatalogoList(SearchCatalogoDTO searchDTO) {
		Session session=null;

		try {
			session  = openSession();

			StringBuilder sql = new StringBuilder(CustomSQLUtil.get(GET_CATALOGO_LIST));
			SQLQuery query = appendSearchCriteria(searchDTO, session, sql);
			int start = (searchDTO.getPageNumber() - 1) * searchDTO.getPageSize();
			query.setFirstResult(start);
			query.setMaxResults(searchDTO.getPageSize());

			@SuppressWarnings("unused")
			List<Object[]> list = query.list();
			_log.debug("Catalogo list :- "+list);			

			List<CatalogoListDTO> catalogoList = new ArrayList<CatalogoListDTO>();

			for(Object[] row:list) {
				CatalogoListDTO catalogoListDTO = new CatalogoListDTO();

				catalogoListDTO.setCatalogoId(Long.valueOf(row[0].toString()));
				catalogoListDTO.setFornitoreDesc("" + row[1] + "\n " + row[2]);
				catalogoListDTO.setMatCatLvl4("" + row[3]);
				catalogoListDTO.setMatCatLvl4Desc("" + row[4]);
				catalogoListDTO.setMatCod(remove_zeros("" + row[5]));
				catalogoListDTO.setMatCodFornFinale("" + row[6]);
				catalogoListDTO.setMatDesc("" + row[7]);
				catalogoListDTO.setPrezzo("" + row[8]);
				catalogoListDTO.setValuta("" + row[9]);
				catalogoListDTO.setDataFineValidita(formatDate("" + row[10]));

				_log.debug("catalogoListDTO :- " + catalogoListDTO);
				catalogoList.add(catalogoListDTO);
			}
			return catalogoList;

		} finally{
			if(session != null) {
				closeSession(session);
			}
		}

	}

	public Long getCatalogoListCount(SearchCatalogoDTO searchDTO) {
		Session session = null; 
		try {

			session = openSession();
			StringBuilder sql = new StringBuilder(CustomSQLUtil.get(GET_COUNT_CATALOGO));
			SQLQuery query = appendSearchCriteria(searchDTO, session, sql);
			query.addScalar("COUNT_VALUE", Type.LONG);

			//query.set
			//query.setFirstResult(searchDTO.getfi)
			@SuppressWarnings("unchecked")
			List<Long> list = query.list();
			if(list != null && list.size() > 0) {
				return list.get(0);
			}
			return 0L;
		} finally {
			if(session != null) {
				closeSession(session);
			}
		}
	}

	public List<Catalogo> getCatalogoForVendor(int start, int end, String vendorCode)
			throws SystemException {
		Session session=null;
		List<Catalogo> list = new ArrayList<Catalogo>();
		try {
			session  = openSession();

			String sql = null;

			sql = CustomSQLUtil.get(GET_CATALOGO_FOR_VENDOR);
			SQLQuery query = session.createSQLQuery(sql);

			if(query == null){
				_log.error("get catalogo for vendor list error, no sql found");
				return null;
			}
			
			query.setString(0, vendorCode);

			query.addEntity("CatalogoList", CatalogoImpl.class);
			list = (List<Catalogo>)QueryUtil.list(query, getDialect(), start, end);

		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
		return list;
	}

	private SQLQuery appendSearchCriteria(SearchCatalogoDTO searchDTO, Session session, StringBuilder sql) {
		List<Object> parameterList = new ArrayList<Object>();

		sql.append(" Where ca.datafinevalidita >= sysdate and ca.flgCanc is null and ca.prezzo > 0 ");
		
		if(searchDTO.getVendorNotView() != null){
			_log.info("VendorNotView = " + searchDTO.getVendorNotView());
			sql.append(" and (ca.fornitoreCode not in (?");
			String[] arrOfVendor = searchDTO.getVendorNotView().split(",");
			parameterList.add(arrOfVendor[0]);
			if(arrOfVendor.length > 1){
				for (String a : arrOfVendor) {
		            System.out.println(a); 
		            sql.append(" , ?");
		            parameterList.add(a);
				}
			}
			sql.append(" )) ");
		}
		
		String searchKey = searchDTO.getMiniSearchValue();
		if(searchDTO.getMiniSearchValue() != null && !"".equals(searchDTO.getMiniSearchValue().trim()) && searchDTO.isMiniSeach()) {
			sql.append(" and (ca.fornitoreCode like ? or upper(ca.fornitoreDesc) like ?");
			sql.append(" or ca.matCod like ? or upper(ca.matDesc) like ? or upper(ca.matCatLvl4) like ?)");
			
			if(StringUtils.isNumeric(searchKey)){
				sql.append(" or ca.prezzo = ? ");
			}
			
			parameterList.add("%"+searchKey+"%"); 				// fornitoreCode
			parameterList.add("%"+searchKey.toUpperCase()+"%");	// fornitoreDesc
			parameterList.add("%"+searchKey+"%");				// matCod
			parameterList.add("%"+searchKey.toUpperCase()+"%");	// matDesc
			parameterList.add("%"+searchKey.toUpperCase()+"%");	// matCatLvl4
			
			if(StringUtils.isNumeric(searchKey)){
				parameterList.add(searchKey);						// prezzo
			}
		} else {
			if(searchDTO.getVendor() != null && searchDTO.getVendor().trim().length() > 0) {
				sql.append(" and (ca.fornitoreCode like ? or upper(ca.fornitoreDesc) like ? )");
				parameterList.add("%" + searchDTO.getVendor() + "%"); 	// fornitoreCode
				parameterList.add("%" + searchDTO.getVendor().trim().toUpperCase() + "%");	// fornitoreDesc

			}
			if(searchDTO.getMatCatLvl4() != null && searchDTO.getMatCatLvl4().trim().length() > 0){
				sql.append(" and upper(ca.matCatLvl4) like ?");
				parameterList.add("%" + searchDTO.getMatCatLvl4() + "%");
			}
			if(searchDTO.getCodMateriale() != null && searchDTO.getCodMateriale().trim().length() > 0) {
				sql.append(" and ca.matCod like ?");
				parameterList.add("%" + searchDTO.getCodMateriale() + "%");
			}
			if(searchDTO.getCodFornitore() != null && searchDTO.getCodFornitore().trim().length() > 0) {
				sql.append(" and ca.matCodFornFinale like ?");
				parameterList.add("%" + searchDTO.getCodFornitore() + "%");
			}
			if(searchDTO.getDescMateriale() != null && searchDTO.getDescMateriale().trim().length() > 0) {
				sql.append(" and upper(ca.matDesc) like ?");
				parameterList.add("%" + searchDTO.getDescMateriale().trim().toUpperCase() + "%");
			}
			if(searchDTO.getPrezzo() != 0) {
				sql.append(" and ca.prezzo = ?");
				parameterList.add(searchDTO.getPrezzo());
			}
		}

		_log.info(sql.toString());

		sql.append(" order by ca.fornitoreDesc, ca.matCatLvl4, ca.matCod, ca.matCodFornFinale ");

		SQLQuery query = session.createSQLQuery(sql.toString());
		for (int i = 0; i < parameterList.size(); i++) {
			_log.info(parameterList.get(i));
			query.setString(i, String.valueOf(parameterList.get(i)));
		}
		return query;
	}

	private String remove_zeros(String str) {
		return str.replaceFirst("^0+(?!$)", "");
	}
	
	private String formatDate(String date){
		try{
			String anno = date.substring(0, 4);
			String mese = date.substring(5, 7);
			String giorno = date.substring(8, 10);
			 
			return (giorno + '/' + mese + '/' + anno);
		}catch(Exception e){
			return "";
		}
	}

	public List<CatalogoExportListDTO> getExportCatalogoList(
			SearchCatalogoDTO searchDTO) {
		Session session=null;

		try {
			session  = openSession();

			StringBuilder sql = new StringBuilder(CustomSQLUtil.get(GET_EXPORT_CATALOGO_LIST));
			SQLQuery query = appendSearchCriteria(searchDTO, session, sql);
			int start = (searchDTO.getPageNumber() - 1) * searchDTO.getPageSize();
			query.setFirstResult(start);
			query.setMaxResults(searchDTO.getPageSize());

			@SuppressWarnings("unused")
			List<Object[]> list = query.list();
			_log.debug("Catalogo list :- "+list);			

			List<CatalogoExportListDTO> catalogoList = new ArrayList<CatalogoExportListDTO>();

			for(Object[] row:list) {
				CatalogoExportListDTO catalogoListDTO = new CatalogoExportListDTO();
				
				catalogoListDTO.setChiaveInforecord("" + row[0]);
				catalogoListDTO.setDataCreazione(formatDate("" + row[1]));
				catalogoListDTO.setDataFineValidita(formatDate("" + row[2]));
				catalogoListDTO.setDivisione("" + row[3]);
				catalogoListDTO.setFlgCanc(null); 				// Vengono estratti sempre e solo i record con FLGCANC = NULL
				catalogoListDTO.setFornFinCode("" + row[5]);
				catalogoListDTO.setFornitoreCode("" + row[6]);
				catalogoListDTO.setFornitoreDesc("" + row[7]);
				catalogoListDTO.setMatCatLvl2("" + row[8]);
				catalogoListDTO.setMatCatLvl2Desc("" + row[9]);
				catalogoListDTO.setMatCatLvl4("" + row[10]);
				catalogoListDTO.setMatCatLvl4Desc("" + row[11]);
				catalogoListDTO.setMatCod("" + row[12]);
				catalogoListDTO.setMatCodFornFinale("" + row[13]);
				catalogoListDTO.setMatDesc("" + row[14]);
				catalogoListDTO.setPrezzo("" + row[15]);
				catalogoListDTO.setPurchOrganiz("" + row[16]);
				catalogoListDTO.setUm("" + row[17]);
				catalogoListDTO.setUmPrezzo("" + row[18]);
				catalogoListDTO.setUmPrezzoPo("" + row[19]);
				catalogoListDTO.setValuta("" + row[20]);
				catalogoListDTO.setUpperMatDesc("" + row[21]);

				_log.debug("catalogoListDTO :- " + catalogoListDTO);
				catalogoList.add(catalogoListDTO);
			}
			return catalogoList;

		} finally{
			if(session != null) {
				closeSession(session);
			}
		}

	}
	
}
