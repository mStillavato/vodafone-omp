package com.hp.omp.model.impl;

import com.hp.omp.model.Position;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing Position in entity cache.
 *
 * @author HP Egypt team
 * @see Position
 * @generated
 */
public class PositionCacheModel implements CacheModel<Position>, Serializable {
    public long positionId;
    public String fiscalYear;
    public String description;
    public String categoryCode;
    public String offerNumber;
    public long offerDate;
    public String quatity;
    public long deliveryDate;
    public String deliveryAddress;
    public String actualUnitCost;
    public String numberOfUnit;
    public long labelNumber;
    public long poLabelNumber;
    public String targaTecnica;
    public boolean approved;
    public String icApproval;
    public String approver;
    public String assetNumber;
    public String shoppingCart;
    public String poNumber;
    public String evoLocation;
    public String wbsCodeId;
    public String purchaseRequestId;
    public String purchaseOrderId;
    public long createdDate;
    public long createdUserId;
    public String evoCodMateriale;
    public String evoDesMateriale;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(53);

        sb.append("{positionId=");
        sb.append(positionId);
        sb.append(", fiscalYear=");
        sb.append(fiscalYear);
        sb.append(", description=");
        sb.append(description);
        sb.append(", categoryCode=");
        sb.append(categoryCode);
        sb.append(", offerNumber=");
        sb.append(offerNumber);
        sb.append(", offerDate=");
        sb.append(offerDate);
        sb.append(", quatity=");
        sb.append(quatity);
        sb.append(", deliveryDate=");
        sb.append(deliveryDate);
        sb.append(", deliveryAddress=");
        sb.append(deliveryAddress);
        sb.append(", actualUnitCost=");
        sb.append(actualUnitCost);
        sb.append(", numberOfUnit=");
        sb.append(numberOfUnit);
        sb.append(", labelNumber=");
        sb.append(labelNumber);
        sb.append(", poLabelNumber=");
        sb.append(poLabelNumber);
        sb.append(", targaTecnica=");
        sb.append(targaTecnica);
        sb.append(", approved=");
        sb.append(approved);
        sb.append(", icApproval=");
        sb.append(icApproval);
        sb.append(", approver=");
        sb.append(approver);
        sb.append(", assetNumber=");
        sb.append(assetNumber);
        sb.append(", shoppingCart=");
        sb.append(shoppingCart);
        sb.append(", poNumber=");
        sb.append(poNumber);
        sb.append(", evoLocation=");
        sb.append(evoLocation);
        sb.append(", wbsCodeId=");
        sb.append(wbsCodeId);
        sb.append(", purchaseRequestId=");
        sb.append(purchaseRequestId);
        sb.append(", purchaseOrderId=");
        sb.append(purchaseOrderId);
        sb.append(", createdDate=");
        sb.append(createdDate);
        sb.append(", createdUserId=");
        sb.append(createdUserId);
        sb.append(", evoCodMateriale=");
        sb.append(evoCodMateriale);
        sb.append(", evoDesMateriale=");
        sb.append(evoDesMateriale);
        sb.append("}");

        return sb.toString();
    }

    public Position toEntityModel() {
        PositionImpl positionImpl = new PositionImpl();

        positionImpl.setPositionId(positionId);

        if (fiscalYear == null) {
            positionImpl.setFiscalYear(StringPool.BLANK);
        } else {
            positionImpl.setFiscalYear(fiscalYear);
        }

        if (description == null) {
            positionImpl.setDescription(StringPool.BLANK);
        } else {
            positionImpl.setDescription(description);
        }

        if (categoryCode == null) {
            positionImpl.setCategoryCode(StringPool.BLANK);
        } else {
            positionImpl.setCategoryCode(categoryCode);
        }

        if (offerNumber == null) {
            positionImpl.setOfferNumber(StringPool.BLANK);
        } else {
            positionImpl.setOfferNumber(offerNumber);
        }

        if (offerDate == Long.MIN_VALUE) {
            positionImpl.setOfferDate(null);
        } else {
            positionImpl.setOfferDate(new Date(offerDate));
        }

        if (quatity == null) {
            positionImpl.setQuatity(StringPool.BLANK);
        } else {
            positionImpl.setQuatity(quatity);
        }

        if (deliveryDate == Long.MIN_VALUE) {
            positionImpl.setDeliveryDate(null);
        } else {
            positionImpl.setDeliveryDate(new Date(deliveryDate));
        }

        if (deliveryAddress == null) {
            positionImpl.setDeliveryAddress(StringPool.BLANK);
        } else {
            positionImpl.setDeliveryAddress(deliveryAddress);
        }

        if (actualUnitCost == null) {
            positionImpl.setActualUnitCost(StringPool.BLANK);
        } else {
            positionImpl.setActualUnitCost(actualUnitCost);
        }

        if (numberOfUnit == null) {
            positionImpl.setNumberOfUnit(StringPool.BLANK);
        } else {
            positionImpl.setNumberOfUnit(numberOfUnit);
        }

        positionImpl.setLabelNumber(labelNumber);
        positionImpl.setPoLabelNumber(poLabelNumber);

        if (targaTecnica == null) {
            positionImpl.setTargaTecnica(StringPool.BLANK);
        } else {
            positionImpl.setTargaTecnica(targaTecnica);
        }

        positionImpl.setApproved(approved);

        if (icApproval == null) {
            positionImpl.setIcApproval(StringPool.BLANK);
        } else {
            positionImpl.setIcApproval(icApproval);
        }

        if (approver == null) {
            positionImpl.setApprover(StringPool.BLANK);
        } else {
            positionImpl.setApprover(approver);
        }

        if (assetNumber == null) {
            positionImpl.setAssetNumber(StringPool.BLANK);
        } else {
            positionImpl.setAssetNumber(assetNumber);
        }

        if (shoppingCart == null) {
            positionImpl.setShoppingCart(StringPool.BLANK);
        } else {
            positionImpl.setShoppingCart(shoppingCart);
        }

        if (poNumber == null) {
            positionImpl.setPoNumber(StringPool.BLANK);
        } else {
            positionImpl.setPoNumber(poNumber);
        }

        if (evoLocation == null) {
            positionImpl.setEvoLocation(StringPool.BLANK);
        } else {
            positionImpl.setEvoLocation(evoLocation);
        }

        if (evoCodMateriale == null) {
            positionImpl.setEvoCodMateriale(StringPool.BLANK);
        } else {
            positionImpl.setEvoCodMateriale(evoCodMateriale);
        }
        
        if (evoDesMateriale == null) {
            positionImpl.setEvoDesMateriale(StringPool.BLANK);
        } else {
            positionImpl.setEvoDesMateriale(evoDesMateriale);
        }
        
        if (wbsCodeId == null) {
            positionImpl.setWbsCodeId(StringPool.BLANK);
        } else {
            positionImpl.setWbsCodeId(wbsCodeId);
        }

        if (purchaseRequestId == null) {
            positionImpl.setPurchaseRequestId(StringPool.BLANK);
        } else {
            positionImpl.setPurchaseRequestId(purchaseRequestId);
        }

        if (purchaseOrderId == null) {
            positionImpl.setPurchaseOrderId(StringPool.BLANK);
        } else {
            positionImpl.setPurchaseOrderId(purchaseOrderId);
        }

        if (createdDate == Long.MIN_VALUE) {
            positionImpl.setCreatedDate(null);
        } else {
            positionImpl.setCreatedDate(new Date(createdDate));
        }

        positionImpl.setCreatedUserId(createdUserId);

        positionImpl.resetOriginalValues();

        return positionImpl;
    }
}
