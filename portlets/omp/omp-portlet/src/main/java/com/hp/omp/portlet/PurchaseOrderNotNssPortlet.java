package com.hp.omp.portlet;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.helper.POSearchHelper;
import com.hp.omp.helper.UploadDownloadHelper;
import com.hp.omp.model.Buyer;
import com.hp.omp.model.CostCenter;
import com.hp.omp.model.GoodReceipt;
import com.hp.omp.model.Position;
import com.hp.omp.model.Project;
import com.hp.omp.model.PurchaseOrder;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.TrackingCodes;
import com.hp.omp.model.Vendor;
import com.hp.omp.model.WbsCode;
import com.hp.omp.model.custom.Currency;
import com.hp.omp.model.custom.FileUpload;
import com.hp.omp.model.custom.GruppoUsers;
import com.hp.omp.model.custom.OmpFormatterUtil;
import com.hp.omp.model.custom.PlantType;
import com.hp.omp.model.custom.PositionStatus;
import com.hp.omp.model.custom.PurchaseOrderStatus;
import com.hp.omp.model.custom.TipoPr;
import com.hp.omp.model.impl.GoodReceiptImpl;
import com.hp.omp.model.impl.PurchaseOrderImpl;
import com.hp.omp.service.BuyerLocalServiceUtil;
import com.hp.omp.service.CostCenterLocalServiceUtil;
import com.hp.omp.service.GoodReceiptLocalServiceUtil;
import com.hp.omp.service.PositionLocalServiceUtil;
import com.hp.omp.service.ProjectLocalServiceUtil;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.hp.omp.service.TrackingCodesLocalServiceUtil;
import com.hp.omp.service.VendorLocalServiceUtil;
import com.hp.omp.service.WbsCodeLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class PurchaseOrderPortlet
 */
public class PurchaseOrderNotNssPortlet extends MVCPortlet {

	Logger logger = LoggerFactory.getLogger(PurchaseOrderNotNssPortlet.class);
	public static final String DATE_FORMAT = "dd/MM/yyyy"; 

	private Object lock  = new Object();

	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		HttpServletRequest httpServletRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(request));
		String jspPage = httpServletRequest.getParameter("jspPage");
		if(jspPage == null) {
			jspPage = ParamUtil.getString(request, "jspPage","/html/purchaseordernotnss/addpo.jsp");
		}
		handleGRForm(request,response);
		handleAddPOForm(request,response);
		handleEditPOForm(request, response);
		handlePositionGRList(request, response);

		try {
			List<WbsCode> wbsCodeList = WbsCodeLocalServiceUtil.getWbsCodes(0, WbsCodeLocalServiceUtil.getWbsCodesCount());
			request.setAttribute("allWbsCodes", wbsCodeList);



		} catch (SystemException e) {
			logger.error("error happened during retrieving WBS code/Projects list",e);
		}

		getPortletContext().getRequestDispatcher(jspPage).include(request, response);
	}


	private void handlePositionGRList(RenderRequest request,
			RenderResponse response) {
		String grForm = ParamUtil.getString(request, "GET_CMD", "");
		if ("grList".equals(grForm)) {
			try {
				String positionId = ParamUtil.getString(request, "positionId");
				List<GoodReceipt> grList = GoodReceiptLocalServiceUtil
						.getPositionGoodReceipts(positionId);
				request.setAttribute("grList", grList);
			} catch (SystemException e) {
				logger.error("Error in get Good Receipt list", e);
			}
		}
	}


	private void handleGRForm(RenderRequest request, RenderResponse response) {
		String grForm = ParamUtil.getString(request,"GET_CMD", "");
		if("grForm".equals(grForm)) {
			Long grId = ParamUtil.getLong(request, "grId");
			if(grId != 0) {
				try {
					GoodReceipt goodReceipt = GoodReceiptLocalServiceUtil.getGoodReceipt(grId);
					request.setAttribute("gr", goodReceipt);
				} catch (PortalException e) {
					logger.error("Error in get good Receipt for edit",e);
				} catch (SystemException e) {
					logger.error("Error in get good Receipt for edit",e);
				} 
			} else {
				GoodReceipt goodReceipt = new GoodReceiptImpl();
				long positionId = ParamUtil.getLong(request, "positionId", 0);
				String catCode = ParamUtil.getString(request, "catCode","");
				goodReceipt.setPositionId(String.valueOf(positionId));
				request.setAttribute("gr", goodReceipt);
				
				/* 
				 * MS 21.10.2020
				 * Verifico che per il category code indicato
				 * devo chiedere o meno di allegare la documentazione
				 */
				Properties propsCatCode = readProperties();
				request.setAttribute("attachDoc4Gr", false);
				if(!propsCatCode.getProperty("categoryCodeList").contains(catCode.toUpperCase())){
					request.setAttribute("attachDoc4Gr", true);
				}
			}
		}
	}




	private void handleAddPOForm(RenderRequest request, RenderResponse response) {

		logger.info("handleAddPOForm");
		HttpServletRequest httpServletRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(request));
		String addPoForm = httpServletRequest.getParameter("GET_ADD_PO");
		String finalStep = httpServletRequest.getParameter("finalStep");
		if("true".equals(finalStep)) {
			request.setAttribute("finalStep", true);
		}
		if("addPoForm".equals(addPoForm)) {
			String purchaseRequestId = httpServletRequest.getParameter("prId");
			String purchaseRequestIds = httpServletRequest.getParameter("prIds");
			List<Long> purchaseRequestsIDList = new ArrayList<Long>();

			if(purchaseRequestIds == null || purchaseRequestIds.isEmpty()){
				purchaseRequestIds = purchaseRequestId;
				purchaseRequestsIDList = getPoIdsList(purchaseRequestIds);
			} else {
				purchaseRequestsIDList = getPoIdsList(purchaseRequestIds);
			}


			if(purchaseRequestId == null || purchaseRequestId.isEmpty()){
				purchaseRequestId = String.valueOf(purchaseRequestsIDList.get(0));
			}


			String redirect=httpServletRequest.getParameter("redirect");

			if(redirect==null || redirect.isEmpty()){
				redirect ="/group/vodafone/home"; 
			}


			try {
				logger.info("purchaseRequestsID " + purchaseRequestId );
				//populate the purchase order from the selected purchase requests.
				PurchaseOrder purchaseOrder= retrievePurchaseOrderInfo(
						PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.parseLong(purchaseRequestId)));

				if(Currency.EUR.getCode() == Integer.valueOf(purchaseOrder.getCurrency())){
					request.setAttribute("currencyName", Currency.EUR.getLabel());
				} else if(Currency.USD.getCode() == Integer.valueOf(purchaseOrder.getCurrency())){
					request.setAttribute("currencyName", Currency.USD.getLabel());
				}

				request.setAttribute("purchaseOrder", purchaseOrder);

				if(purchaseOrder.getTotalValue() != null && ! "".equals(purchaseOrder.getTotalValue())) {
					request.setAttribute("prTotalValue", OmpFormatterUtil.formatNumberByLocale(Double.valueOf(purchaseOrder.getTotalValue()),Locale.ITALY));
				}else {
					request.setAttribute("prTotalValue","0");
				}

				//Retrieve positions of selected purchase requests.
				List<Position> availablePositions = new ArrayList<Position>();
				List<Position> positionsPerPR = null;
				int positionStatus = PositionStatus.SC_RECEIVED.getCode();

				if(purchaseOrder.getAutomatic())
					positionStatus = PositionStatus.NEW.getCode();

				for ( Long prID: purchaseRequestsIDList){

					positionsPerPR = new ArrayList<Position>();
					positionsPerPR = PositionLocalServiceUtil.getPositionsNssByStatus(prID,positionStatus);	

					for(Position pos : positionsPerPR){
						availablePositions.add(pos);
					}
				}
				request.setAttribute("availablePositions",availablePositions);
				
				List<Vendor> allVendorsList= VendorLocalServiceUtil.getVendors(0, VendorLocalServiceUtil.getVendorsCount());
				List<Vendor> vendorListVbs = new ArrayList<Vendor>();
				for(Vendor vendor : allVendorsList){
					if (vendor.getGruppoUsers() == GruppoUsers.GNED.getCode()){
						vendorListVbs.add(vendor);
					}
				}
				request.setAttribute("allVendors", vendorListVbs);

				List<Project> projectsList = ProjectLocalServiceUtil.getProjects(0, ProjectLocalServiceUtil.getProjectsCount());
				request.setAttribute("allProjects", projectsList);

				//cost center list
				//List<CostCenter> allCostCenters = CostCenterLocalServiceUtil.getCostCenters(0, CostCenterLocalServiceUtil.getCostCentersCount());
				List<CostCenter> allCostCenters = CostCenterLocalServiceUtil.getCostCentersList(0,
						CostCenterLocalServiceUtil.getCostCentersCount(), 
						null, 
						GruppoUsers.GNED.getCode());
				request.setAttribute("allCostCenters", allCostCenters);

				//tracking code list
				List<TrackingCodes> allTrackingCodes = TrackingCodesLocalServiceUtil.getTrackingCodeses(0, TrackingCodesLocalServiceUtil.getTrackingCodesesCount());
				request.setAttribute("allTrackingCodes", allTrackingCodes);

				//wbs code list
				List<WbsCode> allWbsCodes = WbsCodeLocalServiceUtil.getWbsCodes(0, WbsCodeLocalServiceUtil.getWbsCodesCount());
				request.setAttribute("allWbsCodes", allWbsCodes);

				//List<Buyer> buyersList = BuyerLocalServiceUtil.getBuyers(0, BuyerLocalServiceUtil.getBuyersCount());
				List<Buyer> buyersList = BuyerLocalServiceUtil.getBuyersList(0, 
						BuyerLocalServiceUtil.getBuyersCountList(null, GruppoUsers.GNED.getCode()),
						null, GruppoUsers.GNED.getCode());
				request.setAttribute("allBuyers",buyersList);

				List<PlantType> allPlants = new ArrayList<PlantType>();
				allPlants.add(PlantType.PLANT_IT3V);
				allPlants.add(PlantType.PLANT_IT4V);
				allPlants.add(PlantType.PLANT_IT5V);
				allPlants.add(PlantType.PLANT_IT6V);
				allPlants.add(PlantType.PLANT_DI2V);
				allPlants.add(PlantType.PLANT_DISV);
				request.setAttribute("allPlants", allPlants);

				request.setAttribute("redirect",redirect);

			}catch(PortalException e) {
				logger.error("Erro in handle Add PO Form",e);
			} catch(SystemException e) {
				logger.error("Erro in handle Add PO Form",e);
			} catch (Exception e) {
				logger.error("Erro in handle Add PO Form",e);
			}
		}
	}

	private void handleEditPOForm(RenderRequest request, RenderResponse response) {

		logger.info("handleEditPOForm");
		HttpServletRequest httpServletRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(request));
		String editPoForm = httpServletRequest.getParameter("GET_EDIT_PO");
		String finalStep = httpServletRequest.getParameter("finalStep");
		if("true".equals(finalStep)) {
			request.setAttribute("finalStep", true);
		}
		if("editPoForm".equals(editPoForm)) {
			String porderId = httpServletRequest.getParameter("porderId");

			String redirect=httpServletRequest.getParameter("redirect");
			if(redirect==null || redirect.isEmpty()){
				redirect ="/group/vodafone/home"; 
			}

			logger.info("edit purchase order number : " + Long.valueOf(porderId));
			try {
				PurchaseOrder purchaseOrder = PurchaseOrderLocalServiceUtil.getPurchaseOrder(Long.valueOf(porderId));

				ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
				Long userId = themeDisplay.getUserId();

				List<Position> selectedPositions = PositionLocalServiceUtil.getPurchaseOrderPositions(String.valueOf(purchaseOrder.getPurchaseOrderId()),userId);

				if(purchaseOrder.getTotalValue() == null || purchaseOrder.getTotalValue().equals("")) {
					logger.info("edit purchase order number : total value is empty for PO ............. " + porderId);
					double totalValue = 0;
					for(Position position : selectedPositions){
						totalValue =+ (Double.valueOf(position.getQuatity()) * Double.valueOf(position.getActualUnitCost()));						
					}

					logger.info("updating total value " + totalValue +" for PO ..... " + porderId);

					purchaseOrder.setTotalValue(String.valueOf(totalValue));
					PurchaseOrderLocalServiceUtil.updatePurchaseOrder(purchaseOrder, true);
					logger.info("PO "+ porderId +" total value updated Successfully ");
				}

				purchaseOrder = PurchaseOrderLocalServiceUtil.getPurchaseOrder(Long.valueOf(porderId));
				purchaseOrder.setTotalValue(OmpFormatterUtil.formatNumberByLocale(Double.valueOf(purchaseOrder.getTotalValue()), Locale.ITALY));

				if(purchaseOrder.getTotalValue() != null && ! "".equals(purchaseOrder.getTotalValue())) {
					request.setAttribute("prTotalValue", purchaseOrder.getTotalValue());
				}else {
					request.setAttribute("prTotalValue","0");
				}

				if(Currency.EUR.getCode() == Integer.valueOf(purchaseOrder.getCurrency())){
					request.setAttribute("currencyName", Currency.EUR.getLabel());
				} else if(Currency.USD.getCode() == Integer.valueOf(purchaseOrder.getCurrency())){
					request.setAttribute("currencyName", Currency.USD.getLabel());
				}

				List<Project> projectsList = ProjectLocalServiceUtil.getProjects(0, ProjectLocalServiceUtil.getProjectsCount());
				request.setAttribute("allProjects", projectsList);

				request.setAttribute("purchaseOrder", purchaseOrder);
				request.setAttribute("selectedPositions", selectedPositions);

				//Retrieve the rest positions of selected purchase requests.
				List<Position> availablePositions = new ArrayList<Position>();
				List<Position> positionsPerPR = null;

				int positionStatus = PositionStatus.SC_RECEIVED.getCode();
				if(purchaseOrder.getAutomatic())
					positionStatus = PositionStatus.NEW.getCode();

				List<Vendor> allVendorsList= VendorLocalServiceUtil.getVendors(0, VendorLocalServiceUtil.getVendorsCount());
				List<Vendor> vendorListVbs = new ArrayList<Vendor>();
				for(Vendor vendor : allVendorsList){
					if (vendor.getGruppoUsers() == GruppoUsers.GNED.getCode()){
						vendorListVbs.add(vendor);
					}
				}
				request.setAttribute("allVendors", vendorListVbs);

				//cost center list
				//List<CostCenter> allCostCenters = CostCenterLocalServiceUtil.getCostCenters(0, CostCenterLocalServiceUtil.getCostCentersCount());
				List<CostCenter> allCostCenters = CostCenterLocalServiceUtil.getCostCentersList(0,
						CostCenterLocalServiceUtil.getCostCentersCount(), 
						null, 
						GruppoUsers.GNED.getCode());
				request.setAttribute("allCostCenters", allCostCenters);

				//tracking code list
				List<TrackingCodes> allTrackingCodes = TrackingCodesLocalServiceUtil.getTrackingCodeses(0, TrackingCodesLocalServiceUtil.getTrackingCodesesCount());
				request.setAttribute("allTrackingCodes", allTrackingCodes);

				//wbs code list
				List<WbsCode> allWbsCodes = WbsCodeLocalServiceUtil.getWbsCodes(0, WbsCodeLocalServiceUtil.getWbsCodesCount());
				request.setAttribute("allWbsCodes", allWbsCodes);

				//List<Buyer> buyersList = BuyerLocalServiceUtil.getBuyers(0, BuyerLocalServiceUtil.getBuyersCount());
				List<Buyer> buyersList = BuyerLocalServiceUtil.getBuyersList(0, 
						BuyerLocalServiceUtil.getBuyersCountList(null, GruppoUsers.GNED.getCode()),
						null, GruppoUsers.GNED.getCode());
				request.setAttribute("allBuyers",buyersList);

				List<PlantType> allPlants = new ArrayList<PlantType>();
				if(purchaseOrder != null && purchaseOrder.getTipoPr() == TipoPr.NSS.getCode()){
					for(PlantType pt: PlantType.values()){
						if(pt.getCode().equals(purchaseOrder.getPlant()))
							allPlants.add(pt);
					}
				} else {
					allPlants.add(PlantType.PLANT_IT3V);
					allPlants.add(PlantType.PLANT_IT4V);
					allPlants.add(PlantType.PLANT_IT5V);
					allPlants.add(PlantType.PLANT_IT6V);
					allPlants.add(PlantType.PLANT_DI2V);
					allPlants.add(PlantType.PLANT_DISV);
				}
				request.setAttribute("allPlants", allPlants);
				
				//List<String> uploadedFiles = new ArrayList<String>();
				List<String> uploadedFiles = UploadDownloadHelper.getGRUploadedFilesNames(purchaseOrder.getPurchaseOrderId());
				request.setAttribute("uploadedFiles", uploadedFiles);

				request.setAttribute("redirect",redirect);

			}catch(PortalException e) {
				logger.error("Erro in handle Edit PO Form",e);
			} catch(SystemException e) {
				logger.error("Erro in handle Edit PO Form",e);
			} catch (Exception e) {
				logger.error("Erro in handle Edit PO Form",e);
			}
		}
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		try {

			if("getSubProjects".equals(resourceRequest.getResourceID())){
				POSearchHelper poSearchHelper = new POSearchHelper();
				try {
					poSearchHelper.getSubProjects(resourceRequest, resourceResponse);
				} catch (SystemException e) {
					logger.error("error while happend to retrieve sub projects", e);
				}
			} else if ("getLovByTrackingCode".equals(resourceRequest.getResourceID())) {
				getTrackingCodeLov(resourceRequest, resourceResponse);

			} else if("uploadFileGR".equals(resourceRequest.getResourceID())){
				uploadFileGR(resourceRequest, resourceResponse);
				
			} else if("listUploadedFileGR".equals(resourceRequest.getResourceID())){
				listUploadedFilesGR(resourceRequest, resourceResponse);
				
			} else if("downloadallFiles".equals(resourceRequest.getResourceID())){
				downloadAllFiles(resourceRequest, resourceResponse);
				
			} else if("downloadFile".equals(resourceRequest.getResourceID())){
				downloadFile(resourceRequest, resourceResponse);
				
			} else if("isUploadFileGR".equals(resourceRequest.getResourceID())){
				isUploadFileGR(resourceRequest, resourceResponse); 
				
			} else if("deleteFile".equals(resourceRequest.getResourceID())){
				deleteFile(resourceRequest, resourceResponse); 
				
			} else{
				postGRForm(resourceRequest,resourceResponse);
				deleteGRForm(resourceRequest,resourceResponse);
			}
		} catch (NumberFormatException e) {
			logger.error("error happen", e);
		} catch (PortalException e) {
			logger.error("error happen", e);
		}

	}


	private void postGRForm(ResourceRequest resourceRequest, ResourceResponse resourceResponse) 
			throws NumberFormatException, PortalException {
		
		String grForm = ParamUtil.getString(resourceRequest, "POST_CMD","");
		String jspPage = "/html/purchaseordernotnss/grlist.jsp";
		String positionId = ParamUtil.getString(resourceRequest, "positionId");
		Position position = null;
		
		ThemeDisplay themeDisplay = (ThemeDisplay)resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String userType = OmpHelper.getUserRoles(themeDisplay.getUserId());
		
		if("grForm".equals(grForm)) {
			try {
				GoodReceipt goodReceipt = bindGoodReceipt(resourceRequest);
				position = PositionLocalServiceUtil.getPosition(Long.valueOf(positionId));
				logger.debug("*******Position gr percentages is {} **************", position.getSumPositionGRPercentage());
				synchronized (lock) {
					if(goodReceipt.getGoodReceiptId() != 0) {
						GoodReceipt oldGoodReceipt = GoodReceiptLocalServiceUtil.getGoodReceipt(goodReceipt.getGoodReceiptId());
						if(((position.getSumPositionGRPercentage() - oldGoodReceipt.getPercentage()) + goodReceipt.getPercentage()) <= 100) {
							goodReceipt.setRequestDate(oldGoodReceipt.getRequestDate());
							GoodReceiptLocalServiceUtil.updateGoodReceipt(goodReceipt);
						}
					} else {
						if((position.getSumPositionGRPercentage() + goodReceipt.getPercentage()) <= 100 ) {
							goodReceipt.setRequestDate(new Date());
							goodReceipt.setCreatedUserId(String.valueOf(themeDisplay.getUserId()));
							GoodReceiptLocalServiceUtil.addGoodReceipt(goodReceipt);
						}
					}
				}
			} catch(SystemException e) {
				logger.error("Error in save or update Good Receipt", e);
			}
			try {
				List<GoodReceipt> grList = GoodReceiptLocalServiceUtil.getPositionGoodReceipts(positionId);
				resourceRequest.setAttribute("grList", grList);
				resourceRequest.setAttribute("position", position);
				getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
			} catch (PortletException e) {
				logger.error("Error in get gr list",e);
			} catch(SystemException e) {
				logger.error("Error in get gr list",e);
			}catch (IOException e) {
				logger.error("Error in get gr list",e);
			}
		}
	}

	private void deleteGRForm(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws NumberFormatException, PortalException {
		String grDelete = ParamUtil.getString(resourceRequest, "POST_CMD","");
		String jspPage = "/html/purchaseordernotnss/grlist.jsp";
		if("grDelete".equals(grDelete)) {
			Long goodReceiptId = ParamUtil.getLong(resourceRequest, "grId");
			try {
				GoodReceiptLocalServiceUtil.deleteGoodReceipt(goodReceiptId);
			} catch (PortalException e) {
				logger.error("Error in delete gr",e);
			} catch (SystemException e) {
				logger.error("Error in delete gr",e);
			}
			try {
				String positionId = ParamUtil.getString(resourceRequest, "positionId");
				List<GoodReceipt> grList = GoodReceiptLocalServiceUtil.getPositionGoodReceipts(positionId);
				resourceRequest.setAttribute("grList", grList);
				Position position = PositionLocalServiceUtil.getPosition(Long.valueOf(positionId));
				resourceRequest.setAttribute("position", position);
				getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
			} catch (PortletException e) {
				logger.error("Error in get gr list",e);
			} catch(SystemException e) {
				logger.error("Error in get gr list",e);
			}catch (IOException e) {
				logger.error("Error in get gr list",e);
			}
		}

	}

	private GoodReceipt bindGoodReceipt(ResourceRequest request) throws NumberFormatException, PortalException, SystemException {
		Long grId = ParamUtil.getLong(request, "grId");
		GoodReceipt goodReceipt;
		if(grId != 0) {
			goodReceipt = GoodReceiptLocalServiceUtil.getGoodReceipt(grId);
		} else {
			goodReceipt = new GoodReceiptImpl();
			goodReceipt.setPositionId(ParamUtil.getString(request, "positionId"));
		}
		double percentage = ParamUtil.getDouble(request, "percentage", 0D);
		goodReceipt.setPercentage(percentage);
		double total = Double.valueOf(goodReceipt.getPosition().getActualUnitCost()) * Double.valueOf(goodReceipt.getPosition().getQuatity());
		
		double t = arrotonda(total, 2);
		double value = (double)(t * percentage)/100;
		
		goodReceipt.setGrValue(String.valueOf(value));
		goodReceipt.setGoodReceiptNumber(ParamUtil.getString(request, "grNumber",""));
		DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
		goodReceipt.setRegisterationDate(ParamUtil.getDate(request, "registerationDate",dateFormat,null));
		
		String grFile = ParamUtil.getString(request, "fileUploaded","");
		if(grFile.trim().length() > 0)
			grFile = grFile.substring(grFile.lastIndexOf("\\") + 1);
		
		goodReceipt.setGrFile(grFile);
		
		return goodReceipt;
	}

	private static double arrotonda(double value, int numCifreDecimali) {
		double temp = Math.pow(10, numCifreDecimali);
		return Math.round(value * temp) / temp; 
	}


	private List<Long> getPoIdsList(String poIds){
		List<Long> poIdList = new ArrayList<Long>();
		if(poIds != null && !poIds.isEmpty()){
			for(String id : Arrays.asList(poIds.split(","))){
				poIdList.add(Long.parseLong(id));
			}
		}

		return poIdList;
	}

	
	public void postPOForm(ActionRequest request, ActionResponse response) throws IOException, PortletException {

		long poid = Long.valueOf(ParamUtil.getString(request, "poid"));
		logger.info("postPOForm Id "+poid);

		try {
			if(poid != 0) {
				doUpdate(request, response, poid);
			} else {
				doAdd(request, response);
			}
		} catch(Exception e) {
			logger.error("Error in save or update Good Receipt", e);
		}
		
		try {
			sendRedirect(request, response);
		}catch (IOException e) {
			logger.error("Error in get po list",e);
		}

	}

	private void doAdd(ActionRequest request, ActionResponse response) throws IOException, PortletException {

		try {
			String[] positions = ParamUtil.getParameterValues(request, "positions");
			logger.info("doAdd");

			Long poNumber = 0L;
			String assetNumber = null;
			String shoppingCart = null;
			
			for (int i = 0 ; i < positions.length ; i++) {
				String positionId = positions[i];
				
				if(i == 0){
					// 10.10.2019
					// E' stato richiesto di spalmare su tutte le positions selezionate
					// il PO Number inserito nella prima position
					poNumber = ParamUtil.getLong(request , "ponum" + positionId);
					assetNumber = ParamUtil.getString(request, "assetnum" + positionId);
					shoppingCart = ParamUtil.getString(request, "shoppingcardnum" + positionId);
				}
				
				try {
					PurchaseOrder purchaseOrder = PurchaseOrderLocalServiceUtil.getPurchaseOrder(poNumber);				
					assignPositionToPurchaseOrder(request, purchaseOrder, positionId, poNumber, assetNumber, shoppingCart);
					logger.debug("assign positions to a po.");
				} catch (PortalException e) {
					createNewPurchaseOrder(request, positionId, poNumber, assetNumber, shoppingCart);
					logger.debug("Create a new PO for the Position.");
				}
			}

			sendRedirect(request, response);

		} catch (SystemException e) {
			logger.error("Erro in Add PO Form",e);
			SessionErrors.add(request, "error-submitting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaseordernotnss/addpo.jsp");
		} catch (Exception e) {
			logger.error("Erro in Add PO Form",e);
			SessionErrors.add(request, "error-submitting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaseordernotnss/addpo.jsp");
		}

	}


	private void createNewPurchaseOrder(ActionRequest request, String positionId, 
			Long poNumber, String assetNumber, String shoppingCart) throws PortalException, SystemException {
		
		PurchaseOrder purchaseOrder = new PurchaseOrderImpl();
		//the new created po has the user enter po number.
		logger.debug("costcenter = " +ParamUtil.getString(request, "costcenter"));
		logger.debug("vendor = " + ParamUtil.getString(request, "vendor"));
		logger.debug("projectname = "+ ParamUtil.getString(request, "projectname"));
		logger.debug("ola = "+ ParamUtil.getString(request, "ola"));


		purchaseOrder.setNew(true);
		purchaseOrder.setPurchaseOrderId(poNumber);
		purchaseOrder.setCostCenterId(ParamUtil.getString(request, "costcenter"));
		purchaseOrder.setBuyerId(ParamUtil.getString(request, "buyer"));
		purchaseOrder.setVendorId(ParamUtil.getString(request, "vendor"));
		purchaseOrder.setOla(ParamUtil.getString(request, "ola"));
		purchaseOrder.setProjectId(ParamUtil.getString(request, "projectname"));
		purchaseOrder.setSubProjectId(ParamUtil.getString(request, "subproject"));
		purchaseOrder.setMacroDriverId(ParamUtil.getString(request, "macrodriver"));
		purchaseOrder.setMicroDriverId(ParamUtil.getString(request, "microdriver"));
		purchaseOrder.setOdnpCodeId(ParamUtil.getString(request, "odnpcode"));
		purchaseOrder.setOdnpNameId(ParamUtil.getString(request, "odnpname"));
		//purchaseOrder.setTotalValue(ParamUtil.getString(request, "totalvalue"));
		purchaseOrder.setCurrency(ParamUtil.getString(request, "currencyid"));
		purchaseOrder.setStatus(PurchaseOrderStatus.PO_RECEIVED.getCode());
		purchaseOrder.setAutomatic(ParamUtil.getBoolean(request, "postatus"));
		purchaseOrder.setBudgetCategoryId(ParamUtil.getString(request, "budgetCategoryId"));
		purchaseOrder.setBudgetSubCategoryId(ParamUtil.getString(request, "budgetSubCategoryId"));
		purchaseOrder.setActivityDescription(ParamUtil.getString(request, "activityDescription"));
		User currentUser = PortalUtil.getUser(request);
		purchaseOrder.setCreatedUserId(currentUser.getUserId());
		purchaseOrder.setScreenNameRequester(ParamUtil.getString(request, "screenNameReq"));
		purchaseOrder.setScreenNameReciever(ParamUtil.getString(request, "screenNameRec"));
		purchaseOrder.setWbsCodeId(ParamUtil.getString(request, "wbscode"));
		purchaseOrder.setTrackingCode(ParamUtil.getString(request, "trackingcode"));
		purchaseOrder.setCreatedDate(new Date());
		purchaseOrder.setTargaTecnica(ParamUtil.getString(request, "targatecnica"));
		purchaseOrder.setEvoLocationId(ParamUtil.getString(request, "evolocationid"));
		purchaseOrder.setTipoPr(ParamUtil.getInteger(request, "tipoPr"));
		purchaseOrder.setPlant(ParamUtil.getString(request, "plant"));

		PurchaseOrder addPurchaseOrder = PurchaseOrderLocalServiceUtil.addPurchaseOrder(purchaseOrder);
		logger.debug("The new PO Id is {}",addPurchaseOrder.getPurchaseOrderId() );
		assignPositionToPurchaseOrder(request, purchaseOrder, positionId, poNumber, assetNumber, shoppingCart);
	}


	private void assignPositionToPurchaseOrder(ActionRequest request, PurchaseOrder purchaseOrder,
			String positionId,Long poNumber, String assetNumber, String shoppingCart) throws NumberFormatException, PortalException, SystemException {

		Position position = PositionLocalServiceUtil.getPosition(Long.valueOf(positionId));
		
		logger.info("assign Position " + positionId + ": to Purchase Order Id" + purchaseOrder.getPurchaseOrderId());
		
		position.setPurchaseOrderId(String.valueOf(purchaseOrder.getPurchaseOrderId()));
		// 10.10.2019
		//position.setPoNumber(ParamUtil.getString(request , "ponum"+positionId));
		//position.setAssetNumber(ParamUtil.getString(request , "assetnum"+positionId));
		//position.setShoppingCart(ParamUtil.getString(request , "shoppingcardnum"+positionId));

		position.setPoNumber(String.valueOf(poNumber));
		position.setAssetNumber(assetNumber);
		position.setShoppingCart(shoppingCart);

		position.setPoLabelNumber(ParamUtil.getLong(request , "polabelnumber"+positionId));

		PositionLocalServiceUtil.updatePosition(position);

	}





	private void doUpdate(ActionRequest request,
			ActionResponse response ,long pOrderId) throws IOException, PortletException {

		try {
			logger.info("doUpdate");


			PurchaseOrder existingPo = PurchaseOrderLocalServiceUtil.getPurchaseOrder(pOrderId);

			existingPo.setCostCenterId(ParamUtil.getString(request, "costcenter"));
			existingPo.setBuyerId(ParamUtil.getString(request, "buyer"));
			existingPo.setVendorId(ParamUtil.getString(request, "vendor"));
			existingPo.setOla(ParamUtil.getString(request, "ola"));
			existingPo.setProjectId(ParamUtil.getString(request, "projectname"));
			existingPo.setSubProjectId(ParamUtil.getString(request, "subproject"));
			existingPo.setMacroDriverId(ParamUtil.getString(request, "macrodriver"));
			existingPo.setMicroDriverId(ParamUtil.getString(request, "microdriver"));
			existingPo.setOdnpCodeId(ParamUtil.getString(request, "odnpcode"));
			existingPo.setOdnpNameId(ParamUtil.getString(request, "odnpname"));
			existingPo.setCurrency(ParamUtil.getString(request, "currencyid"));
			existingPo.setAutomatic(ParamUtil.getBoolean(request, "postatus"));
			existingPo.setBudgetCategoryId(ParamUtil.getString(request, "budgetCategoryId"));
			existingPo.setBudgetSubCategoryId(ParamUtil.getString(request, "budgetSubCategoryId"));
			existingPo.setActivityDescription(ParamUtil.getString(request, "activityDescription"));
			existingPo.setWbsCodeId(ParamUtil.getString(request, "wbscode"));
			existingPo.setTrackingCode(ParamUtil.getString(request, "trackingcode"));
			PurchaseOrderLocalServiceUtil.updatePurchaseOrder(existingPo, true);

			List<Position> oldSelectedPositions= PositionLocalServiceUtil.getPositionsByPurchaseOrderId(pOrderId);
			String[] newSelectedPositions = ParamUtil.getParameterValues(request, "positions");

			for (int i = 0 ; i < newSelectedPositions.length ; i++) {
				String positionId = newSelectedPositions[i];
				Long poNumber = ParamUtil.getLong(request , "ponum"+positionId);
				String assetNumber = ParamUtil.getString(request, "assetnum" + positionId);
				String shoppingCart = ParamUtil.getString(request, "shoppingcardnum" + positionId);
				try {
					PurchaseOrder purchaseOrder = PurchaseOrderLocalServiceUtil.getPurchaseOrder(poNumber);
					assignPositionToPurchaseOrder(request, purchaseOrder, positionId, poNumber, assetNumber, shoppingCart);
				} catch (PortalException e) {
					createNewPurchaseOrder(request, positionId, poNumber, assetNumber, shoppingCart);
					logger.debug("Create a new PO for the Position.");
				}
			}

			List<Long> deSelectedPostions = getDetSelectedPostionsIds(newSelectedPositions,oldSelectedPositions);
			Position deSelectPosition = null;
			for(Long posId : deSelectedPostions){
				deSelectPosition = PositionLocalServiceUtil.getPosition(posId);
				PurchaseOrder savedPO = PurchaseOrderLocalServiceUtil.getPurchaseOrder(Long.valueOf(deSelectPosition.getPoNumber()));

				deSelectPosition.setPurchaseOrderId(null);
				logger.info("doUpdate(): updated Purchase Order Id  with null for position "+posId);
				deSelectPosition.setPoNumber(null);
				logger.info("doUpdate(): updated Purchase Order Number  with null for position "+posId);

				deSelectPosition.setAssetNumber(ParamUtil.getString(request , "assetnum"+posId));
				deSelectPosition.setShoppingCart(ParamUtil.getString(request , "shoppingcardnum"+posId));
				if(savedPO.getAutomatic()){
					deSelectPosition.setAssetNumber(null);
					deSelectPosition.setShoppingCart(null);
				}

				PositionLocalServiceUtil.updatePosition(deSelectPosition);
			}
			sendRedirect(request, response);

		} catch (SystemException e) {
			logger.error("Erro in Update PO Form",e);
			SessionErrors.add(request, "error-submitting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaseordernotnss/addpo.jsp");
		} catch (Exception e) {
			logger.error("Erro in Add PO Form",e);
			SessionErrors.add(request, "error-submitting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaseordernotnss/addpo.jsp");
		}

	}

	public void doDelete(ActionRequest request,
			ActionResponse response) throws IOException, PortletException {

		long pOrderId = Long.valueOf(ParamUtil.getString(request, "poid"));
		String pOrderStatus = ParamUtil.getString(request, "postatus");

		logger.info("deleted purchase order id "+pOrderId);

		try {
			HttpServletRequest httpServletRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(request));
			String redirect=httpServletRequest.getParameter("redirect");

			if(redirect==null || redirect.isEmpty()){
				redirect ="/group/vodafone/home"; 
			}

			List<Position> selectedPositions= PositionLocalServiceUtil.getPositionsByPurchaseOrderId(pOrderId);
			Position deSelectPosition = null;
			for(Position pos : selectedPositions){
				////////////////////delete GRs///////////////////////////////////
				List<GoodReceipt> positionGRS=GoodReceiptLocalServiceUtil.getPositionGoodReceipts(String.valueOf(pos.getPositionId()));

				for(GoodReceipt gr :positionGRS) {
					GoodReceiptLocalServiceUtil.deleteGoodReceipt(gr);
					logger.debug("GR No "+gr.getGoodReceiptId()+" is deleted");
				}

				///////////////////////////////////////////////////////////
				deSelectPosition = PositionLocalServiceUtil.getPosition(pos.getPositionId());
				deSelectPosition.setPurchaseRequestId(pos.getPurchaseRequestId());
				deSelectPosition.setPurchaseOrderId(null);
				logger.info("doDelete() : updated Purchase Order Id  with null for position "+pos.getPositionId());
				deSelectPosition.setPoNumber(null);
				logger.info("doDelete() : updated Purchase Order Number  with null for position "+pos.getPositionId());
				if(Boolean.parseBoolean(pOrderStatus)){
					deSelectPosition.setAssetNumber(null);
					deSelectPosition.setShoppingCart(null);
				} else {
					deSelectPosition.setAssetNumber(pos.getAssetNumber());
					deSelectPosition.setShoppingCart(pos.getShoppingCart());

				}

				PositionLocalServiceUtil.updatePosition(deSelectPosition);

			}

			PurchaseOrderLocalServiceUtil.deletePurchaseOrder(pOrderId);

			request.setAttribute("redirect", redirect);
			sendRedirect(request, response);

		} catch (PortalException e) {
			logger.error("Erro in Delete PO Form",e);
			SessionErrors.add(request, "error-deleting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaseordernotnss/addpo.jsp");
		} catch (SystemException e) {
			logger.error("Erro in Delete PO Form",e);
			SessionErrors.add(request, "error-deleting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaseordernotnss/addpo.jsp");
		} catch (Exception e) {
			logger.error("Erro in Delete PO Form",e);
			SessionErrors.add(request, "error-deleting");
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("jspPage", "/html/purchaseordernotnss/addpo.jsp");
		}

	}

	private List<Long> getDetSelectedPostionsIds(String[] newSelectedPositions,
			List<Position> oldSelectedPositions) {
		List<String> newSelectedPositionsList =  Arrays.asList(newSelectedPositions);
		List<Long> detSelectedPostions = new ArrayList<Long>();


		for(Position pos : oldSelectedPositions){

			if(!newSelectedPositionsList.contains(String.valueOf(pos.getPositionId()))){
				detSelectedPostions.add(pos.getPositionId());
			}
		}

		return detSelectedPostions;

	}

	private PurchaseOrder retrievePurchaseOrderInfo(PurchaseRequest pr) throws PortalException, SystemException{

		PurchaseOrder purchaseOrder = new PurchaseOrderImpl();

		purchaseOrder.setCostCenterId(pr.getCostCenterId());
		purchaseOrder.setBuyerId(pr.getBuyerId());
		purchaseOrder.setVendorId(pr.getVendorId());
		purchaseOrder.setProjectId(pr.getProjectId());
		purchaseOrder.setCurrency(pr.getCurrency());
		purchaseOrder.setTotalValue(pr.getTotalValue());
		purchaseOrder.setAutomatic(pr.getAutomatic());
		purchaseOrder.setScreenNameRequester(pr.getScreenNameRequester());
		purchaseOrder.setScreenNameReciever(pr.getScreenNameReciever());
		purchaseOrder.setActivityDescription(pr.getActivityDescription());
		purchaseOrder.setTrackingCode(pr.getTrackingCode());
		purchaseOrder.setWbsCodeId(pr.getWbsCodeId());
		purchaseOrder.setTargaTecnica(pr.getTargaTecnica());
		purchaseOrder.setEvoLocationId(pr.getEvoLocationId());
		purchaseOrder.setTipoPr(pr.getTipoPr());
		purchaseOrder.setPlant(pr.getPlant());
		purchaseOrder.setOla(pr.getOla());

		return purchaseOrder;

	}



	/*
	 * *Retrieve purchase requests id
	 */

	private Set<Long> getPurchaseRequestsIdList(List<Position> selectedPositions){
		Set<Long> purchaseRequestIDs = new HashSet<Long>();

		for(Position pos : selectedPositions){
			purchaseRequestIDs.add(Long.valueOf(pos.getPurchaseRequestId()));
		}
		return purchaseRequestIDs;
	}

	private void getTrackingCodeLov(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) {

		JSONObject optionsJSON = JSONFactoryUtil.createJSONObject();
		String trackingCode = ParamUtil.getString(resourceRequest, "selectedTrackingCode");
		if(trackingCode.contains("||")) {
			trackingCode = trackingCode.trim().split("\\|\\|")[0].trim();
		}
		TrackingCodes trCode;
		try {
			trCode = TrackingCodesLocalServiceUtil.getTrackingCodeByName(trackingCode);
			if(trCode != null) {
				optionsJSON.put("budgetCategoryId",trCode.getBudgetCategoryId());
				optionsJSON.put("budgetSubCategoryId",trCode.getBudgetSubCategoryId());
				optionsJSON.put("macroDriverId",trCode.getMacroDriverId());
				optionsJSON.put("microDriverId",trCode.getMicroDriverId());
				optionsJSON.put("odnpNameId",trCode.getOdnpNameId());
				optionsJSON.put("projectId",trCode.getProjectId());
				optionsJSON.put("subProjectId",trCode.getSubProjectId());
				optionsJSON.put("wbsCodeId",trCode.getWbsCodeId());
			} else {
				optionsJSON.put("empty","true");
			}

			PrintWriter writer = resourceResponse.getWriter();
			writer.write(optionsJSON.toString());
		}catch (SystemException e) {
			logger.error("error in retrieve Tracking Code list of values", e);
			optionsJSON.put("status","failure");
		} catch (IOException e) {
			logger.error("error in retrieve Tracking Code list of values", e);
			optionsJSON.put("status","failure");
		}
	}
	
	private void uploadFileGR(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		
		logger.debug("inside upload File Offerta");
		
		long purchaseOrderId = ParamUtil.getLong(request, "purchaseOrderId");

		try {
			logger.debug("file to view is " + purchaseOrderId);
			UploadDownloadHelper.uploadFile(purchaseOrderId, true,  request);
			JSONObject json = JSONFactoryUtil.createJSONObject();
			json.put("name","success");
			json.put("id",purchaseOrderId);
			
			logger.debug(" Purchase Request "+json);
			response.getWriter().print(json);
		} catch (UploadException ue) {
			logger.error(ue.getMessage());
			response.getWriter().write("failed");

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
	}
	
	private void listUploadedFilesGR(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		
		logger.debug("inside listUploadedFilesGR");
		
		long purchaseOrderId = ParamUtil.getLong(request, "purchaseOrderId");
		String jspPage = "/html/purchaseordernotnss/filelist.jsp";
		try {
			logger.debug("list files for PO ID " + purchaseOrderId);
			//List<String> uploadedFiles = new ArrayList<String>();
			List<String> uploadedFiles = UploadDownloadHelper.getGRUploadedFilesNames(purchaseOrderId);
			request.setAttribute("uploadedFiles", uploadedFiles);
			getPortletContext().getRequestDispatcher(jspPage).include(request, response);
		}catch (Exception e) {
			logger.error("error while retrieving the files list",e);
		}
	}
	
	
	public void downloadFile(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		logger.debug("inside download File");
		long purchaseOrderId = ParamUtil.getLong(request, "purchaseOrderId");
		String filename=ParamUtil.getString(request, "fileName");
		try {
			logger.debug("file to view is " + purchaseOrderId+"/"+filename);
			// Download action
			UploadDownloadHelper.downloadFileByName(purchaseOrderId,filename,response);
		} catch (UploadException ue) {
			logger.error(ue.getMessage());
			response.getWriter().write("failed");

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
	}
	
	public void downloadAllFiles(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		logger.debug("inside download All Files");
		long purchaseOrderId = ParamUtil.getLong(request, "purchaseOrderId");
		try {
			logger.debug("Zip file to view is " + purchaseOrderId);
			// Download action
			UploadDownloadHelper.downloadZipFile(purchaseOrderId,response);
		} catch (UploadException ue) {
			logger.error(ue.getMessage());
			response.getWriter().write("failed");

		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
	}
	
	private void isUploadFileGR(ResourceRequest request, ResourceResponse response) throws PortletException, IOException {

		logger.info("private void isUploadFileGR() - Start");
		
		JSONObject jsonObject = JSONFactoryUtil.createJSONObject();

		long pOrderId = Long.valueOf(ParamUtil.getString(request, "pOrderId"));
		logger.info("private void isUploadFileGR() - pOrderId: " + pOrderId);

		try {
			Properties propsCatCode = readProperties();

			List<Position> selectedPositions = PositionLocalServiceUtil.getPositionsByPurchaseOrderId(pOrderId);
			boolean viewPopup = false;
			for(Position pos : selectedPositions){
				if(!propsCatCode.getProperty("categoryCodeList").contains(pos.getCategoryCode())){
					viewPopup = true;
					break;
				}
			}
			if(viewPopup){
				jsonObject.put("isUploadFileGR","true");
			} else{
				jsonObject.put("isUploadFileGR","false");
			}
			response.getWriter().print(jsonObject);
			
		} catch(Exception e) {
			logger.error("Error in save or update Good Receipt", e);
		}

	}
	
	private void deleteFile(ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException {
		logger.debug("inside delete File");
		long purchaseOrderId = ParamUtil.getLong(request, "purchaseOrderId");
		String filename=ParamUtil.getString(request, "fileName");
		try {
			logger.debug("file to delete is " + purchaseOrderId+"/"+filename);
			// Download action
			UploadDownloadHelper.deleteFileByName(purchaseOrderId,filename);
			listUploadedFilesGR(request, response);
		} catch (Exception e) {
			logger.error(e.getMessage());
			response.getWriter().write("failed");
		}
	}
	
	private static synchronized Properties readProperties(){
		String resourceName = "categoryCode.properties";
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		
		Properties props = new Properties();
		try  {
			InputStream resourceStream = loader.getResourceAsStream(resourceName);
			props.load(resourceStream);
		    
		} catch (IOException e){
			System.out.println(e);
		}
		return props;
	}

}