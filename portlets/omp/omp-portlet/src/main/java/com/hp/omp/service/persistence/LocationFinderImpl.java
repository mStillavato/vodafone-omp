package com.hp.omp.service.persistence;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.hp.omp.model.Location;
import com.hp.omp.model.custom.LocationDTO;
import com.hp.omp.model.custom.LocationDTO;
import com.hp.omp.model.impl.LocationImpl;
import com.liferay.portal.kernel.dao.orm.ORMException;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.dao.orm.Type;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class LocationFinderImpl extends LocationPersistenceImpl implements LocationFinder{

	private static Log _log = LogFactoryUtil.getLog(LocationFinderImpl.class);
	static final String GET_LOCATION_LIST = LocationFinderImpl.class.getName()+".getLocationsList";
	static final String GET_COUNT_LOCATION = LocationFinderImpl.class.getName()+".getLocationsCountList";
	static final String GET_LOCATION_FOR_TARGA_TECNICA = LocationFinderImpl.class.getName()+".getLocationByTargaTecnica";
	static final String GET_LOCATION_FOR_LOCATION_EVO = LocationFinderImpl.class.getName()+".getLocationByLocationEvo";

	@SuppressWarnings("unchecked")
	public List<LocationDTO> getLocationsList() throws SystemException {
		
		Session session = null;
		
		try {
			session = openSession();
			
			String sql = null;
			sql = CustomSQLUtil.get(GET_LOCATION_LIST);
			SQLQuery query = session.createSQLQuery(sql);
			
			if(query == null){
				_log.error("get Location list error, no sql found");
				return null;
			}
			
			@SuppressWarnings("unused")
			List<Object[]> list = query.list();
			_log.debug("Location list :- " + list);			

			List<LocationDTO> locationList = new ArrayList<LocationDTO>();
			
			for(Object[] row:list) {
				LocationDTO locationDTO = new LocationDTO();

				locationDTO.setTargaTecnicaSap("" + row[0]);
				locationDTO.setLocation("" + row[1]);

				_log.debug("locationDTO :- " + locationDTO);
				locationList.add(locationDTO);
			}
			return locationList;			
			
		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
	}

	public long getLocationsCountList(String searchFilter) {
		Session session = null; 
		try {

			session = openSession();
			
			String sql = null;
			
			if(StringUtils.isEmpty(searchFilter))
				searchFilter = "%%";
			
			sql = CustomSQLUtil.get(GET_COUNT_LOCATION);
			SQLQuery query = session.createSQLQuery(sql);
			if(query == null){
				_log.error("get location count list error, no sql found");
				return 0;
			}
			
			query.addScalar("COUNT_VALUE", Type.LONG);

			@SuppressWarnings("unchecked")
			List<Long> list = query.list();
			if(list != null && list.size() > 0) {
				return list.get(0);
			}
			return 0L;
		} finally {
			if(session != null) {
				closeSession(session);
			}
		}
	}

	private String remove_zeros(String str) {
		return str.replaceFirst("^0+(?!$)", "");
	}
	
	private String formatDate(String date){
		try{
			String anno = date.substring(0, 4);
			String mese = date.substring(5, 7);
			String giorno = date.substring(8, 10);
			 
			return (giorno + '/' + mese + '/' + anno);
		}catch(Exception e){
			return "";
		}
	}

	public LocationDTO getLocationByTargaTecnica(String targaTecnica)
			throws SystemException {
		
		Session session = null;
		try {
			session  = openSession();

			String sql = null;
			sql = CustomSQLUtil.get(GET_LOCATION_FOR_TARGA_TECNICA);
			SQLQuery query = session.createSQLQuery(sql);

			if(query == null){
				_log.error("get location for targa tecnica list error, no sql found");
				return null;
			}
			
			query.setString(0, targaTecnica);

			@SuppressWarnings("unused")
			List<Object[]> list = query.list();
			_log.debug("Location list :- " + list);			

			LocationDTO locationDTO = new LocationDTO();
			if(list != null && list.size() > 0){
				locationDTO.setTargaTecnicaSap("" + list.get(0)[0]);
				locationDTO.setLocation("" + list.get(0)[1]);
			} else{
				locationDTO = null;
			}
			
			return locationDTO;	
			
		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
	}

	
	public LocationDTO getLocationByLocationEvo(String locationEvo)
			throws SystemException {
		
		Session session = null;
		try {
			session  = openSession();

			String sql = null;
			sql = CustomSQLUtil.get(GET_LOCATION_FOR_LOCATION_EVO);
			SQLQuery query = session.createSQLQuery(sql);

			if(query == null){
				_log.error("get location for location evo list error, no sql found");
				return null;
			}
			
			query.setString(0, locationEvo);

			@SuppressWarnings("unused")
			List<Object[]> list = query.list();
			_log.debug("Location list :- " + list);			

			LocationDTO locationDTO = new LocationDTO();
			if(list != null && list.size() > 0){
				locationDTO.setTargaTecnicaSap("" + list.get(0)[0]);
				locationDTO.setLocation("" + list.get(0)[1]);
			} else{
				locationDTO = null;
			}
			
			return locationDTO;			
			
		} catch (ORMException e) {
			_log.error(e);
			throw processException(e);
		} finally{
			closeSession(session);
		}
	}
}
