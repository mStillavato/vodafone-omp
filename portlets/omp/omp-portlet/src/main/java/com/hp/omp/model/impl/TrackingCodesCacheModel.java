package com.hp.omp.model.impl;

import com.hp.omp.model.TrackingCodes;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

/**
 * The cache model class for representing TrackingCodes in entity cache.
 *
 * @author HP Egypt team
 * @see TrackingCodes
 * @generated
 */
public class TrackingCodesCacheModel implements CacheModel<TrackingCodes>,
    Serializable {
    public long trackingCodeId;
    public String trackingCodeName;
    public String trackingCodeDescription;
    public String projectId;
    public String subProjectId;
    public String budgetCategoryId;
    public String budgetSubCategoryId;
    public String macroDriverId;
    public String microDriverId;
    public String odnpNameId;
    public String wbsCodeId;
    public boolean display;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(25);

        sb.append("{trackingCodeId=");
        sb.append(trackingCodeId);
        sb.append(", trackingCodeName=");
        sb.append(trackingCodeName);
        sb.append(", trackingCodeDescription=");
        sb.append(trackingCodeDescription);
        sb.append(", projectId=");
        sb.append(projectId);
        sb.append(", subProjectId=");
        sb.append(subProjectId);
        sb.append(", budgetCategoryId=");
        sb.append(budgetCategoryId);
        sb.append(", budgetSubCategoryId=");
        sb.append(budgetSubCategoryId);
        sb.append(", macroDriverId=");
        sb.append(macroDriverId);
        sb.append(", microDriverId=");
        sb.append(microDriverId);
        sb.append(", odnpNameId=");
        sb.append(odnpNameId);
        sb.append(", wbsCodeId=");
        sb.append(wbsCodeId);
        sb.append(", display=");
        sb.append(display);
        sb.append("}");

        return sb.toString();
    }

    public TrackingCodes toEntityModel() {
        TrackingCodesImpl trackingCodesImpl = new TrackingCodesImpl();

        trackingCodesImpl.setTrackingCodeId(trackingCodeId);

        if (trackingCodeName == null) {
            trackingCodesImpl.setTrackingCodeName(StringPool.BLANK);
        } else {
            trackingCodesImpl.setTrackingCodeName(trackingCodeName);
        }

        if (trackingCodeDescription == null) {
            trackingCodesImpl.setTrackingCodeDescription(StringPool.BLANK);
        } else {
            trackingCodesImpl.setTrackingCodeDescription(trackingCodeDescription);
        }

        if (projectId == null) {
            trackingCodesImpl.setProjectId(StringPool.BLANK);
        } else {
            trackingCodesImpl.setProjectId(projectId);
        }

        if (subProjectId == null) {
            trackingCodesImpl.setSubProjectId(StringPool.BLANK);
        } else {
            trackingCodesImpl.setSubProjectId(subProjectId);
        }

        if (budgetCategoryId == null) {
            trackingCodesImpl.setBudgetCategoryId(StringPool.BLANK);
        } else {
            trackingCodesImpl.setBudgetCategoryId(budgetCategoryId);
        }

        if (budgetSubCategoryId == null) {
            trackingCodesImpl.setBudgetSubCategoryId(StringPool.BLANK);
        } else {
            trackingCodesImpl.setBudgetSubCategoryId(budgetSubCategoryId);
        }

        if (macroDriverId == null) {
            trackingCodesImpl.setMacroDriverId(StringPool.BLANK);
        } else {
            trackingCodesImpl.setMacroDriverId(macroDriverId);
        }

        if (microDriverId == null) {
            trackingCodesImpl.setMicroDriverId(StringPool.BLANK);
        } else {
            trackingCodesImpl.setMicroDriverId(microDriverId);
        }

        if (odnpNameId == null) {
            trackingCodesImpl.setOdnpNameId(StringPool.BLANK);
        } else {
            trackingCodesImpl.setOdnpNameId(odnpNameId);
        }

        if (wbsCodeId == null) {
            trackingCodesImpl.setWbsCodeId(StringPool.BLANK);
        } else {
            trackingCodesImpl.setWbsCodeId(wbsCodeId);
        }

        trackingCodesImpl.setDisplay(display);

        trackingCodesImpl.resetOriginalValues();

        return trackingCodesImpl;
    }
}
