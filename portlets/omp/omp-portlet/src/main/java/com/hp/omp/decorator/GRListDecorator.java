package com.hp.omp.decorator;

import org.displaytag.decorator.TableDecorator;

import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.custom.Currency;
import com.hp.omp.model.custom.ListDTO;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;


public class GRListDecorator extends TableDecorator {
	
	private static Log _log = LogFactoryUtil.getLog(com.hp.omp.decorator.GRListDecorator.class);
	
	ListDTO item = null;
	
	public Integer getIdentifier(){
		item = (ListDTO) this.getCurrentRowObject();
		String idString = item.getIdentifier();
		Integer id = 0;
		try {
			id = Integer.parseInt(idString);
		} catch (NumberFormatException e) {
			_log.error("Parsing Error", e);
		}
		return id;
	}
	
	public String getTotalValue (){
		
		item = (ListDTO) this.getCurrentRowObject();
		
		String totalValue = "";
		
		totalValue = item.getTotalValue();
		
		if(totalValue != null && !("").equals(totalValue)){
			String currency = "";
			if(item.getCurrency().equals(Currency.USD.getCode()))
				currency = "$";
			else if (item.getCurrency().equals(Currency.EUR.getCode()))
				currency = "€";
			totalValue = totalValue + " " + currency;
		}
		else{
			totalValue = "-";
		}

		return totalValue;
	}
	
}
