/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service.impl;

import java.util.List;

import com.hp.omp.model.custom.LayoutNotNssCatalogoListDTO;
import com.hp.omp.model.custom.SearchDTO;
import com.hp.omp.service.base.LayoutNotNssCatalogoLocalServiceBaseImpl;
import com.hp.omp.service.persistence.LayoutNotNssCatalogoFinderUtil;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * The implementation of the layoutNotNssCatalogo local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.LayoutNotNssCatalogoLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author HP Egypt team
 * @see com.hp.omp.service.base.LayoutNotNssCatalogoLocalServiceBaseImpl
 * @see com.hp.omp.service.LayoutNotNssCatalogoLocalServiceUtil
 */
public class LayoutNotNssCatalogoLocalServiceImpl extends LayoutNotNssCatalogoLocalServiceBaseImpl {

	public List<LayoutNotNssCatalogoListDTO> getLayoutNotNssCatalogoList(String user) {
		return LayoutNotNssCatalogoFinderUtil.getLayoutNotNssCatalogoList(user);
	}

	public Long getLayoutNotNssCatalogoListCount(String user) {
		return LayoutNotNssCatalogoFinderUtil.getLayoutNotNssCatalogoListCount(user);
	}
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.hp.omp.service.LayoutNotNssCatalogoLocalServiceUtil} to access the layoutNotNssCatalogo local service.
	 */

	public void deleteLayoutNotNssCatalogo4User(String user)
			throws SystemException {
		LayoutNotNssCatalogoFinderUtil.deleteLayoutNotNssCatalogo4User(user);
		
	}

	
}