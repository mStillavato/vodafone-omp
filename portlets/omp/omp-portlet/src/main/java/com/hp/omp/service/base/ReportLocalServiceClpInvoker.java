package com.hp.omp.service.base;

import com.hp.omp.service.ReportLocalServiceUtil;

import java.util.Arrays;


public class ReportLocalServiceClpInvoker {
    private String _methodName130;
    private String[] _methodParameterTypes130;
    private String _methodName131;
    private String[] _methodParameterTypes131;
    private String _methodName136;
    private String[] _methodParameterTypes136;
    private String _methodName137;
    private String[] _methodParameterTypes137;
    private String _methodName138;
    private String[] _methodParameterTypes138;
    private String _methodName139;
    private String[] _methodParameterTypes139;
    private String _methodName140;
    private String[] _methodParameterTypes140;
    private String _methodName141;
    private String[] _methodParameterTypes141;
    private String _methodName142;
    private String[] _methodParameterTypes142;
    private String _methodName143;
    private String[] _methodParameterTypes143;
    private String _methodName144;
    private String[] _methodParameterTypes144;
    private String _methodName145;
    private String[] _methodParameterTypes145;
    private String _methodName146;
    private String[] _methodParameterTypes146;

    public ReportLocalServiceClpInvoker() {
        _methodName130 = "getBeanIdentifier";

        _methodParameterTypes130 = new String[] {  };

        _methodName131 = "setBeanIdentifier";

        _methodParameterTypes131 = new String[] { "java.lang.String" };

        _methodName136 = "getAllCostCenters";

        _methodParameterTypes136 = new String[] {  };

        _methodName137 = "getAllProjectNames";

        _methodParameterTypes137 = new String[] {  };

        _methodName138 = "getSubProjects";

        _methodParameterTypes138 = new String[] { "java.lang.Long" };

        _methodName139 = "getAllFiscalYears";

        _methodParameterTypes139 = new String[] {  };

        _methodName140 = "generateAggregateReportClosed";

        _methodParameterTypes140 = new String[] {
                "com.hp.omp.model.custom.ReportDTO"
            };

        _methodName141 = "generateAggregateReportClosing";

        _methodParameterTypes141 = new String[] {
                "com.hp.omp.model.custom.ReportDTO"
            };

        _methodName142 = "generateAggregateReportTotalPoistionValues";

        _methodParameterTypes142 = new String[] {
                "com.hp.omp.model.custom.ReportDTO"
            };

        _methodName143 = "generateAggregatePrPositionsWithoutPoNumberValue";

        _methodParameterTypes143 = new String[] {
                "com.hp.omp.model.custom.ReportDTO"
            };

        _methodName144 = "generateDetailReport";

        _methodParameterTypes144 = new String[] {
                "com.hp.omp.model.custom.ReportDTO"
            };

        _methodName145 = "exportDetailsReportAsExcel";

        _methodParameterTypes145 = new String[] {
                "com.hp.omp.model.custom.ReportDTO", "java.util.List",
                "java.io.OutputStream"
            };

        _methodName146 = "exportReportAsExcel";

        _methodParameterTypes146 = new String[] {
                "com.hp.omp.model.custom.ReportDTO", "java.util.List",
                "java.io.OutputStream"
            };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName130.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes130, parameterTypes)) {
            return ReportLocalServiceUtil.getBeanIdentifier();
        }

        if (_methodName131.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes131, parameterTypes)) {
            ReportLocalServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName136.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes136, parameterTypes)) {
            return ReportLocalServiceUtil.getAllCostCenters();
        }

        if (_methodName137.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes137, parameterTypes)) {
            return ReportLocalServiceUtil.getAllProjectNames();
        }

        if (_methodName138.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes138, parameterTypes)) {
            return ReportLocalServiceUtil.getSubProjects((java.lang.Long) arguments[0]);
        }

        if (_methodName139.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes139, parameterTypes)) {
            return ReportLocalServiceUtil.getAllFiscalYears();
        }

        if (_methodName140.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes140, parameterTypes)) {
            return ReportLocalServiceUtil.generateAggregateReportClosed((com.hp.omp.model.custom.ReportDTO) arguments[0]);
        }

        if (_methodName141.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes141, parameterTypes)) {
            return ReportLocalServiceUtil.generateAggregateReportClosing((com.hp.omp.model.custom.ReportDTO) arguments[0]);
        }

        if (_methodName142.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes142, parameterTypes)) {
            return ReportLocalServiceUtil.generateAggregateReportTotalPoistionValues((com.hp.omp.model.custom.ReportDTO) arguments[0]);
        }

        if (_methodName143.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes143, parameterTypes)) {
            return ReportLocalServiceUtil.generateAggregatePrPositionsWithoutPoNumberValue((com.hp.omp.model.custom.ReportDTO) arguments[0]);
        }

        if (_methodName144.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes144, parameterTypes)) {
            return ReportLocalServiceUtil.generateDetailReport((com.hp.omp.model.custom.ReportDTO) arguments[0]);
        }

        if (_methodName145.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes145, parameterTypes)) {
            ReportLocalServiceUtil.exportDetailsReportAsExcel((com.hp.omp.model.custom.ReportDTO) arguments[0],
                (java.util.List<java.lang.Object>) arguments[1],
                (java.io.OutputStream) arguments[2]);

            return null;
        }

        if (_methodName146.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes146, parameterTypes)) {
            ReportLocalServiceUtil.exportReportAsExcel((com.hp.omp.model.custom.ReportDTO) arguments[0],
                (java.util.List<java.lang.Object>) arguments[1],
                (java.io.OutputStream) arguments[2]);

            return null;
        }

        throw new UnsupportedOperationException();
    }
}
