package com.hp.omp.service.base;

import com.hp.omp.service.PositionLocalServiceUtil;

import java.util.Arrays;


public class PositionLocalServiceClpInvoker {
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName2;
    private String[] _methodParameterTypes2;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;
    private String _methodName9;
    private String[] _methodParameterTypes9;
    private String _methodName10;
    private String[] _methodParameterTypes10;
    private String _methodName11;
    private String[] _methodParameterTypes11;
    private String _methodName12;
    private String[] _methodParameterTypes12;
    private String _methodName13;
    private String[] _methodParameterTypes13;
    private String _methodName14;
    private String[] _methodParameterTypes14;
    private String _methodName15;
    private String[] _methodParameterTypes15;
    private String _methodName146;
    private String[] _methodParameterTypes146;
    private String _methodName147;
    private String[] _methodParameterTypes147;
    private String _methodName152;
    private String[] _methodParameterTypes152;
    private String _methodName153;
    private String[] _methodParameterTypes153;
    private String _methodName154;
    private String[] _methodParameterTypes154;
    private String _methodName155;
    private String[] _methodParameterTypes155;
    private String _methodName156;
    private String[] _methodParameterTypes156;
    private String _methodName157;
    private String[] _methodParameterTypes157;
    private String _methodName164;
    private String[] _methodParameterTypes164;
    private String _methodName165;
    private String[] _methodParameterTypes165;
    private String _methodName166;
    private String[] _methodParameterTypes166;
    private String _methodName167;
    private String[] _methodParameterTypes167;
    private String _methodName168;
    private String[] _methodParameterTypes168;
    private String _methodName169;
    private String[] _methodParameterTypes169;
    private String _methodName170;
    private String[] _methodParameterTypes170;
    private String _methodName171;
    private String[] _methodParameterTypes171;
    private String _methodName172;
    private String[] _methodParameterTypes172;
    private String _methodName173;
    private String[] _methodParameterTypes173;
    private String _methodName174;
    private String[] _methodParameterTypes174;
    private String _methodName175;
    private String[] _methodParameterTypes175;
    private String _methodName176;
    private String[] _methodParameterTypes176;
    private String _methodName177;
    private String[] _methodParameterTypes177;
    private String _methodName182;
    private String[] _methodParameterTypes182;
    private String _methodName183;
    private String[] _methodParameterTypes183;
    private String _methodName186;
    private String[] _methodParameterTypes186;
    private String _methodName187;
    private String[] _methodParameterTypes187;
    private String _methodName188;
    private String[] _methodParameterTypes188;
    private String _methodName189;
    private String[] _methodParameterTypes189;
    private String _methodName190;
    private String[] _methodParameterTypes190;

    public PositionLocalServiceClpInvoker() {
        _methodName0 = "addPosition";

        _methodParameterTypes0 = new String[] { "com.hp.omp.model.Position" };

        _methodName1 = "createPosition";

        _methodParameterTypes1 = new String[] { "long" };

        _methodName2 = "deletePosition";

        _methodParameterTypes2 = new String[] { "long" };

        _methodName3 = "deletePosition";

        _methodParameterTypes3 = new String[] { "com.hp.omp.model.Position" };

        _methodName4 = "dynamicQuery";

        _methodParameterTypes4 = new String[] {  };

        _methodName5 = "dynamicQuery";

        _methodParameterTypes5 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName6 = "dynamicQuery";

        _methodParameterTypes6 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
            };

        _methodName7 = "dynamicQuery";

        _methodParameterTypes7 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName8 = "dynamicQueryCount";

        _methodParameterTypes8 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName9 = "fetchPosition";

        _methodParameterTypes9 = new String[] { "long" };

        _methodName10 = "getPosition";

        _methodParameterTypes10 = new String[] { "long" };

        _methodName11 = "getPersistedModel";

        _methodParameterTypes11 = new String[] { "java.io.Serializable" };

        _methodName12 = "getPositions";

        _methodParameterTypes12 = new String[] { "int", "int" };

        _methodName13 = "getPositionsCount";

        _methodParameterTypes13 = new String[] {  };

        _methodName14 = "updatePosition";

        _methodParameterTypes14 = new String[] { "com.hp.omp.model.Position" };

        _methodName15 = "updatePosition";

        _methodParameterTypes15 = new String[] {
                "com.hp.omp.model.Position", "boolean"
            };

        _methodName146 = "getBeanIdentifier";

        _methodParameterTypes146 = new String[] {  };

        _methodName147 = "setBeanIdentifier";

        _methodParameterTypes147 = new String[] { "java.lang.String" };

        _methodName152 = "addPosition";

        _methodParameterTypes152 = new String[] { "com.hp.omp.model.Position" };

        _methodName153 = "deletePosition";

        _methodParameterTypes153 = new String[] { "long" };

        _methodName154 = "deletePosition";

        _methodParameterTypes154 = new String[] { "com.hp.omp.model.Position" };

        _methodName155 = "updatePosition";

        _methodParameterTypes155 = new String[] { "com.hp.omp.model.Position" };

        _methodName156 = "updatePosition";

        _methodParameterTypes156 = new String[] {
                "com.hp.omp.model.Position", "boolean"
            };

        _methodName157 = "updatePositionWithoutHocks";

        _methodParameterTypes157 = new String[] {
                "com.hp.omp.model.Position", "boolean"
            };

        _methodName164 = "getPositionsMaxRegisterationDate";

        _methodParameterTypes164 = new String[] { "long", "java.lang.String" };

        _methodName165 = "getPurchaseOrderPositions";

        _methodParameterTypes165 = new String[] { "java.lang.String", "long" };

        _methodName166 = "getPurchaseRequestPositionsCount";

        _methodParameterTypes166 = new String[] { "com.hp.omp.model.Position" };

        _methodName167 = "getPurchaseRequestPostionsByStatusCount";

        _methodParameterTypes167 = new String[] {
                "com.hp.omp.model.Position",
                "com.hp.omp.model.custom.PositionStatus"
            };

        _methodName168 = "getPurchaseRequestPositions";

        _methodParameterTypes168 = new String[] { "java.lang.String" };

        _methodName169 = "getPositionsByPrId";

        _methodParameterTypes169 = new String[] { "long" };

        _methodName170 = "getPrPositionsMinimumDeliveryDate";

        _methodParameterTypes170 = new String[] { "long" };

        _methodName171 = "getPoRecievedPrPositionsMinimumDeliveryDate";

        _methodParameterTypes171 = new String[] { "long" };

        _methodName172 = "getPoPositionsMinimumDeliveryDate";

        _methodParameterTypes172 = new String[] { "long" };

        _methodName173 = "getPositionsMaxLabelNumber";

        _methodParameterTypes173 = new String[] { "long" };

        _methodName174 = "getPositionsByStatus";

        _methodParameterTypes174 = new String[] { "long", "int" };

        _methodName175 = "getPositionsByStatusOnly";

        _methodParameterTypes175 = new String[] { "int" };

        _methodName176 = "getPositionsByPurchaseOrderId";

        _methodParameterTypes176 = new String[] { "long" };

        _methodName177 = "getPositionsByWbsCodeId";

        _methodParameterTypes177 = new String[] { "java.lang.String" };

        _methodName182 = "getAllFiscalYears";

        _methodParameterTypes182 = new String[] {  };

        _methodName183 = "getAllWBSCodes";

        _methodParameterTypes183 = new String[] {  };

        _methodName186 = "getIAndCPositionsCount";

        _methodParameterTypes186 = new String[] { "java.lang.String" };

        _methodName187 = "getPositionsByIAndCType";

        _methodParameterTypes187 = new String[] { "long", "java.lang.String" };

        _methodName188 = "getPositionsByIAndCTypeAndApprovalStatus";

        _methodParameterTypes188 = new String[] {
                "long", "java.lang.String", "boolean"
            };

        _methodName189 = "getIAndCPositions";

        _methodParameterTypes189 = new String[] { "long" };

        _methodName190 = "copyWBSfromPR";

        _methodParameterTypes190 = new String[] { "com.hp.omp.model.Position" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName0.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
            return PositionLocalServiceUtil.addPosition((com.hp.omp.model.Position) arguments[0]);
        }

        if (_methodName1.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
            return PositionLocalServiceUtil.createPosition(((Long) arguments[0]).longValue());
        }

        if (_methodName2.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
            return PositionLocalServiceUtil.deletePosition(((Long) arguments[0]).longValue());
        }

        if (_methodName3.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
            return PositionLocalServiceUtil.deletePosition((com.hp.omp.model.Position) arguments[0]);
        }

        if (_methodName4.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
            return PositionLocalServiceUtil.dynamicQuery();
        }

        if (_methodName5.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
            return PositionLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName6.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
            return PositionLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName7.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
            return PositionLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[3]);
        }

        if (_methodName8.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
            return PositionLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName9.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
            return PositionLocalServiceUtil.fetchPosition(((Long) arguments[0]).longValue());
        }

        if (_methodName10.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
            return PositionLocalServiceUtil.getPosition(((Long) arguments[0]).longValue());
        }

        if (_methodName11.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
            return PositionLocalServiceUtil.getPersistedModel((java.io.Serializable) arguments[0]);
        }

        if (_methodName12.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
            return PositionLocalServiceUtil.getPositions(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName13.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
            return PositionLocalServiceUtil.getPositionsCount();
        }

        if (_methodName14.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
            return PositionLocalServiceUtil.updatePosition((com.hp.omp.model.Position) arguments[0]);
        }

        if (_methodName15.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
            return PositionLocalServiceUtil.updatePosition((com.hp.omp.model.Position) arguments[0],
                ((Boolean) arguments[1]).booleanValue());
        }

        if (_methodName146.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes146, parameterTypes)) {
            return PositionLocalServiceUtil.getBeanIdentifier();
        }

        if (_methodName147.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes147, parameterTypes)) {
            PositionLocalServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName152.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes152, parameterTypes)) {
            return PositionLocalServiceUtil.addPosition((com.hp.omp.model.Position) arguments[0]);
        }

        if (_methodName153.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes153, parameterTypes)) {
            return PositionLocalServiceUtil.deletePosition(((Long) arguments[0]).longValue());
        }

        if (_methodName154.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes154, parameterTypes)) {
            return PositionLocalServiceUtil.deletePosition((com.hp.omp.model.Position) arguments[0]);
        }

        if (_methodName155.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes155, parameterTypes)) {
            return PositionLocalServiceUtil.updatePosition((com.hp.omp.model.Position) arguments[0]);
        }

        if (_methodName156.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes156, parameterTypes)) {
            return PositionLocalServiceUtil.updatePosition((com.hp.omp.model.Position) arguments[0],
                ((Boolean) arguments[1]).booleanValue());
        }

        if (_methodName157.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes157, parameterTypes)) {
            return PositionLocalServiceUtil.updatePositionWithoutHocks((com.hp.omp.model.Position) arguments[0],
                ((Boolean) arguments[1]).booleanValue());
        }

        if (_methodName164.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes164, parameterTypes)) {
            return PositionLocalServiceUtil.getPositionsMaxRegisterationDate(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1]);
        }

        if (_methodName165.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes165, parameterTypes)) {
            return PositionLocalServiceUtil.getPurchaseOrderPositions((java.lang.String) arguments[0],
                ((Long) arguments[1]).longValue());
        }

        if (_methodName166.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes166, parameterTypes)) {
            return PositionLocalServiceUtil.getPurchaseRequestPositionsCount((com.hp.omp.model.Position) arguments[0]);
        }

        if (_methodName167.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes167, parameterTypes)) {
            return PositionLocalServiceUtil.getPurchaseRequestPostionsByStatusCount((com.hp.omp.model.Position) arguments[0],
                (com.hp.omp.model.custom.PositionStatus) arguments[1]);
        }

        if (_methodName168.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes168, parameterTypes)) {
            return PositionLocalServiceUtil.getPurchaseRequestPositions((java.lang.String) arguments[0]);
        }

        if (_methodName169.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes169, parameterTypes)) {
            return PositionLocalServiceUtil.getPositionsByPrId(((Long) arguments[0]).longValue());
        }

        if (_methodName170.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes170, parameterTypes)) {
            return PositionLocalServiceUtil.getPrPositionsMinimumDeliveryDate(((Long) arguments[0]).longValue());
        }

        if (_methodName171.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes171, parameterTypes)) {
            return PositionLocalServiceUtil.getPoRecievedPrPositionsMinimumDeliveryDate(((Long) arguments[0]).longValue());
        }

        if (_methodName172.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes172, parameterTypes)) {
            return PositionLocalServiceUtil.getPoPositionsMinimumDeliveryDate(((Long) arguments[0]).longValue());
        }

        if (_methodName173.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes173, parameterTypes)) {
            return PositionLocalServiceUtil.getPositionsMaxLabelNumber(((Long) arguments[0]).longValue());
        }

        if (_methodName174.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes174, parameterTypes)) {
            return PositionLocalServiceUtil.getPositionsByStatus(((Long) arguments[0]).longValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName175.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes175, parameterTypes)) {
            return PositionLocalServiceUtil.getPositionsByStatusOnly(((Integer) arguments[0]).intValue());
        }

        if (_methodName176.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes176, parameterTypes)) {
            return PositionLocalServiceUtil.getPositionsByPurchaseOrderId(((Long) arguments[0]).longValue());
        }

        if (_methodName177.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes177, parameterTypes)) {
            return PositionLocalServiceUtil.getPositionsByWbsCodeId((java.lang.String) arguments[0]);
        }

        if (_methodName182.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes182, parameterTypes)) {
            return PositionLocalServiceUtil.getAllFiscalYears();
        }

        if (_methodName183.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes183, parameterTypes)) {
            return PositionLocalServiceUtil.getAllWBSCodes();
        }

        if (_methodName186.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes186, parameterTypes)) {
            return PositionLocalServiceUtil.getIAndCPositionsCount((java.lang.String) arguments[0]);
        }

        if (_methodName187.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes187, parameterTypes)) {
            return PositionLocalServiceUtil.getPositionsByIAndCType(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1]);
        }

        if (_methodName188.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes188, parameterTypes)) {
            return PositionLocalServiceUtil.getPositionsByIAndCTypeAndApprovalStatus(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1],
                ((Boolean) arguments[2]).booleanValue());
        }

        if (_methodName189.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes189, parameterTypes)) {
            return PositionLocalServiceUtil.getIAndCPositions(((Long) arguments[0]).longValue());
        }

        if (_methodName190.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes190, parameterTypes)) {
            PositionLocalServiceUtil.copyWBSfromPR((com.hp.omp.model.Position) arguments[0]);

            return null;
        }

        throw new UnsupportedOperationException();
    }
}
