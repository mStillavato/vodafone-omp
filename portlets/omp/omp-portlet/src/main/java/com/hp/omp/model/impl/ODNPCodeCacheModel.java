package com.hp.omp.model.impl;

import com.hp.omp.model.ODNPCode;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

/**
 * The cache model class for representing ODNPCode in entity cache.
 *
 * @author HP Egypt team
 * @see ODNPCode
 * @generated
 */
public class ODNPCodeCacheModel implements CacheModel<ODNPCode>, Serializable {
    public long odnpCodeId;
    public String odnpCodeName;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(5);

        sb.append("{odnpCodeId=");
        sb.append(odnpCodeId);
        sb.append(", odnpCodeName=");
        sb.append(odnpCodeName);
        sb.append("}");

        return sb.toString();
    }

    public ODNPCode toEntityModel() {
        ODNPCodeImpl odnpCodeImpl = new ODNPCodeImpl();

        odnpCodeImpl.setOdnpCodeId(odnpCodeId);

        if (odnpCodeName == null) {
            odnpCodeImpl.setOdnpCodeName(StringPool.BLANK);
        } else {
            odnpCodeImpl.setOdnpCodeName(odnpCodeName);
        }

        odnpCodeImpl.resetOriginalValues();

        return odnpCodeImpl;
    }
}
