package com.hp.omp.portlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.PortletURL;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.helper.TrackingCodeExcelFileHelper;
import com.hp.omp.model.Project;
import com.hp.omp.model.ProjectSubProject;
import com.hp.omp.model.TrackingCodes;
import com.hp.omp.model.WbsCode;
import com.hp.omp.model.custom.TrackingCodeExcelRow;
import com.hp.omp.model.impl.ProjectImpl;
import com.hp.omp.model.impl.ProjectSubProjectImpl;
import com.hp.omp.model.impl.TrackingCodesImpl;
import com.hp.omp.model.impl.WbsCodeImpl;
import com.hp.omp.service.ProjectLocalServiceUtil;
import com.hp.omp.service.ProjectSubProjectLocalServiceUtil;
import com.hp.omp.service.TrackingCodesLocalServiceUtil;
import com.hp.omp.service.WbsCodeLocalServiceUtil;
import com.hp.omp.validation.TrackingCodeValidator;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.HttpHeaders;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.PortletURLFactoryUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class TrackingCodeAdminPortlet extends MVCPortlet{
	Logger logger = LoggerFactory.getLogger(TrackingCodeAdminPortlet.class);



	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws PortletException, IOException {
		String jspPage = ParamUtil.getString(renderRequest, "jspPage","/html/trackingcodeadmin/trackinglist.jsp");
		String searchTCN = ParamUtil.getString(renderRequest, "searchFilter","").toUpperCase();
		logger.info("jspPage is {}",jspPage);
		if("/html/trackingcodeadmin/trackingform.jsp".equals(jspPage)) {
			String trackingId = ParamUtil.getString(renderRequest,"trackingId", "");
			String validationError = ParamUtil.getString(renderRequest,"validationError", "false");
			if((trackingId != null && !"".equals(trackingId)) || "true".equals(validationError )) {
				try {
					TrackingCodes trackingCode;
					if("true".equals(validationError)) {
						trackingCode = getTrackingCodeFromRequest(renderRequest);  
					} else {

						trackingCode = TrackingCodesLocalServiceUtil.getTrackingCodes(Long.valueOf(trackingId));
					}
					logger.debug("Tracking code with Id: {}", trackingCode.getTrackingCodeId());
					renderRequest.setAttribute("trackingCode", trackingCode);

				} catch (NumberFormatException e) {
					logger.error("Error in tracking code Id. ",e);
				} catch (PortalException e) {
					logger.error("Error in get tracking code with Id {}",trackingId,e);
				} catch (SystemException e) {
					logger.error("Error in get tracking code with Id {}",trackingId,e);
				}
			}else {
				TrackingCodes trackingCode=new TrackingCodesImpl();
				trackingCode.setDisplay(true);
				renderRequest.setAttribute("trackingCode", trackingCode);
			}
			try {
				addListofValuestoTheRequest(renderRequest);
			} catch (SystemException e) {
				logger.error("Error in get the static list",e);
			}
		} else if("/html/trackingcodeadmin/trackinglist.jsp".equals(jspPage)){
			try {
				int totalCount = 0;
				
				if (searchTCN == null || searchTCN.trim().length() == 0)
					totalCount = TrackingCodesLocalServiceUtil.getTrackingCodesesCount();
				else
					totalCount = TrackingCodesLocalServiceUtil.getTrackingCodesCountList(searchTCN);

				logger.debug("Tracking Number Total Count = " + totalCount);
				
				renderRequest.setAttribute("numberOfPages", getTotalNumberOfPages(totalCount, OmpHelper.PAGE_SIZE));
				renderRequest.setAttribute("pageNumber", 1);
				renderRequest.setAttribute("trackingCodeList", TrackingCodesLocalServiceUtil.getTrackingCodesList(1, OmpHelper.PAGE_SIZE, searchTCN));
				logger.info(">>>>>>>>>>>>>>>>>total records>>>>>>>>>>>>>>>>>>>>>{}",ParamUtil.getString(renderRequest, "totalRecords",null));
				renderRequest.setAttribute("totalRecords", ParamUtil.getString(renderRequest, "totalRecords",null));
			} catch (SystemException e) {
				logger.error("Error in get total tracking code counts.",e);
			}  
		} else if("/html/trackingcodeadmin/trackinguploadform.jsp".equals(jspPage)) {
			if("true".equals(ParamUtil.getString(renderRequest,"validationError", "false"))) {
				PortletSession portletSession = renderRequest.getPortletSession();
				Map<Integer,String> errors = (Map<Integer,String>)portletSession.getAttribute("uploadedBatch");
				logger.debug("Get error map size {}",errors.size());
				int totalRecords = ParamUtil.getInteger(renderRequest,"totalRecords", 0);
				renderRequest.setAttribute("totalRecords", totalRecords);
				renderRequest.setAttribute("faliedRecords", errors.size());
				renderRequest.setAttribute("successRecords", totalRecords - errors.size());
				renderRequest.setAttribute("uploadedBatchErrors", errors);
			}

		}
		super.render(renderRequest, renderResponse);
	}


	private void addListofValuestoTheRequest(RenderRequest renderRequest) throws SystemException {
		//List<WbsCode> wbsCodeList = WbsCodeLocalServiceUtil.getWbsCodes(0, WbsCodeLocalServiceUtil.getWbsCodesCount());
		List<WbsCode> wbsCodeList = WbsCodeLocalServiceUtil.getWbsCodesList(0, WbsCodeLocalServiceUtil.getWbsCodesCount(), null);
		renderRequest.setAttribute("allWbsCodes", wbsCodeList);

		//List<Project> projectsList = ProjectLocalServiceUtil.getProjects(0, ProjectLocalServiceUtil.getProjectsCount());
		List<Project> projectsList = ProjectLocalServiceUtil.getProjectsList(0, ProjectLocalServiceUtil.getProjectsCount(), null);
		renderRequest.setAttribute("allProjects", projectsList);
	}


	public void uploadBatchExcelFile(ActionRequest actionRequest, ActionResponse actionResponse) throws IOException, PortletException{
		UploadPortletRequest uploadRequest = PortalUtil
				.getUploadPortletRequest(actionRequest);
		logger.info("Start uploading excel file");
		Map<Integer, String> errors = new LinkedHashMap<Integer, String>();
		if (uploadRequest.getSize("fileName")==0) {
			errors.put(1,"Empty file");
			SessionErrors.add(actionRequest, "Empty File, Please upload the right excel file");
		}
		List<TrackingCodeExcelRow> readExcelFile = new ArrayList<TrackingCodeExcelRow>();
		try {
			readExcelFile = TrackingCodeExcelFileHelper.readExcelFile(uploadRequest.getFile("fileName"));
		} catch (Exception e) {
			errors.put(1, "Wrong file, please upload the right excel file");
			logger.error("Error in reading excel file",e);
		}
		insertBatchList(readExcelFile, errors);
		if (readExcelFile.size()==0) {
			errors.put(1,"Empty file");
			SessionErrors.add(actionRequest, "Empty File, Please upload the right excel file");
		}
		if(errors.size() == 0 ) {
			SessionMessages.add(actionRequest, "trackingcode-saved");
			//actionResponse.setRenderParameter("totalRecords", readExcelFile.size()+"");
			String portletName = (String)actionRequest.getAttribute(WebKeys.PORTLET_ID);
			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			PortletURL redirectURL = PortletURLFactoryUtil.create(PortalUtil.getHttpServletRequest(actionRequest),portletName,
					themeDisplay.getLayout().getPlid(), PortletRequest.RENDER_PHASE);
			redirectURL.setParameter("totalRecords", readExcelFile.size()+""); // set required parameter that you need in doView or render Method
			actionResponse.sendRedirect(redirectURL.toString()); 
			// sendRedirect(actionRequest, actionResponse);

		} else {
			actionResponse.setRenderParameter("totalRecords", readExcelFile.size()+"");
			SessionErrors.add(actionRequest, "Error in the uploaded file");
			actionResponse.setRenderParameter("validationError", "true");
			actionResponse.setRenderParameter("jspPage", "/html/trackingcodeadmin/trackinguploadform.jsp");
			PortletSession portletSession = actionRequest.getPortletSession();
			logger.debug(" Set Error map size {}",errors.size());
			portletSession.setAttribute("uploadedBatch", errors);
		}

		logger.info("End uploading excel file");
	}

	private void insertBatchList(List<TrackingCodeExcelRow> trackingCodeRows, Map<Integer, String> errors) {
		for(int row = 0 ; row < trackingCodeRows.size(); row++) {
			TrackingCodes trackingCode = new TrackingCodesImpl();
			trackingCode.setTrackingCodeName(trackingCodeRows.get(row).getTrackingCode());
			trackingCode.setTrackingCodeDescription(trackingCodeRows.get(row).getTrackingDescription());
			trackingCode.setDisplay(true);

			try {
				Project projectByName = ProjectLocalServiceUtil.getProjectByName(trackingCodeRows.get(row).getProject());
				if(projectByName != null) {
					trackingCode.setProjectId(projectByName.getProjectId() + "");
					trackingCode.setSubProjectId("" + 1696);
				} else {
					Project project = new ProjectImpl();
					project.setProjectName(trackingCodeRows.get(row).getProject());
					project.setDescription(null);
					project.setDisplay(true);					
					ProjectLocalServiceUtil.addProject(project);
					
					ProjectSubProject pSp = new ProjectSubProjectImpl();
					pSp.setProjectId(project.getProjectId());
					pSp.setSubProjectId(1696); 	// 1696 è il subproject Id che essendo da oggi in poi sempre NA 
												// perchè non lo gestiamo tutto sarà sempre 1696
					
					ProjectSubProjectLocalServiceUtil.addProjectSubProject(pSp);
					
					trackingCode.setProjectId("" + project.getProjectId());
					trackingCode.setSubProjectId("" + 1696);
//					errors.put(row+1, "Project name is not exist");
//					continue;
				}
			} catch(Exception e) {
				errors.put(row+1, "Project name is not exist and insert failed");
				logger.error("Error in get project by name", e);
				continue;
			}

			try {
				WbsCode wbsCodeByName = WbsCodeLocalServiceUtil.getWbsCodeByName(trackingCodeRows.get(row).getWbsCode());
				if(wbsCodeByName != null) {
					trackingCode.setWbsCodeId(wbsCodeByName.getWbsCodeId() + "");
				} else {
					WbsCode wbsCode = new WbsCodeImpl();
					wbsCode.setWbsCodeName(trackingCodeRows.get(row).getWbsCode());
					wbsCode.setDescription(trackingCodeRows.get(row).getWbsCode());
					wbsCode.setDisplay(true);
					WbsCodeLocalServiceUtil.addWbsCode(wbsCode);
					
					trackingCode.setWbsCodeId("" + wbsCode.getWbsCodeId());
//					errors.put(row+1, "wbs name is not exist");
//					continue;
				}
			} catch (Exception e) {
				errors.put(row+1, "wbs name is not exist and insert failed");
				logger.error("Error in wbs code", e);
				continue;
			}

			List<String> errorList = new ArrayList<String>();
			boolean valid = false;
			try {
				valid = TrackingCodeValidator.validateUploadedTrackingCode(trackingCode, errorList);
			} catch(SystemException e) {
				errors.put(row+1, "Error in validation the row");
				logger.error("Error in validation the tracking Code.",e);
			}
			if(!valid) {
				if(errorList.size() > 0) {
					errors.put(row+1,errorList.get(0));
				} else {
					errors.put(row+1,"Error in validation");
				}
				continue;
			} 
			try {
				logger.info("Adding a tracking  code from the file");
				TrackingCodesLocalServiceUtil.addTrackingCodes(trackingCode);
			} catch (SystemException e) {
				errors.put(row+1, "Error in adding tracking code");
				logger.error("Error in insert tracking code");
			}

		}
	}


	public void addTrackingCode(ActionRequest request, ActionResponse response) throws IOException, PortletException{
		TrackingCodes trackingCode = getTrackingCodeFromRequest(request);
		List<String> errors = new ArrayList<String>();
		boolean validTrackingCode = false;
		try {
			validTrackingCode = TrackingCodeValidator.validateTrackingCode(trackingCode, errors);
			logger.info("Tracking code validation result: {}", validTrackingCode);
		} catch (SystemException e) {
			errors.add("validation-exception");
			logger.error("Error in Validating tracking Code.", e);
		}  
		if(validTrackingCode) {
			if(trackingCode.getTrackingCodeId() != 0) {
				try {
					TrackingCodesLocalServiceUtil.updateTrackingCodes(trackingCode);
					//SessionMessages.add(request, "trackingcode-updated");
					request.setAttribute("successMSG", "Tracking code [" + trackingCode.getTrackingCodeName() + "] successfully updated");

				} catch (SystemException e) {
					errors.add("updating-exception");
					logger.error("Error in update tracking code with Id {}", trackingCode.getTrackingCodeId(), e);
				}
			} else {
				try {
					TrackingCodesLocalServiceUtil.addTrackingCodes(trackingCode);
					//SessionMessages.add(request, "trackingcode-saved");
					request.setAttribute("successMSG", "Tracking code [" + trackingCode.getTrackingCodeName() + "] successfully saved");

				} catch (SystemException e) {
					errors.add("saveing-exception");
					logger.error("Error in add new tracking code",e);
				}
			}
			sendRedirect(request, response);
		} else {
			for (String error : errors) {
				SessionErrors.add(request, error);
			}
			PortalUtil.copyRequestParameters(request, response);
			response.setRenderParameter("validationError", "true");
			response.setRenderParameter("jspPage", "/html/trackingcodeadmin/trackingform.jsp");
		}
	}

	private TrackingCodes getTrackingCodeFromRequest(PortletRequest request) {
		TrackingCodes trackingCode = new TrackingCodesImpl();
		trackingCode.setTrackingCodeId(ParamUtil.getLong(request, "trackingCodeId"));
		trackingCode.setTrackingCodeName(ParamUtil.getString(request, "trackingCodeName"));
		trackingCode.setTrackingCodeDescription(ParamUtil.getString(request, "trackingCodeDescription"));
		trackingCode.setProjectId(ParamUtil.getString(request, "projectname"));
		trackingCode.setWbsCodeId(ParamUtil.getString(request, "wbscode"));
		trackingCode.setDisplay(ParamUtil.getBoolean(request, "display"));
		return trackingCode;
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest,
			ResourceResponse resourceResponse) throws IOException,
			PortletException {
		
		logger.info("inside serveResource");
		
		String jspPage = "/html/trackingcodeadmin/trackingtable.jsp";
		String action = resourceRequest.getParameter("action");
		
		if("exportTrackingCode".equals(action)){
			exportTrackingCode(resourceRequest, resourceResponse);
			
		} else {

			if("deleteTrackingCode".equals(resourceRequest.getResourceID())) {
				try {
					TrackingCodes trackingCode = TrackingCodesLocalServiceUtil.getTrackingCodes(ParamUtil.getLong(resourceRequest, "trackingId"));
					TrackingCodesLocalServiceUtil.deleteTrackingCodes(ParamUtil.getLong(resourceRequest, "trackingId"));
					resourceRequest.setAttribute("statusMessage", "Tracking code [" + trackingCode.getTrackingCodeName() + "] successfully deleted");
				} catch (PortalException e) {
					logger.error("Error in delete tracking code",e);
				} catch (SystemException e) {
					logger.error("Error in delete tracking code",e);
				}
			}
			
			Integer pageNumber = ParamUtil.getInteger(resourceRequest, "pageNumber", 1);
			try {
				resourceRequest.setAttribute("numberOfPages", getTotalNumberOfPages(TrackingCodesLocalServiceUtil.getTrackingCodesesCount(),OmpHelper.PAGE_SIZE));
			} catch (SystemException e) {
				logger.error("Error in get total count of tracking codes",e);
			}
			resourceRequest.setAttribute("pageNumber", pageNumber);
			try {
				resourceRequest.setAttribute("trackingCodeList", TrackingCodesLocalServiceUtil.getTrackingCodesList(pageNumber, OmpHelper.PAGE_SIZE, null));
			} catch (SystemException e) {
				logger.error("Error in get tracking codes list",e);
			}
			getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
		}
	}

	private Integer getTotalNumberOfPages(Integer totalNubmerOfResult, Integer pageSize) {
		return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
	}

	public void searchFilter(ActionRequest request, ActionResponse response) throws IOException, PortletException{
		response.setRenderParameter("searchFilter", "%" + ParamUtil.getString(request, "trackingCodeName") + "%");  
	}
	
	private void exportTrackingCode(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException{
		logger.info("inside exportTrackingCode");  
		
		String searchTCN = ParamUtil.getString(resourceRequest, "searchFilter","").toUpperCase();
		
		logger.info("searchTCN = " + searchTCN);
		
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		
		String fileName = "OMP_TrackingNumber_"+formatter.format(date)+".xls";
		resourceResponse.setContentType("application/vnd.ms-excel");
		
		resourceResponse.addProperty(HttpHeaders.CONTENT_DISPOSITION,
				"attachment; filename=\""+fileName+"\"");			
		resourceResponse.addProperty(HttpHeaders.CACHE_CONTROL,"max-age=3600, must-revalidate");

		logger.debug("exporting.." + fileName);
		HttpServletResponse httpServletResponse = PortalUtil.getHttpServletResponse(resourceResponse);
		try {
			int totalCount = 0;
			
			if (searchTCN == null || searchTCN.trim().length() == 0)
				totalCount = TrackingCodesLocalServiceUtil.getTrackingCodesesCount();
			else
				totalCount = TrackingCodesLocalServiceUtil.getTrackingCodesCountList(searchTCN);

			logger.debug("Tracking Number Total Count = " + totalCount);
			List<com.hp.omp.model.TrackingCodes> trackingCodesList = TrackingCodesLocalServiceUtil.getTrackingCodesList(1, totalCount, searchTCN);
			TrackingCodesLocalServiceUtil.exportTrackingCodesAsExcel(trackingCodesList, getTrackingCodeReportHeader(), httpServletResponse.getOutputStream());

		} catch (SystemException e) {
			logger.error("Error in exportTrackingCode", e);
			e.printStackTrace();
		}
	}
	
	private List<Object> getTrackingCodeReportHeader() {
		List<Object> header = new ArrayList<Object>();
		header.add("TRACKING CODE");//0
		header.add("DESCRIPTION");//1
		header.add("PROJECT");//2
		header.add("WBS");//3
		header.add("DISPLAY");//4
		
		return header;
	}
}