package com.hp.omp.exceptions;



/**
 * @author Sami.basyoni
 */
public class UserHasNoCostCenterException extends Exception {

	public UserHasNoCostCenterException() {
		super();
	}

	public UserHasNoCostCenterException(String msg) {
		super(msg);
	}

	public UserHasNoCostCenterException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public UserHasNoCostCenterException(Throwable cause) {
		super(cause);
	}

}