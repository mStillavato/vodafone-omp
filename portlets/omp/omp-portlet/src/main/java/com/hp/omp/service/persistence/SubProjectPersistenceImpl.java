package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchSubProjectException;
import com.hp.omp.model.SubProject;
import com.hp.omp.model.impl.SubProjectImpl;
import com.hp.omp.model.impl.SubProjectModelImpl;
import com.hp.omp.service.persistence.BudgetCategoryPersistence;
import com.hp.omp.service.persistence.BudgetSubCategoryPersistence;
import com.hp.omp.service.persistence.BuyerPersistence;
import com.hp.omp.service.persistence.CostCenterPersistence;
import com.hp.omp.service.persistence.CostCenterUserPersistence;
import com.hp.omp.service.persistence.GRAuditPersistence;
import com.hp.omp.service.persistence.GoodReceiptPersistence;
import com.hp.omp.service.persistence.MacroDriverPersistence;
import com.hp.omp.service.persistence.MacroMicroDriverPersistence;
import com.hp.omp.service.persistence.MicroDriverPersistence;
import com.hp.omp.service.persistence.ODNPCodePersistence;
import com.hp.omp.service.persistence.ODNPNamePersistence;
import com.hp.omp.service.persistence.POAuditPersistence;
import com.hp.omp.service.persistence.PRAuditPersistence;
import com.hp.omp.service.persistence.PositionAuditPersistence;
import com.hp.omp.service.persistence.PositionPersistence;
import com.hp.omp.service.persistence.ProjectPersistence;
import com.hp.omp.service.persistence.ProjectSubProjectPersistence;
import com.hp.omp.service.persistence.PurchaseOrderPersistence;
import com.hp.omp.service.persistence.PurchaseRequestPersistence;
import com.hp.omp.service.persistence.SubProjectPersistence;
import com.hp.omp.service.persistence.TrackingCodesPersistence;
import com.hp.omp.service.persistence.VendorPersistence;
import com.hp.omp.service.persistence.WbsCodePersistence;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the sub project service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see SubProjectPersistence
 * @see SubProjectUtil
 * @generated
 */
public class SubProjectPersistenceImpl extends BasePersistenceImpl<SubProject>
    implements SubProjectPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link SubProjectUtil} to access the sub project persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = SubProjectImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(SubProjectModelImpl.ENTITY_CACHE_ENABLED,
            SubProjectModelImpl.FINDER_CACHE_ENABLED, SubProjectImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(SubProjectModelImpl.ENTITY_CACHE_ENABLED,
            SubProjectModelImpl.FINDER_CACHE_ENABLED, SubProjectImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(SubProjectModelImpl.ENTITY_CACHE_ENABLED,
            SubProjectModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_SUBPROJECT = "SELECT subProject FROM SubProject subProject";
    private static final String _SQL_COUNT_SUBPROJECT = "SELECT COUNT(subProject) FROM SubProject subProject";
    private static final String _ORDER_BY_ENTITY_ALIAS = "subProject.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No SubProject exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(SubProjectPersistenceImpl.class);
    private static SubProject _nullSubProject = new SubProjectImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<SubProject> toCacheModel() {
                return _nullSubProjectCacheModel;
            }
        };

    private static CacheModel<SubProject> _nullSubProjectCacheModel = new CacheModel<SubProject>() {
            public SubProject toEntityModel() {
                return _nullSubProject;
            }
        };

    @BeanReference(type = BudgetCategoryPersistence.class)
    protected BudgetCategoryPersistence budgetCategoryPersistence;
    @BeanReference(type = BudgetSubCategoryPersistence.class)
    protected BudgetSubCategoryPersistence budgetSubCategoryPersistence;
    @BeanReference(type = BuyerPersistence.class)
    protected BuyerPersistence buyerPersistence;
    @BeanReference(type = CostCenterPersistence.class)
    protected CostCenterPersistence costCenterPersistence;
    @BeanReference(type = CostCenterUserPersistence.class)
    protected CostCenterUserPersistence costCenterUserPersistence;
    @BeanReference(type = GoodReceiptPersistence.class)
    protected GoodReceiptPersistence goodReceiptPersistence;
    @BeanReference(type = GRAuditPersistence.class)
    protected GRAuditPersistence grAuditPersistence;
    @BeanReference(type = MacroDriverPersistence.class)
    protected MacroDriverPersistence macroDriverPersistence;
    @BeanReference(type = MacroMicroDriverPersistence.class)
    protected MacroMicroDriverPersistence macroMicroDriverPersistence;
    @BeanReference(type = MicroDriverPersistence.class)
    protected MicroDriverPersistence microDriverPersistence;
    @BeanReference(type = ODNPCodePersistence.class)
    protected ODNPCodePersistence odnpCodePersistence;
    @BeanReference(type = ODNPNamePersistence.class)
    protected ODNPNamePersistence odnpNamePersistence;
    @BeanReference(type = POAuditPersistence.class)
    protected POAuditPersistence poAuditPersistence;
    @BeanReference(type = PositionPersistence.class)
    protected PositionPersistence positionPersistence;
    @BeanReference(type = PositionAuditPersistence.class)
    protected PositionAuditPersistence positionAuditPersistence;
    @BeanReference(type = PRAuditPersistence.class)
    protected PRAuditPersistence prAuditPersistence;
    @BeanReference(type = ProjectPersistence.class)
    protected ProjectPersistence projectPersistence;
    @BeanReference(type = ProjectSubProjectPersistence.class)
    protected ProjectSubProjectPersistence projectSubProjectPersistence;
    @BeanReference(type = PurchaseOrderPersistence.class)
    protected PurchaseOrderPersistence purchaseOrderPersistence;
    @BeanReference(type = PurchaseRequestPersistence.class)
    protected PurchaseRequestPersistence purchaseRequestPersistence;
    @BeanReference(type = SubProjectPersistence.class)
    protected SubProjectPersistence subProjectPersistence;
    @BeanReference(type = TrackingCodesPersistence.class)
    protected TrackingCodesPersistence trackingCodesPersistence;
    @BeanReference(type = VendorPersistence.class)
    protected VendorPersistence vendorPersistence;
    @BeanReference(type = WbsCodePersistence.class)
    protected WbsCodePersistence wbsCodePersistence;
    @BeanReference(type = ResourcePersistence.class)
    protected ResourcePersistence resourcePersistence;
    @BeanReference(type = UserPersistence.class)
    protected UserPersistence userPersistence;

    /**
     * Caches the sub project in the entity cache if it is enabled.
     *
     * @param subProject the sub project
     */
    public void cacheResult(SubProject subProject) {
        EntityCacheUtil.putResult(SubProjectModelImpl.ENTITY_CACHE_ENABLED,
            SubProjectImpl.class, subProject.getPrimaryKey(), subProject);

        subProject.resetOriginalValues();
    }

    /**
     * Caches the sub projects in the entity cache if it is enabled.
     *
     * @param subProjects the sub projects
     */
    public void cacheResult(List<SubProject> subProjects) {
        for (SubProject subProject : subProjects) {
            if (EntityCacheUtil.getResult(
                        SubProjectModelImpl.ENTITY_CACHE_ENABLED,
                        SubProjectImpl.class, subProject.getPrimaryKey()) == null) {
                cacheResult(subProject);
            } else {
                subProject.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all sub projects.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(SubProjectImpl.class.getName());
        }

        EntityCacheUtil.clearCache(SubProjectImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the sub project.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(SubProject subProject) {
        EntityCacheUtil.removeResult(SubProjectModelImpl.ENTITY_CACHE_ENABLED,
            SubProjectImpl.class, subProject.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<SubProject> subProjects) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (SubProject subProject : subProjects) {
            EntityCacheUtil.removeResult(SubProjectModelImpl.ENTITY_CACHE_ENABLED,
                SubProjectImpl.class, subProject.getPrimaryKey());
        }
    }

    /**
     * Creates a new sub project with the primary key. Does not add the sub project to the database.
     *
     * @param subProjectId the primary key for the new sub project
     * @return the new sub project
     */
    public SubProject create(long subProjectId) {
        SubProject subProject = new SubProjectImpl();

        subProject.setNew(true);
        subProject.setPrimaryKey(subProjectId);

        return subProject;
    }

    /**
     * Removes the sub project with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param subProjectId the primary key of the sub project
     * @return the sub project that was removed
     * @throws com.hp.omp.NoSuchSubProjectException if a sub project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public SubProject remove(long subProjectId)
        throws NoSuchSubProjectException, SystemException {
        return remove(Long.valueOf(subProjectId));
    }

    /**
     * Removes the sub project with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the sub project
     * @return the sub project that was removed
     * @throws com.hp.omp.NoSuchSubProjectException if a sub project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SubProject remove(Serializable primaryKey)
        throws NoSuchSubProjectException, SystemException {
        Session session = null;

        try {
            session = openSession();

            SubProject subProject = (SubProject) session.get(SubProjectImpl.class,
                    primaryKey);

            if (subProject == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchSubProjectException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(subProject);
        } catch (NoSuchSubProjectException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected SubProject removeImpl(SubProject subProject)
        throws SystemException {
        subProject = toUnwrappedModel(subProject);

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.delete(session, subProject);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        clearCache(subProject);

        return subProject;
    }

    @Override
    public SubProject updateImpl(com.hp.omp.model.SubProject subProject,
        boolean merge) throws SystemException {
        subProject = toUnwrappedModel(subProject);

        boolean isNew = subProject.isNew();

        Session session = null;

        try {
            session = openSession();

            BatchSessionUtil.update(session, subProject, merge);

            subProject.setNew(false);
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(SubProjectModelImpl.ENTITY_CACHE_ENABLED,
            SubProjectImpl.class, subProject.getPrimaryKey(), subProject);

        return subProject;
    }

    protected SubProject toUnwrappedModel(SubProject subProject) {
        if (subProject instanceof SubProjectImpl) {
            return subProject;
        }

        SubProjectImpl subProjectImpl = new SubProjectImpl();

        subProjectImpl.setNew(subProject.isNew());
        subProjectImpl.setPrimaryKey(subProject.getPrimaryKey());

        subProjectImpl.setSubProjectId(subProject.getSubProjectId());
        subProjectImpl.setSubProjectName(subProject.getSubProjectName());
        subProjectImpl.setDescription(subProject.getDescription());
        subProjectImpl.setDisplay(subProject.isDisplay());

        return subProjectImpl;
    }

    /**
     * Returns the sub project with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the sub project
     * @return the sub project
     * @throws com.liferay.portal.NoSuchModelException if a sub project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SubProject findByPrimaryKey(Serializable primaryKey)
        throws NoSuchModelException, SystemException {
        return findByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the sub project with the primary key or throws a {@link com.hp.omp.NoSuchSubProjectException} if it could not be found.
     *
     * @param subProjectId the primary key of the sub project
     * @return the sub project
     * @throws com.hp.omp.NoSuchSubProjectException if a sub project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public SubProject findByPrimaryKey(long subProjectId)
        throws NoSuchSubProjectException, SystemException {
        SubProject subProject = fetchByPrimaryKey(subProjectId);

        if (subProject == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + subProjectId);
            }

            throw new NoSuchSubProjectException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                subProjectId);
        }

        return subProject;
    }

    /**
     * Returns the sub project with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the sub project
     * @return the sub project, or <code>null</code> if a sub project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public SubProject fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        return fetchByPrimaryKey(((Long) primaryKey).longValue());
    }

    /**
     * Returns the sub project with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param subProjectId the primary key of the sub project
     * @return the sub project, or <code>null</code> if a sub project with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    public SubProject fetchByPrimaryKey(long subProjectId)
        throws SystemException {
        SubProject subProject = (SubProject) EntityCacheUtil.getResult(SubProjectModelImpl.ENTITY_CACHE_ENABLED,
                SubProjectImpl.class, subProjectId);

        if (subProject == _nullSubProject) {
            return null;
        }

        if (subProject == null) {
            Session session = null;

            boolean hasException = false;

            try {
                session = openSession();

                subProject = (SubProject) session.get(SubProjectImpl.class,
                        Long.valueOf(subProjectId));
            } catch (Exception e) {
                hasException = true;

                throw processException(e);
            } finally {
                if (subProject != null) {
                    cacheResult(subProject);
                } else if (!hasException) {
                    EntityCacheUtil.putResult(SubProjectModelImpl.ENTITY_CACHE_ENABLED,
                        SubProjectImpl.class, subProjectId, _nullSubProject);
                }

                closeSession(session);
            }
        }

        return subProject;
    }

    /**
     * Returns all the sub projects.
     *
     * @return the sub projects
     * @throws SystemException if a system exception occurred
     */
    public List<SubProject> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the sub projects.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of sub projects
     * @param end the upper bound of the range of sub projects (not inclusive)
     * @return the range of sub projects
     * @throws SystemException if a system exception occurred
     */
    public List<SubProject> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the sub projects.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
     * </p>
     *
     * @param start the lower bound of the range of sub projects
     * @param end the upper bound of the range of sub projects (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of sub projects
     * @throws SystemException if a system exception occurred
     */
    public List<SubProject> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        FinderPath finderPath = null;
        Object[] finderArgs = new Object[] { start, end, orderByComparator };

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<SubProject> list = (List<SubProject>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_SUBPROJECT);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_SUBPROJECT;
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (orderByComparator == null) {
                    list = (List<SubProject>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);
                } else {
                    list = (List<SubProject>) QueryUtil.list(q, getDialect(),
                            start, end);
                }
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (list == null) {
                    FinderCacheUtil.removeResult(finderPath, finderArgs);
                } else {
                    cacheResult(list);

                    FinderCacheUtil.putResult(finderPath, finderArgs, list);
                }

                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the sub projects from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    public void removeAll() throws SystemException {
        for (SubProject subProject : findAll()) {
            remove(subProject);
        }
    }

    /**
     * Returns the number of sub projects.
     *
     * @return the number of sub projects
     * @throws SystemException if a system exception occurred
     */
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_SUBPROJECT);

                count = (Long) q.uniqueResult();
            } catch (Exception e) {
                throw processException(e);
            } finally {
                if (count == null) {
                    count = Long.valueOf(0);
                }

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);

                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the sub project persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.com.hp.omp.model.SubProject")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<SubProject>> listenersList = new ArrayList<ModelListener<SubProject>>();

                for (String listenerClassName : listenerClassNames) {
                    Class<?> clazz = getClass();

                    listenersList.add((ModelListener<SubProject>) InstanceFactory.newInstance(
                            clazz.getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(SubProjectImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
