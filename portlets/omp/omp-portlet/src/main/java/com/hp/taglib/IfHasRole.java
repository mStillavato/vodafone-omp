package com.hp.taglib;

import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Role;
import com.liferay.portal.model.User;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;

public class IfHasRole extends TagSupport {

	private String roleName;
	private long userId;
	
	@Override
	public int doStartTag() throws JspException {
		// handle conditional behavior
		boolean result = false;
		try {
			User user = UserLocalServiceUtil.getUser(userId);
			String[] roleNameList = roleName.split(",");
			for (String roleNameStr : roleNameList) {
				Role role = RoleLocalServiceUtil.getRole(user.getCompanyId(), roleNameStr);
				result = UserLocalServiceUtil.hasRoleUser(role.getRoleId(), userId);
				if(result) break;
			}
		} catch (SystemException e) {
			throw new JspException("Error in check user has role", e);
		} catch (PortalException e) {
			throw new JspException("Error in check user has role", e);
		}
		if (result) {
			return EVAL_BODY_INCLUDE;
		} else {
			return SKIP_BODY;
		}
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

}
