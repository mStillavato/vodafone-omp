package com.hp.omp.portlet;

import com.liferay.portal.model.Portlet;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.BaseControlPanelEntry;


/**
 * Control panel entry class BudgetSubCategoryAdminPortletControlPanelEntry
 */
public class BudgetSubCategoryAdminPortletControlPanelEntry extends BaseControlPanelEntry {

	public boolean isVisible(PermissionChecker permissionChecker,
			Portlet portlet) throws Exception {
		// TODO Auto-generated method stub
		return true;
	}

    @Override
    public boolean isVisible(
			Portlet portlet, String category, ThemeDisplay themeDisplay)
            throws Exception {
        return true;
    }

}