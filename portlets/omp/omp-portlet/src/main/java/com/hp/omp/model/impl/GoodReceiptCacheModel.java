package com.hp.omp.model.impl;

import com.hp.omp.model.GoodReceipt;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Serializable;

import java.util.Date;

/**
 * The cache model class for representing GoodReceipt in entity cache.
 *
 * @author HP Egypt team
 * @see GoodReceipt
 * @generated
 */
public class GoodReceiptCacheModel implements CacheModel<GoodReceipt>,
    Serializable {
    public long goodReceiptId;
    public String goodReceiptNumber;
    public long requestDate;
    public double percentage;
    public String grValue;
    public long registerationDate;
    public String createdUserId;
    public String positionId;
    public String grFile;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(17);

        sb.append("{goodReceiptId=");
        sb.append(goodReceiptId);
        sb.append(", goodReceiptNumber=");
        sb.append(goodReceiptNumber);
        sb.append(", requestDate=");
        sb.append(requestDate);
        sb.append(", percentage=");
        sb.append(percentage);
        sb.append(", grValue=");
        sb.append(grValue);
        sb.append(", registerationDate=");
        sb.append(registerationDate);
        sb.append(", createdUserId=");
        sb.append(createdUserId);
        sb.append(", positionId=");
        sb.append(positionId);
        sb.append(", grFile=");
        sb.append(grFile);        
        sb.append("}");

        return sb.toString();
    }

    public GoodReceipt toEntityModel() {
        GoodReceiptImpl goodReceiptImpl = new GoodReceiptImpl();

        goodReceiptImpl.setGoodReceiptId(goodReceiptId);

        if (goodReceiptNumber == null) {
            goodReceiptImpl.setGoodReceiptNumber(StringPool.BLANK);
        } else {
            goodReceiptImpl.setGoodReceiptNumber(goodReceiptNumber);
        }

        if (requestDate == Long.MIN_VALUE) {
            goodReceiptImpl.setRequestDate(null);
        } else {
            goodReceiptImpl.setRequestDate(new Date(requestDate));
        }

        goodReceiptImpl.setPercentage(percentage);

        if (grValue == null) {
            goodReceiptImpl.setGrValue(StringPool.BLANK);
        } else {
            goodReceiptImpl.setGrValue(grValue);
        }

        if (registerationDate == Long.MIN_VALUE) {
            goodReceiptImpl.setRegisterationDate(null);
        } else {
            goodReceiptImpl.setRegisterationDate(new Date(registerationDate));
        }

        if (createdUserId == null) {
            goodReceiptImpl.setCreatedUserId(StringPool.BLANK);
        } else {
            goodReceiptImpl.setCreatedUserId(createdUserId);
        }

        if (positionId == null) {
            goodReceiptImpl.setPositionId(StringPool.BLANK);
        } else {
            goodReceiptImpl.setPositionId(positionId);
        }
        
        if (grFile == null) {
            goodReceiptImpl.setGrFile(StringPool.BLANK);
        } else {
            goodReceiptImpl.setGrFile(grFile);
        }

        goodReceiptImpl.resetOriginalValues();

        return goodReceiptImpl;
    }
}
