/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service.persistence;

import com.hp.omp.NoSuchLayoutNotNssCatalogoException;
import com.hp.omp.model.LayoutNotNssCatalogo;
import com.hp.omp.model.impl.LayoutNotNssCatalogoImpl;
import com.hp.omp.model.impl.LayoutNotNssCatalogoModelImpl;

import com.liferay.portal.NoSuchModelException;
import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.BatchSessionUtil;
import com.liferay.portal.service.persistence.ResourcePersistence;
import com.liferay.portal.service.persistence.UserPersistence;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the layoutNotNssCatalogo service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author HP Egypt team
 * @see LayoutNotNssCatalogoPersistence
 * @see LayoutNotNssCatalogoUtil
 * @generated
 */
public class LayoutNotNssCatalogoPersistenceImpl extends BasePersistenceImpl<LayoutNotNssCatalogo>
	implements LayoutNotNssCatalogoPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link LayoutNotNssCatalogoUtil} to access the layoutNotNssCatalogo persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = LayoutNotNssCatalogoImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(LayoutNotNssCatalogoModelImpl.ENTITY_CACHE_ENABLED,
			LayoutNotNssCatalogoModelImpl.FINDER_CACHE_ENABLED, LayoutNotNssCatalogoImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(LayoutNotNssCatalogoModelImpl.ENTITY_CACHE_ENABLED,
			LayoutNotNssCatalogoModelImpl.FINDER_CACHE_ENABLED, LayoutNotNssCatalogoImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LayoutNotNssCatalogoModelImpl.ENTITY_CACHE_ENABLED,
			LayoutNotNssCatalogoModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	/**
	 * Caches the layoutNotNssCatalogo in the entity cache if it is enabled.
	 *
	 * @param layoutNotNssCatalogo the layoutNotNssCatalogo
	 */
	public void cacheResult(LayoutNotNssCatalogo layoutNotNssCatalogo) {
		EntityCacheUtil.putResult(LayoutNotNssCatalogoModelImpl.ENTITY_CACHE_ENABLED,
				LayoutNotNssCatalogoImpl.class, layoutNotNssCatalogo.getPrimaryKey(), layoutNotNssCatalogo);

		layoutNotNssCatalogo.resetOriginalValues();
	}

	/**
	 * Caches the catalogos in the entity cache if it is enabled.
	 *
	 * @param catalogos the catalogos
	 */
	public void cacheResult(List<LayoutNotNssCatalogo> layoutNotNssCatalogos) {
		for (LayoutNotNssCatalogo layoutNotNssCatalogo : layoutNotNssCatalogos) {
			if (EntityCacheUtil.getResult(
					LayoutNotNssCatalogoModelImpl.ENTITY_CACHE_ENABLED,
					LayoutNotNssCatalogoImpl.class, layoutNotNssCatalogo.getPrimaryKey()) == null) {
				cacheResult(layoutNotNssCatalogo);
			}
			else {
				layoutNotNssCatalogo.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all catalogos.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(LayoutNotNssCatalogoImpl.class.getName());
		}

		EntityCacheUtil.clearCache(LayoutNotNssCatalogoImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the layoutNotNssCatalogo.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(LayoutNotNssCatalogo layoutNotNssCatalogo) {
		EntityCacheUtil.removeResult(LayoutNotNssCatalogoModelImpl.ENTITY_CACHE_ENABLED,
				LayoutNotNssCatalogoImpl.class, layoutNotNssCatalogo.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<LayoutNotNssCatalogo> layoutNotNssCatalogos) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (LayoutNotNssCatalogo layoutNotNssCatalogo : layoutNotNssCatalogos) {
			EntityCacheUtil.removeResult(LayoutNotNssCatalogoModelImpl.ENTITY_CACHE_ENABLED,
					LayoutNotNssCatalogoImpl.class, layoutNotNssCatalogo.getPrimaryKey());
		}
	}

	/**
	 * Creates a new layoutNotNssCatalogo with the primary key. Does not add the layoutNotNssCatalogo to the database.
	 *
	 * @param layoutNotNssCatalogoId the primary key for the new layoutNotNssCatalogo
	 * @return the new layoutNotNssCatalogo
	 */
	public LayoutNotNssCatalogo create(long layoutNotNssCatalogoId) {
		LayoutNotNssCatalogo layoutNotNssCatalogo = new LayoutNotNssCatalogoImpl();

		layoutNotNssCatalogo.setNew(true);
		layoutNotNssCatalogo.setPrimaryKey(layoutNotNssCatalogoId);

		return layoutNotNssCatalogo;
	}

	/**
	 * Removes the layoutNotNssCatalogo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param layoutNotNssCatalogoId the primary key of the layoutNotNssCatalogo
	 * @return the layoutNotNssCatalogo that was removed
	 * @throws com.hp.omp.NoSuchLayoutNotNssCatalogoException if a layoutNotNssCatalogo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LayoutNotNssCatalogo remove(long layoutNotNssCatalogoId)
		throws NoSuchLayoutNotNssCatalogoException, SystemException {
		return remove(Long.valueOf(layoutNotNssCatalogoId));
	}

	/**
	 * Removes the layoutNotNssCatalogo with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the layoutNotNssCatalogo
	 * @return the layoutNotNssCatalogo that was removed
	 * @throws com.hp.omp.NoSuchLayoutNotNssCatalogoException if a layoutNotNssCatalogo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LayoutNotNssCatalogo remove(Serializable primaryKey)
		throws NoSuchLayoutNotNssCatalogoException, SystemException {
		Session session = null;

		try {
			session = openSession();

			LayoutNotNssCatalogo layoutNotNssCatalogo = (LayoutNotNssCatalogo)session.get(LayoutNotNssCatalogoImpl.class,
					primaryKey);

			if (layoutNotNssCatalogo == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLayoutNotNssCatalogoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(layoutNotNssCatalogo);
		}
		catch (NoSuchLayoutNotNssCatalogoException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected LayoutNotNssCatalogo removeImpl(LayoutNotNssCatalogo layoutNotNssCatalogo) throws SystemException {
		layoutNotNssCatalogo = toUnwrappedModel(layoutNotNssCatalogo);

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.delete(session, layoutNotNssCatalogo);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		clearCache(layoutNotNssCatalogo);

		return layoutNotNssCatalogo;
	}

	@Override
	public LayoutNotNssCatalogo updateImpl(com.hp.omp.model.LayoutNotNssCatalogo layoutNotNssCatalogo, boolean merge)
		throws SystemException {
		layoutNotNssCatalogo = toUnwrappedModel(layoutNotNssCatalogo);

		boolean isNew = layoutNotNssCatalogo.isNew();

		LayoutNotNssCatalogoModelImpl layoutNotNssCatalogoModelImpl = (LayoutNotNssCatalogoModelImpl)layoutNotNssCatalogo;

		Session session = null;

		try {
			session = openSession();

			BatchSessionUtil.update(session, layoutNotNssCatalogo, merge);

			layoutNotNssCatalogo.setNew(false);
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew || !LayoutNotNssCatalogoModelImpl.COLUMN_BITMASK_ENABLED) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(LayoutNotNssCatalogoModelImpl.ENTITY_CACHE_ENABLED,
				LayoutNotNssCatalogoImpl.class, layoutNotNssCatalogo.getPrimaryKey(), layoutNotNssCatalogo);

		return layoutNotNssCatalogo;
	}

	protected LayoutNotNssCatalogo toUnwrappedModel(LayoutNotNssCatalogo layoutNotNssCatalogo) {
		if (layoutNotNssCatalogo instanceof LayoutNotNssCatalogoImpl) {
			return layoutNotNssCatalogo;
		}

		LayoutNotNssCatalogoImpl layoutNotNssCatalogoImpl = new LayoutNotNssCatalogoImpl();

		layoutNotNssCatalogoImpl.setNew(layoutNotNssCatalogo.isNew());
		layoutNotNssCatalogoImpl.setPrimaryKey(layoutNotNssCatalogo.getPrimaryKey());

		layoutNotNssCatalogoImpl.setLayoutNotNssCatalogoId(layoutNotNssCatalogo.getLayoutNotNssCatalogoId());
		layoutNotNssCatalogoImpl.setLineNumber(layoutNotNssCatalogo.getLineNumber());
		layoutNotNssCatalogoImpl.setMaterialId(layoutNotNssCatalogo.getMaterialId());
		layoutNotNssCatalogoImpl.setMaterialDesc(layoutNotNssCatalogo.getMaterialDesc());
		layoutNotNssCatalogoImpl.setQuantity(layoutNotNssCatalogo.getQuantity());
		layoutNotNssCatalogoImpl.setNetPrice(layoutNotNssCatalogo.getNetPrice());
		layoutNotNssCatalogoImpl.setCurrency(layoutNotNssCatalogo.getCurrency());
		layoutNotNssCatalogoImpl.setDeliveryDate(layoutNotNssCatalogo.getDeliveryDate());
		layoutNotNssCatalogoImpl.setDeliveryAddress(layoutNotNssCatalogo.getDeliveryAddress());
		layoutNotNssCatalogoImpl.setItemText(layoutNotNssCatalogo.getItemText());
		layoutNotNssCatalogoImpl.setLocationEvo(layoutNotNssCatalogo.getLocationEvo());
		layoutNotNssCatalogoImpl.setTargaTecnica(layoutNotNssCatalogo.getTargaTecnica());
		layoutNotNssCatalogoImpl.setUser_(layoutNotNssCatalogo.getUser_());
		layoutNotNssCatalogoImpl.setVendorCode(layoutNotNssCatalogo.getVendorCode());
		layoutNotNssCatalogoImpl.setCatalogoId(layoutNotNssCatalogo.getCatalogoId());

		return layoutNotNssCatalogoImpl;
	}

	/**
	 * Returns the layoutNotNssCatalogo with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the layoutNotNssCatalogo
	 * @return the layoutNotNssCatalogo
	 * @throws com.liferay.portal.NoSuchModelException if a layoutNotNssCatalogo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LayoutNotNssCatalogo findByPrimaryKey(Serializable primaryKey)
		throws NoSuchModelException, SystemException {
		return findByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the layoutNotNssCatalogo with the primary key or throws a {@link com.hp.omp.NoSuchLayoutNotNssCatalogoException} if it could not be found.
	 *
	 * @param layoutNotNssCatalogoId the primary key of the layoutNotNssCatalogo
	 * @return the layoutNotNssCatalogo
	 * @throws com.hp.omp.NoSuchLayoutNotNssCatalogoException if a layoutNotNssCatalogo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LayoutNotNssCatalogo findByPrimaryKey(long layoutNotNssCatalogoId)
		throws NoSuchLayoutNotNssCatalogoException, SystemException {
		LayoutNotNssCatalogo layoutNotNssCatalogo = fetchByPrimaryKey(layoutNotNssCatalogoId);

		if (layoutNotNssCatalogo == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + layoutNotNssCatalogoId);
			}

			throw new NoSuchLayoutNotNssCatalogoException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					layoutNotNssCatalogoId);
		}

		return layoutNotNssCatalogo;
	}

	/**
	 * Returns the layoutNotNssCatalogo with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the layoutNotNssCatalogo
	 * @return the layoutNotNssCatalogo, or <code>null</code> if a layoutNotNssCatalogo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public LayoutNotNssCatalogo fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		return fetchByPrimaryKey(((Long)primaryKey).longValue());
	}

	/**
	 * Returns the layoutNotNssCatalogo with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param layoutNotNssCatalogoId the primary key of the layoutNotNssCatalogo
	 * @return the layoutNotNssCatalogo, or <code>null</code> if a layoutNotNssCatalogo with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	public LayoutNotNssCatalogo fetchByPrimaryKey(long layoutNotNssCatalogoId)
		throws SystemException {
		LayoutNotNssCatalogo layoutNotNssCatalogo = (LayoutNotNssCatalogo)EntityCacheUtil.getResult(LayoutNotNssCatalogoModelImpl.ENTITY_CACHE_ENABLED,
				LayoutNotNssCatalogoImpl.class, layoutNotNssCatalogoId);

		if (layoutNotNssCatalogo == _nullLayoutNotNssCatalogo) {
			return null;
		}

		if (layoutNotNssCatalogo == null) {
			Session session = null;

			boolean hasException = false;

			try {
				session = openSession();

				layoutNotNssCatalogo = (LayoutNotNssCatalogo)session.get(LayoutNotNssCatalogoImpl.class,
						Long.valueOf(layoutNotNssCatalogoId));
			}
			catch (Exception e) {
				hasException = true;

				throw processException(e);
			}
			finally {
				if (layoutNotNssCatalogo != null) {
					cacheResult(layoutNotNssCatalogo);
				}
				else if (!hasException) {
					EntityCacheUtil.putResult(LayoutNotNssCatalogoModelImpl.ENTITY_CACHE_ENABLED,
							LayoutNotNssCatalogoImpl.class, layoutNotNssCatalogoId, _nullLayoutNotNssCatalogo);
				}

				closeSession(session);
			}
		}

		return layoutNotNssCatalogo;
	}


	/**
	 * Returns all the catalogos.
	 *
	 * @return the catalogos
	 * @throws SystemException if a system exception occurred
	 */
	public List<LayoutNotNssCatalogo> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the catalogos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of catalogos
	 * @param end the upper bound of the range of catalogos (not inclusive)
	 * @return the range of catalogos
	 * @throws SystemException if a system exception occurred
	 */
	public List<LayoutNotNssCatalogo> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the catalogos.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	 * </p>
	 *
	 * @param start the lower bound of the range of catalogos
	 * @param end the upper bound of the range of catalogos (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of catalogos
	 * @throws SystemException if a system exception occurred
	 */
	public List<LayoutNotNssCatalogo> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		FinderPath finderPath = null;
		Object[] finderArgs = new Object[] { start, end, orderByComparator };

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<LayoutNotNssCatalogo> list = (List<LayoutNotNssCatalogo>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_CATALOGO);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CATALOGO;
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (orderByComparator == null) {
					list = (List<LayoutNotNssCatalogo>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);
				}
				else {
					list = (List<LayoutNotNssCatalogo>)QueryUtil.list(q, getDialect(),
							start, end);
				}
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (list == null) {
					FinderCacheUtil.removeResult(finderPath, finderArgs);
				}
				else {
					cacheResult(list);

					FinderCacheUtil.putResult(finderPath, finderArgs, list);
				}

				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the catalogos from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	public void removeAll() throws SystemException {
		for (LayoutNotNssCatalogo layoutNotNssCatalogo : findAll()) {
			remove(layoutNotNssCatalogo);
		}
	}


	/**
	 * Returns the number of catalogos.
	 *
	 * @return the number of catalogos
	 * @throws SystemException if a system exception occurred
	 */
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CATALOGO);

				count = (Long)q.uniqueResult();
			}
			catch (Exception e) {
				throw processException(e);
			}
			finally {
				if (count == null) {
					count = Long.valueOf(0);
				}

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);

				closeSession(session);
			}
		}

		return count.intValue();
	}

	/**
	 * Initializes the layoutNotNssCatalogo persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.hp.omp.model.LayoutNotNssCatalogo")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<LayoutNotNssCatalogo>> listenersList = new ArrayList<ModelListener<LayoutNotNssCatalogo>>();

				for (String listenerClassName : listenerClassNames) {
					Class<?> clazz = getClass();

					listenersList.add((ModelListener<LayoutNotNssCatalogo>)InstanceFactory.newInstance(
							clazz.getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(LayoutNotNssCatalogoImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@BeanReference(type = LayoutNotNssCatalogoPersistence.class)
	protected LayoutNotNssCatalogoPersistence layoutNotNssCatalogoPersistence;
	@BeanReference(type = ResourcePersistence.class)
	protected ResourcePersistence resourcePersistence;
	@BeanReference(type = UserPersistence.class)
	protected UserPersistence userPersistence;
	private static final String _SQL_SELECT_CATALOGO = "SELECT layoutNotNssCatalogo FROM LayoutNotNssCatalogo layoutNotNssCatalogo";
	private static final String _SQL_SELECT_CATALOGO_WHERE = "SELECT layoutNotNssCatalogo FROM LayoutNotNssCatalogo layoutNotNssCatalogo WHERE ";
	private static final String _SQL_COUNT_CATALOGO = "SELECT COUNT(layoutNotNssCatalogo) FROM LayoutNotNssCatalogo layoutNotNssCatalogo";
	private static final String _SQL_COUNT_CATALOGO_WHERE = "SELECT COUNT(layoutNotNssCatalogo) FROM LayoutNotNssCatalogo layoutNotNssCatalogo WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "layoutNotNssCatalogo.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No LayoutNotNssCatalogo exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No LayoutNotNssCatalogo exists with the key {";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(LayoutNotNssCatalogoPersistenceImpl.class);
	private static LayoutNotNssCatalogo _nullLayoutNotNssCatalogo = new LayoutNotNssCatalogoImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<LayoutNotNssCatalogo> toCacheModel() {
				return _nullLayoutNotNssCatalogoCacheModel;
			}
		};

	private static CacheModel<LayoutNotNssCatalogo> _nullLayoutNotNssCatalogoCacheModel = new CacheModel<LayoutNotNssCatalogo>() {
			public LayoutNotNssCatalogo toEntityModel() {
				return _nullLayoutNotNssCatalogo;
			}
		};
}