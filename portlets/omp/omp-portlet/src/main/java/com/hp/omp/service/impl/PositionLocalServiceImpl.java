/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.hp.omp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.model.GoodReceipt;
import com.hp.omp.model.Position;
import com.hp.omp.model.PositionAudit;
import com.hp.omp.model.PurchaseOrder;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.custom.IAndCType;
import com.hp.omp.model.custom.PositionStatus;
import com.hp.omp.model.custom.PurchaseRequestStatus;
import com.hp.omp.model.custom.TipoPr;
import com.hp.omp.model.impl.PositionAuditImpl;
import com.hp.omp.service.GoodReceiptLocalServiceUtil;
import com.hp.omp.service.PositionAuditLocalServiceUtil;
import com.hp.omp.service.PositionLocalServiceUtil;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.hp.omp.service.base.PositionLocalServiceBaseImpl;
import com.hp.omp.service.persistence.GoodReceiptFinderUtil;
import com.hp.omp.service.persistence.PositionFinderUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.ProjectionFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.security.auth.PrincipalThreadLocal;

/**
 * The implementation of the position local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.hp.omp.service.PositionLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see com.hp.omp.service.base.PositionLocalServiceBaseImpl
 * @see com.hp.omp.service.PositionLocalServiceUtil
 */
public class PositionLocalServiceImpl extends PositionLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.hp.omp.service.PositionLocalServiceUtil} to access the position local service.
	 */
	public static final Logger logger = LoggerFactory.getLogger(PositionLocalServiceImpl.class);

	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Position addPosition(Position position) throws SystemException {
		copyWBSfromPR(position);
		Position newPosition = super.addPosition(position);
		addAuditPosition(newPosition,newPosition.getPositionStatus());
		updatePurchaseReqeustStatus(newPosition);
		updatePrFiscalYear(newPosition);
		updatePoFiscalYear(newPosition);
		updatePrTotalValue(newPosition);
		updatePurchaseOrderTotalValue(null,newPosition,"add");

		return newPosition;
	}

	@Override
	@Indexable(type = IndexableType.DELETE)
	public Position deletePosition(long positionId) throws PortalException,
	SystemException {
		Position position = PositionLocalServiceUtil.getPosition(positionId);
		addAuditPosition(position, PositionStatus.DELETED);
		Position deletePosition = super.deletePosition(positionId);
		updatePurchaseReqeustStatus(position);
		updatePrFiscalYear(position);
		updatePoFiscalYear(position);
		updatePrTotalValue(position);
		updatePurchaseOrderTotalValue(deletePosition,null,"delete");
		return deletePosition;
	}

	@Override
	@Indexable(type = IndexableType.DELETE)
	public Position deletePosition(Position position) throws SystemException {
		addAuditPosition(position, PositionStatus.DELETED);
		Position deletedPosition = super.deletePosition(position);
		updatePurchaseReqeustStatus(deletedPosition);
		updatePrFiscalYear(deletedPosition);
		updatePoFiscalYear(deletedPosition);
		updatePrTotalValue(deletedPosition);
		updatePurchaseOrderTotalValue(deletedPosition,null,"delete");
		return deletedPosition;
	}

	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Position updatePosition(Position position) throws SystemException {
		Position newPosition = super.updatePosition(position);
		addAuditPosition(newPosition, newPosition.getPositionStatus());
		updatePurchaseReqeustStatus(newPosition);
		updatePrFiscalYear(newPosition);
		updatePoFiscalYear(newPosition);
		updatePrTotalValue(newPosition);
		return newPosition;
	}


	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Position updatePosition(Position position, boolean merge)
			throws SystemException {
		Position currentPosition = null;
		try {
			currentPosition = PositionLocalServiceUtil.getPosition(position.getPositionId());
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Position newPosition = super.updatePosition(position, merge);
		addAuditPosition(newPosition, newPosition.getPositionStatus());
		updatePurchaseReqeustStatus(newPosition);
		updatePrFiscalYear(newPosition);
		updatePoFiscalYear(newPosition);
		updatePrTotalValue(newPosition);
		updatePurchaseOrderTotalValue(currentPosition,newPosition,"update");

		return newPosition;
	}

	public Position updatePositionWithoutHocks(Position position, boolean merge)
			throws SystemException {
		Position newPosition = super.updatePosition(position, merge);
		addAuditPosition(newPosition, newPosition.getPositionStatus());
		return newPosition;
	}


	private void addAuditPosition(Position position, PositionStatus status) throws SystemException {
		PositionAudit positionAudit = new PositionAuditImpl();
		positionAudit.setNew(true);
		positionAudit.setStatus(status.getCode());
		long userId =  PrincipalThreadLocal.getUserId();
		positionAudit.setUserId(userId);
		positionAudit.setPositionId(position.getPositionId());
		positionAudit.setChangeDate(new Date());
		PositionAuditLocalServiceUtil.addPositionAudit(positionAudit);
	}

	private int getPrStatus(long prId) {
		try {
			return PurchaseRequestLocalServiceUtil.getPurchaseRequest(prId).getStatus();
		} catch (Exception e) {
			logger.debug("can't get position status "+e);
		}
		return 0;
	}

	private void updatePurchaseReqeustStatus(Position position) throws SystemException {
		try {
			PurchaseRequest purchaseRequest = PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.valueOf(position.getPurchaseRequestId()));
			Long totalCount = getPurchaseRequestPositionsCount(position);

			Long poReceivedCount = 0L;
			if(purchaseRequest.getTipoPr() == TipoPr.NORMAL.getCode())
				poReceivedCount = getPurchaseRequestPostionsByStatusCount(position, PositionStatus.PO_RECEIVED);

			else if(purchaseRequest.getTipoPr() == TipoPr.NSS.getCode())
				poReceivedCount = getPurchaseRequestPostionsByStatusCountNss(position, PositionStatus.PO_RECEIVED);

			else if(purchaseRequest.getTipoPr() == TipoPr.NOT_NSS.getCode())
				poReceivedCount = getPurchaseRequestPostionsByStatusCountNotNss(position, PositionStatus.PO_RECEIVED);
			
			else if(purchaseRequest.getTipoPr() == TipoPr.VBS.getCode())
				poReceivedCount = getPurchaseRequestPostionsByStatusCountVbs(position, PositionStatus.PO_RECEIVED);

			Double sumOfClosedValue = GoodReceiptFinderUtil.getSumCompeleteGRForPR(purchaseRequest.getPurchaseRequestId());
			Double totalValue = GoodReceiptFinderUtil.getSumValuePositionForPR(purchaseRequest.getPurchaseRequestId());
			if(totalCount == 0) {
				purchaseRequest.setStatus(PurchaseRequestStatus.CREATED.getCode());
			}
			else if( sumOfClosedValue != 0 && totalValue != 0 && sumOfClosedValue.equals(totalValue) 
					&& purchaseRequest.getTotalValue() != null 
					&& !"".equals(purchaseRequest.getTotalValue()) 
					&& Double.parseDouble(purchaseRequest.getTotalValue()) != 0) {
				purchaseRequest.setStatus(PurchaseRequestStatus.CLOSED.getCode());
			}
			else if(poReceivedCount != 0 && totalCount != 0 && totalCount.equals(poReceivedCount) ) {
				purchaseRequest.setStatus(PurchaseRequestStatus.PO_RECEIVED.getCode());
			} else {

				Long sCReceivedCount = 0L;
				if(purchaseRequest.getTipoPr() == TipoPr.NORMAL.getCode()){
					sCReceivedCount = getPurchaseRequestPostionsByStatusCount(position, PositionStatus.SC_RECEIVED);
					
//				} else if(purchaseRequest.getTipoPr() == TipoPr.NOT_NSS.getCode()){
//					sCReceivedCount = getPurchaseRequestPostionsByStatusCount(position, PositionStatus.SC_RECEIVED);
					
				} else if(purchaseRequest.getTipoPr() == TipoPr.VBS.getCode()){
					sCReceivedCount = getPurchaseRequestPostionsByStatusCountVbs(position, PositionStatus.SC_RECEIVED);
				}

				if(totalCount != 0 && sCReceivedCount != 0 && totalCount.equals(sCReceivedCount)) {
					purchaseRequest.setStatus(PurchaseRequestStatus.SC_RECEIVED.getCode());
				} else {
					Long assetReceivedCount = 0L;
					if(purchaseRequest.getTipoPr() == TipoPr.NORMAL.getCode()){
						assetReceivedCount = getPurchaseRequestPostionsByStatusCount(position, PositionStatus.ASSET_RECEIVED);
					
					} else if(purchaseRequest.getTipoPr() == TipoPr.VBS.getCode()
							|| purchaseRequest.getTipoPr() == TipoPr.NOT_NSS.getCode()){
						// Non bisogna fare nulla perchè lo stato ASSET RECEIVED non c'è per le VBS.
						//assetReceivedCount = getPurchaseRequestPostionsByStatusCountVbs(position, PositionStatus.ASSET_RECEIVED);
					}

					if(totalCount != 0 && assetReceivedCount !=0  && totalCount.equals(assetReceivedCount)) {
						purchaseRequest.setStatus(PurchaseRequestStatus.ASSET_RECEIVED.getCode());
					} else if((purchaseRequest.getPurchaseRequestStatus().equals(PurchaseRequestStatus.ASSET_RECEIVED) ||
							purchaseRequest.getPurchaseRequestStatus().equals(PurchaseRequestStatus.SC_RECEIVED) ||
							purchaseRequest.getPurchaseRequestStatus().equals(PurchaseRequestStatus.PO_RECEIVED)||
							purchaseRequest.getPurchaseRequestStatus().equals(PurchaseRequestStatus.CLOSED))
							&& purchaseRequest.getTipoPr() != TipoPr.NOT_NSS.getCode()) {
						//This means the total PR's positions not equal asset received, sc received and po received positions. 
						if(purchaseRequest.isAutomatic()) {
							purchaseRequest.setStatus(PurchaseRequestStatus.PR_ASSIGNED.getCode());
						}else {
							purchaseRequest.setStatus(PurchaseRequestStatus.SUBMITTED.getCode());
						}
					}
				}
			}

			if(purchaseRequest.getStatus() != PurchaseRequestStatus.CREATED.getCode() ){
				updateApprovalStatus(purchaseRequest);
			}
			PurchaseRequestLocalServiceUtil.updatePurchaseRequest(purchaseRequest);
		} catch(PortalException e) {
			logger.error("Error in update purchase request status while update position", e);
		}
	}

	private void updateApprovalStatus(PurchaseRequest pr) throws SystemException {
		try {
			long pendingApproval =getIAndCPositionsCount(String.valueOf(pr.getPurchaseRequestId()));
			if (pendingApproval == 0) {
				if(pr.getStatus()==PurchaseRequestStatus.PENDING_IC_APPROVAL.getCode()) {
					pr.setStatus(PurchaseRequestStatus.SUBMITTED.getCode());
				}
			} else {
				pr.setStatus(PurchaseRequestStatus.PENDING_IC_APPROVAL.getCode());
			}
		} catch (Exception e) {
			logger.error("Error in update purchase request status while update position",e);
		}
	}

	private void updatePrFiscalYear(Position position) throws SystemException {
		String fiscalYear = "";
		try {
			List<Date> dateList = new ArrayList<Date>();
			PurchaseRequest purchaseRequest = PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.valueOf(position.getPurchaseRequestId()));
			String prStatus = purchaseRequest.getPurchaseRequestStatus().getLabel();
			if (prStatus.equals(PurchaseRequestStatus.PO_RECEIVED.getLabel())) {
				dateList = getPositionsMaxRegisterationDate(purchaseRequest.getPurchaseRequestId(), "PR");
				if (dateList.size() == 0 || dateList.get(0)==null) {
					dateList = getPoRecievedPrPositionsMinimumDeliveryDate(purchaseRequest.getPurchaseRequestId());
				}
			} else {
				dateList = getPrPositionsMinimumDeliveryDate(purchaseRequest.getPurchaseRequestId());
			}
			if (dateList.size() > 0 && dateList.get(0) !=null) {
				Date requiredDate = dateList.get(0);
				fiscalYear=OmpHelper.calculateFiscalYear(requiredDate);
			}
			purchaseRequest.setFiscalYear(fiscalYear);
			PurchaseRequestLocalServiceUtil.updatePurchaseRequest(purchaseRequest);

		} catch (Exception e) {
			logger.error("Error in update Pr FiscalYear", e);
		}
	}

	private void updatePoFiscalYear(Position position) throws SystemException {
		if(position.getPurchaseOrderId()!=null && !"".equals(position.getPurchaseOrderId())){
			String fiscalYear = "";
			try {
				List<Date> dateList = new ArrayList<Date>();
				PurchaseOrder Purchaseorder = PurchaseOrderLocalServiceUtil.getPurchaseOrder(Long.valueOf(position.getPurchaseOrderId()));
				dateList = getPositionsMaxRegisterationDate(Purchaseorder.getPurchaseOrderId(), "PO");
				if (dateList.size() == 0 || dateList.get(0)==null) {
					dateList = getPoPositionsMinimumDeliveryDate(Purchaseorder.getPurchaseOrderId());
				}
				if (dateList.size() > 0 && dateList.get(0) !=null) {
					Date requiredDate = dateList.get(0);
					fiscalYear=OmpHelper.calculateFiscalYear(requiredDate);
				}
				Purchaseorder.setFiscalYear(fiscalYear);
				PurchaseOrderLocalServiceUtil.updatePurchaseOrder(Purchaseorder);

			} catch (Exception e) {
				logger.error("Error in update Po FiscalYear", e);
			}
		}
	}

	public List<Date> getPositionsMaxRegisterationDate(long Id,String type) throws SystemException {

		DynamicQuery positionQuery = DynamicQueryFactoryUtil.forClass(Position.class);
		if("PR".equals(type)) {
			positionQuery.add(RestrictionsFactoryUtil.eq("purchaseRequestId", String.valueOf(Id)));
		}else {
			positionQuery.add(RestrictionsFactoryUtil.eq("purchaseOrderId", String.valueOf(Id)));
		}
		positionQuery.setProjection(ProjectionFactoryUtil.distinct(ProjectionFactoryUtil.property("positionId"))); 

		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(GoodReceipt.class);
		dynamicQuery.add(RestrictionsFactoryUtil.isNotNull("goodReceiptNumber"));
		dynamicQuery.add(RestrictionsFactoryUtil.isNotNull("registerationDate"));
		dynamicQuery.add(PropertyFactoryUtil.forName("positionId").in(positionQuery));
		dynamicQuery.setProjection(ProjectionFactoryUtil.max("registerationDate"));
		@SuppressWarnings("unchecked")
		List<Date> resultList = GoodReceiptLocalServiceUtil.dynamicQuery(dynamicQuery);
		if(resultList!= null)  {
			return resultList;
		}
		return new ArrayList<Date>();
	}



	@SuppressWarnings("unchecked")
	public List<Position> getPurchaseOrderPositions(String purchaseOrderId,long userId) throws SystemException{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Position.class);

		if(OmpHelper.isNdso(userId)) {
			dynamicQuery.add(RestrictionsFactoryUtil.eq("icApproval", String.valueOf(IAndCType.ICFIELD.getCode())));
		} else if(OmpHelper.isTc(userId)) {
			dynamicQuery.add(RestrictionsFactoryUtil.eq("icApproval", String.valueOf(IAndCType.ICTEST.getCode())));
		} else if(OmpHelper.isApproverVbs(userId)) {
			dynamicQuery.add(RestrictionsFactoryUtil.eq("icApproval", String.valueOf(IAndCType.ICSERVIZI.getCode())));
		}
		
		dynamicQuery.add(RestrictionsFactoryUtil.eq("purchaseOrderId", purchaseOrderId));
		dynamicQuery.addOrder(OrderFactoryUtil.asc("poLabelNumber"));
		return PositionLocalServiceUtil.dynamicQuery(dynamicQuery);
	}
	public Long getPurchaseRequestPositionsCount(Position position) throws SystemException {
		DynamicQuery countQuery = DynamicQueryFactoryUtil.forClass(Position.class)
				.add(PropertyFactoryUtil.forName("purchaseRequestId").eq(position.getPurchaseRequestId())).setProjection(ProjectionFactoryUtil.rowCount());
		@SuppressWarnings("unchecked")
		List<Long> resultQuery = super.dynamicQuery(countQuery);
		return (resultQuery == null || resultQuery.size() == 0) ?  0L  : resultQuery.get(0);
	}
	public Long getPurchaseRequestPostionsByStatusCount(Position position, PositionStatus status) throws SystemException {
		DynamicQuery countQuery = DynamicQueryFactoryUtil.forClass(Position.class);
		Criterion criterion = PropertyFactoryUtil.forName("purchaseRequestId").eq(position.getPurchaseRequestId());
		switch (status) {
		case PO_RECEIVED:
			criterion = getPOReceivedStatusQuery(criterion);
			break;
		case SC_RECEIVED:
			criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("assetNumber"));
			criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("shoppingCart"));
			break;
		case ASSET_RECEIVED:
			criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("assetNumber"));
			break;
		}
		countQuery.add(criterion);
		countQuery.setProjection(ProjectionFactoryUtil.rowCount());
		@SuppressWarnings("unchecked")
		List<Long> resultQuery = super.dynamicQuery(countQuery);
		return (resultQuery == null || resultQuery.size() == 0) ?  0L  : resultQuery.get(0);
	}

	public Long getPurchaseRequestPostionsByStatusCountNss(Position position, PositionStatus status) throws SystemException {
		DynamicQuery countQuery = DynamicQueryFactoryUtil.forClass(Position.class);
		Criterion criterion = PropertyFactoryUtil.forName("purchaseRequestId").eq(position.getPurchaseRequestId());
		switch (status) {
		case PO_RECEIVED:
			criterion = getPOReceivedStatusQueryNss(criterion);
			break;
		case SC_RECEIVED:
			break;
		case ASSET_RECEIVED:
			break;
		}
		countQuery.add(criterion);
		countQuery.setProjection(ProjectionFactoryUtil.rowCount());
		@SuppressWarnings("unchecked")
		List<Long> resultQuery = super.dynamicQuery(countQuery);
		return (resultQuery == null || resultQuery.size() == 0) ?  0L  : resultQuery.get(0);
	}

	public Long getPurchaseRequestPostionsByStatusCountNotNss(Position position, PositionStatus status) throws SystemException {
		DynamicQuery countQuery = DynamicQueryFactoryUtil.forClass(Position.class);
		Criterion criterion = PropertyFactoryUtil.forName("purchaseRequestId").eq(position.getPurchaseRequestId());
		switch (status) {
		case PO_RECEIVED:
			criterion = getPOReceivedStatusQueryNss(criterion);
			break;
		case SC_RECEIVED:
			//criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("assetNumber"));
			break;
		case ASSET_RECEIVED:
			//criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("assetNumber"));
			break;
		}
		countQuery.add(criterion);
		countQuery.setProjection(ProjectionFactoryUtil.rowCount());
		@SuppressWarnings("unchecked")
		List<Long> resultQuery = super.dynamicQuery(countQuery);
		return (resultQuery == null || resultQuery.size() == 0) ?  0L  : resultQuery.get(0);
	}
	
	public Long getPurchaseRequestPostionsByStatusCountVbs(Position position, PositionStatus status) throws SystemException {
		DynamicQuery countQuery = DynamicQueryFactoryUtil.forClass(Position.class);
		Criterion criterion = PropertyFactoryUtil.forName("purchaseRequestId").eq(position.getPurchaseRequestId());
		switch (status) {
		case PO_RECEIVED:
			criterion = getPOReceivedStatusQueryVbs(criterion);
			break;
		case SC_RECEIVED:
			criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("shoppingCart"));
			break;
		case ASSET_RECEIVED:
			break;
		}
		countQuery.add(criterion);
		countQuery.setProjection(ProjectionFactoryUtil.rowCount());
		@SuppressWarnings("unchecked")
		List<Long> resultQuery = super.dynamicQuery(countQuery);
		return (resultQuery == null || resultQuery.size() == 0) ?  0L  : resultQuery.get(0);
	}
	
	@SuppressWarnings("unchecked")
	public List<Position> getPurchaseRequestPositions(String purchaseRequestId) throws SystemException {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Position.class);
		dynamicQuery.add(RestrictionsFactoryUtil.eq("purchaseRequestId", purchaseRequestId));
		return PositionLocalServiceUtil.dynamicQuery(dynamicQuery);
	}

	@SuppressWarnings("unchecked")
	public List<Position> getPositionsByPrId(long purchaseRequestId) throws SystemException {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Position.class);
		dynamicQuery.add(RestrictionsFactoryUtil.eq("purchaseRequestId", String.valueOf(purchaseRequestId)));
		dynamicQuery.addOrder(OrderFactoryUtil.asc("positionId"));
		return PositionLocalServiceUtil.dynamicQuery(dynamicQuery);
	}
	public List<Date> getPrPositionsMinimumDeliveryDate(long purchaseRequestId) throws SystemException {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Position.class);
		dynamicQuery.add(RestrictionsFactoryUtil.eq("purchaseRequestId", String.valueOf(purchaseRequestId)));
		dynamicQuery.add(RestrictionsFactoryUtil.isNull("poNumber"));
		dynamicQuery.setProjection(ProjectionFactoryUtil.min("deliveryDate"));
		@SuppressWarnings("unchecked")
		List<Date> resultList = PositionLocalServiceUtil.dynamicQuery(dynamicQuery);
		if(resultList!= null)  {
			return resultList;
		}
		return new ArrayList<Date>();
	}

	public List<Date> getPoRecievedPrPositionsMinimumDeliveryDate(long purchaseRequestId) throws SystemException {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Position.class);
		dynamicQuery.add(RestrictionsFactoryUtil.eq("purchaseRequestId", String.valueOf(purchaseRequestId)));
		dynamicQuery.setProjection(ProjectionFactoryUtil.min("deliveryDate"));
		@SuppressWarnings("unchecked")
		List<Date> resultList = PositionLocalServiceUtil.dynamicQuery(dynamicQuery);
		if(resultList!= null)  {
			return resultList;
		}
		return new ArrayList<Date>();
	}

	public List<Date> getPoPositionsMinimumDeliveryDate(long purchaseorderId) throws SystemException {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Position.class);
		dynamicQuery.add(RestrictionsFactoryUtil.eq("purchaseOrderId", String.valueOf(purchaseorderId)));
		dynamicQuery.add(RestrictionsFactoryUtil.isNotNull("poNumber"));
		dynamicQuery.setProjection(ProjectionFactoryUtil.min("deliveryDate"));
		@SuppressWarnings("unchecked")
		List<Date> resultList = PositionLocalServiceUtil.dynamicQuery(dynamicQuery);
		if(resultList!= null)  {
			return resultList;
		}
		return new ArrayList<Date>();
	}

	public List<Long> getPositionsMaxLabelNumber(long purchaseRequestId) throws SystemException {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Position.class);
		dynamicQuery.add(RestrictionsFactoryUtil.eq("purchaseRequestId", String.valueOf(purchaseRequestId)));
		dynamicQuery.setProjection(ProjectionFactoryUtil.max("labelNumber"));
		@SuppressWarnings("unchecked")
		List<Long> resultList = PositionLocalServiceUtil.dynamicQuery(dynamicQuery);
		if(resultList!= null)  {
			return resultList;
		}
		return new ArrayList<Long>();
	}


	public List<Position> getPositionsByStatus(long prId, int positionStatus) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Position.class);

		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("purchaseRequestId", String.valueOf(prId));

		if(positionStatus == PositionStatus.NEW.getCode()){
			criterion = getNEWStatusQuery(criterion);
		}else if(positionStatus == PositionStatus.ASSET_RECEIVED.getCode()){
			criterion = getAssetReceivedStatusQuery(criterion);
		}else if(positionStatus == PositionStatus.SC_RECEIVED.getCode()){
			criterion = getSCReceivedStatusQuery(criterion);
		}else if(positionStatus == PositionStatus.PO_RECEIVED.getCode()){
			criterion = getPOReceivedStatusQuery(criterion);
		}else{
			throw new Exception("position status not defined"); 
		}
		dynamicQuery.add(criterion); 
		dynamicQuery.addOrder(OrderFactoryUtil.asc("positionId"));
		@SuppressWarnings("unchecked")
		List<Position>  positionList = PositionLocalServiceUtil.dynamicQuery(dynamicQuery);

		return positionList;
	}
	
	public List<Position> getPositionsByStatusVbs(long prId, int positionStatus) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Position.class);

		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("purchaseRequestId", String.valueOf(prId));

		if(positionStatus == PositionStatus.NEW.getCode()){
			criterion = getNEWStatusQuery(criterion);
		}else if(positionStatus == PositionStatus.ASSET_RECEIVED.getCode()){
			criterion = getAssetReceivedStatusQueryVbs(criterion);
		}else if(positionStatus == PositionStatus.SC_RECEIVED.getCode()){
			criterion = getSCReceivedStatusQueryVbs(criterion);
		}else if(positionStatus == PositionStatus.PO_RECEIVED.getCode()){
			criterion = getPOReceivedStatusQueryVbs(criterion);
		}else{
			throw new Exception("position status not defined"); 
		}
		dynamicQuery.add(criterion); 
		dynamicQuery.addOrder(OrderFactoryUtil.asc("positionId"));
		@SuppressWarnings("unchecked")
		List<Position>  positionList = PositionLocalServiceUtil.dynamicQuery(dynamicQuery);

		return positionList;
	}

	public List<Position> getPositionsByStatusOnly(int positionStatus) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Position.class);
		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.isNotNull("purchaseRequestId");
		if(positionStatus == PositionStatus.NEW.getCode()){
			criterion = getNEWStatusQuery(criterion);
		}else if(positionStatus == PositionStatus.ASSET_RECEIVED.getCode()){
			criterion = getAssetReceivedStatusQuery(criterion);
		}else if(positionStatus == PositionStatus.SC_RECEIVED.getCode()){
			criterion = getSCReceivedStatusQuery(criterion);
		}else if(positionStatus == PositionStatus.PO_RECEIVED.getCode()){
			criterion = getPOReceivedStatusQuery(criterion);
		}else{
			throw new Exception("position status not defined"); 
		}
		dynamicQuery.add(criterion); 
		@SuppressWarnings("unchecked")
		List<Position>  positionList = PositionLocalServiceUtil.dynamicQuery(dynamicQuery);
		return positionList;
	}

	public List<Position> getPositionsByPurchaseOrderId(long poId) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Position.class);

		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("purchaseOrderId", String.valueOf(poId));

		dynamicQuery.add(criterion); 
		@SuppressWarnings("unchecked")
		List<Position>  positionList = PositionLocalServiceUtil.dynamicQuery(dynamicQuery);

		return positionList;
	}

	public List<Position> getPositionsByWbsCodeId(String wbsCodeId) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Position.class);

		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("wbsCodeId", wbsCodeId);

		dynamicQuery.add(criterion); 
		@SuppressWarnings("unchecked")
		List<Position>  positionList = PositionLocalServiceUtil.dynamicQuery(dynamicQuery);

		return positionList;
	}

	private Criterion getNEWStatusQuery(Criterion criterion){
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("assetNumber"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("shoppingCart"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("poNumber"));
		return criterion;
	}

	private Criterion getNEWStatusQueryNss(Criterion criterion){
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("poNumber"));
		return criterion;
	}

	private Criterion getNEWStatusQueryNotNss(Criterion criterion){
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("assetNumber"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("poNumber"));
		return criterion;
	}

	private Criterion getAssetReceivedStatusQuery(Criterion criterion){
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("assetNumber"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("shoppingCart"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("poNumber"));
		return criterion;
	}
	
	private Criterion getAssetReceivedStatusQueryVbs(Criterion criterion){
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("shoppingCart"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("poNumber"));
		return criterion;
	}

	private Criterion getAssetReceivedStatusQueryNss(Criterion criterion){
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("poNumber"));
		return criterion;
	}

	private Criterion getAssetReceivedStatusQueryNotNss(Criterion criterion){
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("assetNumber"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("poNumber"));
		return criterion;
	}

	private Criterion getSCReceivedStatusQuery(Criterion criterion){
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("assetNumber"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("shoppingCart"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("poNumber"));
		return criterion;
	}
	
	private Criterion getSCReceivedStatusQueryVbs(Criterion criterion){
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("shoppingCart"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("poNumber"));
		return criterion;
	}

	private Criterion getSCReceivedStatusQueryNss(Criterion criterion){
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("poNumber"));
		return criterion;
	}

	private Criterion getSCReceivedStatusQueryNotNss(Criterion criterion){
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("assetNumber"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNull("poNumber"));
		return criterion;
	}

	private Criterion getPOReceivedStatusQuery(Criterion criterion){
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("assetNumber"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("shoppingCart"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("poNumber"));
		return criterion;
	}
	
	private Criterion getPOReceivedStatusQueryVbs(Criterion criterion){
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("shoppingCart"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("poNumber"));
		return criterion;
	}

	private Criterion getPOReceivedStatusQueryNss(Criterion criterion){
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("poNumber"));
		return criterion;
	}

	private Criterion getPOReceivedStatusQueryNotNss(Criterion criterion){
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("assetNumber"));
		criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.isNotNull("poNumber"));
		return criterion;
	}

	public List<String> getAllFiscalYears() throws SystemException{
		return PositionFinderUtil.getAllFiscalYears();
	}

	public List<String> getAllWBSCodes() throws SystemException{
		return PositionFinderUtil.getAllWBSCodes();
	}


	private void updatePurchaseOrderTotalValue(Position currentposition,
			Position newPosition, String action) throws SystemException {

		try {
			if ("add".equalsIgnoreCase(action) && newPosition != null && !newPosition.getPoNumber().isEmpty()) {
				String poNumber = newPosition.getPoNumber();
				try{
					PurchaseOrder purchaseOrder = PurchaseOrderLocalServiceUtil.getPurchaseOrder(Long.valueOf(poNumber));
					double totalValue = Double.valueOf(purchaseOrder.getTotalValue()) + (Double.valueOf(newPosition.getQuatity()) * Double.valueOf(newPosition.getActualUnitCost()));
					purchaseOrder.setTotalValue(String.valueOf(totalValue));
					PurchaseOrderLocalServiceUtil.updatePurchaseOrder(purchaseOrder, true);
				}catch(Exception e){
					logger.error("purchase order " + poNumber +"not exist in the DB", e);
				}


			} else if ("update".equalsIgnoreCase(action)) {

				if (currentposition != null && newPosition != null) {
					String currPoNumber = currentposition.getPoNumber();
					String newPoNumber = newPosition.getPoNumber();

					// this case when the controller assign the position from purchase order to another one.
					if (!currPoNumber.isEmpty() && !newPoNumber.isEmpty()
							&& !currPoNumber.equalsIgnoreCase(newPoNumber)) {

						PurchaseOrder currPurchaseOrder = PurchaseOrderLocalServiceUtil.getPurchaseOrder(Long.valueOf(currPoNumber));

						double totalValue1 = Double.valueOf(currPurchaseOrder.getTotalValue()) - (Double.valueOf(currentposition.getQuatity()) * Double.valueOf(currentposition.getActualUnitCost()));

						currPurchaseOrder.setTotalValue(String.valueOf(totalValue1));
						PurchaseOrderLocalServiceUtil.updatePurchaseOrder(currPurchaseOrder, true);

						PurchaseOrder newPurchaseOrder = PurchaseOrderLocalServiceUtil.getPurchaseOrder(Long.valueOf(newPoNumber));
						double totalValue2 = Double.valueOf(newPurchaseOrder.getTotalValue()) + (Double.valueOf(newPosition.getQuatity()) * Double.valueOf(newPosition.getActualUnitCost()));
						newPurchaseOrder.setTotalValue(String.valueOf(totalValue2));

						PurchaseOrderLocalServiceUtil.updatePurchaseOrder(newPurchaseOrder, true);

						// this case when the Controller or the Antex assign new position to purchase order.
					} else if (currPoNumber.isEmpty() && !newPoNumber.isEmpty()) {

						PurchaseOrder newPurchaseOrder = PurchaseOrderLocalServiceUtil.getPurchaseOrder(Long.valueOf(newPoNumber));
						double posTotalValue = Double.valueOf(newPosition.getQuatity()) * Double.valueOf(newPosition.getActualUnitCost());
						double totalValue =0d;
						if(!newPurchaseOrder.getTotalValue().isEmpty())
							totalValue = Double.valueOf(newPurchaseOrder.getTotalValue());

						totalValue += posTotalValue;
						newPurchaseOrder.setTotalValue(String.valueOf(totalValue));
						PurchaseOrderLocalServiceUtil.updatePurchaseOrder(newPurchaseOrder, true);

						// this case when the Controller unassign position from purchase order.
					} else if (!currPoNumber.isEmpty() && newPoNumber.isEmpty()) {

						PurchaseOrder currPurchaseOrder = PurchaseOrderLocalServiceUtil.getPurchaseOrder(Long.valueOf(currPoNumber));
						double posTotalValue = Double.valueOf(currentposition.getQuatity()) * Double.valueOf(currentposition.getActualUnitCost());
						double totalValue = Double.valueOf(currPurchaseOrder.getTotalValue());

						totalValue -= posTotalValue;
						currPurchaseOrder.setTotalValue(String.valueOf(totalValue));
						PurchaseOrderLocalServiceUtil.updatePurchaseOrder(currPurchaseOrder, true);

						// this case when the Controller change the quantity or unit cost of the position.
					} else if (((!currentposition.getActualUnitCost().equalsIgnoreCase(newPosition.getActualUnitCost())) 
							|| (!currentposition.getQuatity().equalsIgnoreCase(newPosition.getQuatity())))
							&& 	!newPoNumber.isEmpty()
							&& 	!currPoNumber.isEmpty()
							&& 	 currentposition.getPoNumber().equalsIgnoreCase(newPosition.getPoNumber())) {

						PurchaseOrder currentPurchaseOrder;
						currentPurchaseOrder = PurchaseOrderLocalServiceUtil.getPurchaseOrder(Long.valueOf(currentposition.getPoNumber()));
						double currentPosTotalValue = Double.valueOf(currentposition.getActualUnitCost()) * Double.valueOf(currentposition.getQuatity());
						double newPosTotalValue = Double.valueOf(newPosition.getActualUnitCost()) * Double.valueOf(newPosition.getQuatity());
						double newTotalValue = (Double.valueOf(currentPurchaseOrder.getTotalValue()) - currentPosTotalValue ) +  newPosTotalValue;
						currentPurchaseOrder.setTotalValue(String.valueOf(newTotalValue));

						PurchaseOrderLocalServiceUtil.updatePurchaseOrder(currentPurchaseOrder, true);
					}
				}

			} else if ("delete".equalsIgnoreCase(action)) {
				PurchaseOrder purchaseOrder;
				purchaseOrder = PurchaseOrderLocalServiceUtil.getPurchaseOrder(Long.valueOf(currentposition.getPoNumber()));
				double newTotalValue = Double.valueOf(purchaseOrder.getTotalValue()) - (Double.valueOf(currentposition.getQuatity()) * Double.valueOf(currentposition.getActualUnitCost()));
				purchaseOrder.setTotalValue(String.valueOf(newTotalValue));
				PurchaseOrderLocalServiceUtil.updatePurchaseOrder(purchaseOrder, true);
			}
		} catch (NumberFormatException e) {
			logger.error("Error in update total value of purchase order while update position", e);
		} catch (PortalException e) {
			logger.error("Error in update total value of purchase order while update position", e);

		}

	}

	private void updatePrTotalValue(Position position) {

		try {
			PurchaseRequest pr=PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.valueOf(position.getPurchaseRequestId()));
			double totalValue=0;
			List<Position> positions=new ArrayList<Position>();
			positions=PositionLocalServiceUtil.getPositionsByPrId(Long.valueOf(position.getPurchaseRequestId()));

			for(Position pos:positions){
				if(pos.getActualUnitCost()!=null && ! "".equals(pos.getActualUnitCost())
						&&pos.getQuatity()!=null && !"".equals(pos.getQuatity()))
					totalValue=totalValue+(Double.valueOf(pos.getActualUnitCost())*Double.valueOf(pos.getQuatity()));
			}
			pr.setTotalValue(String.valueOf(totalValue));
			PurchaseRequestLocalServiceUtil.updatePurchaseRequest(pr, true);

		}catch(Exception e) {
			logger.error("Error in update total value of purchase request", e);
		}
	}

	@SuppressWarnings("unchecked")
	public Long getIAndCPositionsCount(String purchaseRequestId) throws SystemException {

		return PositionFinderUtil.getIAndCPositionsCount(purchaseRequestId);
	}

	public List<Position> getPositionsByIAndCType(long prId, String iAndCType) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Position.class);

		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("purchaseRequestId", String.valueOf(prId));

		if(IAndCType.ICFIELD.getLabel().equalsIgnoreCase(iAndCType) ){
			criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.eq("icApproval",String.valueOf(IAndCType.ICFIELD.getCode())));
		} else if(IAndCType.ICTEST.getLabel().equalsIgnoreCase(iAndCType)){
			criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.eq("icApproval",String.valueOf(IAndCType.ICTEST.getCode())));
		} else if(IAndCType.ICSERVIZI.getLabel().equalsIgnoreCase(iAndCType)){
			criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.eq("icApproval",String.valueOf(IAndCType.ICSERVIZI.getCode())));
		} else{
			throw new Exception("position status not defined"); 
		}

		dynamicQuery.add(criterion); 
		dynamicQuery.addOrder(OrderFactoryUtil.asc("positionId"));
		@SuppressWarnings("unchecked")
		List<Position>  positionList = PositionLocalServiceUtil.dynamicQuery(dynamicQuery);

		return positionList;
	}

	public List<Position> getPositionsByIAndCTypeAndApprovalStatus(long prId, String iAndCType,boolean approved) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Position.class);

		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("purchaseRequestId", String.valueOf(prId));

		if(approved) {
			criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.eq("approved",true));	
		}else {
			criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.ne("approved",true));	
		}

		if(IAndCType.ICFIELD.getLabel().equalsIgnoreCase(iAndCType) ){
			criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.eq("icApproval",String.valueOf(IAndCType.ICFIELD.getCode())));
		} else if(IAndCType.ICTEST.getLabel().equalsIgnoreCase(iAndCType)){
			criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.eq("icApproval",String.valueOf(IAndCType.ICTEST.getCode())));
		} else if(IAndCType.ICSERVIZI.getLabel().equalsIgnoreCase(iAndCType)){
			criterion = RestrictionsFactoryUtil.and(criterion, RestrictionsFactoryUtil.eq("icApproval",String.valueOf(IAndCType.ICSERVIZI.getCode())));
		} else{
			throw new Exception("position status not defined"); 
		}

		dynamicQuery.add(criterion); 
		dynamicQuery.addOrder(OrderFactoryUtil.asc("positionId"));
		@SuppressWarnings("unchecked")
		List<Position>  positionList = PositionLocalServiceUtil.dynamicQuery(dynamicQuery);

		if(positionList != null)  {
			return positionList;
		}
		return new ArrayList<Position>();
	}

	public List<Position> getIAndCPositions(long prId) throws Exception{
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Position.class);
		dynamicQuery.add(RestrictionsFactoryUtil.eq("purchaseRequestId", String.valueOf(prId)));
		dynamicQuery.add(RestrictionsFactoryUtil.isNotNull("icApproval"));

		@SuppressWarnings("unchecked")
		List<Position>  positionList = PositionLocalServiceUtil.dynamicQuery(dynamicQuery);

		if(positionList != null)  {
			return positionList;
		}
		return new ArrayList<Position>();
	}

	public void copyWBSfromPR(Position position) throws SystemException{
		try {
			PurchaseRequest pr=PurchaseRequestLocalServiceUtil.getPurchaseRequest(Long.valueOf(position.getPurchaseRequestId()));
			if(pr !=null) {
				if(pr.getWbsCodeId() !=null || ! "".equals(pr.getWbsCodeId())) {
					position.setWbsCodeId(pr.getWbsCodeId());
				}
			}
		}catch(Exception e) {
			logger.error("Error in copyWBSfromPR", e);
		}
	}

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Position> getPositionsNssByStatus(long prId, int positionStatus)
			throws Exception {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Position.class);

		Criterion criterion = null;
		criterion = RestrictionsFactoryUtil.eq("purchaseRequestId", String.valueOf(prId));

		if(positionStatus == PositionStatus.NEW.getCode()){
			criterion = getNEWStatusQueryNss(criterion);

		}else if(positionStatus == PositionStatus.ASSET_RECEIVED.getCode()){
			criterion = getAssetReceivedStatusQueryNss(criterion);

		}else if(positionStatus == PositionStatus.SC_RECEIVED.getCode()){
			criterion = getSCReceivedStatusQueryNss(criterion);

		}else if(positionStatus == PositionStatus.PO_RECEIVED.getCode()){
			criterion = getPOReceivedStatusQueryNss(criterion);

		}else{
			throw new Exception("position status not defined"); 
		}
		dynamicQuery.add(criterion); 
		dynamicQuery.addOrder(OrderFactoryUtil.asc("positionId"));
		@SuppressWarnings("unchecked")
		List<Position>  positionList = PositionLocalServiceUtil.dynamicQuery(dynamicQuery);

		return positionList;
	}
}

