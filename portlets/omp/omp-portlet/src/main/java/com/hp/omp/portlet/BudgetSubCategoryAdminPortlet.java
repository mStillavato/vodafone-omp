package com.hp.omp.portlet;

import java.io.IOException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hp.omp.helper.OmpHelper;
import com.hp.omp.model.BudgetCategory;
import com.hp.omp.model.BudgetSubCategory;
import com.hp.omp.model.PurchaseOrder;
import com.hp.omp.model.PurchaseRequest;
import com.hp.omp.model.impl.BudgetSubCategoryImpl;
import com.hp.omp.service.BudgetCategoryLocalServiceUtil;
import com.hp.omp.service.BudgetSubCategoryLocalServiceUtil;
import com.hp.omp.service.PurchaseOrderLocalServiceUtil;
import com.hp.omp.service.PurchaseRequestLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class BudgetSubCategoryAdminPortlet
 */
public class BudgetSubCategoryAdminPortlet extends MVCPortlet {
 
	private transient Logger logger = LoggerFactory.getLogger(BudgetSubCategoryAdminPortlet.class);
	private String errorMSG = "";
	private String successMSG = "";
	private String ACTION_PARAM= "action";
	private String ADD_ACTION= "add";
	private String UPDATE_ACTION= "update";
	private String DELETE_ACTION= "delete";
	private String PAGINATION_ACTION= "pagination";
	
	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#doView(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void doView(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		super.doView(renderRequest, renderResponse);
	}
	
	
	/* (non-Javadoc)
	 * @see javax.portlet.GenericPortlet#render(javax.portlet.RenderRequest, javax.portlet.RenderResponse)
	 */
	@Override
	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {
		viewAddBudgetSubCategory(request, response);
		super.render(request, response);
	}
	
	public void viewAddBudgetSubCategory(RenderRequest renderRequest,
			RenderResponse renderResponse) throws IOException, PortletException {
		String action = renderRequest.getParameter(ACTION_PARAM);
		
		if(ADD_ACTION.equals(action)){
			renderRequest.setAttribute(ACTION_PARAM, ADD_ACTION);
			renderRequest.setAttribute("display",true);
		}else if(UPDATE_ACTION.equals(action)){
			String budgetSubCategoryId = renderRequest.getParameter("budgetSubCategoryId");
			BudgetSubCategory budgetSubCategory = getBudgetSubCategory(budgetSubCategoryId);
			
			logger.debug("render update action for : "+budgetSubCategory.getBudgetSubCategoryName());
			logger.debug("Display = "+budgetSubCategory.getDisplay());
			
			renderRequest.setAttribute(ACTION_PARAM, UPDATE_ACTION);
			renderRequest.setAttribute("budgetSubCategoryId", budgetSubCategory.getBudgetSubCategoryId());
			renderRequest.setAttribute("budgetSubCategoryName", budgetSubCategory.getBudgetSubCategoryName());
			renderRequest.setAttribute("display", budgetSubCategory.getDisplay());
		}
	}

	
	@Override
    public void serveResource(ResourceRequest resourceRequest,
                  ResourceResponse resourceResponse) throws IOException,
                  PortletException {
		String action=resourceRequest.getParameter(ACTION_PARAM);
		if(DELETE_ACTION.equals(action)){
			deleteBudgetSubCategory(resourceRequest, resourceResponse);
		}else if(PAGINATION_ACTION.equals(action)){
			paginationList(resourceRequest, resourceResponse);
		}
		
		clearMessages();
           super.serveResource(resourceRequest, resourceResponse);
    }
	
	/* (non-Javadoc)
	 * @see com.liferay.util.bridges.mvc.MVCPortlet#processAction(javax.portlet.ActionRequest, javax.portlet.ActionResponse)
	 */
	@Override
	public void processAction(ActionRequest actionRequest,
			ActionResponse actionResponse) throws IOException, PortletException {

		clearMessages();
		String action = actionRequest.getParameter(ACTION_PARAM);
		String budgetSubCategoryName = actionRequest.getParameter("budgetSubCategoryName");
		boolean display=ParamUtil.getBoolean(actionRequest,"display");
		String operation = "";
		
		logger.debug("budgetSubCategoryName="+budgetSubCategoryName);
		logger.debug("display = "+display);
		logger.debug("creating BudgetSubCategory object ...");
		BudgetSubCategory newBudgetSubCategory = new BudgetSubCategoryImpl();
		newBudgetSubCategory.setBudgetSubCategoryName(budgetSubCategoryName);
		newBudgetSubCategory.setDisplay(display);
		logger.debug("BudgetSubCategory object created !!");
		try {
			if(ADD_ACTION.equals(action)){
				operation = "added";
				logger.debug("adding new BudgetSubCategory ["+budgetSubCategoryName+"] in database ...");
				if(BudgetSubCategoryLocalServiceUtil.getBudgetSubCategoryByName(budgetSubCategoryName) == null){
					
					BudgetSubCategoryLocalServiceUtil.addBudgetSubCategory(newBudgetSubCategory);
					
					successMSG = "BudgetSubCategory name ["+budgetSubCategoryName+"] successfully Added";
					
				}else{
					errorMSG = "BudgetSubCategory name ["+budgetSubCategoryName+"] already existing";
				}
			}else if(UPDATE_ACTION.equals(action)){
				operation = "updated";
				String budgetSubCategoryId = actionRequest.getParameter("budgetSubCategoryId");
				
				newBudgetSubCategory.setBudgetSubCategoryId(Integer.valueOf(budgetSubCategoryId));
				
				if(! (isBudgetSubCategoryNameExists(newBudgetSubCategory, budgetSubCategoryName))){
					
					logger.debug("updating  BudgetSubCategory ["+budgetSubCategoryId+"] in database ...");
					
					
					BudgetSubCategoryLocalServiceUtil.updateBudgetSubCategory(newBudgetSubCategory);
					successMSG = "BudgetSubCategory Name ["+budgetSubCategoryName+"] successfully Updated";
				}else{
					errorMSG = "BudgetSubCategory name ["+budgetSubCategoryName+"] already existing";
				}
				
			}
			
			
		} catch (SystemException e) {
			errorMSG = "BudgetSubCategory ["+budgetSubCategoryName+"] not "+operation+".";
			logger.error("error happened while "+operation+" BudgetSubCategory ["+budgetSubCategoryName+"] in database.");
			logger.error(e.getMessage(), e);
		}catch (NumberFormatException nfe) {
			errorMSG = "BudgetSubCategory ["+budgetSubCategoryName+"] not "+operation+".";
			logger.error("error happened while "+operation+" BudgetSubCategory ["+budgetSubCategoryName+"] in database.");
			logger.error(nfe.getMessage(), nfe);
		}
		
		actionRequest.setAttribute("errorMSG", errorMSG);
		actionRequest.setAttribute("successMSG", successMSG);
		super.processAction(actionRequest, actionResponse);
	}
	
	private void retrieveBudgetSubCategoryListToRequest(PortletRequest renderRequest) throws SystemException{
		try {
			String searchFilter = ParamUtil.getString(renderRequest, "searchFilter","");
			int totalCount = BudgetSubCategoryLocalServiceUtil.getBudgetSubCategoriesCount();

			Integer pageNumber = ParamUtil.getInteger(renderRequest, "pageNumber", 1);
			Integer pageSize = OmpHelper.PAGE_SIZE;
			int start = (pageNumber - 1) * pageSize;
			int end = start + pageSize;

			renderRequest.setAttribute("numberOfPages", getTotalNumberOfPages(totalCount, pageSize));
			renderRequest.setAttribute("pageNumber", pageNumber);


			//List<BudgetSubCategory> budgetSubCategoryList = BudgetSubCategoryLocalServiceUtil.getBudgetSubCategories(start, end);
			List<BudgetSubCategory> budgetSubCategoryList = BudgetSubCategoryLocalServiceUtil.getBudgetSubCategoryList(start, end, searchFilter);

			renderRequest.setAttribute("budgetSubCategoryList", budgetSubCategoryList);
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			errorMSG = "can not retrieve BudgetSubCategory list";
			renderRequest.setAttribute("errorMSG", errorMSG);
		}
		
	}
	
	public void deleteBudgetSubCategory(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse)
			throws PortletException, IOException {
		clearMessages();
		
		String budgetSubCategoryId=resourceRequest.getParameter("budgetSubCategoryId");
		String budgetSubCategoryName=resourceRequest.getParameter("budgetSubCategoryName");
		String action=resourceRequest.getParameter("action");
		String jspPage = "/html/budgetsubcategoryadmin/budgetsubcategorylist.jsp";
		
		if(Validator.isBlank(action)){
			errorMSG = "No Action in the request";
		}else if(Validator.isBlank(budgetSubCategoryId)){
			errorMSG = "No Budget Sub Category Id in the request";
		}else{
			
			try {
				List<PurchaseOrder> purchaseOrderList = PurchaseOrderLocalServiceUtil.getPurchaseOrderByBudgetSubCategoryId(budgetSubCategoryId);
				List<PurchaseRequest> purchaseRequestList = PurchaseRequestLocalServiceUtil.getPurchaseRequestByBudgetSubCategoryId(budgetSubCategoryId);
				
				if(purchaseOrderList != null && ! (purchaseOrderList.isEmpty())){
					errorMSG = "Can Not remove BudgetSubCategory assigned to Purchase Order, BudgetSubCategory name ["+budgetSubCategoryName+"]";
				}else if(purchaseRequestList != null && ! (purchaseRequestList.isEmpty())){
					errorMSG = "Can Not remove BudgetSubCategory assigned to Purchase Request, BudgetSubCategory name ["+budgetSubCategoryName+"]";
				}else{
					BudgetSubCategoryLocalServiceUtil.deleteBudgetSubCategory(Long.valueOf(budgetSubCategoryId));
					successMSG = "BudgetSubCategory name ["+budgetSubCategoryName+"] successfully deleted";
				}
				
			} catch (NumberFormatException e) {
				logger.error(e.getMessage());
				errorMSG = "BudgetSubCategory Id is invalid";
			} catch (PortalException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove BudgetSubCategory";
			} catch (SystemException e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove BudgetSubCategory [most probably BudgetSubCategory is assigned.]";
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
				errorMSG = "Cannot remove BudgetSubCategory";
			}
			
		}
		
		//retrieve updated list
		try {
			retrieveBudgetSubCategoryListToRequest(resourceRequest);
		} catch (SystemException e) {
			logger.error(e.getMessage(), e);
			errorMSG = "can not retrieve BudgetSubCategory list";
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
		
	}
	
	private BudgetSubCategory getBudgetSubCategory(String budgetSubCategoryId){
		BudgetSubCategory budgetSubCategory = null;
		try {
			budgetSubCategory = BudgetSubCategoryLocalServiceUtil.getBudgetSubCategory(Integer.valueOf(budgetSubCategoryId));
		} catch (NumberFormatException e) {
			logger.error("BudgetSubCategory Id has wrong format", e);
		} catch (PortalException e) {
			logger.error("unexpected error", e);
		} catch (SystemException e) {
			logger.error("unexpected error", e);
		}
		
		return budgetSubCategory;
	}
	
	private boolean isBudgetSubCategoryNameExists(BudgetSubCategory newBudgetSubCategory, String budgetSubCategoryName) throws SystemException{
		
		BudgetSubCategory oldBudgetSubCategory = BudgetSubCategoryLocalServiceUtil.getBudgetSubCategoryByName(budgetSubCategoryName);
		boolean isExists = false;
		
		if(oldBudgetSubCategory != null && 
				(newBudgetSubCategory.getBudgetSubCategoryId() != oldBudgetSubCategory.getBudgetSubCategoryId()) ){
			isExists = true;
		}
		return isExists;
	}
	
	private void paginationList(ResourceRequest resourceRequest,
            ResourceResponse resourceResponse) throws PortletException, IOException{
		
		String jspPage = "/html/budgetsubcategoryadmin/budgetsubcategorylist.jsp";
		try {
			retrieveBudgetSubCategoryListToRequest(resourceRequest);
		} catch (SystemException e) {
		logger.error("error happened in pagination ... ",e);
		}
		
		resourceRequest.setAttribute("errorMSG", errorMSG);
		resourceRequest.setAttribute("successMSG", successMSG);
		getPortletContext().getRequestDispatcher(jspPage).include(resourceRequest, resourceResponse);
	}
	
	private Integer getTotalNumberOfPages(Integer totalNubmerOfResult, Integer pageSize) {
        return (int)Math.ceil((double)totalNubmerOfResult/pageSize);
	}
	
	private void clearMessages(){
		errorMSG = "";
		successMSG = "";
	}
	
	public void searchFilter(ActionRequest request, ActionResponse response) throws IOException, PortletException{
		response.setRenderParameter("searchFilter", "%" + ParamUtil.getString(request, "budgetSubCategoryName") + "%");  
	}

}
