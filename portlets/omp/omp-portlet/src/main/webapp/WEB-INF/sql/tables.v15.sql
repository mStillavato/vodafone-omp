create table SUB_PROJECT (
    subprojectId NUMBER(30) not null primary key,
    subprojectName VARCHAR(300) not null,
    description VARCHAR(300) null
);

--==========================================

ALTER TABLE OMP_PORTLET.SUB_PROJECT
 ADD CONSTRAINT UNIQUE_SUB_PROJECT_NAME
  UNIQUE (SUBPROJECTNAME)
  ENABLE VALIDATE;

--==========================================

create sequence SUB_PROJECT_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER SUB_PROJECT_ID_TRIG
  before insert on SUB_PROJECT              
  for each row
begin   
  if :NEW.subprojectId is null then 
    select SUB_PROJECT_ID_SEQ.nextval into :NEW.subprojectId from dual; 
  end if; 
end;

--==========================================

Insert into SUB_PROJECT
  ( SUBPROJECTNAME, DESCRIPTION)
 Values
   ('Test SUB Project', 'sub project description');


--===================================================================
--===============        PURCHASE_ORDER    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
RENAME COLUMN SUBPROJECTNAME TO SUBPROJECTID;

--==========================================

update PURCHASE_ORDER set  SUBPROJECTID = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
MODIFY (SUBPROJECTID NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
 ADD CONSTRAINT FK_PO_SUB_PROJECT 
  FOREIGN KEY (SUBPROJECTID) 
  REFERENCES OMP_PORTLET.SUB_PROJECT (SUBPROJECTID)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_ORDER set  SUBPROJECTID = 1  ;

--===========================================================================
--===============        PURCHASE_REQUEST    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
RENAME COLUMN SUBPROJECTNAME TO SUBPROJECTID;

--==========================================

update PURCHASE_REQUEST set  SUBPROJECTID = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
MODIFY (SUBPROJECTID NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
 ADD CONSTRAINT FK_PR_SUB_PROJECT 
  FOREIGN KEY (SUBPROJECTID) 
  REFERENCES OMP_PORTLET.SUB_PROJECT (SUBPROJECTID)
  ENABLE VALIDATE;



--===========================================================================
--===============        PROJECT_SUB_PROJECT    ===========================

create table PROJECT_SUB_PROJECT (
    projectSubProjectId NUMBER(30) not null primary key,
    projectId NUMBER(30) not null,
    subProjectId NUMBER(30) not null
);

--==========================================

ALTER TABLE OMP_PORTLET.PROJECT_SUB_PROJECT
 ADD CONSTRAINT FK_PSP_PROJECT 
  FOREIGN KEY (PROJECTID) 
  REFERENCES OMP_PORTLET.PROJECT (PROJECTID)
  ENABLE VALIDATE;

 --==========================================

ALTER TABLE OMP_PORTLET.PROJECT_SUB_PROJECT
 ADD CONSTRAINT FK_PSP_SUB_PROJECT  
  FOREIGN KEY (SUBPROJECTID) 
  REFERENCES OMP_PORTLET.SUB_PROJECT (SUBPROJECTID)
  ENABLE VALIDATE;

  --==========================================
  create sequence PROJECT_SUB_PROJECT_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER PROJECT_SUB_PROJECT_ID_TRIG
  before insert on PROJECT_SUB_PROJECT              
  for each row
begin   
  if :NEW.projectSubprojectId is null then 
    select PROJECT_SUB_PROJECT_ID_SEQ.nextval into :NEW.projectSubprojectId from dual; 
  end if; 
end;
