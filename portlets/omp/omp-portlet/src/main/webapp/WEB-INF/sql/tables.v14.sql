create table PROJECT (
    projectId NUMBER(30) not null primary key,
    projectName VARCHAR(300) not null,
    description VARCHAR(300) null
);

--==========================================

ALTER TABLE OMP_PORTLET.PROJECT
 ADD CONSTRAINT UNIQUE_PROJECT_NAME
  UNIQUE (PROJECTNAME)
  ENABLE VALIDATE;

--==========================================

create sequence PROJECT_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER PROJECT_ID_TRIG
  before insert on PROJECT              
  for each row
begin   
  if :NEW.projectId is null then 
    select PROJECT_ID_SEQ.nextval into :NEW.projectId from dual; 
  end if; 
end;

--==========================================

Insert into PROJECT
  ( PROJECTNAME, DESCRIPTION)
 Values
   ('Test Project', 'project description');


--===================================================================
--===============        PURCHASE_ORDER    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
RENAME COLUMN PROJECTNAME TO PROJECTID;

--==========================================

update PURCHASE_ORDER set  PROJECTID = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
MODIFY (PROJECTID NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
 ADD CONSTRAINT FK_PO_PROJECT 
  FOREIGN KEY (PROJECTID) 
  REFERENCES OMP_PORTLET.PROJECT (PROJECTID)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_ORDER set  PROJECTID = 1  ;

--===========================================================================
--===============        PURCHASE_REQUEST    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
RENAME COLUMN PROJECTNAME TO PROJECTID;

--==========================================

update PURCHASE_REQUEST set  PROJECTID = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
MODIFY (PROJECTID NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
 ADD CONSTRAINT FK_PR_PROJECT 
  FOREIGN KEY (PROJECTID) 
  REFERENCES OMP_PORTLET.PROJECT (PROJECTID)
  ENABLE VALIDATE;

--==========================================

