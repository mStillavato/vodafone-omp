create sequence COST_CENTER_ID_SEQ start with 1 increment by 1 
  NOMAXVALUE  
  NOCYCLE
  NOCACHE
  NOORDER; 
CREATE OR REPLACE TRIGGER  COST_CENTER_ID_TRIG
  before insert on COST_CENTER              
  for each row  
begin   
  if :NEW.costCenterId is null then 
    select COST_CENTER_ID_SEQ.nextval into :NEW.costCenterId from dual; 
  end if; 
end;

create sequence COST_CENTER_USER_ID_SEQ start with 1 increment by 1 
  NOMAXVALUE  
  NOCYCLE
  NOCACHE
  NOORDER; 
CREATE OR REPLACE TRIGGER  COST_CENTER_USER_ID_TRIG
  before insert on COST_CENTER_USER              
  for each row  
begin   
  if :NEW.costCenterUserId is null then 
    select COST_CENTER_USER_ID_SEQ.nextval into :NEW.costCenterUserId from dual; 
  end if; 
end;