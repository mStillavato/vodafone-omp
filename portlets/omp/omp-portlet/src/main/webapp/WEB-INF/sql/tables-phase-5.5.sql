
-- CR 49 add check field to list of values to display or not
alter table BUDGET_CATEGORY add  display char(1) default '1' not null check (display in ('0','1')) ;

alter table TRACKING_CODES add  display char(1) default '1' not null check (display in ('0','1')) ;

alter table BUDGET_SUB_CATEGORY add  display char(1) default '1' not null check (display in ('0','1')) ;

alter table MACRO_DRIVER add  display char(1) default '1' not null check (display in ('0','1')) ;

alter table MICRO_DRIVER add  display char(1) default '1' not null check (display in ('0','1')) ;

alter table ODNP_NAME add  display char(1) default '1' not null check (display in ('0','1')) ;

alter table PROJECT add  display char(1) default '1' not null check (display in ('0','1')) ;

alter table SUB_PROJECT add  display char(1) default '1' not null check (display in ('0','1')) ;

alter table VENDOR add  display char(1) default '1' not null check (display in ('0','1')) ;

alter table WBS_CODE add  display char(1) default '1' not null check (display in ('0','1')) ;

alter table COST_CENTER add  display char(1) default '1' not null check (display in ('0','1')) ;
