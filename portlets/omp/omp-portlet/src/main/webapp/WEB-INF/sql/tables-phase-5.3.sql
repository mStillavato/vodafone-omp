create table TRACKING_CODES (
	trackingCodeId NUMBER(30) not null primary key,
	trackingCodeName VARCHAR(300) not null,
	trackingCodeDescription VARCHAR(300) null,
	wbsCodeId            NUMBER(30),
	vendorId             NUMBER(30),
	projectId            NUMBER(30),
	subProjectId         NUMBER(30),
	budgetCategoryId     NUMBER(30), 
	budgetSubCategoryId  NUMBER(30),
	macroDriverId        NUMBER(30),
	microDriverId        NUMBER(30),
	odnpNameId           NUMBER(30)
	
);



create sequence TRACKING_CODE_ID_SEQ start with 1 increment by 1 
  NOMAXVALUE  
  NOCYCLE
  NOCACHE
  NOORDER; 
  
CREATE OR REPLACE TRIGGER OMP_PORTLET.TRACKING_CODE_ID_TRIG
  before insert ON OMP_PORTLET.TRACKING_CODES              
  for each row
begin   
  if :NEW.trackingCodeId is null then 
    select TRACKING_CODE_ID_SEQ.nextval into :NEW.trackingCodeId from dual; 
  end if; 
end;
/

 ALTER TABLE OMP_PORTLET.TRACKING_CODES ADD (
  CONSTRAINT FK_TC_BUDGET_CATEGORY 
  FOREIGN KEY (BUDGETCATEGORYID) 
  REFERENCES OMP_PORTLET.BUDGET_CATEGORY (BUDGETCATEGORYID)
  ENABLE VALIDATE,
  CONSTRAINT FK_TC_BUDGET_SUB_CATEGORY 
  FOREIGN KEY (BUDGETSUBCATEGORYID) 
  REFERENCES OMP_PORTLET.BUDGET_SUB_CATEGORY (BUDGETSUBCATEGORYID)
  ENABLE VALIDATE,
  CONSTRAINT FK_TC_MACRO_DRIVER 
  FOREIGN KEY (MACRODRIVERID) 
  REFERENCES OMP_PORTLET.MACRO_DRIVER (MACRODRIVERID)
  ENABLE VALIDATE,
  CONSTRAINT FK_TC_MICRO_DRIVER 
  FOREIGN KEY (MICRODRIVERID) 
  REFERENCES OMP_PORTLET.MICRO_DRIVER (MICRODRIVERID)
  ENABLE VALIDATE,
  CONSTRAINT FK_TC_ODNP_NAME 
  FOREIGN KEY (ODNPNAMEID) 
  REFERENCES OMP_PORTLET.ODNP_NAME (ODNPNAMEID)
  ENABLE VALIDATE,
  CONSTRAINT FK_TC_PROJECT 
  FOREIGN KEY (PROJECTID) 
  REFERENCES OMP_PORTLET.PROJECT (PROJECTID)
  ENABLE VALIDATE,
  CONSTRAINT FK_TC_SUB_PROJECT 
  FOREIGN KEY (SUBPROJECTID) 
  REFERENCES OMP_PORTLET.SUB_PROJECT (SUBPROJECTID)
  ENABLE VALIDATE,
  CONSTRAINT FK_TC_VENDOR 
  FOREIGN KEY (VENDORID) 
  REFERENCES OMP_PORTLET.VENDOR (VENDORID)
  ENABLE VALIDATE,
  CONSTRAINT FK_TC_WBS_CODE 
  FOREIGN KEY (WBSCODEID) 
  REFERENCES OMP_PORTLET.WBS_CODE (WBSCODEID)
  ENABLE VALIDATE);
  
  --- update purchase order table -----
  
ALTER TABLE  PURCHASE_ORDER ADD (TRACKINGCODEID   NUMBER(30));
ALTER TABLE  PURCHASE_ORDER ADD (WBSCODEID   NUMBER(30));


  --- update purchase request table -----
  
ALTER TABLE  PURCHASE_REQUEST ADD (TRACKINGCODEID   NUMBER(30));
ALTER TABLE  PURCHASE_REQUEST ADD (WBSCODEID   NUMBER(30));

  
  
  ------ CR # 43 - script to fill in po label number with position label number -------
  
  
  DECLARE
  v_LowValue  NUMBER ;
  v_HighValue NUMBER ;
  BEGIN
    select max(pos.POSITIONID) into v_HighValue from POSITION pos;
    select min(pos.POSITIONID) into v_LowValue from POSITION pos;
    
     FOR v_Counter IN REVERSE v_LowValue .. v_HighValue LOOP
    
     UPDATE  OMP_PORTLET.POSITION pos SET pos.POLABELNUMBER = 
        (select pos.LABELNUMBER FROM POSITION pos where pos.POSITIONID = v_Counter)
     where pos.POSITIONID = v_Counter 
     and pos.POLABELNUMBER is null;

  END LOOP;
 END;

--------------- SELECT PURCHASE REQUEST IDs HAVING MULTIPLE POSTIONS WITH DIFFERENT WBS CODES -----------------------
--------------------------------(SHOULD BE UPDATED MANUALLY FROM THE APPLICATION)------------------------------------

SELECT prwbs.prId
FROM 
	(	SELECT DISTINCT pr.PURCHASEREQUESTID prId, position.WBSCODEID wbs
		FROM PURCHASE_REQUEST pr 
			INNER JOIN POSITION position ON position.PURCHASEREQUESTID = pr.PURCHASEREQUESTID
	) prwbs 
GROUP BY prwbs.prId
HAVING count(*) > 1
ORDER BY prwbs.prId;

------------------ UPDATE PURCHASE REQUESTS WITH WBS CODES RETRIEVED FROM ASSOCIATED POSITIONS ----------------------

UPDATE PURCHASE_REQUEST pr
SET   pr.WBSCODEID	= (	SELECT allprwbs.wbsCode
						FROM  
							( SELECT DISTINCT pr.PURCHASEREQUESTID prId , position.WBSCODEID wbsCode
							  FROM PURCHASE_REQUEST pr 
								INNER JOIN POSITION position ON position.PURCHASEREQUESTID = pr.PURCHASEREQUESTID
							) allprwbs -- ALL PR/WBS COMBINATIONS

							INNER JOIN
						
							( SELECT prwbs.prId singlePrId
							  FROM
								  (SELECT DISTINCT pr.PURCHASEREQUESTID prId, position.WBSCODEID wbs
								   FROM PURCHASE_REQUEST pr 
										INNER JOIN POSITION position ON position.PURCHASEREQUESTID = pr.PURCHASEREQUESTID
								  ) prwbs
							  GROUP BY prwbs.prId
							  HAVING count(*) = 1
							) singleprwbs -- ONLY PRs HAVING POSITIONS WITH UNIQUE WBS 
								
							ON singleprwbs.singlePrId = allprwbs.prId
								
						WHERE pr.PURCHASEREQUESTID = allprwbs.prId
						);
						
--------------- SELECT PURCHASE ORDERS IDs HAVING NO POSITIONS -----------------------
SELECT DISTINCT po.PURCHASEORDERID
FROM PURCHASE_ORDER po LEFT JOIN POSITION position ON position.PURCHASEORDERID = po.PURCHASEORDERID
WHERE position.POSITIONID IS NULL;						

------------------ UPDATE PURCHASE ORDERS WITH WBS CODES RETRIEVED FROM ASSOCIATED PURCHASE REQUESTS ----------------------

UPDATE PURCHASE_ORDER po
SET po.WBSCODEID = ( SELECT alldata.wbsCodeID
                      
                      FROM (
                            SELECT c.PURCHASEORDERID,  c.LABELNUMBER, c.PRID, pr.WBSCODEID
                            FROM (SELECT a.PURCHASEORDERID,  a.LABELNUMBER, MIN(position.PURCHASEREQUESTID) PRID
                                    FROM (SELECT po.PURCHASEORDERID, MIN(position.LABELNUMBER) LABELNUMBER
                                          FROM PURCHASE_ORDER po
                                          INNER JOIN POSITION position ON position.PURCHASEORDERID = po.PURCHASEORDERID
                                          GROUP BY  po.PURCHASEORDERID) a
                                          INNER JOIN 
                                          POSITION position ON a.PURCHASEORDERID = position.PURCHASEORDERID AND a.LABELNUMBER = position.LABELNUMBER  
                                          INNER JOIN 
                                          PURCHASE_REQUEST pr ON position.PURCHASEREQUESTID = pr.PURCHASEREQUESTID
                                          
                                    GROUP BY  a.PURCHASEORDERID,  a.LABELNUMBER) c
                                    
                                    INNER JOIN PURCHASE_REQUEST pr ON c.PRID = pr.PURCHASEREQUESTID
                            ) alldata
                      
                      WHERE po.PURCHASEORDERID = alldata.PURCHASEORDERID
                    );
