alter table POSITION add (approved char(1) check (approved in ('0','1')));

alter table POSITION add icApproval char(1) null;

alter table POSITION add   approver  VARCHAR(300) null;

alter table POSITION add  trackingCode  VARCHAR(300) null;
