 DECLARE
  v_LowValue  NUMBER ;
  v_HighValue NUMBER ;
  gr_totalValue Number;
  po_totalValue Number;
  BEGIN
    select max(pr.PURCHASEREQUESTID) into v_HighValue from PURCHASE_REQUEST pr;
    select min(pr.PURCHASEREQUESTID) into v_LowValue from PURCHASE_REQUEST pr;
    
     FOR v_Counter IN REVERSE v_LowValue .. v_HighValue LOOP
     
     
     select sum(GR.grvalue) 
     into gr_totalValue 
     from Good_Receipt GR, position p, PURCHASE_REQUEST pr
            where pr.PURCHASEREQUESTID=v_Counter 
            and pr.PURCHASEREQUESTID = p.PURCHASEREQUESTID 
            and gr.positionId = p.positionId
            and GR.REGISTERATIONDATE is not null and GR.goodReceiptNumber is not null;
            
            
    select sum(p.quatity*p.actualUnitCost) into po_totalValue from  position p, PURCHASE_REQUEST pr
            where pr.PURCHASEREQUESTID=v_Counter and pr.PURCHASEREQUESTID = p.PURCHASEREQUESTID;
            
    UPDATE  OMP_PORTLET.PURCHASE_REQUEST pr SET pr.STATUS = 5
     where pr.PURCHASEREQUESTID = v_Counter
     and  gr_totalValue = po_totalValue;
    
  END LOOP;
 END;
 commit;