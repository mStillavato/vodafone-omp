create table PURCHASE_REQUEST (
	purchaseRequestId NUMBER(30) not null primary key,
	costCenter VARCHAR(300) null,
	vendor VARCHAR(300) null,
	buyer VARCHAR(300) null,
	ola VARCHAR(300) null,
	projectName VARCHAR(300) null,
	subProjectName VARCHAR(300) null,
	budgetCategory VARCHAR(300) null,
	budgetSubCategory VARCHAR(300) null,
	activityDescription VARCHAR(300) null,
	macroDriver VARCHAR(300) null,
	microDriver VARCHAR(300) null,
	odnpProgrameCode VARCHAR(300) null,
	odnpProgrameName VARCHAR(300) null,
	totalValue VARCHAR(300) null,
	equivalentCost VARCHAR(300) null,
	currency_ VARCHAR(300) null,
	status INTEGER not null,
	automatic char(1) check (automatic in ('0','1')),
	createdDate DATE default sysdate,
	createdUserId NUMBER(30) not null 
);

create table PURCHASE_ORDER (
	purchaseOrderId NUMBER(30) not null primary key,
	costCenter VARCHAR(300) null,
	vendor VARCHAR(300) null,
	buyer VARCHAR(300) null,
	ola VARCHAR(300) null,
	projectName VARCHAR(300) null,
	subProjectName VARCHAR(300) null,
	budgetCategory VARCHAR(300) null,
	budgetSubCategory VARCHAR(300) null,
	activityDescription VARCHAR(300) null,
	macroDriver VARCHAR(300) null,
	microDriver VARCHAR(300) null,
	odnpProgrameCode VARCHAR(300) null,
	odnpProgrameName VARCHAR(300) null,
	totalValue VARCHAR(300) null,
	equivalentCost VARCHAR(300) null,
	currency_ VARCHAR(300) null,
	status INTEGER not null,
	automatic char(1) check (automatic in ('0','1')),
	createdDate DATE default sysdate,
	createdUserId NUMBER(30) not null
);

create table POSITION (
	positionId NUMBER(30) not null primary key,
	fiscalYear VARCHAR(300) null,
	description VARCHAR(300) null,
	categoryCode VARCHAR(300) null,
	offerNumber VARCHAR(300) null,
	offerDate DATE null,
	quatity VARCHAR(300) null,
	deliveryDate DATE null,
	deliveryAddress VARCHAR(300) null,
	actualUnitCost VARCHAR(300) null,
	numberOfUnit VARCHAR(300) null,
	equivalentCost VARCHAR(300) null,
	assetNumber VARCHAR(300) null,
	shoppingCart VARCHAR(300) null,
	poNumber VARCHAR(300) null,
	evoLocation VARCHAR(300) null,
	wbsCode VARCHAR(300) null,
	receiverUserId NUMBER(30) null,
	purchaseRequestId NUMBER(30),
	purchaseOrderId NUMBER(30),
	createdDate DATE default sysdate,
	createdUserId NUMBER(30) not null,
	CONSTRAINT fk_position_pr FOREIGN KEY (purchaseRequestId)
	REFERENCES PURCHASE_REQUEST(purchaseRequestId),
	CONSTRAINT fk_position_po FOREIGN KEY (purchaseOrderId)
	REFERENCES PURCHASE_ORDER(purchaseOrderId)


);

create table GOOD_RECEIPT (
	goodReceiptId NUMBER(30) not null primary key,
	requestDate DATE null,
	percentage VARCHAR(300) null,
	grValue VARCHAR(300) null,
	competenceDate DATE null,
	registerationDate DATE null,
	status INTEGER,
	createdDate DATE default sysdate,
	createdUserId NUMBER(30) not null,
	positionId NUMBER(30) not null,
	CONSTRAINT fk_gr_position FOREIGN KEY (positionId)
	REFERENCES POSITION(positionId)
);


--------------AUDITING TABLES-------------------------
create table PR_AUDIT (
	auditId NUMBER(30) not null primary key,
	purchaseRequestId NUMBER(30) not null,
	status INTEGER not null,
	userId NUMBER(30) not null,
	changeDate DATE default sysdate
);
create table PO_AUDIT (
	auditId NUMBER(30) not null primary key,
	purchaseOrderId NUMBER(30) not null,
	status INTEGER not null,
	userId NUMBER(30) not null,
	changeDate DATE default sysdate NOT NULL
);
create table POSITION_AUDIT (
	auditId NUMBER(30) not null primary key,
	positionId NUMBER(30) not null,
	status INTEGER not null,
	userId NUMBER(30) not null,
	changeDate DATE default sysdate
);
create table GR_AUDIT (
	auditId NUMBER(30) not null primary key,
	goodReceiptId NUMBER(30),
	status INTEGER not null,
	userId NUMBER(30) not null,
	changeDate DATE default sysdate
);








