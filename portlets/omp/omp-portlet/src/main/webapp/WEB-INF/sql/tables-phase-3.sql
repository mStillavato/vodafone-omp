create table WBS_CODE (
	wbsCodeId NUMBER(30) not null primary key,
	wbsCodeName VARCHAR(300) not null,
	description VARCHAR(300) null
);


ALTER TABLE OMP_PORTLET.WBS_CODE
 ADD CONSTRAINT UNIQUE_WBSCODE_NAME
  UNIQUE (WBSCODENAME)
  ENABLE VALIDATE;
  
ALTER TABLE OMP_PORTLET.POSITION
RENAME COLUMN WBSCODE TO WBSCODEID;

update OMP_PORTLET.POSITION set WBSCODEID = null;

ALTER TABLE OMP_PORTLET.POSITION
MODIFY(WBSCODEID NUMBER(30));

ALTER TABLE OMP_PORTLET.POSITION
 ADD CONSTRAINT FK_WBS_CODE 
  FOREIGN KEY (WBSCODEID) 
  REFERENCES OMP_PORTLET.WBS_CODE (WBSCODEID)
  ENABLE VALIDATE;
  
 
--=================================================
alter table PURCHASE_ORDER add  fiscalYear  VARCHAR(300) null;

alter table PURCHASE_REQUEST add  fiscalYear  VARCHAR(300) null;

--============================================


create table PROJECT (
    projectId NUMBER(30) not null primary key,
    projectName VARCHAR(300) not null,
    description VARCHAR(300) null
);

--==========================================

ALTER TABLE OMP_PORTLET.PROJECT
 ADD CONSTRAINT UNIQUE_PROJECT_NAME
  UNIQUE (PROJECTNAME)
  ENABLE VALIDATE;

--==========================================



--===================================================================
--===============        PURCHASE_ORDER    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
RENAME COLUMN PROJECTNAME TO PROJECTID;

--==========================================

update PURCHASE_ORDER set  PROJECTID = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
MODIFY (PROJECTID NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
 ADD CONSTRAINT FK_PO_PROJECT 
  FOREIGN KEY (PROJECTID) 
  REFERENCES OMP_PORTLET.PROJECT (PROJECTID)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_ORDER set  PROJECTID = null  ;

--===========================================================================
--===============        PURCHASE_REQUEST    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
RENAME COLUMN PROJECTNAME TO PROJECTID;

--==========================================

update PURCHASE_REQUEST set  PROJECTID = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
MODIFY (PROJECTID NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
 ADD CONSTRAINT FK_PR_PROJECT 
  FOREIGN KEY (PROJECTID) 
  REFERENCES OMP_PORTLET.PROJECT (PROJECTID)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_REQUEST set  PROJECTID = null  ;

--==========================================


create table SUB_PROJECT (
    subprojectId NUMBER(30) not null primary key,
    subprojectName VARCHAR(300) not null,
    description VARCHAR(300) null
);

--==========================================

ALTER TABLE OMP_PORTLET.SUB_PROJECT
 ADD CONSTRAINT UNIQUE_SUB_PROJECT_NAME
  UNIQUE (SUBPROJECTNAME)
  ENABLE VALIDATE;




--===================================================================
--===============        PURCHASE_ORDER    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
RENAME COLUMN SUBPROJECTNAME TO SUBPROJECTID;

--==========================================

update PURCHASE_ORDER set  SUBPROJECTID = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
MODIFY (SUBPROJECTID NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
 ADD CONSTRAINT FK_PO_SUB_PROJECT 
  FOREIGN KEY (SUBPROJECTID) 
  REFERENCES OMP_PORTLET.SUB_PROJECT (SUBPROJECTID)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_ORDER set  SUBPROJECTID = null  ;

--===========================================================================
--===============        PURCHASE_REQUEST    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
RENAME COLUMN SUBPROJECTNAME TO SUBPROJECTID;

--==========================================

update PURCHASE_REQUEST set  SUBPROJECTID = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
MODIFY (SUBPROJECTID NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
 ADD CONSTRAINT FK_PR_SUB_PROJECT 
  FOREIGN KEY (SUBPROJECTID) 
  REFERENCES OMP_PORTLET.SUB_PROJECT (SUBPROJECTID)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_REQUEST set  SUBPROJECTID = null  ;

--===========================================================================
--===============        PROJECT_SUB_PROJECT    ===========================

create table PROJECT_SUB_PROJECT (
    projectSubProjectId NUMBER(30) not null primary key,
    projectId NUMBER(30) not null,
    subProjectId NUMBER(30) not null
);

--==========================================

ALTER TABLE OMP_PORTLET.PROJECT_SUB_PROJECT
 ADD CONSTRAINT FK_PSP_PROJECT 
  FOREIGN KEY (PROJECTID) 
  REFERENCES OMP_PORTLET.PROJECT (PROJECTID)
  ENABLE VALIDATE;

 --==========================================

ALTER TABLE OMP_PORTLET.PROJECT_SUB_PROJECT
 ADD CONSTRAINT FK_PSP_SUB_PROJECT  
  FOREIGN KEY (SUBPROJECTID) 
  REFERENCES OMP_PORTLET.SUB_PROJECT (SUBPROJECTID)
  ENABLE VALIDATE;


--============= BUDGET_CATEGORY =============
create table OMP_PORTLET.BUDGET_CATEGORY (
    budgetCategoryId NUMBER(30) not null primary key,
    budgetCategoryName VARCHAR(300) not null
);

--==========================================

ALTER TABLE OMP_PORTLET.BUDGET_CATEGORY
 ADD CONSTRAINT UNIQUE_BUDGET_CATEGORY_NAME
  UNIQUE (budgetCategoryName)
  ENABLE VALIDATE;



--===================================================================
--===============        PURCHASE_ORDER    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
RENAME COLUMN BUDGETCATEGORY TO budgetCategoryId;

--==========================================

update PURCHASE_ORDER set  budgetCategoryId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
MODIFY (budgetCategoryId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
 ADD CONSTRAINT FK_PO_BUDGET_CATEGORY 
  FOREIGN KEY (budgetCategoryId) 
  REFERENCES OMP_PORTLET.BUDGET_CATEGORY (budgetCategoryId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_ORDER set  budgetCategoryId = null  ;

--===========================================================================
--===============        PURCHASE_REQUEST    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
RENAME COLUMN BUDGETCATEGORY TO budgetCategoryId;

--==========================================

update PURCHASE_REQUEST set  budgetCategoryId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
MODIFY (budgetCategoryId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
 ADD CONSTRAINT FK_PR_BUDGET_CATEGORY 
  FOREIGN KEY (budgetCategoryId) 
  REFERENCES OMP_PORTLET.BUDGET_CATEGORY (budgetCategoryId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_REQUEST set  budgetCategoryId = null  ;






create table BUDGET_SUB_CATEGORY (
    budgetSubCategoryId NUMBER(30) not null primary key,
    budgetSubCategoryName VARCHAR(300) not null
);

--==========================================

ALTER TABLE OMP_PORTLET.BUDGET_SUB_CATEGORY
 ADD CONSTRAINT UNIQUE_SUB_CATEGORY_NAME
  UNIQUE (budgetSubCategoryName)
  ENABLE VALIDATE;



--===================================================================
--===============        PURCHASE_ORDER    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
RENAME COLUMN BUDGETSUBCATEGORY TO budgetSubCategoryId;

--==========================================

update PURCHASE_ORDER set  budgetSubCategoryId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
MODIFY (budgetSubCategoryId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
 ADD CONSTRAINT FK_PO_BUDGET_SUB_CATEGORY 
  FOREIGN KEY (budgetSubCategoryId) 
  REFERENCES OMP_PORTLET.BUDGET_SUB_CATEGORY (budgetSubCategoryId)
  ENABLE VALIDATE;
  


--==========================================

update PURCHASE_ORDER set  budgetSubCategoryId = null ;

--===========================================================================
--===============        PURCHASE_REQUEST    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
RENAME COLUMN BUDGETSUBCATEGORY TO budgetSubCategoryId;

--==========================================

update PURCHASE_REQUEST set  budgetSubCategoryId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
MODIFY (budgetSubCategoryId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
 ADD CONSTRAINT FK_PR_BUDGET_SUB_CATEGORY 
  FOREIGN KEY (budgetSubCategoryId) 
  REFERENCES OMP_PORTLET.BUDGET_SUB_CATEGORY (budgetSubCategoryId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_REQUEST set  budgetSubCategoryId = null  ;





create table VENDOR (
    vendorId NUMBER(30) not null primary key,
    supplier VARCHAR(300) not null,
	supplierCode VARCHAR(300) not null,
	vendorName VARCHAR(300) not null
);



--===================================================================
--===============        PURCHASE_ORDER    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
RENAME COLUMN VENDOR TO vendorId;

--==========================================

update PURCHASE_ORDER set  vendorId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
MODIFY (vendorId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
 ADD CONSTRAINT FK_PO_VENDOR 
  FOREIGN KEY (vendorId) 
  REFERENCES OMP_PORTLET.VENDOR (vendorId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_ORDER set  vendorId = null  ;

--===========================================================================
--===============        PURCHASE_REQUEST    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
RENAME COLUMN VENDOR TO vendorId;

--==========================================

update PURCHASE_REQUEST set  vendorId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
MODIFY (vendorId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
 ADD CONSTRAINT FK_PR_VENDOR 
  FOREIGN KEY (vendorId) 
  REFERENCES OMP_PORTLET.VENDOR (vendorId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_REQUEST set  vendorId = null  ;




create table MACRO_DRIVER (
    macroDriverId NUMBER(30) not null primary key,
	macroDriverName VARCHAR(300) not null
);

--==========================================

ALTER TABLE OMP_PORTLET.MACRO_DRIVER
 ADD CONSTRAINT UNIQUE_MACRO_DRIVER_NAME
  UNIQUE (macroDriverName)
  ENABLE VALIDATE;



--===================================================================
--===============        PURCHASE_ORDER    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
RENAME COLUMN MACRODRIVER TO macroDriverId;

--==========================================

update PURCHASE_ORDER set  macroDriverId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
MODIFY (macroDriverId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
 ADD CONSTRAINT FK_PO_MACRO_DRIVER
  FOREIGN KEY (macroDriverId) 
  REFERENCES OMP_PORTLET.MACRO_DRIVER (macroDriverId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_ORDER set  macroDriverId = null  ;

--===========================================================================
--===============        PURCHASE_REQUEST    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
RENAME COLUMN MACRODRIVER TO macroDriverId;

--==========================================

update PURCHASE_REQUEST set  macroDriverId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
MODIFY (macroDriverId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
 ADD CONSTRAINT FK_PR_MACRO_DRIVER
  FOREIGN KEY (macroDriverId) 
  REFERENCES OMP_PORTLET.MACRO_DRIVER (macroDriverId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_REQUEST set  macroDriverId = null  ;





create table MICRO_DRIVER (
    microDriverId NUMBER(30) not null primary key,
	microDriverName VARCHAR(300) not null
);

--==========================================

ALTER TABLE OMP_PORTLET.MICRO_DRIVER
 ADD CONSTRAINT UNIQUE_MICRO_DRIVER_NAME
  UNIQUE (microDriverName)
  ENABLE VALIDATE;


--===================================================================
--===============        PURCHASE_ORDER    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
RENAME COLUMN MICRODRIVER TO microDriverId;

--==========================================

update PURCHASE_ORDER set  microDriverId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
MODIFY (microDriverId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
 ADD CONSTRAINT FK_PO_MICRO_DRIVER
  FOREIGN KEY (microDriverId) 
  REFERENCES OMP_PORTLET.MICRO_DRIVER (microDriverId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_ORDER set  microDriverId = null  ;

--===========================================================================
--===============        PURCHASE_REQUEST    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
RENAME COLUMN MICRODRIVER TO microDriverId;

--==========================================

update PURCHASE_REQUEST set  microDriverId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
MODIFY (microDriverId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
 ADD CONSTRAINT FK_PR_MICRO_DRIVER
  FOREIGN KEY (microDriverId) 
  REFERENCES OMP_PORTLET.MICRO_DRIVER (microDriverId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_REQUEST set  microDriverId = null  ;





create table MACRO_MICRO_DRIVER (
    macroMicroDriverId NUMBER(30) not null primary key,
	macroDriverId NUMBER(30) not null,
	microDriverId NUMBER(30) not null
);


--==========================================

ALTER TABLE OMP_PORTLET.MACRO_MICRO_DRIVER
 ADD CONSTRAINT FK_MICRO_DRIVER 
  FOREIGN KEY (microDriverId) 
  REFERENCES OMP_PORTLET.MICRO_DRIVER (microDriverId)
  ENABLE VALIDATE;

 --==========================================

ALTER TABLE OMP_PORTLET.MACRO_MICRO_DRIVER
 ADD CONSTRAINT FK_MACRO_DRIVER  
  FOREIGN KEY (macroDriverId) 
  REFERENCES OMP_PORTLET.MACRO_DRIVER (macroDriverId)
  ENABLE VALIDATE;


--==========================================


create table ODNP_NAME (
    odnpNameId NUMBER(30) not null primary key,
	odnpNameName VARCHAR(300) not null
);

--==========================================

ALTER TABLE OMP_PORTLET.ODNP_NAME
 ADD CONSTRAINT UNIQUE_ODNP_NAME
  UNIQUE (odnpNameName)
  ENABLE VALIDATE;



--===================================================================
--===============        PURCHASE_ORDER    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
RENAME COLUMN ODNPPROGRAMENAME TO odnpNameId;

--==========================================

update PURCHASE_ORDER set  odnpNameId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
MODIFY (odnpNameId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
 ADD CONSTRAINT FK_PO_ODNP_NAME
  FOREIGN KEY (odnpNameId) 
  REFERENCES OMP_PORTLET.ODNP_NAME (odnpNameId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_ORDER set  odnpNameId = null  ;

--===========================================================================
--===============        PURCHASE_REQUEST    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
RENAME COLUMN ODNPPROGRAMENAME TO odnpNameId;

--==========================================

update PURCHASE_REQUEST set  odnpNameId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
MODIFY (odnpNameId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
 ADD CONSTRAINT FK_PR_ODNP_NAME
  FOREIGN KEY (odnpNameId) 
  REFERENCES OMP_PORTLET.ODNP_NAME (odnpNameId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_REQUEST set  odnpNameId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
RENAME COLUMN odnpProgrameCode TO odnpCodeId;



ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
RENAME COLUMN odnpProgrameCode TO odnpCodeId;



--==========================================
alter table PURCHASE_ORDER add  screenNameRequester  VARCHAR(300) null;
alter table PURCHASE_ORDER add  screenNameReciever  VARCHAR(300) null;

alter table PURCHASE_REQUEST add  screenNameRequester  VARCHAR(300) null;
alter table PURCHASE_REQUEST add  screenNameReciever  VARCHAR(300) null;
/