<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>

<div>
	<font color="green">${successMSG}</font> <br/> <font color="red">${errorMSG}</font>
</div>

<portlet:actionURL name="searchFilter" var="searchFilter" >
	<portlet:param name="redirect" value="${redirect}" />
</portlet:actionURL>  
<aui:form  class="cmxform" action="${searchFilter}" method="post" id="searchFilterForm">
	<table>
		<tr>
			<td>
				<aui:input label="Odnp Name " name="odnpNameAdminName" type="text" value=""></aui:input>
			</td>
			<td>
				<br>				
				<aui:button value="Search" type="submit" />
			</td>
		</tr>
	</table>				
</aui:form>
<br>

<display:table decorator="com.hp.omp.decorator.OdnpNameListDecorator" name="odnpNameList" list="odnpNameList" id="odnpName">
  <display:column property="odnpNameId" title="Delete" class="deleteicon"/>
  <display:column  title="Name" >
  
  
   <portlet:renderURL var="editOdnpName">
	    <portlet:param name="jspPage" value="/html/odnpnameadmin/edit.jsp" />
	    <portlet:param name="odnpNameId" value="${odnpName.odnpNameId}" />
	    <portlet:param name="action" value="update" />
	</portlet:renderURL>
	
  	<a href="${editOdnpName}">${odnpName.odnpNameName}</a>
  
  </display:column>
    <display:column property="display" title="Display" />
</display:table>
<%@include file="/html/tablepaging/paging.jsp"%>