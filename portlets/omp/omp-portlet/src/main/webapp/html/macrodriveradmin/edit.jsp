<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ page import="javax.portlet.PortletPreferences" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">  
<portlet:defineObjects />

<portlet:actionURL var="macroDriveradminURL">
<portlet:param name="jspPage" value="/html/macrodriveradmin/view.jsp" />
</portlet:actionURL>
<div class="adminform">
<aui:form method="post" action="<%=macroDriveradminURL%>">
<aui:input label="Macro Driver Name: " name="macroDriverName" type="text" value="${macroDriverName}">
	<aui:validator name="required"/>
</aui:input>
<aui:input name="action" type="hidden" value="${action}"/>

<c:if test="${not empty macroDriverId}">
<aui:input name="macroDriverId" type="hidden" value="${macroDriverId}"/>
</c:if>
<aui:input inlineLabel="top" label="Display" name="display" type="checkbox" checked="${display}" />
<aui:button type="submit" /> 
</aui:form> 
</div>