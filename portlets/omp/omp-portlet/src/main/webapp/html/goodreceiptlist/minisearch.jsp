
<script type="text/javascript">

function <portlet:namespace/>dopoMiniSearch(){
	advancedSearch = "on";
	var ddVal = $("#<portlet:namespace/>minisearchId").val();
	var url = '${doSearchURL}&searchAction=doPoSearch&miniSearch=true&miniSearchValue='+ddVal;
	<portlet:namespace/>ompListing(url);
}
</script>


<div class="searcharea">

<div class="advancedsearchcontainer"><a href="javascript:void(0)" data-toggle="popover" data-placement="left" data-content="" class="advancedsearchlink"  >Advanced Search</a>

<%@include file="searchfilters.jsp"%>
</div>

<div class="normalsearchcontainer">


	<input type="text" name="<portlet:namespace/>minisearch"  id="<portlet:namespace/>minisearchId" placeholder="Search..." value="${minisearchValue}">


	<input type="button" value="Submit" class="submitbtn"  id='<portlet:namespace/>doMiniSearch'   onclick='<portlet:namespace/>dopoMiniSearch();' />
</div>

  
	<div class="clear"></div>
</div>