<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div id="statusMessage">
	<c:if test="${not empty totalRecords }">
		<div class='portlet-msg-success' id="totalRecords">${totalRecords} records uploaded successfully</div>
	</c:if>
</div>

<display:table name="poList" id="poReceiverTable" 
			 decorator="com.hp.omp.decorator.PODecorator">
			
	<display:column title="<a href='javascript:void(null)' id='purchaseOrderId' onclick='orderPOList(this)'>PO #</a>" sortable="true">
		<c:if test="${poReceiverTable.tipoPr == 0}">
			<a href="/group/vodafone/purchase-order?GET_EDIT_PO=editPoForm&porderId=${poReceiverTable.purchaseOrderId}&redirect=${redirect}&finalStep=true">${poReceiverTable.purchaseOrderId}</a>
		</c:if>
		<c:if test="${poReceiverTable.tipoPr == 1}">
			<a href="/group/vodafone/purchase-order-nss?GET_EDIT_PO=editPoForm&porderId=${poReceiverTable.purchaseOrderId}&redirect=${redirect}&finalStep=true">${poReceiverTable.purchaseOrderId}</a>
		</c:if>
		<c:if test="${poReceiverTable.tipoPr == 2}">
			<a href="/group/vodafone/purchase-order-not-nss?GET_EDIT_PO=editPoForm&porderId=${poReceiverTable.purchaseOrderId}&redirect=${redirect}&finalStep=true">${poReceiverTable.purchaseOrderId}</a>
		</c:if>		
		<c:if test="${poReceiverTable.tipoPr == 3}">
			<a href="/group/vodafone/purchase-order-vbs?GET_EDIT_PO=editPoForm&porderId=${poReceiverTable.purchaseOrderId}&redirect=${redirect}&finalStep=true">${poReceiverTable.purchaseOrderId}</a>
		</c:if>	
	</display:column>
	<display:column property="screenNameRequester" title="<a href='javascript:void(null)' id='screenNameRequester' onclick='orderPOList(this)'>Requester</a>" />
	<display:column property="screenNameReciever" title="<a href='javascript:void(null)' id='screenNameReciever' onclick='orderPOList(this)'>Receiver</a>" />
	<display:column property="vendorName" title="<a href='javascript:void(null)' id='vendorName' onclick='orderPOList(this)'>Vendor</a>" />
	<display:column property="foramttedTotalValue" title="<a href='javascript:void(null)' id='totalValue' onclick='orderPOList(this)'>Total Value</a>"/>
	<display:column property="fiscalYear" title="<a href='javascript:void(null)' id='fiscalYear' onclick='orderPOList(this)'>Fiscal Year</a>" />
	<display:column property="activityDescription" title="<a href='javascript:void(null)' id='activityDescription' onclick='orderPOList(this)'>Description</a>"/>
	<display:column property="status" title="Status"/>
</display:table>

<%@include file="/html/tablepaging/paging.jsp"%>
