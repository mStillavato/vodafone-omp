<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<script type="text/javascript" src="../../omp_main-theme/js/jquery-1.10.2.min.js"></script>  
<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">     
<portlet:defineObjects />

<div id="<portlet:namespace/>vendorTableList"  class="listingcontainer"></div>
<div class="loading" style='display:none;font-size:30px; color:#eee;margin-top:60px; text-align: center;'>Loading...</div>
<div>
<portlet:renderURL var="addvendorURL">
    <portlet:param name="jspPage" value="/html/vendoradmin/edit.jsp" />
</portlet:renderURL>
<p><a href="<%= addvendorURL %>&action=add" class="actionbutn"> add new vendor</a></p>

<portlet:resourceURL var="exportListUrl" id="exportList" escapeXml="false" >
	<portlet:param name="action" value="exportList"/>
</portlet:resourceURL>
<a href="javascript:void(null);" class="actionbutn" onclick="<portlet:namespace/>exportList()">Export Vendor</a>

<portlet:resourceURL var="deletevendorURL">
	<portlet:param name="action" value="delete"/>
</portlet:resourceURL>
<portlet:resourceURL var="paginationURL">
</portlet:resourceURL>
	<script type="text/javascript">
	 	function removeVendor(vendorId, vendorName){
		 if(confirm("Do you want to delete this vendor ["+vendorName+"]?")) {
			 var url="${deletevendorURL}&vendorId="+vendorId+"&vendorName="+vendorName;
				jQuery.get(url,function( data ) {
					  jQuery( "#<portlet:namespace/>vendorTableList" ).html( data );
				});
			}
	 	}
	 
	 
		function <portlet:namespace/>ompListing(url){
			jQuery("#<portlet:namespace/>vendorTableList").fadeOut( "fast" );
			jQuery(".loading").fadeIn( "normal" );
			jQuery.post(url+"&action=pagination",
					 function(data) {
					   jQuery("#<portlet:namespace/>vendorTableList").html(data);
					   jQuery(".loading").fadeOut( "fast" );
					   jQuery("#<portlet:namespace/>vendorTableList").fadeIn( "fast" );
					 }
			);
		}
	 
		function <portlet:namespace/>exportList(){
			document.location.href ='${exportListUrl}';
		}
	
		<portlet:namespace/>ompListing('${paginationURL}');
	</script>
</div>