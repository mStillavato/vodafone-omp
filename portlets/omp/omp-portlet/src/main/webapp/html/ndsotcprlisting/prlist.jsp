<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

	<liferay-ui:error key="Unassgined-CostCenter" message="You don't have a Cost Center number. Please contact the administratro to assign you to a cost center." />

	<display:table decorator="com.hp.omp.decorator.PRListDecorator"  name="prList" id="pr">
	
	
 <display:column property="purchaseRequestId" class="rqstnum" title="<a href='javascript:void(null)' id='purchaseRequestId' onclick='orderRequesterPRList(this)'>OMP Request #</a>" />
 	  <display:column property="screenNameRequester" title="<a href='javascript:void(null)' id='screenNameRequester' onclick='orderRequesterPRList(this)'>Requestor</a>" class="substring"/>
 	  <display:column property="screenNameReciever" title="<a href='javascript:void(null)' id='screenNameReciever' onclick='orderRequesterPRList(this)'>Receiver</a>" />
	  <display:column property="vendorName" title="<a href='javascript:void(null)' id='vendorName' onclick='orderRequesterPRList(this)'>Vendor</a>" class="substring"/>
	  <display:column property="budgetSubCategoryName" title="<a href='javascript:void(null)' id='budgetSubCategoryName' onclick='orderRequesterPRList(this)'>Sub Category</a>" class="substring"/> 
	  <display:column property="foramttedTotalValue" title="<a href='javascript:void(null)' id='totalValue' onclick='orderRequesterPRList(this)'>Total Value</a>" class="substring"/>
	   <display:column property="costCenterName" title="<a href='javascript:void(null)' id='costCenterName' onclick='orderRequesterPRList(this)'>Cost Center</a>" />
	  <display:column property="fiscalYear" title="<a href='javascript:void(null)' id='fiscalYear' onclick='orderRequesterPRList(this)'>Fiscal Year</a>" />
	  <display:column property="activityDescription" title="<a href='javascript:void(null)' id='activityDescription' onclick='orderRequesterPRList(this)'>Description</a>" class="substring"/>
	  <display:column property="status" title="Status"/>
	  <display:column title="Download">
	  <c:choose>
	  <c:when test="${pr.automatic==true}">
		<portlet:resourceURL id="downloadFile" var="downloadFileURL" >
			<portlet:param name="action" value="downloadFile"/>
			<portlet:param name="purchaseRequestId" value="${pr.purchaseRequestId}"/>
		</portlet:resourceURL>
	  <a href="${downloadFileURL}">Download</a>
	  </c:when>
	  <c:otherwise>
	  <c:if test="${pr.hasFile == true}">
		<portlet:resourceURL id="downloadZipFile" var="downloadZipFileURL" >
			<portlet:param name="action" value="downloadZipFile"/>
			<portlet:param name="purchaseRequestId" value="${pr.purchaseRequestId}"/>
		</portlet:resourceURL>
	  <a href="${downloadZipFileURL}">Download</a>
	  </c:if>
	  </c:otherwise>
	  </c:choose>
	  </display:column>
	</display:table>

<%@include file="/html/tablepaging/paging.jsp"%>
