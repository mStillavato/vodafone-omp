<%@include file="init.jsp"%>
<%

String purchaseOrderPortletName = "Purchase-Order";
String purchaseRequestPortletPageName = "/purchase-order";
String redirect = PortalUtil.getCurrentURL(renderRequest);
Group guestGroup = GroupLocalServiceUtil.getGroup(themeDisplay.getCompanyId(), "Guest");
Long pLid = LayoutLocalServiceUtil.getFriendlyURLLayout(guestGroup.getGroupId(),false,purchaseRequestPortletPageName).getPlid();


%>

<div>
		<liferay-portlet:renderURL plid='<%=pLid%>' portletName="<%=purchaseOrderPortletName %>" var="purchaseOrderPortletURL" >
	 		<liferay-portlet:param  name="jspPage" value="/html/purchaserequest/view.jsp" />
	 		<liferay-portlet:param name="mvcPath" value="/html/purchaserequest/view.jsp"/>
	 		<liferay-portlet:param  name="automatic" value="false" />
	 		<liferay-portlet:param  name="redirect" value="<%= redirect %>" />
		 </liferay-portlet:renderURL>
		 
		 	<a href="<%= purchaseOrderPortletURL.toString() %>" >add manual request </a>
</div>

<div id="created">

	<display:table decorator="com.hp.omp.decorator.POListDecorator" name="poListCreatedArray" list="prListArray" pagesize="20" class="simple" >
	
	
	  <display:column property="purchaseOrdertId" title="ID" sortable="true" class="rqstnum"/>
	  <display:column property="receiverUserId" title="Receiver" sortable="true"/>
	  <display:column property="vendor" title="Vendor" sortable="true"/>
	  <display:column property="totalValue" title="totalValue" sortable="true"/>
	  <display:column property="activityDescription" title="Description"/>
	   <display:column property="fiscalYear" title="Fiscal Year" />
	  <display:column property="status" title="Status"/>
	</display:table>

	
</div>

<div id="otheres">
	<portlet:actionURL var="loadPRListByStatus" name="loadPRListByStatus" >
		<portlet:param name="redirect" value="<%=redirect %>"/>
	</portlet:actionURL>
	
<form action="<%= loadPRListByStatus%>" method="post" name="<portlet:namespace />fm" >
	<aui:select id="statusSelection" name="status" onChange='this.form.submit()' >
		<aui:option name="" label="Other Requests" />
		<%
		ListIterator<String>  statusNamesItr = ( (List) request.getAttribute("statusNames")).listIterator();
  		String statusName = "";
		 while(statusNamesItr.hasNext()){ 
			 statusName = statusNamesItr.next();
		 %>
		<aui:option label="<%= statusName %>" value="<%=statusName %>"  />
				
		<%} %>
	</aui:select>

	<display:table decorator="com.hp.omp.decorator.PRListDecorator" name="prListArray" list="prListArray" pagesize="10" class="simple" >
	
	
	  <display:column property="purchaseRequestId" title="ID" sortable="true" class="rqstnum"/>
	  <display:column property="receiverUserId" title="Receiver" sortable="true"/>
	  <display:column property="vendor" title="Vendor" sortable="true"/>
	  <display:column property="totalValue" title="totalValue" sortable="true"/>
	   <display:column property="fiscalYear" title="Fiscal Year" />
	  <display:column property="activityDescription" title="Description"/>
	  <display:column property="status" title="Status"/>
	  
	</display:table>

<noscript><input type="submit" value="Submit"></noscript>

	</form>
</div>