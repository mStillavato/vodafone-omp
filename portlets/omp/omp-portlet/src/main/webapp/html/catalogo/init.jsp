<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/portlet"  prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/security" prefix="liferay-security" %>
<%@ taglib uri="http://liferay.com/tld/theme"    prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui"       prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/util"     prefix="liferay-util" %>
<%@ taglib uri="http://liferay.com/tld/aui"      prefix="aui" %>
<%@ taglib uri="http://displaytag.sf.net"        prefix="display" %>

<%@ page import="java.util.List" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow" %>
<%@ page import="com.liferay.portal.kernel.util.GetterUtil" %>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil" %>
<%@ page import="com.liferay.portal.kernel.util.WebKeys" %>
<%@ page import="com.liferay.portal.model.Group"%>
<%@ page import="com.liferay.portal.security.permission.ActionKeys"%>
<%@ page import="com.liferay.portal.util.PortalUtil" %>
<%@page import="com.hp.omp.model.custom.OmpGroups"%>
<%@page import="com.hp.omp.helper.OmpHelper"%>
<%@page import="com.liferay.portal.kernel.util.StringUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ page import="com.liferay.portal.kernel.util.Validator"%>
<%@ page import="com.liferay.util.PwdGenerator"%>

<liferay-theme:defineObjects />

