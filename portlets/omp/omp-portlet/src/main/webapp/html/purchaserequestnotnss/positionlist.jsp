<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<liferay-theme:defineObjects />
<portlet:defineObjects />

<c:if test="${numberOfPositions > 0 }">
	<form id='positionsTableForm'>
		<div class='panel-group' id='positionsaccordion'>
			<c:forEach items="${positions}" var="position" varStatus="i">
				<div class='panel-heading'><div id='positionErrors_tr${i.index}' class="positionerrormsghead"></div>
					<h4 class='panel-title'>
					
						<c:if test="${(userType == 'NDSO' || userType == 'TC' || userType == 'CONTROLLER') && actionStatus == 'Pending IC approval' }">
							<c:if test="${position.approved == false}">
								<input type='checkbox' name='positionsSel_${i.index}' id='positionsSel_${i.index}' value='${position.positionId}' onchange='enableApproveBtn();'/>
							</c:if>
							<c:if test="${position.approved == true}">
								<input type='checkbox' name='positionsSel_${i.index}' id='positionsSel_${i.index}' value='${position.positionId}' checked disabled/>
							</c:if>
						</c:if>
						
						<a data-toggle='collapse' data-parent='#positionsaccordion' href='#collapse${i.index}'>Position No: ${position.labelNumber}</a>
						<c:if test="${position.positionEditBtton==true}">
							<div class='positioneditbtnarea' id='positionErrors_td${i.index}'>
								<input type='button' id='editPositionBtnTr'  value ='Edit' class='positioneditbtn' data-toggle='modal' data-target='#addpositionmodal' onClick='fillPositionForm(${i.index});'>
							</div>
						</c:if>
						<div class='clear'></div>
					</h4>
				</div>
				<div class='clear'></div>
				
				<div id='collapse${i.index}' class='panel-collapse in'>
					<div class='panel-body'>
						<div class='posaccordlabel'>I&C</div><div class='posaccordinfo'>${position.iCApprovalText}</div>
						<div class='posaccordlabel'>Position Status</div><div class='posaccordinfo'>${position.positionStatusLabel}</div>
						<div class='posaccordlabel'>Approver</div><div class='posaccordinfo'>${position.approverName}</div>
						<div class='clear'></div>
						
						<div class='posaccordlabel'>Fiscal Year</div>
						<div class='posaccordinfo'>${position.fiscalYear}</div>
						
						<div class='posaccordlabel'>Category Code</div>
						<div class='posaccordinfo'>${position.categoryCode}</div>
						
						<div class='posaccordlabel'>Offer Number</div>
						<div class='posaccordinfo'>${position.offerNumber}</div>
						<div class='clear'></div>
						
						<div class='posaccordlabel'>Delivery Date</div>
						<div class='posaccordinfo'>${position.deliveryDateText}</div>
						
						<div class='posaccordlabel'>Delivery Address</div>
						<div class='posaccordinfo'>${position.deliveryAddress}</div>
						<div class='clear'></div>

						<div class='posaccordlabel'>Targa Tecnica</div>
						<div class='posaccordinfo'>${position.targaTecnica}</div>
						
						<div class='posaccordlabel'>Location EVO</div>
						<div class='posaccordinfo'>${position.evoLocation}</div>
						<div class='clear'></div>
												 			 	 
						<div class='posaccordlabel'>Material ID</div>
						<div class='posaccordinfo'>${position.evoCodMateriale}</div>
						<div class='clear'></div>
						
						<div class='posaccordlabel'>Material Description</div>
						<div class='posaccorddesc'>${position.evoDesMateriale}</div>
						<div class='clear'></div> 			 	 
						 			 	 
						<div class='posaccordlabel'>Asset Number</div><div class='posaccordinfo'>${position.assetNumber}</div>
						<div class='posaccordlabel'>PR/SC Number</div> <div class='posaccordinfo'>${position.shoppingCart}</div>
						<div class='posaccordlabel'>PO Number</div><div class='posaccordinfo'>${position.poNumber}</div>
						<div class='clear'></div>
						
						<div class='posaccordlabel'>Quantity</div><div class='posaccordinfo'>${position.quatity}</div>
						<div class='posaccordlabel'>Unit Cost</div> <div class='posaccordinfo'>${position.formattedUnitCost}</div>
						<div class='clear'></div>
						 
						<div class='posaccordlabel'>Description</div>
						<div class='posaccorddesc'>${position.description}</div>
						<div class='clear'></div>
						
						<input type='hidden' id='positionId_tr${i.index}' value='${position.positionId}'/>
						<input type='hidden' id='labelNumber_tr${i.index}' value='${position.labelNumber}'/>
						<input type='hidden' id='positionStatus_tr${i.index}' value='${position.positionStatus}'/>
						<input type='hidden' id='fiscalyear_tr${i.index}' value='${position.fiscalYear}'/>
						<input type='hidden' id='categorycode_tr${i.index}' value='${position.categoryCode}'/>
						<input type='hidden' id='offernumber_tr${i.index}' value='${position.offerNumber}'/>
						<input type='hidden' id='quantity_tr${i.index}' value='${position.quatity}'/>
						<input type='hidden' id='deliverydate_tr${i.index}' value='${position.deliveryDateText}'/>
						<input type='hidden' id='deliveryadress_tr${i.index}' value='${position.deliveryAddress}'/>
						<input type='hidden' id='asset-number_tr${i.index}' value='${position.assetNumber}'/>
						<input type='hidden' id='prcs-number_tr${i.index}' value='${position.shoppingCart}'/>
						<input type='hidden' id='po-number_tr${i.index}' value='${position.poNumber}'/>
						<input type='hidden' id='actual-unit-cost_tr${i.index}' value='${position.commaFormattedUnitCost}'/>
						<input type='hidden' id='position-description_tr${i.index}' value='${position.description}'/>
						<input type='hidden' id='iandc_tr${i.index}' value='${position.icApproval}'/>
						<input type='hidden' id='positionStatusLabel_tr${i.index}' value='${position.positionStatusLabel}'/>
						<input type='hidden' id='approverName_tr${i.index}' value='${position.approverName}'/>
						<input type='hidden' id='approved_tr${i.index}' value='${position.approved}'/>
						<input type='hidden' id='approveBtn_tr${i.index}' value='${position.approveBtn}'/>
						<input type='hidden' id='evoCodMateriale_tr${i.index}' value='${position.evoCodMateriale}' />
						<input type='hidden' id='evoDesMateriale_tr${i.index}' value='${position.evoDesMateriale}' />
						<input type='hidden' id='targaTecnica_tr${i.index}' value='${position.targaTecnica}' /> 
						<input type='hidden' id='locationEvo_tr${i.index}' value='${position.evoLocation}' /> 
						<div class='clear'></div>
					</div>
				</div>
			</c:forEach>
		</div>
	</form>
</c:if>

<c:if test="${hideReject =='true' }">
	<script type="text/javascript">
		document.getElementById('rejectPurchaseRequestBtn').style.display='none';
	</script>
</c:if>

<script type="text/javascript">
	if(${prTotalValue} != "" || ${prTotalValue} != "0"){
		$("#totalvalue").val("${prTotalValue}");
	}
	$("#numberOfPositions").val("${numberOfPositions}");
</script>
		