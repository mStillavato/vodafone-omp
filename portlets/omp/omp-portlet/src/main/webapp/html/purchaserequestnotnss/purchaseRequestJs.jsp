<%@ page import="com.hp.omp.model.TrackingCodes" %>
<%@ page import="java.util.List" %>
<%@ page import="com.hp.omp.service.TrackingCodesLocalServiceUtil" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://hp.com/vfItaly/jsp/taglib" prefix="hp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

	<script src="<%=request.getContextPath()%>/js/upload Ajax/ajaxfileupload.js"></script>
	
	<portlet:resourceURL var="addPosition" id="addPosition" escapeXml="false" >
		<portlet:param name="action" value="addPosition"/>
	</portlet:resourceURL>
	
	<portlet:resourceURL var="updatePosition" id="updatePosition" escapeXml="false" >
		<portlet:param name="action" value="updatePosition"/>
	</portlet:resourceURL>
	
	<portlet:resourceURL var="deletePosition" id="deletePosition" escapeXml="false" >
		<portlet:param name="action" value="deletePosition"/>
	</portlet:resourceURL>
	
	<portlet:resourceURL var="approvePosition" id="approvePosition" escapeXml="false" >
		<portlet:param name="action" value="approvePosition"/>
	</portlet:resourceURL>
	
	<portlet:resourceURL var="approvePositionSel" id="approvePositionSel"	escapeXml="false">
		<portlet:param name="action" value="approvePositionSel" />
	</portlet:resourceURL>
	
	<portlet:resourceURL var="addPurchaseRequest" id="addPurchaseRequest" escapeXml="false" >
		<portlet:param name="action" value="addPurchaseRequest"/>
	</portlet:resourceURL>
	
	<portlet:resourceURL var="updatePurchaseRequest" id="updatePurchaseRequest" escapeXml="false" >
		<portlet:param name="action" value="updatePurchaseRequest"/>
	</portlet:resourceURL>
	
	<portlet:actionURL name="submitPurchaseRequest"  var="submitPurchaseRequestURL" >
	</portlet:actionURL>
	
	<portlet:actionURL name="deletePurchaseRequest" var="deletePurchaseRequestURL" >
		<portlet:param name="redirect" value="/group/vodafone/home" />
	</portlet:actionURL>
	
	<portlet:actionURL name="startPurchaseRequest" var="startPurchaseRequestURL" >
	</portlet:actionURL>
	
	<portlet:actionURL name="rejectPurchaseRequest" var="rejectPurchaseRequestURL" >
	</portlet:actionURL>
	
	<portlet:actionURL name="approvePositionAll" var="approvePositionAllURL">
	</portlet:actionURL>
	
	<portlet:resourceURL id="downloadFile" var="downloadFileURL" >
		<portlet:param name="action" value="downloadFile"/>
	</portlet:resourceURL>
	
	<portlet:resourceURL id="deleteFile" var="deleteFileURL" >
		<portlet:param name="action" value="deleteFile"/>
	</portlet:resourceURL>
	
	<portlet:resourceURL id="downloadallFiles" var="downloadAllFilesURL" >
		<portlet:param name="action" value="downloadAllFiles"/>
	</portlet:resourceURL>
	
	<portlet:resourceURL id="uploadFileURL" var="uploadFileURL">
		<portlet:param name="action" value="uploadFile" />
	</portlet:resourceURL>
	
	<portlet:resourceURL id="uploadFileOffertaURL" var="uploadFileOffertaURL">
		<portlet:param name="action" value="uploadFileOfferta" />
	</portlet:resourceURL>
	
	<portlet:resourceURL id="listUploaded" var="listUploadedFilesURL">
		<portlet:param name="action" value="listFiles" />
	</portlet:resourceURL>
	
	<portlet:resourceURL id="refreshPosition" var="refreshPositionURL">
		<portlet:param name="action" value="refreshPosition"/>
	</portlet:resourceURL>
	
	<portlet:resourceURL id="poulateTrackingValues" var="poulateTrackingValuesUrl"/>
	
	<portlet:resourceURL id="getLabelNumber" var="getLabelNumberURL" >
		<portlet:param name="action" value="getLabelNumber"/>
	</portlet:resourceURL>
	
	
	<script type="text/javascript">
	
	var validatePos;
	function addUpdatepurchaseRequest(){
		
		resetRequiredFields();
		
		var purchaseRequestId=document.getElementById("purchaseRequestId").value;
		var automatic=document.getElementById("automatic").value;
		var costcenter=document.getElementById("costcenter").value;
		var ola=document.getElementById("ola").value;
		var buyer=document.getElementById("buyer").value;
		var vendor=document.getElementById("vendor").value;
		var projectname=document.getElementById("projectname").value;
		var totalvalue=document.getElementById("totalvalue").value;
		var prCurrency=document.getElementById("prCurrency").value;
		var activitydescription=document.getElementById("activitydescription").value;
		var trackingcode=document.getElementById("trackingcode").value;
		var prwbscode=document.getElementById("prwbscode").value;
		var resourceURL;

		if(purchaseRequestId !== null && purchaseRequestId != ""){
			resourceURL='${updatePurchaseRequest}';
			//alert("updatepurchaseRequest  "+resourceURL);
			
			var actionStatus = document.getElementById("actionStatus").value;
			var userType = document.getElementById("userType").value;
			if((actionStatus !="Created" && actionStatus !="Rejected") && userType =="CONTROLLER" ){
				if(!validatePR()){
					return false;
				}
				
			}
		}else{
			resourceURL='${addPurchaseRequest}';
			//alert("addpurchaseRequest "+resourceURL);
		}
	
		  jQuery.ajax({
			    type: "POST",
			    dataType: "json",
			    url: resourceURL,
			    data: {"purchaseRequestId":purchaseRequestId,"automatic":automatic,"costcenter":costcenter,"ola":ola,"buyer":buyer,"vendor":vendor,
			    	"projectname":projectname,"totalvalue":totalvalue,"prCurrency":prCurrency,"activitydescription":activitydescription,
			    	"trackingcode":trackingcode,"prwbscode":prwbscode},
			    success: function(data) {
			    	//alert(data);
			    	if(data.status == "success"){
		    			document.getElementById("purchaseRequestId").value = data.purchaseRequestID;
			    		document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-success">OMP Request # '+data.purchaseRequestID+'  Saved Successfully</div>';
			    		var actionStatus=document.getElementById("actionStatus").value;
			    		if(actionStatus != 'Rejected'){
			    			document.getElementById("deleteBtnDiv").innerHTML='<button type="button" class="cancelbtn" onclick="deletepurchaseRequest();" value="delete">delete</button>';
			    		}
				    		 
			    	} else if(data.status == "trackingCodeKO"){
			    		 document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-danger">Tracking Code non presente in archivio</div>';
			    		 
			    	} else if(data.status == "failure"){
			    		 document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-danger">Your purchase Request failed to Save</div>';
			    	}
			    	
			    },
			    error : function(XMLHttpRequest, textStatus, errorThrown) 
			    {
			       document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-danger">Your purchase Request failed to Save</div>';
			    }
			    });
		
	}
	
	function validatePREntities(){
	
		var numberofPositions=document.getElementById("numberOfPositions").value;
		//alert("numberofPositions = "+numberofPositions);
		if(numberofPositions == "0"){
			alert("number of positions = 0  you should first add at least one position");
			return false;
		} else {
			if(validateRequiredFields()){
				//alert("validation is ok will submit");
				return true;
			}else{
				//alert("validation error");
				 document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-danger">your purchase Request contains missing parameters complete fields in Red color</div>';
				 return false;
			}
		}
	}
	
	function submitpurchaseRequest(){
		var actionURL;
		actionURL='${submitPurchaseRequestURL}';
		
		if(validatePR()){
			$(".trackingrelated").prop('disabled',false);
			document.getElementById("editpurchaseRequestForm").action=actionURL;
			document.getElementById("editpurchaseRequestForm").submit();
		}
	}
	
	function checkIC(){
		var numberofPositions = document.getElementById("numberOfPositions").value;
		var ic = 0;
		var viewPopupIcc = 0;
		
		for(i = 0; i < (numberofPositions); i++){
			ic = document.getElementById("iandc_tr" + i).value;
			if(ic == 1 || ic == 2){
				viewPopupIcc = 1;
				break;
			}
		}
		
		if(viewPopupIcc == 1) {
			$("#presaVisione").prop('checked', false);
			$("#iccCloseBtn").prop('disabled', true);
			$('#viewModalIcc').modal({
				backdrop: 'static',
			    keyboard: false
			});
		} else {
			submitpurchaseRequest();
		}
	}
	
	function validatePR(){
		
		var automatic = $('#automatic').val();
		if(automatic=="true"){
			if($('#filesNumber').val() != null && $('#filesNumber').val() != ''){
				return validatePREntities();
			}
			else{
				alert("your PR is automatic you should upload document before submit");
				return false;
			}
			
		}else{
			return validatePREntities();	
		}
	}
	
	function deletepurchaseRequest(){
		if(window.confirm("Are you sure you want to delete?you will delete the GRs,Positions,PO related to this PR")){
		document.getElementById("editpurchaseRequestForm").action='${deletePurchaseRequestURL}';
		document.getElementById("editpurchaseRequestForm").submit();
		}
	}
	
	function startPurchaseRequest(){
		document.getElementById("editpurchaseRequestForm").action='${startPurchaseRequestURL}';
		document.getElementById("editpurchaseRequestForm").submit();
	}
	
	function rejectPurchaseRequest(){
		var prForm=document.getElementById("editpurchaseRequestForm");
		
		 if((prForm.elements["rejectionCause"].value =="" || prForm.elements["rejectionCause"].value ==null)){
			 prForm.elements["rejectionCause"].style.borderColor="#FF0000";
			 document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-danger">your purchase Request contains missing parameters complete fields in Red color</div>';
			 
			 return false;
			} else{
			 prForm.elements["rejectionCause"].style.borderColor="";
			 document.getElementById("editpurchaseRequestForm").action='${rejectPurchaseRequestURL}';
			 document.getElementById("editpurchaseRequestForm").submit();
			}
		 
		
	}
	
	function approvePositionSel(){
		var resourceURL = '${approvePositionSel}';
		var actionStatus = document.getElementById("actionStatus").value;
		var numberofPositions = document.getElementById("numberOfPositions").value;
		var positionSel = "";
		for(i = 0; i < (numberofPositions); i++){
			var positionStatus = document.getElementById("positionStatusLabel_tr" + i).value;
			if(document.getElementById("positionsSel_" + i).checked){
				if(positionStatus == 'Pending I&C approval') {
					var positionId = document.getElementById("positionsSel_" + i).value;
					positionSel = positionSel + positionId + ';';
				}
			}
		}
		
	    jQuery.ajax({
		    type: "POST",
		    url: resourceURL,
		    data: {"positionSel":positionSel, "actionStatus":actionStatus},
		    success: function(data) { 
			         $('#positionsContent').html(data);
			         
					//resetPositionForm();
		    		}
		    });
	}
	
	function approvePositionAll(){
		if(window.confirm("Are you sure you want to approve all positions?")){
			document.getElementById("editpurchaseRequestForm").action='${approvePositionAllURL}';
			document.getElementById("editpurchaseRequestForm").submit();
		}
	}
	
	function cancelPR(){
		document.location.href ='/group/vodafone/home';
	}
	
	function savePRAddPositionBtn(){
		
		var purchaseRequestId=document.getElementById("purchaseRequestId").value;
		var automatic=document.getElementById("automatic").value;
		var costcenter=document.getElementById("costcenter").value;
		var ola=document.getElementById("ola").value;
		var buyer=document.getElementById("buyer").value;
		var vendor=document.getElementById("vendor").value;
		var projectname=document.getElementById("projectname").value;
		var totalvalue=document.getElementById("totalvalue").value;
		var prCurrency=document.getElementById("prCurrency").value;
		var activitydescription=document.getElementById("activitydescription").value;
		
		//CR for tracking code------------
		var trackingcode=document.getElementById("trackingcode").value;
		var prwbscode=document.getElementById("prwbscode").value;
		
		var resourceURL;
		
		if(purchaseRequestId !== null && purchaseRequestId !=""){
			resourceURL='${updatePurchaseRequest}';
			//alert("updatepurchaseRequest  "+resourceURL);
		}else{
			resourceURL='${addPurchaseRequest}';
			//alert("addpurchaseRequest "+resourceURL);
		}
	
		  jQuery.ajax({
			    type: "POST",
			    dataType: "json",
			    url: resourceURL,
			    data: {"purchaseRequestId":purchaseRequestId,"automatic":automatic,"costcenter":costcenter,"ola":ola,"buyer":buyer,"vendor":vendor,
			    	"projectname":projectname,"totalvalue":totalvalue,"prCurrency":prCurrency,"activitydescription":activitydescription,
			    	"trackingcode":trackingcode,"prwbscode":prwbscode},
			    success: function(data) {
			    	//alert(data);
			    	if(data.status == "success"){
		    			document.getElementById("purchaseRequestId").value=data.purchaseRequestID;
			    		document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-success">OMP Request # '+data.purchaseRequestID+'  Saved Successfully</div>';
			    		
			    		document.getElementById('positionFormContainer').style.display='block';
			    		document.getElementById('editPositionBtn').value='Add';
			    		if(document.getElementById('deletePositionBtn') !=null){
			    			document.getElementById('deletePositionBtn').style.display='none';
			    		 }
			    		//resetPositionForm();
			    		document.getElementById('labelNumber').value=10;
			    		document.getElementById("labelNumberText").innerHTML=10;
			    		var actionStatus=document.getElementById("actionStatus").value;
			    		if(actionStatus != 'Rejected'){
			    			document.getElementById("deleteBtnDiv").innerHTML='<button type="button" class="cancelbtn" onclick="deletepurchaseRequest();" value="delete">delete</button>';
			    		}
			    		
			    	} else if(data.status == "trackingCodeKO"){
			    		 document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-danger">Tracking Code non presente in archivio</div>';
			    		 closemodal();
			    		 $('html,body').scrollTop(0);
			    		 
			    	} else if(data.status == "failure"){
			    		 document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-danger">your purchase Request failed to Save</div>';
			    	}
			    	
			    },
			    error : function(XMLHttpRequest, textStatus, errorThrown) {
			       document.getElementById("purchaseRequestSuccessMSG").innerHTML="<div class='alert alert-danger'>your purchase Request failed to Save</div>";
			    }
		});
		
	}
	
	function checkFileExist(){
		var fileSelected=document.getElementById("fileName").value;
		if(fileSelected=="" ||fileSelected==null){
			alert("please choose file first");
		}
		else{
			var automatic=document.getElementById("automatic").value;
			if(automatic=="true" && $('#filesNumber').val() !=null && $('#filesNumber').val() !=''){
				if(window.confirm("Are you sure you want to replace the file")){
			  	uploadFile();
				}
			}else{
			if($('#purchaseRequestId').val()=='' || $('#purchaseRequestId').val()==null){
				//savePRUploadBtn();
				uploadFile();
			}else{
				uploadFile();
			}
			}
			
		}
	}
	
	function checkFileOffertaExist(){
		var fileSelected=document.getElementById("fileNameOfferta").value;
		if(fileSelected=="" ||fileSelected==null){
			alert("please choose file first");
		}
		else{
			if($('#purchaseRequestId').val()=='' || $('#purchaseRequestId').val()==null){
				savePRUploadBtn();
			}else{
				uploadFileOfferta();
			}
		}
	}
	
	function savePRUploadBtn(){
		var purchaseRequestId=document.getElementById("purchaseRequestId").value;
		var automatic=document.getElementById("automatic").value;
		var costcenter=document.getElementById("costcenter").value;
		var ola=document.getElementById("ola").value;
		var buyer=document.getElementById("buyer").value;
		var vendor=document.getElementById("vendor").value;
		var projectname=document.getElementById("projectname").value;
		var totalvalue=document.getElementById("totalvalue").value;
		var prCurrency=document.getElementById("prCurrency").value;
		var activitydescription=document.getElementById("activitydescription").value;
		
		//CR for tracking code------------
		var trackingcode=document.getElementById("trackingcode").value;
		var prwbscode=document.getElementById("prwbscode").value;
		
		var resourceURL;
		
		if(purchaseRequestId !== null && purchaseRequestId !=""){
			resourceURL='${updatePurchaseRequest}';
			//alert("updatepurchaseRequest  "+resourceURL);
		}else{
			resourceURL='${addPurchaseRequest}';
			//alert("addpurchaseRequest "+resourceURL);
		}
	
		  jQuery.ajax({
			    type: "POST",
			    dataType: "json",
			    url: resourceURL,
			    data: {"purchaseRequestId":purchaseRequestId,"automatic":automatic,"costcenter":costcenter,"ola":ola,"buyer":buyer,"vendor":vendor,
			    	"projectname":projectname,"totalvalue":totalvalue,"prCurrency":prCurrency,"activitydescription":activitydescription,
			    	"trackingcode":trackingcode,"prwbscode":prwbscode},
			    success: function(data) {
			    	//alert(data);
			    	if(data.status == "success"){
		    			document.getElementById("purchaseRequestId").value=data.purchaseRequestID;
			    		document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-success">OMP Request # '+data.purchaseRequestID+'  Saved Successfully</div>';
			    		uploadFileOfferta();
			    		var actionStatus=document.getElementById("actionStatus").value;
			    		if(actionStatus !='Rejected'){
			    			document.getElementById("deleteBtnDiv").innerHTML='<button type="button" class="cancelbtn" onclick="deletepurchaseRequest();" value="delete">delete</button>';
			    		}
				    		
			    	} else if(data.status == "trackingCodeKO"){
			    		 document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-danger">Tracking Code non presente in archivio</div>';
			    		 
			    	} else if(data.status == "failure"){
			    		 document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-danger">your purchase Request failed to Save</div>';
			    	}
			    	
			    },
			    error : function(XMLHttpRequest, textStatus, errorThrown) {
			       document.getElementById("purchaseRequestSuccessMSG").innerHTML="<div class='alert alert-danger'>your purchase Request failed to Save</div>";
			    }
		});
		
	}
	
	
	function addUpdatePosition(action) {
	
		var actionStatus=document.getElementById("actionStatus").value;
		
		var positionId=document.getElementById("positionId").value;
		var labelNumber=document.getElementById("labelNumber").value;
		var positionStatus=document.getElementById("positionStatus").value;
		var purchaseRequestId=document.getElementById("purchaseRequestId").value;
		var fiscalYear=document.getElementById("fiscalyear").value;
		var categoryCode=document.getElementById("categorycode").value;
		var offerNumber=document.getElementById("offernumber").value;
		var quantity=document.getElementById("quantity").value;
		var deliveryDate=document.getElementById("deliverydate").value;
		var deliveryAdress=document.getElementById("deliveryadress").value;
		var assetNumber=document.getElementById("asset-number").value;
		var prcsNumber=document.getElementById("prcs-number").value;
		var poNumber=document.getElementById("po-number").value;
		var actualUnitCost=document.getElementById("actual-unit-cost").value;
		var positionDescription=document.getElementById("position-description").value;
		var iandc = document.getElementById("iandc").value;
		var evoCodMateriale = document.getElementById("evocodmateriale").value;
		var evoDesMateriale = document.getElementById("evodesmateriale").value;

		var targaTecnica = document.getElementById("targaTecnica").value;
		var locationEvo = targaTecnica.split('/')[1];
		targaTecnica = targaTecnica.split('/')[0];
		
		var resourceURL;
		
		if(action =='approve'){
			resourceURL='${approvePosition}';	
		}
		else{
			if(action !='delete'){
				if(document.getElementById("userType").value == "ANTEX"){
					var automatic=document.getElementById("automatic").value;
					//alert("automatic ="+automatic);
					if(automatic=="true"){
						//alert("inside automatic "+automatic);
						var automaticReturn=true;
						if(actionStatus !="Created"){
							if(assetNumber=="" || assetNumber==null){
								 document.getElementById("positionErrorMSG").innerHTML='<div class="alert alert-danger">you should add asset,SC and PO numbers first</div>';
								 document.getElementById("asset-number").style.borderColor="#FF0000";
								 automaticReturn=false;
							}
							if(prcsNumber=="" || prcsNumber==null){
								 document.getElementById("positionErrorMSG").innerHTML='<div class="alert alert-danger">you should add asset,SC and PO numbers first</div>';
								 document.getElementById("prcs-number").style.borderColor="#FF0000";
								 automaticReturn=false;
							}
							if(poNumber=="" || poNumber==null){
								 document.getElementById("positionErrorMSG").innerHTML='<div class="alert alert-danger">you should add asset,SC and PO numbers first</div>';
								 document.getElementById("po-number").style.borderColor="#FF0000";
								 automaticReturn=false;
							}
							//alert("automaticReturn ="+automaticReturn);
							if(! automaticReturn){
								//alert("not automaticReturn ");
								return automaticReturn;
							}
						}
						
					} else {
						//alert("inside manual "+automatic);
						if(positionStatus =="NEW" && actionStatus=="PR Assigned"){
							//alert("enter the asset number");
							var missingAsset=false;
							var missingEvo=false;
							if(assetNumber=="" || assetNumber==null){
							 document.getElementById("asset-number").style.borderColor="#FF0000";
							 missingAsset=true;
							} else {
								 document.getElementById("asset-number").style.borderColor="";
							}
							
							if(missingAsset==true && missingEvo==true){
								 document.getElementById("positionErrorMSG").innerHTML='<div class="alert alert-danger">you should add asset number and EVO location</div>';
								return false;
							} else if(missingAsset==true){
								document.getElementById("positionErrorMSG").innerHTML='<div class="alert alert-danger">you should add asset number</div>';
								return false;
							} else if(missingEvo==true){
								document.getElementById("positionErrorMSG").innerHTML='<div class="alert alert-danger">you should add EVO location</div>';
								return false;
							}
						}
						if(positionStatus =="ASSET_RECEIVED" && (prcsNumber=="" || prcsNumber==null)){
							//alert("enter the prcs number");
							 document.getElementById("positionErrorMSG").innerHTML='<div class="alert alert-danger">you should add PR/CS number first</div>';
							 document.getElementById("prcs-number").style.borderColor="#FF0000";
							return false;
						}
						
						if(positionStatus =="SC_RECEIVED" && (poNumber=="" || poNumber==null)){
							//alert("enter the prcs number");
							 document.getElementById("positionErrorMSG").innerHTML='<div class="alert alert-danger">you should add PO number first</div>';
							 document.getElementById("po-number").style.borderColor="#FF0000";
							return false;
						}
					}
				}
				if(positionId !== null && positionId !=""){
					resourceURL='${updatePosition}';
					//alert("updatePosition  "+resourceURL);
				} else {
					resourceURL='${addPosition}';
					//alert("addPosition "+resourceURL);
				}
			}
			else{
				resourceURL='${deletePosition}';
				//alert("deletePosition "+resourceURL);
				}
			}
	        
		    jQuery.ajax({
		    type: "POST",
		    url: resourceURL,
		    data: {"positionId":positionId,"positionStatus":positionStatus,"actionStatus":actionStatus,"labelNumber":labelNumber,"purchaseRequestId":purchaseRequestId,
		    	"fiscalyear":fiscalYear,"categorycode":categoryCode,"offernumber":offerNumber,"quantity":quantity,
		    	"deliverydate":deliveryDate,"deliveryadress":deliveryAdress,"asset-number":assetNumber,
		    	"prcs-number":prcsNumber,"po-number":poNumber,"actual-unit-cost":actualUnitCost,"position-description":positionDescription,
		    	"iandc":iandc, "evocodmateriale":evoCodMateriale, "evodesmateriale":evoDesMateriale, "targaTecnica":targaTecnica,"locationEvo":locationEvo},
		    success: function(data) {
			    	try {
				    	var myObj = JSON.parse(data);
				    	if(myObj.status == "targaTecnicaKO"){
				   			alert('Targa Tecnica non presente in archivio');
					   	}
			    	} catch(e) {
				        $('#positionsContent').html(data);
						closemodal();
						resetPositionForm();
			    	}
		    	}
	    });
		$('.modal').on('shown.bs.modal', function (e) {
  $('.tltp').tooltip();
});
		
	}
	
	
	function resetPositionForm() {
	    var frm_elements = document.getElementById("positionForm").elements;
	    for (i = 0; i < frm_elements.length; i++)
	    {
	        field_type = frm_elements[i].type.toLowerCase();
	        switch (field_type)
	        {
	        case "text":
	        case "password":
	        case "textarea":
	        case "hidden":
	            frm_elements[i].value = "";
	            break;
	        case "radio":
	        case "checkbox":
	            if (frm_elements[i].checked)
	            {
	                frm_elements[i].checked = false;
	            }
	            break;
	        case "select-one":
	        case "select-multi":
	            frm_elements[i].value ="";
	            break;
	        default:
	            break;
	        }
	        frm_elements[i].style.borderColor="";
	    }
	}
	function setPositionFormInAddMode(){
		var numberofPositions=document.getElementById("numberOfPositions").value;
		//alert("numberofPositions = "+numberofPositions);
		if(numberofPositions == "0")
			{
			//alert("number of positions = 0 will add first")
			savePRAddPositionBtn();
			}
		else{
		document.getElementById('positionFormContainer').style.display='block';
		document.getElementById('editPositionBtn').value='Add';
		 if(document.getElementById('deletePositionBtn') !=null){
			 document.getElementById('deletePositionBtn').style.display='none'; 
		 }
		 
		 resetPositionForm();
		 fillPositionFromLatest(numberofPositions-1);
		}
		 var num=parseInt(numberofPositions)+1;
			var labelNumber=num*10;
			document.getElementById("labelNumberText").innerHTML=labelNumber;
			document.getElementById('labelNumber').value=labelNumber;
			//$(".modal").appendTo($("body"));
			/////////////////////////////////////////////////////
			//var purchaseRequestId=$('#purchaseRequestId').val();
			//jQuery.post('${getLabelNumberURL}',{purchaseRequestId:purchaseRequestId},
				//function(data) {
			 	//	$('#labelNumberText').html(data);
			 	//	$('#labelNumber').val(data);
				// }
			//);
			
			//////////////////////////////////////////////////////
			
			$('.modal').on('shown.bs.modal', function (e) {
  $('.tltp').tooltip();
});
		
	}
	
	
	function hidePositionForm(){
		resetPositionForm();
		document.getElementById('positionFormContainer').style.display='none';
		
	}
	
	function fillPositionForm(i){
		document.getElementById('positionFormContainer').style.display='block';
		
		if(document.getElementById('editPositionBtn') != null){
			 document.getElementById('editPositionBtn').value = 'update';
		}
		
		document.getElementById("positionErrorMSG").innerHTML = '';
		
		document.getElementById("prcs-number").style.borderColor = "";
		if(document.getElementById('deletePositionBtn') != null){
			document.getElementById('deletePositionBtn').style.display='inline-block';
		}
		 
		var actionStatus = document.getElementById("actionStatus").value;
		var userType = document.getElementById("userType").value;
		var positionStatus = document.getElementById("positionStatus_tr"+i).value;
		
		document.getElementById("positionId").value = document.getElementById("positionId_tr"+i).value;
		document.getElementById("positionStatus").value = document.getElementById("positionStatus_tr"+i).value;
		document.getElementById("labelNumber").value = document.getElementById("labelNumber_tr"+i).value;
		document.getElementById("labelNumberText").innerHTML = document.getElementById("labelNumber_tr"+i).value;
		document.getElementById("fiscalyear").value = document.getElementById("fiscalyear_tr"+i).value;
		document.getElementById("categorycode").value = document.getElementById("categorycode_tr"+i).value;
		document.getElementById("offernumber").value = document.getElementById("offernumber_tr"+i).value;
		document.getElementById("quantity").value = document.getElementById("quantity_tr"+i).value;
		document.getElementById("deliverydate").value = document.getElementById("deliverydate_tr"+i).value;
		document.getElementById("deliveryadress").value = document.getElementById("deliveryadress_tr"+i).value;
		document.getElementById("asset-number").value = document.getElementById("asset-number_tr"+i).value;
		document.getElementById("prcs-number").value = document.getElementById("prcs-number_tr"+i).value;
		document.getElementById("po-number").value = document.getElementById("po-number_tr"+i).value;
		document.getElementById("actual-unit-cost").value = document.getElementById("actual-unit-cost_tr"+i).value;
		document.getElementById("position-description").value = document.getElementById("position-description_tr"+i).value;
		document.getElementById("iandc").value = document.getElementById("iandc_tr"+i).value;
		document.getElementById("positionStatusLabel").value = document.getElementById("positionStatusLabel_tr"+i).value;
		document.getElementById("approverName").value = document.getElementById("approverName_tr"+i).value;
		document.getElementById("evocodmateriale").value = document.getElementById("evoCodMateriale_tr"+i).value;
		document.getElementById("evodesmateriale").value = document.getElementById("evoDesMateriale_tr"+i).value;
		document.getElementById("targaTecnica").value = document.getElementById("targaTecnica_tr"+i).value + " / " + document.getElementById("locationEvo_tr"+i).value;
		
		if(document.getElementById("approveBtn_tr"+i).value =='true' &&(userType =="CONTROLLER" ||userType=='NDSO' ||userType=='TC')){
		    document.getElementById('approvePositionBtn').style.display='inline-block';
		}else{
			document.getElementById('approvePositionBtn').style.display='none';
		}

		/////////////////////////////////////////// highligh the fields ////////////////////////////////////////////////////
		if(validatePos=='true'){
		var prForm=document.getElementById("positionForm");
		//alert("number of params "+prForm.elements.length);
		var ignoreElementsArr = ['asset-number', 'prcs-number', 'po-number', 'iandc', 'positionStatusLabel', 'approverName', 'fiscalyear', 'offernumber', 'targaTecnica', 'locationEvo'];
			for(i=0; i<prForm.elements.length; i++){
				     field_type = prForm.elements[i].type.toLowerCase();
				
		        switch (field_type)
		        {
		        case "text":
		        case "password":
		        case "textarea":
		        case "hidden":
		   		 if((prForm.elements[i].value =="" || prForm.elements[i].value ==null) && $.inArray(prForm.elements[i].id,ignoreElementsArr) == -1){
					 prForm.elements[i].style.borderColor="#FF0000";
					} else{
					 	prForm.elements[i].style.borderColor="";
						 }
		        case "select":
		        case "select-one":
		        case "select-multi":
			   		 if((prForm.elements[i].value =="" || prForm.elements[i].value ==null)&& $.inArray(prForm.elements[i].id,ignoreElementsArr) == -1){
						 prForm.elements[i].style.borderColor="#FF0000";
						} else{
						 	prForm.elements[i].style.borderColor="";
							 }
		        }
			}
		 }
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	}
	
	function fillPositionFromLatest(i){
	
		document.getElementById("fiscalyear").value=document.getElementById("fiscalyear_tr"+i).value;
		document.getElementById("categorycode").value=document.getElementById("categorycode_tr"+i).value;
		document.getElementById("offernumber").value=document.getElementById("offernumber_tr"+i).value;
		document.getElementById("deliverydate").value=document.getElementById("deliverydate_tr"+i).value;
		document.getElementById("deliveryadress").value=document.getElementById("deliveryadress_tr"+i).value;
		document.getElementById("asset-number").value=document.getElementById("asset-number_tr"+i).value;
		document.getElementById("prcs-number").value=document.getElementById("prcs-number_tr"+i).value;
		document.getElementById("po-number").value=document.getElementById("po-number_tr"+i).value;
		document.getElementById("position-description").value=document.getElementById("position-description_tr"+i).value;
		document.getElementById('positionStatusLabel').value='New';
		document.getElementById('approverName').value='N/A';
		document.getElementById("evocodmateriale").value = document.getElementById("evoCodMateriale_tr"+i).value;
		document.getElementById("evodesmateriale").value = document.getElementById("evoDesMateriale_tr"+i).value;
		document.getElementById("targaTecnica").value =	document.getElementById("targaTecnica_tr"+i).value + " / " + document.getElementById("locationEvo_tr"+i).value;;
	}
	
	function resetRequiredFields(){
		var prForm=document.getElementById("editpurchaseRequestForm");
			for(i=0; i<prForm.elements.length; i++){
		     	   	prForm.elements[i].style.borderColor="";
		        	}

	}
	
	function validateRequiredFields(){
		//alert("start validation");
		var result = true;
		var prForm = document.getElementById("editpurchaseRequestForm");
		//alert("number of params "+prForm.elements.length);
		var prIgnoreElementsArr = ['ola', 'rejectionCause'];
		for(i = 0; i < prForm.elements.length; i++){
			field_type = prForm.elements[i].type.toLowerCase();
			var placehoder = prForm.elements[i].getAttribute("placeholder");
			//alert(prForm.elements[i].id);
/* 			if(placehoder == "Select Tracking Code .." ||prForm.elements[i].id=="trackingcode" || prForm.elements[i].name=="trackingcode"){
				continue;
			}  */
	        switch (field_type) {
		        case "text":
		        case "password":
		        case "textarea":
		        case "hidden":
		   		 if((prForm.elements[i].value =="" || prForm.elements[i].value ==null || prForm.elements[i].value == placehoder) && $.inArray(prForm.elements[i].id, prIgnoreElementsArr) == -1){
					 prForm.elements[i].style.borderColor = "#FF0000";
					 result = false;
					} else{
					 	prForm.elements[i].style.borderColor = "";
						 }
		        case "select":
		        case "select-one":
		        case "select-multi":
			   		if((prForm.elements[i].value == "" || prForm.elements[i].value == null ||  prForm.elements[i].value == placehoder)&& $.inArray(prForm.elements[i].id, prIgnoreElementsArr) == -1){
						prForm.elements[i].style.borderColor = "#FF0000";
						result = false;
					} else {
						prForm.elements[i].style.borderColor = "";
					}
	        }
		}
		
		
 		var positionsTableForm = document.getElementById("positionsTableForm");
		var ignoreElementsArr = ['asset-number', 'prcs-number', 'po-number', 'iandc', 'positionStatusLabel', 'approverName',  'fiscalyear', 'offernumber', 'targaTecnica', 'locationEvo'];
		for(i=0; i<positionsTableForm.elements.length; i++){
			field_type = positionsTableForm.elements[i].type.toLowerCase();
			switch (field_type) {
				case "text":
				case "password":
				case "textarea":
				case "hidden":
					var elementID = positionsTableForm.elements[i].id.split('_')[0];
					var trNumber = positionsTableForm.elements[i].id.split('_')[1];
					//alert("elementID 1 = " + elementID);
					if(positionsTableForm.elements[i].value == "" && $.inArray(elementID,ignoreElementsArr) == -1){
						//alert("elementID 2 = " + elementID);
						//alert("trNumber = " + trNumber);
						//alert("will chane the colcor for  " + document.getElementById("positionErrors_" + trNumber).id);
						document.getElementById("positionErrors_"+trNumber).innerHTML='<div class="alert alert-danger">you should fill the required fields</div>';
						validatePos = 'true';
						result = false;
					}
			}
		}
		return result;
	}
	 
	function uploadFile(){
		$("#uploading").fadeIn( "normal" );
		
		 $.ajaxFileUpload
		 ({
			 url:'${uploadFileURL}'+"&purchaseRequestId="+$('#purchaseRequestId').val()+"&automatic="+$('#automatic').val(),
			 secureuri : false,
			 fileElementId : 'fileName',
			 dataType : 'json',
			 data : {name : 'name', id : 'id'},
			 success : function(data, status) {
				 document.getElementById("purchaseRequestId").value = data.purchaseRequestID;
				 
				 $("#vendor option").remove();
				 $("#vendor").append("<option value='"+data.vendorId+"'>"+data.vendorName+"</option>");
				 $("#vendor").data('combobox').refresh();
				 
				 $("#prCurrency option").remove();
				 $("#prCurrency").append("<option value='"+data.currencyCode+"'>"+data.currencyDesc+"</option>");
				 $("#prCurrency").data('combobox').refresh();
				 
				 document.getElementById("totalvalue").value=data.prTotalValue;
				 document.getElementById("numberOfPositions").value=data.numberOfPositions;
				 document.getElementById('addPositionNotNss').style.display='none';
				 document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-success">OMP Request # '+data.purchaseRequestID+'  Saved Successfully</div>';
				 
				 $('#hasUploadedFile').val('true');	
				 listUploadedFiles();
				 $("#fileName").val(null);
				 $("#uploading").fadeOut( "fast" );
				 
				 refreshPosition();
				 
				 $("#deliverydate").prop('disabled', 'disabled');
				 $("#deliveryadress").prop('disabled', 'disabled');
				 $("#evocodmateriale").prop('disabled', 'disabled');
				 $("#evodesmateriale").prop('disabled', 'disabled');
				 $("#quantity").prop('disabled', 'disabled');
				 $("#actual-unit-cost").prop('disabled', 'disabled');
				 $("#position-description").prop('disabled', 'disabled');
				 
				 if (typeof (data.error) != 'undefined') {
					 if (data.error != '') {
						alert("done success <1> with error! data.error="+data.error);
					 	alert(data.error);
					 } else {
						alert("done success <2> with error! data.error="+data.error);
					 	alert(data.msg);
					 }
				 }
			 },
			 error : function(data, status, e) {
				 alert("done error! status="+status);
				 alert("done error! data="+data);
				 alert("done error! e="+e);
				 alert(e);
			 }
		 });
	 }
	function viewUploadedfile(fileName){
		document.location.href ='${downloadFileURL}'+"&purchaseRequestId="+$('#purchaseRequestId').val()+"&fileName="+encodeURIComponent(fileName.replace(/\u00a0/g, ' '));
	}
	
	function viewAllUploadedfiles(){
		document.location.href ='${downloadAllFilesURL}'+"&purchaseRequestId="+$('#purchaseRequestId').val();
	}
	
	function listUploadedFiles(){
		var purchaseRequestId=$('#purchaseRequestId').val();
		jQuery.post('${listUploadedFilesURL}',{purchaseRequestId:purchaseRequestId},
				 function(data) {
			 		$('#uploadedFile').html(data);
					$('.deletered').tooltip();
					$("#uploadedFile").fadeIn( "normal" );
				 }
			);
	}
	
	function uploadFileOfferta(){
		$("#uploadingOfferta").fadeIn( "normal" );
		
	 $.ajaxFileUpload
	 (
	 {
	 url:'${uploadFileOffertaURL}'+"&purchaseRequestId="+$('#purchaseRequestId').val()+"&automatic="+$('#automatic').val(),
	 secureuri : false,
	 fileElementId : 'fileNameOfferta',
	 dataType : 'json',
	 data : {
		 name : 'name',
		 id : 'id'
		 },
	 success : function(data, status) {
			 $('#hasUploadedFile').val('true');	
			 //$('#uploadedFile').html('<a href="javascript:viewUploadedfile()">view Uploaded File</a>');
			 listUploadedFiles();
			 $("#fileNameOfferta").val(null);
			 $("#uploadingOfferta").fadeOut( "fast" );
			 
			 //alert("done success! status="+status);
			 //alert("done success! data="+data);
	 if (typeof (data.error) != 'undefined') {
	 if (data.error != '') {
		 alert("done success <1> with error! data.error="+data.error);
	 alert(data.error);
	 } else {
		 alert("done success <2> with error! data.error="+data.error);
	 alert(data.msg);
	 }
	 }
	 },
	 error : function(data, status, e) {
		 alert("done error! status="+status);
		 alert("done error! data="+data);
		 alert("done error! e="+e);
	 alert(e);
	 }
	 });
	}
	
	function refreshPosition(){
		var purchaseRequestId=$('#purchaseRequestId').val();
		jQuery.post('${refreshPositionURL}',{purchaseRequestId:purchaseRequestId},
				 function(data) {
	         		$('#positionsContent').html(data);
					closemodal();
					resetPositionForm();
				 }
			);
	}
	
	function deleteFile(fileName){
		if(window.confirm("Are you sure you want to delete the file")){
		var purchaseRequestId=$('#purchaseRequestId').val();
		jQuery.post('${deleteFileURL}',{purchaseRequestId:purchaseRequestId,fileName:fileName.replace(/\u00a0/g, ' ')},
				 function(data) {
			 		$('#uploadedFile').html(data);
				 }
			);
		}
	}
	
	function poulateTrackingValues(){
		
		var selectedTrackingCode = $('#trackingcode').val();
		if(selectedTrackingCode == ""){
			$(".trackingrelated").val("");
			$(".trackingsubrelated").val("");
			
			$(".trackingrelated").prop('disabled',false);
			$(".trackingsubrelated").prop('disabled',true);
			
			$("#prwbscode").data('combobox').refresh();
			$("#projectname").data('combobox').refresh();
		}else{
			selectedTrackingCode=selectedTrackingCode.replace(/%/g,"%25");
			//alert("not empty tracking code");
			$.getJSON('${poulateTrackingValuesUrl}',"selectedTrackingCode="+selectedTrackingCode,function(data) {
				//var trackingCodesValues = data.trackingCodesValues;
				if(data.empty != "true") {
					$("#prwbscode").val(data.wbsCodeId);
					$("#projectname").val(data.projectId);
					
					$(".trackingrelated").prop('disabled',true);
					$('.trackingsubrelated').prop('disabled',false);
					
					$("#prwbscode").data('combobox').refresh();
					$("#projectname").data('combobox').refresh();
				}
	    	});
		}
	}
	
	
	function checkIsNumber(field,fieldName){
		if(field.value !="" && ! $.isNumeric(field.value)){
			alert("the "+fieldName+" should be number");
			field.value="";
		}
	}
	
	function handleClick(cb) {
		if(cb.checked){
			$("#iccCloseBtn").prop('disabled', false);
		}
		else{
			$("#iccCloseBtn").prop('disabled', true);
		}
	}
	
	function enableApproveBtn(){
		$("#approvePositionSelBtn").prop('disabled', true);
		var numberofPositions = document.getElementById("numberOfPositions").value;
		var positionSel = "";
		for(i = 0; i < (numberofPositions); i++){
			if(document.getElementById("positionsSel_" + i).checked){
				var positionStatus = document.getElementById("positionStatusLabel_tr" + i).value;
				if(positionStatus == 'Pending I&C approval') {
					$("#approvePositionSelBtn").prop('disabled', false);
				}
			}
		}
	}
	
	$(document).ready(function(){
		$(function() {
		    var availableTags = [
				<c:forEach items="${allTrackingCodes}" var="trackingcode" varStatus="loopStatus">
					<c:if test="${trackingcode.display eq true || trackingcode.trackingCodeName  eq fn:trim(fn:split(pr.trackingCode, '||')[0])}">
					"<hp:escapeQuotes value='${trackingcode.trackingCodeName}'/> || <hp:escapeQuotes value='${trackingcode.trackingCodeDescription}'/> "
					
					<c:if test="${!loopStatus.last}"> , </c:if>
					  
					</c:if>
				</c:forEach>
		    ];
		    
		    $( "#trackingcode" ).autocomplete({
		      source: availableTags,
		      change: function (event, ui) { poulateTrackingValues(); }
		    });
		    
			var availableTags = [
			   	<c:forEach items="${allLocations}" var="locationEvo" varStatus="loopStatus">
			   			"${locationEvo.targaTecnicaSap} / ${locationEvo.location}"
			   		
			   			<c:if test="${!loopStatus.last}"> , </c:if>
				</c:forEach>
			];
	       		  
			autocomplete(document.getElementById("targaTecnica"), availableTags);
	  });
	
	var purchaseRequestId = document.getElementById("purchaseRequestId").value;
	if (purchaseRequestId == null || purchaseRequestId == "") {
		$("#popupNotNSS").modal();
	}
	
	$('#viewModalIcc').on('hidden.bs.modal', function () {
		submitpurchaseRequest();
	})
	
  });

	function autocomplete(inp, arr) {
		  /*the autocomplete function takes two arguments,
		  the text field element and an array of possible autocompleted values:*/
		  var currentFocus;
		  /*execute a function when someone writes in the text field:*/
		  inp.addEventListener("input", function(e) {
		      var a, b, i, val = this.value;
		      /*close any already open lists of autocompleted values*/
		      closeAllLists();
		      if (!val) { return false;}
		      currentFocus = -1;
		      /*create a DIV element that will contain the items (values):*/
		      a = document.createElement("DIV");
		      a.setAttribute("id", this.id + "autocomplete-list");
		      a.setAttribute("class", "autocomplete-items");
		      /*append the DIV element as a child of the autocomplete container:*/
		      this.parentNode.appendChild(a);
		      /*for each item in the array...*/
		      for (i = 0; i < arr.length; i++) {
		        /*check if the item starts with the same letters as the text field value:*/
		        //if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
		        if (arr[i].toUpperCase().includes(val.toUpperCase())) {
		          /*create a DIV element for each matching element:*/
		          b = document.createElement("DIV");
		          /*make the matching letters bold:*/
		          b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
		          b.innerHTML += arr[i].substr(val.length);
		          /*insert a input field that will hold the current array item's value:*/
		          b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
		          /*execute a function when someone clicks on the item value (DIV element):*/
		          b.addEventListener("click", function(e) {
		              /*insert the value for the autocomplete text field:*/
		              inp.value = this.getElementsByTagName("input")[0].value;
		              /*close the list of autocompleted values,
		              (or any other open lists of autocompleted values:*/
		              closeAllLists();
		          });
		          a.appendChild(b);
		        }
		      }
		  });
		  /*execute a function presses a key on the keyboard:*/
		  inp.addEventListener("keydown", function(e) {
		      var x = document.getElementById(this.id + "autocomplete-list");
		      if (x) x = x.getElementsByTagName("div");
		      if (e.keyCode == 40) {
		        /*If the arrow DOWN key is pressed,
		        increase the currentFocus variable:*/
		        currentFocus++;
		        /*and and make the current item more visible:*/
		        addActive(x);
		      } else if (e.keyCode == 38) { //up
		        /*If the arrow UP key is pressed,
		        decrease the currentFocus variable:*/
		        currentFocus--;
		        /*and and make the current item more visible:*/
		        addActive(x);
		      } else if (e.keyCode == 13) {
		        /*If the ENTER key is pressed, prevent the form from being submitted,*/
		        e.preventDefault();
		        if (currentFocus > -1) {
		          /*and simulate a click on the "active" item:*/
		          if (x) x[currentFocus].click();
		        }
		      }
		  });
		  function addActive(x) {
		    /*a function to classify an item as "active":*/
		    if (!x) return false;
		    /*start by removing the "active" class on all items:*/
		    removeActive(x);
		    if (currentFocus >= x.length) currentFocus = 0;
		    if (currentFocus < 0) currentFocus = (x.length - 1);
		    /*add class "autocomplete-active":*/
		    x[currentFocus].classList.add("autocomplete-active");
		  }
		  function removeActive(x) {
		    /*a function to remove the "active" class from all autocomplete items:*/
		    for (var i = 0; i < x.length; i++) {
		      x[i].classList.remove("autocomplete-active");
		    }
		  }
		  function closeAllLists(elmnt) {
		    /*close all autocomplete lists in the document,
		    except the one passed as an argument:*/
		    var x = document.getElementsByClassName("autocomplete-items");
		    for (var i = 0; i < x.length; i++) {
		      if (elmnt != x[i] && elmnt != inp) {
		        x[i].parentNode.removeChild(x[i]);
		      }
		    }
		  }
		  /*execute a function when someone clicks in the document:*/
		  document.addEventListener("click", function (e) {
		      closeAllLists(e.target);
		  });
		}
	</script>
	
<style>

/*the container must be positioned relative:*/
.autocomplete {
position: relative;
display: inline-block;
}


.autocomplete-items {
position: absolute;
border: 1px solid #d4d4d4;
border-bottom: none;
border-top: none;
z-index: 99;
/*position the autocomplete items to be the same width as the container:*/
top: 100%;
left: 0;
right: 0;
}

.autocomplete-items div {
padding: 10px;
cursor: pointer;
background-color: #fff; 
border-bottom: 1px solid #d4d4d4; 
}

/*when hovering an item:*/
.autocomplete-items div:hover {
background-color: #e9e9e9; 
}

/*when navigating through the items using the arrow keys:*/
.autocomplete-active {
background-color: DodgerBlue !important; 
color: #ffffff; 
}
</style>