<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<portlet:resourceURL var="nextURL">
<portlet:param name="pageNumber" value="${ pageNumber + 1 }"></portlet:param>
</portlet:resourceURL>
<portlet:resourceURL var="curURL">
<portlet:param name="pageNumber" value="${pageNumber}"></portlet:param>
</portlet:resourceURL>
<portlet:resourceURL var="previousURL">
<portlet:param name="pageNumber" value="${pageNumber - 1 }"></portlet:param>
</portlet:resourceURL>

<div class="tablePaging">
<c:if test="${pageNumber > 1 }">

<a class="previousArrow" href="javascript:void(null)" onclick="<portlet:namespace/>ompListing('${previousURL}')"> previous </a>
</c:if>

<c:set var="previousPage" value="${pageNumber - 5}" scope="page"/>
<c:forEach var="index" begin="${0}" end="${4}">
<c:if test="${previousPage > 0}">
<portlet:resourceURL var="previousURL"> 
	<portlet:param name="pageNumber" value="${previousPage}"></portlet:param>
</portlet:resourceURL>
<a  href="javascript:void(null)" onclick="<portlet:namespace/>ompListing('${previousURL}')">${previousPage}</a>
</c:if>		
<c:set var="previousPage" value="${previousPage+1}" scope="page"/>
</c:forEach>
<c:if test="${numberOfPages > 1}">

<span class="currentPage"> ${pageNumber} </span>

</c:if>
 <c:set var="nextPage"  value="${pageNumber}" />
<c:forEach var="index" begin="${0}" end="${4}">
<c:set var="nextPage" value="${nextPage +  1}" />
<c:if test="${nextPage <= numberOfPages}">
<portlet:resourceURL var="nextURLNo">
<portlet:param name="pageNumber" value="${nextPage}"></portlet:param>
</portlet:resourceURL>
<a href="javascript:void(null)" onclick="<portlet:namespace/>ompListing('${nextURLNo}')">${nextPage}</a>
</c:if>							
</c:forEach>
<c:if test="${pageNumber < numberOfPages}">
<a class="nextArrow" href="javascript:void(null)" onclick="<portlet:namespace/>ompListing('${nextURL}')"> next </a>
</c:if>
</div>


