<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<portlet:defineObjects />
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL var="writeExcelFileUrl" id="writeExcelFile" escapeXml="false" >
	<portlet:param name="action" value="writeExcelFile"/>
</portlet:resourceURL>

<portlet:actionURL name="clearExcelFile" var="clearExcelFile" />

<c:if test="${numberOfPositions > 0 }">
	<form id="positionsTableForm" action="" method="post">
	
		<display:table decorator="com.hp.omp.decorator.LayoutNotNssCatalogoListDecorator" name="layoutNotNssCatalogoList" list="layoutNotNssCatalogoList" id="layoutNotNssCatalogo">
		 	<display:column title="Material ID">
				<a href="/group/vodafone/catalogo-new-not-nss?GET_ADD_PO=getRowExcel&layoutNotNssCatalogoId=${layoutNotNssCatalogo.layoutNotNssCatalogoId}">${layoutNotNssCatalogo.materialId}</a>
			</display:column>
		 	<display:column property="materialDesc" title="Material Desc" />
		 	<display:column property="quantity" title="PO Quantity" />
		 	<display:column property="netPrice" title="Net Price" />
		 	<display:column property="currency" title="Currency" />
		 	<display:column property="deliveryDate" title="Delivery Date" />
		 	<display:column property="deliveryAddress" title="Delivery Address" />
		 	<display:column property="itemText" title="Item Text" />
		 	<display:column property="locationEvo" title="Location EVO" />
		 	<display:column property="targaTecnica" title="Targa Tecnica" />
		 	<display:column property="vendorCode" title="Vendor" />
		</display:table>
	
		<div class="formactionbuttons">
       		<button type="button" class="exportStyle" onclick="<portlet:namespace/>writeExcelFile();" value="Write">Write Excel File</button>
       		<button type="button" class="submitbtn"   onclick="delExcelFile();" value="Submit">Delete Excel File</button>
		</div>
	</form>
</c:if>

<script type="text/javascript">
	$("#numberOfPositions").val("${numberOfPositions}");
	
	function <portlet:namespace/>writeExcelFile() {
		var actionURL;
		actionURL='${writeExcelFileUrl}';
		document.getElementById("positionsTableForm").action=actionURL;
		document.getElementById("positionsTableForm").submit();
	}
	
	function delExcelFile(){
		if (confirm("Are you sure you want to delete the excel file?")) {
			var actionURL;
			actionURL='${clearExcelFile}';
			document.getElementById("positionsTableForm").action=actionURL;
			document.getElementById("positionsTableForm").submit();
		} else {
			return false;
		}
	}
	
</script>
