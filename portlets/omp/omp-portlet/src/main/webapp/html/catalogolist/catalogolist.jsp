<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<portlet:resourceURL var="writeExcelCatalogoUrl" id="writeExcelCatalogo" escapeXml="false" >
	<portlet:param name="action" value="writeExcelCatalogo"/>
</portlet:resourceURL>

<form name="prListCatalogo" id="prListCatalogo" method="post">
	<input type="hidden" name="searchKeyWord" 	id="searchKeyWord"	value="${searchKeyWord}" />
	<input type="hidden" name="vendor" 			id="vendor" 		value="${vendor}" />
	<input type="hidden" name="matCatLvl4" 		id="matCatLvl4" 	value="${matCatLvl4}" />
	<input type="hidden" name="codMateriale" 	id="codMateriale"	value="${codMateriale}" />
	<input type="hidden" name="codFornitore" 	id="codFornitore"	value="${codFornitore}" />
	<input type="hidden" name="descMateriale" 	id="descMateriale"	value="${descMateriale}" />
	<input type="hidden" name="prezzo"		 	id="prezzo"			value="${prezzo}" />

	<display:table decorator="com.hp.omp.decorator.CatalogoListDecorator" name="catalogoList" list="catalogoList" id="catalogo">
	 	
		<display:column title="Vendor">
			<a href="/group/vodafone/catalogo-new-not-nss?GET_ADD_PO=addPoForm&catalogoId=${catalogo.catalogoId}">${catalogo.fornitoreDesc}</a>
		</display:column>
	 	
	 	<display:column property="matCatLvl4" title="Mat Cat Lvl4" />
	 	<display:column property="matCod" title="Codice Materiale" />
	 	<display:column property="matCodFornFinale" title="Codice Fornitore" />
	 	<display:column property="matDesc" title="Descrizione Materiale" />
	 	<display:column property="formattedPrezzo" title="Prezzo" />
	 	<display:column property="valuta" title="Valuta" />
	 	<display:column property="dataFineValidita" title="Data Fine Validita" />
	 </display:table>
</form>

<%@include file="/html/tablepaging/paging.jsp"%>

<div class="formactionbuttons">
	<c:if test="${searchKeyWord == '' && vendor == '' && matCatLvl4 == '' && codMateriale == '' && codFornitore == '' && descMateriale == '' && prezzo == ''}">
   		<button type="button" class="exportStyle" value="Write" disabled>Export Catalog</button>
   	</c:if>
	<c:if test="${searchKeyWord != '' || vendor != '' || matCatLvl4 != '' || codMateriale != '' || codFornitore != '' || descMateriale != '' || prezzo != ''}">
   		<button type="button" class="exportStyle" onclick="<portlet:namespace/>exportCatalog();" value="Write">Export Catalog</button>
   	</c:if>
</div>
	
<div id="positionsContent">
	<%@include file="positionnotnsslist.jsp"%>
</div>

<script type="text/javascript">
	
	function <portlet:namespace/>exportCatalog() {
		var actionURL;
		actionURL='${writeExcelCatalogoUrl}';
		document.getElementById("prListCatalogo").action=actionURL;
		document.getElementById("prListCatalogo").submit();
	}

</script>
