<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://hp.com/vfItaly/jsp/taglib" prefix="hp" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>


<liferay-theme:defineObjects />
<portlet:defineObjects />		

<div class="clear"></div>
<portlet:resourceURL var="searchURL"/>

<ul class="nav nav-tabs" id="myTab">
	<div class="filter">Filtered by</div>
	<%@include file="vendorsearch.jsp"%>
	<div class="clear"></div>
</ul>
<div id="<portlet:namespace/>prListDiv" class="listingcontainer"></div>
<div class="loading" style='display:none;font-size:30px; color:#eee;margin-top:60px; text-align: center;'>Loading...</div>
<script type="text/javascript">
var <portlet:namespace/>status = "${createStatus.code}";
var <portlet:namespace/>idOrder = "desc"; 
var <portlet:namespace/>requesterOrder = "desc"; 
var <portlet:namespace/>recieverOrder = "desc";
var <portlet:namespace/>vendorOrder = "desc"; 
var <portlet:namespace/>subCategoryOrder = "desc"; 
var <portlet:namespace/>totalValueOrder = "desc"; 
var <portlet:namespace/>activityDescriptionOrder = "desc";
var <portlet:namespace/>automaticOrder = "desc";
var <portlet:namespace/>fiscalYearOrder = "desc";
var <portlet:namespace/>orderby = "purchaseRequestId"; 
var <portlet:namespace/>order = "desc";
var <portlet:namespace/>isMini = "true";


<portlet:namespace/>ompListing('${searchURL}');

function <portlet:namespace/>ompListing(url){
	var vendor 			= jQuery("#vendor").val();
	var matCatLvl4 		= jQuery("#matCatLvl4").val();
	var codMateriale 	= jQuery("#codMateriale").val();
	var codFornitore 	= jQuery("#codFornitore").val();
	var descMateriale 	= jQuery("#descMateriale").val();
	var prezzo		 	= jQuery("#prezzo").val();
	var searchKeyWord 	= jQuery("#<portlet:namespace/>searchKeyWord").val();
	
	jQuery("#<portlet:namespace/>prListDiv").fadeOut( "fast" );
	jQuery(".loading").fadeIn( "normal" );
	jQuery.post(url,{isMini:<portlet:namespace/>isMini, vendor:vendor, matCatLvl4:matCatLvl4, codMateriale:codMateriale, codFornitore:codFornitore, descMateriale:descMateriale, prezzo:prezzo, searchKeyWord:searchKeyWord},
			 function(data) {
			   jQuery("#<portlet:namespace/>prListDiv").html(data);
			   jQuery(".loading").fadeOut( "fast" );
			   jQuery("#<portlet:namespace/>prListDiv").fadeIn( "fast" );
			 }
		);
	}
	
function <portlet:namespace/>prSearch(url){
	
	var searchPrValue = jQuery("#<portlet:namespace/>searchPr").val();
	jQuery("#<portlet:namespace/>prListDiv").fadeOut( "fast" );
	jQuery(".loading").fadeIn( "normal" );
	jQuery.post(url,{searchPrValue:searchPrValue},
			 function(data) {
			   jQuery("#<portlet:namespace/>prListDiv").html(data);
			   jQuery(".loading").fadeOut( "fast" );
			   jQuery("#<portlet:namespace/>prListDiv").fadeIn( "fast" );
			 }
		);
	}
	
function <portlet:namespace/>search(url,isMini){
	if(isMini == "false") {
		jQuery("#<portlet:namespace/>searchKeyWord").val("");
		
		jQuery("#matCatLvl4").val(jQuery("#<portlet:namespace/>matCatLvl4").val());
		jQuery("#vendor").val(jQuery("#<portlet:namespace/>vendor").val());
		jQuery("#codMateriale").val(jQuery("#<portlet:namespace/>codMateriale").val());
		jQuery("#codFornitore").val(jQuery("#<portlet:namespace/>codFornitore").val());
		jQuery("#descMateriale").val(jQuery("#<portlet:namespace/>descMateriale").val());
		jQuery("#prezzo").val(jQuery("#<portlet:namespace/>prezzo").val());
	}
	
	<portlet:namespace/>isMini = isMini;
	<portlet:namespace/>ompListing(url);
}


function <portlet:namespace/>statusSelected(url,status){
	<portlet:namespace/>status = status;
	<portlet:namespace/>ompListing(url);
}
function orderRequesterPRList(orderby){
	<portlet:namespace/>orderby = orderby.id;
	if(orderby.id == "purchaseRequestId"){
		if (<portlet:namespace/>idOrder == "desc") {
			<portlet:namespace/>order="asc"; <portlet:namespace/>idOrder = "asc"; 
		} else {
			<portlet:namespace/>order="desc";<portlet:namespace/>idOrder = "desc";
		}
	} else if(orderby.id == "screenNameRequester"){
		if (<portlet:namespace/>requesterOrder == "desc") {
			<portlet:namespace/>order="asc"; <portlet:namespace/>requesterOrder = "asc";
		} else {
			<portlet:namespace/>order="desc"; <portlet:namespace/>requesterOrder = "desc";
		}
	} else if(orderby.id == "screenNameReciever"){
		if (<portlet:namespace/>recieverOrder == "desc") {
			<portlet:namespace/>order="asc"; <portlet:namespace/>recieverOrder = "asc";
		} else {
			<portlet:namespace/>order="desc"; <portlet:namespace/>recieverOrder = "desc";
		}
	} else if(orderby.id == "vendorName") {
		if (<portlet:namespace/>vendorOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>vendorOrder = "asc";
		} else {
			<portlet:namespace/>order="desc"; <portlet:namespace/>vendorOrder = "desc";	
		}
	}else if(orderby.id == "totalValue") { 
		if (<portlet:namespace/>totalValueOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>totalValueOrder = "asc";
		} else {
			<portlet:namespace/>order="desc";<portlet:namespace/>totalValueOrder = "desc";
		}
	}else if(orderby.id == "activityDescription") {
		if (<portlet:namespace/>activityDescriptionOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>activityDescriptionOrder = "asc";
		} else {
			<portlet:namespace/>order="desc";<portlet:namespace/>activityDescriptionOrder = "desc";
		}
	}else if(orderby.id == "automatic") { 
		if (<portlet:namespace/>automaticOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>automaticOrder = "asc";
		} else {
			<portlet:namespace/>order="desc";<portlet:namespace/>automaticOrder = "desc";
		}
	}else if(orderby.id == "fiscalYear") { 
		if (<portlet:namespace/>fiscalYearOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>fiscalYearOrder = "asc";
		} else {
			<portlet:namespace/>order="desc";<portlet:namespace/>fiscalYearOrder = "desc";
		}
}
	<portlet:namespace/>ompListing('${searchURL}');
}
</script>
