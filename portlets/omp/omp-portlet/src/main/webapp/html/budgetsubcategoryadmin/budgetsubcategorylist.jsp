<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>

<div>
	<font color="green">${successMSG}</font> <br/> <font color="red">${errorMSG}</font>
</div>

<portlet:actionURL name="searchFilter" var="searchFilter" >
	<portlet:param name="redirect" value="${redirect}" />
</portlet:actionURL>  
<aui:form  class="cmxform" action="${searchFilter}" method="post" id="searchFilterForm">
	<table>
		<tr>
			<td>
				<aui:input label="Budget Sub Category Name " name="budgetSubCategoryName" type="text" value=""></aui:input>
			</td>
			<td>
				<br>								
				<aui:button value="Search" type="submit" />
			</td>
		</tr>
	</table>					
</aui:form>
<br>

<display:table decorator="com.hp.omp.decorator.BudgetSubCategoryListDecorator" name="budgetSubCategoryList" list="budgetSubCategoryList" id="budgetSubCategory">
  <display:column property="budgetSubCategoryId"  title="Delete" class="deleteicon"/>
  <display:column  title="Name" >
  
   	<portlet:renderURL var="editBudgetSubCategory">
	    <portlet:param name="jspPage" value="/html/budgetsubcategoryadmin/edit.jsp" />
	    <portlet:param name="budgetSubCategoryId" value="${budgetSubCategory.budgetSubCategoryId}" />
	    <portlet:param name="action" value="update" />
	</portlet:renderURL>
	
  	<a href="${editBudgetSubCategory}">${budgetSubCategory.budgetSubCategoryName}</a>
  </display:column>
  <display:column property="display" title="Display" />
</display:table>
<%@include file="/html/tablepaging/paging.jsp"%>