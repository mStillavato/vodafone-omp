<%@ page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://hp.com/vfItaly/jsp/taglib" prefix="hp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<script
	src="<%=request.getContextPath()%>/js/upload Ajax/ajaxfileupload.js"></script>
<portlet:resourceURL var="addPosition" id="addPosition"
	escapeXml="false">
	<portlet:param name="action" value="addPosition" />
</portlet:resourceURL>
<portlet:resourceURL var="updatePosition" id="updatePosition"
	escapeXml="false">
	<portlet:param name="action" value="updatePosition" />
</portlet:resourceURL>
<portlet:resourceURL var="deletePosition" id="deletePosition"
	escapeXml="false">
	<portlet:param name="action" value="deletePosition" />
</portlet:resourceURL>

<portlet:resourceURL var="approvePosition" id="approvePosition"
	escapeXml="false">
	<portlet:param name="action" value="approvePosition" />
</portlet:resourceURL>

<portlet:resourceURL var="addPurchaseRequest" id="addPurchaseRequest"
	escapeXml="false">
	<portlet:param name="action" value="addPurchaseRequest" />
</portlet:resourceURL>
<portlet:resourceURL var="updatePurchaseRequest"
	id="updatePurchaseRequest" escapeXml="false">
	<portlet:param name="action" value="updatePurchaseRequest" />
</portlet:resourceURL>

<portlet:actionURL name="submitPurchaseRequest"
	var="submitPurchaseRequestURL">
</portlet:actionURL>
<portlet:actionURL name="deletePurchaseRequest"
	var="deletePurchaseRequestURL">
	<portlet:param name="redirect"
		value="/html/purchaseRequest/landing.jsp" />
</portlet:actionURL>
<portlet:actionURL name="startPurchaseRequest"
	var="startPurchaseRequestURL">
</portlet:actionURL>

<portlet:actionURL name="rejectPurchaseRequest"
	var="rejectPurchaseRequestURL">
</portlet:actionURL>

<portlet:resourceURL id="downloadFile" var="downloadFileURL">
	<portlet:param name="action" value="downloadFile" />
</portlet:resourceURL>

<portlet:resourceURL id="deleteFile" var="deleteFileURL">
	<portlet:param name="action" value="deleteFile" />
</portlet:resourceURL>

<portlet:resourceURL id="downloadallFiles" var="downloadAllFilesURL">
	<portlet:param name="action" value="downloadAllFiles" />
</portlet:resourceURL>

<portlet:resourceURL id="uploadFileURL" var="uploadFileURL">
	<portlet:param name="action" value="uploadFile" />
</portlet:resourceURL>

<portlet:resourceURL id="listUploaded" var="listUploadedFilesURL">
	<portlet:param name="action" value="listFiles" />
</portlet:resourceURL>

<portlet:resourceURL id="getLabelNumber" var="getLabelNumberURL">
	<portlet:param name="action" value="getLabelNumber" />
</portlet:resourceURL>


<script type="text/javascript">
	var validatePos;
	function addUpdatepurchaseRequest() {

		resetRequiredFields();
		var purchaseRequestId = document.getElementById("purchaseRequestId").value;
		var automatic = document.getElementById("automatic").value;
		var costcenter = document.getElementById("costcenter").value;
		var ola = document.getElementById("ola").value;
		var buyer = document.getElementById("buyer").value;
		var vendor = document.getElementById("vendor").value;
		var totalvalue = document.getElementById("totalvalue").value;
		var prCurrency = document.getElementById("prCurrency").value;
		var activitydescription = document
				.getElementById("activitydescription").value;
		
		var resourceURL;

		if (purchaseRequestId !== null && purchaseRequestId != "") {
			resourceURL = '${updatePurchaseRequest}';
			//alert("updatepurchaseRequest  "+resourceURL);

			var actionStatus = document.getElementById("actionStatus").value;
			var userType = document.getElementById("userType").value;
			if ((actionStatus != "Created" && actionStatus != "Rejected")
					&& userType == "CONTROLLER_VBS") {
				if (!validatePR()) {
					return false;
				}

			}
		} else {
			resourceURL = '${addPurchaseRequest}';
			//alert("addpurchaseRequest "+resourceURL);
		}

		jQuery
				.ajax({
					type : "POST",
					dataType : "json",
					url : resourceURL,
					data : {
						"purchaseRequestId" : purchaseRequestId,
						"automatic" : automatic,
						"costcenter" : costcenter,
						"ola" : ola,
						"buyer" : buyer,
						"vendor" : vendor,
						"totalvalue" : totalvalue,
						"prCurrency" : prCurrency,
						"activitydescription" : activitydescription
					},
					success : function(data) {
						//alert(data);
						if (data.status != "failure") {
							document.getElementById("purchaseRequestId").value = data.purchaseRequestID;
							document
									.getElementById("purchaseRequestSuccessMSG").innerHTML = '<div class="alert alert-success">OMP Request # '
									+ data.purchaseRequestID
									+ '  Saved Successfully</div>';
							var actionStatus = document
									.getElementById("actionStatus").value;
							if (actionStatus != 'Rejected') {
								document.getElementById("deleteBtnDiv").innerHTML = '<button type="button" class="cancelbtn" onclick="deletepurchaseRequest();" value="delete">delete</button>';
							}
						} else {
							document
									.getElementById("purchaseRequestSuccessMSG").innerHTML = '<div class="alert alert-danger">Your purchase Request failed to Save</div>';
						}

					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						document.getElementById("purchaseRequestSuccessMSG").innerHTML = '<div class="alert alert-danger">Your purchase Request failed to Save</div>';
					}
				});

	}

	function validatePREntities() {

		var numberofPositions = document.getElementById("numberOfPositions").value;
		//alert("numberofPositions = "+numberofPositions);
		if (numberofPositions == "0") {
			alert("number of positions = 0  you should first add at least one position");
			return false;
		} else {
			if (validateRequiredFields()) {
				//alert("validation is ok will submit");
				return true;
			} else {
				//alert("validation error");
				document.getElementById("purchaseRequestSuccessMSG").innerHTML = '<div class="alert alert-danger">your purchase Request contains missing parameters complete fields in Red color</div>';
				return false;
			}
		}
	}

	function submitpurchaseRequest() {
		var actionURL;
		actionURL = '${submitPurchaseRequestURL}';

		if (validatePR()) {
			document.getElementById("editpurchaseRequestForm").action = actionURL;
			document.getElementById("editpurchaseRequestForm").submit();
		}
	}

	function validatePR() {

		var automatic = $('#automatic').val();
		if (automatic == "true") {
			if ($('#filesNumber').val() != null
					&& $('#filesNumber').val() != '') {
				return validatePREntities();
			} else {
				alert("your PR is automatic you should upload document before submit");
				return false;
			}

		} else {
			return validatePREntities();
		}
	}

	function deletepurchaseRequest() {
		if (window
				.confirm("Are you sure you want to delete?you will delete the GRs,Positions,PO related to this PR")) {
			document.getElementById("editpurchaseRequestForm").action = '${deletePurchaseRequestURL}';
			document.getElementById("editpurchaseRequestForm").submit();
		}
	}

	function startPurchaseRequest() {
		document.getElementById("editpurchaseRequestForm").action = '${startPurchaseRequestURL}';
		document.getElementById("editpurchaseRequestForm").submit();
	}

	function rejectPurchaseRequest() {
		var prForm = document.getElementById("editpurchaseRequestForm");

		if ((prForm.elements["rejectionCause"].value == "" || prForm.elements["rejectionCause"].value == null)) {
			prForm.elements["rejectionCause"].style.borderColor = "#FF0000";
			document.getElementById("purchaseRequestSuccessMSG").innerHTML = '<div class="alert alert-danger">your purchase Request contains missing parameters complete fields in Red color</div>';

			return false;
		} else {
			prForm.elements["rejectionCause"].style.borderColor = "";
			document.getElementById("editpurchaseRequestForm").action = '${rejectPurchaseRequestURL}';
			document.getElementById("editpurchaseRequestForm").submit();
		}

	}

	function cancelPR() {
		document.location.href = '/group/vodafone/home';
	}

	function savePRAddPositionBtn() {

		var purchaseRequestId = document.getElementById("purchaseRequestId").value;
		var automatic = document.getElementById("automatic").value;
		var costcenter = document.getElementById("costcenter").value;
		var ola = document.getElementById("ola").value;
		var buyer = document.getElementById("buyer").value;
		var vendor = document.getElementById("vendor").value;
		var totalvalue = document.getElementById("totalvalue").value;
		var prCurrency = document.getElementById("prCurrency").value;
		var activitydescription = document
				.getElementById("activitydescription").value;

		var resourceURL;

		if (purchaseRequestId !== null && purchaseRequestId != "") {
			resourceURL = '${updatePurchaseRequest}';
			//alert("updatepurchaseRequest  "+resourceURL);
		} else {
			resourceURL = '${addPurchaseRequest}';
			//alert("addpurchaseRequest "+resourceURL);
		}

		jQuery
				.ajax({
					type : "POST",
					dataType : "json",
					url : resourceURL,
					data : {
						"purchaseRequestId" : purchaseRequestId,
						"automatic" : automatic,
						"costcenter" : costcenter,
						"ola" : ola,
						"buyer" : buyer,
						"vendor" : vendor,
						"totalvalue" : totalvalue,
						"prCurrency" : prCurrency,
						"activitydescription" : activitydescription
					},
					success : function(data) {
						//alert(data);
						if (data.status != "failure") {
							document.getElementById("purchaseRequestId").value = data.purchaseRequestID;
							document
									.getElementById("purchaseRequestSuccessMSG").innerHTML = '<div class="alert alert-success">OMP Request # '
									+ data.purchaseRequestID
									+ '  Saved Successfully</div>';

							document.getElementById('positionFormContainer').style.display = 'block';
							document.getElementById('editPositionBtn').value = 'Add';
							if (document.getElementById('deletePositionBtn') != null) {
								document.getElementById('deletePositionBtn').style.display = 'none';
							}
							//resetPositionForm();
							document.getElementById('labelNumber').value = 10;
							document.getElementById("labelNumberText").innerHTML = 10;
							var actionStatus = document
									.getElementById("actionStatus").value;
							if (actionStatus != 'Rejected') {
								document.getElementById("deleteBtnDiv").innerHTML = '<button type="button" class="cancelbtn" onclick="deletepurchaseRequest();" value="delete">delete</button>';
							}
						} else {
							document
									.getElementById("purchaseRequestSuccessMSG").innerHTML = '<div class="alert alert-danger">your purchase Request failed to Save</div>';
						}

					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						document.getElementById("purchaseRequestSuccessMSG").innerHTML = "<div class='alert alert-danger'>your purchase Request failed to Save</div>";
					}
				});

	}

	function checkFileExist() {
		var fileSelected = document.getElementById("fileName").value;
		if (fileSelected == "" || fileSelected == null) {
			alert("please choose file first");
		} else {
			var automatic = document.getElementById("automatic").value;
			if (automatic == "true" && $('#filesNumber').val() != null
					&& $('#filesNumber').val() != '') {
				if (window.confirm("Are you sure you want to replace the file")) {
					uploadFile();
				}
			} else {
				if ($('#purchaseRequestId').val() == ''
						|| $('#purchaseRequestId').val() == null) {
					savePRUploadBtn();
				} else {
					uploadFile();
				}
			}

		}
	}
	function savePRUploadBtn() {
		var purchaseRequestId = document.getElementById("purchaseRequestId").value;
		var automatic = document.getElementById("automatic").value;
		var costcenter = document.getElementById("costcenter").value;
		var ola = document.getElementById("ola").value;
		var buyer = document.getElementById("buyer").value;
		var vendor = document.getElementById("vendor").value;
		var totalvalue = document.getElementById("totalvalue").value;
		var prCurrency = document.getElementById("prCurrency").value;
		var activitydescription = document
				.getElementById("activitydescription").value;

		var resourceURL;

		if (purchaseRequestId !== null && purchaseRequestId != "") {
			resourceURL = '${updatePurchaseRequest}';
			//alert("updatepurchaseRequest  "+resourceURL);
		} else {
			resourceURL = '${addPurchaseRequest}';
			//alert("addpurchaseRequest "+resourceURL);
		}

		jQuery
				.ajax({
					type : "POST",
					dataType : "json",
					url : resourceURL,
					data : {
						"purchaseRequestId" : purchaseRequestId,
						"automatic" : automatic,
						"costcenter" : costcenter,
						"ola" : ola,
						"buyer" : buyer,
						"vendor" : vendor,
						"totalvalue" : totalvalue,
						"prCurrency" : prCurrency,
						"activitydescription" : activitydescription
					},
					success : function(data) {
						//alert(data);
						if (data.status != "failure") {
							document.getElementById("purchaseRequestId").value = data.purchaseRequestID;
							document
									.getElementById("purchaseRequestSuccessMSG").innerHTML = '<div class="alert alert-success">OMP Request # '
									+ data.purchaseRequestID
									+ '  Saved Successfully</div>';
							uploadFile();
							var actionStatus = document
									.getElementById("actionStatus").value;
							if (actionStatus != 'Rejected') {
								document.getElementById("deleteBtnDiv").innerHTML = '<button type="button" class="cancelbtn" onclick="deletepurchaseRequest();" value="delete">delete</button>';
							}

						} else {
							document
									.getElementById("purchaseRequestSuccessMSG").innerHTML = '<div class="alert alert-danger">your purchase Request failed to Save</div>';
						}

					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						document.getElementById("purchaseRequestSuccessMSG").innerHTML = "<div class='alert alert-danger'>your purchase Request failed to Save</div>";
					}
				});

	}

	function addUpdatePosition(action) {

		var actionStatus = document.getElementById("actionStatus").value;

		var positionId = document.getElementById("positionId").value;
		var labelNumber = document.getElementById("labelNumber").value;
		var positionStatus = document.getElementById("positionStatus").value;
		var purchaseRequestId = document.getElementById("purchaseRequestId").value;
		var fiscalYear = document.getElementById("fiscalyear").value;
		var categoryCode = document.getElementById("categorycode").value;
		var offerNumber = document.getElementById("offernumber").value;
		var quantity = document.getElementById("quantity").value;
		var deliveryAdress = document.getElementById("deliveryadress").value;
		var assetNumber = document.getElementById("asset-number").value;
		var prcsNumber = document.getElementById("prcs-number").value;
		var poNumber = document.getElementById("po-number").value;
		var actualUnitCost = document.getElementById("actual-unit-cost").value;
		var positionDescription = document
				.getElementById("position-description").value;
		var iandc = document.getElementById("iandc").value;
		
		var today = new Date();
        var year = today.getFullYear();
        var month = today.getMonth() + 1;
        var day = today.getDate();
        
        var month2;
        if (month == 10 || month == 11 || month == 12){
        	month2 = month;
        } else {
        	month2 = "0" + month;
        }
        
        var deliveryDate = "" + day + "/" + month2 + "/" + year ;
        var today2 = "" + year + month2 + day;
        var comparedDate = year + "0401";
        
        var todayInt = parseInt(today2);
        
        var comparedDateInt = parseInt(comparedDate);
        
        if(today < comparedDate){
            var newYear = Number(year) - 1;
            fiscalYear = newYear + "/" + year;
      	}else{
      		var newYear = Number(year) + 1;
      		fiscalYear = year + "/" + newYear;
      	}

		var resourceURL;

		if (action == 'approve') {
			resourceURL = '${approvePosition}';
		} else {
			if (action != 'delete') {
				if (document.getElementById("userType").value == "ANTEX") {
					var automatic = document.getElementById("automatic").value;
					//alert("automatic ="+automatic);
					if (automatic == "true") {
						//alert("inside automatic "+automatic);
						var automaticReturn = true;
						if (actionStatus != "Created") {
							if (assetNumber == "" || assetNumber == null) {
								document.getElementById("positionErrorMSG").innerHTML = '<div class="alert alert-danger">you should add asset,SC and PO numbers first</div>';
								document.getElementById("asset-number").style.borderColor = "#FF0000";
								automaticReturn = false;
							}
							if (prcsNumber == "" || prcsNumber == null) {
								document.getElementById("positionErrorMSG").innerHTML = '<div class="alert alert-danger">you should add asset,SC and PO numbers first</div>';
								document.getElementById("prcs-number").style.borderColor = "#FF0000";
								automaticReturn = false;
							}
							if (poNumber == "" || poNumber == null) {
								document.getElementById("positionErrorMSG").innerHTML = '<div class="alert alert-danger">you should add asset,SC and PO numbers first</div>';
								document.getElementById("po-number").style.borderColor = "#FF0000";
								automaticReturn = false;
							}
							//alert("automaticReturn ="+automaticReturn);
							if (!automaticReturn) {
								//alert("not automaticReturn ");
								return automaticReturn;
							}
						}

					} else {
						//alert("inside manual "+automatic);
						if (positionStatus == "NEW"
								&& actionStatus == "PR Assigned") {
							//alert("enter the asset number");
							var missingAsset = false;
							if (assetNumber == "" || assetNumber == null) {
								document.getElementById("asset-number").style.borderColor = "#FF0000";
								missingAsset = true;
							} else {
								document.getElementById("asset-number").style.borderColor = "";
							}
							if (missingAsset == true) {
								document.getElementById("positionErrorMSG").innerHTML = '<div class="alert alert-danger">you should add asset number</div>';
								return false;
							}
						}
						if (positionStatus == "ASSET_RECEIVED"
								&& (prcsNumber == "" || prcsNumber == null)) {
							//alert("enter the prcs number");
							document.getElementById("positionErrorMSG").innerHTML = '<div class="alert alert-danger">you should add PR/CS number first</div>';
							document.getElementById("prcs-number").style.borderColor = "#FF0000";
							return false;
						}
						if (positionStatus == "SC_RECEIVED"
								&& (poNumber == "" || poNumber == null)) {
							//alert("enter the prcs number");
							document.getElementById("positionErrorMSG").innerHTML = '<div class="alert alert-danger">you should add PO number first</div>';
							document.getElementById("po-number").style.borderColor = "#FF0000";
							return false;
						}
					}
				}
				if (positionId !== null && positionId != "") {
					resourceURL = '${updatePosition}';
					//alert("updatePosition  "+resourceURL);
				} else {
					resourceURL = '${addPosition}';
					//alert("addPosition "+resourceURL);
				}
			} else {
				resourceURL = '${deletePosition}';
				//alert("deletePosition "+resourceURL);
			}
		}

		jQuery.ajax({
			type : "POST",
			url : resourceURL,
			data : {
				"positionId" : positionId,
				"positionStatus" : positionStatus,
				"actionStatus" : actionStatus,
				"labelNumber" : labelNumber,
				"purchaseRequestId" : purchaseRequestId,
				"fiscalyear" : fiscalYear,
				"categorycode" : categoryCode,
				"offernumber" : offerNumber,
				"quantity" : quantity,
				"deliveryadress" : deliveryAdress,
				"asset-number" : assetNumber,
				"prcs-number" : prcsNumber,
				"po-number" : poNumber,
				"actual-unit-cost" : actualUnitCost,
				"position-description" : positionDescription,
				"iandc" : iandc,
				"deliverydate":deliveryDate
			},
			success : function(data) {
				$('#positionsContent').html(data);

				closemodal();
				resetPositionForm();
			}
		});
		$('.modal').on('shown.bs.modal', function(e) {
			$('.tltp').tooltip();
		});

	}

	function resetPositionForm() {
		var frm_elements = document.getElementById("positionForm").elements;
		for (i = 0; i < frm_elements.length; i++) {
			field_type = frm_elements[i].type.toLowerCase();
			switch (field_type) {
			case "text":
			case "password":
			case "textarea":
			case "hidden":
				frm_elements[i].value = "";
				break;
			case "radio":
			case "checkbox":
				if (frm_elements[i].checked) {
					frm_elements[i].checked = false;
				}
				break;
			case "select-one":
			case "select-multi":
				frm_elements[i].value = "";
				break;
			default:
				break;
			}
			frm_elements[i].style.borderColor = "";
		}
	}
	function setPositionFormInAddMode() {
		var numberofPositions = document.getElementById("numberOfPositions").value;
		//alert("numberofPositions = "+numberofPositions);
		if (numberofPositions == "0") {
			//alert("number of positions = 0 will add first")
			savePRAddPositionBtn();
		} else {
			document.getElementById('positionFormContainer').style.display = 'block';
			document.getElementById('editPositionBtn').value = 'Add';
			if (document.getElementById('deletePositionBtn') != null) {
				document.getElementById('deletePositionBtn').style.display = 'none';
			}

			resetPositionForm();
			fillPositionFromLatest(numberofPositions - 1);
		}
		var num = parseInt(numberofPositions) + 1;
		var labelNumber = num * 10;
		document.getElementById("labelNumberText").innerHTML = labelNumber;
		document.getElementById('labelNumber').value = labelNumber;
		//$(".modal").appendTo($("body"));
		/////////////////////////////////////////////////////
		//var purchaseRequestId=$('#purchaseRequestId').val();
		//jQuery.post('${getLabelNumberURL}',{purchaseRequestId:purchaseRequestId},
		//function(data) {
		//	$('#labelNumberText').html(data);
		//	$('#labelNumber').val(data);
		// }
		//);

		//////////////////////////////////////////////////////

		$('.modal').on('shown.bs.modal', function(e) {
			$('.tltp').tooltip();
		});

	}

	function hidePositionForm() {
		resetPositionForm();
		document.getElementById('positionFormContainer').style.display = 'none';

	}

	function fillPositionForm(i) {

		document.getElementById('positionFormContainer').style.display = 'block';
		if (document.getElementById('editPositionBtn') != null) {
			document.getElementById('editPositionBtn').value = 'update';
		}
		//document.getElementById('editPositionBtn').value='update';
		document.getElementById("positionErrorMSG").innerHTML = '';
		document.getElementById("prcs-number").style.borderColor = "";
		if (document.getElementById('deletePositionBtn') != null) {
			document.getElementById('deletePositionBtn').style.display = 'inline-block';
		}

		var actionStatus = document.getElementById("actionStatus").value;
		var userType = document.getElementById("userType").value;
		var positionStatus = document.getElementById("positionStatus_tr" + i).value;

		document.getElementById("positionId").value = document
				.getElementById("positionId_tr" + i).value;
		//alert(document.getElementById("positionId").id);
		document.getElementById("positionStatus").value = document
				.getElementById("positionStatus_tr" + i).value;
		document.getElementById("labelNumber").value = document
				.getElementById("labelNumber_tr" + i).value;
		document.getElementById("labelNumberText").innerHTML = document
				.getElementById("labelNumber_tr" + i).value;
		//alert(document.getElementById("positionStatus").id);
		document.getElementById("fiscalyear").value = document
				.getElementById("fiscalyear_tr" + i).value;
		//alert(document.getElementById("fiscalyear").id);
		document.getElementById("categorycode").value = document
				.getElementById("categorycode_tr" + i).value;
		//alert(document.getElementById("categorycode").id);
		document.getElementById("offernumber").value = document
				.getElementById("offernumber_tr" + i).value;
		//alert(document.getElementById("offernumber").id);
		document.getElementById("quantity").value = document
				.getElementById("quantity_tr" + i).value;
		//alert(document.getElementById("quantity").id);
		document.getElementById("deliveryadress").value = document
				.getElementById("deliveryadress_tr" + i).value;
		//alert(document.getElementById("deliveryadress").id);
		//document.getElementById("wbscode").value=document.getElementById("wbscode_tr"+i).value;
		//var wbscodeValue = document.getElementById("wbscode_tr"+i).value;
		//jQuery('#wbscode option:selected').removeAttr('selected');
		//jQuery('#wbscode').find('option:selected').removeAttr("selected");
		//jQuery("#wbscode :selected").prop('selected', false);
		//jQuery('#wbscode option[value="'+wbscodeValue+'"]').prop("selected",true);
		//$("#wbscode").data('combobox').refresh();
		//alert(document.getElementById("wbscode").id);
		document.getElementById("asset-number").value = document
				.getElementById("asset-number_tr" + i).value;
		//alert(document.getElementById("asset-number").id);
		document.getElementById("prcs-number").value = document
				.getElementById("prcs-number_tr" + i).value;
		//alert(document.getElementById("prcs-number").id);
		document.getElementById("po-number").value = document
				.getElementById("po-number_tr" + i).value;
		//alert(document.getElementById("po-number").id);
		document.getElementById("actual-unit-cost").value = document
				.getElementById("actual-unit-cost_tr" + i).value;
		//alert(document.getElementById("actual-unit-cost").id);
		document.getElementById("position-description").value = document
				.getElementById("position-description_tr" + i).value;
		//alert(document.getElementById("position-description").id);

		document.getElementById("iandc").value = document
				.getElementById("iandc_tr" + i).value;
		document.getElementById("positionStatusLabel").value = document
				.getElementById("positionStatusLabel_tr" + i).value;
		document.getElementById("approverName").value = document
				.getElementById("approverName_tr" + i).value;

		//alert(document.getElementById("position-approved_tr"+i).value);

		if (document.getElementById("approveBtn_tr" + i).value == 'true'
				&& (userType == "CONTROLLER_VBS" || userType == 'APPROVER_VBS')) {
			document.getElementById('approvePositionBtn').style.display = 'inline-block';
		} else {
			document.getElementById('approvePositionBtn').style.display = 'none';
		}

		/////////////////////////////////////////// highligh the fields ////////////////////////////////////////////////////
		if (validatePos == 'true') {
			var prForm = document.getElementById("positionForm");
			//alert("number of params "+prForm.elements.length);
			var ignoreElementsArr = [ 'asset-number', 'prcs-number',
					'po-number', 'iandc', 'positionStatusLabel', 
					'approverName', 'fiscalyear' ];
			for (i = 0; i < prForm.elements.length; i++) {
				field_type = prForm.elements[i].type.toLowerCase();

				switch (field_type) {
				case "text":
				case "password":
				case "textarea":
				case "hidden":
					if ((prForm.elements[i].value == "" || prForm.elements[i].value == null)
							&& $.inArray(prForm.elements[i].id,
									ignoreElementsArr) == -1) {
						prForm.elements[i].style.borderColor = "#FF0000";
					} else {
						prForm.elements[i].style.borderColor = "";
					}
				case "select":
				case "select-one":
				case "select-multi":
					if ((prForm.elements[i].value == "" || prForm.elements[i].value == null)
							&& $.inArray(prForm.elements[i].id,
									ignoreElementsArr) == -1) {
						prForm.elements[i].style.borderColor = "#FF0000";
					} else {
						prForm.elements[i].style.borderColor = "";
					}
				}
			}
		}

		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	}

	function fillPositionFromLatest(i) {

		//alert('fillPositionFromLatest');
		document.getElementById("fiscalyear").value = document
				.getElementById("fiscalyear_tr" + i).value;
		//alert(document.getElementById("fiscalyear").id);
		document.getElementById("categorycode").value = document
				.getElementById("categorycode_tr" + i).value;
		//alert(document.getElementById("categorycode").id);
		document.getElementById("offernumber").value = document
				.getElementById("offernumber_tr" + i).value;
		//alert(document.getElementById("offernumber").id);
		document.getElementById("deliveryadress").value = document
				.getElementById("deliveryadress_tr" + i).value;
		//alert(document.getElementById("deliveryadress").id);
		//document.getElementById("wbscode").value=document.getElementById("wbscode_tr"+i).value;
		//var wbscodeValue = document.getElementById("wbscode_tr"+i).value;
		//jQuery("#wbscode :selected").prop('selected', false);
		//jQuery('#wbscode option[value="'+wbscodeValue+'"]').prop("selected",true);
		//alert(document.getElementById("wbscode").id);
		document.getElementById("asset-number").value = document
				.getElementById("asset-number_tr" + i).value;
		//alert(document.getElementById("asset-number").id);
		document.getElementById("prcs-number").value = document
				.getElementById("prcs-number_tr" + i).value;
		//alert(document.getElementById("prcs-number").id);
		document.getElementById("po-number").value = document
				.getElementById("po-number_tr" + i).value;
		//alert(document.getElementById("po-number").id);
		document.getElementById("position-description").value = document
				.getElementById("position-description_tr" + i).value;
		//alert(document.getElementById("position-description").id);

		document.getElementById('positionStatusLabel').value = 'New';
		document.getElementById('approverName').value = 'N/A';
	}

	function resetRequiredFields() {
		var prForm = document.getElementById("editpurchaseRequestForm");
		for (i = 0; i < prForm.elements.length; i++) {
			prForm.elements[i].style.borderColor = "";
		}

	}

	function validateRequiredFields() {
		//alert("start validation");
		var result = true;
		var prForm = document.getElementById("editpurchaseRequestForm");
		//alert("number of params "+prForm.elements.length);
		var prIgnoreElementsArr = [ 'ola', 'rejectionCause' ];
		for (i = 0; i < prForm.elements.length; i++) {
			field_type = prForm.elements[i].type.toLowerCase();
			switch (field_type) {
			case "text":
			case "password":
			case "textarea":
			case "hidden":
				if ((prForm.elements[i].value == ""
						|| prForm.elements[i].value == null)
						&& $
								.inArray(prForm.elements[i].id,
										prIgnoreElementsArr) == -1) {
					prForm.elements[i].style.borderColor = "#FF0000";
					result = false;
				} else {
					prForm.elements[i].style.borderColor = "";
				}
			case "select":
			case "select-one":
			case "select-multi":
				if ((prForm.elements[i].value == ""
						|| prForm.elements[i].value == null)
						&& $
								.inArray(prForm.elements[i].id,
										prIgnoreElementsArr) == -1) {
					prForm.elements[i].style.borderColor = "#FF0000";
					result = false;
				} else {
					prForm.elements[i].style.borderColor = "";
				}
			}
		}
		var positionsTableForm = document.getElementById("positionsTableForm");
		//alert("positionsTableForm "+positionsTableForm);
		//alert("number of params "+positionsTableForm.elements.length);
		var ignoreElementsArr = [ 'asset-number', 'prcs-number', 'po-number',
				'iandc', 'positionStatusLabel',
				'approverName' ];
		for (i = 0; i < positionsTableForm.elements.length; i++) {
			field_type = positionsTableForm.elements[i].type.toLowerCase();
			switch (field_type) {
			case "text":
			case "password":
			case "textarea":
			case "hidden":
				var elementID = positionsTableForm.elements[i].id.split('_')[0];
				var trNumber = positionsTableForm.elements[i].id.split('_')[1];
				if (positionsTableForm.elements[i].value == ""
						&& $.inArray(elementID, ignoreElementsArr) == -1) {
					//alert(elementID);
					//alert("will chane the colcor for  "+document.getElementById("positionErrors_"+trNumber).id);
					document.getElementById("positionErrors_" + trNumber).innerHTML = '<div class="alert alert-danger">you should fill the required fields</div>';
					validatePos = 'true';
					result = false;
				}
			}
		}

		return result;
	}

	function uploadFile() {
		$(".uploading").fadeIn("normal");

		$.ajaxFileUpload({
			url : '${uploadFileURL}' + "&purchaseRequestId="
					+ $('#purchaseRequestId').val() + "&automatic="
					+ $('#automatic').val(),
			secureuri : false,
			fileElementId : 'fileName',
			dataType : 'json',
			data : {
				name : 'name',
				id : 'id'
			},
			success : function(data, status) {
				$('#hasUploadedFile').val('true');
				//$('#uploadedFile').html('<a href="javascript:viewUploadedfile()">view Uploaded File</a>');
				listUploadedFiles();
				$("#fileName").val(null);
				$(".uploading").fadeOut("fast");

				//alert("done success! status="+status);
				//alert("done success! data="+data);
				if (typeof (data.error) != 'undefined') {
					if (data.error != '') {
						alert("done success <1> with error! data.error="
								+ data.error);
						alert(data.error);
					} else {
						alert("done success <2> with error! data.error="
								+ data.error);
						alert(data.msg);
					}
				}
			},
			error : function(data, status, e) {
				alert("done error! status=" + status);
				alert("done error! data=" + data);
				alert("done error! e=" + e);
				alert(e);
			}
		});

	}
	function viewUploadedfile(fileName) {
		document.location.href = '${downloadFileURL}' + "&purchaseRequestId="
				+ $('#purchaseRequestId').val() + "&fileName="
				+ encodeURIComponent(fileName.replace(/\u00a0/g, ' '));
	}

	function viewAllUploadedfiles() {
		document.location.href = '${downloadAllFilesURL}'
				+ "&purchaseRequestId=" + $('#purchaseRequestId').val();
	}

	function listUploadedFiles() {
		var purchaseRequestId = $('#purchaseRequestId').val();
		jQuery.post('${listUploadedFilesURL}', {
			purchaseRequestId : purchaseRequestId
		}, function(data) {
			$('#uploadedFile').html(data);
			$('.deletered').tooltip();
			$("#uploadedFile").fadeIn("normal");
		});
	}

	function deleteFile(fileName) {
		if (window.confirm("Are you sure you want to delete the file")) {
			var purchaseRequestId = $('#purchaseRequestId').val();
			jQuery.post('${deleteFileURL}', {
				purchaseRequestId : purchaseRequestId,
				fileName : fileName.replace(/\u00a0/g, ' ')
			}, function(data) {
				$('#uploadedFile').html(data);
			});
		}
	}

	function checkIsNumber(field, fieldName) {
		if (field.value != "" && !$.isNumeric(field.value)) {
			alert("the " + fieldName + " should be number");
			field.value = "";
		}

	}
</script>