<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>   
           <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
           
           <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
           <link href="../../omp_main-theme/css/admin.css" rel="stylesheet">  
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
        <script type="text/javascript">
<!--
jQuery(document).ready(function() {
	jQuery("#buyerForm").validate();
});
//-->
</script>  

 
<portlet:actionURL name="addBuyer" var="addBuyerURL" >
<portlet:param name="redirect" value="${redirect}" />
</portlet:actionURL>  
<div class="adminform">         
<aui:form  class="cmxform" action="${addBuyerURL}" method="post" id="buyerForm">
<input type="hidden" name="buyerId" value="${buyer.buyerId}"/>
<aui:input label="Name: " name="name" type="text" value="${buyer.name}">
	<aui:validator name="required"/>
</aui:input>

<liferay-ui:error key="This buyer name exist" message="This buyer name exist"/>

		<div class="ompformlable"><label for="display">Display</label></div>
		<div class="ompforminput"><input type="checkbox" name="display"   ${buyer.display? "checked": ""}/></div>
                      
                        <input type="submit" value="Save"/>
</aui:form>
</div>
	
<script type="text/javascript">

</script>