<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
	<%@ taglib uri="http://hp.com/vfItaly/jsp/taglib" prefix="hp" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:renderURL var="getGRForm" windowState="exclusive">
	<portlet:param name="jspPage" value="/html/purchaseordernotnss/grform.jsp"/>
	<portlet:param name="grId" value="${gr.goodReceiptId}"/>
	<portlet:param name="GET_CMD" value="grForm"/>
</portlet:renderURL>
<portlet:resourceURL var="deleteGR">
	<portlet:param name="grId" value="${gr.goodReceiptId}"/>
	<portlet:param name="positionId" value="${gr.positionId}"/>
	<portlet:param name="POST_CMD" value="grDelete"/>
</portlet:resourceURL>

<div class="clear"></div>
					<div class="grinfobox" >
						<div class="grnumbertitle">GR #${gr.goodReceiptId}</div>
						<div class="clear"></div>
						<div class='posaccordlabel'>GR Value</div> <div class='posaccordinfo'>${gr.formattedGrValue }</div>
						<div class='posaccordlabel'>Request date</div><div class='posaccordinfo'>${gr.requestDateText }</div>
						<div class='posaccordlabel'>Registration date</div> <div class='posaccordinfo'>${gr.registerationDateText }</div>
						<div class="clear"></div>
						<div class='posaccordlabel'>Percentage</div> <div class='posaccordinfo'>${gr.formattedPercentage}</div>
						<div class='posaccordlabel'>Number</div> <div class='posaccordinfo'>${gr.goodReceiptNumber}</div>
						<div class="clear"></div>
						<div class='posaccordlabel'>File Uploaded</div> <div class='posaccorddesc'>${gr.grFile}</div>
						<div class='positioneditbtnarea'>
							
						<portlet:renderURL var="editGRForm" windowState="exclusive">
							<portlet:param name="jspPage" value="/html/purchaseordernotnss/grform.jsp"/>
							<portlet:param name="grId" value="${gr.goodReceiptId}"/>
							<portlet:param name="GET_CMD" value="grForm"/>
						</portlet:renderURL>
					
						<c:if test="${(empty gr.registerationDate || empty gr.goodReceiptNumber) && (selectedposition.editGrBtn || position.editGrBtn)}">
							<input type='button' id='editPositionBtnTr'  value ='Edit' class='positioneditbtn' data-toggle='modal' data-target='#addeditgr' onClick="<portlet:namespace/>getGoodReceiptForm('${editGRForm}')">
						</c:if>
						
						<hp:if-hasrole userId="${user.userId}" roleName="OMP_CONTROLLER">
							<c:if test="${(not empty gr.registerationDate && not empty gr.goodReceiptNumber)}">
								<input type='button' id='editPositionBtnTr'  value ='Edit' class='positioneditbtn' data-toggle='modal' data-target='#addeditgr' onClick="<portlet:namespace/>getGoodReceiptForm('${editGRForm}')">
							</c:if>
							<input type='button' id='editPositionBtnTr' value ='Delete' class='positioneditbtn'  onClick="<portlet:namespace/>deleteGoodReceipt('${deleteGR}','grList${gr.positionId}')"/>
						</hp:if-hasrole>
							
						</div>
						<div class='clear'></div>
						<div class="addgrbtnarea"></div>
				</div>