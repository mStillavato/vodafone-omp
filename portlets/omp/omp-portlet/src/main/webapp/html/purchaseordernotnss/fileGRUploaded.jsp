<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<div class="uploadedfilestitle">Attached GR file</div>
<div class="clear"></div>

<a href="javascript:viewUploadedfile('${file}')">${file}</a>
<c:if test="${not empty file}">
	<button type="button" class="deletered"	onclick="deleteFile('${file}');" data-toggle="tooltip" title="" data-placement="right" data-original-title="Delete">delete</button>
</c:if>
<br>

