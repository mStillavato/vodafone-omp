<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<script type="text/javascript" src="../../omp_main-theme/js/jquery-1.10.2.min.js"></script>   
<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">   
<portlet:defineObjects />

<div id="<portlet:namespace/>costCenterTableList"  class="listingcontainer"></div>
<div class="loading" style='display:none;font-size:30px; color:#eee;margin-top:60px; text-align: center;'>Loading...</div>
<div>
<portlet:renderURL var="addcosetcenterURL">
    <portlet:param name="jspPage" value="/html/costcenteradmin/edit.jsp" />
</portlet:renderURL>

<p><a href="<%= addcosetcenterURL %>&action=add" class="actionbutn">add new cost center</a></p>

<portlet:resourceURL var="deletecostcenterURL">
	<portlet:param name="action" value="delete"/>
</portlet:resourceURL>
<portlet:resourceURL var="paginationURL">
</portlet:resourceURL>
 <script type="text/javascript">
 function removeCostCenter(costCenterId, costCenterName){
	 if(confirm("Do you want to delete this cost center ["+costCenterName+"]?")) {
		 var url="${deletecostcenterURL}&costCenterId="+costCenterId+"&costCenterName="+costCenterName;
			jQuery.get(url,function( data ) {
				  jQuery( "#<portlet:namespace/>costCenterTableList" ).html( data );
				});
			}
 		}
 
 
 function <portlet:namespace/>ompListing(url){
		jQuery("#<portlet:namespace/>costCenterTableList").fadeOut( "fast" );
		jQuery(".loading").fadeIn( "normal" );
		jQuery.post(url+"&action=pagination",
				 function(data) {
				   jQuery("#<portlet:namespace/>costCenterTableList").html(data);
				   jQuery(".loading").fadeOut( "fast" );
				   jQuery("#<portlet:namespace/>costCenterTableList").fadeIn( "fast" );
				 }
			);
		}
 
 
 <portlet:namespace/>ompListing('${paginationURL}');
 </script>
</div>