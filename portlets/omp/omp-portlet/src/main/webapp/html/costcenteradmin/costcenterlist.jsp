<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>	

<div>
	<font color="green">${successMSG}</font> <br/> <font color="red">${errorMSG}</font>
</div>

<portlet:actionURL name="searchFilter" var="searchFilter" >
	<portlet:param name="redirect" value="${redirect}" />
</portlet:actionURL>  
<aui:form  class="cmxform" action="${searchFilter}" method="post" id="searchFilterForm">
	<table>
		<tr>
			<td>
				<aui:input label="Cost Center Name " name="costCenterName" type="text" value=""></aui:input>
			</td>
			<td>
				<br>
				<aui:button value="Search" type="submit" />
			</td>
		</tr>
	</table>
</aui:form>
<br>

<display:table decorator="com.hp.omp.decorator.CostCenterListDecorator" id="costCenter" name="costCenterList" list="costCenterList" >
  <display:column property="costCenterId" title="Delete" class="deleteicon"/>
  <display:column  title="Name" >
  
  <portlet:renderURL var="editBudgetCategory">
	    <portlet:param name="jspPage" value="/html/costcenteradmin/edit.jsp" />
	    <portlet:param name="costCenterId" value="${costCenter.costCenterId}" />
	    <portlet:param name="action" value="update" />
	</portlet:renderURL>
	
  	<a href="${editBudgetCategory}">${costCenter.costCenterName}</a>
  
  
  </display:column>
  <display:column property="description" title="Description" />
  <display:column property="display" title="Display" />
</display:table>
<%@include file="/html/tablepaging/paging.jsp"%>