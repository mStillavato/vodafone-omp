			<div id="grList${selectedposition.positionId}">
				<c:forEach items="${selectedposition.positionGoodReceipts }" var="gr">
					<%@include file="gritem.jsp"%>
				</c:forEach>
				
				<portlet:resourceURL var="saveGR" />
							<portlet:renderURL var="getGRList" windowState="exclusive">
								<portlet:param name="positionId" value="${selectedposition.positionId}"/>
								<portlet:param name="jspPage" value="/html/purchaseordervbs/grlist.jsp"/>
								<portlet:param name="GET_CMD" value="grList"/>
				</portlet:renderURL>
							
							<!-- Add condition to in position impl ot get the sum of the position's gr  -->
							<c:if test="${(empty selectedposition.sumPositionGRPercentage || selectedposition.sumPositionGRPercentage < 100) && (selectedposition.addGrBtn == 'true')}">
								<portlet:renderURL var="getGRForm" windowState="exclusive">
									<portlet:param name="jspPage" value="/html/purchaseordervbs/grform.jsp"/>
									<portlet:param name="positionId" value="${selectedposition.positionId}"/>
									<portlet:param name="GET_CMD" value="grForm"/>
								</portlet:renderURL>
								<div class="addposition">
									<a  onclick="<portlet:namespace/>getGoodReceiptForm('${getGRForm}')" data-toggle='modal' data-target='#addeditgr' href="javascript:void(0)">add Good Receipt</a>
								</div>
							</c:if>
			</div>		
							<script type="text/javascript">
							
								function <portlet:namespace/>getGoodReceiptForm(url){
									
									jQuery.get(url,function( data ) {
										  jQuery( "#grForm" ).html( data );
										});
										
										$('.modal').on('shown.bs.modal', function (e) {
$(".datepicker").datepicker({
		  showOn: "both",
	      buttonImage: "../../omp_main-theme/images/datepicker.png",
	      buttonImageOnly: true,
	      dateFormat: 'dd/mm/yy',
});
})
										
								}
								
								function <portlet:namespace/>deleteGoodReceipt(url, divId){
									if(confirm("Do you want to delete this Good Receipt?")) {
										jQuery.get(url, function( data ) {
											  jQuery( "#" + divId ).html( data );
											});
									}
								}
								function <portlet:namespace/>saveGR(divId){
									var grId = jQuery("#<portlet:namespace/>grId").val();
									var positionId = jQuery("#<portlet:namespace/>positionId").val();
									var requestDate = jQuery("#<portlet:namespace/>requestDate").val();
									var percentage = jQuery("#<portlet:namespace/>percentage").val();
									percentage=percentage.replace(',','.');
									var grValue = jQuery("#<portlet:namespace/>grValue").val();
									var grNumber = jQuery("#<portlet:namespace/>grNumber").val();
									var registerationDate = jQuery("#<portlet:namespace/>registerationDate").val();
									var maxPercentage = jQuery("#<portlet:namespace/>maxPercentage").val();
									if(maxPercentage == "") {
										maxPercentage = 0;
									}
									if(Number(percentage) > Number(maxPercentage)){
										alert("Maximum percentage is: "+ maxPercentage);
										return;
									} else {
										   closemodal();
										jQuery.post('${saveGR}', {"POST_CMD":"grForm", grId: grId,positionId:positionId , requestDate: requestDate, percentage: percentage, grValue: grValue,grNumber:grNumber, registerationDate: registerationDate},
												 function(data) {
												   jQuery("#"+divId).html(data);
												 }
												);
												$.fn.clearForm = function() {
  return this.each(function() {
    var type = this.type, tag = this.tagName.toLowerCase();
    if (tag == 'form')
      return $(':input',this).clearForm();
    if (type == 'text' || type == 'password' || tag == 'textarea')
      this.value = '';
    else if (type == 'checkbox' || type == 'radio')
      this.checked = false;
    else if (tag == 'select')
      this.selectedIndex = -1;
  });
};
$('#adgrform').clearForm();
									}
									
									
								}
							</script>	