<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://hp.com/vfItaly/jsp/taglib" prefix="hp"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://hp.com/vfItaly/jsp/taglib" prefix="hp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />


<portlet:actionURL name="doDelete" var="deletePurchaseOrdertURL">
	<portlet:param name="redirect" value="${redirect}" />
</portlet:actionURL>

<portlet:actionURL name="postPOForm" var="postPOFormUrl">
	<portlet:param name="redirect" value="${redirect}" />
</portlet:actionURL>

<portlet:defineObjects />
<script src="../../omp_main-theme/js/jquery.numeric.js"></script>
<script type="text/javascript">

$(document).ready(function() {
	$(".numeric").numeric();
});

function deletepurchaseOrder(){
	
	var r=confirm("Are you sure you want to delete this purchase order ");
	if (r==true)
	  {
		var actionURL;
		actionURL='${deletePurchaseOrdertURL}';
		document.getElementById("poForm").action=actionURL;
		document.getElementById("poForm").submit();
	  }
	else
	  {
	 	 return false;
	  } 
	
}


function submitpurchaseOrder(){
	var actionURL;
	
	actionURL='${postPOFormUrl}';
	
	if(validateOneSelectedPositionAtLeast()){
		if(validateRequiredFields()){
			$('.combobox').removeAttr('disabled');
			document.getElementById("poForm").action=actionURL;
			document.getElementById("poForm").submit();
			} else {
				document.getElementById("purchaseOrderFailureMSG").innerHTML='<div class="alert alert-danger">your purchase order contains missing parameters complete fields in Red color</div>';
			}
	} else{
		alert('You Should Select at least one Position.');
	}
	
}

function validateOneSelectedPositionAtLeast(){
	//alert("validateOneSelectedPositionAtLeast");
	
	var oneChkBoxSelected = false;
	var poForm=document.getElementById("poForm");
	//alert("number of params "+poForm.elements.length);
		for(i=0; i<poForm.elements.length; i++){
			     field_type = poForm.elements[i].type.toLowerCase();
			     //alert('field_type' + field_type);
	        switch (field_type)
	        {
	        case "checkbox":
	        	if(poForm.elements[i].checked){
	        		oneChkBoxSelected = true;
	        	}
	        
			    	        
	        }
		}
		
	return oneChkBoxSelected;
}

function calculateTheValue(total){
	var percentage = jQuery("#<portlet:namespace/>percentage").val();
	percentage =percentage.replace(',','.');
	var value  = (Number(total) * Number(percentage))/100;
	value=value.toLocaleString('it-IT', {
	    minimumFractionDigits: 2 
	});
	jQuery("#<portlet:namespace/>grValue").val(value);
}

function validateRequiredFields(){
	//alert("start validation");
	var result=true;
	var poForm=document.getElementById("poForm");

		for(i = 0; i < poForm.elements.length; i++){
			  
			     field_type = poForm.elements[i].type.toLowerCase();
// 			     alert('field_type=' + field_type);
			     var placehoder = poForm.elements[i].getAttribute("placeholder");
			     
			     field_name = poForm.elements[i].name;
// 			     alert('field_name=' + field_name);
				
	        switch (field_type)
	        {
	        case "hidden":
	        case "button":
	        case "checkbox":
	        	if(poForm.elements[i].checked){
	        		var posid = poForm.elements[i].value;
	        		//alert('vare posid    '+posid);
	        		var povar = 'ponum';
	        		var polabelvar = 'polabelnumber';
	        		povar += posid;
	        		polabelvar += posid;
	        		//alert('ponum value '+document.getElementById(povar).value);
	        		if(document.getElementById(povar).value =="" || document.getElementById(povar).value ==null || document.getElementById(povar).value == document.getElementById(povar).getAttribute("placeholder")){
	        			document.getElementById(povar).style.borderColor="#FF0000";
	   				 	result=false;
	        		} else {
	        			document.getElementById(povar).style.borderColor="";
	        			
	        		}
	        		
	        		//alert('polabelvar  '+polabelvar);
	        		//alert('polabelvar value '+document.getElementById(polabelvar).value);
	        		if(document.getElementById(polabelvar).value =="" || document.getElementById(polabelvar).value ==null || document.getElementById(polabelvar).value == document.getElementById(polabelvar).getAttribute("placeholder")){
	        			
	        			document.getElementById(polabelvar).style.borderColor="#FF0000";
	        		//	alert('polabelvar text color '+document.getElementById(polabelvar).style.borderColor);
	   				 	result=false;
	        			
	        		} else {
	        			document.getElementById(polabelvar).style.borderColor="";
	        		}
	        		
	        		//alert('vare postatus    '+document.getElementById('postatus').value);
	        		if(document.getElementById('postatus').value == "true" ||document.getElementById('postatus').value == true ){
	        			//alert('po status is automatic    ');
	        			var assetvar = 'assetnum';
		        		assetvar += posid;
		        		
		        		var shoppingvar = 'shoppingcardnum';
		        		shoppingvar += posid;
		        		
		        	//	var targatecnicavar = 'targatecnica';
		        	//	targatecnicavar += posid;
		        	//	alert('targatecnicavar '+targatecnicavar);
		        		if(document.getElementById(assetvar).value =="" || document.getElementById(assetvar).value ==null || document.getElementById(assetvar).value == document.getElementById(assetvar).getAttribute("placeholder")){
		        			document.getElementById(assetvar).style.borderColor="#FF0000";
		        			//alert('assetvar text color '+document.getElementById(assetvar).style.borderColor);
		   				 	result=false;
		        		} else {
		        			document.getElementById(assetvar).style.borderColor="";
		        		}
		        		
		        		
		        		if(document.getElementById(shoppingvar).value =="" || document.getElementById(shoppingvar).value ==null || document.getElementById(shoppingvar).value == document.getElementById(shoppingvar).getAttribute("placeholder")){
		        			document.getElementById(shoppingvar).style.borderColor="#FF0000";

		        			//alert('shoppingvar text color '+document.getElementById(shoppingvar).style.borderColor);
		   				 	result=false;
		        		} else {
		        			
		        			document.getElementById(shoppingvar).style.borderColor="";
		        		}
		        		
		        		//alert('document.getElementById(targatecnicavar).value    '+document.getElementById(targatecnicavar).value);
		        		/* if(document.getElementById(targatecnicavar).value =="" || document.getElementById(targatecnicavar).value ==null){
		        			document.getElementById(targatecnicavar).style.borderColor="#FF0000";

		        			//alert('targatecnicavar text color '+document.getElementById(targatecnicavar).style.borderColor);
		   				 	result=false;
		        		} else {
		        			document.getElementById(targatecnicavar).style.borderColor="";
		        		} */
	        		}
	        		
	        	}
	        case "text":
// 	        	alert(poForm.elements[i]);
	        	var filedname = poForm.elements[i].name;
 	       // 	alert('filedname '+filedname);
			//	alert('filed value'+poForm.elements[i].value);
					
	   		 	if(filedname.indexOf("ponum") == -1
	   		 		&& filedname.indexOf("polabelnumber") == -1
	   				&& filedname.indexOf("assetnum") == -1
	   				&& filedname.indexOf("shoppingcardnum") == -1
	   				&& filedname.indexOf("targatecnica") == -1
	   				&& filedname.indexOf("totalvalue") == -1
	   				&& filedname.indexOf("ola") == -1
	   				&& filedname.indexOf("screenNameReq") == -1
	   				&& filedname.indexOf("screenNameRec") == -1
	   				&& filedname.indexOf("fieldStatusId") == -1
	   				&& (poForm.elements[i].value =="" || poForm.elements[i].value ==null || poForm.elements[i].value == poForm.elements[i].getAttribute("placeholder"))){
	   					poForm.elements[i].style.borderColor="#FF0000";
	   					//alert('filedname has validation erro '+filedname);
						 result=false;
					 } else if(filedname.indexOf("ponum") == -1 
							 && filedname.indexOf("assetnum") == -1 
							 && filedname.indexOf("polabelnumber") == -1
							 && filedname.indexOf("shoppingcardnum") == -1
							 && filedname.indexOf("targatecnica") == -1){
					 	poForm.elements[i].style.borderColor="";
					}
	        }
		}

		
	return result;
}


function enableLabNumTxtBox(checkbox,polabelnumtxtbox,postatus,poid) {
	
	//alert('checkbox.checked '+checkbox.checked);
	//alert('polabelnumtxtbox disabled '+document.getElementById(polabelnumtxtbox).disabled);
	
	        		
    if (checkbox.checked && (postatus == 5 || poid == 0 )) {
    	//alert("firstselectedpr value " + document.getElementById("firstselectedpr").value);
            document.getElementById(polabelnumtxtbox).disabled = false;
		
       
    
        
    } else {
    //	alert('set the id with empty');
    	 document.getElementById(polabelnumtxtbox).disabled = true;
    }
  
 // alert('polabelnumtxtbox disabled '+document.getElementById(polabelnumtxtbox).disabled);
}


</script>

<liferay-ui:error key="error-deleting"
	message="an error has occurred during delete purchase order" />
<liferay-ui:error key="error-submitting"
	message="an error has occurred during add/update purchase order" />



<div class="ompformcontainer">

	<div class="ompformtitle">Purchase Order VBS</div>
	<div class="clear"></div>
	<div id="purchaseOrderFailureMSG"></div>
	<form action="" method="post" id="poForm">
		<input type="hidden" name="poid" id="poid"
			value="${purchaseOrder.purchaseOrderId}" /> <input type="hidden"
			name="postatus" id="postatus" value="${purchaseOrder.automatic}" />
		<%-- 			<input type="hidden" name="costcenterid" id="costcenterid" value="${purchaseOrder.costCenterId}" /> --%>
		<input type="hidden" name="currencyid" id="currencyid"
			value="${purchaseOrder.currency}" /> <input type="hidden"
			name="screenNameReq" id="screenNameReq"
			value="${purchaseOrder.screenNameRequester}" /> <input type="hidden"
			name="screenNameRec" id="screenNameRec"
			value="${purchaseOrder.screenNameReciever}" /> <input type="hidden"
			name="activityDescription" id="activityDescription"
			value="${purchaseOrder.activityDescription}" /> <input type="hidden"
			name="vendorId" id="vendorId" value="${purchaseOrder.vendorId}" /> 
		<input type="hidden" name="fieldStatusId" id="fieldStatusId"
			value="${purchaseOrder.fieldStatus}" /> <input type="hidden"
			name="redirect" value="${redirect}" />


		<div class="ompformwrapper">
			<div class="ompformlable">
				<label>Cost Center</label>
			</div>
			<%--   		<div class="ompforminput"><input name="costCenterName" type="text" value="${costCenterName}" disabled /></div> --%>

			<div class="ompforminput">
				<select name="costcenter" class="combobox" id="costcenter"
					${purchaseOrder.fieldStatus}>
					<option selected value="">Select Cost Center..</option>
					<c:forEach items="${allCostCenters}" var="costcenter">
						<c:choose>
							<c:when
								test="${costcenter.costCenterId eq purchaseOrder.costCenterId}">
								<option selected value="${costcenter.costCenterId}">${costcenter.costCenterName}</option>
							</c:when>
							<c:otherwise>
								<option value="${costcenter.costCenterId}">${costcenter.costCenterName}</option>
							</c:otherwise>
						</c:choose>
					</c:forEach>
				</select>
			</div>
			
			<div class="ompformlable">
				<label>OLA</label>
			</div>
			<div class="ompforminput">
				<input name="ola" type="text" value="${purchaseOrder.ola}"
					${purchaseOrder.txtBoxStatus} placeholder="OLA" />
			</div>
			
			<div class="clear"></div>
			<div class="ompformlable">
				<label>Buyer</label>
			</div>
			<div class="ompforminput">
				<select name="buyer" class="combobox" id="buyer"
					${purchaseOrder.fieldStatus}>
					<option selected value="">Select Buyer..</option>
					<c:forEach items="${allBuyers}" var="buyer">
						<c:if
							test="${buyer.display eq true || buyer.buyerId eq purchaseOrder.buyerId}">
							<c:choose>
								<c:when test="${buyer.buyerId eq purchaseOrder.buyerId}">
									<option selected value="${buyer.buyerId}">${buyer.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${buyer.buyerId}">${buyer.name}</option>
								</c:otherwise>
							</c:choose>
						</c:if>
					</c:forEach>
				</select>
			</div>

			<div class="ompformlable">
				<label>Vendor </label>
			</div>
			<div class="ompforminput trackingrelatedDiv">
				<select name="vendor" class="combobox" id="vendor"
					${purchaseOrder.fieldStatus}>
					<option selected value="">Select Vendor..</option>
					<c:forEach items="${allVendors}" var="vendor">
						<c:if
							test="${vendor.display eq true || vendor.vendorId eq purchaseOrder.vendorId}">
							<c:choose>
								<c:when test="${vendor.vendorId eq purchaseOrder.vendorId}">
									<option selected value="${vendor.vendorId}">${vendor.vendorName}</option>
								</c:when>
								<c:otherwise>
									<option value="${vendor.vendorId}">${vendor.vendorName}</option>
								</c:otherwise>
							</c:choose>
						</c:if>
					</c:forEach>
				</select>
			</div>

			<div class="clear"></div>
			<div class="ompformlable">
				<label>Total Value</label>
			</div>
			<div class="ompforminput">
				<input name="totalvalue" type="text"
					value="${purchaseOrder.totalValue}" ${purchaseOrder.fieldStatus}
					readonly />
			</div>

			<div class="ompformlable">
				<label>Currency</label>
			</div>
			<div class="ompforminput">
				<input name="currencyName" id="currencyName" type="text"
					value="${currencyName}" readonly />
			</div>

			<!--  CR 3.3 edit position label number -->
			<c:choose>
				<c:when test="${purchaseOrder.status == '5'}">
					<c:set var="editlabelnumvar" value='disabled="disabled"' />
					<hp:if-hasrole userId="${user.userId}"
						roleName="OMP_CONTROLLER_VBS">
						<c:set var="editlabelnumvar" value="" />
					</hp:if-hasrole>
				</c:when>
				<c:otherwise>
					<c:set var="editlabelnumvar" value="disabled" />

				</c:otherwise>
			</c:choose>

			<!-- Selected Positions loop start-->
			<div class='panel-group' id='positionsaccordion'>
				<c:forEach items="${selectedPositions}" var="selectedposition"
					varStatus="i">
					<c:set value="ponum${selectedposition.positionId}"
						var="selectedpovar" />
					<c:set value="prid${selectedposition.positionId}"
						var="selectedposprvar" />
					<c:set value="assetnum${selectedposition.positionId}"
						var="selectedasvar" />
					<c:set value="shoppingcardnum${selectedposition.positionId}"
						var="selectedscvar" />
					<c:set value="polabelnumber${selectedposition.positionId}"
						var="selectedpolabelnumvar" />
					<c:set value="targatecnica${selectedposition.positionId}"
						var="selectedtargatecnicavar" />


					<c:choose>
						<c:when test="${selectedposition.poLabelNumber == '0'}">
							<c:set var="poLabelNumbervalue"
								value="${selectedposition.labelNumber}" />
						</c:when>
						<c:otherwise>
							<c:set var="poLabelNumbervalue"
								value="${selectedposition.poLabelNumber}" />
						</c:otherwise>
					</c:choose>

					<div class="ompforminput">
						<input type="hidden" name="${selectedposprvar}"
							id="${selectedposprvar}"
							value="${selectedposition.purchaseRequestId}" />
					</div>
					<div class="clear"></div>
					<c:if test="${not empty finalStep}">


						<!--Positions loop start-->

						<div class='panel-heading'>
							<div id='positionErrors_tr${i.index}'></div>
							<h4 class='panel-title'>
								<hp:if-hasrole userId="${user.userId}"
									roleName="OMP_CONTROLLER_VBS">
									<input type="checkbox" name="positions"
										value="${selectedposition.positionId}"
										${selectedposition.checkboxStatus}
										onchange="enableLabNumTxtBox(this,'${selectedpolabelnumvar}','${purchaseOrder.status}','${purchaseOrder.purchaseOrderId}');"
										checked />
								</hp:if-hasrole>
								<div class="posnumedit">
									<input id="${selectedpolabelnumvar}"
										name="${selectedpolabelnumvar}" type="text"
										class="number numeric" value="${poLabelNumbervalue}"
										${editlabelnumvar} />
								</div>
								<a data-toggle='collapse' data-parent='#positionsaccordion'
									href='#collapse${poLabelNumbervalue}'><span
									style="margin-right: 120px;">Position No:</span> <span
									style="padding-left: 20px;">OMP Request No:</span>
								<c:out value="${selectedposition.purchaseRequestId}" /></a>
								<div class='clear'></div>
							</h4>
						</div>

						<div class='clear'></div>
						<div id='collapse${poLabelNumbervalue}' class='panel-collapse in'>

							<div class='panel-body'>
								<div class='posaccordlabel'>I&C</div>
								<div class='posaccordinfo'>${selectedposition.iCApprovalText}</div>
								<div class='posaccordlabel'>Position Status</div>
								<div class='posaccordinfo'>${selectedposition.positionStatusLabel}</div>
								<div class='posaccordlabel'>Approver</div>
								<div class='posaccordinfo'>${selectedposition.approverName}</div>
								<div class="clear"></div>
								<div class='posaccordlabel'>Fiscal Year</div>
								<div class='posaccordinfo'>${selectedposition.fiscalYear }
								</div>
								<div class='posaccordlabel'>Category Code</div>
								<div class='posaccordinfo'>
									${selectedposition.categoryCode }</div>
								<div class='posaccordlabel'>Offer Number</div>
								<div class='posaccordinfo'>${selectedposition.offerNumber
									}</div>
								<div class="clear"></div>
								<div class='posaccordlabel'>Indirizzo di esecuzione attivit�</div>
								<div class='posaccordinfo'>
									${selectedposition.deliveryAddress }</div>
								<div class='posaccordlabel'>Asset Number</div>
								<div class='posaccordinfo'>${selectedposition.assetNumber
									}</div>
								<div class='posaccordlabel'>PR/SC Number</div>
								<div class='posaccordinfo'>
									${selectedposition.shoppingCart }</div>
								<div class="clear"></div>
								<div class='posaccordlabel'>PO Number</div>
								<div class='posaccordinfo'>${selectedposition.poNumber }</div>
								<div class='posaccordlabel'>Quantity</div>
								<div class='posaccordinfo'>${selectedposition.quatity }</div>
								<div class='posaccordlabel'>Actual Unit Cost</div>
								<div class='posaccordinfo'>
									${selectedposition.formattedUnitCost}</div>
								<div class="clear"></div>
								<div class='posaccordlabel'>Description</div>
								<div class='posaccordinfo'>${selectedposition.description
									}</div>
								<div class="clear"></div>

								<input type="hidden" name="${selectedpovar}"
									id="${selectedpovar}" value="${selectedposition.poNumber}" /> <input
									type="hidden" name="${selectedasvar}" id="${selectedasvar}"
									value="${selectedposition.assetNumber}" /> <input type="hidden"
									name="${selectedscvar}" id="${selectedscvar}"
									value="${selectedposition.shoppingCart}" /> 

								<!--gr loop start-->

								<%@include file="grpo.jsp"%>

								<div class='clear'></div>
								<!--gr loop end-->
							</div>
							<div class='clear'></div>
							<!--Positions loop end-->
						</div>

					</c:if>

					<div class="clear"></div>
					<!-- GR Part I have to put it in a separate jsp  -->



					<!-- The End of the GR part  -->


				</c:forEach>
			</div>
			<!-- Selected Positions loop end-->

			<!-- available Positions loop start-->


			<hp:if-hasrole userId="${user.userId}"
				roleName="OMP_CONTROLLER_VBS,OMP_ANTEX">
				<div class="clear"></div>
				<div class='panel-group' id='positionsaccordion'>
					<c:forEach items="${availablePositions}" var="position">
						<c:set value="ponum${position.positionId}" var="povar" />
						<c:set value="prid${position.positionId}" var="posprvar" />
						<c:set value="assetnum${position.positionId}" var="asvar" />
						<c:set value="shoppingcardnum${position.positionId}" var="scvar" />
						<c:set value="polabelnumber${position.positionId}"
							var="polabelnumvar" />
						<c:set value="targatecnica${position.positionId}"
							var="targatecnicavar" />


						<div class="ompforminput">
							<input type="hidden" name="${posprvar}" id="${posprvar}"
								value="${position.purchaseRequestId}" />
						</div>
						<div class="clear"></div>
						<div class='panel-heading'>
							<div id='positionErrors_tr${i.index}'></div>
							<h4 class='panel-title'>
								<input type="checkbox" name="positions"
									value="${position.positionId}" ${position.checkboxStatus}
									onchange="enableLabNumTxtBox(this,'${polabelnumvar}','${purchaseOrder.status}','${purchaseOrder.purchaseOrderId}');" />
								<div class="posnumedit">
									<input id="${polabelnumvar}" name="${polabelnumvar}"
										type="text" value="${position.labelNumber}"
										class="number numeric" disabled="disabled" />
								</div>
								<a data-toggle='collapse' data-parent='#positionsaccordion'
									href='#collapse${i.index}'><span
									style="margin-right: 120px;">Position No:</span> <span
									style="padding-left: 20px;">OMP Request No: </span>
								<c:out value="${position.purchaseRequestId}" /></a>
								<div class='clear'></div>
							</h4>
						</div>
						<div class='clear'></div>
						<div id='collapse${i.index}' class='panel-collapse in'>
							<div class='panel-body'>
								<div class='posaccordlabel'>I&C</div>
								<div class='posaccordinfo'>${position.iCApprovalText}</div>
								<div class='posaccordlabel'>Position Status</div>
								<div class='posaccordinfo'>${position.positionStatusLabel}</div>
								<div class='posaccordlabel'>Approver</div>
								<div class='posaccordinfo'>${position.approverName}</div>
								<div class="clear"></div>
								<div class='posaccordlabel'>Fiscal Year</div>
								<div class='posaccordinfo'>${position.fiscalYear }</div>
								<div class='posaccordlabel'>Category Code</div>
								<div class='posaccordinfo'>${position.categoryCode }</div>
								<div class='posaccordlabel'>Offer Number</div>
								<div class='posaccordinfo'>${position.offerNumber }</div>
								<div class="clear"></div>
								<div class='posaccordlabel'>Indirizzo di esecuzione attivit�</div>
								<div class='posaccordinfo'>${position.deliveryAddress }</div>
								<div class="posaccordlabel">
									<label>PO Number</label>
								</div>
								<div class="posaccordinfo">
									<input name="${povar}" id="${povar}" type="text"
										value="${position.poNumber}" ${position.fieldStatus} />
								</div>

								<c:choose>
									<c:when test="${purchaseOrder.automatic}">

										<div class="posaccordlabel">
											<label>Asset Number</label>
										</div>
										<div class="posaccordinfo">
											<input name="${asvar}" id="${asvar}" type="text"
												value="${position.assetNumber}" ${position.fieldStatus} />
										</div>
										<div class="clear"></div>
										<div class="posaccordlabel">
											<label>Shopping Card Number</label>
										</div>
										<div class="posaccordinfo">
											<input name="${scvar}" id="${scvar}" type="text"
												value="${position.shoppingCart}" ${position.fieldStatus} />
										</div>

									</c:when>
									<c:otherwise>
										<div class='posaccordlabel'>Asset Number</div>
										<div class='posaccordinfo'>${position.assetNumber}</div>
										<div class="clear"></div>
										<div class='posaccordlabel'>Shopping Card Number</div>
										<div class='posaccordinfo'>${position.shoppingCart}</div>
										<input type="hidden" name="${asvar}" id="${asvar}"
											value="${position.assetNumber}" />
										<input type="hidden" name="${scvar}" id="${scvar}"
											value="${position.shoppingCart}" />
										<div class="clear"></div>
									</c:otherwise>
								</c:choose>
								<div class='posaccordlabel'>Quantity</div>
								<div class='posaccordinfo'>${position.quatity }</div>
								<div class='posaccordlabel'>Actual Unit Cost</div>
								<div class='posaccordinfo'>${position.formattedUnitCost}</div>
								<div class="clear"></div>
								<div class='posaccordlabel'>Description</div>
								<div>${position.description }</div>
								<div class="clear"></div>
							</div>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>



					</c:forEach>
				</div>
				<div class="clear"></div>
			</hp:if-hasrole>

			<!-- Endl loop-->


			<div class="formactionbuttons">
				<c:if
					test="${(purchaseOrder.allowAdd) && (purchaseOrder.purchaseOrderId == 0) }">

					<button type="button" class="submitbtn" value="Submit"
						onclick="submitpurchaseOrder();">Add PO</button>

				</c:if>
				<c:if
					test="${(purchaseOrder.allowUpdate) && (purchaseOrder.purchaseOrderId != 0)}">
					<button type="button" class="submitbtn" value="Submit"
						onclick="submitpurchaseOrder();">Update PO</button>

				</c:if>

				<c:if
					test="${(purchaseOrder.allowDelete) && (purchaseOrder.purchaseOrderId != 0)}">
					<button type="button" class="cancelbtn" value="Submit"
						onclick="deletepurchaseOrder();">Delete PO</button>

				</c:if>

				<button type="button" class="cancelbtn" value="cancel"
					onclick="history.go(-1);">Cancel</button>

			</div>

		</div>
	</form>
	<div class="clear"></div>
</div>





<div class="modal fade" id="addeditgr" tabindex="-1" role="dialog"
	aria-labelledby="addpositionmodalLabel" aria-hidden="true">
	<div class="modal-dialog">

		<form name="positionForm" id="positionForm">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<div id="grForm"></div>
				</div>

			</div>
			<!-- /.modal-content -->
		</form>
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->