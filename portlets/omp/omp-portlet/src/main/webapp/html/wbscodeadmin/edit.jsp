<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ page import="javax.portlet.PortletPreferences" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">  
<portlet:defineObjects />

<portlet:actionURL var="wbscodeadminURL">
<portlet:param name="jspPage" value="/html/wbscodeadmin/view.jsp" />
</portlet:actionURL>
<div class="adminform">
<aui:form method="post" action="<%=wbscodeadminURL%>">
<aui:input label="WBS Code Name: " name="wbsCodeName" type="text" value="${wbsCodeName}">
<aui:validator name="required"/>
</aui:input>
<aui:input label="WBS CODE Description: " name="wbsCodeDesc" type="textarea" value="${wbsCodeDesc}"/> 
<aui:input label="WBS Owner Name: " name="wbsOwnerName" type="text" value="${wbsOwnerName}"/>
<aui:input name="action" type="hidden" value="${action}"/>

<c:if test="${not empty wbsCodeId}">
<aui:input name="wbsCodeId" type="hidden" value="${wbsCodeId}"/>
</c:if>
<aui:input inlineLabel="top" label="Display" name="display" type="checkbox" checked="${display}" />
<aui:button type="submit" /> 
</aui:form> 
</div>