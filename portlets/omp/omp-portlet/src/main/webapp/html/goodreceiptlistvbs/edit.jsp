<%@include file="init.jsp"%>

<portlet:actionURL name="setPortletMode" var="actionPortletMode"/>

<form action='${actionPortletMode}' method="post">
	<select name="mode">
		<option value="0" <c:if test="${mode eq 0}">selected="true"</c:if>>Requester Mode</option>
		<option value="1" <c:if test="${mode eq 1}">selected="true"</c:if>>Receiver Mode</option>
		<option value="2" <c:if test="${mode eq 2}">selected="true"</c:if>>Controller Mode</option>
	</select>
	<br/>
	<input type="submit" value="Change"> 
</form>
