<%@include file="init.jsp"%>
<%@include file="purchaseRequestJs.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://hp.com/vfItaly/jsp/taglib" prefix="hp"%>

<portlet:defineObjects />

<liferay-ui:error key="error-handling"
	message="Sorry, an error prevented handling your purchase request" />
<liferay-ui:error key="error-costcenter"
	message="Sorry, the logged inuser or the PR has no cost center" />
<liferay-ui:error key="error-deleting"
	message="Sorry, an error prevented deleting your purchase request" />
<liferay-ui:error key="error-submitting"
	message="Sorry, an error prevented submitting your purchase request" />


<div class="ompformcontainer">
	<div class="ompformtitle">Purchase Request</div>
	<div class="clear"></div>
	<div id="purchaseRequestSuccessMSG"></div>
	<c:if test="${prDisabled==''}">

		<form method="post" enctype="multipart/form-data" name="form" id=" ">
			<div class="uploadFileWrapper">
				<div class="popup_row2 browseFileWrapper">
					<div class="uploading">
						<img id="loading"
							src="<%=request.getContextPath()%>/js/upload Ajax/loading.gif"
							style=""> Uploading...
					</div>
					<div class="popup_row2_browse popup_add_bottomBtns"
						id="uploadFileDiv">
						<input type="file" name="fileName" id="fileName"
							value="Select File" class="input" required="required" />
					</div>
				</div>
				<div class="popup_row3 uploadBtn">
					<input type="button" name="upload" id="upload" value="Upload"
						class="noText_shadow" onclick="checkFileExist()" />
				</div>
			</div>
		</form>
	</c:if>

	<div ${pr.hasFile? "class='hasfiles'":"class='displaynone'"}
		id="uploadedFile">

		<%@include file="filelist.jsp"%>
	</div>

	<!--  	<div id="uploadedFile">
	<c:if test="${hasUploadedFile==true}">
		<a href="javascript:viewUploadedfile()">view Uploaded File</a> 
	</c:if>
	</div>-->

	<form action="" method="post" id="editpurchaseRequestForm">
		<input type="hidden" id="numberOfPositions" name="numberOfPositions"
			value="${numberOfPositions}" /> <input type="hidden"
			id="actionStatus" name="actionStatus" value="${actionStatus}" /> <input
			type="hidden" name="automatic" id="automatic" value="${pr.automatic}" />
		<input type="hidden" id="hasUploadedFile" name="hasUploadedFile"
			value="${hasUploadedFile}" /> <input type="hidden" id="userType"
			name="userType" value="${userType}" /> <input type="hidden"
			name="redirect" id="redirect" value="${redirect}" />
		<c:if test="${pr.purchaseRequestId == 0 }">
			<input type="hidden" name="purchaseRequestId" id="purchaseRequestId"
				value="" />
		</c:if>
		<c:if test="${pr.purchaseRequestId != 0 }">
			<input type="hidden" name="purchaseRequestId" id="purchaseRequestId"
				value="${pr.purchaseRequestId}" />
		</c:if>

		<div class="ompformwrapper">
			<div class="ompformlable">
				<label for="costcenter">Cost Center</label>
			</div>
			<div class="ompforminput">
				<select class="combobox" name="costcenter" id="costcenter"
					<c:if test="${userType !='CONTROLLER'}">disabled="disabled"</c:if>>
					<option selected value="">Select cost center..</option>
					<c:forEach items="${allCostCenters}" var="costcenter">
						<c:if
							test="${costcenter.display eq true || costcenter.costCenterId eq pr.costCenterId}">
							<c:choose>
								<c:when test="${costcenter.costCenterId eq pr.costCenterId}">
									<option selected value="${costcenter.costCenterId}">${costcenter.costCenterName}</option>
								</c:when>
								<c:otherwise>
									<option value="${costcenter.costCenterId}">${costcenter.costCenterName}</option>
								</c:otherwise>
							</c:choose>
						</c:if>
					</c:forEach>
				</select>
			</div>
			<div class="ompformlable">
				<label for="ola">OLA</label>
			</div>
			<div class="ompforminput">
				<input name="ola" id="ola" type="text" value="${pr.ola}"
					${prDisabled} placeholder="OLA" />
			</div>


			<div class="ompformlable">
				<label for="trackingcode">Tracking Code</label>
			</div>
			<div class="ompforminput trackingcode">

				<input name="trackingcode" id="trackingcode" type="text"
					value="<c:out value='${pr.trackingCode}'/>" ${prDisabled}
					placeholder="Tracking code" onchange="poulateTrackingValues();"/>
			</div>

			<div class="clear"></div>
			<div class="ompformlable">
				<label for="prwbscode">WBS Code</label>
			</div>
			<div class="ompforminput trackingrelatedDiv">
				<select class="trackingrelated" name="prwbscode"
					id="prwbscode" ${prDisabled}>
					<option selected value="">Select WBS Code ..</option>
					<c:forEach items="${allWbsCodes}" var="wbscode">
						<c:if
							test="${wbscode.display eq true || wbscode.wbsCodeId eq pr.wbsCodeId}">
							<c:choose>
								<c:when test="${wbscode.wbsCodeId eq pr.wbsCodeId}">
									<option selected value="${wbscode.wbsCodeId}">${wbscode.wbsCodeName}</option>
								</c:when>
								<c:otherwise>
									<option value="${wbscode.wbsCodeId}">${wbscode.wbsCodeName}</option>
								</c:otherwise>
							</c:choose>
						</c:if>
					</c:forEach>
				</select>
			</div>
			
			<div class="ompformlable">
				<label for="projectname">Project Name</label>
			</div>
			<div class="ompforminput trackingrelatedDiv">
				<select class="trackingrelated" id="projectname"
					name="projectname" ${prDisabled}>

					<option selected value="">Select Project..</option>
					<c:forEach items="${allProjects}" var="project">
						<c:if
							test="${project.display eq true || project.projectId eq pr.projectId}">
							<c:choose>
								<c:when test="${project.projectId eq pr.projectId}">
									<option selected value="${project.projectId}">${project.projectName}</option>
								</c:when>
								<c:otherwise>
									<option value="${project.projectId}">${project.projectName}</option>
								</c:otherwise>
							</c:choose>
						</c:if>
					</c:forEach>
				</select>
			</div>
			<div class="clear"></div>

			<div class="ompformlable">
				<label for="buyer">Buyer</label>
			</div>
			<div class="ompforminput">

				<select class="combobox" id="buyer" name="buyer" ${prDisabled}>

					<option selected value="">Select Buyer..</option>
					<c:forEach items="${allBuyers}" var="buyer">
						<c:if
							test="${buyer.display eq true || buyer.buyerId eq pr.buyerId}">
							<c:choose>
								<c:when test="${buyer.buyerId eq pr.buyerId}">
									<option selected value="${buyer.buyerId}">${buyer.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${buyer.buyerId}">${buyer.name}</option>
								</c:otherwise>
							</c:choose>
						</c:if>
					</c:forEach>
				</select>
			</div>

			<div class="ompformlable">
				<label for="vendor">Vendor</label>
			</div>
			<div class="ompforminput trackingrelatedDiv">
				<select class="combobox" name="vendor" id="vendor" ${prDisabled}>
					<option selected value="">Select Vendor..</option>
					<c:forEach items="${allVendors}" var="vendor">
						<c:if
							test="${vendor.display eq true || vendor.vendorId eq pr.vendorId}">
							<c:choose>
								<c:when test="${vendor.vendorId eq pr.vendorId}">
									<option selected value="${vendor.vendorId}">${vendor.vendorName}</option>
								</c:when>
								<c:otherwise>
									<option value="${vendor.vendorId}">${vendor.vendorName}</option>
								</c:otherwise>
							</c:choose>
						</c:if>
					</c:forEach>
				</select>
			</div>

			<div class="clear"></div>
			<div class="ompformlable">
				<label for="totalvalue">Total Value</label>
			</div>
			<div class="ompforminput">
				<input name="totalvalue" id="totalvalue" type="text"
					value="${prTotalValue}" readonly />
			</div>
			<div class="ompformlable">
				<label for="prCurrency">Currency</label>
			</div>
			<div class="ompforminput">
				<select name="prCurrency" class="combobox" id="prCurrency"
					${prDisabled}>
					<option value="">Select Currency..</option>
					<option selected value="1" ${euroSelected}>Euro</option>
					<option value="0" ${dollarSelected}>Dollar</option>
				</select>
			</div>

			<div class="clear"></div>
			<div class="ompformlable">
				<label for="activitydescription">Activity Description</label>
			</div>
			<div class="ompforminputtextarea">
				<div class="counter"></div>
				<textarea class="txtareamax" rows="4" cols="50"
					name="activitydescription" id="activitydescription" ${prDisabled}>${pr.activityDescription}</textarea>
			</div>

			<div class="clear"></div>
			<div class="ompformlable">
				<label for="rejectionCause">Rejection Cause</label>
			</div>
			<div class="ompforminputtextarea">
				<div class="counter"></div>
				<textarea class="txtareamax" rows="4" cols="50"
					name="rejectionCause" id="rejectionCause" ${rejCauseDisabled}>${pr.rejectionCause}</textarea>
			</div>
			<div class="clear"></div>

		</div>
	</form>
	<%--  <div id="wbscodeSelectboxDiv" style="display: none">
      <select name="wbscodeSelectboxHidden" id="wbscodeSelectboxHidden" disabled>
			<option selected value="">Select WBS Code ..</option>
			<c:forEach items="${allWbsCodes}" var="wbscode">
				<option value="${wbscode.wbsCodeId}">${wbscode.wbsCodeName}</option>
			</c:forEach>
		</select>
     </div> --%>

	<div id="positionsContent">
		<%@include file="positionlist.jsp"%>
	</div>



	<div class="addposition">
		<c:if test="${prDisabled==''}">
			<button type="button" onclick="setPositionFormInAddMode();"
				value="Add Position" data-toggle="modal"
				data-target="#addpositionmodal">Add Position</button>
		</c:if>
	</div>

	<div class="formactionbuttons">
		<c:if test="${prDisabled==''}">
			<button type="button" onclick="addUpdatepurchaseRequest();"	class="savebtn" value="Save">Save</button>
			<button type="button" onclick="checkIC();" id="idBtnSubmit" value="Submit" class="submitbtn">Submit</button>
		</c:if>

		<c:if test="${(userType =='ANTEX' ||userType =='CONTROLLER') && actionStatus=='Submitted' }">
			<button type="button" class="submitbtn"	onclick="startPurchaseRequest();" value="Start/Assign">Start/Assign</button>
			<button type="button" class="submitbtn"	onclick="rejectPurchaseRequest();" value="Reject">Reject</button>
		</c:if>

		<c:if test="${(userType =='NDSO' || userType =='TC' ||userType =='CONTROLLER') && actionStatus=='Pending IC approval' }">
			<button type="button" id="approvePositionAllBtn" class="submitbtn" onclick="approvePositionAll();" value="ApproveAll">Approve All</button>
			<button type="button" id="approvePositionSelBtn" class="submitbtn" onclick="approvePositionSel();" value="Approve" disabled>Approve</button>
			<button type="button" id="rejectPurchaseRequestBtn" class="submitbtn" onclick="rejectPurchaseRequest();" value="Reject">Reject</button>
		</c:if>

		<c:if test="${prDisabled==''}">
			<button type="button" value="cancel" onclick="cancelPR();" class="cancelbtn">Cancel</button>
		</c:if>
		
		<div id="deleteBtnDiv">
			<c:if test="${pr.purchaseRequestId != 0 && prDeleteBtn =='yes'}">
				<button type="button" class="cancelbtn"	onclick="deletepurchaseRequest();" value="delete">delete</button>
			</c:if>
		</div>
	</div>

	<script type="text/javascript">
$(document).ready(function () {
    $('#deliverydate').datepicker(
	{
	    showOn: "both",
	      buttonImage: "../../omp_main-theme/images/datepicker.png",
	      buttonImageOnly: true,
	      dateFormat: 'dd/mm/yy',
			onSelect: function(dateText) {
          var deliveryDate= $(this).datepicker('getDate');
          var year=$.datepicker.formatDate('yy', deliveryDate);
          var comparedDate = $.datepicker.parseDate('dd/mm/yy',"01/04/"+year);
          if(deliveryDate < comparedDate){
              var newYear=Number(year)-1;
              $('#fiscalyear').val(newYear+"/"+year);
        }else{
        	var newYear=Number(year)+1;
        	 $('#fiscalyear').val(year+"/"+newYear);
        }
         
    }});
    
    $('#deliverydate').blur(function (e) 
    		{
    	var deliveryDate= $(this).datepicker('getDate');
        var year=$.datepicker.formatDate('yy', deliveryDate);
        var comparedDate = $.datepicker.parseDate('dd/mm/yy',"01/04/"+year);
        if(deliveryDate < comparedDate){
            var newYear=Number(year)-1;
            $('#fiscalyear').val(newYear+"/"+year);
      }else{
      	var newYear=Number(year)+1;
      	 $('#fiscalyear').val(year+"/"+newYear);
      }
    	
    		});

    $(".number").keydown(function (e) {
        if (e.shiftKey) e.preventDefault();
        else {
            var nKeyCode = e.keyCode;
            //Ignore Backspace and Tab keys and comma
            if (nKeyCode == 8 || nKeyCode == 9 || (nKeyCode == 188 && this.value.split(',').length <= 1) ) return;
            if (nKeyCode < 95) {
                if (nKeyCode < 48 || nKeyCode > 57) e.preventDefault();
            } else {
                if (nKeyCode < 96 || nKeyCode > 105) e.preventDefault();
            }
        }
    });
});

</script>

	<br>
	<div class="modal fade" id="addpositionmodal" tabindex="-1" role="dialog" aria-labelledby="addpositionmodalLabel" aria-hidden="true">
		<div class="modal-dialog">
			
			<form name="positionForm" id="positionForm">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title" id="myModalLabel">
							Position No: <span id="labelNumberText"></span>
						</h4>
					</div>
					<div class="modal-body">
						<div id="positionFormContainer" style="display: none;">
							<div id="positionErrorMSG"></div>

							<input type="hidden" name="positionId" id="positionId" /> <input
								type="hidden" name="positionStatus" id="positionStatus" value="" />
							<input name="labelNumber" id="labelNumber" type="hidden"
								value="10" />

							<div class="ompformlable">
								<label for="iandc">I&C</label>
							</div>
							<div class="ompforminput">
								<select name="iandc" id="iandc" ${prDisabled} >
									<option selected value="">Select i&c ..</option>
									<c:forEach items="${allICTypes}" var="icType">
										<option value="${icType.code}">${icType.label}</option>
									</c:forEach>
								</select>
							</div>
							<div class="ompformlable">
								<label for="positionStatusLabel">Position Status</label>
							</div>
							<div class="ompforminput">
								<input name="positionStatusLabel" id="positionStatusLabel"
									type="text" value="New" disabled />
							</div>
							
							
							<div class="ompformlable">
								<label for="approverName">Approver Name</label>
							</div>
							<div class="ompforminput">
								<input name="approverName" id="approverName" type="text"
									value="N/A" disabled />
							</div>
							<div class="ompformlable">
								<label for="fiscalyear">Fiscal Year</label>
							</div>
							<div class="ompforminput">
								<input name="fiscalyear" id="fiscalyear" type="text" value=""
									disabled />
							</div>
							
							
							<div class="ompformlable">
								<label for="categorycode">Category Code</label>
							</div>
							<div class="ompforminput">
								<input name="categorycode" id="categorycode" type="text"
									value="" ${prDisabled} />
							</div>
							<div class="clear"></div>


							<div class="ompformlable">
								<label for="offernumber">Offer Number</label>
							</div>
							<div class="ompforminput">
								<input name="offernumber" id="offernumber" type="text" value=""
									${prDisabled} />
							</div>
							<div class="ompformlable">
								<label for="offerdate">Offer date</label>
							</div>
							<div class="ompforminput">
								<input name="offerdate" id="offerdate" class="datepicker"
									type="text" value="" ${prDisabled} />
							</div>


							<div class="ompformlable">
								<label for="deliverydate">Delivery Date</label>
							</div>
							<div class="ompforminput">
								<input name="deliverydate" id="deliverydate" class="datepicker"
									type="text" value="" ${prDisabled} />
							</div>
							<div class="ompformlable">
								<label for="deliveryadress">Delivery Address</label>
							</div>
							<div class="ompforminput">
								<input name="deliveryadress" id="deliveryadress" type="text"
									value="" ${prDisabled} />
							</div>

							<div class="ompformlable">
								<label for="targaTecnica">Targa Tecnica / Location EVO</label>
							</div>
							<div class="ompforminput autocomplete">
								<input name="targaTecnica" id="targaTecnica" type="text"
									value="" ${prDisabled}/>
							</div>

							<div class="ompformlable">
								<label for="asset-number">Asset Number</label>
							</div>
							<div class="ompforminput">
								<input name="asset-number" id="asset-number" type="text"
									value="" ${assetNumberDisabled} />
							</div>
							<div class="clear"></div>
							
							<div class="ompformlable">
								<label for="prcs-number">PR/SC Number</label>
							</div>
							<div class="ompforminput">
								<input name="prcs-number" id="prcs-number" type="text" value=""
									${prcsNumberDisabled} />
							</div>
							
							
							<div class="ompformlable">
								<label for="po-number">PO Number</label>
							</div>
							<div class="ompforminput">
								<input name="po-number" id="po-number" type="text" value=""
									${poNumberDisabled} />
							</div>
							<div class="clear"></div>
							
							
							<div class="ompformlable">
								<label for="quantity">Quantity</label>
							</div>
							<div class="ompforminput">
								<input name="quantity" id="quantity" type="text" value=""
									class="number" ${prDisabled} />
							</div>
							<div class="ompformlable">
								<label for="actual-unit-cost">Actual Unit Cost</label>
							</div>
							<div class="ompforminput">
								<input name="actual-unit-cost" id="actual-unit-cost" type="text"
									value="" data-toggle="tooltip" data-placement="top"
									data-original-title='Only " , " allowed as a decimal seperator'
									class="number tltp" ${prDisabled} />
							</div>

							<div class="clear"></div>
							<div class="ompformlable">
								<label for="position-description">Description</label>
							</div>
							<div class="ompforminput">
								<div class="counter"></div>
								<textarea class="txtareamax" rows="4" cols="30"
									name="position-description" id="position-description"
									${prDisabled}></textarea>
							</div>
							<div class="clear"></div>

						</div>
					</div>
					<div class="modal-footer">
						<div class="formactionbuttons">
							<c:if
								test="${userType !='OBSERVER' && userType !='NDSO' && userType !='TC'}">
								<input type="button" id="editPositionBtn" class="submitbtn"
									value="Update"
									onClick="addUpdatePosition('addUpdate');return false;">
							</c:if>
							<hp:if-hasrole userId="${user.userId}" roleName="OMP_ENG">
								<c:if test="${userType =='OBSERVER' }">
									<input type="button" id="editPositionBtn" class="submitbtn"
										value="Update"
										onClick="addUpdatePosition('addUpdate');return false;">
								</c:if>
							</hp:if-hasrole>

							<input type="button" id="cancelEditPositionBtn" class="cancelbtn"
								value="Cancel" data-dismiss="modal"
								onClick="resetPositionForm();">
							<c:if
								test="${userType =='CONTROLLER' || (userType =='ENG' && (actionStatus=='Created'|| actionStatus=='Rejected') )}">
								<input type="button" id="deletePositionBtn" value="Delete"
									class="cancelbtn"
									onClick="addUpdatePosition('delete');return false;">
							</c:if>

							<input type="button" id="approvePositionBtn" value="Approve"
								class="submitbtn" style="display: none"
								onClick="addUpdatePosition('approve');return false;">

						</div>
					</div>
				</div>
				<!-- /.modal-content -->
			</form>
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	
	<br>
	<div class="modal fade" id="viewModalIcc" tabindex="-1"
		role="dialog" aria-labelledby="addpositionmodalLabel"
		aria-hidden="true">
		<div class="modal-dialog">
			<form name="iccForm" id="iccForm">
				<div class="modal-content">
					<div class="modal-header">
						<div class="formactionbuttons">
							<b> Obblighi normativi e procedurali per la Salute e Sicurezza dei lavoratori per l'installation and commissioning</b>
						</div>
					</div>
					<div class="modal-body">
						<div id="iccFormContainer" style="display: block;">
							<b>
								Per il rispetto delle regole di Health & Safety (Rif. DLG 81/08), l'attivit� di installazione potr� essere eseguita dal fornitore/subcontractor solo se verr� data evidenza al committente (NM per le sedi Core / Regioni per le altre sedi e comunque facendo riferimento alla matrice di committenza) della disponibilit� della seguente documentazione:
								<p>
								<br>&emsp;- Qualifica del fornitore completa e non scaduta
								<br>&emsp;- Autorizzazione al subappalto (se utilizzato) completa e non scaduta
								<br>&emsp;- DUVRI associato al contratto o all'ordine
								<p>L'emissione di tale documentazione � a cura di SCM, che si coordina con il fornitore e gli altri enti aziendali coinvolti (rivolgersi al Buyer in caso di dubbi in fase di emissione dell'ordine)
								Qualsiasi ritardo nella produzione dei documenti causa conseguente ritardo alle attivit�
							</b>
						</div>
					</div>
					<div class="modal-footer">
						<div class="formactionbuttons">
						    <input type="checkbox" id="presaVisione" name="presaVisione" value="ok" onclick="handleClick(this);">Dichiaro di aver preso visione dell'informativa<br>
							<br>
							<input type="button" id="iccCloseBtn" class="cancelbtn"
								value="Close" data-dismiss="modal" disabled>
						</div>
					</div>
				</div>
				<!-- /.modal-content -->
			</form>
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->