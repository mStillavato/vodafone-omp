<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://hp.com/vfItaly/jsp/taglib" prefix="hp" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script	src="<%=request.getContextPath()%>/js/upload Ajax/ajaxfileupload.js"></script>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:resourceURL id="uploadFileGR" var="uploadFileGRURL"/>

<portlet:defineObjects />
<script src="../../omp_main-theme/js/jquery.numeric.js"></script>

<hp:if-hasrole userId="${user.userId}" roleName="OMP_CONTROLLER,OMP_ENG,OMP_NDSO,OMP_TC">
	<c:if test="${(attachDoc4Gr)}">
		<div class="alert alert-success"><p align="center">Secondo le ultime specifiche rigurardo il nuovo processo GR, il Category Code inserito prevede di allegare la documentazione</p></div>
	</c:if>
</hp:if-hasrole>

<div  class="ompformcontainer">
 	
 		<div class="ompformtitle">Good Receipt</div>
 		<div class="clear"></div>
		<form method="post" id="adgrform" enctype="multipart/form-data">
			<input type="hidden" name="grId" id="<portlet:namespace/>grId" value="${gr.goodReceiptId}"/>
			<input type="hidden" name="positionId" id="<portlet:namespace/>positionId" value="${gr.positionId}"/>
			<input type="hidden" name="maxPercentage" id="<portlet:namespace/>maxPercentage" value="${gr.maxPercentage}"/>
		
			<div class="ompformwrapper">	
				<div class="ompformlable"><label>Request Date</label></div>
				<div class="ompforminput"><input type="text" name="requestDate" id="<portlet:namespace/>requestDate" class="datepicker" value="${gr.requestDateText}" ${gr.requestDateReadonly}/></div>
				
				<div class="ompformlable"><label>Percentage</label></div>
				<div class="ompforminput"><input type="text" name="percentage" class="number" id="<portlet:namespace/>percentage" value="${gr.formattedPercentageText}" ${gr.percentageReadonly} onmouseout="calculateTheValue('${gr.position.actualUnitCost * gr.position.quatity}');" onblur="calculateTheValue('${gr.position.actualUnitCost * gr.position.quatity}');"  onchange="calculateTheValue('${gr.position.actualUnitCost * gr.position.quatity}');"/></div>
				
				<div class="ompformlable"><label>Value</label></div>
				<div class="ompforminput"><input type="text" name="grValue" id="<portlet:namespace/>grValue" value="${gr.formattedGrValueText}" ${gr.grValueReadonly}/></div>
				
				<div class='ompformlable'><label>File Uploaded</label></div> 
				<div class='ompforminput'>
					<input type="text" name="grFile" id="<portlet:namespace/>grFile" value="${gr.grFile}" readonly/>
				</div>
				
				<hp:if-hasrole userId="${user.userId}" roleName="OMP_ANTEX">
					<div class="ompformlable"><label>Number</label></div>
					<div class="ompforminput">
						<input type="text" name="grNumber" id="<portlet:namespace/>grNumber" value="${gr.goodReceiptNumber}" ${gr.grNumberReadonly}/>
					</div>
					
					<div class="ompformlable"><label>Registration Date</label></div>
					<div class="ompforminput">
						<input type="text" name="registerationDate" id="<portlet:namespace/>registerationDate" class="datepicker" value="${gr.registerationDateText}" ${gr.registerationDateReadonly}/>
					</div>
				</hp:if-hasrole>
				<div class="clear"></div>
				
				<hp:if-hasrole userId="${user.userId}" roleName="OMP_CONTROLLER,OMP_ENG,OMP_NDSO,OMP_TC">
					<c:if test="${(attachDoc4Gr)}">
						<div class="uploading">
							<img id="loading" src="<%=request.getContextPath()%>/js/upload Ajax/loading.gif" style=""> Uploading...
						</div>
						<div class="ompformlable"><label>Upload GR Doc</label></div>
						<div class="ompforminput">
							<input type="file" name="fileName" id="<portlet:namespace/>fileName" value="Select File" required="required" />
						</div>

						<div class="popup_row3 uploadBtn">
							<input type="button" name="upload" id="upload" value="Upload" class="noText_shadow" onclick="checkFileExist()" />
						</div>
						<div class="clear"></div>

					</c:if>
				</hp:if-hasrole>
			</div>
		
			<div class="clear"></div>
			<div class="formactionbuttons">
				<hp:if-hasrole userId="${user.userId}" roleName="OMP_CONTROLLER,OMP_ENG,OMP_NDSO,OMP_TC">
					<c:if test="${(attachDoc4Gr)}">
						<button type='button' id='submitGR' value="Submit" class="disablebtn" onclick="<portlet:namespace/>saveGR('grList${gr.positionId}')" disabled>Submit</button>
					</c:if>
					<c:if test="${not (attachDoc4Gr)}">
						<button type='button' id='submitGR' value="Submit" class="submitbtn" onclick="<portlet:namespace/>saveGR('grList${gr.positionId}')">Submit</button>
					</c:if>
				</hp:if-hasrole>
				
				<hp:if-hasrole userId="${user.userId}" roleName="OMP_ANTEX">
					<button type='button' id='submitGR' class="submitbtn" value="Submit" onclick="<portlet:namespace/>saveGR('grList${gr.positionId}')">Submit</button>
				</hp:if-hasrole>
				
				<input type="button" value="Cancel" class="cancelbtn" data-dismiss="modal" />
			</div>
			
		</form>
		
</div>

<script type="text/javascript">
$(document).ready(function () {
       $(".number").keydown(function (e) {
        if (e.shiftKey) e.preventDefault();
        else {
            var nKeyCode = e.keyCode;
            //Ignore Backspace and Tab keys and comma
            if (nKeyCode == 8 || nKeyCode == 9 || (nKeyCode == 188 && this.value.split(',').length <= 1) ) return;
            if (nKeyCode < 95) {
                if (nKeyCode < 48 || nKeyCode > 57) e.preventDefault();
            } else {
                if (nKeyCode < 96 || nKeyCode > 105) e.preventDefault();
            }
        }
    });
});

function checkFileExist(){
	var fileSelected = jQuery("#<portlet:namespace/>fileName").val();
	if(fileSelected == "" || fileSelected == null){
		alert("please choose file first");
	} else{
		uploadFile();
	}
}

function uploadFile(){
	$(".uploading").fadeIn( "normal" );
	
	$.ajaxFileUpload(
		 {
			 url:'${uploadFileGRURL}'+"&purchaseOrderId="+$('#poid').val(),
			 secureuri : false,
			 fileElementId : '<portlet:namespace/>fileName',
			 dataType : 'json',
			 data : {
				 name : 'name',
				 id : 'id'
				 },
			 success : function(data, status) {
							 $("#submitGR").removeAttr('disabled');
							 $("#submitGR").removeClass("disablebtn");
							 $("#submitGR").addClass("submitbtn");
							 
							 var fileUploaded = jQuery("#<portlet:namespace/>fileName").val();
							 if(fileUploaded.indexOf('\\') != -1){
								 //alert("fileUploaded \\ = " + fileUploaded.lastIndexOf('\\'));
								 fileUploaded = fileUploaded.substring(fileUploaded.lastIndexOf('\\') + 1);
							 } else if(fileUploaded.indexOf('\\') == -1){
								 //alert("fileUploaded // = " + fileUploaded.lastIndexOf('/'));
								 fileUploaded = fileUploaded.substring(fileUploaded.lastIndexOf('/') + 1);
							 }
							 
							 $("#<portlet:namespace/>grFile").val(fileUploaded);
							 $("#<portlet:namespace/>fileName").val(null);

							 $(".uploading").fadeOut( "fast" );
							 
							 listUploadedFiles();
							 
							 //alert("done success! status="+status);
							 //alert("done success! data="+data);
							 if (typeof (data.error) != 'undefined') {
								 if (data.error != '') {
								 	alert("done success <1> with error! data.error="+data.error);
								 alert(data.error);
								 } else {
									alert("done success <2> with error! data.error="+data.error);
								 	alert(data.msg);
								 }
							 }
			 			},
			 error : function(data, status, e) {
							alert("done error! status = " + status);
							alert("done error! data = " + data);
							alert("done error! e = " + e);
						 	alert(e);
			 			}
		 }
	 );
}

</script>