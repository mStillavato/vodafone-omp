<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<liferay-theme:defineObjects />
<portlet:defineObjects />
<div class="uploadedfilestitle">Attached GR files</div>
<div class="clear"></div>
<c:forEach var="file" items="${uploadedFiles}" varStatus="status">
<li>
	<a href="javascript:viewUploadedfile('${file}')">${file}</a>
	<c:if test="${empty prDisabled}">
		<button type="button" class="deletered"	onclick="deleteFile('${file}');" data-toggle="tooltip" title="" data-placement="right" data-original-title="Delete">delete</button>
	</c:if>
	<br>
	<c:if test="${status.last}">
		<input type="hidden" id="filesNumber" name="filesNumber" value="${status.count}" />
		</li>
	</c:if>
</c:forEach>
<br>

<c:if test="${fn:length(uploadedFiles) gt 1}">
	<div class="viewallfiles"><button type="button" onclick="viewAllUploadedfiles();" class="submitbtn">Download All Files</button></div>
</c:if>

