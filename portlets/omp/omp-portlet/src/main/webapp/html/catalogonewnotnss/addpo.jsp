<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://hp.com/vfItaly/jsp/taglib" prefix="hp" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
	
<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:actionURL name="addPosition" var="addPositionUrl">
	<portlet:param name="redirect" value="${redirect}" />
</portlet:actionURL>

<portlet:actionURL name="deletePosition" var="deletePositionUrl" >
	<portlet:param name="redirect" value="${redirect}" />
</portlet:actionURL>

<portlet:actionURL name="updatePosition" var="updatePositionUrl" >
	<portlet:param name="redirect" value="${redirect}" />
</portlet:actionURL>

<portlet:defineObjects />

<script src="../../omp_main-theme/js/jquery.numeric.js"></script>

<script type="text/javascript">

$(document).ready(function() {
	$(".numeric").numeric();
});

function addPosition(){
	var primoVendorCode = document.getElementById("primoVendorCode").value;
	var vendorCode = document.getElementById("vendorCode").value;
	
	primoVendorCode = primoVendorCode.trim();
	vendorCode = vendorCode.trim();
	if (primoVendorCode != ""){
		if(primoVendorCode != vendorCode) {
			alert("ERROR! \n A vendor different from the previous one was selected");
			return false;
		}
	}
	
	var actionURL;
	actionURL='${addPositionUrl}';
	document.getElementById("poForm").action = actionURL;
	document.getElementById("poForm").submit();
}

function deletePosition(){
	if (confirm("Are you sure you want to delete this position?")) {
		var actionURL;
		actionURL = '${deletePositionUrl}';
		document.getElementById("poForm").action = actionURL;
		document.getElementById("poForm").submit();
	} else {
		return false;
	} 
}

function updatePosition(){
	if (confirm("Are you sure you want to update this position?")) {
		var actionURL;
		actionURL = '${updatePositionUrl}';
		document.getElementById("poForm").action = actionURL;
		document.getElementById("poForm").submit();
	} else {
		return false;
	} 
}

</script>

<liferay-ui:error key="error-deleting" message="an error has occurred during delete purchase order" />
<liferay-ui:error key="error-submitting" message="an error has occurred during add/update purchase order" />


<div  class="ompformcontainer">
 	<c:if test="${layoutNotNssCatalogoId == 0 }">
		<div class="ompformtitle">Add Position Not NSS Catalog</div>
	</c:if>
	<c:if test="${layoutNotNssCatalogoId != 0 }">
		<div class="ompformtitle">Update Position Not NSS Catalog</div>
	</c:if>
		
	<div id="purchaseOrderFailureMSG" ></div>
	
	<form action="" method="post" id="poForm">
		<input type="hidden" name="catalogoId"   			id="catalogoId"   			value="${catalogo.catalogoId}" />
		<input type="hidden" name="vendorCode"   			id="vendorCode"   			value="${catalogo.fornitoreCode}" />
		<input type="hidden" name="vendorDesc"   			id="vendorDesc"   			value="${catalogo.fornitoreDesc}" />
		<input type="hidden" name="materialId"   			id="materialId"   			value="${catalogo.matCod}" />
		<input type="hidden" name="materialDesc" 			id="materialDesc" 			value="${catalogo.matDesc}" />
		<input type="hidden" name="netPrice"     			id="netPrice"     			value="${catalogo.prezzo}" />
		<input type="hidden" name="currency"     			id="currency" 				value="${catalogo.valuta}" />
		<input type="hidden" name="primoVendorCode" 		id="primoVendorCode"		value="${primoVendorCode}" />
		<input type="hidden" name="layoutNotNssCatalogoId" 	id="layoutNotNssCatalogoId"	value="${layoutNotNssCatalogoId}" />
		<input type="hidden" name="lineNumber"   			id="lineNumber"   			value="${lineNumber}" />
		<input type="hidden" name="redirect" value="${redirect}" />		
		
		<div class='panel-body'>
			<div class='posaccordlabel'>Vendor</div>
			<div class='posaccorddesc'>${catalogo.fornitoreCode} - ${catalogo.fornitoreDesc}</div>
			<div class='clear'></div>
			
			<div class='posaccordlabel'>Material</div>
			<div class='posaccorddesc'>${catalogo.matCod} - ${catalogo.matDesc}</div>
			<div class='clear'></div>
									
			<div class='posaccordlabel'>Unit Cost</div>
			<div class='posaccorddesc'>${prezzo} ${catalogo.valuta}</div>
			
			<div class="ompformlable"><label for="quantity">Quantity</label></div>
			<div class="ompforminput"><input name="quantity" id="quantity" type="text" value="${quantity}" class="number"/></div>
			
			<div class="ompformlable">
				<label for="iandc">I&C</label>
			</div>
			<div class="ompforminput">
				<select name="iandc" id="iandc">
					<option selected value="">Select i&c ..</option>
					<c:choose>
						<c:when test="${icApproval eq '1'}">
							<option selected value="1">I&C Field</option>
						</c:when>
						<c:otherwise>
							<option value="1">I&C Field</option>
						</c:otherwise>
					</c:choose>
					<c:choose>
						<c:when test="${icApproval eq '2'}">
							<option selected value="2">I&C Test</option>
						</c:when>
						<c:otherwise>
							<option value="2">I&C Test</option>
						</c:otherwise>
					</c:choose>
				</select>
			</div>
			<div class='clear'></div>
			
			<div class="ompformlable">
				<label for="categorycode">Category Code</label>
			</div>
			<div class="ompforminput">
				<input name="categorycode" id="categorycode" type="text" value="${categoryCode}"  />
			</div>
			
			<div class="ompformlable">
				<label for="offernumber">Offer Number</label>
			</div>
			<div class="ompforminput">
				<input name="offernumber" id="offernumber" type="text" value="${offerNumber}" />
			</div>
			<div class='clear'></div>
							
	        <div class="ompformlable"><label for="deliverydate">Delivery Date</label></div>
	        <div class="ompforminput"><input name="deliverydate" id="deliverydate" class="datepicker" type="text" value="${deliverydate}"/></div>
			
	        <div class="ompformlable"><label for="deliveryadress">Delivery Address</label></div>
	        <div class="ompforminput"><input name="deliveryadress" id="deliveryadress" type="text" value="${deliveryadress}" /></div>
	
			<div class="ompformlable"><label for="locationEVO">Location EVO</label></div>
			<div class="ompforminput"><input name="locationEVO" id="locationEVO" type="text" value="${locationEVO}" /></div>
	
			<div class="ompformlable"><label for="targaTecnica">Targa Tecnica</label></div>
	        <div class="ompforminput"><input name="targaTecnica" id="targaTecnica" type="text" value="${targaTecnica}"/></div>
	
	        <div class="clear"></div>
	        <div class="ompformlable"><label for="itemText">Item Text</label></div>
			<div class="ompforminputtextarea">
				<div class="counter"></div>
				<textarea class="txtareamax" rows="4" cols="30" name="itemText" id="itemText">${itemText}</textarea>
			</div>
			<div class="clear"></div>
						 									 		
			<div class="formactionbuttons">
				<c:if test="${layoutNotNssCatalogoId == 0 }">
					<button type="button" class="submitbtn" value="Submit" onclick="addPosition();">Add Position</button>
				</c:if>
				<c:if test="${layoutNotNssCatalogoId != 0 }">
					<button type="button" class="submitbtn" value="Submit" onclick="updatePosition();">Update Position</button>
					<button type="button" class="submitbtn" value="Submit" onclick="deletePosition();">Delete Position</button>
				</c:if>
	       		
	      		<button type="button" class="cancelbtn"  value="cancel" onclick="history.go(-1);">Cancel</button>
			</div>
		</div>
	</form>
    <div class="clear"></div> 
</div>    
   
