create table OMP_PORTLET.BUYER (
    buyerId NUMBER(30) not null primary key,
    name VARCHAR(300) not null,
    display char(1) check (display in ('0','1'))
);

create sequence BUYER_ID_SEQ start with 1 increment by 1 
  NOMAXVALUE  
  NOCYCLE
  NOCACHE
  NOORDER; 
CREATE OR REPLACE TRIGGER  BUYER_ID_TRIG
  before insert on BUYER              
  for each row  
begin   
  if :NEW.buyerId is null then 
    select BUYER_ID_SEQ.nextval into :NEW.buyerId from dual; 
  end if; 
end;

alter table purchase_request add  buyerId NUMBER(30);
alter table purchase_order add  buyerId NUMBER(30);

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
 ADD CONSTRAINT fk_buyerId_buyer 
  FOREIGN KEY (BUYERID) 
  REFERENCES OMP_PORTLET.BUYER (BUYERID)
  ENABLE VALIDATE;
  
  ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
 ADD CONSTRAINT fk_buyer_po 
  FOREIGN KEY (BUYERID) 
  REFERENCES OMP_PORTLET.BUYER (BUYERID)
  ENABLE VALIDATE;
  
  insert into buyer (name,display)  select distinct pr.buyer,1 from purchase_request pr where pr.buyer is not null;  


update purchase_request set buyerId = (select buyerId from buyer where buyer.name = purchase_request.buyer)  where purchase_request.buyer is not null;

update purchase_order set buyerId = (select buyerId from buyer where buyer.name = purchase_order.buyer)  where purchase_order.buyer is not null;


