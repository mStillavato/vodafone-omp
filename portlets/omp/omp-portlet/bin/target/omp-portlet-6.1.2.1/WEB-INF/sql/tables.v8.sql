alter table 
   PURCHASE_REQUEST
modify 
( 
   costCenter    NUMBER(30) not null
);

alter table 
   PURCHASE_ORDER
modify 
( 
   costCenter    NUMBER(30) not null
);

alter table PURCHASE_REQUEST rename column costCenter to costCenterId;
alter table PURCHASE_ORDER rename column costCenter to costCenterId;


create table COST_CENTER (
	costCenterId NUMBER(30) not null primary key,
	costCenterName VARCHAR(300) not null,
	description VARCHAR(300) null
);

create table COST_CENTER_USER (
	costCenterUserId NUMBER(30) not null primary key,
	costCenterId NUMBER(30) not null,
	userId NUMBER(30) not null
);

ALTER TABLE PURCHASE_REQUEST
ADD CONSTRAINT fk_PR_COST_CENTER
FOREIGN KEY (costCenterId)
REFERENCES COST_CENTER(costCenterId);

ALTER TABLE PURCHASE_ORDER
ADD CONSTRAINT fk_PO_COST_CENTER
FOREIGN KEY (costCenterId)
REFERENCES COST_CENTER(costCenterId);

