<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>




<div  class="ompformcontainer">
 	
 		<div class="ompformtitle">Good Receipt</div>
 		<div class="clear"></div>
		<form method="post" id="adgrform">
			<input type="hidden" name="grId" id="<portlet:namespace/>grId" value="${gr.goodReceiptId}"/>
			<input type="hidden" name="positionId" id="<portlet:namespace/>positionId" value="${gr.positionId}"/>
			<input type="hidden" name="maxPercentage" id="<portlet:namespace/>maxPercentage" value="${gr.maxPercentage}"/>
		
		 <div class="ompformwrapper">	
			<div class="ompformlable"><label>Request Date</label></div>
			<div class="ompforminput"><input type="text" name="requestDate" id="<portlet:namespace/>requestDate" class="datepicker" value="${gr.requestDateText}" ${gr.requestDateReadonly}/></div>
			
			<div class="ompformlable"><label>Percentage</label></div>
			<div class="ompforminput"><input type="text" name="percentage" class="number" id="<portlet:namespace/>percentage" value="${gr.formattedPercentageText}" ${gr.percentageReadonly} onmouseout="calculateTheValue('${gr.position.actualUnitCost * gr.position.quatity}');" onblur="calculateTheValue('${gr.position.actualUnitCost * gr.position.quatity}');"  onchange="calculateTheValue('${gr.position.actualUnitCost * gr.position.quatity}');"/></div>
			
			<div class="ompformlable"><label>Value</label></div>
			<div class="ompforminput"><input type="text" name="grValue" id="<portlet:namespace/>grValue" value="${gr.formattedGrValueText}" ${gr.grValueReadonly}/></div>
			
			<div class="ompformlable"><label>Number</label></div>
			<div class="ompforminput"><input type="text" name="grNumber" id="<portlet:namespace/>grNumber" value="${gr.goodReceiptNumber}" ${gr.grNumberReadonly}/></div>
			
			<div class="ompformlable"><label>Registration Date</label></div>
			<div class="ompforminput"><input type="text" name="registerationDate" id="<portlet:namespace/>registerationDate" class="datepicker" value="${gr.registerationDateText}" ${gr.registerationDateReadonly}/></div>
			<div class="clear"></div>
		</div>	
			
			<div class="clear"></div>
			<div class="formactionbuttons">
				<input type="button" value="Submit" class="submitbtn" onclick="<portlet:namespace/>saveGR('grList${gr.positionId}')"/>
				<input type="button" value="Cancel" class="cancelbtn" data-dismiss="modal" />
			</div>
			
		</form>
		
</div>

<script type="text/javascript">
$(document).ready(function () {
       $(".number").keydown(function (e) {
        if (e.shiftKey) e.preventDefault();
        else {
            var nKeyCode = e.keyCode;
            //Ignore Backspace and Tab keys and comma
            if (nKeyCode == 8 || nKeyCode == 9 || (nKeyCode == 188 && this.value.split(',').length <= 1) ) return;
            if (nKeyCode < 95) {
                if (nKeyCode < 48 || nKeyCode > 57) e.preventDefault();
            } else {
                if (nKeyCode < 96 || nKeyCode > 105) e.preventDefault();
            }
        }
    });
});

</script>