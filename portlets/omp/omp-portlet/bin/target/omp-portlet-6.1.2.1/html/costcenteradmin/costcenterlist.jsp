<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
	<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<div>
	<font color="green">${successMSG}</font> <br/> <font color="red">${errorMSG}</font>
</div>

<display:table decorator="com.hp.omp.decorator.CostCenterListDecorator" id="costCenter" name="costCenterList" list="costCenterList" >
  <display:column property="costCenterId" title="Delete" class="deleteicon"/>
  <display:column  title="Name" >
  
  <portlet:renderURL var="editBudgetCategory">
	    <portlet:param name="jspPage" value="/html/costcenteradmin/edit.jsp" />
	    <portlet:param name="costCenterId" value="${costCenter.costCenterId}" />
	    <portlet:param name="action" value="update" />
	</portlet:renderURL>
	
  	<a href="${editBudgetCategory}">${costCenter.costCenterName}</a>
  
  
  </display:column>
  <display:column property="description" title="Description" />
  <display:column property="display" title="Display" />
</display:table>
<%@include file="/html/tablepaging/paging.jsp"%>