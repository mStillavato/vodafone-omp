<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ page import="javax.portlet.PortletPreferences" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script type="text/javascript" src="../../omp_main-theme/js/jquery-1.10.2.min.js"></script>   
<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">  
<portlet:defineObjects />

<portlet:actionURL var="microDriveradminURL">
<portlet:param name="jspPage" value="/html/microdriveradmin/view.jsp" />
</portlet:actionURL>
<div class="adminform">
<aui:form method="post" action="<%=microDriveradminURL%>"  onSubmit="checkAssignedMacroDrivers()">
<b>Macro Driver Name: </b><div>
<select id="macroDrivers" name="macroDrivers" multiple="multiple" style="float:left">
		<c:forEach items="${macroDriverList}" var="macroDriver">
		    <c:if test="${macroDriver.display eq true}">
					<option value="${macroDriver.macroDriverId}">${macroDriver.macroDriverName}</option>
			</c:if>
		</c:forEach>
	</select>
	
<div style="float:left; margin:3px 20px;">
	      <input id="btnleft" value=" >> " type="button" />
        <br /><br />
        <input id="btnright" value=" << " type="button" />
        </div>
       
        
	<select id="assignedMacroDrivers" name="assignedMacroDrivers" multiple="multiple" style="float:left">
		<c:forEach items="${assignedMacroDrivers}" var="assignedMacroDriver">
					<option value="${assignedMacroDriver.macroDriverId}">${assignedMacroDriver.macroDriverName}</option>
		</c:forEach>
	</select>
	         <div class="clear"></div>
<aui:input label="Micro Driver Name: " name="microDriverName" type="text" value="${microDriverName}">
	<aui:validator name="required"/>
</aui:input>
<aui:input name="action" type="hidden" value="${action}"/>

<c:if test="${not empty microDriverId}">
<aui:input name="microDriverId" type="hidden" value="${microDriverId}"/>
</c:if>
<aui:input inlineLabel="top" label="Display" name="display" type="checkbox" checked="${display}" />

<aui:button type="submit" /> 


<script type="text/javascript">

    jQuery(document).ready(function () {
        jQuery("#btnleft").click(function () {
            jQuery("#macroDrivers option:selected").each(function () {
                jQuery("#assignedMacroDrivers").append(jQuery(this).clone());
                jQuery(this).remove();
            });
        });

        jQuery("#btnright").click(function () {
            jQuery("#assignedMacroDrivers option:selected").each(function () {
                jQuery("#macroDrivers").append(jQuery(this).clone());
                jQuery(this).remove();
            });
        });
    });
    
    $(function() {
        $("#assignedMacroDrivers option").each(function(i){
            $("#macroDrivers option[value='"+$(this).val()+"']").remove();
        });
    });
    
    function checkAssignedMacroDrivers(){
      	 $("#assignedMacroDrivers option").each(function(i){
              $(this).prop("selected",true);
           });
    }
    
</script>

</aui:form> 
</div>
