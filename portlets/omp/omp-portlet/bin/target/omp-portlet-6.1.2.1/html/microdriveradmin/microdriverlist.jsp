<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<div>
	<font color="green">${successMSG}</font> <br/> <font color="red">${errorMSG}</font>
</div>

<display:table decorator="com.hp.omp.decorator.MicroDriverListDecorator" name="microDriverList" list="microDriverList" id="microDriver">
  <display:column property="microDriverId" title="Delete" class="deleteicon" />
  <display:column  title="Name" >
  <portlet:renderURL var="editMicroDriver">
	    <portlet:param name="jspPage" value="/html/microdriveradmin/edit.jsp" />
	    <portlet:param name="microDriverId" value="${microDriver.microDriverId}" />
	    <portlet:param name="action" value="update" />
	</portlet:renderURL>
	
  	<a href="${editMicroDriver}">${microDriver.microDriverName}</a>
  </display:column>
  <display:column property="display" title="Display" />
</display:table>
<%@include file="/html/tablepaging/paging.jsp"%>