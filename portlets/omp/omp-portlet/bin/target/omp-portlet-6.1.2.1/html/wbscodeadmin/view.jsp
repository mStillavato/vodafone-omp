<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<script type="text/javascript" src="../../omp_main-theme/js/jquery-1.10.2.min.js"></script>    
<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">
<portlet:defineObjects />

<div id="<portlet:namespace/>wbsCodeTableList"  class="listingcontainer"></div>
<div class="loading" style='display:none;font-size:30px; color:#eee;margin-top:60px; text-align: center;'>Loading...</div>
<div>
<portlet:renderURL var="addwbscodeURL">
    <portlet:param name="jspPage" value="/html/wbscodeadmin/edit.jsp" />
</portlet:renderURL>

<p><a href="<%= addwbscodeURL %>&action=add"  class="actionbutn"> add new wbs code</a></p>

<portlet:resourceURL var="deletewbscodeURL">
	<portlet:param name="action" value="delete"/>
</portlet:resourceURL>
<portlet:resourceURL var="paginationURL">
</portlet:resourceURL>
 <script type="text/javascript">
 function removeWbsCode(wbsCodeId, wbsCodeName){
	 if(confirm("Do you want to delete this wbs code ["+wbsCodeName+"]?")) {
		 var url="${deletewbscodeURL}&wbsCodeId="+wbsCodeId+"&wbsCodeName="+wbsCodeName;
			jQuery.get(url,function( data ) {
				  jQuery( "#<portlet:namespace/>wbsCodeTableList" ).html( data );
				});
			}
 		}
 
 
 function <portlet:namespace/>ompListing(url){
		jQuery("#<portlet:namespace/>wbsCodeTableList").fadeOut( "fast" );
		jQuery(".loading").fadeIn( "normal" );
		jQuery.post(url+"&action=pagination",
				 function(data) {
				   jQuery("#<portlet:namespace/>wbsCodeTableList").html(data);
				   jQuery(".loading").fadeOut( "fast" );
				   jQuery("#<portlet:namespace/>wbsCodeTableList").fadeIn( "fast" );
				 }
			);
		}
 

 <portlet:namespace/>ompListing('${paginationURL}');
 </script>
</div>