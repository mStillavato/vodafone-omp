<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
	<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
		<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	
	
<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">  
<div id="statusMessage">
	<div>
		<font color="green">${successMSG}</font> <br/> <font color="red">${errorMSG}</font>
	</div>
		
		<c:if test="${not empty totalRecords }">
			<div class='portlet-msg-success' id="totalRecords">${totalRecords} records uploaded successfully</div>
		</c:if>
	</div>
<div style="display: none;" id="trackingStatusMessage">${statusMessage}</div>
<display:table decorator="com.hp.omp.decorator.TrackingCodeListDecorator" name="trackingCodeList" id="tracking" >
  <display:column title="Delete" class="deleteicon" >
  	<portlet:resourceURL var="deleteTrackingCode" id="deleteTrackingCode">
		<portlet:param name="trackingId" value="${tracking.trackingCodeId}"/>
	</portlet:resourceURL>
  	<a href="javascript:void(null);" onclick="<portlet:namespace/>deleteTracking('${deleteTrackingCode}')">delete</a>
  </display:column>
  <display:column title="Tracking Code" >
  	
	<portlet:renderURL var="editTrackingCode">
	    <portlet:param name="jspPage" value="/html/trackingcodeadmin/trackingform.jsp" />
	    <portlet:param name="trackingId" value="${tracking.trackingCodeId}" />
	</portlet:renderURL>
  	<a href="${editTrackingCode}">${tracking.trackingCodeName}</a>
  </display:column>
  <display:column property="trackingCodeDescription" title="Description" />
  <display:column property="projectName" title="Project" />
  <display:column property="subProjectName" title="Sub Proejct" />
  <display:column property="budgetCategoryName" title="Budget Category" />
  <display:column property="budgetSubCategoryName" title="Budget Sub Category" />
  <display:column property="macroDriverName" title="Macro Driver" />
  <display:column property="microDriverName" title="Micro Driver" />
  <display:column property="odnpName" title="ODNP Name" />
  <display:column property="wbsCodeName" title="WBS Code" />
  <display:column property="display" title="Display" />
  
</display:table>
<%@include file="/html/tablepaging/paging.jsp"%>