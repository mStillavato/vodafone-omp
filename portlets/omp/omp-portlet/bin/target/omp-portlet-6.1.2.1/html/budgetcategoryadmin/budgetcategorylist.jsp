<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<div>
	<font color="green">${successMSG}</font> <br/> <font color="red">${errorMSG}</font>
</div>

<display:table decorator="com.hp.omp.decorator.BudgetCategoryListDecorator" name="budgetCategoryList" list="budgetCategoryList" id="budgetCategory">
  <display:column property="budgetCategoryId" title="Delete" class="deleteicon" />
  <display:column  title="Name" >
  		
  	<portlet:renderURL var="editCostCenter">
	    <portlet:param name="jspPage" value="/html/costcenteradmin/edit.jspp" />
	    <portlet:param name="budgetCategoryId" value="${budgetCategory.budgetCategoryId}" />
	    <portlet:param name="action" value="update" />
	</portlet:renderURL>
	
  	<a href="${editCostCenter}">${budgetCategory.budgetCategoryName}</a>
  </display:column>
  <display:column property="display" title="Display" />
</display:table>
<%@include file="/html/tablepaging/paging.jsp"%>