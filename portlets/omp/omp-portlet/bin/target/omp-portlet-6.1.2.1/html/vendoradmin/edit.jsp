<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ page import="javax.portlet.PortletPreferences" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">  
<portlet:defineObjects />

<portlet:actionURL var="vendoradminURL">
<portlet:param name="jspPage" value="/html/vendoradmin/view.jsp" />
</portlet:actionURL>
<div class="adminform">
<aui:form method="post" action="<%=vendoradminURL%>">
<aui:input label="Supplier: " name="supplier" type="text" value="${supplier}">
<aui:validator name="required"/>
</aui:input>
<aui:input label="Supplier Code: " name="supplierCode" type="text" value="${supplierCode}">
<aui:validator name="required"/>
</aui:input> 
<aui:input name="action" type="hidden" value="${action}"/>

<c:if test="${not empty vendorId}">
<aui:input name="vendorId" type="hidden" value="${vendorId}"/>
</c:if>
<aui:input inlineLabel="top" label="Display" name="display" type="checkbox" checked="${display}" />
<aui:button type="submit" /> 
</aui:form> 
</div>