<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<div>
	<font color="green">${successMSG}</font> <br/> <font color="red">${errorMSG}</font>
</div>

<display:table decorator="com.hp.omp.decorator.VendorListDecorator" name="vendorList" list="vendorList" id="vendor">
  <display:column property="vendorId"  title="Delete" class="deleteicon" />
  <display:column  title="Name" >
  
   <portlet:renderURL var="editVendor">
	    <portlet:param name="jspPage" value="/html/vendoradmin/edit.jsp" />
	    <portlet:param name="vendorId" value="${vendor.vendorId}" />
	    <portlet:param name="action" value="update" />
	</portlet:renderURL>
	
  	<a href="${editVendor}">${vendor.vendorName}</a>
  
  </display:column>
  <display:column property="supplier" title="Supplier" />
  <display:column property="supplierCode" title="SupplierCode" />
    <display:column property="display" title="Display" />
  
</display:table>
<%@include file="/html/tablepaging/paging.jsp"%>