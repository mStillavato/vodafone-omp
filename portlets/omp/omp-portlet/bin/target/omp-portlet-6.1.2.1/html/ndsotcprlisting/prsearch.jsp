<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<portlet:resourceURL id="getSubProjects" var="getSubProjectUrl"/>
<script type="text/javascript">
function <portlet:namespace/>updateSelectOptions(parentId,childId){
	$("#"+childId+" option").remove();
	$("#"+childId).append("<option selected value=''>Select Sub Project..</option>");
	var selectedProject = $("#"+parentId+" option:selected").val();
	$.getJSON('${getSubProjectUrl}',"project="+selectedProject,function(data) {
			if(data.subProjects.length > 0){
				for(var i=0;i<data.subProjects.length;i++){
					var subProject = data.subProjects[i];
					$("#"+childId).append("<option value='"+subProject.id+"'>"+subProject.name+"</option>");
				}
					$("#"+childId).prop('disabled',false);
						$("#"+childId).data('combobox').refresh();
					$(".combobox-container .subprojectcombo").prop('disabled',false);
					$(".subprojctdiv input").val("");
					$(".subprojctdiv .add-on").css("display","inline-block");
					
			}else{
				$("#"+childId).prop('disabled',true);
				$("#"+childId).data('combobox').refresh();
				$(".combobox-container .subprojectcombo").prop('disabled',true);
				$(".subprojctdiv input").val("");
				$(".subprojctdiv .add-on").css("display","none");
			}
    	});
}
</script>
<portlet:defineObjects />
<li class="searcharea">
	<div class="advancedsearchcontainer"><a href="javascript:void(0)" data-toggle="popover" data-placement="left" data-content="" class="advancedsearchlink">Advanced Search</a>
	<div class="advancedsearchcontent" id="searchpopover">
	
<form method="post" id="advanced">

<portlet:resourceURL var="searchURL"/>

 <div class="advancedsearchform">
 
 <div class="formItem">
	<div class="formLabel"><label>Cost Center</label></div>
	<div class="clear"></div>
	<div class="formField"><select name="costCenter" id="<portlet:namespace/>costCenter" class="combobox" <c:if test="${(userType =='ENG' ||userType =='OBSERVER')}">disabled="disabled"</c:if>>
		<option selected value="">Select Cost Center..</option>
		<c:forEach items="${costCentersList}" var="costCenter">
		<c:choose>
			<c:when test="${costCenter.costCenterId eq userCostCenterId}">
				<option selected value="${costCenter.costCenterId}">${costCenter.costCenterName}</option>
			</c:when>
			<c:otherwise>
				<option value="${costCenter.costCenterId}">${costCenter.costCenterName}</option>
			</c:otherwise>
			</c:choose>
			
		</c:forEach>
	</select></div>
	<div class="clear"></div>
	</div>
	
  <div class="formItem">
 <div class="formLabel"><label>OMP Request #</label></div>
 <div class="clear"></div>
 <div class="formField"><input name="prNumber" id="<portlet:namespace/>prNumber" placeholder="OMP Request Number..." type="text"/></div>
 <div class="clear"></div>
 </div>
 
 <div class="clear"></div>
 
 <div class="formItem">
 <div class="formLabel"><label>Fiscal Year</label></div>
 <div class="clear"></div>
 <div class="formField"><input name="fYear" id="<portlet:namespace/>fYear" type="text" placeholder="Fiscal Year..."/></div>
 <div class="clear"></div>
 </div>
 

 
 <div class="formItem">
 <div class="formLabel"><label>Project</label></div>
 <div class="clear"></div>
 <div class="formField">
 <select name="project" class="combobox" id="<portlet:namespace/>project" onchange='<portlet:namespace/>updateSelectOptions("<portlet:namespace/>project","<portlet:namespace/>subProject")'>
	<option selected value="">Select Project ..</option>
	<c:forEach items="${allProjects}" var="project">
		<option value="${project.projectId}">${project.projectName}</option>
	</c:forEach>
	</select>
 </div>
 <div class="clear"></div>
 </div>
 
 <div class="clear"></div>
 
 <div class="formItem">
	 <div class="formLabel"><label>Sub Project</label></div>
	 <div class="clear"></div>
	 <div class="formField subprojctdiv">
	 <select class="combobox subprojectcombo" name="subProject" id='<portlet:namespace/>subProject'  <c:if test="${allSubProjects eq null}">disabled="disabled"</c:if> >
		<option selected value="">Select Sub Project..</option>
		<c:forEach items="${allSubProjects}" var="subProject">
			<option value="${subProject.subProjectId}">${subProject.subProjectName}</option>
		</c:forEach>
		</select>
	 </div>
	 <div class="clear"></div>
  </div>
  
 
 
 <div class="formItem">
 <div class="formLabel"><label>WBS Code</label></div>
 <div class="clear"></div>
 <div class="formField">
 <select name="wbsCode" id="<portlet:namespace/>wbsCode" class="combobox">
		<option selected value="">Select WBS Code ..</option>
		<c:forEach items="${allWbsCodes}" var="wbscode">
			<c:choose>
				<c:when test="${wbscode.wbsCodeId eq searchDTO.wbsCode}">
					<option selected value="${wbscode.wbsCodeId}">${wbscode.wbsCodeName}</option>
				</c:when>
				<c:otherwise>
					<option value="${wbscode.wbsCodeId}">${wbscode.wbsCodeName}</option>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</select>
 </div>
 <div class="clear"></div>
  </div>
  <div class="clear"></div>
  
 <input type="button" value="Search" class="submitbtn clssrch" onclick="<portlet:namespace/>search('${searchURL}','false')"/>
 <div class="clear"></div>
 
 </div>

</form>
</div>
	
	</div>

	<div class="normalsearchcontainer">
		<form method="post" id="global">
			<input name="searchKeyWord" id="<portlet:namespace/>searchKeyWord" type="text" placeholder="Search..."/>
			<input type="button" value="Submit" class="submitbtn" onclick="<portlet:namespace/>search('${searchURL}','true')"/>
		</form> 
	</div>


  
	<div class="clear"></div>
</li>





