<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
	<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">  
<div id="statusMessage">
	
	<div>
		<font color="green">${successMSG}</font> <br/> <font color="red">${errorMSG}</font>
	</div>
	</div>
<div style="display: none;" id="buyerStatusMessage">${statusMessage}</div>
<display:table name="buyerList" id="buyer" >
  <display:column title="Delete" class="deleteicon" >
  	<portlet:resourceURL var="deleteBuyer" id="deleteBuyer">
		<portlet:param name="buyerId" value="${buyer.buyerId}"/>
	</portlet:resourceURL>
  	<a href="javascript:void(null);" onclick="<portlet:namespace/>deleteBuyer('${deleteBuyer}')">delete</a>
  </display:column>
  <display:column title="Name" >
  	
	<portlet:renderURL var="editBuyer">
	    <portlet:param name="jspPage" value="/html/buyeradmin/buyerform.jsp" />
	    <portlet:param name="buyerId" value="${buyer.buyerId}" />
	</portlet:renderURL>
	
  	<a href="${editBuyer}">${buyer.name}</a>
  </display:column>
  <display:column property="display" title="Display" />
  
</display:table>
<%@include file="/html/tablepaging/paging.jsp"%>