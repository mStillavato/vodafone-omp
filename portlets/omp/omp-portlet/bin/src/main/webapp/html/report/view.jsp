<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<portlet:defineObjects />
	<%@ taglib uri="http://hp.com/vfItaly/jsp/taglib" prefix="hp" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<liferay-theme:defineObjects />
<portlet:defineObjects />

<portlet:actionURL name="generateReport" var="generateReport" />
	
    
    <div class="clear"></div>
<div class="formFrame" >
<div class=" titleHolder">
	   <div class="blockTitle"><label >Generated Report</label></div></div>
<form action="" method="post" id="reportForm">
<div id="reportFailureMSG"></div>
<div class="formItem">
	<div class="formLabel"><label >Fiscal Year</label></div>
	<div class="formField"><select name="fiscalYear" id="fiscalYear" class="combobox">
	<option selected value="">Select Fiscal Year..</option>
		<c:forEach items="${reportDTO.allFiscalYears}" var="fiscalYear">
			<c:choose>
				<c:when test="${fiscalYear eq reportDTO.fiscalYear}">
					<option selected value="${fiscalYear}">${fiscalYear}</option>
				</c:when>
				<c:otherwise>
					<option value="${fiscalYear}">${fiscalYear}</option>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</select></div>
	
</div>	
<div class="formItem">	
	<div class="formLabel"><label >WBS Code</label></div>

 	<div class="formField"><select name="wbsCode" id="wbsCode" class="combobox">
		<option selected value="">Select WBS Code ..</option>
		<c:forEach items="${reportDTO.allWbsCodes}" var="wbscode">
			<c:choose>
				<c:when test="${wbscode.wbsCodeId eq reportDTO.wbsCode}">
					<option selected value="${wbscode.wbsCodeId}">${wbscode.wbsCodeName}</option>
				</c:when>
				<c:otherwise>
					<option value="${wbscode.wbsCodeId}">${wbscode.wbsCodeName}</option>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</select>
	</div>
</div>
<div class="formItem">
	
	<div class="formLabel"><label>Project</label></div> 
	<div class="formField"><select class="combobox" name="project" id='<portlet:namespace/>projects'  
			onchange='<portlet:namespace/>updateSelectOptions("<portlet:namespace/>projects","<portlet:namespace/>subProjects")'>
		<option selected value="">Select Project..</option>
		<c:forEach items="${reportDTO.allProjects}" var="project">
			<c:choose>
				<c:when test="${project.projectId eq reportDTO.projectId}">
					<option selected value="${project.projectId}">${project.projectName}</option>
				</c:when>
				<c:otherwise>
					<option value="${project.projectId}">${project.projectName}</option>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</select></div>
	
</div>

<div class="formItem">
	<div class="formLabel"><label>Sub Project</label></div>
	
	<div class="formField"><select class="subprojectcombo combobox" name="subProject" id='<portlet:namespace/>subProjects' <c:if test="${reportDTO.allSubProjects eq null}">disabled="disabled"</c:if> >
		<option selected value="">Select Sub Project..</option>
		<c:forEach items="${reportDTO.allSubProjects}" var="subProject">
			<c:choose>
				<c:when test="${subProject.subProjectId eq reportDTO.subProjectId}">
					<option selected value="${subProject.subProjectId}">${subProject.subProjectName}</option>
				</c:when>
				<c:otherwise>
					<option value="${subProject.subProjectId}">${subProject.subProjectName}</option>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</select></div>
</div>

<div class="formItem">
	<div class="formLabel"><label>Cost Center</label></div>
	<div class="formField"><select name="costCenter" id="costCenter" class="combobox" <c:if test="${reportDTO.userType !='CONTROLLER' && reportDTO.userType !='NDSO' && reportDTO.userType !='TC' && reportDTO.userType !='ANTEX'}">disabled="disabled"</c:if>>
		<option selected value="">Select Cost Center..</option>
		<c:forEach items="${reportDTO.allCostCenters}" var="costcenter">
			<c:choose>
				<c:when test="${costcenter.costCenterId eq reportDTO.costCenterId}">
					<option selected value="${costcenter.costCenterId}">${costcenter.costCenterName}</option>
				</c:when>
				<c:otherwise>
					<option value="${costcenter.costCenterId}">${costcenter.costCenterName}</option>
				</c:otherwise>
			</c:choose>
					
		</c:forEach>
	</select></div>
	
	</div>
	
<div class="formItem">
	<div class="formLabel"><label>Currency</label></div>
	<div class="formField"><select id="<portlet:namespace/>currency-select" name="currency" class="combobox">
		<option value="">Select Currency..</option>
			<c:choose>
				<c:when test="${reportDTO.currency == '0'}">
					<option selected value="0" ${USD} >Dollar</option>
					<option value="1" ${EUR} >Euro</option>
				</c:when>
				<c:when test="${reportDTO.currency == '1'}">
					<option value="0" ${USD} >Dollar</option>
					<option selected value="1" ${EUR} >Euro</option>
				</c:when>
				<c:otherwise>
					<option value="0" ${USD} >Dollar</option>
					<option selected value="1" ${EUR} >Euro</option>
				</c:otherwise>
		</c:choose>
		
	</select>
	</div>
	</div>
	
	<c:choose>
			<c:when test="${reportDTO.userType == 'NDSO'}">
				<div class="formItem">
				<div class="formLabel"><label>I&C </label></div><div class="formField"><input value="I&C Field" disabled="disabled" /></div></div>
				</c:when>
				<c:when test="${reportDTO.userType == 'TC'}">
				<div class="formItem">
				<div class="formLabel"><label>I&C </label></div><div class="formField"><input value="I&C Test" disabled="disabled" /></div></div>
				</c:when>
				<c:otherwise/>
		</c:choose>
		
<div class="clearBoth"></div>
	<div class="btnContainer"><button type="button" class="submitbtn" value="Submit" onclick="getReport();">Submit</button></div>



</form>

</div>	
<div class="clear"></div>
<c:if test="${reportDTO.hasReport}">

    
    
<div class="formFrame reportForm" >
	<div class=" titleHolder">
	   <div class="blockTitle"><label >Generated Report</label></div></div>
	<div class="formItem">
	
	<div class="formLabel">Fiscal Year:</div> <div class="formTxt">${reportDTO.escapedFiscalYear}</div></div>
	<div class="formItem">
	
	<div class="formLabel">Project: </div><div class="formTxt">${reportDTO.escapedProjectName}</div></div>
	<div class="formItem">
	
	<div class="formLabel">Sub Project: </div> <div class="formTxt">${reportDTO.escapedSubProjectName}</div></div>
	<div class="formItem">
	
	<div class="formLabel">WBS Code:</div>
	 <div class="formTxt" >${reportDTO.escapedWbsCodeName}</div></div>
	<div class="formItem">
	
	<div class="formLabel">Cost Center: </div><div class="formTxt">${reportDTO.escapedCostCenter}</div></div>
	
	<c:choose>
			<c:when test="${reportDTO.currency == '0'}">
				<div class="formItem">
				<div class="formLabel">Currency: </div><div class="formTxt">Dollar</div></div>
				</c:when>
				<c:otherwise>
				<div class="formItem">
				<div class="formLabel">Currency: </div><div class="formTxt">Euro</div></div>
				</c:otherwise>
		</c:choose>
		
		<c:choose>
			<c:when test="${reportDTO.userType == 'NDSO'}">
				<div class="formItem">
				<div class="formLabel">I&C: </div><div class="formTxt">I&C Field</div></div>
				</c:when>
				<c:when test="${reportDTO.userType == 'TC'}">
				<div class="formItem">
				<div class="formLabel">I&C: </div><div class="formTxt">I&C Test</div></div>
				</c:when>
				<c:otherwise/>
		</c:choose>
	
	<div class="clearBoth"></div>
	<br/>
	<div id="<portlet:namespace/>usd-report" style="display: block;"> 
	<div class="valueReport"> 
		<div class="reportTitle">Closed Value </div>
	 	<div class="reportValue">${reportDTO.closedUSD}  </div>
	 	<div class="reportSign">$</div>
	 </div>
	<div class="valueReport">
		<div class="reportTitle">Closing Value</div>
		<div class="reportValue">${reportDTO.closingUSD} </div>
		<div class="reportSign">$</div>
	</div>
	<div class="valueReport">
		<div class="reportTitle">Comitted Value</div>
		<div class="reportValue">${reportDTO.comittedUSD} </div>
		<div class="reportSign">$</div>
	</div>
	<div class="valueReport">
		<div class="reportTitle">PO Total Value</div>
		<div class="reportValue">${reportDTO.totalUSD} </div>
		<div class="reportSign">$</div>
	</div>
	
	<div class="valueReport">
		<div class="reportTitle">PR Total Value</div>
		<div class="reportValue">${reportDTO.prTotalUSD} </div>
		<div class="reportSign">$</div>
	</div>
	
	<div class="valueReport">
		<div class="reportTitle">Total Value</div>
		<div class="reportValue">${reportDTO.aggregateTotalUSD} </div>
		<div class="reportSign">$</div>
	</div>
	
	</div>
	<div id="<portlet:namespace/>eur-report" style="display: none;"> 
	<div class="valueReport">
		<div class="reportTitle">Closed Value</div>
		<div class="reportValue"> ${reportDTO.closedEUR} </div>
		<div class="reportSign"> &#128; </div>
	</div>
	<div class="valueReport">
		<div class="reportTitle">Closing Value</div>
		<div class="reportValue"> ${reportDTO.closingEUR} </div>
		<div class="reportSign">&#128;</div>
	</div>
	<div class="valueReport">
		<div class="reportTitle">Comitted Value</div>
		<div class="reportValue"> ${reportDTO.comittedEUR} </div>
		<div class="reportSign">&#128;</div>
	</div>
	<div class="valueReport">
		<div class="reportTitle">PO Total Value</div>
		<div class="reportValue"> ${reportDTO.totalEUR}  </div>
		<div class="reportSign"> &#128; </div>
	</div>
	
	<div class="valueReport">
		<div class="reportTitle">PR Total Value</div>
		<div class="reportValue">${reportDTO.prTotalEUR} </div>
		<div class="reportSign">&#128;</div>
	</div>
	
	<div class="valueReport">
		<div class="reportTitle">Total Value</div>
		<div class="reportValue">${reportDTO.aggregateTotalEUR} </div>
		<div class="reportSign">&#128;</div>
	</div>
	
	</div>	
	
	<div class="clearBoth"></div><br/>
	<div class="linedBreak"></div>
	<div class="btnContainer">
	
	<button class="exportStyle" onclick="window.print()">Print</button>
	<button class="exportStyle" id='<portlet:namespace/>exportButton' onclick='<portlet:namespace/>exportReport("reportForm");'>Export Excel File</button> 
	<c:if test="${reportDTO.prReport}">
		<button class="exportStyle" id='<portlet:namespace/>exportDetailsButton' onclick='<portlet:namespace/>exportDetailsReport("reportForm");'>Export Details Excel File</button>
	</c:if>
	<div class="clear"></div> 
	
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
</c:if>
<portlet:resourceURL id="getSubProjects" var="getSubProjectUrl"/>
<portlet:resourceURL id="exportReport" var="exportReportUrl"/>
<portlet:resourceURL id="exportDetailsReport" var="exportDetailsReportUrl"/>

<script type="text/javascript"> 
function <portlet:namespace/>updateSelectOptions(parentId,childId){
	$("#"+childId+" option").remove();
	$("#"+childId).append("<option selected value=''>Select Sub Project..</option>");
	var selectedProject = $("#"+parentId+" option:selected").val();
	$.getJSON('${getSubProjectUrl}',"project="+selectedProject,function(data) {
			if(data.subProjects.length > 0){
				for(var i=0;i<data.subProjects.length;i++){
					var subProject = data.subProjects[i];
					$("#"+childId).append("<option value='"+subProject.id+"'>"+subProject.name+"</option>");
				}
					$("#"+childId).prop('disabled',false);
					$("#"+childId).data('combobox').refresh();
					$(".combobox-container .subprojectcombo").prop('disabled',false);
					$(".subprojctdiv input").val("");
					$(".subprojctdiv .add-on").css("display","inline-block");
			}else{
				$("#"+childId).prop('disabled',true);
				$("#"+childId).data('combobox').refresh();
				$(".combobox-container .subprojectcombo").prop('disabled',true);
				$(".subprojctdiv input").val("");
				$(".subprojctdiv .add-on").css("display","none");
			}
    	});
}

function <portlet:namespace/>toggleCurrency(id){	
	$("#<portlet:namespace/>usd-report").hide();
	$("#<portlet:namespace/>eur-report").hide();
		if(id == "0") {
			$("#<portlet:namespace/>usd-report").show();
		}else if(id == "1"){
			$("#<portlet:namespace/>eur-report").show();
		}

}

function <portlet:namespace/>exportReport(formId){
	var formData = $("#"+formId).serialize();
	var url = '${exportReportUrl}'+'&'+formData;
	window.location = url;
}
<c:if test="${reportDTO.prReport}">
	function <portlet:namespace/>exportDetailsReport(formId){
		var formData = $("#"+formId).serialize();
		var url = '${exportDetailsReportUrl}'+'&'+formData;
		window.location = url;
	}
</c:if>
//show currency block
<portlet:namespace/>toggleCurrency("${reportDTO.currency}");




function getReport(){
	var actionURL;
	actionURL='${generateReport}';
	if(validateOneSelectedListAtLeast()){
			document.getElementById("reportForm").action=actionURL;
			document.getElementById("reportForm").submit();
			
	} else{
		document.getElementById("reportFailureMSG").innerHTML='<div class="alert alert-danger" style="margin:0 20px 20px; color:red;">You should select at least one of the following lists </div>';
	}
	
}

function validateOneSelectedListAtLeast(){
	
	var result = false;
	
	var fYear=document.getElementById("fiscalYear").value;
	var wCode=document.getElementById("wbsCode").value;
	var proj=document.getElementById("<portlet:namespace/>projects").value;
	var sProject=document.getElementById("<portlet:namespace/>subProjects").value;
	var cCenter=document.getElementById("costCenter").value;
	var currency=document.getElementById("<portlet:namespace/>currency-select").value;
	
	//alert('fYear '+ fYear+'  wCode '+ wCode +'  proj '+proj +' sProject '+sProject + ' cCenter '+cCenter +' currency '+currency);
	
	if(currency !="" || fYear !="" || wCode !="" || proj !="" || sProject !="" || cCenter !=""){

		result = true;
	} 
	return result;
}


</script>

