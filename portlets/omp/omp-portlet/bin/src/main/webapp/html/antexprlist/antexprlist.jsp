<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

<portlet:resourceURL id="listPR" var="listPRURL" >
</portlet:resourceURL>


 
    <c:if test="${actionStatus=='PR/SC Received'}">
	<div class="listingtoplinksarea">
 <a href="javascript:void(null);" onclick="addPurchaseOrder()"  class="listingtoplink">Add PO</a>
 <div class="clear"></div>
</div>
 <div class="clear"></div>
 </c:if>
   <ul class="nav nav-tabs" id="myTab">
 <c:if test="${actionStatus=='PR/SC Received' ||empty(actionStatus)}">
 <div class="filter">Filtered by</div>
 <li class="active"><a href="javascript:void(null)" onclick="<portlet:namespace/>tabSelected('${listPRURL}','${actionStatus}',false)" data-toggle="tab">Manual Requests</a>  <div class="nav-tabs-arrow"></div></li>

 <li ><a href="javascript:void(null)" onclick="<portlet:namespace/>tabSelected('${listPRURL}','${actionStatus}',true)" data-toggle="tab">Automatic Requests</a><div class="nav-tabs-arrow"></div></li>
 </c:if>
<%@include file="/html/purchaserequestlist/prsearch.jsp"%>
 <div class="clear"></div>
</ul>

 
 
 <div id="<portlet:namespace/>prListDiv" class="listingcontainer"></div>
<div class="loading" style='display:none;font-size:30px; color:#eee;margin-top:60px; text-align: center;'>Loading...</div>
<script type="text/javascript">
var <portlet:namespace/>actionStatus ='${actionStatus}';
var <portlet:namespace/>automatic =false;
var <portlet:namespace/>idOrder = "desc"; 
var <portlet:namespace/>requesterOrder = "desc"; 
var <portlet:namespace/>recieverOrder = "desc"; 
var <portlet:namespace/>vendorOrder = "desc"; 
var <portlet:namespace/>subCategoryOrder = "desc"; 
var <portlet:namespace/>totalValueOrder = "desc"; 
var <portlet:namespace/>activityDescriptionOrder = "desc"; 
var <portlet:namespace/>statusOrder = "desc";
var <portlet:namespace/>costCenterNameOrder = "desc";
var <portlet:namespace/>fiscalYearOrder = "desc";
var <portlet:namespace/>orderby = "purchaseRequestId"; 
var <portlet:namespace/>order = "desc"; 
var <portlet:namespace/>isMini = "true";



<portlet:namespace/>ompListing('${listPRURL}');
function <portlet:namespace/>ompListing(url){
	
	var prId = jQuery("#<portlet:namespace/>prNumber").val();
	var costCenter = jQuery("#<portlet:namespace/>costCenter").val();
	var prStatus = jQuery("#<portlet:namespace/>prStatus").val();
	var fYear = jQuery("#<portlet:namespace/>fYear").val();
	var project = jQuery("#<portlet:namespace/>project").val();
	var subProject = jQuery("#<portlet:namespace/>subProject").val();
	var wbsCode = jQuery("#<portlet:namespace/>wbsCode").val();
	var searchKeyWord = jQuery("#<portlet:namespace/>searchKeyWord").val();
	
	
	jQuery("#<portlet:namespace/>prListDiv").fadeOut( "fast" );
	jQuery(".loading").fadeIn( "normal" );
	
	jQuery.post(url,{isMini:<portlet:namespace/>isMini,prId:prId,costCenter:costCenter,prStatus:prStatus,fYear:fYear,project:project,subProject:subProject,wbsCode:wbsCode,searchKeyWord:searchKeyWord,actionStatus:<portlet:namespace/>actionStatus,automatic:<portlet:namespace/>automatic,action:"listPR",orderby:<portlet:namespace/>orderby,order:<portlet:namespace/>order},
			 function(data) {
			 
			   jQuery("#<portlet:namespace/>prListDiv").html(data);
			   jQuery(".loading").fadeOut( "fast" );
			   jQuery("#<portlet:namespace/>prListDiv").fadeIn( "fast" );
			 }
		);
	}
	
	function <portlet:namespace/>prSearch(url){
		
		var searchPrValue = jQuery("#<portlet:namespace/>searchPr").val();
		jQuery("#<portlet:namespace/>prListDiv").fadeOut( "fast" );
		jQuery(".loading").fadeIn( "normal" );
		jQuery.post(url,{searchPrValue:searchPrValue},
				 function(data) {
				   jQuery("#<portlet:namespace/>prListDiv").html(data);
				   jQuery(".loading").fadeOut( "fast" );
				   jQuery("#<portlet:namespace/>prListDiv").fadeIn( "fast" );
				 }
			);
		}
	
function <portlet:namespace/>search(url,isMini){
	if(isMini=="false"){
		jQuery("#<portlet:namespace/>searchKeyWord").val("");
	}
	<portlet:namespace/>isMini=isMini;
	<portlet:namespace/>ompListing(url);
}

function <portlet:namespace/>tabSelected(url,actionStatus,automatic){
	<portlet:namespace/>actionStatus = actionStatus;
	<portlet:namespace/>automatic = automatic;
	<portlet:namespace/>ompListing(url);
}

function orderRequesterPRList(orderby){
	<portlet:namespace/>orderby = orderby.id;
	if(orderby.id == "purchaseRequestId"){
		if (<portlet:namespace/>idOrder == "desc") {
			<portlet:namespace/>order="asc"; <portlet:namespace/>idOrder = "asc"; 
		} else {
			<portlet:namespace/>order="desc";<portlet:namespace/>idOrder = "desc";
		}
	} else if(orderby.id == "screenNameRequester"){
		if (<portlet:namespace/>requesterOrder == "desc") {
			<portlet:namespace/>order="asc"; <portlet:namespace/>requesterOrder = "asc";
		} else {
			<portlet:namespace/>order="desc"; <portlet:namespace/>requesterOrder = "desc";
		}
	} else if(orderby.id == "screenNameReciever"){
		if (<portlet:namespace/>recieverOrder == "desc") {
			<portlet:namespace/>order="asc"; <portlet:namespace/>recieverOrder = "asc";
		} else {
			<portlet:namespace/>order="desc"; <portlet:namespace/>recieverOrder = "desc";
		}
	}else if(orderby.id == "vendorName") {
		if (<portlet:namespace/>vendorOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>vendorOrder = "asc";
		} else {
			<portlet:namespace/>order="desc"; <portlet:namespace/>vendorOrder = "desc";	
		}
	}else if(orderby.id == "budgetSubCategoryName") {
		if (<portlet:namespace/>subCategoryOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>subCategoryOrder="asc";
		}else {
			<portlet:namespace/>order="desc";<portlet:namespace/>subCategoryOrder="desc";
		}
	}else if(orderby.id == "totalValue") { 
		if (<portlet:namespace/>totalValueOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>totalValueOrder = "asc";
		} else {
			<portlet:namespace/>order="desc";<portlet:namespace/>totalValueOrder = "desc";
		}
	}else if(orderby.id == "activityDescription") { 
		if (<portlet:namespace/>activityDescriptionOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>activityDescriptionOrder = "asc";
		} else {
			<portlet:namespace/>order="desc";<portlet:namespace/>activityDescriptionOrder = "desc";
		}
	}else if(orderby.id == "status") { 
		if (<portlet:namespace/>statusOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>statusOrder = "asc";
		} else {
			<portlet:namespace/>order="desc";<portlet:namespace/>statusOrder = "desc";
		}
	}else if(orderby.id == "costCenterName") { 
		if (<portlet:namespace/>costCenterNameOrder == "desc") {
			<portlet:namespace/>order="asc";<portlet:namespace/>costCenterNameOrder = "asc";
		} else {
			<portlet:namespace/>order="desc";<portlet:namespace/>costCenterNameOrder = "desc";
		}
	}else if(orderby.id == "fiscalYear") { 
			if (<portlet:namespace/>fiscalYearOrder == "desc") {
				<portlet:namespace/>order="asc";<portlet:namespace/>fiscalYearOrder = "asc";
			} else {
				<portlet:namespace/>order="desc";<portlet:namespace/>fiscalYearOrder = "desc";
			}
	}
	<portlet:namespace/>ompListing('${listPRURL}');
}

</script>
