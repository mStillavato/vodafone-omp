<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<div>
	<font color="green">${successMSG}</font> <br/> <font color="red">${errorMSG}</font>
</div>

<display:table decorator="com.hp.omp.decorator.ProjectListDecorator" name="projectList" list="projectList" id="project">
  <display:column property="projectId" title="Delete" class="deleteicon"/>
  <display:column  title="Name" >
  
  
   <portlet:renderURL var="editproject">
	    <portlet:param name="jspPage" value="/html/projectadmin/edit.jsp" />
	    <portlet:param name="projectId" value="${project.projectId}" />
	    <portlet:param name="action" value="update" />
	</portlet:renderURL>
	
  	<a href="${editproject}">${project.projectName}</a>
  
  
  </display:column>
  <display:column property="description" title="Description" />
    <display:column property="display" title="Display" />
  
</display:table>
<%@include file="/html/tablepaging/paging.jsp"%>