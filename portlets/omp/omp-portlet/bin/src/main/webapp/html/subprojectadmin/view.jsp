<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<script type="text/javascript" src="../../omp_main-theme/js/jquery-1.10.2.min.js"></script>   
<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">   
<portlet:defineObjects />

<div id="<portlet:namespace/>subProjectTableList"  class="listingcontainer"></div>
<div class="loading" style='display:none;font-size:30px; color:#eee;margin-top:60px; text-align: center;'>Loading...</div>
<div>
<portlet:renderURL var="addSubProjectURL">
    <portlet:param name="jspPage" value="/html/subprojectadmin/edit.jsp" />
</portlet:renderURL>

<p><a href="<%= addSubProjectURL %>&action=add"  class="actionbutn">add new Sub Project</a></p>

<portlet:resourceURL var="deleteSubProjectURL">
	<portlet:param name="action" value="delete"/>
</portlet:resourceURL>
<portlet:resourceURL var="paginationURL">
</portlet:resourceURL>
 <script type="text/javascript">
 function removeSubProject(subProjectId, subProjectName){
	 if(confirm("Do you want to delete this Sub Project ["+subProjectName+"]?")) {
		 var url="${deleteSubProjectURL}&subProjectId="+subProjectId+"&subProjectName="+subProjectName;
			jQuery.get(url,function( data ) {
				  jQuery( "#<portlet:namespace/>subProjectTableList" ).html( data );
				});
			}
 		}
 
 
 function <portlet:namespace/>ompListing(url){
		jQuery("#<portlet:namespace/>subProjectTableList").fadeOut( "fast" );
		jQuery(".loading").fadeIn( "normal" );
		jQuery.post(url+"&action=pagination",
				 function(data) {
				   jQuery("#<portlet:namespace/>subProjectTableList").html(data);
				   jQuery(".loading").fadeOut( "fast" );
				   jQuery("#<portlet:namespace/>subProjectTableList").fadeIn( "fast" );
				 }
			);
		}
 

 
 <portlet:namespace/>ompListing('${paginationURL}');
 </script>
</div>