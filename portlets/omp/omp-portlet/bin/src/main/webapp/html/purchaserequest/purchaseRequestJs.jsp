<%@ page import="com.hp.omp.model.TrackingCodes" %>
<%@ page import="java.util.List" %>
<%@ page import="com.hp.omp.service.TrackingCodesLocalServiceUtil" %>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
	<%@ taglib uri="http://hp.com/vfItaly/jsp/taglib" prefix="hp" %>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
	<script src="<%=request.getContextPath()%>/js/upload Ajax/ajaxfileupload.js"></script>
	<portlet:resourceURL var="addPosition" id="addPosition" escapeXml="false" >
	<portlet:param name="action" value="addPosition"/>
	</portlet:resourceURL>
	<portlet:resourceURL var="updatePosition" id="updatePosition" escapeXml="false" >
	<portlet:param name="action" value="updatePosition"/>
	</portlet:resourceURL>
	<portlet:resourceURL var="deletePosition" id="deletePosition" escapeXml="false" >
	<portlet:param name="action" value="deletePosition"/>
	</portlet:resourceURL>
	
	<portlet:resourceURL var="approvePosition" id="approvePosition" escapeXml="false" >
	<portlet:param name="action" value="approvePosition"/>
	</portlet:resourceURL>
	
	<portlet:resourceURL var="addPurchaseRequest" id="addPurchaseRequest" escapeXml="false" >
	<portlet:param name="action" value="addPurchaseRequest"/>
	</portlet:resourceURL>
	<portlet:resourceURL var="updatePurchaseRequest" id="updatePurchaseRequest" escapeXml="false" >
	<portlet:param name="action" value="updatePurchaseRequest"/>
	</portlet:resourceURL>
	
	<portlet:actionURL name="submitPurchaseRequest"  var="submitPurchaseRequestURL" >
	</portlet:actionURL>
	<portlet:actionURL name="deletePurchaseRequest" var="deletePurchaseRequestURL" >
	<portlet:param name="redirect" value="/html/purchaseRequest/landing.jsp" />
	</portlet:actionURL>
	<portlet:actionURL name="startPurchaseRequest" var="startPurchaseRequestURL" >
	</portlet:actionURL>
	
	<portlet:actionURL name="rejectPurchaseRequest" var="rejectPurchaseRequestURL" >
	</portlet:actionURL>
	
	<portlet:resourceURL id="downloadFile" var="downloadFileURL" >
	<portlet:param name="action" value="downloadFile"/>
	</portlet:resourceURL>
	
	<portlet:resourceURL id="deleteFile" var="deleteFileURL" >
	<portlet:param name="action" value="deleteFile"/>
	</portlet:resourceURL>
	
	<portlet:resourceURL id="downloadallFiles" var="downloadAllFilesURL" >
	<portlet:param name="action" value="downloadAllFiles"/>
	</portlet:resourceURL>
	
	<portlet:resourceURL id="uploadFileURL" var="uploadFileURL">
	 <portlet:param name="action" value="uploadFile" />
	</portlet:resourceURL>
	
	<portlet:resourceURL id="listUploaded" var="listUploadedFilesURL">
	 <portlet:param name="action" value="listFiles" />
	</portlet:resourceURL>
	
	<portlet:resourceURL id="getSubProjects" var="getSubProjectUrl"/>
	<portlet:resourceURL id="getMicroDrivers" var="getMicroDriversUrl"/>
	
	<portlet:resourceURL id="poulateTrackingValues" var="poulateTrackingValuesUrl"/>
	
	<portlet:resourceURL id="getLabelNumber" var="getLabelNumberURL" >
	<portlet:param name="action" value="getLabelNumber"/>
	</portlet:resourceURL>
	
	
	<script type="text/javascript">
	
	var validatePos;
	function addUpdatepurchaseRequest(){
		
		resetRequiredFields();
		var purchaseRequestId=document.getElementById("purchaseRequestId").value;
		var automatic=document.getElementById("automatic").value;
		var costcenter=document.getElementById("costcenter").value;
		var ola=document.getElementById("ola").value;
		var buyer=document.getElementById("buyer").value;
		var vendor=document.getElementById("vendor").value;
		var projectname=document.getElementById("projectname").value;
		var subproject=document.getElementById("subproject").value;
		var budgetCategory=document.getElementById("budgetCategory").value;
		var budgetSubCategory=document.getElementById("budgetSubCategory").value;
		var macrodriver=document.getElementById("macrodriver").value;
		var microdriver=document.getElementById("microdriver").value;
		var odnpcode=document.getElementById("odnpcode").value;
		var odnpname=document.getElementById("odnpname").value;
		var totalvalue=document.getElementById("totalvalue").value;
		var prCurrency=document.getElementById("prCurrency").value;
		var activitydescription=document.getElementById("activitydescription").value;
		
		//CR for tracking code------------
		var trackingcode=document.getElementById("trackingcode").value;
		var prwbscode=document.getElementById("prwbscode").value;
		
		//document.getElementById("purchaseRequestId").value='';
		var resourceURL;
		
		if(purchaseRequestId !== null && purchaseRequestId !=""){
			resourceURL='${updatePurchaseRequest}';
			//alert("updatepurchaseRequest  "+resourceURL);
			
			var actionStatus=document.getElementById("actionStatus").value;
			var userType=document.getElementById("userType").value;
			if((actionStatus !="Created" && actionStatus !="Rejected") && userType =="CONTROLLER" ){
				if(!validatePR()){
					return false;
				}
				
			}
		}else{
			resourceURL='${addPurchaseRequest}';
			//alert("addpurchaseRequest "+resourceURL);
		}
	
		  jQuery.ajax({
			    type: "POST",
			    dataType: "json",
			    url: resourceURL,
			    data: {"purchaseRequestId":purchaseRequestId,"automatic":automatic,"costcenter":costcenter,"ola":ola,"buyer":buyer,"vendor":vendor,
			    	"projectname":projectname,"subproject":subproject,"budgetCategory":budgetCategory,"budgetSubCategory":budgetSubCategory,
			    	"macrodriver":macrodriver,"microdriver":microdriver,"odnpcode":odnpcode,"odnpname":odnpname,"totalvalue":totalvalue,
			    	"prCurrency":prCurrency,"activitydescription":activitydescription,"trackingcode":trackingcode,"prwbscode":prwbscode},
			    success: function(data) {
			    	//alert(data);
			    	if(data.status !="failure"){
			    			document.getElementById("purchaseRequestId").value=data.purchaseRequestID;
				    		document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-success">OMP Request # '+data.purchaseRequestID+'  Saved Successfully</div>';
				    		 var actionStatus=document.getElementById("actionStatus").value;
				    		 if(actionStatus !='Rejected'){
				    			document.getElementById("deleteBtnDiv").innerHTML='<button type="button" class="cancelbtn" onclick="deletepurchaseRequest();" value="delete">delete</button>';
				    		 }
			    		}
			    	else{
			    		 document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-danger">Your purchase Request failed to Save</div>';
			    	}
			    	
			    },
			    error : function(XMLHttpRequest, textStatus, errorThrown) 
			    {
			       document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-danger">Your purchase Request failed to Save</div>';
			    }
			    });
		
	}
	
	function validatePREntities(){
	
		var numberofPositions=document.getElementById("numberOfPositions").value;
		//alert("numberofPositions = "+numberofPositions);
		if(numberofPositions == "0"){
			alert("number of positions = 0  you should first add at least one position");
			return false;
					}	else{
						if(validateRequiredFields()){
							//alert("validation is ok will submit");
						return true;
						}else{
							//alert("validation error");
							 document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-danger">your purchase Request contains missing parameters complete fields in Red color</div>';
							 return false;
						}
					}
	}
	
	function submitpurchaseRequest(){
		var actionURL;
		actionURL='${submitPurchaseRequestURL}';
		
		if(validatePR()){
		document.getElementById("editpurchaseRequestForm").action=actionURL;
		document.getElementById("editpurchaseRequestForm").submit();
		}
	}
	
	function validatePR(){
		
		var automatic=$('#automatic').val();
		if(automatic=="true"){
			if($('#filesNumber').val() !=null && $('#filesNumber').val() !=''){
				return validatePREntities();
			}
			else{
				alert("your PR is automatic you should upload document before submit");
				return false;
			}
			
		}else{
			return validatePREntities();	
		}
	}
	
	function deletepurchaseRequest(){
		if(window.confirm("Are you sure you want to delete?you will delete the GRs,Positions,PO related to this PR")){
		document.getElementById("editpurchaseRequestForm").action='${deletePurchaseRequestURL}';
		document.getElementById("editpurchaseRequestForm").submit();
		}
	}
	
	function startPurchaseRequest(){
		document.getElementById("editpurchaseRequestForm").action='${startPurchaseRequestURL}';
		document.getElementById("editpurchaseRequestForm").submit();
	}
	
	function rejectPurchaseRequest(){
		var prForm=document.getElementById("editpurchaseRequestForm");
		
		 if((prForm.elements["rejectionCause"].value =="" || prForm.elements["rejectionCause"].value ==null)){
			 prForm.elements["rejectionCause"].style.borderColor="#FF0000";
			 document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-danger">your purchase Request contains missing parameters complete fields in Red color</div>';
			 
			 return false;
			} else{
			 prForm.elements["rejectionCause"].style.borderColor="";
			 document.getElementById("editpurchaseRequestForm").action='${rejectPurchaseRequestURL}';
			 document.getElementById("editpurchaseRequestForm").submit();
			}
		 
		
	}
	
	function cancelPR(){
		document.location.href ='/group/vodafone/home';
	}
	
	function savePRAddPositionBtn(){
		
		var purchaseRequestId=document.getElementById("purchaseRequestId").value;
		var automatic=document.getElementById("automatic").value;
		var costcenter=document.getElementById("costcenter").value;
		var ola=document.getElementById("ola").value;
		var buyer=document.getElementById("buyer").value;
		var vendor=document.getElementById("vendor").value;
		var projectname=document.getElementById("projectname").value;
		var subproject=document.getElementById("subproject").value;
		var budgetCategory=document.getElementById("budgetCategory").value;
		var budgetSubCategory=document.getElementById("budgetSubCategory").value;
		var macrodriver=document.getElementById("macrodriver").value;
		var microdriver=document.getElementById("microdriver").value;
		var odnpcode=document.getElementById("odnpcode").value;
		var odnpname=document.getElementById("odnpname").value;
		var totalvalue=document.getElementById("totalvalue").value;
		var prCurrency=document.getElementById("prCurrency").value;
		var activitydescription=document.getElementById("activitydescription").value;
		
		//CR for tracking code------------
		var trackingcode=document.getElementById("trackingcode").value;
		var prwbscode=document.getElementById("prwbscode").value;
		//document.getElementById("purchaseRequestId").value='';
		var resourceURL;
		
		if(purchaseRequestId !== null && purchaseRequestId !=""){
			resourceURL='${updatePurchaseRequest}';
			//alert("updatepurchaseRequest  "+resourceURL);
		}else{
			resourceURL='${addPurchaseRequest}';
			//alert("addpurchaseRequest "+resourceURL);
		}
	
		  jQuery.ajax({
			    type: "POST",
			    dataType: "json",
			    url: resourceURL,
			    data: {"purchaseRequestId":purchaseRequestId,"automatic":automatic,"costcenter":costcenter,"ola":ola,"buyer":buyer,"vendor":vendor,
			    	"projectname":projectname,"subproject":subproject,"budgetCategory":budgetCategory,"budgetSubCategory":budgetSubCategory,
			    	"macrodriver":macrodriver,"microdriver":microdriver,"odnpcode":odnpcode,"odnpname":odnpname,"totalvalue":totalvalue,
			    	"prCurrency":prCurrency,"activitydescription":activitydescription,"trackingcode":trackingcode,"prwbscode":prwbscode},
			    success: function(data) {
			    	//alert(data);
			    	if(data.status !="failure"){
			    			document.getElementById("purchaseRequestId").value=data.purchaseRequestID;
				    		document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-success">OMP Request # '+data.purchaseRequestID+'  Saved Successfully</div>';
				    		
				    			document.getElementById('positionFormContainer').style.display='block';
				    		document.getElementById('editPositionBtn').value='Add';
				    		 if(document.getElementById('deletePositionBtn') !=null){
				    		document.getElementById('deletePositionBtn').style.display='none';
				    		 }
				    		//resetPositionForm();
				    		 document.getElementById('labelNumber').value=10;
				    		 document.getElementById("labelNumberText").innerHTML=10;
				    		 var actionStatus=document.getElementById("actionStatus").value;
				    		 if(actionStatus !='Rejected'){
				    			document.getElementById("deleteBtnDiv").innerHTML='<button type="button" class="cancelbtn" onclick="deletepurchaseRequest();" value="delete">delete</button>';
				    		 }
			    		}
			    	else{
			    		 document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-danger">your purchase Request failed to Save</div>';
			    	}
			    	
			    },
			    error : function(XMLHttpRequest, textStatus, errorThrown) 
			    {
			       document.getElementById("purchaseRequestSuccessMSG").innerHTML="<div class='alert alert-danger'>your purchase Request failed to Save</div>";
			    }
			    });
		
	}
	
	function checkFileExist(){
		var fileSelected=document.getElementById("fileName").value;
		if(fileSelected=="" ||fileSelected==null){
			alert("please choose file first");
		}
		else{
			var automatic=document.getElementById("automatic").value;
			if(automatic=="true" && $('#filesNumber').val() !=null && $('#filesNumber').val() !=''){
				if(window.confirm("Are you sure you want to replace the file")){
			  	uploadFile();
				}
			}else{
			if($('#purchaseRequestId').val()=='' || $('#purchaseRequestId').val()==null){
				savePRUploadBtn();
			}else{
				uploadFile();
			}
			}
			
		}
	}
	function savePRUploadBtn(){
		var purchaseRequestId=document.getElementById("purchaseRequestId").value;
		var automatic=document.getElementById("automatic").value;
		var costcenter=document.getElementById("costcenter").value;
		var ola=document.getElementById("ola").value;
		var buyer=document.getElementById("buyer").value;
		var vendor=document.getElementById("vendor").value;
		var projectname=document.getElementById("projectname").value;
		var subproject=document.getElementById("subproject").value;
		var budgetCategory=document.getElementById("budgetCategory").value;
		var budgetSubCategory=document.getElementById("budgetSubCategory").value;
		var macrodriver=document.getElementById("macrodriver").value;
		var microdriver=document.getElementById("microdriver").value;
		var odnpcode=document.getElementById("odnpcode").value;
		var odnpname=document.getElementById("odnpname").value;
		var totalvalue=document.getElementById("totalvalue").value;
		var prCurrency=document.getElementById("prCurrency").value;
		var activitydescription=document.getElementById("activitydescription").value;
		
		//CR for tracking code------------
		var trackingcode=document.getElementById("trackingcode").value;
		var prwbscode=document.getElementById("prwbscode").value;
		//document.getElementById("purchaseRequestId").value='';
		var resourceURL;
		
		if(purchaseRequestId !== null && purchaseRequestId !=""){
			resourceURL='${updatePurchaseRequest}';
			//alert("updatepurchaseRequest  "+resourceURL);
		}else{
			resourceURL='${addPurchaseRequest}';
			//alert("addpurchaseRequest "+resourceURL);
		}
	
		  jQuery.ajax({
			    type: "POST",
			    dataType: "json",
			    url: resourceURL,
			    data: {"purchaseRequestId":purchaseRequestId,"automatic":automatic,"costcenter":costcenter,"ola":ola,"buyer":buyer,"vendor":vendor,
			    	"projectname":projectname,"subproject":subproject,"budgetCategory":budgetCategory,"budgetSubCategory":budgetSubCategory,
			    	"macrodriver":macrodriver,"microdriver":microdriver,"odnpcode":odnpcode,"odnpname":odnpname,"totalvalue":totalvalue,
			    	"prCurrency":prCurrency,"activitydescription":activitydescription,"trackingcode":trackingcode,"prwbscode":prwbscode},
			    success: function(data) {
			    	//alert(data);
			    	if(data.status !="failure"){
			    			document.getElementById("purchaseRequestId").value=data.purchaseRequestID;
				    		document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-success">OMP Request # '+data.purchaseRequestID+'  Saved Successfully</div>';
				    		uploadFile();
				    		 var actionStatus=document.getElementById("actionStatus").value;
				    		 if(actionStatus !='Rejected'){
				    			document.getElementById("deleteBtnDiv").innerHTML='<button type="button" class="cancelbtn" onclick="deletepurchaseRequest();" value="delete">delete</button>';
				    		 }
				    		
			    		}
			    	else{
			    		 document.getElementById("purchaseRequestSuccessMSG").innerHTML='<div class="alert alert-danger">your purchase Request failed to Save</div>';
			    	}
			    	
			    },
			    error : function(XMLHttpRequest, textStatus, errorThrown) 
			    {
			       document.getElementById("purchaseRequestSuccessMSG").innerHTML="<div class='alert alert-danger'>your purchase Request failed to Save</div>";
			    }
			    });
		
	}
	
	
	function addUpdatePosition(action) {
	
		var actionStatus=document.getElementById("actionStatus").value;
		
		var positionId=document.getElementById("positionId").value;
		var labelNumber=document.getElementById("labelNumber").value;
		var positionStatus=document.getElementById("positionStatus").value;
		var purchaseRequestId=document.getElementById("purchaseRequestId").value;
		var fiscalYear=document.getElementById("fiscalyear").value;
		var categoryCode=document.getElementById("categorycode").value;
		var offerNumber=document.getElementById("offernumber").value;
		var offerDate=document.getElementById("offerdate").value;
		var quantity=document.getElementById("quantity").value;
		var deliveryDate=document.getElementById("deliverydate").value;
		var deliveryAdress=document.getElementById("deliveryadress").value;
		//var wbscode=document.getElementById("wbscode").value;
		var locationEvo=document.getElementById("locationEVO").value;
		var assetNumber=document.getElementById("asset-number").value;
		var prcsNumber=document.getElementById("prcs-number").value;
		var poNumber=document.getElementById("po-number").value;
		var actualUnitCost=document.getElementById("actual-unit-cost").value;
		var positionDescription=document.getElementById("position-description").value;
		var iandc = document.getElementById("iandc").value;
		//var trackingCode = document.getElementById("trackingCode").value;
		
		
		//CR5.1
		var targaTecnica=document.getElementById("targaTecnica").value;
		
		var resourceURL;
		
		if(action =='approve'){
			resourceURL='${approvePosition}';	
		}
		else{
		if(action !='delete'){
		if(document.getElementById("userType").value =="ANTEX"){
		var automatic=document.getElementById("automatic").value;
		//alert("automatic ="+automatic);
		if(automatic=="true"){
			//alert("inside automatic "+automatic);
			var automaticReturn=true;
			if(actionStatus !="Created"){
				if(assetNumber=="" || assetNumber==null){
					 document.getElementById("positionErrorMSG").innerHTML='<div class="alert alert-danger">you should add asset,SC and PO numbers first</div>';
					 document.getElementById("asset-number").style.borderColor="#FF0000";
					 automaticReturn=false;
				}
				if(prcsNumber=="" || prcsNumber==null){
					 document.getElementById("positionErrorMSG").innerHTML='<div class="alert alert-danger">you should add asset,SC and PO numbers first</div>';
					 document.getElementById("prcs-number").style.borderColor="#FF0000";
					 automaticReturn=false;
				}
				if(poNumber=="" || poNumber==null){
					 document.getElementById("positionErrorMSG").innerHTML='<div class="alert alert-danger">you should add asset,SC and PO numbers first</div>';
					 document.getElementById("po-number").style.borderColor="#FF0000";
					 automaticReturn=false;
				}
				//alert("automaticReturn ="+automaticReturn);
				if(! automaticReturn){
					//alert("not automaticReturn ");
					return automaticReturn;
				}
			}
			
		}else{
			//alert("inside manual "+automatic);
		if(positionStatus =="NEW" && actionStatus=="PR Assigned"){
			//alert("enter the asset number");
			var missingAsset=false;
			var missingEvo=false;
			if(assetNumber=="" || assetNumber==null){
			 document.getElementById("asset-number").style.borderColor="#FF0000";
			 missingAsset=true;
			}else{
				 document.getElementById("asset-number").style.borderColor="";
			}
			if(locationEvo=="" || locationEvo==null){
				 document.getElementById("locationEVO").style.borderColor="#FF0000";
				 missingEvo=true;	
			}else{
				 document.getElementById("locationEVO").style.borderColor="";
			}
			if(missingAsset==true && missingEvo==true){
				 document.getElementById("positionErrorMSG").innerHTML='<div class="alert alert-danger">you should add asset number and EVO location</div>';
				return false;
			}else if(missingAsset==true){
				document.getElementById("positionErrorMSG").innerHTML='<div class="alert alert-danger">you should add asset number</div>';
				return false;
			}else if(missingEvo==true){
				document.getElementById("positionErrorMSG").innerHTML='<div class="alert alert-danger">you should add EVO location</div>';
				return false;
			}
		}
		if(positionStatus =="ASSET_RECEIVED" && (prcsNumber=="" || prcsNumber==null)){
			//alert("enter the prcs number");
			 document.getElementById("positionErrorMSG").innerHTML='<div class="alert alert-danger">you should add PR/CS number first</div>';
			 document.getElementById("prcs-number").style.borderColor="#FF0000";
			return false;
		}
		if(positionStatus =="SC_RECEIVED" && (poNumber=="" || poNumber==null)){
			//alert("enter the prcs number");
			 document.getElementById("positionErrorMSG").innerHTML='<div class="alert alert-danger">you should add PO number first</div>';
			 document.getElementById("po-number").style.borderColor="#FF0000";
			return false;
		}
		}
	}
		if(positionId !== null && positionId !=""){
			resourceURL='${updatePosition}';
			//alert("updatePosition  "+resourceURL);
		}else{
			resourceURL='${addPosition}';
			//alert("addPosition "+resourceURL);
		}
	}
	else{
		resourceURL='${deletePosition}';
		//alert("deletePosition "+resourceURL);
		}
		}
	        
	        
	    jQuery.ajax({
	    type: "POST",
	    url: resourceURL,
	    data: {"positionId":positionId,"positionStatus":positionStatus,"actionStatus":actionStatus,"labelNumber":labelNumber,"purchaseRequestId":purchaseRequestId,
	    	"fiscalyear":fiscalYear,"categorycode":categoryCode,"offernumber":offerNumber,"offerdate":offerDate,"quantity":quantity,
	    	"deliverydate":deliveryDate,"deliveryadress":deliveryAdress,"locationEvo":locationEvo,"asset-number":assetNumber,
	    	"prcs-number":prcsNumber,"po-number":poNumber,"actual-unit-cost":actualUnitCost,"position-description":positionDescription,
	    	"iandc":iandc, "targaTecnica":targaTecnica},
	    success: function(data) { 
		         $('#positionsContent').html(data);
		         
				closemodal();
				resetPositionForm();
	    }
	    });
		$('.modal').on('shown.bs.modal', function (e) {
  $('.tltp').tooltip();
});
		
	}
	
	
	function resetPositionForm() {
	    var frm_elements = document.getElementById("positionForm").elements;
	    for (i = 0; i < frm_elements.length; i++)
	    {
	        field_type = frm_elements[i].type.toLowerCase();
	        switch (field_type)
	        {
	        case "text":
	        case "password":
	        case "textarea":
	        case "hidden":
	            frm_elements[i].value = "";
	            break;
	        case "radio":
	        case "checkbox":
	            if (frm_elements[i].checked)
	            {
	                frm_elements[i].checked = false;
	            }
	            break;
	        case "select-one":
	        case "select-multi":
	            frm_elements[i].value ="";
	            break;
	        default:
	            break;
	        }
	        frm_elements[i].style.borderColor="";
	    }
	}
	function setPositionFormInAddMode(){
		var numberofPositions=document.getElementById("numberOfPositions").value;
		//alert("numberofPositions = "+numberofPositions);
		if(numberofPositions == "0")
			{
			//alert("number of positions = 0 will add first")
			savePRAddPositionBtn();
			}
		else{
		document.getElementById('positionFormContainer').style.display='block';
		document.getElementById('editPositionBtn').value='Add';
		 if(document.getElementById('deletePositionBtn') !=null){
			 document.getElementById('deletePositionBtn').style.display='none'; 
		 }
		 
		 resetPositionForm();
		 fillPositionFromLatest(numberofPositions-1);
		}
		 var num=parseInt(numberofPositions)+1;
			var labelNumber=num*10;
			document.getElementById("labelNumberText").innerHTML=labelNumber;
			document.getElementById('labelNumber').value=labelNumber;
			//$(".modal").appendTo($("body"));
			/////////////////////////////////////////////////////
			//var purchaseRequestId=$('#purchaseRequestId').val();
			//jQuery.post('${getLabelNumberURL}',{purchaseRequestId:purchaseRequestId},
				//function(data) {
			 	//	$('#labelNumberText').html(data);
			 	//	$('#labelNumber').val(data);
				// }
			//);
			
			//////////////////////////////////////////////////////
			
			$('.modal').on('shown.bs.modal', function (e) {
  $('.tltp').tooltip();
});
		
	}
	
	
	function hidePositionForm(){
		resetPositionForm();
		document.getElementById('positionFormContainer').style.display='none';
		
	}
	
	function fillPositionForm(i){
		
		
		
		document.getElementById('positionFormContainer').style.display='block';
		 if(document.getElementById('editPositionBtn') !=null)
		 {
			 document.getElementById('editPositionBtn').value='update';
		 }
		//document.getElementById('editPositionBtn').value='update';
		 document.getElementById("positionErrorMSG").innerHTML='';
		 document.getElementById("prcs-number").style.borderColor="";
		 if(document.getElementById('deletePositionBtn') !=null)
			 {
					document.getElementById('deletePositionBtn').style.display='inline-block';
			 }
		 
		 
		 var actionStatus=document.getElementById("actionStatus").value;
		 var userType=document.getElementById("userType").value;
		 var positionStatus=document.getElementById("positionStatus_tr"+i).value;
		 
		document.getElementById("positionId").value=document.getElementById("positionId_tr"+i).value;
		//alert(document.getElementById("positionId").id);
		document.getElementById("positionStatus").value=document.getElementById("positionStatus_tr"+i).value;
		document.getElementById("labelNumber").value=document.getElementById("labelNumber_tr"+i).value;
		 document.getElementById("labelNumberText").innerHTML=document.getElementById("labelNumber_tr"+i).value;
		//alert(document.getElementById("positionStatus").id);
		document.getElementById("fiscalyear").value=document.getElementById("fiscalyear_tr"+i).value;
		//alert(document.getElementById("fiscalyear").id);
		document.getElementById("categorycode").value=document.getElementById("categorycode_tr"+i).value;
		//alert(document.getElementById("categorycode").id);
		document.getElementById("offernumber").value=document.getElementById("offernumber_tr"+i).value;
		//alert(document.getElementById("offernumber").id);
		document.getElementById("offerdate").value=document.getElementById("offerdate_tr"+i).value;
		//alert(document.getElementById("offerdate").id);
		document.getElementById("quantity").value=document.getElementById("quantity_tr"+i).value;
		//alert(document.getElementById("quantity").id);
		document.getElementById("deliverydate").value=document.getElementById("deliverydate_tr"+i).value;
		//alert(document.getElementById("deliverydate").id);
		document.getElementById("deliveryadress").value=document.getElementById("deliveryadress_tr"+i).value;
		//alert(document.getElementById("deliveryadress").id);
		//document.getElementById("wbscode").value=document.getElementById("wbscode_tr"+i).value;
		//var wbscodeValue = document.getElementById("wbscode_tr"+i).value;
		//jQuery('#wbscode option:selected').removeAttr('selected');
		//jQuery('#wbscode').find('option:selected').removeAttr("selected");
		//jQuery("#wbscode :selected").prop('selected', false);
		//jQuery('#wbscode option[value="'+wbscodeValue+'"]').prop("selected",true);
		//$("#wbscode").data('combobox').refresh();
		//alert(document.getElementById("wbscode").id);
		document.getElementById("locationEVO").value=document.getElementById("locationEvo_tr"+i).value;
		//alert(document.getElementById("locationEVO").id);
		document.getElementById("asset-number").value=document.getElementById("asset-number_tr"+i).value;
		//alert(document.getElementById("asset-number").id);
		document.getElementById("prcs-number").value=document.getElementById("prcs-number_tr"+i).value;
		//alert(document.getElementById("prcs-number").id);
		document.getElementById("po-number").value=document.getElementById("po-number_tr"+i).value;
		//alert(document.getElementById("po-number").id);
		document.getElementById("actual-unit-cost").value=document.getElementById("actual-unit-cost_tr"+i).value;
		//alert(document.getElementById("actual-unit-cost").id);
		document.getElementById("position-description").value=document.getElementById("position-description_tr"+i).value;
		//alert(document.getElementById("position-description").id);
		
		document.getElementById("iandc").value=document.getElementById("iandc_tr"+i).value;
		//document.getElementById("trackingCode").value=document.getElementById("trackingCode_tr"+i).value;
		document.getElementById("positionStatusLabel").value=document.getElementById("positionStatusLabel_tr"+i).value;
		document.getElementById("approverName").value=document.getElementById("approverName_tr"+i).value;
		
		//CR5.1
		document.getElementById("targaTecnica").value=document.getElementById("targaTecnica_tr"+i).value;
		
		//alert(document.getElementById("position-approved_tr"+i).value);
	
		if(document.getElementById("approveBtn_tr"+i).value =='true' &&(userType =="CONTROLLER" ||userType=='NDSO' ||userType=='TC')){
		    	document.getElementById('approvePositionBtn').style.display='inline-block';
		}else{
			document.getElementById('approvePositionBtn').style.display='none';
		}
	         


		/////////////////////////////////////////// highligh the fields ////////////////////////////////////////////////////
		if(validatePos=='true'){
		var prForm=document.getElementById("positionForm");
		//alert("number of params "+prForm.elements.length);
		var ignoreElementsArr = ['asset-number','prcs-number','po-number','locationEVO','offerdate','iandc','positionStatusLabel','approverName','targaTecnica','deliveryadress','fiscalyear'];
			for(i=0; i<prForm.elements.length; i++){
				     field_type = prForm.elements[i].type.toLowerCase();
				
		        switch (field_type)
		        {
		        case "text":
		        case "password":
		        case "textarea":
		        case "hidden":
		   		 if((prForm.elements[i].value =="" || prForm.elements[i].value ==null) && $.inArray(prForm.elements[i].id,ignoreElementsArr) == -1){
					 prForm.elements[i].style.borderColor="#FF0000";
					} else{
					 	prForm.elements[i].style.borderColor="";
						 }
		        case "select":
		        case "select-one":
		        case "select-multi":
			   		 if((prForm.elements[i].value =="" || prForm.elements[i].value ==null)&& $.inArray(prForm.elements[i].id,ignoreElementsArr) == -1){
						 prForm.elements[i].style.borderColor="#FF0000";
						} else{
						 	prForm.elements[i].style.borderColor="";
							 }
		        }
			}
		 }
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
	}
	
	function fillPositionFromLatest(i){
	
		//alert('fillPositionFromLatest');
		document.getElementById("fiscalyear").value=document.getElementById("fiscalyear_tr"+i).value;
		//alert(document.getElementById("fiscalyear").id);
		document.getElementById("categorycode").value=document.getElementById("categorycode_tr"+i).value;
		//alert(document.getElementById("categorycode").id);
		document.getElementById("offernumber").value=document.getElementById("offernumber_tr"+i).value;
		//alert(document.getElementById("offernumber").id);
		document.getElementById("offerdate").value=document.getElementById("offerdate_tr"+i).value;
		//alert(document.getElementById("offerdate").id);
		document.getElementById("deliverydate").value=document.getElementById("deliverydate_tr"+i).value;
		//alert(document.getElementById("deliverydate").id);
		document.getElementById("deliveryadress").value=document.getElementById("deliveryadress_tr"+i).value;
		//alert(document.getElementById("deliveryadress").id);
		//document.getElementById("wbscode").value=document.getElementById("wbscode_tr"+i).value;
		//var wbscodeValue = document.getElementById("wbscode_tr"+i).value;
		//jQuery("#wbscode :selected").prop('selected', false);
		//jQuery('#wbscode option[value="'+wbscodeValue+'"]').prop("selected",true);
		//alert(document.getElementById("wbscode").id);
		document.getElementById("locationEVO").value=document.getElementById("locationEvo_tr"+i).value;
		//alert(document.getElementById("locationEVO").id);
		document.getElementById("asset-number").value=document.getElementById("asset-number_tr"+i).value;
		//alert(document.getElementById("asset-number").id);
		document.getElementById("prcs-number").value=document.getElementById("prcs-number_tr"+i).value;
		//alert(document.getElementById("prcs-number").id);
		document.getElementById("po-number").value=document.getElementById("po-number_tr"+i).value;
		//alert(document.getElementById("po-number").id);
		document.getElementById("position-description").value=document.getElementById("position-description_tr"+i).value;
		//alert(document.getElementById("position-description").id);
		
		document.getElementById('positionStatusLabel').value='New';
		document.getElementById('approverName').value='N/A';
		
		//CR5.1
		document.getElementById("targaTecnica").value=document.getElementById("targaTecnica_tr"+i).value;
	}
	
	function resetRequiredFields(){
		var prForm=document.getElementById("editpurchaseRequestForm");
			for(i=0; i<prForm.elements.length; i++){
		     	   	prForm.elements[i].style.borderColor="";
		        	}

	}
	
	function validateRequiredFields(){
		//alert("start validation");
		var result=true;
		var prForm=document.getElementById("editpurchaseRequestForm");
		//alert("number of params "+prForm.elements.length);
		var prIgnoreElementsArr = ['trackingcode','ola','rejectionCause','odnpcode'];
			for(i=0; i<prForm.elements.length; i++){
				     field_type = prForm.elements[i].type.toLowerCase();
				     var placehoder = prForm.elements[i].getAttribute("placeholder");
				     //alert(prForm.elements[i].id);
				     if(placehoder == "Select Tracking Code .." ||prForm.elements[i].id=="trackingcode" || prForm.elements[i].name=="trackingcode"){
				    	 continue;
				     } 
		        switch (field_type)
		        {
		        case "text":
		        case "password":
		        case "textarea":
		        case "hidden":
		   		 if((prForm.elements[i].value =="" || prForm.elements[i].value ==null || prForm.elements[i].value ==placehoder) && $.inArray(prForm.elements[i].id,prIgnoreElementsArr) == -1){
					 prForm.elements[i].style.borderColor="#FF0000";
					 result=false;
					} else{
					 	prForm.elements[i].style.borderColor="";
						 }
		        case "select":
		        case "select-one":
		        case "select-multi":
			   		 if((prForm.elements[i].value =="" || prForm.elements[i].value ==null ||  prForm.elements[i].value ==placehoder)&& $.inArray(prForm.elements[i].id,prIgnoreElementsArr) == -1){
						 prForm.elements[i].style.borderColor="#FF0000";
						 result=false;
						} else{
						 	prForm.elements[i].style.borderColor="";
							 }
		        }
			}
			var positionsTableForm=document.getElementById("positionsTableForm");
			//alert("positionsTableForm "+positionsTableForm);
			//alert("number of params "+positionsTableForm.elements.length);
			var ignoreElementsArr = ['asset-number','prcs-number','po-number','locationEvo','offerdate','iandc','positionStatusLabel','approverName','targaTecnica','deliveryadress'];
			for(i=0; i<positionsTableForm.elements.length; i++){
			     field_type = positionsTableForm.elements[i].type.toLowerCase();
	       switch (field_type)
	       {
	       case "text":
	       case "password":
	       case "textarea":
	       case "hidden":
	    	   var elementID=positionsTableForm.elements[i].id.split('_')[0];
	    	   var trNumber=positionsTableForm.elements[i].id.split('_')[1];
	  		 if(positionsTableForm.elements[i].value =="" && $.inArray(elementID,ignoreElementsArr) == -1){
	  			 ////alert("will chane the colcor for  "+document.getElementById("positionErrors_"+trNumber).id);
	  			document.getElementById("positionErrors_"+trNumber).innerHTML='<div class="alert alert-danger">you should fill the required fields</div>';
	  			validatePos='true';
				 result=false;
					 }
	       }
		}
			
		return result;
	}
	 
	function uploadFile(){
		$(".uploading").fadeIn( "normal" );
		
	 $.ajaxFileUpload
	 (
	 {
	 url:'${uploadFileURL}'+"&purchaseRequestId="+$('#purchaseRequestId').val()+"&automatic="+$('#automatic').val(),
	 secureuri : false,
	 fileElementId : 'fileName',
	 dataType : 'json',
	 data : {
		 name : 'name',
		 id : 'id'
		 },
	 success : function(data, status) {
			 $('#hasUploadedFile').val('true');	
			 //$('#uploadedFile').html('<a href="javascript:viewUploadedfile()">view Uploaded File</a>');
			 listUploadedFiles();
			 $("#fileName").val(null);
			 $(".uploading").fadeOut( "fast" );
			 
			 //alert("done success! status="+status);
			 //alert("done success! data="+data);
	 if (typeof (data.error) != 'undefined') {
	 if (data.error != '') {
		 alert("done success <1> with error! data.error="+data.error);
	 alert(data.error);
	 } else {
		 alert("done success <2> with error! data.error="+data.error);
	 alert(data.msg);
	 }
	 }
	 },
	 error : function(data, status, e) {
		 alert("done error! status="+status);
		 alert("done error! data="+data);
		 alert("done error! e="+e);
	 alert(e);
	 }
	 });
	
	
	 }
	function viewUploadedfile(fileName){
		document.location.href ='${downloadFileURL}'+"&purchaseRequestId="+$('#purchaseRequestId').val()+"&fileName="+encodeURIComponent(fileName.replace(/\u00a0/g, ' '));
	}
	
	function viewAllUploadedfiles(){
		document.location.href ='${downloadAllFilesURL}'+"&purchaseRequestId="+$('#purchaseRequestId').val();
	}
	
	function listUploadedFiles(){
		var purchaseRequestId=$('#purchaseRequestId').val();
		jQuery.post('${listUploadedFilesURL}',{purchaseRequestId:purchaseRequestId},
				 function(data) {
			 		$('#uploadedFile').html(data);
					$('.deletered').tooltip();
					$("#uploadedFile").fadeIn( "normal" );
				 }
			);
	}
	
	function deleteFile(fileName){
		if(window.confirm("Are you sure you want to delete the file")){
		var purchaseRequestId=$('#purchaseRequestId').val();
		jQuery.post('${deleteFileURL}',{purchaseRequestId:purchaseRequestId,fileName:fileName.replace(/\u00a0/g, ' ')},
				 function(data) {
			 		$('#uploadedFile').html(data);
				 }
			);
		}
	}
	
	function <portlet:namespace/>updateSelectOptions(parentId,childId,subProjectId){
		$("#"+childId+" option").remove();
		$("#"+childId).append("<option selected value=''>Select Sub Project..</option>");
		var selectedProject = $("#"+parentId+" option:selected").val();
		$.getJSON('${getSubProjectUrl}',"project="+selectedProject+"&subProjectId="+subProjectId,function(data) {
				if(data.subProjects.length > 0){
					for(var i=0;i<data.subProjects.length;i++){
						var subProject = data.subProjects[i];
						if(subProject.display == true || subProjectId == subProject.id){
						$("#"+childId).append("<option value='"+subProject.id+"'>"+subProject.name+"</option>");
						}
					}
						$("#"+childId).prop('disabled',false);
						$("#"+childId).data('combobox').refresh();
						$(".combobox-container .subprojectcombo").prop('disabled',false);
						$(".subprojctdiv input").val("");
						$(".subprojctdiv .add-on").css("display","inline-block");
				}else{
					$("#"+childId).prop('disabled',true);
					$("#"+childId).data('combobox').refresh();
					$(".combobox-container .subprojectcombo").prop('disabled',true);
					$(".subprojctdiv input").val("");
					$(".subprojctdiv .add-on").css("display","none");
				}
	    	});
	}
	
	function <portlet:namespace/>updateSubProjectValue(parentId,childId,value){
		$("#"+childId+" option").remove();
		$("#"+childId).append("<option selected value=''>Select Sub Project..</option>");
		var selectedProject = $("#"+parentId+" option:selected").val();
		$.getJSON('${getSubProjectUrl}',"project="+selectedProject+"&subProjectId="+value,function(data) {
				if(data.subProjects.length > 0){
					$(".subprojctdiv input").val("");
					for(var i=0;i<data.subProjects.length;i++){
						var subProject = data.subProjects[i];
						if(subProject.display == true){
						if(subProject.id == value){
						  $("#"+childId).append("<option selected value='"+subProject.id+"'>"+subProject.name+"</option>");
						}else{
							$("#"+childId).append("<option value='"+subProject.id+"'>"+subProject.name+"</option>");
						}
					  }
					}
						$("#"+childId).prop('disabled',true);
						$("#"+childId).data('combobox').refresh();
						//$(".combobox-container .subprojectcombo").prop('disabled',true);
						//$(".subprojctdiv input").val(value);
						$(".subprojctdiv .add-on").css("display","none");
				}else{
					$("#"+childId).prop('disabled',true);
					$("#"+childId).data('combobox').refresh();
					$(".combobox-container .subprojectcombo").prop('disabled',true);
					$(".subprojctdiv input").val("");
					$(".subprojctdiv .add-on").css("display","none");
				}
	    	});
	}
	
	function <portlet:namespace/>updateMicroDriverSelectOptions(parentId,childId,microDriverId){
	
		$("#"+childId+" option").remove();
		$("#"+childId).append("<option selected value=''>Select Micro Drivers..</option>");
		var selectedMacro = $("#"+parentId+" option:selected").val();
		$.getJSON('${getMicroDriversUrl}',"macroId="+selectedMacro+"&microDriverId="+microDriverId,function(data) {
				if(data.microDrivers.length > 0){
					for(var i=0;i<data.microDrivers.length;i++){
						var microDriver = data.microDrivers[i];
						if(microDriver.display == true || microDriverId == microDriver.id){
						$("#"+childId).append("<option value='"+microDriver.id+"'>"+microDriver.name+"</option>");
						 }
					}
						$("#"+childId).prop('disabled',false);
						$("#"+childId).data('combobox').refresh();
						$(".combobox-container .microcombobox").prop('disabled',false);
						$(".microdiv input").val("");
						$(".microdiv .add-on").css("display","inline-block");
				}else{
					$("#"+childId).prop('disabled',true);
					$("#"+childId).data('combobox').refresh();
						$(".combobox-container .microcombobox").prop('disabled',true);
						$(".microdiv input").val("");
						$(".microdiv .add-on").css("display","none");
				}
	    	});
	}
	
	function <portlet:namespace/>updateMicroDriverSelectOptionsToValue(parentId,childId,value){
		
		$("#"+childId+" option").remove();
		$("#"+childId).append("<option selected value=''>Select Micro Drivers..</option>");
		var selectedMacro = $("#"+parentId+" option:selected").val();
		$.getJSON('${getMicroDriversUrl}',"macroId="+selectedMacro+"&microDriverId="+value,function(data) {
				if(data.microDrivers.length > 0){
					$(".microdiv input").val("");
					for(var i=0;i<data.microDrivers.length;i++){
						var microDriver = data.microDrivers[i];
						if(microDriver.display == true){
						if(microDriver.id ==value){
						$("#"+childId).append("<option selected value='"+microDriver.id+"'>"+microDriver.name+"</option>");
						}else{
						  $("#"+childId).append("<option value='"+microDriver.id+"'>"+microDriver.name+"</option>");
						}
					  }
					}
						$("#"+childId).prop('disabled',true);
						$("#"+childId).data('combobox').refresh();
						//$(".combobox-container .microcombobox").prop('disabled',true);
						//$(".microdiv input").val(value);
						$(".microdiv .add-on").css("display","none");
				}else{
					$("#"+childId).prop('disabled',true);
					$("#"+childId).data('combobox').refresh();
						$(".combobox-container .microcombobox").prop('disabled',true);
						$(".microdiv input").val("");
						$(".microdiv .add-on").css("display","none");
				}
	    	});
	}
	
	function poulateTrackingValues(){
		
		var selectedTrackingCode = $('#trackingcode').val();
		selectedTrackingCode
		if(selectedTrackingCode ==""){
		
			//alert("empty tracking code");
			$(".trackingrelated").val("");
			$(".trackingsubrelated").val("");
			
			$(".trackingrelated").prop('disabled',false);
			$(".trackingsubrelated").prop('disabled',true);
			
				$("#prwbscode").data('combobox').refresh();
				$("#projectname").data('combobox').refresh();
				$("#budgetCategory").data('combobox').refresh();
				$("#budgetSubCategory").data('combobox').refresh();
				$("#macrodriver").data('combobox').refresh();
				$("#odnpname").data('combobox').refresh();
		}else{
			selectedTrackingCode=selectedTrackingCode.replace(/%/g,"%25");
			//alert("not empty tracking code");
			$.getJSON('${poulateTrackingValuesUrl}',"selectedTrackingCode="+selectedTrackingCode,function(data) {
				//var trackingCodesValues = data.trackingCodesValues;
			if(data.empty != "true") {
				$("#prwbscode").val(data.wbsCodeId);
				$("#projectname").val(data.projectId);
				$("#budgetCategory").val(data.budgetCategoryId);
				$("#budgetSubCategory").val(data.budgetSubCategoryId);
				$("#macrodriver").val(data.macroDriverId);
				$("#odnpname").val(data.odnpNameId);
				
				<portlet:namespace/>updateSubProjectValue("projectname","subproject",data.subProjectId);
				<portlet:namespace/>updateMicroDriverSelectOptionsToValue("macrodriver","microdriver",data.microDriverId);
				
				//$("#subproject").val(data.subProjectId);
				//$("#microdriver").val(data.microDriverId);
				
				
				
				$(".trackingrelated").prop('disabled',false);
				
				$('.trackingsubrelated').prop('disabled',false);
				$("#prwbscode").data('combobox').refresh();
				$("#projectname").data('combobox').refresh();
				$("#budgetCategory").data('combobox').refresh();
				$("#budgetSubCategory").data('combobox').refresh();
				$("#macrodriver").data('combobox').refresh();
				$("#odnpname").data('combobox').refresh();
								
				
				//////////// for micro 
			}
				
	    	});
		}

	}
	


		
	
	
	
	function checkIsNumber(field,fieldName){
		if(field.value !="" && ! $.isNumeric(field.value)){
			alert("the "+fieldName+" should be number");
			field.value="";
		}
		
	}
	
	$(document).ready(function(){
	$(function() {
    var availableTags = [
<c:forEach items="${allTrackingCodes}" var="trackingcode" varStatus="loopStatus">
<c:if test="${trackingcode.display eq true || trackingcode.trackingCodeName  eq fn:trim(fn:split(pr.trackingCode, '||')[0])}">
"<hp:escapeQuotes value='${trackingcode.trackingCodeName}'/> || <hp:escapeQuotes value='${trackingcode.trackingCodeDescription}'/> "

<c:if test="${!loopStatus.last}"> , </c:if>
  
</c:if>
</c:forEach>
               
                         
    ];
    $( "#trackingcode" ).autocomplete({
      source: availableTags,
      change: function (event, ui) { poulateTrackingValues(); }
    });
  });
  });
	
	</script>