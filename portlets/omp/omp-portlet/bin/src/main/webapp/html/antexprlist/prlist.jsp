<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<script type="text/javascript">

function getSelectedChbox() {
	var selchbox = []; 
	var prListForm=document.getElementById("prListForm");
	for(i=0; i<prListForm.elements.length; i++){
	field_type = prListForm.elements[i].type.toLowerCase();
	switch (field_type)
		        {
		        case "checkbox":
		        	if(prListForm.elements[i].checked){
		        	var prsid = prListForm.elements[i].value;
		        	selchbox.push(prsid);
		        	}
					}
	}
	
	return selchbox;
	}
	
	
function validateSelectedPrs(){
	
	var result=true;
	var selchbox = getSelectedChbox();
	//alert('selchbox size'+ selchbox.length);
	var copyselchbox = selchbox;
	//alert(' selchbox value ' + selchbox);
	for(i=0; i<selchbox.length;i++){
		
		//alert('i value' +i);
		var prId = selchbox[i];
		//alert('posId >>>>>'+selchbox[i]);
		
		var costcentervar = 'costcenter';
		costcentervar += prId;
		var currencyvar = 'currency';
		currencyvar += prId;
		
		var costcenterValue = document.getElementById(costcentervar).value;
		var currencyValue = document.getElementById(currencyvar).value;
		
			for(w=0; w<copyselchbox.length; w++){
				var copyPrId = copyselchbox[w];
				//alert('copyPosId '+copyPosId);
				
				var copycostcentervar = 'costcenter';
				copycostcentervar += copyPrId;
				var copycurrencyvar = 'currency';
				copycurrencyvar += copyPrId;
				
				var copycostcentValue = document.getElementById(copycostcentervar).value;
				var copycurrencyValue = document.getElementById(copycurrencyvar).value;
				//alert('assetValue '+assetValue);
				//alert('copyassetValue '+copyassetValue);
				if(copyPrId != prId && costcenterValue != copycostcentValue){
				//alert('cost center values not equals');
					result=false;
					break;
					
				}
				
				if(copyPrId != prId && currencyValue != copycurrencyValue){
					result=false;
					break;
				}
			}
	}
	
	
	return result;

}

function addPurchaseOrder(){
	var prIds = getSelectedChbox();
	if(prIds.length > 0){
	if(validateSelectedPrs()){
		//alert('validation ok');
		var prIds = getSelectedChbox();
		//alert('getSelectedChbox '+prIds);
		//alert('will redirect to /group/vodafone/purchase-order?GET_ADD_PO=addPoForm&prIds' + prIds);
		 var prid= document.getElementById("firstselectedpr").value;
		//alert('first selected pr' + document.getElementById("firstselectedpr").value);
		document.location.href='/group/vodafone/purchase-order?GET_ADD_PO=addPoForm&prIds=' + prIds+'&prId='+prid+"&redirect=${redirect}"; 

	} else {
		alert('You Should Select Purchase Requests have the same Cost Center and Currency.');
	}
	} else {
		alert('You Should Select At Least One Purchase Request to Create Purchase order.');
	}
	
}


function updateClickOrder(checkbox) {
	
	//alert('checkbox.checked '+checkbox.checked);
    if (checkbox.checked) {
    	//alert("firstselectedpr value " + document.getElementById("firstselectedpr").value);
        if (document.getElementById("firstselectedpr").value == "") {
			//alert('firstselectedpr is empty');
            document.getElementById("firstselectedpr").value  = checkbox.value;
        } 
    } else {
    //	alert('set the id with empty');
    	document.getElementById("firstselectedpr").value = "";
    }
  
  // alert("firstselectedpr " + document.getElementById("firstselectedpr").value  );
}

</script>

<form name="prListForm" id="prListForm">
	<display:table decorator="com.hp.omp.decorator.AntexPRListDecorator" name="prList" id="pr" >
	
	   <c:choose>
	  <c:when test="${actionStatus=='PR/SC Received'}">
       <display:column>
       <input type="checkbox" name="option" value="${pr.purchaseRequestId}" onchange="updateClickOrder(this);" />
       
        <c:set value="costcenter${pr.purchaseRequestId}" var="selectedcostcenter" />
       <c:set value="currency${pr.purchaseRequestId}" var="selectedcurrency" />
       <input type="hidden" name="${selectedcostcenter}" id="${selectedcostcenter}" value="${pr.costCenterId}"/>
       <input type="hidden" name="${selectedcurrency}" id="${selectedcurrency}" value="${pr.currency}"/>
       <input type="hidden" name="firstselectedpr" id="firstselectedpr" value=""/>
       </display:column>
	   <display:column class="rqstnum" title="<a href='javascript:void(null)' id='purchaseRequestId' onclick='orderRequesterPRList(this)'>OMP Request #</a>">
  		<a href="/group/vodafone/purchase-order?GET_ADD_PO=addPoForm&prId=${pr.purchaseRequestId}&redirect=${redirect}">${pr.purchaseRequestId}</a>
  	   </display:column>
	   </c:when>
	 	<c:otherwise>
	  <display:column class="rqstnum" title="<a href='javascript:void(null)' id='purchaseRequestId' onclick='orderRequesterPRList(this)'>OMP Request #</a>">
	  	<a href="/group/vodafone/purchase-request?purchaseRequestID=${pr.purchaseRequestId}&actionStatus=${toSendStatus}&redirect=${redirect}">${pr.purchaseRequestId}</a>
	  </display:column>	
	   </c:otherwise> 
	 </c:choose>
	  <display:column property="screenNameRequester" title="<a href='javascript:void(null)' id='screenNameRequester' onclick='orderRequesterPRList(this)'>Requestor</a>" />
	  <display:column property="screenNameReciever" title="<a href='javascript:void(null)' id='screenNameReciever' onclick='orderRequesterPRList(this)'>Receiver</a>" />
	  <display:column property="vendorName" title="<a href='javascript:void(null)' id='vendorName' onclick='orderRequesterPRList(this)'>Vendor</a>" />
	  <display:column property="budgetSubCategoryName" title="<a href='javascript:void(null)' id='budgetSubCategoryName' onclick='orderRequesterPRList(this)'>Sub Category</a>" />
	  <display:column property="foramttedTotalValue" title="<a href='javascript:void(null)' id='totalValue' onclick='orderRequesterPRList(this)'>Total Value</a>"/>
	  <display:column property="costCenterName" title="<a href='javascript:void(null)' id='costCenterName' onclick='orderRequesterPRList(this)'>Cost Center</a>" />
	  <display:column property="fiscalYear" title="<a href='javascript:void(null)' id='fiscalYear' onclick='orderRequesterPRList(this)'>Fiscal Year</a>" />
	  <display:column property="activityDescription" title="<a href='javascript:void(null)' id='activityDescription' onclick='orderRequesterPRList(this)'>Description</a>"/>
	  <display:column property="status" title="Status"/>
	  <display:column title="Download">
	  <c:choose>
	  <c:when test="${automatic==true}">
		<portlet:resourceURL id="downloadFile" var="downloadFileURL" >
			<portlet:param name="action" value="downloadFile"/>
			<portlet:param name="purchaseRequestId" value="${pr.purchaseRequestId}"/>
		</portlet:resourceURL>
	  <a href="${downloadFileURL}">Download</a>
	  </c:when>
	  <c:otherwise>
	  <c:if test="${pr.hasFile == true}">
		<portlet:resourceURL id="downloadZipFile" var="downloadZipFileURL" >
			<portlet:param name="action" value="downloadZipFile"/>
			<portlet:param name="purchaseRequestId" value="${pr.purchaseRequestId}"/>
		</portlet:resourceURL>
	  <a href="${downloadZipFileURL}">Download</a>
	  </c:if>
	  </c:otherwise>
	  </c:choose>
	  </display:column>
	</display:table>
	</form>
	<%@include file="/html/tablepaging/paging.jsp"%>

	