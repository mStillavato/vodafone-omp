<portlet:resourceURL id="getSubProjects" var="getSubProjectUrl"/>

<script type="text/javascript">

var advancedSearch = "off";

	function <portlet:namespace/>dopoSearch(formId){
		advancedSearch = "on";
		var formData = <portlet:namespace/>poSearchFormData(formId);
		var url = '${doSearchURL}&searchAction=doPoSearch&'+formData;
		
		if($(costCenterFieldId).is(":disabled")){
			var costCenterFieldId = "#<portlet:namespace/>costCenter";
			url = url + '&' + $(costCenterFieldId).attr('name') + '=' + $(costCenterFieldId).val();
		}
		<portlet:namespace/>ompListing(url);
	}
	
	function <portlet:namespace/>poSearchFormData(formId){
		var formData = $("#"+formId).serialize();
		return formData;
		
	}
	
	
	function <portlet:namespace/>updateSelectOptions(parentId,childId){
		$("#"+childId+" option").remove();
		$("#"+childId).append("<option selected value=''>Select Sub Project..</option>");
		var selectedProject = $("#"+parentId+" option:selected").val();
		$.getJSON('${getSubProjectUrl}',"project="+selectedProject,function(data) {
				if(data.subProjects.length > 0){
					for(var i=0;i<data.subProjects.length;i++){
						var subProject = data.subProjects[i];
						$("#"+childId).append("<option value='"+subProject.id+"'>"+subProject.name+"</option>");
					}
						$("#"+childId).prop('disabled',false);
						$("#"+childId).data('combobox').refresh();
					$(".combobox-container .subprojectcombo").prop('disabled',false);
					$(".subprojctdiv input").val("");
					$(".subprojctdiv .add-on").css("display","inline-block");
				}else{
					$("#"+childId).prop('disabled',true);
					$("#"+childId).data('combobox').refresh();
				$(".combobox-container .subprojectcombo").prop('disabled',true);
				$(".subprojctdiv input").val("");
				$(".subprojctdiv .add-on").css("display","none");
				}
	    	});
	}

</script>





 <div class="advancedsearchcontent" id="searchpopover"> 
<form action="" method="post" id='<portlet:namespace/>poSearchForm'>
<div class="advancedsearchform">

<div class="formItem">
	<div class="formLabel"><label>Cost Center</label></div>
	<div class="clear"></div>
	<div class="formField"><select name="costCenter" class="combobox"  id="<portlet:namespace/>costCenter">
		<option selected value="">Select Cost Center...</option>
		<c:forEach items="${searchDTO.allCostCenters}" var="costCenter">
			<c:choose>
				<c:when test="${searchDTO.costCenterId ne null && searchDTO.costCenterId eq costCenter.costCenterId}">
					<option selected value="${costCenter.costCenterId}">${costCenter.costCenterName}</option>
				</c:when>
				<c:otherwise>
					<option value="${costCenter.costCenterId}">${costCenter.costCenterName}</option>
				</c:otherwise>
			</c:choose>
					
		</c:forEach>
	</select></div>
	<div class="clear"></div>
	</div>
	
<div class="formItem">	
	<div class="formLabel"><label >PO Number</label></div>
	<div class="clear"></div>
	<div class="formField"><input type="text" name="poNumber" placeholder="PO Number..." value="${searchDTO.poNumber}"></div>
	<div class="clear"></div>
</div>



<div class="clear"></div>


<div class="formItem">	
	<div class="formLabel"><label >Fiscal Year</label></div>
	<div class="clear"></div>
	<div class="formField"><input type="text" name="fiscalYear" placeholder="Fiscal Year..." value="${searchDTO.fiscalYear}"></div>
	<div class="clear"></div>
</div>



<div class="formItem">	
	<div class="formLabel"><label >Project</label></div>
	<div class="clear"></div>
	<div class="formField">
	<select name="project" class="combobox" id="<portlet:namespace/>projects" onchange='<portlet:namespace/>updateSelectOptions("<portlet:namespace/>projects","<portlet:namespace/>subProjects")'>
	<option selected value="">Select Project ..</option>
	<c:forEach items="${searchDTO.allProjects}" var="project">
		<c:choose>
			<c:when test="${project.projectId eq searchDTO.projectName}">
				<option selected value="${project.projectId}">${searchDTO.projectName}</option>
			</c:when>
			<c:otherwise>
				<option value="${project.projectId}">${project.projectName}</option>
			</c:otherwise>
		</c:choose>
	</c:forEach>
	</select>
	<div class="clear"></div>
</div>
</div>

<div class="clear"></div>


<div class="formItem">	
	<div class="formLabel"><label >Sub Project</label></div>
	<div class="clear"></div>
		<div class="formField subprojctdiv"><select class="combobox subprojectcombo"  name="subProject" id='<portlet:namespace/>subProjects' <c:if test="${searchDTO.allSubProjects eq null}">disabled="disabled"</c:if> >
		<option selected value="">Select Sub Project..</option>
		<c:forEach items="${searchDTO.allSubProjects}" var="subProject">
			<c:choose>
				<c:when test="${subProject.subProjectId eq searchDTO.subProjectName}">
					<option selected value="${subProject.subProjectId}">${subProject.subProjectName}</option>
				</c:when>
				<c:otherwise>
					<option value="${subProject.subProjectId}">${subProject.subProjectName}</option>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</select></div>
	<div class="clear"></div>
</div>

<div class="formItem">	
	<div class="formLabel"><label >WBS Code</label></div>
	<div class="clear"></div>
	<div class="formField">
	<select name="wbsCode" id="wbsCode" class="combobox" >
		<option selected value="">Select WBS Code ..</option>
		<c:forEach items="${searchDTO.allWbsCodes}" var="wbscode">
			<c:choose>
				<c:when test="${wbscode.wbsCodeId eq searchDTO.wbsCode}">
					<option selected value="${wbscode.wbsCodeId}">${wbscode.wbsCodeName}</option>
				</c:when>
				<c:otherwise>
					<option value="${wbscode.wbsCodeId}">${wbscode.wbsCodeName}</option>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</select>
	</div>
	<div class="clear"></div>
</div>
	
<div class="clear"></div>

	<input type="button" value="Search" class="submitbtn clssrch" onclick='<portlet:namespace/>dopoSearch("<portlet:namespace/>poSearchForm");' />

	<div class="clear"></div>
 <c:if test="${searchDTO.disableCostCenter}">
	<script type="text/javascript">
	$("#<portlet:namespace/>costCenter").prop('disabled',true);
	</script>
</c:if>
</form>
</div>
</div>


	
