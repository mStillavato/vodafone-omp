<%@include file="init.jsp"%>

<portlet:actionURL name="setPortletStatus" var="actionPortletStatus"/>

<form action='${actionPortletStatus}' method="post">
	<select name="status">
		<option value="0" ${status eq 0? 'selected': ''}>Submitted</option>
		
		<option value="1" ${status eq 1? 'selected': ''}>Asset Insert</option>
		<option value="2" ${status eq 2? 'selected': ''}>PR/SC Received</option>
		<option value="3" ${status eq 3? 'selected': ''}>PO Insert</option>
		<option value="4" ${status eq 4? 'selected': ''}>I&C Pending Approval</option>
	</select>
	<br/>
	<input type="submit" value="Change"> 
</form>
