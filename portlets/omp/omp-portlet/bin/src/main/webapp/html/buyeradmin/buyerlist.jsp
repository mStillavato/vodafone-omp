	<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
	 <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>   
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	
<script src="${pageContext.request.contextPath}/js/jquery.min.js"></script>

<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">  

<div id="buyerList">
<%@include file="/html/buyeradmin/buyertable.jsp"%>
</div>

<portlet:renderURL var="addBuyer">
    <portlet:param name="jspPage" value="/html/buyeradmin/buyerform.jsp" />
</portlet:renderURL>
<a href="${addBuyer}" class="actionbutn" >Add New Buyer</a>
<script type="text/javascript">

function <portlet:namespace/>deleteBuyer(url){

	if(confirm("Do you want to delete this Buyer?")) {
		jQuery.get(url, function( data ) {
			  jQuery("#buyerList").html( data );
			 
				  jQuery("#statusMessage").html("<div><font color='green'>"+jQuery(data).filter("#buyerStatusMessage").html()+"</font></div>");
			});
	}
}



function <portlet:namespace/>ompListing(url){
	jQuery.get(url,
			 function(data) {
			   jQuery("#buyerList").html(data);
			 }
		);
	}
</script>