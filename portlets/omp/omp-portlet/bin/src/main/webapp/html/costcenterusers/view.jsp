<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script type="text/javascript" src="../../omp_main-theme/js/jquery-1.10.2.min.js"></script>
<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">   
<portlet:defineObjects />

<div id="erroDiv">
	<font color="green">${successMSG_2}</font> <br/> <font color="red">${errorMSG_2}</font>
</div>
<div id="<portlet:namespace/>costCenteruserTableList"  class="listingcontainer"></div>
<div class="loading" style='display:none;font-size:30px; color:#eee;margin-top:60px; text-align: center;'>Loading...</div>
<br/>
<br/>
<c:if test="${showDropdowns}">

<portlet:actionURL var="costcenterusersURL">
<portlet:param name="jspPage" value="/html/costcenterusers/view.jsp" />
</portlet:actionURL>
<portlet:resourceURL var="paginationURL">
</portlet:resourceURL>
<div class="adminform">
<aui:form method="post" action="<%=costcenterusersURL%>">
 <aui:select id="user"  name="users" style="float: left;">
        <c:forEach var="user" items="${usersList}">
            <aui:option value="${user.userId}" >${user.fullName}</aui:option>
        </c:forEach>                                
    </aui:select>

     <aui:select id="costcenter"  name="costcenters" style="float: left;" >
        <c:forEach var="costcenter" items="${costCenterList}">
           <c:if test="${costcenter.display eq true}">
            <aui:option value="${costcenter.costCenterId}" >${costcenter.costCenterName}</aui:option>
           </c:if>
        </c:forEach>                                
    </aui:select>
    <aui:button type="submit" value="assign user to cost center" /> 
</aui:form> 
</div>
<portlet:resourceURL var="deletecostcenteruserURL">
<portlet:param name="action" value="removeuser"/>
</portlet:resourceURL>
 <script type="text/javascript">
 function removeCostCenterfromUser(costCenterUserId, userName, costcenterName){
	 if(confirm("Do you want to remove this cost center ["+costcenterName+"] from user ["+userName+"]?")) {
		 var url="${deletecostcenteruserURL}&costCenterUserId="+costCenterUserId+"";
			jQuery.get(url,function( data ) {
				  jQuery( "#<portlet:namespace/>costCenteruserTableList" ).html( data );
				});
			$("#erroDiv").hide();
			}
 		}
 
 function <portlet:namespace/>ompListing(url){
		jQuery("#<portlet:namespace/>costCenteruserTableList").fadeOut( "fast" );
		jQuery(".loading").fadeIn( "normal" );
		jQuery.post(url+"&action=pagination",
				 function(data) {
				   jQuery("#<portlet:namespace/>costCenteruserTableList").html(data);
				   jQuery(".loading").fadeOut( "fast" );
				   jQuery("#<portlet:namespace/>costCenteruserTableList").fadeIn( "fast" );
				 }
			);
		}
<portlet:namespace/>ompListing('${paginationURL}');
 </script>
 </c:if>