<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>   
           <%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
           
           <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
           <link href="../../omp_main-theme/css/admin.css" rel="stylesheet">  
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.min.js"></script>
        <script type="text/javascript">
<!--
jQuery(document).ready(function() {
	jQuery("#trackingForm").validate();
});
//-->
</script>  

 
<portlet:actionURL name="addTrackingCode" var="addTrackingCodeURL" >
<portlet:param name="redirect" value="${redirect}" />
</portlet:actionURL>  
<div class="adminform">         
<aui:form  class="cmxform" action="${addTrackingCodeURL}" method="post" id="trackingForm">
<input type="hidden" name="trackingCodeId" value="${trackingCode.trackingCodeId}"/>
<liferay-ui:error key="duplicate tracking code" message="This tracking code is used"/>
<liferay-ui:error key="duplicate tracking input fields" message="This tracking fields are exist"/>

<aui:input label="Tracking Code " name="trackingCodeName" type="text" value="${trackingCode.trackingCodeName}">
	<aui:validator name="required"/>
</aui:input>
<liferay-ui:error key="name-required" message="This field is required"/>

		<aui:input label="Tracking Description " name="trackingCodeDescription" type="text" value="${trackingCode.trackingCodeDescription}">
	<aui:validator name="required"/>
</aui:input>
		
<liferay-ui:error key="description-required" message="This field is required"/>
		<div class="clear"></div>
		<div class="ompformlable"><label for="projectname">Project Name</label></div>
	    <div class="ompforminput">
		<select class="combobox pproject" id="projectname"  onchange='<portlet:namespace/>updateSelectOptions("projectname","subproject","${trackingCode.subProjectId}")' name="projectname" required>
			<option  value="">Select Project..</option>
			<c:forEach items="${allProjects}" var="project">
			<c:if test="${project.display eq true || project.projectId eq trackingCode.projectId}">
				<c:choose>
					<c:when test="${project.projectId eq trackingCode.projectId}">
						<option selected value="${project.projectId}">${project.projectName}</option>
					</c:when>
					<c:otherwise>
						<option value="${project.projectId}">${project.projectName}</option>
					</c:otherwise>
				</c:choose>
				</c:if>
			</c:forEach>
		</select>
		<liferay-ui:error key="project-required" message="This field is required"/>
	    </div>
	    <div class="ompformlable"><label for="subproject">Sub Project</label></div>
        <div class="ompforminput subprojctdiv">
        	<select class="combobox subprojectcombo" name="subproject" id='subproject' <c:if test="${allSubProjects eq null}">disabled="disabled"</c:if>  required>
				<option  value="">Select Sub Project..</option>
				<c:forEach items="${allSubProjects}" var="subProject">
				<c:if test="${subProject.display eq true || subProject.subProjectId eq trackingCode.subProjectId}">
				<c:choose>
					<c:when test="${subProject.subProjectId eq trackingCode.subProjectId}">
				<option selected value="${subProject.subProjectId}">${subProject.subProjectName}</option>
				</c:when>
				<c:otherwise>
					<option value="${subProject.subProjectId}">${subProject.subProjectName}</option>
							</c:otherwise>
						</c:choose>
						</c:if>
					</c:forEach>
			</select>
					<liferay-ui:error key="subproject-required" message="This field is required"/>
        </div>
             <div class="clear"></div>
			<div class="ompformlable"><label for="budgetCategory">Budget Category</label></div>
		   <div class="ompforminput">
			<select class="combobox" name="budgetCategory" id="budgetCategory" required>
				<option  value="">Select Budget Category..</option>
				<c:forEach items="${allBudgetCategories}" var="budgetCategory">
				<c:if test="${budgetCategory.display eq true || budgetCategory.budgetCategoryId eq trackingCode.budgetCategoryId}">
				<c:choose>
					<c:when test="${budgetCategory.budgetCategoryId eq trackingCode.budgetCategoryId}">
				<option selected value="${budgetCategory.budgetCategoryId}">${budgetCategory.budgetCategoryName}</option>
				</c:when>
				<c:otherwise>
					<option value="${budgetCategory.budgetCategoryId}">${budgetCategory.budgetCategoryName}</option>
							</c:otherwise>
						</c:choose>
						</c:if>
					</c:forEach>
			</select>
			<liferay-ui:error key="budgetCategory-required" message="This field is required"/>
          </div>
                        
						<div class="ompformlable"><label for="budgetSubCategory">Budget Sub Category</label></div>
                        <div class="ompforminput">
                        <select name="budgetSubCategory" class="combobox" id="budgetSubCategory" required>
							<option  value="">Select Budget Sub Category..</option>
							<c:forEach items="${allBudgetSubCategories}" var="budgetSubCategory">
							<c:if test="${budgetSubCategory.display eq true || budgetSubCategory.budgetSubCategoryId eq trackingCode.budgetSubCategoryId}">
								<c:choose>
									<c:when test="${budgetSubCategory.budgetSubCategoryId eq trackingCode.budgetSubCategoryId}">
										<option selected value="${budgetSubCategory.budgetSubCategoryId}">${budgetSubCategory.budgetSubCategoryName}</option>
									</c:when>
									<c:otherwise>
										<option value="${budgetSubCategory.budgetSubCategoryId}">${budgetSubCategory.budgetSubCategoryName}</option>
									</c:otherwise>
								</c:choose>
								</c:if>
							</c:forEach>
						</select>
									<liferay-ui:error key="budgetsubCategory-required" message="This field is required"/>
                        </div>
						<div class="clear"></div>
                        <div class="ompformlable"><label for="macrodriver">Macro Driver</label></div>
                        <div class="ompforminput">

                        <select class="combobox macromacro" name="macrodriver" id="macrodriver" onchange='<portlet:namespace/>updateMicroDriverSelectOptions("macrodriver","microdriver","${trackingCode.microDriverId}")' required>

							<option  value="">Select Macro Driver..</option>
							<c:forEach items="${allMacroDrivers}" var="macroDriver">
							<c:if test="${macroDriver.display eq true || macroDriver.macroDriverId eq trackingCode.macroDriverId}">
								<c:choose>
									<c:when test="${macroDriver.macroDriverId eq trackingCode.macroDriverId}">
										<option selected value="${macroDriver.macroDriverId}">${macroDriver.macroDriverName}</option>
									</c:when>
									<c:otherwise>
										<option value="${macroDriver.macroDriverId}">${macroDriver.macroDriverName}</option>
									</c:otherwise>
								</c:choose>
								</c:if>
							</c:forEach>
						</select>
									<liferay-ui:error key="macrodriver-required" message="This field is required"/>
                        </div>
                        
						<div class="ompformlable"><label for="microdriver">Micro Driver</label></div>
                        <div class="ompforminput microdiv">
                          <select class="microcombobox combobox" name="microdriver" id="microdriver" <c:if test="${allMicroDrivers eq null}">disabled="disabled"</c:if> required>
							<option  value="">Select Micro Driver..</option>
							<c:forEach items="${allMicroDrivers}" var="microDriver">
							<c:if test="${microDriver.display eq true || microDriver.microDriverId eq trackingCode.microDriverId}">
								<c:choose>
									<c:when test="${microDriver.microDriverId eq trackingCode.microDriverId}">
										<option selected value="${microDriver.microDriverId}">${microDriver.microDriverName}</option>
									</c:when>
									<c:otherwise>
										<option value="${microDriver.microDriverId}">${microDriver.microDriverName}</option>
									</c:otherwise>
								</c:choose>
								</c:if>
							</c:forEach>
						</select>
									<liferay-ui:error key="microdriver-required" message="This field is required"/>
                        </div>
                        <div class="ompformlable"><label for="odnpname">ODNP Name</label></div>
                        <div class="ompforminput">
                        <select class="combobox" name="odnpname" id="odnpname" ${prDisabled} required>
							<option selected value="">Select ODNP Name..</option>
							<c:forEach items="${allODNPNames}" var="odnpName">
							<c:if test="${odnpName.display eq true || odnpName.odnpNameId eq trackingCode.odnpNameId}">
								<c:choose>
									<c:when test="${odnpName.odnpNameId eq trackingCode.odnpNameId}">
										<option selected value="${odnpName.odnpNameId}">${odnpName.odnpNameName}</option>
									</c:when>
									<c:otherwise>
										<option value="${odnpName.odnpNameId}">${odnpName.odnpNameName}</option>
									</c:otherwise>
								</c:choose>
								</c:if>
							</c:forEach>
						</select>
									<liferay-ui:error key="odnpname-required" message="This field is required"/>
						</div>
						
                        <div class="ompforminput">
						 <div id="wbscodeSelectboxDiv" ><div class="ompformlable"><label for="vendor">WBS Code</label></div>
						<div class="ompforminput"><select name="wbscode" id="wbscode" required>
							<option selected value="">Select WBS Code ..</option>
							<c:forEach items="${allWbsCodes}" var="wbscode">
							<c:if test="${wbscode.display eq true || wbscode.wbsCodeId eq trackingCode.wbsCodeId}">
							<c:choose>
									<c:when test="${wbscode.wbsCodeId eq trackingCode.wbsCodeId}">
								<option selected value="${wbscode.wbsCodeId}">${wbscode.wbsCodeName}</option>
									</c:when>
									<c:otherwise>
								<option value="${wbscode.wbsCodeId}">${wbscode.wbsCodeName}</option>
									</c:otherwise>
								</c:choose>
								</c:if>
							</c:forEach>
						</select></div>
									<liferay-ui:error key="wbscode-required" message="This field is required"/>
					</div>
                        </div>
                        <aui:input inlineLabel="top" label="Display" name="display" type="checkbox" checked="${trackingCode.display}" />
                        <input type="submit" value="Save"/>
</aui:form>
</div>
<portlet:resourceURL id="getSubProjects" var="getSubProjectUrl"/>
	<portlet:resourceURL id="getMicroDrivers" var="getMicroDriversUrl"/>
	
<script type="text/javascript">


function <portlet:namespace/>updateSelectOptions(parentId,childId,subProjectId){
	jQuery("#"+childId+" option").remove();
	jQuery("#"+childId).append("<option selected value=''>Select Sub Project..</option>");
	var selectedProject = jQuery("#"+parentId+" option:selected").val();
	jQuery.getJSON('${getSubProjectUrl}',"project="+selectedProject+"&subProjectId="+subProjectId,function(data) {
			if(data.subProjects.length > 0){
				for(var i=0;i<data.subProjects.length;i++){
					var subProject = data.subProjects[i];
					if(subProject.display == true || subProjectId == subProject.id){
					jQuery("#"+childId).append("<option value='"+subProject.id+"'>"+subProject.name+"</option>");
					 }
				}
				jQuery("#"+childId).prop('disabled',false);
				//jQuery("#"+childId).data('combobox').refresh();
				jQuery(".combobox-container .subprojectcombo").prop('disabled',false);
				jQuery(".subprojctdiv input").val("");
				jQuery(".subprojctdiv .add-on").css("display","inline-block");
			}else{
				jQuery("#"+childId).prop('disabled',true);
				//jQuery("#"+childId).data('combobox').refresh();
				jQuery(".combobox-container .subprojectcombo").prop('disabled',true);
				jQuery(".subprojctdiv input").val("");
				jQuery(".subprojctdiv .add-on").css("display","none");
			}
    	});
}

function <portlet:namespace/>updateMicroDriverSelectOptions(parentId,childId,microDriverId){
	jQuery("#"+childId+" option").remove();
	jQuery("#"+childId).append("<option selected value=''>Select Micro Drivers..</option>");
	var selectedMacro = jQuery("#"+parentId+" option:selected").val();
	jQuery.getJSON('${getMicroDriversUrl}',"macroId="+selectedMacro+"&microDriverId="+microDriverId,function(data) {
			if(data.microDrivers.length > 0){
				for(var i=0;i<data.microDrivers.length;i++){
					var microDriver = data.microDrivers[i];
					if(microDriver.display == true || microDriverId == microDriver.id){
					jQuery("#"+childId).append("<option value='"+microDriver.id+"'>"+microDriver.name+"</option>");
					}
				}
				jQuery("#"+childId).prop('disabled',false);
				//jQuery("#"+childId).data('combobox').refresh();
				jQuery(".combobox-container .microcombobox").prop('disabled',false);
				jQuery(".microdiv input").val("");
				jQuery(".microdiv .add-on").css("display","inline-block");
			}else{
				jQuery("#"+childId).prop('disabled',true);
				//jQuery("#"+childId).data('combobox').refresh();
				jQuery(".combobox-container .microcombobox").prop('disabled',true);
				jQuery(".microdiv input").val("");
				jQuery(".microdiv .add-on").css("display","none");
			}
    	});
}
</script>