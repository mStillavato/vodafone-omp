<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ page import="javax.portlet.PortletPreferences" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link href="../../omp_main-theme/css/admin.css" rel="stylesheet">  
<portlet:defineObjects />

<portlet:actionURL var="budgetCategoryadminURL">
<portlet:param name="jspPage" value="/html/budgetcategoryadmin/view.jsp" />
</portlet:actionURL>
<div class="adminform">
<aui:form method="post" action="<%=budgetCategoryadminURL%>">
<aui:input label="Budget Category Name: " name="budgetCategoryName" type="text" value="${budgetCategoryName}">
	<aui:validator name="required"/>
</aui:input>

<aui:input name="action" type="hidden" value="${action}"/>

<c:if test="${not empty budgetCategoryId}">
<aui:input name="budgetCategoryId" type="hidden" value="${budgetCategoryId}"/>
</c:if>

<aui:input inlineLabel="top" label="Display" name="display" type="checkbox" checked="${display}" />

<aui:button type="submit" /> 
</aui:form> 
</div>
