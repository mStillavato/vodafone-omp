<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<div>
	<font color="green">${successMSG}</font> <br/> <font color="red">${errorMSG}</font>
</div>

<display:table decorator="com.hp.omp.decorator.MacroDriverListDecorator" name="macroDriverList" list="macroDriverList" id="macroDriver">

  <display:column property="macroDriverId" title="Delete" class="deleteicon"/>
  <display:column title="Name" >
  
  	<portlet:renderURL var="editMacroDriver">
	    <portlet:param name="jspPage" value="/html/macrodriveradmin/edit.jsp" />
	    <portlet:param name="macroDriverId" value="${macroDriver.macroDriverId}" />
	    <portlet:param name="action" value="update" />
	</portlet:renderURL>
	
  	<a href="${editMacroDriver}">${macroDriver.macroDriverName}</a>
  </display:column>
  <display:column property="display" title="Display" />
</display:table>
<%@include file="/html/tablepaging/paging.jsp"%>