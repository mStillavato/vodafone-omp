<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<div>
	<font color="green">${successMSG}</font> <br/> <font color="red">${errorMSG}</font>
</div>

<table class="listingtable ccuser">
<thead>
<tr>
<th>Name</th>
<th>Cost Center</th>
<th>Delete</th>
</tr></thead>
<c:forEach items="${costcenterMap}" var="costcentersmap">   
	<c:forEach items="${costcentersmap.value}" var="usercostcenter">             
	   <tr class="listingtreven">       
	  <td  style="padding: 10px;"><c:out value="${usercostcenter.key.fullName}" /></td>
	  <td  style="padding: 10px;"><c:out value="${usercostcenter.value.costCenterName}"/> </td>
	  
	  
	  <td class="deleteicon" style="padding: 10px;"><a href='javascript:removeCostCenterfromUser("${costcentersmap.key.costCenterUserId}", "${usercostcenter.key.fullName}","${usercostcenter.value.costCenterName}")'>remove</a></td>
	  </tr>
	</c:forEach>
</c:forEach>
</table>

<%@include file="/html/tablepaging/paging.jsp"%>