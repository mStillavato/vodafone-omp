create sequence PURCHASE_REQUEST_ID_SEQ start with 1 increment by 1 
  NOMAXVALUE  
  NOCYCLE
  NOCACHE
  NOORDER; 
CREATE OR REPLACE TRIGGER  PURCHASE_REQUEST_ID_TRIG
  before insert on PURCHASE_REQUEST              
  for each row  
begin   
  if :NEW.purchaseRequestId is null then 
    select PURCHASE_REQUEST_ID_SEQ.nextval into :NEW.purchaseRequestId from dual; 
  end if; 
end;



create sequence PURCHASE_ORDER_ID_SEQ start with 1 increment by 1 
  NOMAXVALUE  
  NOCYCLE
  NOCACHE
  NOORDER; 
CREATE OR REPLACE TRIGGER  PURCHASE_ORDER_ID_TRIG
  before insert on PURCHASE_ORDER              
  for each row  
begin   
  if :NEW.purchaseOrderId is null then 
    select PURCHASE_ORDER_ID_SEQ.nextval into :NEW.purchaseOrderId from dual; 
  end if; 
end;


create sequence POSITION_ID_SEQ start with 1 increment by 1 
  NOMAXVALUE  
  NOCYCLE
  NOCACHE
  NOORDER; 
CREATE OR REPLACE TRIGGER  POSITION_ID_TRIG
  before insert on POSITION              
  for each row  
begin   
  if :NEW.positionId is null then 
    select POSITION_ID_SEQ.nextval into :NEW.positionId from dual; 
  end if; 
end;

create sequence GOOD_RECEIPT_ID_SEQ start with 1 increment by 1 
  NOMAXVALUE  
  NOCYCLE
  NOCACHE
  NOORDER; 
CREATE OR REPLACE TRIGGER  GOOD_RECEIPT_ID_TRIG
  before insert on GOOD_RECEIPT              
  for each row  
begin   
  if :NEW.goodReceiptId is null then 
    select GOOD_RECEIPT_ID_SEQ.nextval into :NEW.goodReceiptId from dual; 
  end if; 
end;



create sequence PR_AUDIT_ID_SEQ start with 1 increment by 1 
  NOMAXVALUE  
  NOCYCLE
  NOCACHE
  NOORDER; 
CREATE OR REPLACE TRIGGER  PR_AUDIT_ID_TRIG
  before insert on PR_AUDIT              
  for each row  
begin   
  if :NEW.auditId is null then 
    select PR_AUDIT_ID_SEQ.nextval into :NEW.auditId from dual; 
  end if; 
end;

create sequence PO_AUDIT_ID_SEQ start with 1 increment by 1 
  NOMAXVALUE  
  NOCYCLE
  NOCACHE
  NOORDER; 
CREATE OR REPLACE TRIGGER  PO_AUDIT_ID_TRIG
  before insert on PO_AUDIT              
  for each row  
begin   
  if :NEW.auditId is null then 
    select PO_AUDIT_ID_SEQ.nextval into :NEW.auditId from dual; 
  end if; 
end;

create sequence POSITION_AUDIT_ID_SEQ start with 1 increment by 1 
  NOMAXVALUE  
  NOCYCLE
  NOCACHE
  NOORDER; 
CREATE OR REPLACE TRIGGER  POSITION_AUDIT_ID_TRIG
  before insert on POSITION_AUDIT              
  for each row  
begin   
  if :NEW.auditId is null then 
    select POSITION_AUDIT_ID_SEQ.nextval into :NEW.auditId from dual; 
  end if; 
end;

create sequence GR_AUDIT_ID_SEQ start with 1 increment by 1 
  NOMAXVALUE  
  NOCYCLE
  NOCACHE
  NOORDER; 
CREATE OR REPLACE TRIGGER  GR_AUDIT_ID_TRIG
  before insert on GR_AUDIT              
  for each row  
begin   
  if :NEW.auditId is null then 
    select GR_AUDIT_ID_SEQ.nextval into :NEW.auditId from dual; 
  end if; 
end;

create sequence COST_CENTER_ID_SEQ start with 1 increment by 1 
  NOMAXVALUE  
  NOCYCLE
  NOCACHE
  NOORDER; 
CREATE OR REPLACE TRIGGER  COST_CENTER_ID_TRIG
  before insert on COST_CENTER              
  for each row  
begin   
  if :NEW.costCenterId is null then 
    select COST_CENTER_ID_SEQ.nextval into :NEW.costCenterId from dual; 
  end if; 
end;

create sequence COST_CENTER_USER_ID_SEQ start with 1 increment by 1 
  NOMAXVALUE  
  NOCYCLE
  NOCACHE
  NOORDER; 
CREATE OR REPLACE TRIGGER  COST_CENTER_USER_ID_TRIG
  before insert on COST_CENTER_USER              
  for each row  
begin   
  if :NEW.costCenterUserId is null then 
    select COST_CENTER_USER_ID_SEQ.nextval into :NEW.costCenterUserId from dual; 
  end if; 
end;