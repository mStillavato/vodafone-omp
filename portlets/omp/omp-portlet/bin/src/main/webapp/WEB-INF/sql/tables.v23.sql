-- remove the WBS and tracking code from position
alter table   OMP_PORTLET.POSITION
drop   (WBSCODEID, TRACKINGCODE);