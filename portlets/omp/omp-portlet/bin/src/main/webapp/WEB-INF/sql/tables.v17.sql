
============== BUDGET_CATEGORY =============
create table OMP_PORTLET.BUDGET_CATEGORY (
    budgetCategoryId NUMBER(30) not null primary key,
    budgetCategoryName VARCHAR(300) not null
);

--==========================================

ALTER TABLE OMP_PORTLET.BUDGET_CATEGORY
 ADD CONSTRAINT UNIQUE_BUDGET_CATEGORY_NAME
  UNIQUE (budgetCategoryName)
  ENABLE VALIDATE;

--==========================================

create sequence BUDGET_CATEGORY_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER BUDGET_CATEGORY_ID_TRIG
  before insert on BUDGET_CATEGORY             
  for each row
begin   
  if :NEW.budgetCategoryId is null then 
    select BUDGET_CATEGORY_ID_SEQ.nextval into :NEW.budgetCategoryId from dual; 
  end if; 
end;

--==========================================

Insert into BUDGET_CATEGORY
  ( budgetCategoryName)
 Values
   ('Test BUDGET CATEGORY');

--===================================================================
--===============        PURCHASE_ORDER    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
RENAME COLUMN BUDGETCATEGORY TO budgetCategoryId;

--==========================================

update PURCHASE_ORDER set  budgetCategoryId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
MODIFY (budgetCategoryId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
 ADD CONSTRAINT FK_PO_BUDGET_CATEGORY 
  FOREIGN KEY (budgetCategoryId) 
  REFERENCES OMP_PORTLET.BUDGET_CATEGORY (budgetCategoryId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_ORDER set  budgetCategoryId = 1  ;

--===========================================================================
--===============        PURCHASE_REQUEST    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
RENAME COLUMN BUDGETCATEGORY TO budgetCategoryId;

--==========================================

update PURCHASE_REQUEST set  budgetCategoryId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
MODIFY (budgetCategoryId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
 ADD CONSTRAINT FK_PR_BUDGET_CATEGORY 
  FOREIGN KEY (budgetCategoryId) 
  REFERENCES OMP_PORTLET.BUDGET_CATEGORY (budgetCategoryId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_REQUEST set  budgetCategoryId = 1  ;



********************  BUDGET_SUB_CATEGORY *********************


create table BUDGET_SUB_CATEGORY (
    budgetSubCategoryId NUMBER(30) not null primary key,
    budgetSubCategoryName VARCHAR(300) not null
);

--==========================================

ALTER TABLE OMP_PORTLET.BUDGET_SUB_CATEGORY
 ADD CONSTRAINT UNIQUE_SUB_CATEGORY_NAME
  UNIQUE (budgetSubCategoryName)
  ENABLE VALIDATE;

--==========================================

create sequence BUDGET_SUB_CATEGORY_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER BUDGET_SUB_CATEGORY_ID_TRIG
  before insert on BUDGET_SUB_CATEGORY             
  for each row
begin   
  if :NEW.budgetSubCategoryId is null then 
    select BUDGET_SUB_CATEGORY_ID_SEQ.nextval into :NEW.budgetSubCategoryId from dual; 
  end if; 
end;

--==========================================

Insert into BUDGET_SUB_CATEGORY
  ( budgetSubCategoryName)
 Values
   ('Test BUDGET SUB CATEGORY');

--===================================================================
--===============        PURCHASE_ORDER    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
RENAME COLUMN BUDGETSUBCATEGORY TO budgetSubCategoryId;

--==========================================

update PURCHASE_ORDER set  budgetSubCategoryId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
MODIFY (budgetSubCategoryId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
 ADD CONSTRAINT FK_PO_BUDGET_SUB_CATEGORY 
  FOREIGN KEY (budgetSubCategoryId) 
  REFERENCES OMP_PORTLET.BUDGET_SUB_CATEGORY (budgetSubCategoryId)
  ENABLE VALIDATE;
  


--==========================================

update PURCHASE_ORDER set  budgetSubCategoryId = 1  ;

--===========================================================================
--===============        PURCHASE_REQUEST    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
RENAME COLUMN BUDGETSUBCATEGORY TO budgetSubCategoryId;

--==========================================

update PURCHASE_REQUEST set  budgetSubCategoryId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
MODIFY (budgetSubCategoryId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
 ADD CONSTRAINT FK_PR_BUDGET_SUB_CATEGORY 
  FOREIGN KEY (budgetSubCategoryId) 
  REFERENCES OMP_PORTLET.BUDGET_SUB_CATEGORY (budgetSubCategoryId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_REQUEST set  budgetSubCategoryId = 1  ;


********************  VENDOR *********************


create table VENDOR (
    vendorId NUMBER(30) not null primary key,
    supplier VARCHAR(300) not null,
	supplierCode VARCHAR(300) not null,
	vendorName VARCHAR(300) not null
);


--==========================================

create sequence VENDOR_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER VENDOR_ID_SEQ_TRIG
  before insert on VENDOR            
  for each row
begin   
  if :NEW.vendorId is null then 
    select VENDOR_ID_SEQ.nextval into :NEW.vendorId from dual; 
  end if; 
end;

--==========================================

Insert into VENDOR
  ( supplier,supplierCode,vendorName)
 Values
   ('Test supplier' ,'Test supplierCode','Test Vendor');

--===================================================================
--===============        PURCHASE_ORDER    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
RENAME COLUMN VENDOR TO vendorId;

--==========================================

update PURCHASE_ORDER set  vendorId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
MODIFY (vendorId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
 ADD CONSTRAINT FK_PO_VENDOR 
  FOREIGN KEY (vendorId) 
  REFERENCES OMP_PORTLET.VENDOR (vendorId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_ORDER set  vendorId = 1  ;

--===========================================================================
--===============        PURCHASE_REQUEST    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
RENAME COLUMN VENDOR TO vendorId;

--==========================================

update PURCHASE_REQUEST set  vendorId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
MODIFY (vendorId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
 ADD CONSTRAINT FK_PR_VENDOR 
  FOREIGN KEY (vendorId) 
  REFERENCES OMP_PORTLET.VENDOR (vendorId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_REQUEST set  vendorId = 1  ;


********************  MACRO DRIVER *********************


create table MACRO_DRIVER (
    macroDriverId NUMBER(30) not null primary key,
	macroDriverName VARCHAR(300) not null
);

--==========================================

ALTER TABLE OMP_PORTLET.MACRO_DRIVER
 ADD CONSTRAINT UNIQUE_MACRO_DRIVER_NAME
  UNIQUE (macroDriverName)
  ENABLE VALIDATE;

--==========================================

create sequence MACRO_DRIVER_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER MACRO_DRIVER_ID_SEQ_TRIG
  before insert on MACRO_DRIVER            
  for each row
begin   
  if :NEW.macroDriverId is null then 
    select MACRO_DRIVER_ID_SEQ.nextval into :NEW.macroDriverId from dual; 
  end if; 
end;

--==========================================

Insert into MACRO_DRIVER
  ( macroDriverName)
 Values
   ('Test MACRO Driver');

--===================================================================
--===============        PURCHASE_ORDER    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
RENAME COLUMN MACRODRIVER TO macroDriverId;

--==========================================

update PURCHASE_ORDER set  macroDriverId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
MODIFY (macroDriverId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
 ADD CONSTRAINT FK_PO_MACRO_DRIVER
  FOREIGN KEY (macroDriverId) 
  REFERENCES OMP_PORTLET.MACRO_DRIVER (macroDriverId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_ORDER set  macroDriverId = 1  ;

--===========================================================================
--===============        PURCHASE_REQUEST    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
RENAME COLUMN MACRODRIVER TO macroDriverId;

--==========================================

update PURCHASE_REQUEST set  macroDriverId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
MODIFY (macroDriverId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
 ADD CONSTRAINT FK_PR_MACRO_DRIVER
  FOREIGN KEY (macroDriverId) 
  REFERENCES OMP_PORTLET.MACRO_DRIVER (macroDriverId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_REQUEST set  macroDriverId = 1  ;



********************  MICRO DRIVER *********************


create table MICRO_DRIVER (
    microDriverId NUMBER(30) not null primary key,
	microDriverName VARCHAR(300) not null
);

--==========================================

ALTER TABLE OMP_PORTLET.MICRO_DRIVER
 ADD CONSTRAINT UNIQUE_MICRO_DRIVER_NAME
  UNIQUE (microDriverName)
  ENABLE VALIDATE;

--==========================================


create sequence MICRO_DRIVER_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER MICRO_DRIVER_ID_SEQ_TRIG
  before insert on MICRO_DRIVER            
  for each row
begin   
  if :NEW.microDriverId is null then 
    select MICRO_DRIVER_ID_SEQ.nextval into :NEW.microDriverId from dual; 
  end if; 
end;

--==========================================

Insert into MICRO_DRIVER
  ( microDriverName)
 Values
   ('Test MICRO Driver');

--===================================================================
--===============        PURCHASE_ORDER    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
RENAME COLUMN MICRODRIVER TO microDriverId;

--==========================================

update PURCHASE_ORDER set  microDriverId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
MODIFY (microDriverId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
 ADD CONSTRAINT FK_PO_MICRO_DRIVER
  FOREIGN KEY (microDriverId) 
  REFERENCES OMP_PORTLET.MICRO_DRIVER (microDriverId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_ORDER set  microDriverId = 1  ;

--===========================================================================
--===============        PURCHASE_REQUEST    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
RENAME COLUMN MICRODRIVER TO microDriverId;

--==========================================

update PURCHASE_REQUEST set  microDriverId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
MODIFY (microDriverId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
 ADD CONSTRAINT FK_PR_MICRO_DRIVER
  FOREIGN KEY (microDriverId) 
  REFERENCES OMP_PORTLET.MICRO_DRIVER (microDriverId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_REQUEST set  microDriverId = 1  ;



*******************  MACRO_MICRO_DRIVER *********************


create table MACRO_MICRO_DRIVER (
    macroMicroDriverId NUMBER(30) not null primary key,
	macroDriverId NUMBER(30) not null,
	microDriverId NUMBER(30) not null
);


--==========================================

ALTER TABLE OMP_PORTLET.MACRO_MICRO_DRIVER
 ADD CONSTRAINT FK_MICRO_DRIVER 
  FOREIGN KEY (microDriverId) 
  REFERENCES OMP_PORTLET.MICRO_DRIVER (microDriverId)
  ENABLE VALIDATE;

 --==========================================

ALTER TABLE OMP_PORTLET.MACRO_MICRO_DRIVER
 ADD CONSTRAINT FK_MACRO_DRIVER  
  FOREIGN KEY (macroDriverId) 
  REFERENCES OMP_PORTLET.MACRO_DRIVER (macroDriverId)
  ENABLE VALIDATE;

  --==========================================
create sequence MACRO_MICRO_DRIVER_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER MACRO_MICRO_DRIVER_ID_SEQ_TRIG
  before insert on MACRO_MICRO_DRIVER            
  for each row
begin   
  if :NEW.macroMicroDriverId is null then 
    select MACRO_MICRO_DRIVER_ID_SEQ.nextval into :NEW.macroMicroDriverId from dual; 
  end if; 
end;

--==========================================

Insert into MACRO_MICRO_DRIVER
  ( macroDriverId,microDriverId)
 Values
   (1,1);

********************  ODNP_NAME*********************


create table ODNP_NAME (
    odnpNameId NUMBER(30) not null primary key,
	odnpNameName VARCHAR(300) not null
);

--==========================================

ALTER TABLE OMP_PORTLET.ODNP_NAME
 ADD CONSTRAINT UNIQUE_ODNP_NAME
  UNIQUE (odnpNameName)
  ENABLE VALIDATE;

--==========================================

create sequence ODNP_NAME_ID_SEQ start with 1 increment by 1 ;

CREATE OR REPLACE TRIGGER ODNP_NAME_ID_SEQ_TRIG
  before insert on ODNP_NAME            
  for each row
begin   
  if :NEW.odnpNameId is null then 
    select ODNP_NAME_ID_SEQ.nextval into :NEW.odnpNameId from dual; 
  end if; 
end;

--==========================================

Insert into ODNP_NAME
  ( odnpNameName)
 Values
   ('Test odnp Name');

--===================================================================
--===============        PURCHASE_ORDER    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
RENAME COLUMN ODNPPROGRAMENAME TO odnpNameId;

--==========================================

update PURCHASE_ORDER set  odnpNameId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
MODIFY (odnpNameId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
 ADD CONSTRAINT FK_PO_ODNP_NAME
  FOREIGN KEY (odnpNameId) 
  REFERENCES OMP_PORTLET.ODNP_NAME (odnpNameId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_ORDER set  odnpNameId = 1  ;

--===========================================================================
--===============        PURCHASE_REQUEST    ===========================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
RENAME COLUMN ODNPPROGRAMENAME TO odnpNameId;

--==========================================

update PURCHASE_REQUEST set  odnpNameId = null  ;

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
MODIFY (odnpNameId NUMBER(30));

--==========================================

ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
 ADD CONSTRAINT FK_PR_ODNP_NAME
  FOREIGN KEY (odnpNameId) 
  REFERENCES OMP_PORTLET.ODNP_NAME (odnpNameId)
  ENABLE VALIDATE;

--==========================================

update PURCHASE_REQUEST set  odnpNameId = 1  ;

===============================

ALTER TABLE OMP_PORTLET.PURCHASE_ORDER
RENAME COLUMN odnpProgrameCode TO odnpCodeId;



ALTER TABLE OMP_PORTLET.PURCHASE_REQUEST
RENAME COLUMN odnpProgrameCode TO odnpCodeId;




