create table PURCHASE_REQUEST (
	purchaseRequestId NUMBER(30) not null primary key,
	costCenterId NUMBER(30) not null,
	vendor VARCHAR(300) null,
	buyer VARCHAR(300) null,
	ola VARCHAR(300) null,
	projectName VARCHAR(300) null,
	subProjectName VARCHAR(300) null,
	budgetCategory VARCHAR(300) null,
	budgetSubCategory VARCHAR(300) null,
	activityDescription VARCHAR(300) null,
	macroDriver VARCHAR(300) null,
	microDriver VARCHAR(300) null,
	odnpProgrameCode VARCHAR(300) null,
	odnpProgrameName VARCHAR(300) null,
	totalValue DECIMAL(30,5) null,
	currency_ NUMBER(30,0) null,
	status INTEGER not null,
	automatic char(1) check (automatic in ('0','1')),
	createdDate DATE default sysdate,
	createdUserId NUMBER(30) not null,
	receiverUserId NUMBER(30) null
);

create table PURCHASE_ORDER (
	purchaseOrderId NUMBER(30) not null primary key,
	costCenterId NUMBER(30) not null,
	vendor VARCHAR(300) null,
	buyer VARCHAR(300) null,
	ola VARCHAR(300) null,
	projectName VARCHAR(300) null,
	subProjectName VARCHAR(300) null,
	budgetCategory VARCHAR(300) null,
	budgetSubCategory VARCHAR(300) null,
	activityDescription VARCHAR(300) null,
	macroDriver VARCHAR(300) null,
	microDriver VARCHAR(300) null,
	odnpProgrameCode VARCHAR(300) null,
	odnpProgrameName VARCHAR(300) null,
	totalValue DECIMAL(30,5) null,
	currency_ NUMBER(30,0) null,
	status INTEGER not null,
	automatic char(1) check (automatic in ('0','1')),
	createdDate DATE default sysdate,
	createdUserId NUMBER(30) not null,
	receiverUserId NUMBER(30) null
);

create table POSITION (
	positionId NUMBER(30) not null primary key,
	fiscalYear VARCHAR(300) null,
	description VARCHAR(300) null,
	categoryCode VARCHAR(300) null,
	offerNumber VARCHAR(300) null,
	offerDate DATE null,
	quatity DECIMAL(30,5) null,
	deliveryDate DATE null,
	deliveryAddress VARCHAR(300) null,
	actualUnitCost DECIMAL(30,5) null,
	numberOfUnit VARCHAR(300) null,
	assetNumber VARCHAR(300) null,
	shoppingCart VARCHAR(300) null,
	poNumber VARCHAR(300) null,
	evoLocation VARCHAR(300) null,
	wbsCode VARCHAR(300) null,
	purchaseRequestId NUMBER(30),
	purchaseOrderId NUMBER(30),
	createdDate DATE  default sysdate null,
	createdUserId NUMBER(30) not null,
	labelNumber NUMBER(30) not null,
	CONSTRAINT fk_position_pr FOREIGN KEY (purchaseRequestId)
	REFERENCES PURCHASE_REQUEST(purchaseRequestId),
	CONSTRAINT fk_position_po FOREIGN KEY (purchaseOrderId)
	REFERENCES PURCHASE_ORDER(purchaseOrderId)


);

create table GOOD_RECEIPT (
	goodReceiptId NUMBER(30) not null primary key,
	requestDate DATE  default sysdate null,
	percentage decimal(5,2) null,
	grValue VARCHAR(300) null,
	registerationDate DATE null,
	createdUserId NUMBER(30) not null,
	positionId NUMBER(30) not null,
	goodReceiptNumber NUMBER(30) null,
	CONSTRAINT fk_gr_position FOREIGN KEY (positionId)
	REFERENCES POSITION(positionId)
);


--------------AUDITING TABLES-------------------------
create table PR_AUDIT (
	auditId NUMBER(30) not null primary key,
	purchaseRequestId NUMBER(30) not null,
	status INTEGER not null,
	userId NUMBER(30) not null,
	changeDate DATE  default sysdate null
);
create table PO_AUDIT (
	auditId NUMBER(30) not null primary key,
	purchaseOrderId NUMBER(30) not null,
	status INTEGER not null,
	userId NUMBER(30) not null,
	changeDate DATE default sysdate NULL
);
create table POSITION_AUDIT (
	auditId NUMBER(30) not null primary key,
	positionId NUMBER(30) not null,
	status INTEGER not null,
	userId NUMBER(30) not null,
	changeDate DATE  default sysdate null
);
create table GR_AUDIT (
	auditId NUMBER(30) not null primary key,
	goodReceiptId NUMBER(30),
	status INTEGER not null,
	userId NUMBER(30) not null,
	changeDate DATE  default sysdate null
);





create table COST_CENTER (
	costCenterId NUMBER(30) not null primary key,
	costCenterName VARCHAR(300) not null,
	description VARCHAR(300) null
);

create table COST_CENTER_USER (
	costCenterUserId NUMBER(30) not null primary key,
	costCenterId NUMBER(30) not null,
	userId NUMBER(30) not null
);

ALTER TABLE PURCHASE_REQUEST
ADD CONSTRAINT fk_PR_COST_CENTER
FOREIGN KEY (costCenterId)
REFERENCES COST_CENTER(costCenterId);

ALTER TABLE PURCHASE_ORDER
ADD CONSTRAINT fk_PO_COST_CENTER
FOREIGN KEY (costCenterId)
REFERENCES COST_CENTER(costCenterId);


ALTER TABLE OMP_PORTLET.COST_CENTER_USER
 ADD CONSTRAINT UNIQUE_COSTCENTER_ID_USER_ID
  UNIQUE (COSTCENTERID, USERID)
  ENABLE VALIDATE;

ALTER TABLE OMP_PORTLET.COST_CENTER
 ADD CONSTRAINT UNIQUE_COSTCENTER_NAME
  UNIQUE (COSTCENTERNAME)
  ENABLE VALIDATE;