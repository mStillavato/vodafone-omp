Insert into BUDGET_CATEGORY ( budgetCategoryName)Values ('HW_new');
Insert into BUDGET_CATEGORY ( budgetCategoryName)Values ('HW_upgrade');
Insert into BUDGET_CATEGORY ( budgetCategoryName)Values ('Professional_services');
Insert into BUDGET_CATEGORY ( budgetCategoryName)Values ('Project');
Insert into BUDGET_CATEGORY ( budgetCategoryName)Values ('Site_requirements');
Insert into BUDGET_CATEGORY ( budgetCategoryName)Values ('SW_developments');
Insert into BUDGET_CATEGORY ( budgetCategoryName)Values ('SW_licenses');
Insert into BUDGET_CATEGORY ( budgetCategoryName)Values ('SW_release');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('AAA');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('ABN');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('ACLM');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('ALDSL');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('ALT2');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('BAN');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('BBN');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('BPM');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('BTS');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('CA');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('CAD');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('CALTE');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('CBN');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('CEM');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('CG');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('CIC4');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('CMX');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('CPE');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('CS2000');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('CUR');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('DLT');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('DNS');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('DRN');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('DX2');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('DXC');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('EAG');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('eNodeB');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('ESB');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('EUE');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('EUP/NAB');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('FARM');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('FemtoGW');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('FMBB');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('FMP');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('FW');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('GCG');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('GD');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('GDD');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('GLR');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('GMLC');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('GTW');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('HLR');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('HSS');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('HTS');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('I'||'&'||'C');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('ICA');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('ICP');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('IMSD');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('Infrastructure');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('INOPI');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('INR');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('IPT');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('IRU');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('ISA');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('I-SBC');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('JunoSpace');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('LAN');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('LDP');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('Legacy NIS');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('LIG');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('LSP');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('M2000');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('MAM');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('MCH');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('MDCTS');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('Measurement instruments');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('MGE');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('MGW');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('MME');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('MOD');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('MOP');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('MPC');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('MRF');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('MRN');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('MRT');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('MSC');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('MSD');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('MSP');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('MSS');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('MSS/MGW');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('NAB');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('NetAct');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('NGM');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('NGME');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('NMS');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('NodeB');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('NPC');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('NPG');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('NPS');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('OCS');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('ODN');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('OMC');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('OMV');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('OTA');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('Other');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('OVO');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('PAC/SAG');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('PCRF');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('PFMB');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('Probes');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('PSX');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('RBB');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('RBF');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('Remedy');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('RTI');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SAG');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SAR');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SBC');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SCP');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SCPC');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SDX');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SecGW');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SGS');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SGSN');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SGW');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SICAP');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SMP');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SMS');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SMS Gateway');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SMSC');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SNIP');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SoR');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('Spare Parts');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SPF');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SRF');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SRN');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SRT');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('STH');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('Storage '||'&'||' Server');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('SysLog');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('TAD');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('TAG');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('TC');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('TES');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('Testing Instruments');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('TFTP');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('TMF');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('TOP');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('TP');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('VDS');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('VFX');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('VMS');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('VNS');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('VO');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('Voicemail');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('VPN');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('VRS');
Insert into BUDGET_SUB_CATEGORY ( budgetSubCategoryName) Values ('XSPOTTER');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('AC LAN' ,'400031480','AC LAN - 400031480');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('ACCENTURE' ,'400018744','ACCENTURE - 400018744');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('Advanc� technologies s.r.l.' ,'400075275','Advanc� technologies s.r.l. - 400075275');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('Advertice SRL' ,'400062707','Advertice SRL - 400062707');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('Aircom International' ,'400062885','Aircom International - 400062885');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('ALCATEL-LUCENT' ,'400074971','ALCATEL-LUCENT - 400074971');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('ALCOR' ,'400064043','ALCOR - 400064043');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('ALETEL' ,'400061819','ALETEL - 400061819');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('ALPITEL' ,'400060483','ALPITEL - 400060483');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('ALLOT_VPC' ,'VLU07','ALLOT_VPC - VLU07');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('ALTRAN' ,'400060481','ALTRAN - 400060481');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('AMPERCOM' ,'400062256','AMPERCOM - 400062256');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('ANRITSU S.P.A' ,'400051174','ANRITSU S.P.A - 400051174');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('Arantech' ,'VLU07','Arantech - VLU07');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('Asystel' ,'400031455','Asystel - 400031455');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('ATOS' ,'400075735','ATOS - 400075735');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('ATS' ,'400031459','ATS - 400031459');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('BEEWEEB_VPC' ,'VLU07','BEEWEEB_VPC - VLU07');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('BEST ENGINEERING S.P.A.' ,'400062032','BEST ENGINEERING S.P.A. - 400062032');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('BIZPower' ,'400059048','BIZPower - 400059048');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('BLOM CGR S.P.A.' ,'400063366','BLOM CGR S.P.A. - 400063366');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('BYTEMOBILE' ,'VLU07','BYTEMOBILE - VLU07');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('CAP GEMINI' ,'400018881','CAP GEMINI - 400018881');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('Check Point' ,'VLU07','Check Point - VLU07');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('CISCO SYSTEMS ITALY SRL' ,'400099001','CISCO SYSTEMS ITALY SRL - 400099001');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('COEL Distribution s.r.l.' ,'400031471','COEL Distribution s.r.l. - 400031471');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('COMITEL' ,'400060494','COMITEL - 400060494');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('COMUNICANDO' ,'400062506','COMUNICANDO - 400062506');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('COMVERSE' ,'400005089','COMVERSE - 400005089');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('CONSODATA S.P.A.' ,'400062693','CONSODATA S.P.A. - 400062693');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('CRITICAL PATH' ,'400061773','CRITICAL PATH - 400061773');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('CSC' ,'400062411','CSC - 400062411');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('DDway S.r.l.' ,'400062411','DDway S.r.l. - 400062411');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('DELO INSTRUMENTS S.R.L.' ,'400061778','DELO INSTRUMENTS S.R.L. - 400061778');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('DELTA' ,'400000007','DELTA - 400000007');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('DETICA' ,'400057540','DETICA - 400057540');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('DEVOTEAM' ,'400062304','DEVOTEAM - 400062304');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('DQM STRUMENTAZIONI' ,'400061562','DQM STRUMENTAZIONI - 400061562');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('DS Group SpA' ,'400061821','DS Group SpA - 400061821');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('EMAZE' ,'400063285','EMAZE - 400063285');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('Engineering_IT' ,'400027847','Engineering_IT - 400027847');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('Engineering_IT_ VPC' ,'VLU07','Engineering_IT_ VPC - VLU07');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('ERICSSON' ,'400074970','ERICSSON - 400074970');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('FIAMM' ,'400021399','FIAMM - 400021399');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('FORSK' ,'400000142','FORSK - 400000142');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('GEMALTO' ,'400002319','GEMALTO - 400002319');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('GOOGLE' ,'400021147','GOOGLE - 400021147');
set define off;
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('H&S' ,'400062108','H&S - 400062108');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('HP' ,'400031550','HP - 400031550');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('HUAWEI' ,'400002325','HUAWEI - 400002325');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('IBM' ,'400018750','IBM - 400018750');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('ICT CONSULTING' ,'400062226','ICT CONSULTING - 400062226');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('INFRACOM' ,'NULL','INFRACOM - NULL');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('INNOVA srl' ,'400111267','INNOVA srl - 400111267');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('INSIGHT' ,'400066729','INSIGHT - 400066729');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('IRPE' ,'400063070','IRPE - 400063070');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('IRTE' ,'400061721','IRTE - 400061721');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('ITALTEL' ,'400030976','ITALTEL - 400030976');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('ITALTEL_VPC' ,'VLU07','ITALTEL_VPC - VLU07');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('ITECH ENGINEERING & SERVICES ' ,'400060513','ITECH ENGINEERING '||'&'||' SERVICES  - 400060513');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('JDSU ITALIA S.R.L.' ,'400109373','JDSU ITALIA S.R.L. - 400109373');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('Keynote-Sigos' ,'400002790','Keynote-Sigos - 400002790');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('L.P. INSTRUMENTS SRL' ,'400099187','L.P. INSTRUMENTS SRL - 400099187');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('LANTECH' ,'400061675','LANTECH - 400061675');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('LASER' ,'400061579','LASER - 400061579');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('LASER.MEC. SRL' ,'400102997','LASER.MEC. SRL - 400102997');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('Microsoft' ,'VLU07','Microsoft - VLU07');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('Millennium Technologies s.r.l.' ,'400062234','Millennium Technologies s.r.l. - 400062234');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('MISCO' ,'400061583','MISCO - 400061583');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('MOBIXELL' ,'400029175','MOBIXELL - 400029175');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('NOKIA SIEMENS' ,'400001999','NOKIA SIEMENS - 400001999');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('NTT-Data' ,'400031492','NTT-Data - 400031492');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('OMNIA' ,'400063400','OMNIA - 400063400');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('ONE TEAM S.R.L.' ,'400062742','ONE TEAM S.R.L. - 400062742');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('Open Soft Engineering S.r.l.' ,'400076053','Open Soft Engineering S.r.l. - 400076053');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('ORACLE_VPC' ,'VLU07','ORACLE_VPC - VLU07');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('Perani and Partners' ,'400030406','Perani and Partners - 400030406');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('Pitney Bowes' ,'400057062','Pitney Bowes - 400057062');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('PLANET' ,'400062490','PLANET - 400062490');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('POLITECNICO DI MILANO' ,'400061720','POLITECNICO DI MILANO - 400061720');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('POWERWAVE' ,'VLU07','POWERWAVE - VLU07');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('PRIDE' ,'400031462','PRIDE - 400031462');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('PRIME SERVICE' ,'400062505','PRIME SERVICE - 400062505');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('PRISMA' ,'400062578','PRISMA - 400062578');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('Qualta S.p.A.' ,'400087694','Qualta S.p.A. - 400087694');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('QUINARY' ,'400061812','QUINARY - 400061812');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('REPLY' ,'400023332','REPLY - 400023332');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('Rohde&Schwarz' ,'400061670','Rohde'||'&'||'Schwarz - 400061670');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('SEDICOM' ,'400103280','SEDICOM - 400103280');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('SEMITEC' ,'400060522','SEMITEC - 400060522');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('SESA' ,'400031933','SESA - 400031933');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('SETUP S.R.L.' ,'400062613','SETUP S.R.L. - 400062613');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('SIAE' ,'400060482','SIAE - 400060482');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('SIAE_VPC' ,'VLU07','SIAE_VPC - VLU07');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('SICAP_VPC' ,'VLU07','SICAP_VPC - VLU07');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('SIEMENS' ,'400002363','SIEMENS - 400002363');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('SINERGY' ,'400031499','SINERGY - 400031499');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('SIRA S.r.l.' ,'400019082','SIRA S.r.l. - 400019082');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('SIRA_VPC' ,'VLU07','SIRA_VPC - VLU07');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('SIRTI' ,'400060508','SIRTI - 400060508');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('SISGE Informatica Spa' ,'400031497','SISGE Informatica Spa - 400031497');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('SOLETO' ,'400060527','SOLETO - 400060527');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('Sovel Italia S.r.l.' ,'400062024','Sovel Italia S.r.l. - 400062024');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('SPAZIO ZeroUno' ,'400061874','SPAZIO ZeroUno - 400061874');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('STARHOME' ,'400021286','STARHOME - 400021286');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('Synergia ICT Progetti e Servizi Srl ' ,'400064156','Synergia ICT Progetti e Servizi Srl  - 400064156');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('TEKO TELECOM SPA' ,'400102246','TEKO TELECOM SPA - 400102246');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('TEKTRONIX_VPC' ,'VLU07','TEKTRONIX_VPC - VLU07');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('TELEBIT' ,'400060490','TELEBIT - 400060490');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('TELETTRA' ,'400060509','TELETTRA - 400060509');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('TELTEQ' ,'400064042','TELTEQ - 400064042');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('TNF' ,'VLU07','TNF - VLU07');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('UNITEAM' ,'400061832','UNITEAM - 400061832');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('URMET' ,'400062584','URMET - 400062584');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('VALUE TEAM S.P.A.' ,'400031492','VALUE TEAM S.P.A. - 400031492');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('VISIANT SPINDOX' ,'400031959','VISIANT SPINDOX - 400031959');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('VPC' ,'VLU07','VPC - VLU07');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('Wireless Future S.R.L.' ,'400062289','Wireless Future S.R.L. - 400062289');
Insert into VENDOR ( supplier,supplierCode,vendorName) Values ('XECH' ,'400031482','XECH - 400031482');
Insert into MACRO_DRIVER ( macroDriverName) Values ('Business Continuity');
Insert into MACRO_DRIVER ( macroDriverName) Values ('Challenge');
Insert into MACRO_DRIVER ( macroDriverName) Values ('CommercialRequest');
Insert into MACRO_DRIVER ( macroDriverName) Values ('Desktop');
Insert into MACRO_DRIVER ( macroDriverName) Values ('LTE');
Insert into MACRO_DRIVER ( macroDriverName) Values ('Modernization');
Insert into MACRO_DRIVER ( macroDriverName) Values ('Quality');
Insert into MACRO_DRIVER ( macroDriverName) Values ('Regionalization');
Insert into MACRO_DRIVER ( macroDriverName) Values ('Regulatory');
Insert into MACRO_DRIVER ( macroDriverName) Values ('Rollout');
Insert into MACRO_DRIVER ( macroDriverName) Values ('Traffic');
Insert into MICRO_DRIVER (microDriverName) Values ('Infra  '||'&'||'  Disaster Recovery');
Insert into MICRO_DRIVER (microDriverName) Values ('OPEX Capitalization');
Insert into MICRO_DRIVER (microDriverName) Values ('Spare parts  '||'&'||' Energy Systems');
Insert into MICRO_DRIVER (microDriverName) Values ('Support System');
Insert into MICRO_DRIVER (microDriverName) Values ('Core');
Insert into MICRO_DRIVER (microDriverName) Values ('Operations');
Insert into MICRO_DRIVER (microDriverName) Values ('Radio');
Insert into MICRO_DRIVER (microDriverName) Values ('Services');
Insert into MICRO_DRIVER (microDriverName) Values ('Fixed');
Insert into MICRO_DRIVER (microDriverName) Values ('Mobile');
Insert into MICRO_DRIVER (microDriverName) Values ('Roaming  '||'&'||' MVNOs');
Insert into MICRO_DRIVER (microDriverName) Values ('Terminal  '||'&'||' SIM');
Insert into MICRO_DRIVER (microDriverName) Values ('Desktop');
Insert into MICRO_DRIVER (microDriverName) Values ('Performance');
Insert into MICRO_DRIVER (microDriverName) Values ('Transmission');
Insert into MICRO_DRIVER (microDriverName) Values ('Platform Renewal');
Insert into MICRO_DRIVER (microDriverName) Values ('Resiliency');
Insert into MICRO_DRIVER (microDriverName) Values ('Assurance');
Insert into MICRO_DRIVER (microDriverName) Values ('AirCo Swap');
Insert into MICRO_DRIVER (microDriverName) Values ('Infrastructure Security');
Insert into MICRO_DRIVER (microDriverName) Values ('2G');
Insert into MICRO_DRIVER (microDriverName) Values ('3G / HSPA');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-CS-01: R4�-�Basic�SW�upgrade (FMFO_1366)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-CS-01: R4�-�Capacity�related�Spend (FMFO_1365)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-CS-01: R4�-�Optional�Features (FMFO_1367)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-CS-10: CS-BAU�(non�R4�investments) (FMFO_1374)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-IMS-01: A-SBC�HW�'||'&'||'�SW�Capacity (FMFO_2148)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-IMS-01: CSCF Investments (VoLTE/RCS) (FMFO_2772)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-IMS-01: IMS�-�capacity�spend (FMFO_1414)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-IMS-01: I-SBC investments (FMFO_2147)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-IMS-01: CSCF - SW Release - legacy IMS (FMFO_1417)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-IMS-01: A-SBC SW Release - legacy IMS (FMFO_2149)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-IMS-01: A-SBC Virtual Application SW (VoLTE/RCS) (FMFO_2773)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-LTE-01: Evolved�Packet�Core�(EPC)�-�MME (FMFO_1402)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-LTE-01: Evolved�Packet�Core�(EPC)�-�S/P�GW (FMFO_1403)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-PS-07: PS-NGME�HW�'||'&'||'�SW�Capacity (FMFO_1393)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-PS-07: PS-NGME Basic SW upgrades (FMFO_1394)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-PS-07: PS-NGME Optional features (FMFO_1395)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-PS-07: PS-SGSN HW '||'&'||' SW Capacity (FMFO_1396)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-PS-07: PS-SGSN Basic SW upgrades (FMFO_1397)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-PS-07: PS-SGSN Optional features (FMFO_1398)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-SIG-04: CSDB/HLR HW '||'&'||' SW Capacity (FMFO_1379)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-SIG-04: CSDB/HLR Basic SW upgrade (FMFO_1380)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-SIG-04: CSDB/HLR Opt. features (e.g. MNP, Flexible num.,.) (FMFO_1381)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-SIG-05: Investments on Diameter Routing Agent (DRA) (FMFO_2794)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-SIG-05: SIG-SGWs investments (FMFO_1387)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-SIG-10: SIG-BAU any other Spend (FMFO_1390)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-TOC-01: Telco-over-Cloud (ToC) HW infrastructure investments (FMFO_2796)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-YM-01: PCRF�HW�'||'&'||'�SW�Capacity (FMFO_2143)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-YM-01: TMF�HW�'||'&'||'�SW�Capacity (FMFO_2140)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-YM-01: TMF Basic SW upgrades (FMFO_2141)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-YM-01: TMF Optional Features (FMFO_2142)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-YM-01: PCRF Basic SW upgrades (FMFO_2144)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-YM-01: PCRF Optional features (FMFO_2145)');
Insert into ODNP_NAME ( odnpNameName) Values('CORE - CN-YM-02: Network Analytics expenditure (FMFO_2146)');
Insert into ODNP_NAME ( odnpNameName) Values('DSL�'||'&'||'�Fixed - DSL-BH-01: Other�DSL�Transport (FMFO_1522)');
Insert into ODNP_NAME ( odnpNameName) Values('Operations - Assurance (FMFO_2622)');
Insert into ODNP_NAME ( odnpNameName) Values('Operations - Element mgr (FMFO_2623)');
Insert into ODNP_NAME ( odnpNameName) Values('Operations - Overall�Testing�CAPEX (FMFO_1729)');
Insert into ODNP_NAME ( odnpNameName) Values('Operations - Service Management (FMFO_1727)');
Insert into ODNP_NAME ( odnpNameName) Values('Operations - Support '||'&'||' Readiness (FMFO_2625)');
Insert into ODNP_NAME ( odnpNameName) Values('RADIO - RAN-2G-04: 2G RAN Software: Total (FMFO_2618)');
Insert into ODNP_NAME ( odnpNameName) Values('RADIO - RAN-3G-08: 3G RAN Software: Total (FMFO_2617)');
Insert into ODNP_NAME ( odnpNameName) Values('RADIO - RAN-4G-03/04: 4G RAN Software + LTE HW Activation (FMFO_2620)');
Insert into ODNP_NAME ( odnpNameName) Values('RADIO - RAN-LP�Total: Radio�Local�Programmes (FMFO_1357)');
Insert into ODNP_NAME ( odnpNameName) Values('RADIO - RAN-ORN-09-HetNet (Femto,WiFi,In Building Solutions,Oudoor Small Cells) (FMFO_2879)');
Insert into ODNP_NAME ( odnpNameName) Values('Service�Enablers - SE-IN-01: IN�Convergent�Charging�Application SW (FMFO_2151)');
Insert into ODNP_NAME ( odnpNameName) Values('Service�Enablers - SE-IN-01: IN�Convergent�Charging�investments (FMFO_1544)');
Insert into ODNP_NAME ( odnpNameName) Values('Service�Enablers - SE-IN-01: IN�Prepay�-�Application SW (FMFO_1542)');
Insert into ODNP_NAME ( odnpNameName) Values('Service�Enablers - SE-IN-01: IN Prepay - HW and SW Platform Capacity (FMFO_1541)');
Insert into ODNP_NAME ( odnpNameName) Values('Service�Enablers - SE-IN-02: NGIN�-�Applications�and�additional�feat. (FMFO_1551)');
Insert into ODNP_NAME ( odnpNameName) Values('Service�Enablers - SE-IN-02: NGIN�-�HW�and�SW�capacity (FMFO_1547)');
Insert into ODNP_NAME ( odnpNameName) Values('Service�Enablers - SE-PLTF-01: Business Platforms and Applications (FMFO_1566)');
Insert into ODNP_NAME ( odnpNameName) Values('Service�Enablers - SE-PLTF-01: Location Capability (FMFO_2154)');
Insert into ODNP_NAME ( odnpNameName) Values('Service�Enablers - SE-PLTF-01: MSP - SW, Apps, addtl feat., Prof Svcs (FMFO_2152)');
Insert into ODNP_NAME ( odnpNameName) Values('Service�Enablers - SE-PLTF-01: Multi�Service�Proxy (FMFO_1565)');
Insert into ODNP_NAME ( odnpNameName) Values('Service�Enablers - SE-PLTF-01: SMS�platforms (FMFO_1562)');
Insert into ODNP_NAME ( odnpNameName) Values('Service�Enablers - SE-PLTF-01: Voice/Videomail (FMFO_1563)');
Insert into ODNP_NAME ( odnpNameName) Values('Service�Enablers - SE-PLTF-01:Virtual Home Environment (VHE) '||'&'||' Steering of Roaming (SoR) (FMFO_2153)');
Insert into ODNP_NAME ( odnpNameName) Values('Service�Enablers - SE-POT-01: Other�SE (FMFO_1571)');
Insert into ODNP_NAME ( odnpNameName) Values('Service�Enablers - SE-SEC-01: Clean Pipe Investments (FMFO_2771)');
Insert into ODNP_NAME ( odnpNameName) Values('Service�Enablers - SE-SEC-01: Technology Security Baseline (TSB) unallocated investments (FMFO_2770)');
Insert into ODNP_NAME ( odnpNameName) Values('Transport - TX-AC-05/09: Microwave�Ethernet�(BEP2.0) (FMFO_1997)');
Insert into ODNP_NAME ( odnpNameName) Values('Transport - TX-BH-01: BH-BEP1.0�Strategic�aggregation�IP/MPLS (FMFO_1462)');
Insert into ODNP_NAME ( odnpNameName) Values('Transport - TX-BH-03: BH�-�Regional�fibre�infrastructure (FMFO_1989)');
Insert into ODNP_NAME ( odnpNameName) Values('Transport - TX-BH-04: BH�-�BBNS�deployment (FMFO_1476)');
Insert into ODNP_NAME ( odnpNameName) Values('Transport - TX-CO-02: Core�-�SDH�equipment (FMFO_1480)');
Insert into ODNP_NAME ( odnpNameName) Values('Transport - TX-IP-01: CPN�'||'&'||'�CIH (FMFO_1486)');
Insert into ODNP_NAME ( odnpNameName) Values('Transport - TX-SEC-01: LTE Security Gateways Professional Services (FMFO_2801)');
Insert into ODNP_NAME ( odnpNameName) Values('Transport - TX-SEC-01: LTE�Security�Gateways (FMFO_1492)');


SET DEFINE OFF;
Insert into OMP_PORTLET.PROJECT
   (PROJECTID, PROJECTNAME)
 Values
   (1, '2G');
Insert into OMP_PORTLET.PROJECT
   (PROJECTID, PROJECTNAME)
 Values
   (2, '3G');
Insert into OMP_PORTLET.PROJECT
   (PROJECTID, PROJECTNAME)
 Values
   (3, 'Business Process Support Systems');
Insert into OMP_PORTLET.PROJECT
   (PROJECTID, PROJECTNAME)
 Values
   (6, 'Marketing & regulatory products/services');
Insert into OMP_PORTLET.PROJECT
   (PROJECTID, PROJECTNAME)
 Values
   (5, 'LTE');
Insert into OMP_PORTLET.PROJECT
   (PROJECTID, PROJECTNAME)
 Values
   (7, 'Mobile Voice sustain');
Insert into OMP_PORTLET.PROJECT
   (PROJECTID, PROJECTNAME)
 Values
   (8, 'Network and service sustain');
Insert into OMP_PORTLET.PROJECT
   (PROJECTID, PROJECTNAME)
 Values
   (9, 'Smart & Security Network');
Insert into OMP_PORTLET.PROJECT
   (PROJECTID, PROJECTNAME)
 Values
   (10, 'SW_developments');
Insert into OMP_PORTLET.PROJECT
   (PROJECTID, PROJECTNAME)
 Values
   (12, 'Mobile Data sustain');
Insert into OMP_PORTLET.PROJECT
   (PROJECTID, PROJECTNAME)
 Values
   (4, 'Fixed network (VRU/DSL)');
COMMIT;

SET DEFINE OFF;

Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (1, '2G SW');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (2, '3G SW');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (3, 'Antennas');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (4, 'Femtocell');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (5, 'Smallcell');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (6, 'BIG data');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (7, 'FARM & National Backup');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (8, 'Fault Management Systems for mobile');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (9, 'MVNO');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (10, 'NetAct RAN Regionalization');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (11, 'NIS');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (12, 'NOC Transformation');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (13, 'OSS Management');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (14, 'Performance/Assurance Management System for mobile');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (15, 'Remote Access System');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (16, 'SLA management');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (17, 'DSL');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (18, 'Fixed CS network');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (19, 'TeleTu');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (20, 'VRU');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (21, 'VRU/C Operator Console');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (22, '4G SW');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (23, 'Certification Authority for LTE');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (24, 'CSDB deployment');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (25, 'CSFB');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (26, 'Diameter Router');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (27, 'EPC Deployment');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (28, 'Performance Optimization Systems');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (29, 'Renewal LI for 4G');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (30, 'Roaming LTE ');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (31, 'Security GW');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (32, 'Services for LTE');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (33, 'SMS GW for VoLTE');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (34, 'Terminal Evolution');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (35, 'Touchpoint');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (36, 'VoLTE');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (37, 'Application Based Charging');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (38, 'Canvass Italia');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (39, 'Canvass Malta');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (40, 'CleanPipe');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (41, 'EU Roaming Provider (ARP)');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (42, 'IN OCS Convergence');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (43, 'LAN Infrastructure for SG/ST');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (44, 'MVNO SIM qualifications');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (45, 'New MKTG requests');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (46, 'NFC');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (47, 'OTA SIM Configuration');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (48, 'OTA SIM MNO Portal');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (49, 'OTA SIM new services');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (50, 'OTA TERMINAL configuration');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (51, 'Salvadanaio');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (52, 'Share Everything');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (53, 'SIM Certification');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (54, 'SMS GW data production');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (55, 'SMS GW services');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (56, 'SMS services');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (57, 'SMSC data production');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (58, 'Sustainability and media relations');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (59, 'Technology validation');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (60, 'TSB');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (61, 'VF Rete Sicura');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (62, 'VOGE');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (63, 'Vulnerabilities');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (64, 'Core PS Legacy');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (65, 'Core Transmission');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (66, 'E2E planning tool');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (67, 'Fiber to PoC 2/3');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (68, 'Long Distance Fiber');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (69, 'MW Performance Optimization Systems');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (70, 'Other Regional Backhauling');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (71, 'WDWM Regional BH');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (72, 'Call Centres initiatives');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (73, 'CS modernization / swap');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (74, 'CS2K swap');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (75, 'Data Production Mobile Voice');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (76, 'IP interconnection');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (77, 'Mobile CS network');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (78, 'Mobile voice quality');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (79, 'New CS SW release');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (80, 'Resiliency');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (81, 'ARS renewal');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (82, 'BIS/CH renewal');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (83, 'D-Router Virtualization');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (84, 'Energy Efficiency');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (85, 'FMP renewal');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (86, 'Network Abstraction Layer');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (87, 'Offer Management Regionalization');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (88, 'One-Net and VPN');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (89, 'One-Net and VPN Regional Consolidation');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (90, 'OneNet Hosting (AL - MT)');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (91, 'OneNet SelfCare');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (92, 'Radio Configuration');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (93, 'Radio Optimization');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (94, 'Radio Planning');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (95, 'Regional Consolidation for OTA SIM (VF GR Hosting)');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (96, 'Regional Consolidation for Terminal Detection (VF AL Hosting)');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (97, 'Regional VoiceMail');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (98, 'Renewal GMLC');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (99, 'Roaming platform evolution');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (100, 'Rollout enabler');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (101, 'SDD-Transformation');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (102, 'Service Platforms integration');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (103, 'SGW Software Upgrade');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (104, 'Signalling growth');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (105, 'Smart Metering');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (106, 'SMS GW renewal');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (107, 'Streaming Services');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (108, 'Testing Center');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (109, 'Traffic Engineering');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (110, 'Voicemail services');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (111, 'LAN Mobile');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (112, 'LAN Servizi Renewal');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (113, 'MSP Data Traffic upgrade');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (114, 'MSP enhancement');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (115, 'Security enhancements');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (116, 'Smart Network');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (117, 'Traffic Manager evolution');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (118, 'Traffic Manager Upgrade');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (119, 'TSB2');
Insert into OMP_PORTLET.SUB_PROJECT
   (SUBPROJECTID, SUBPROJECTNAME)
 Values
   (120, 'TAD');

COMMIT;


SET DEFINE OFF;

INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = '2G') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = '2G SW')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = '3G') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = '3G SW')
        );

INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = '3G') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Antennas')
        );

INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = '3G') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Femtocell')
        );

INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = '3G') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Smallcell')
        );

INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Business Process Support Systems') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'BIG data')
        );

INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Business Process Support Systems') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = to_char('FARM & National Backup'))
        );

INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Business Process Support Systems') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Fault Management Systems for mobile')
        );
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Business Process Support Systems') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'MVNO')
        );

INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Business Process Support Systems') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'NetAct RAN Regionalization')
        );
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Business Process Support Systems') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'NIS')
        );

INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Business Process Support Systems') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'NOC Transformation')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Business Process Support Systems') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'OSS Management')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Business Process Support Systems') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Performance/Assurance Management System for mobile')
        );            
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Business Process Support Systems') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Remote Access System')
        );            
        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Business Process Support Systems') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'SLA management')
        );            
        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Fixed network (VRU/DSL)') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'DSL')
        );            
                
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Fixed network (VRU/DSL)') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Fixed CS network')
        );
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Fixed network (VRU/DSL)') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'TeleTu')
        );
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Fixed network (VRU/DSL)') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'VRU')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Fixed network (VRU/DSL)') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'VRU/C Operator Console')
        );    
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'LTE') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = '4G SW')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'LTE') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Certification Authority for LTE')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'LTE') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'CSDB deployment')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'LTE') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'CSFB')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'LTE') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Diameter Router')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'LTE') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'EPC Deployment')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'LTE') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Performance Optimization Systems')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'LTE') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Renewal LI for 4G')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'LTE') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Roaming LTE ')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'LTE') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Security GW')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'LTE') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Services for LTE')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'LTE') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'SMS GW for VoLTE')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'LTE') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Terminal Evolution')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'LTE') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Touchpoint')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'LTE') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'VoLTE')
        );

        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Application Based Charging')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Canvass Italia')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Canvass Malta')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'CleanPipe')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'EU Roaming Provider (ARP)')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'IN OCS Convergence')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'LAN Infrastructure for SG/ST')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'MVNO SIM qualifications')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'New MKTG requests')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'NFC')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'OTA SIM Configuration')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'OTA SIM MNO Portal')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'OTA SIM new services')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'OTA TERMINAL configuration')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Salvadanaio')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Share Everything')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'SIM Certification')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'SMS GW data production')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'SMS GW services')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'SMS services')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'SMSC data production')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Sustainability and media relations')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Technology validation')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'TSB')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'VF Rete Sicura')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'VOGE')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Marketing & regulatory products/services') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Vulnerabilities')
        );
        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Mobile Data sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Core PS Legacy')
        );
                
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Mobile Data sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Core Transmission')
        );
                
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Mobile Data sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'E2E planning tool')
        );
                
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Mobile Data sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Fiber to PoC 2/3')
        );
                
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Mobile Data sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Long Distance Fiber')
        );
                
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Mobile Data sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'MW Performance Optimization Systems')
        );
                
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Mobile Data sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Other Regional Backhauling')
        );
                
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Mobile Data sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'WDWM Regional BH')
        );
                
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Mobile Voice sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Call Centres initiatives')
        );
                
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Mobile Voice sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'CS modernization / swap')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Mobile Voice sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'CS2K swap')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Mobile Voice sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Data Production Mobile Voice')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Mobile Voice sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'IP interconnection')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Mobile Voice sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Mobile CS network')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Mobile Voice sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Mobile voice quality')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Mobile Voice sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'New CS SW release')
        );
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Mobile Voice sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Resiliency')
        );        
        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'ARS renewal')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'BIS/CH renewal')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'D-Router Virtualization')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Energy Efficiency')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'FMP renewal')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Network Abstraction Layer')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Offer Management Regionalization')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'One-Net and VPN')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'One-Net and VPN Regional Consolidation')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'OneNet Hosting (AL - MT)')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'OneNet SelfCare')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Radio Configuration')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Radio Optimization')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Radio Planning')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Regional Consolidation for OTA SIM (VF GR Hosting)')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Regional Consolidation for Terminal Detection (VF AL Hosting)')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Regional VoiceMail')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Renewal GMLC')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Roaming platform evolution')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Rollout enabler')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'SDD-Transformation')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Service Platforms integration')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'SGW Software Upgrade')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Signalling growth')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Smart Metering')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'SMS GW renewal')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Streaming Services')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Testing Center')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Traffic Engineering')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Network and service sustain') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Voicemail services')
        );        
    
    
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Smart & Security Network') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'LAN Mobile')
        );        
    
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Smart & Security Network') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'LAN Servizi Renewal')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Smart & Security Network') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'MSP Data Traffic upgrade')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Smart & Security Network') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'MSP enhancement')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Smart & Security Network') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Security enhancements')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Smart & Security Network') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Smart Network')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Smart & Security Network') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Traffic Manager evolution')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Smart & Security Network') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'Traffic Manager Upgrade')
        );        
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'Smart & Security Network') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'TSB2')
        );        
    
        
INSERT INTO OMP_PORTLET.PROJECT_SUB_PROJECT (PROJECTID, SUBPROJECTID) 
VALUES ( 
    (SELECT PROJECTID FROM OMP_PORTLET.PROJECT where PROJECTNAME = 'SW_developments') , 
        (SELECT SUBPROJECTID FROM OMP_PORTLET.SUB_PROJECT where SUBPROJECTNAME = 'TAD')
        );            
		
		
		SET DEFINE OFF;
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (1, 'N.20000067.001 - NE-LTE-4G LTE Deployment 1671');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (2, 'N.20000067.002 (only FY14) - NE-LTE-NTW SERVICE ENABLERS3229');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (3, 'N.20000067.003 (only FY14) - NE-LTE-IP BACKBONE (CPN & CIH) 1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (4, 'N.20000067.004 - NE-LTE-ELEMENT & NW MANAGEMENT 4620');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (5, 'N.20000067.006 - NE-PS LTE 1673');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (6, 'N.20000067.007 - NE-LTE Transmission 4600');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (7, 'N.20000067.008 - NE-CS SW LTE 1673');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (8, 'N.20000067.009 (only FY14) - NE-CS LTE 1673');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (9, 'N.20000067.010 - NE-Sig LTE 1673');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (10, 'N.20000067.012 - NE-CDR-LTE CS&Signalling 1673');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (11, 'N.20000067.015 - NE-LTE-IMASys-4G LTE radio 1671');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (12, 'N.20000067.016 - NE-LTE-RGISSys-4G LTE radio 1671');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (13, 'N.20000067.018 - NE-LTE-IMSServices-IN 1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (14, 'N.20000067.019 - NE-LTE-INAdvancedServices-IN 1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (15, 'N.20000067.020 - NE-LTE-Messaging-Services 1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (16, 'N.20000067.021 - NE-LTE-SIM&VAS-Services 1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (17, 'N.20000067.022 - NE-LTE-Voice-Services 1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (18, 'N.20000067.023 - NE-LTE-NetwPerfManag-Operations 4620');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (19, 'N.20000090.008 (only FY14) - NE- BBNS 4600');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (20, 'N.20000112.001 - NE-New Services-IN PREPAY 1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (21, 'N.20000112.002 - NE-New Services-NTW SERVICEENABLERS 1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (22, 'N.20000112.003 - NE-New Services-NTW SERVICEENABLERS 4751');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (23, 'N.20000112.004 - NE-New Services-NGIN 1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (24, 'N.20000112.005 - NE-New Services-ELEMENT&MANAGEMENT 4620');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (25, 'N.20000112.009 - NE-PCRF_TMF-IMS 1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (26, 'N.20000112.010 (only FY14) - NE-DSL-SE-Ntw Services 1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (27, 'N.20000112.011 - NE-INP-RadioSWUpg 4751');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (28, 'N.20000112.012 - NE-INP Operations 4751');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (29, 'N.20000112.013 - NE-INAdvancedServices-IN 1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (30, 'N.20000112.014 - NE-New Services-VideoServices 1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (31, 'N.20000112.016 - NE-New Services-NTW SERVICEENABLERS 1705');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (32, 'N.20000112.017 - NE-Inventory-Operations 4620');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (33, 'N.20000113.001 (only FY14) - NE-Re.H&S-IN PREPAY 1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (34, 'N.20000113.004 (only FY14) - NE-Re.H&S-NGIN 1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (35, 'N.20000113.005 (only FY14) - NE-Re.H&S-SMS PLATFORMS 1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (36, 'N.20000113.007 - NE-Re.H&S-ELEMENT & NW MANAGEMENT 4620');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (37, 'N.20000114.001 - NE-Traffic-IP BACKBONE (CPN & CIH)1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (38, 'N.20000114.002 - NE-Traffic-MULTI SRV PROXY WAP GATE1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (39, 'N.20000114.003 - NE-Traffic-SMS PLATFORMS1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (40, 'N.20000114.004 - NE-Traffic-VOICE/VIDEOMAIL1684');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (41, 'N.20000114.005 (only FY14) - NE-Traffic-3G WIDE AREA COVERAGE1671');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (42, 'N.20000114.007 - NE-Traffic-ELEMENT & NW MANAGEMENT4620');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (43, 'N.20000114.009 - NE-Traffic-BackhaulAgg.(IP/Eth mig) 4600');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (44, 'N.20000114.010 - NE-Traffic-CORE TRANS.SELF-BUILD 4600');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (45, 'N.20000114.011 (only FY14) - NE-Traffic-PS EVOLUTION1673');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (46, 'N.20000114.012 (only FY14) - NE-Traffic-PS-BAU1673');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (47, 'N.20000114.013 (only FY14) - NE-Traffic-SIGTRAN1673');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (48, 'N.20000114.014 - NE-Fibre & MW Ethernet 4600');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (49, 'N.20000114.015 (only FY14) - NE-Sig Software 1673');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (50, 'N.20000114.019 - NE-Sig CS&Signalling 1673');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (51, 'N.20000114.020 - NE- BBNS 4600');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (52, 'N.20000115.001 (only FY14) - NE-Radio Access 2G-2G BSS Software1671');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (53, 'N.20000115.002 (only FY14) - NE-RadioAccess2G-ELEMENT&NWMANAG.1671');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (54, 'N.20000116.001 (only FY14) - NE-Radio Access 3G-3G RAN Software1671');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (55, 'N.20000116.002 (only FY14) - NE-Radio Access 3G-ELEMENT&NW MANAG1671');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (56, 'N.20000116.003 - NE-RGISOpt-Software2G&3G 1671');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (57, 'N.20000116.004 - NE-Radio Access 3G-ELEMENT&NW MANAG4620');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (58, 'N.20000116.006 - NE-IMAOpt-Software2G&3G 1671');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (59, 'N.20000116.007 - NE-IMASys-Software2G&3G 1671');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (60, 'N.20000116.008 - NE-RGISSys-Software2G&3G 1671');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (61, 'N.20000116.009 - NE-REP-Software2G&3G 1682');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (62, 'N.20000117.003 - NE-Core Rel. Inv.-R4 1673');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (63, 'N.20000117.004 (only FY14) - NE-CS HW Switch sites, tools and spare p');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (64, 'N.20000117.005 - NE-CS SW Software 1673');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (65, 'N.20000117.006 - NE-CS SW Circuit switched & Signalling c');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (66, 'N.20000117.007 - NE-Strategy Software  1673');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (67, 'N.20000117.008 (only FY14) - NE-NTW Data Packet switched & IMS capaci');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (68, 'N.20000117.009 (only FY14) - NE-CS-Packet & IMS 1673');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (69, 'N.20000117.011 - NE-VNS CS & Signalling 1673');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (70, 'N.20000117.012 - NE-IMS-PS&IMS 1673');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (71, 'N.20000118.001 - NE-Op.Sy.Tool-NTW SERVICE ENABLERS3229');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (72, 'N.20000118.002 - NE-Op.Sy.Tool-3G WIDE AREA COVERAGE4600');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (73, 'N.20000118.004 (only FY14) - NE-Op.Sy.Tool-NTW SERVICE ENABLERS1671');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (74, 'N.20000118.005 - NE-Op.Sy.Tool-ELEMENT&NW MANAGEMENT4620');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (75, 'N.20000118.008 - NE-Op.Sy.Tool-TESTING4620');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (76, 'N.20000118.009 - NE-RE-tools & optimisation 1671');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (77, 'N.20000118.010 - NE-IMAOpt-tools & optimisation 1671');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (78, 'N.20000118.011 - NE-IMASys-tools & optimisation 1671');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (79, 'N.20000118.013 - NE-RGISOpt-tools & optimisation 1671');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (80, 'N.20000118.014 - NE-CE-tools & optimisation 1671');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (81, 'N.20000118.015 - NE-RANPerf-tools & optimisation 1671');
Insert into OMP_PORTLET.WBS_CODE
   (WBSCODEID, WBSCODENAME)
 Values
   (82, 'N.20000223.001 (only FY14) - NE-Tech. Arch.-1682');
COMMIT;

		
/